{
  "$schema": "http://json-schema.org/draft-07/schema#",
  "$ref": "#/definitions/BusinessConfig",
  "definitions": {
    "BusinessConfig": {
      "type": "object",
      "properties": {
        "enableLogging": {
          "type": "boolean",
          "description": "Indicates if Game Client logging should be enabled or not. When disabled, the\r Game Client will still generate logs, but they will be minimal."
        },
        "clientDeploymentId": {
          "type": "string",
          "description": "The string name of the client deployment that this Business Config instance\r specifies. Typically, the game client will be deployed to a game site, and\r this field would give us a name for the licensee (but other specialized\r deployments are possible)."
        },
        "clientIdPrefix": {
          "type": "string",
          "description": "A string that is prefixed to the gameClientId, when sending requests to\r the backend server. For example, where gameClientId is \"fowlplaygold\", to\r identify the specific deployment of fowl play gold (eg: on the licensee web\r site \"betium\"), the clientIdPrefix is \"betium\", so we send the string\r \"betiumfowlplaygold\" in a dedicated field in every request sent to the server."
        },
        "clientIdPrefixV1": {
          "type": "string",
          "description": "A string that will be prefixed to the gameClientId, when sending requests to\r the backend server, for the V1 platform. This string is optional: if specified,\r it serves as an override for the vlaue of clientIdPrefix: if absent, then the\r value of clientIdPrefix will be used instead."
        },
        "productionServerUrl": {
          "type": "string",
          "description": "The Url that will be used as the target endpoint in production, for all backend\r service requests."
        },
        "resumeSession": {
          "$ref": "#/definitions/ResumeSessionConfig",
          "description": "Configuration for Resume Seession Functionality. This is an optional config: if\r not specified, appropriate defaults will be used."
        },
        "startSession": {
          "$ref": "#/definitions/StartSessionConfig",
          "description": "Configuration for the Start Session functionality. This is an optional config\r object: some integrations (eg: SGDigital, which uses a dedicated AppController)\r do not have any configurability for this functionality, so its pointless to\r specify it in all cases. If an AppController which does support variability in\r how a Session is started is being used, then if this field is not present, the\r App Controller should simply use default behaviour (start the Session quietly,\r using max available credit)."
        },
        "addCredit": {
          "$ref": "#/definitions/AddCreditConfig",
          "description": "General configuration, to indicate if the Add Credit operation should be\r available. A button may be configured for the GUI in certain places, but the\r game client will also offer the player the chance to add credit when they are\r out of credit (for example, in a dialog shown to the player). If this field is\r absent, then \"Add Credit\" should be assumed to not be enabled at all for the\r game client."
        },
        "exitClientActionMobile": {
          "$ref": "#/definitions/ExitClientAction",
          "description": "A default Exit client action to be used for Mobile games. This is optional\r (not all games will use this feature). If any view has an Exit Client button\r configured, this is the default action that should be used (unless the view\r gets its own custom configuration)."
        },
        "exitClientActionDesktop": {
          "$ref": "#/definitions/ExitClientAction",
          "description": "A default Exit client action to be used for Desktop games. This is optional\r (not all games will use this feature). If any view has an Exit Client button\r configured, this is the default action that should be used (unless the view\r gets its own custom configuration)"
        },
        "urlSelectionView": {
          "$ref": "#/definitions/UrlSelectionViewConfig",
          "description": "Configuration options for the Url Selection View."
        },
        "cashierView": {
          "$ref": "#/definitions/CashierViewConfig",
          "description": "Configuration for the Cashier View."
        },
        "sessionStatsView": {
          "$ref": "#/definitions/SessionStatsViewConfig",
          "description": "Configuration features for the Session Stats view."
        },
        "forfunAdvert": {
          "$ref": "#/definitions/ForFunAdvertConfig",
          "description": "Configuration for a For Fun advert. This advert will only be shown on a ForFun game client\r (its not 100% clear what this will mean, moving forward)."
        },
        "desktopGui": {
          "$ref": "#/definitions/DesktopGuiConfig",
          "description": "Configuration for the main Desktop Gui."
        },
        "mobileGui": {
          "$ref": "#/definitions/MobileGuiConfig",
          "description": "Confoguration for the main Mobile Gui."
        },
        "externalHistoryAction": {
          "$ref": "#/definitions/OpenLinkAction",
          "description": "Default action for an External History button. All new game clients will use the external\r history functionality. We do not currently allow differentation between desktop and mobile,\r although it cannot be ruled out that this will be changed in a future modification."
        },
        "autoplayPresets": {
          "type": "array",
          "items": {
            "type": "number"
          },
          "description": "A custom set of autoplay presets (whole numbers of autoplays which the player may select\r for a new autoplay session). When not specified, a default set of values will be used."
        },
        "responsibleGamingLinks": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/ResponsibleGamingLinkConfig"
          },
          "description": "The standard set of responsible gaming links to show."
        },
        "enabledGameModes": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/GameplayMode"
          },
          "description": "A list of all Gameplay Modes explicitly enabled for this client. This is only used now for\r V1 builds (V2 always accepts Session Mode information through a url parameter)."
        },
        "gameModeUrlParameter": {
          "type": "string",
          "description": "Indicates the string id of a url parameter, which contains information on what \"game modes\" are\r allowed. This field is used for V1 platform only. Game mode in this case, means the type of session\r that may be launched (eg: freeplay, realplay, etc).\r \r If this field is configured in BusinessConfig, then the V1 game client should check for the presence\r of a url parameter with the same key as the value of this field. The v1 platform passes a single\r numerical enum to indicate the combination of game modes to be enabled (either a single game mode, or\r several, can be specified). Not all V1 game client deployments will use this field (or the mechanism\r of configurating game modes via url parameter), so this field is optional."
        },
        "quickLoad":{
          "$ref": "#/definitions/QuickLoadConfig",
          "description": "Configuration which allows the quick load feature to be enabled. This will take the player straight to the cashier view or the game in idle state."
        }
      },
      "required": [
        "enableLogging",
        "clientDeploymentId",
        "clientIdPrefix",
        "productionServerUrl",
        "urlSelectionView",
        "cashierView",
        "sessionStatsView",
        "forfunAdvert",
        "desktopGui",
        "mobileGui",
        "externalHistoryAction",
        "responsibleGamingLinks",
        "quickLoad"
      ],
      "additionalProperties": false,
      "description": "Definition for the Business Config object. Each client deployment, will have a\r Business Config instance associated with it. This allows us to customize features\r of the client behaviour, specifically for certain websites."
    },
    "ResumeSessionConfig": {
      "type": "object",
      "properties": {
        "showNotification": {
          "type": "boolean",
          "description": "If set to true, a notification that a session is being resumed will be shown to the\r player after the loading screen. If set to false, then no notification will be used,\r and the game will skip straight to starting the resumed game."
        }
      },
      "required": [
        "showNotification"
      ],
      "additionalProperties": false,
      "description": "Configuration for Resume Session functionality."
    },
    "StartSessionConfig": {
      "type": "object",
      "properties": {
        "useCashier": {
          "type": "boolean",
          "description": "If true, the Session should be opened using the Cashier. If false, no Cashier will be\r used for starting the session: instead, the Game Client will attempt to open the session\r behgin the scenes in a single step, and to bring the maximum possible credit into the\r session (eg: the maximum amount the player has available, as long as it does not exceed\r any \"max credit in session\" rules that may apply to the legal jurisdiction in which the\r Game Client is running)."
        }
      },
      "required": [
        "useCashier"
      ],
      "additionalProperties": false,
      "description": "Configuration for how a Session is started in the Game Client. This applies to V2 only."
    },
    "AddCreditConfig": {
      "type": "object",
      "properties": {
        "isEnabled": {
          "type": "boolean",
          "description": "Indicates if this feature is enabled or not."
        },
        "useCashier": {
          "type": "boolean",
          "description": "If true, Add Credit will be performed using the Cashier. If false, then the Add Credit\r operation will be performed without the Cashier: the Game Client will attempt to bring in\r the maximum amount of credit possible in the operation (eg: the maximum amount that the\r player has available, as long as it would not exceed Session Transfer limits for the legal\r jurisdiction in which the Game Client is being played)."
        }
      },
      "required": [
        "isEnabled",
        "useCashier"
      ],
      "additionalProperties": false,
      "description": "Configuration for the Add Credit feature. It is possible to specify both whether Add Credit\r is allowed for the Game Client, and if it is, whether we use the Cashier or not."
    },
    "ExitClientAction": {
      "anyOf": [
        {
          "$ref": "#/definitions/RedirectExitClientAction"
        },
        {
          "$ref": "#/definitions/RedirectToUrlParamExitClientAction"
        },
        {
          "$ref": "#/definitions/PostMessageExitClientAction"
        },
        {
          "$ref": "#/definitions/CloseCurrentTabExitClientAction"
        }
      ],
      "description": "Configuration for an Exit Client button. This config defines HOW any exit client button\r could work: WHERE Exit Client buttons are included in the game client is configured separately.\r Please note, it is common to include separate configurations for both mobile and desktop (or to\r only provide an Exit Client button in one of these cases)\r \r Serveral mechanisms for Exiting a game client can currently be offered:\r - a url can be specified. In this case, invoking the Exit Client button will attempt to redirect\r   directly to this url.\r - a url parameter can be specified: \r - an action can be invoked directly.\r \r In theory, the game client can offer the Exit Client feature from a number of places (the\r most common is to include it in the session stats view, but we have also had requests to make\r it available in the error dialog popup)."
    },
    "RedirectExitClientAction": {
      "type": "object",
      "properties": {
        "type": {
          "type": "string",
          "const": "redirect"
        },
        "url": {
          "type": "string",
          "description": "The url that the redirect should be to."
        }
      },
      "required": [
        "type",
        "url"
      ],
      "additionalProperties": false,
      "description": "Config for an Exit Client Action, which redirects the browser to a hard-coded url."
    },
    "RedirectToUrlParamExitClientAction": {
      "type": "object",
      "properties": {
        "type": {
          "type": "string",
          "const": "redirectToUrlInParam"
        },
        "urlParam": {
          "type": "string",
          "description": "The name of the Url Parameter, that contains the URL to which we must redirect."
        }
      },
      "required": [
        "type",
        "urlParam"
      ],
      "additionalProperties": false,
      "description": "Config for an Exit Client Action which redirects the browser to a specific url: the url is\r supplied by a url parameter. When the url parameter is absent, we should not enable any button\r that requires this action."
    },
    "PostMessageExitClientAction": {
      "type": "object",
      "properties": {
        "type": {
          "type": "string",
          "const": "postMessage"
        },
        "postMessageCommand": {
          "type": "string",
          "description": "The command string to pass to postMessage. This is basically the custom event id that the\r licensee website must be listening out for."
        },
        "postMessageTargets": {
          "type": "string",
          "description": "A series of targets to post the message to. We expect this to be dot delineated, eg:\r \"parent.parent\" would invoke \"window.parent.parent.postMessage(postMessageCommand)\".\r You should not add \"window\" to the beginning of this string.\r \r It is also possible to specify a child object of the parent iframe (although this would be\r an unusual way to configure it): for example, by specifying \"parent.childObject\" (this will\r cause the call to be \"window.parent.childObject.postMessage(postMessageCommand)\"."
        }
      },
      "required": [
        "type",
        "postMessageCommand",
        "postMessageTargets"
      ],
      "additionalProperties": false,
      "description": "Config for an Exit Client Action, which sends a command to a parent iframe, telling that iframe\r to shut the window that we are in. This command must be supported by the licensee: the browser\r provides a generic mechanism for sending a request message to a parent iframe, but the actual\r mechanism for closing the window we are in (which could be some kind of custom popup container,\r but contained within an html page) must be a custom implementation provided by the website\r maintainers.\r \r This uses \"Window.postMessage\", as provided by the browser environment. This form of Exit Client\r Action is generally only appropriate for desktop mode (because on mobile, we are unlikely to have\r our game client open with such a modal popup).\r \r We can pass a custom message string,w hich is specified in the field \"postMessageCommand\". This\r message string is effectively the custom event that the licensee website must be listening for.\r We can also customize the \"target\" of the message, using the field \"postMessageTargets\": a simple\r example would have us sending our custom message to \"window.parent\", but on some licensee websites,\r we may need to send the message up through more than 1 iframe layer, and possibly even to a specific\r child object of the parent iframe. The target is basically the object upon which we will actually\r call \"postMessage\"."
    },
    "CloseCurrentTabExitClientAction": {
      "type": "object",
      "properties": {
        "type": {
          "type": "string",
          "const": "closeCurrentTab"
        }
      },
      "required": [
        "type"
      ],
      "additionalProperties": false,
      "description": "Config for an Exit Client Action, which attempts to close the current browser tab, by invoking\r \"window.close\". This will only work if the current tab has been opened programatically, according\r to mozilla docs: https://developer.mozilla.org/en-US/docs/Web/API/Window/close.\r \r Therefore, this is not appropriate for all licensee websites, but if the licensee confirms to us\r that the game client is running in a browser frame that was opened this way, then this configuration\r should work in most modern browsers."
    },
    "UrlSelectionViewConfig": {
      "type": "object",
      "properties": {
        "isEnabled": {
          "type": "boolean",
          "description": "Indicates if the Url selection view is enabled. This view is normally shown for debug\r and testing builds (and never in production), but this option exists to allow us to\r explicitly override its inclusion."
        }
      },
      "required": [
        "isEnabled"
      ],
      "additionalProperties": false,
      "description": "Config for the Url Selection View."
    },
    "CashierViewConfig": {
      "type": "object",
      "properties": {
        "exitClientButton": {
          "type": "object",
          "properties": {
            "isEnabled": {
              "type": "boolean",
              "description": "Indicates if the Exit Client button should be enabled."
            },
            "customActionDesktop": {
              "$ref": "#/definitions/ExitClientAction",
              "description": "A custom exit client action for desktop (if not specified, default exit client action\r will be invoked)"
            },
            "customActionMobile": {
              "$ref": "#/definitions/ExitClientAction",
              "description": "A custom exit client action for desktop (if not specified, default exit client action\r will be invoked)"
            }
          },
          "required": [
            "isEnabled"
          ],
          "additionalProperties": false,
          "description": "Optional config, specifying if an Exit Client button should be included in the Cashier\r (in the top right corner). This was originally requested by BetFlag asked for it, and is\r highly unlikely to ever be used in any other circumstance.\r \r If this field is not specified, then the Exit Client button should not be included."
        }
      },
      "additionalProperties": false,
      "description": "Config for the Cashier View. The Cashier can be used either when starting a session, or adding\r credit to an existing session. There are separate feature configurations for both starting a\r session and adding credit, which may specify whether the Cashier is used for them. The Cashier\r config simply configures functionality available within the cas"
    },
    "SessionStatsViewConfig": {
      "type": "object",
      "properties": {
        "exitClientButtonDesktop": {
          "$ref": "#/definitions/ExitClientButtonConfig",
          "description": "Configuration for an \"Exit Game Client\" button that can appear in the footer\r on the desktop version of the game client. The button is optional (this config\r object allows us to explicitly enable / disable it)."
        },
        "exitClientButtonMobile": {
          "$ref": "#/definitions/ExitClientButtonConfig",
          "description": "Configuration for an \"Exit Game Client\" button that can appear in the footer\r on the mobile version of the game client. The button is optional (this config\r object allows us to explicitly enable / disable it)."
        },
        "restartSessionButtonDesktop": {
          "$ref": "#/definitions/SimpleButtonConfig",
          "description": "Configuration for a \"Restart Session\" button that can appear in the footer\r on the desktop version of the game client. The button is optional (this config\r object allows us to explicitly enable / disable it).\r \r This button will only be available when a session is closed, if the player\r has not breached any session credit related rules (eg: if the player already\r transferred max credit to the just closed session, then the button will not\r be available, even if it is enabled for the client)"
        },
        "restartSessionButtonMobile": {
          "$ref": "#/definitions/SimpleButtonConfig",
          "description": "Configuration for a \"Restart Session\" button that can appear in the footer\r on the mobile version of the game client. The button is optional (this config\r object allows us to explicitly enable / disable it).\r \r This button will only be available when a session is closed, if the player\r has not breached any session credit related rules (eg: if the player already\r transferred max credit to the just closed session, then the button will not\r be available, even if it is enabled for the client)"
        }
      },
      "additionalProperties": false,
      "description": "Features that are enabled for the Default Session Stats view."
    },
    "ExitClientButtonConfig": {
      "type": "object",
      "properties": {
        "isEnabled": {
          "type": "boolean",
          "description": "Indicates if this feature is enabled or not."
        },
        "customAction": {
          "$ref": "#/definitions/ExitClientAction",
          "description": "A custom exit client action, that will be used on this exit client button. When not specified,\r the default action for the view type (desktop or mobile) will be invoked."
        }
      },
      "additionalProperties": false,
      "required": [
        "isEnabled"
      ],
      "description": "Configuration for an Exit Client button. This is normally used in the Session Stats view,\r but in theory we can add this standard button config in other places."
    },
    "SimpleButtonConfig": {
      "type": "object",
      "additionalProperties": false,
      "properties": {
        "isEnabled": {
          "type": "boolean",
          "description": "Indicates if this feature is enabled or not."
        }
      },
      "required": [
        "isEnabled"
      ],
      "description": "The most basic form of configuration that can exist for a button."
    },
    "ForFunAdvertConfig": {
      "type": "object",
      "properties": {
        "isEnabled": {
          "type": "boolean",
          "description": "Indicates if this feature is enabled or not."
        },
        "numGamesBetweenAdvert": {
          "type": "number",
          "description": "Indicates how many games should be pass between each advert, before showing the For Fun\r Advert. If not specified, then a default value will be used."
        },
        "link": {
          "$ref": "#/definitions/OpenLinkAction",
          "description": "An external url, that the ForFun advert can link to. This will generally be used to prompy\r the player to click the link, and to play for real."
        }
      },
      "additionalProperties": false,
      "required": [
        "isEnabled"
      ],
      "description": "Configuration for the For Fun advert. This is an advert that is shown between games, on a ForFun\r (\"Freeplay only\") Game Client deployment."
    },
    "OpenLinkAction": {
      "anyOf": [
        {
          "$ref": "#/definitions/OpenUrlLinkAction"
        },
        {
          "$ref": "#/definitions/OpenUrlParamLinkAction"
        }
      ]
    },
    "OpenUrlLinkAction": {
      "type": "object",
      "properties": {
        "type": {
          "type": "string",
          "const": "url"
        },
        "url": {
          "type": "string",
          "description": "The url that needs to be opened."
        }
      },
      "required": [
        "type",
        "url"
      ],
      "additionalProperties": false,
      "description": "Opens a link in a popup (on desktop) or external tab (on mobile). The link opened is a hard-coded\r value."
    },
    "OpenUrlParamLinkAction": {
      "type": "object",
      "properties": {
        "type": {
          "type": "string",
          "const": "urlParam"
        },
        "urlParam": {
          "type": "string",
          "description": "The url parameter containing a link to open."
        }
      },
      "required": [
        "type",
        "urlParam"
      ],
      "additionalProperties": false,
      "description": "Opens a link, whose url is specified by a url parameter."
    },
    "DesktopGuiConfig": {
      "type": "object",
      "properties": {
        "topLeftInfoSlot": {
          "$ref": "#/definitions/DesktopGuiInfoSlotConfig",
          "description": "Configuration for the Top Left info slot on the GUI (shown in the top row of the desktop GUI,\r and sandwiched between the Open Menu button on the left, and the Game Logo in the middle).\r This config can specify that nothing is to be shown here, or it can specify the component to\r be used (and any other additional data), for example:\r - responsible gaming links"
        },
        "topRightInfoSlot": {
          "$ref": "#/definitions/DesktopGuiInfoSlotConfig",
          "description": "Configuration for the Top Left info slot on the GUI (shown in the top row of the desktop GUI,\r and sandwiched between the Open Menu button on the left, and the Game Logo in the middle).\r This config can specify that nothing is to be shown here, or it can specify the component to\r be used (and any other additional data), for example:\r - responsible gaming links"
        },
        "openMenuButton": {
          "$ref": "#/definitions/SimpleButtonConfig",
          "description": "Configuration for the Open Menu button, in the top left of the screen."
        },
        "closeSessionButton": {
          "$ref": "#/definitions/CloseSessionButtonConfig",
          "description": "Configuration for the Close Session button, in the top right of the screen."
        },
        "addCreditButton": {
          "$ref": "#/definitions/SimpleButtonConfig",
          "description": "Configuration for the Add Credit button."
        },
        "fullScreenButton": {
          "$ref": "#/definitions/SimpleButtonConfig",
          "description": "Configures the Full screen button."
        },
        "toggleSoundButton": {
          "$ref": "#/definitions/SimpleButtonConfig",
          "description": "Configuration for the Toggle Sound Button."
        },
        "socialMediaButtons": {
          "$ref": "#/definitions/SocialMediaButtonsConfig",
          "description": "Configuration for the Social Media Buttons on the right of the screen."
        },
        "autoPlayMenuConfig": {
          "$ref": "#/definitions/AutoPlayMenuConfig",
          "description": "Configuration for the auto play menu."
        },
        "menuViewTabsConfig": {
          "$ref": "#/definitions/MenuViewTabsConfig",
          "description": "Configuration for the menu view tabs."
        },
        "freeGamesNotificationButton" : {
          "$ref": "#/definitions/FreeGamesNotificationButtonConfig",
          "description": "Configuration for the free games notification button."
        }
      },
      "required": [
        "topLeftInfoSlot",
        "topRightInfoSlot",
        "openMenuButton",
        "closeSessionButton",
        "addCreditButton",
        "fullScreenButton",
        "toggleSoundButton",
        "socialMediaButtons",
        "autoPlayMenuConfig",
        "menuViewTabsConfig",
        "freeGamesNotificationButton"
      ],
      "additionalProperties": false,
      "description": "Features available for the Desktop Gui."
    },
    "DesktopGuiInfoSlotConfig": {
      "anyOf": [
        {
          "$ref": "#/definitions/EmptyDesktopGuiInfoSlotConfig"
        },
        {
          "$ref": "#/definitions/ResponsibleGamingLinksInfoSlotConfig"
        },
        {
          "$ref": "#/definitions/SessionInfoSlotConfig"
        }
      ],
      "description": "Specifies a component to be drawn in one of the \"info slots\" on desktop GUI. There are two\r info slots currently, one to the left / and one to the right of the game logo."
    },
    "EmptyDesktopGuiInfoSlotConfig": {
      "type": "object",
      "properties": {
        "type": {
          "type": "string",
          "const": "none"
        }
      },
      "required": [
        "type"
      ],
      "additionalProperties": false,
      "description": "Specifies an empty info slot - nothing shown."
    },
    "ResponsibleGamingLinksInfoSlotConfig": {
      "type": "object",
      "properties": {
        "type": {
          "type": "string",
          "const": "responsibleGamingLinks"
        }
      },
      "required": [
        "type"
      ],
      "additionalProperties": false,
      "description": "Configures a Reponsible Gaming Links component in the info slot."
    },
    "SessionInfoSlotConfig": {
      "type": "object",
      "properties": {
        "type": {
          "type": "string",
          "const": "sessionInfo"
        },
        "openLink": {
          "$ref": "#/definitions/OpenLinkAction",
          "description": "An optional Open Link action. If this setting is enabled, then when the Session Info\r view is pressed, the external link will be opened. If this option is set, then it serves\r as the default mechanism that will be used in all possible cases (mobile, desktop, testing,\r production). It can be overriden for these cases by specifying more explicit options."
        }
      },
      "required": [
        "type"
      ],
      "additionalProperties": false,
      "description": "Configures the Session Info \"Info Slot\" component."
    },
    "CloseSessionButtonConfig": {
      "anyOf": [
        {
          "$ref": "#/definitions/DefaultCloseSessionButtonConfig"
        },
        {
          "$ref": "#/definitions/ExitClientCloseSessionButtonConfig"
        }
      ],
      "description": "Configuration for the Close Session button in the main Gui."
    },
    "DefaultCloseSessionButtonConfig": {
      "type": "object",
      "properties": {
        "isEnabled": {
          "type": "boolean",
          "description": "Indicates if this feature is enabled or not."
        },
        "action": {
          "type": "string",
          "const": "closeSession",
          "description": "The type of action that should be invoked when the button is pressed."
        }
      },
      "required": [
        "action",
        "isEnabled"
      ],
      "additionalProperties": false,
      "description": "Configures a Close Session Button for the GUI, which will send the Close Session request to the\r Game Engine Server, and then show the Session Stats view."
    },
    "ExitClientCloseSessionButtonConfig": {
      "type": "object",
      "properties": {
        "isEnabled": {
          "type": "boolean",
          "description": "Indicates if this feature is enabled or not."
        },
        "action": {
          "type": "string",
          "const": "exitClient",
          "description": "The type of action that should be invoked when the button is pressed."
        },
        "customAction": {
          "$ref": "#/definitions/ExitClientAction",
          "description": "An optional, customized configuration for exiting the game client when the Close Session\r button's action is configured as exitClient. When not specified, the default exit client\r action for the view type (desktop or mobile) will be invoked."
        }
      },
      "required": [
        "action",
        "isEnabled"
      ],
      "additionalProperties": false
    },
    "SocialMediaButtonsConfig": {
      "type": "object",
      "properties": {
        "isEnabled": {
          "type": "boolean",
          "description": "Indicates if this feature is enabled or not."
        },
        "url_facebook": {
          "type": "string",
          "description": "Url to use for a \"share on facebook\" button. This is the url that will be shown to the\r player's friends, if they click the facebook button. If the url is absent, the facebook\r button should not be shown."
        },
        "url_twitter": {
          "type": "string",
          "description": "Url to use for a \"share on twitter\" button. This is the url that will be shown to the\r player's friends, if they click the twitter button. If the url is absent, the twitter\r button should not be shown."
        }
      },
      "additionalProperties": false,
      "required": [
        "isEnabled"
      ],
      "description": "Configuration for the pair of Social Media buttons that can be shown on the Desktop GUI."
    },
    "MobileGuiConfig": {
      "type": "object",
      "properties": {
        "openMenuButton": {
          "$ref": "#/definitions/SimpleButtonConfig",
          "description": "Configuration for the Open Menu button."
        },
        "closeSessionButton": {
          "$ref": "#/definitions/CloseSessionButtonConfig",
          "description": "Configuration for the Close Session button."
        },
        "autoPlayMenuConfig": {
          "$ref": "#/definitions/AutoPlayMenuConfig",
          "description": "Configuration for the auto play menu."
        },
        "menuViewTabsConfig": {
          "$ref": "#/definitions/MenuViewTabsConfig",
          "description": "Configuration for the menu view tabs."
        },
        "freeGamesNotificationButton" : {
          "$ref": "#/definitions/FreeGamesNotificationButtonConfig",
          "description": "Configuration for the free games notification button."
        }
      },
      "required": [
        "openMenuButton",
        "closeSessionButton",
        "autoPlayMenuConfig",
        "menuViewTabsConfig",
        "freeGamesNotificationButton"
      ],
      "additionalProperties": false,
      "description": "Features available for the Mobile Gui."
    },
    "ResponsibleGamingLinkConfig": {
      "type": "object",
      "properties": {
        "id": {
          "type": "string",
          "description": "The id associated with the responsible gaming link. This is presented for\r debug purposes."
        },
        "buttonTexture": {
          "type": "string",
          "description": "Id of the texture that should be shown for the button."
        },
        "action": {
          "$ref": "#/definitions/OpenLinkAction",
          "description": "An optional Open Link action that should be invoked when the responsible gaming button is\r pressed. If no action is specified, then no action should be invoked, and the responsible\r gaming \"link\" is simply a graphic present in the set of responsible gaming icons."
        }
      },
      "required": [
        "id",
        "buttonTexture"
      ],
      "additionalProperties": false,
      "description": "Config for a single Responsible Gaming Link."
    },
    "GameplayMode": {
      "type": "string",
      "enum": [
        "free",
        "real"
      ],
      "description": "Indicates a Session Type"
    },
    "AutoPlayMenuConfig": {
      "type": "object",
      "properties": {
        "LossLimitTextBoxes": {
          "$ref": "#/definitions/LossLimitTextBoxes",
          "description": "Configuration for the loss limit text box in the auto play menu. This configuration is optional. This text box will be shown by default."
        },
        "WinningsLimitTextBox" : {
          "$ref": "#/definitions/WinningsLimitTextBox",
          "description": "Configuration for the winnings limit text box in the auto play menu. This configuration is optional. This text box will be shown by default."
        }
      } 
    },
    "LossLimitTextBoxes":{
      "properties": {
        "isEnabled": {
          "type": "boolean",
          "description": "Indicates if the loss limit text box is enabled in the autoplay menu."
        }
      }
    },
    "WinningsLimitTextBox" : {
      "properties": {
        "isEnabled": {
          "type": "boolean",
          "description": "Indicates if the winnings limit text box is enabled in the autoplay menu."
        }
      }
    },
    "QuickLoadConfig": {
      "type": "object",
      "properties": {
        "isEnabled": {
          "type": "boolean",
          "description": "Indicates if the quick load feature is enabled. If this feature is enabled then the player will be taken straight to the cashier view or the game in idle state without having to click a button."
        }
      },
      "required": [
        "isEnabled"
      ],
      "additionalProperties": false,
      "description": "Config for the quick load feature."
    },
    "MenuViewTabsConfig" : {
      "type": "object",
      "properties": {
        "RulesTabConfig": {
          "$ref": "#/definitions/RulesTabConfig",
          "description": "Configuration for the rules tab in the menu view. This configuration is optional. This tab will be shown by default."
        },
        "HistoryTabConfig" : {
          "$ref": "#/definitions/HistoryTabConfig",
          "description": "Configuration for the history tab in the menu view. This configuration is optional. This tab will be shown by default."
        },
        "SettingsTabConfig" : {
          "$ref": "#/definitions/SettingsTabConfig",
          "description": "Configuration for the winnings limit text box in the auto play menu. This configuration is optional. This tab will be shown by default."
        }
      }
    },
    "RulesTabConfig": {
      "properties": {
        "isEnabled": {
          "type": "boolean",
          "description": "Indicates if the rules tab is enabled in the menu view."
        }
      }
    },
    "HistoryTabConfig": {
      "properties": {
        "isEnabled": {
          "type": "boolean",
          "description": "Indicates if the history tab is enabled in the menu view."
        }
      }
    },
    "SettingsTabConfig": {
      "properties": {
        "isEnabled": {
          "type": "boolean",
          "description": "Indicates if the settings tab is enabled in the menu view."
        }
      }
    },
    "FreeGamesNotificationButtonConfig" : {
      "type": "object",
      "LaunchSessionStatsScreenConfig" : {
        "$ref": "#/definitions/LaunchSessionStatsScreenConfig",
        "description": "Configuration to enable a function which redirects the user to the session stats view when the button is pressed on the free games notification"
      }
    },
    "LaunchSessionStatsScreenConfig" : {
      "properties": {
        "isEnabled": {
          "type": "boolean",
          "description": "Indicates if the player will be taken to the session stats view, after clicking the button on the free games notification pop up."
        }
      }
    }
  }
}