///<reference path='./types.d.ts'/>
import {BonusModel} from './BonusModel';
import { BonusState } from './BonusState';

/**
 * This proposed implementation of BonusController, actually IS very generic. We can sub-class from
 * this. I am entering into it a list of all state transitions that I think are common across games.
 * 
 * I would then ask Wiener to review it, and tell me if they think I am missing anything.
 */
export class BonusController
{
    constructor()
    {
        /**
         * Tracks the current state that the Bonus Phase is in.
         * @private
         * @type {string}
         */
        this._state = null;

        /**
         * @type {BonusState}
         */
        this.STATE = null;

        /**
         * @type {BonusModel}
         */
        this._bonusModel = null;

        this._events.add("global.error", this.handleGlobalError);
        this._events.add("session_timeout", this.handleSessionTimeout);
    }

    //--------------------------------------------------------------------------------------------------
    // TODO:
    // Need a defined set of events to handle here that relate to outside interrupts
    // eg: we may need to kill a state in progress when these outside events occur.
    //--------------------------------------------------------------------------------------------------
    /**
     * @protected
     */
    handleGlobalError() {

    }

    /**
     * @protected
     */
    handleSessionTimeout() {

    }

    /**
     * @protected
     * @param {string} id 
     */
    setState(id, exitStateAction)
    {
        this._state = id;

        this._exitStateAction = exitStateAction;

        // Publishing state change event, tells the view when to resync.
        // This is the only external action the view should be interested in,
        // and our policy should be "view aborts whatever it was doing, and
        // cleans up, whenever it hears a state changed event"
        this._events.dispatch("bonus_state_changed");
    }

    //--------------------------------------------------------------------------------------------------
    // State transition actions
    //--------------------------------------------------------------------------------------------------
    /**
     * Phase intro is any intro sequence that the BaseBonusView shows. I would NOT normally include in this
     * the "scatter win sequence" that we show in the SlotView. Also, I would not include any "transition
     * from Slot View to Bonus View" animation sequences.
     * @protected
     */
    enterStatePhaseIntro() {
        this.setState(this.STATE.PHASE_INTRO);
    }

    /**
     * @protected
     */
    exitStatePhaseIntro() {

    }

    /**
     * Round intro means that we can show indicators, such as "starting round 3 out of 20", or we can
     * move pieces into place (for example: in Fowl Play games, we have between 3 to 5 chickens that can
     * be selected in each round. We might want to play an animation moving them into place). We can do
     * it here. Round is not in progress, so no selections can take place here. We can of course have
     * this be a "dummy" state, and skip it.
     * @protected
     */
    enterStateRoundIntro() {
        this.setState(this.STATE.ROUND_INTRO);
    }

    /**
     * @protected
     */
    exitStateRoundIntro() {

    }

    /**
     * This state could also be called "Select Scatter". It is the only state that accepts user input
     * from the player (for scatter selection).
     * @protected
     */
    enterStateRoundInProgress()
    {
        let autoSelectionTimeout = this._model.isAutoplayInProgress() ? 10 : 20;

        this._autoSelectionTimer = setTimeout(() => {
            this.makeRandomSelection();
        }, autoSelectionTimeout);

        this.setState(this.STATE.ROUND_IN_PROGRESS);
    }

    /**
     * @protected
     */
    exitStateRoundInProgress() {
        clearTimeout(this._autoSelectionTimer);
    }

    /**
     * In the RoundResults state, we show all results for the selection made by the player. This can
     * include prizes, accumulated multipliers, lose lives, level ups. We also send the "Bonus Selection
     * Request" message to the Game Engine Server. The view action that we initiated (showing the round
     * results) and the request message sent to the server are both async events: we can only advance
     * out of this state, when both of those events are completed.
     * @protected
     */
    enterStateRoundResults()
    {   
        this._roundResultsAnimComplete = false;
        this._roundResultsRequestComplete = false;
        
        this._services.sendBonusSelectionRequest(() => {
            this._roundResultsRequestComplete = true;
            this.tryAndEndRoundResults();
        });

        this.setState(this.STATE.ROUND_RESULTS);
    }

    /**
     * @protected
     */
    exitStateRoundResults() {
        // TODO:
        // We could forcibly kill any event listeners on the comms action here
        // This would help us clean up in the event of interruption by outside
        // forces, eg: fatal error, session timeout.

        this._roundResultsAnimComplete = false;
        this._roundResultsRequestComplete = false;
    }

    /**
     * We add a dedicated state to indicate that a "level up" animated transition is taking place.
     * @protected
     */
    enterStateLevelUp() {
        this.setState(this.STATE.START_NEW_LEVEL);
    }

    exitStateLevelUp() {

    };

    /**
     * Phase Results really means "Total Winnings Sequence". During this state, we ask the view to show
     * the Total Winnings animation, and we also send a "Bonus Phase Complete" request to the server.
     * Once this 2 asynchronous actions are completed, we may start to end the bonus state.
     * @protected
     */
    enterStatePhaseResults()
    {
        this._phaseResultsAnimComplete = false;
        this._phaseResultsRequestComplete = false;

        this._services.sendBonusPhaseCompleteRequest(() => {
            this._phaseResultsRequestComplete = true;
            this.tryAndEndPhaseResults();
        });

        this.setState(this.STATE.PHASE_RESULTS);
    }

    /**
     * @protected
     */
    exitStatePhaseResults() {
        this._phaseResultsAnimComplete = false;
        this._phaseResultsRequestComplete = false;
    }

    /**
     * Show Phase Outro to the player. Again, for some games, this can be a dummy state where nothing
     * happens (and is over immediately), but in others, we may show a final outro sequence / play a
     * final outro tune.
     * @protected
     */
    enterStatePhaseOutro() {
        this.setState(this.STATE.PHASE_OUTRO);
    }

    /**
     * @protected
     */
    exitStatePhaseOutro() {

    }

    //--------------------------------------------------------------------------------------------------
    // Public API (the part accessed by BaseBonusView)
    // We can either
    // a) Give BaseBonusView access to controller, or
    // b) Have BaseBonusView talk to controller through events
    // 
    // In both cases, I propose NOT to give Controller access to view. Instead, the BaseBonusView
    // simply has to sync to the state changes instigated by BonusController. BaseBonusView is going
    // to get ALL data it needs from the model directly - I don't like the idea of passing data
    // to BaseBonusView through method calls (because those method calls quickly become huge and hard
    // to read).
    //
    // However, if Wiener can suggest a good reason to have BaseBonusView directly owned by BonusController
    // I am happy to listen (I would still insist that almost no data is passed to BaseBonusView through
    // any public BaseBonusView API, however)
    //--------------------------------------------------------------------------------------------------
    /**
     * Action to be invoked by the view, when a Phase Intro sequence is completed.
     * @public
     */
    actionOnPhaseIntroComplete() {
        if (this._state === this.STATE.PHASE_INTRO) {
            this.exitStatePhaseIntro();
            if (this._bonusModel.getNumRoundsRemaining() > 0) {
                this.enterStatePhaseResults();
            }
            else {
                this.enterStateRoundIntro();
            }
        }
    }

    /**
     * Action to be invoked by the view, when a Round Intro sequence is completed.
     * @public
     */
    actionOnRoundIntroComplete() {
        if (this._state === this.STATE.ROUND_INTRO) {
            this.exitStateRoundIntro();
            this.enterStateRoundInProgress();
        }
    }

    /**
     * Makes a selection.
     * @public
     * @param {number} selectionId
     * The id of the selection to make.
     */
    makeSelection(selectionId) {
        if (this._state === this.STATE.ROUND_IN_PROGRESS) {
            this.exitStateRoundInProgress();
            this._bonusModel.makeSelection(selectionId);
            this.enterStateRoundResults();
        }
    }

    /**
     * Makes a random selection. This will get called by BonusController automatically
     * after a certain time has elapsed in the "RoundInProgress" state. This could also
     * be called by BaseBonusView (we may allow a "random selection" action to the player,
     * even if there is no on-screen button to do it, on a game like Big Ghoulies where
     * the player has 20 windows they could select, pressing the 1 to 5 keys on the
     * keyboard on desktop can route to "makeRandomSelection").
     * @public
     */
    makeRandomSelection() {
        if (this._state === this.STATE.ROUND_IN_PROGRESS) {
            this.exitStateRoundInProgress();
            this._bonusModel.makeRandomSelection();
            this.enterStateRoundResults();
        }
    }

    /**
     * Action to be invoked by the view, when a Round Results sequence is completed
     * (ie: all individual results for the round shown, any winnings for the round
     * are shown).
     * @public
     */
    actionOnRoundResultsComplete() {
        if (this._state === this.STATE.ROUND_RESULTS) {
            this._roundResultsAnimComplete = true;
            this.tryAndEndRoundResults();
        }
    }

    /**
     * @private
     */
    tryAndEndRoundResults () {
        if (this._state ===  this.STATE.PHASE_RESULTS &&
            this._roundResultsAnimComplete && this._roundResultsRequestComplete) {
            this.exitStateRoundResults();
            if (this._bonusModel.getNumRoundsRemaining() > 0) {
                if (this._bonusModel.getLastSelection().roundResult.isLevelUp) {
                    this.enterStateLevelUp();
                }
                else {
                    this.enterStateRoundIntro();
                }
            }
            else {
                this.enterStatePhaseResults();
            }
        }
    }

    /**
     * Action to be invoked by the view, when a Level Up animation sequence has been completed.
     * @public
     */
    actionOnLevelUpComplete() {
        if (this._state === this.STATE.START_NEW_LEVEL) {
            this.enterStateRoundIntro();
        }
    }

    /**
     * Action to be invoked by the view, when a Phase Results (total winnings & multiplier)
     * animation sequence is completed.
     * @public
     */
    actionOnPhaseResultsComplete() {
        if (this._state === this.STATE.PHASE_RESULTS) {
            this._phaseResultsAnimComplete = true;
            this.tryAndEndPhaseResults();
        }
    }

    /**
     * @private
     */
    tryAndEndPhaseResults () {
        if (this._state === this.STATE.PHASE_RESULTS &&
            this._phaseResultsAnimComplete && this._phaseResultsAnimComplete) {
                this.exitStatePhaseResults();
                this.enterStatePhaseOutro();
            }
    }

    /**
     * Action to be invoked by the view, when a Phase Outro animation sequence is completed.
     * @public
     */
    actionOnPhaseOutroComplete() {
        this.setState(null, null);
    }
}