buil///<reference path='./types.d.ts'/>

// Here is the key for Luck Aliens ResultTypes
// 0 = LoseLife
// 1 = Prize
// 2 = FakeMultiplier (mothership that will not get destroyed)
// 3 = RealMultiplier (mothership that WILL be destroyed)

// The value in "values" for each index can be parsed differently,
// according to the "result" enum. eg: when result === 1 (prize),
// then "value" is the value of that prize. When result === 2
// (fake multiplier) or 3 (real multiplier), then the "value" means
// the multiplier value to show

// Here is an example Bonus Message for LuckyAliens
export const ExampleLuckyAliensBonusMsg =
{
    "results" : "1,   0, 1,   1,   0, 1,    0, 1,   2, 1,   0",

    "values" :  "400, 0, 200, 300, 0, 1000, 0, 200, 4, 200, 0",

    "numRoundsPerLevel" : "4, 4, 1, 2",

    "numLivesAwarded" : "4",

    "multiplierAwarded" : "0",

    "totalWinnings" : "2300",

    "isReloaded" : "true",

    "selectionsMade" : "1, 2, 4, 3, 7"
};


/**
 * This is somewhere between our existing "BonusPhaseModel" approach, and the old "BonusController"
 * approach. Now, either:
 * a) We can allow the view to access a model, or
 * b) We provide all fields here.
 * @todo:
 * Decide if to call it a "ViewModel".
 */
export class BonusModel
{
    /**
     * 
     * @param {*} resultsMsg 
     * Bonus Results Message returned from the Game Engine Server.
     */
    constructor(resultsMsg)
    {
        /**
         * Indicates if this is a reloaded bonus phase.
         * @private
         * @type {boolean}
         */
        this._isReloaded = false;

        /**
         * Wallet value at the start of the bonus phase.
         * @private
         * @type {number}
         */
        this._startWallet = 0;

        /**
         * Total amount won in the bonus phase (in points).
         * @private
         * @type {number}
         */
        this._totalWinnings = 0;

        /**
         * The total number of lives awarded for the bonus phase.
         * @private
         * @type {number}
         */
        this._numLivesAwarded = 0;

        /**
         * The multiplier level awarded. This should be the actual value returned by the
         * Game Engine Server.
         * @private
         * @type {number}
         */
        this._multiplierAwarded = 0;

        /**
         * The list of all RoundResults for this Bonus Phase.
         * @type {BonusRoundResult[]}
         * @private
         */
        this._roundResults = [];

        /**
         * The list of all selections so far made in the Bonus Phase. We should be sending a
         * single selection value for each round. The length of this array, should therefore
         * be identical to the value of "currentRoundIndex".
         * @private
         * @type {number[]}
         */
        this._selectionsMade = [];

        /**
         * Tracks the index of the round currently in progress. If all rounds are completed,
         * then this must be set to the index of the final round of the bonus phase.
         * @private
         * @type {number}
         */
        this._currentRoundIndex = 0;

        /**
         * Records the total number of levels that will be played in the Bonus Phase.
         * @private
         * @type {number}
         */
        this._numLevelsTotal = 0;

        /**
         * Instance of a BonusResultsType object: we expect a different implementation of this
         * object for each Game Client. It includes constants that relate to the ResultType
         * enums that the server will return for the current game's bonus phase: however,
         * Bonus Model actually uses its public methods (such as "isLoseLife(resultEnum)") in
         * order to check if a given ResultType enum matches a specific key. They key it matches
         * dictates how the "value" associated with that ResultType is interpreted.
         * 
         * EXAMPLES:
         * 
         * PRIZES
         * We might reserve "1,2,3,4" as prize ResultTypes: in this case, the associated value
         * means the prize won for each, in points. The GameClient's BaseBonusView would then show
         * different graphics behind the prize (eg: 1 is white egg, 2 is silver egg, 3 is gold
         * egg, 4 is diamond egg). If our specific GameClient only needs to show a single type
         * of prize graphic (eg: probably just show a winning value), then we would only need
         * to reserve ResultType "1" to indicate a prize.
         * 
         * LOSE LIFE
         * We could simply reserve "0" as our enum for lose life, if we only wanted to show lose
         * life in a single way. If we wanted to show 3 types of lose life graphic (eg: a bomb,
         * a bear trap, a piano falling on the character's head), then we could reserve Result
         * Type enums "0,1,2" to indicate the 3 different levels of lose life. In this way,
         * this.BONUS_RESULTS.isLoseLife(resultType) would check against all 3 lose life enum
         * types. The "value" associated with the lose life field, would tell us how many lives
         * to deduct.
         * @private
         * @type {BonusResultType}
         */
        this.BONUS_RESULTS = null;
    }

    /**
     * Parses the Bonus Results Message. In theory, this functionality will work for all new
     * games (the Legacy games work a bit differently).
     * @param {*} msg 
     */
    parseBonusResultsMsg(msg) {
        // Here, we can place a really common implementation - we are basically going to use
        // this for all new games going forward.

        this._startWallet = msg.extractNumber("wallet");
        this._isReloaded = msg.extractBoolean("isReloaded");
        this._selectionsMade = msg.extractNumberArray("selectionsMade");
        this._numLivesAwarded = msg.extractNumber("numLivesAwarded");
        this._multiplierAwarded = msg.extractNumber("multiplierAwarded");
        this._totalWinnings = msg.extractNumber("totalWinnings");

        // "results" and "values" field are directly equivalent to the "results" and "prizes"
        // fields in Big Ghoulies. Internally, I have renamed "prizes" to "values". This is
        // because now, we will treat each value in "values" differently according to what the
        // enum was in "results".
        let results = msg.extractNumberArray("results");
        let values = msg.extractNumberArray("values");
        let numResults = results.length;

        let numResultsPerRound, numRoundsPerLevel;
        if (msg.hasField("numResultsPerRound")) {
            numResultsPerRound = msg.extractNumberArray("numResultsPerRound");
        }

        if (msg.hasField("numRoundsPerLevel")) {
            numRoundsPerLevel = msg.extractNumberArray("numRoundsPerLevel");
        }

        this._numLevelsTotal = numRoundsPerLevel ? numRoundsPerLevel.length : 0;

        // Now begins our parsing logic: it looks a bit convoluted, but all it is doing
        // is iterating over all results, deciding when they need grouping into a single
        // round (this is only done when the "numResultsPerRound" field was sent by the
        // server), and then deciding where the level-up divisions are (if the numRounds
        // perLevel field is present this takes precedence, otherwise we look for level
        // up results in the actual results returned).
        let numResultsInCurrentRound = 0;
        let numRoundsInCurrentLevel = 0;
        let currNumResultPerRoundIndex = 0;
        let currNumRoundPerLevelIndex = 0;
        let groupResultsPerRound = numResultsPerRound;
        let useSingleResultPerRound = !groupResultsPerRound;
        let createNewRoundResult = true;

        for (let i = 0; i < results.length; i ++)
        {
            let roundIsComplete = useSingleResultPerRound;
            
            if (createNewRoundResult)
            {
                createNewRoundResult = false;

                this._roundResults.push({
                    winnings : 0,
                    isLevelUp : false,
                    multiplierCollected : 0,
                    numLivesLost : 0,
                    numLevelUp : 0,
                    results : !useSingleResultPerRound ? [] : null
                });
            }
            
            let roundResult = this._roundResults[this._roundResults.length - 1];
            let results = results[i];
            let value = values[i];
            
            roundResult.numLevelUp += this.BONUS_RESULTS.isLevelUp(result) ? value : 0;
            roundResult.winnings += this.BONUS_RESULTS.isPrize(result) ? value : 0;
            roundResult.multiplierCollected += this.BONUS_RESULTS.isRealMultiplier(result) ? value : 0;
            roundResult.numLivesLost += this.BONUS_RESULTS.isLoseLife(result) ? value : 0;
            
            // If we need to group multiple results into a single round
            if (groupResultsPerRound) {
                roundResult.results.push({result,value});
                
                // We have assembled enough results to group them
                if (roundResult.results.length === numResultsPerRound[currNumResultPerRoundIndex]) {
                    currNumResultPerRoundIndex ++;
                    roundIsComplete = true;
                }
            }

            if (roundIsComplete) {
                createNewRoundResult = true;
                if (numRoundsPerLevel && numRoundsInCurrentLevel === numRoundsPerLevel[currNumRoundPerLevelIndex]) {
                    roundResult.isLevelUp = true;
                    numRoundsInCurrentLevel = 0;
                    currNumRoundPerLevelIndex ++;
                }
            }
        }
        
        // GAME RESTORATION
        // All we need to know is how many selections have already been made. This
        // tells us what our current round index is. We then know how far we have
        // advanced into the local field _roundResults. All public state that is
        // available on the BonusModel, works on the basis of checking current
        // round and then calculating the values to show.
        if (this._isReloaded) {
            this._currentRoundIndex = this._selectionsMade.length;
        } else {
            this._currentRoundIndex = 0;
        }
    };

    //--------------------------------------------------------------------------------------------------
    // Public actions, callable by view.
    //--------------------------------------------------------------------------------------------------
    /**
     * Makes a selection for the current round.
     * @public
     * @param {number} id
     * The id of the selection to make for the current round.
     */
    makeSelection(id)
    {
        this._selectionsMade.push(id);
        this._currentRoundIndex ++;
    };

    /**
     * Makes a selection for the current round, using a random selection id.
     * @public
     */
    makeRandomSelection()
    {
        let selectionId = this.getRandomSelectionId();
        this.makeSelection(selectionId);
    };

    /**
     * Finds and returns a valid random selection id.
     * @todo: Determine what should happen if a valid selection id is not available.
     * @public
     * @return {number}
     */
    getRandomSelectionId()
    {
        let availableSelectionIds = this.getAvailableSelectionIds();
        let randomIndex = RNG.getRandom(availableSelectionIds.length);
        let randomSelection = availableSelectionIds[randomIndex];
        return randomSelection;
    };

    /**
     * Checks if a given selection id is available at the moment. "Available" means that the
     * selection id has not already been used. This implicitly validates the id of the selection,
     * returning false if it is not a valid selection id for the game client.
     * @public
     * @param {number} id 
     * @return {boolean}
     */
    isSelectionIdAvailable(id)
    {
        let availableIds = this.getAvailableSelectionIds();
        let isAvailable = availableIds.indexOf(id) !== -1;
        return isAvailable;
    };

    /**
     * Returns the list of selection ids available for the current round. This will be a
     * truncated list, containing only acceptable selection id values.
     * EXAMPLES:
     * 1) If we have a bonus phase like Haunted House / Fowl Play London / Montezumas
     *    Treasure, with 5 selections available per level (values: 1 to 5), and we have
     *    already selected 2 * 3 on the current level, then this will return [1,4,5].
     * 2) If we have a bonus phase with one large grid of selectable "windows" like Big
     *    Ghoulies (and all windows are selectable in any round, assuming they are not
     *    already selected) then this method would return ALL selection ids, minus the
     *    ones already selected.
     * 3) If every round in the bonus phase makes all selections available (like Knights
     *    Treasure) and we never exclude previously selected items, then this method will
     *    return the list of all selection ids possible in the bonus phase.
     * 
     * NOTE:
     * I think this is the important method to override, if you need to change the way
     * that selection id functionality behaves. The other local methods "makeRandomSelection",
     * "getRandomSelectionId", "isSelectionIdAvailable" all rely in this method - and all
     * should work for any arbitrary game client, if this method is sensibly overridden.
     * @public
     * @return {number[]}
     */
    getAvailableSelectionIds()
    {
        return [];
    };

    /**
     * Returns the list of all possible selection ids for the Bonus Phase. Previously selected
     * items would not be excluded. This method is proposed more as a book-keeping utility.
     * Some implementations of "getAvailableSelectionIds" may make use of this.
     * @public
     * @return {number[]}
     */
    getAllSelectionIds()
    {
        return [];
    };

    /**
     * Indicates if this is a reloaded bonus phase.
     * @public
     * @return {boolean}
     */
    isReloaded()
    {
        return this._isReloaded;
    };

    /**
     * Returns the total amount that will be won (in points) for the whole of the Bonus
     * Phase. This includes all multipliers being applied. If the player has not completed
     * all rounds yet, then they will not yet have collected this total amount. To access
     * this value, please see the "getWinningsCurrent" field.
     * @public
     * @return {number}
     */
    getWinningsTotal()
    {
        return this._totalWinnings;
    };

    /**
     * Returns the total winnings from the bonus phase (in points, including multipliers)
     * rendered as a currency string.
     * @public
     * @return {string}
     */
    getWinningsTotalString()
    {
        return this._model.getCurrencyString(this.getWinningsTotal());
    };

    /**
     * Returns the amount won so far in the Bonus Phase (in points).
     * @public
     * @return {number}
     */
    getWinningsCurrent()
    {
        let winnings = 0;
        for (let i = 0; i < this._currentRoundIndex; i ++) {
            winnings += this._roundResults[i].winnings;
        }
        return winnings;
    };

    /**
     * Returns the amount won so far, as a currency string. This is the value which should be
     * shown on any field in the Bonus Phase (it is already formatted correctly as a string).
     * @public
     * @return {string}
     */
    getWinningsCurrentString()
    {
        return this._model.getCurrencyString(this.getWinnings());
    };

    /**
     * Returns the id of the current round in progress. If all rounds are completed, this will return
     * the id of the very last round of the Bonus Phase.
     * @public
     * @return {number}
     */
    getCurrentRoundId()
    {
        let roundId = this._currentRoundIndex + 1;
        let totalRounds = this.getNumRoundsTotal();
        if (roundId > totalRounds) {
            roundId = totalRounds;
        }
        return roundId;
    };

    /**
     * Returns the total number of rounds awarded in the Bonus Phase. A Round == a Selection must be
     * made by the player (or made automatically), and we send a Selection Request to the Game Engine
     * Server. So, "number of rounds awarded" === "number of selection requests to send to the server".
     * @public
     * @return {number}
     */
    getNumRoundsTotal()
    {
        return this._roundResults.length;
    };

    /**
     * Returns the total number of rounds already played in the Bonus Phase.
     * @public
     * @return {number}
     */
    getNumRoundsPlayed()
    {
        return this._currentRoundIndex;
    };

    /**
     * Returns the total number of rounds remaining in the Bonus Phase.
     * @public
     * @return {number}
     */
    getNumRoundsRemaining()
    {
        let numTotal = this.getNumRoundsTotal();
        let numPlayed = this.getNumRoundsPlayed();
        return numTotal - numPlayed;
    };

    /**
     * Returns the total number of Levels that will actually be played in this Bonus
     * Phase - a game client's bonus design may have 7 levels maximum, but this field
     * will return the actual number that are going to happen, based on the results msg
     * received from the server.
     * 
     * Level means "group of round results". In some GameClients, a different terminology
     * is used, eg:
     * - Haunted House - Level
     * - Fowl Play London - Room
     * - Montezumas Treasure - Room
     * - Lucky Aliens - Wave
     * @public
     * @return {number}
     */
    getLevelsTotal()
    {
        return this._numLevelsTotal;
    };

    /**
     * Returns the id of the level currently in progress.
     * @public
     * @return {number}
     */
    getLevelCurrent()
    {
        let numLevelUps = 0;
        let roundResults = this._roundResults;

        for (let i = 0; i < this._currentRoundIndex; i ++) {
            if (roundResults.isLevelUp) {
                numLevelUps += 1;
            }
        }

        let levelCurrent = numLevelUps + 1;
        let levelsTotal = this.getLevelsTotal();
        if (levelCurrent >= levelsTotal) {
            return levelsTotal;
        }
    };

    /**
     * Returns the total number of lives lost so far in the bonus phase.
     * @public
     * @return {number}
     */
    getNumLivesLost()
    {
        let numLivesLost = 0;
        let roundResults = this._roundResults;

        for (let i = 0; i < this._currentRoundIndex; i ++)
        {
            numLivesLost += roundResults[i].numLivesLost;
        };

        return numLivesLost;
    };

    /**
     * Returns the total number of lives remaining in the bonus phase.
     * @public
     * @return {number}
     */
    getNumLivesRemaining()
    {
        let numLivesTotal = this.getNumLivesTotal();
        let numLivesLost = this.getNumLivesLost();
        return numLivesTotal - numLivesLost;
    };

    /**
     * Returns the total number of lives awarded in the bonus phase.
     * @public
     * @return {number}
     */
    getNumLivesTotal()
    {
        return this._data.numLivesAwarded;
    };

    /**
     * Returns the total multiplier amount that will be awarded for this Bonus Phase.
     * NOTE: when this field returns "0", this means that no multiplier wins are
     * collected. In terms of total amount won, this is the same as the player getting
     * "1" for total multiplier, except when the value is "1" we expect the game client
     * to show a simulation of this winning being collected (and for the Total Winnings
     * sequence to hint at the existence of the multiplier wintype).
     * @public
     * @return {number}
     */
    getMultiplierTotal()
    {
        return this._multiplierAwarded;
    };

    /**
     * Returns the total amount of "multiplier" that has been collected so far,
     * from all rounds completed.
     * @public
     * @return {number}
     */
    getMultiplierCollected()
    {
        let multiplierCollected = 0;
        let roundResults = this._roundResults;
        
        for (let i = 0; i < this._currentRoundIndex; i ++)
        {
            multiplierCollected += roundResults[i].multiplierCollected;
        }

        return multiplierCollected;
    };

    /**
     * Returns the result object for the current round that is in progress. If all rounds
     * are already completed, then this returns the RoundResult for the very last round of
     * the Bonus Phase.
     * @public
     * @return {Object}
     */
    getCurrentRoundResult()
    {
        return this._roundResults[this._currentRoundIndex];
    };

    /**
     * This returns the object that we expect our view to access: its a simple union,
     * containing the id of the selection made, and the Round Result data for that
     * selection. If no selections have yet been made, this will return null.
     * @public
     * @return {BonusSelection}
     */
    getLastSelection()
    {
        if (this._selectionsMade.length > 0) {
            let selIndex = this._selectionsMade.length - 1;
            let selectionId = this._selectionsMade[selIndex];
            let roundResult = this._roundResults[selIndex];
            return {selectionId, roundResult};
        } else {
            return null;
        }
    };

    /**
     * This method returns a list of data that can be used by the view for restoration.
     * It basically returns a list of objects: each object represents a selection ID,
     * and the BonusRoundResult that was awarded. Its intended use case is for
     * 1) Restoring the Bonus Phase
     * 2) We could "re-sync" bonus view state to the model using this data as well
     *    (if we ever needed to, eg: recover from error or something like that)
     * @public
     * @return {BonusSelection[]}
     */
    getSelections()
    {
        let data = [];

        for (let i = 0; i < this._currentRoundIndex; i ++)
        {
            let selectionId = this._selectionsMade[i];
            let roundResult = this._roundResults[i];
            data.push({selectionId, roundResult});
        }

        return data;
    };

    /**
     * Returns selections made, only for the current level. In a game with no levels,
     * this would return the same information as "getSelections()" (ie: all selections
     * already made). In a game with levels, if we restored the Bonus Phase, and we
     * are on level 2 (1 selection made, and we still need to make some selections
     * on level 2), then this would return a 1 element array (all selections made on
     * level 2). If the bonus phase is already complete (no selections remaining to
     * be made), this will return the selections data of the final level of the bonus
     * phase (we CAN end up in this scenario: all selections might be completed, but
     * until the server gets a specical "Bonus Phase Complete" notification, the Server
     * would continue to tell the game client to restore the bonus phase).
     * @public
     * @return {BonusSelection[]}
     */
    getCurrentLevelSelections()
    {
        // todo:
        // There are a few implementation details I haven't decided yet,
        // but this will return only the sub-set of selections for current
        // level in progress.
    }
}