interface BonusResult
{
    type : number;

    value : number;
}

/**
 * Defines the parsed results for a single Bonus Round (selection). Because a single
 * selection may still show multiple results, the round result object presents data
 * about the sum of information from the round (as well as a field for the individual
 * results).
 */
interface BonusRoundResult
{
    /**
     * The total amount won from this round (in points).
     */
    winnings : number;

    /**
     * Indicates if we should start a new level after this round.
     */
    isLevelUp : boolean;

    /**
     * Indicates if we should advance to a new level, after this round is played.
     * NOTE:
     * Tentatively, I have placed this as "numLevelUp" to be more flexible... might
     * we one day want to indicate that we skip past multiplie "levels" after a single
     * round is played ? I will await Wiener feedback on this point.
     */
    numLevelUp : number;

    /**
     * The number of lives lost on this round.
     */
    numLivesLost : number;

    /**
     * The amount of multiplier collected for this round.
     */
    multiplierCollected : number;

    /**
     * The list of all results to show for this round (selection). For many games
     * this would contain only 1 item (ie: we only show a single result for a round),
     * but for some games (such as Fowl Play London) we show multiple results for
     * each selection (Prizes, LoseLives, LevelUps).
     * 
     * I propose to leave this field as optional : its presence indicates there are
     * multiple values to show for the round. Its absence means we simply read from
     * the information listed with the result of the BonusRoundResult object. If this
     * field is present, then the other information on the BonusRoundResult object
     * still gives us the sum total of information about the round.
     */
    results ? : BonusResult[];
}

interface BonusSelection
{
    /**
     * The id of the bonus selection : this should meaningfully tie in to some object
     * within BaseBonusView that was selected by the player. We should ALWAYS make sure
     * that this value is meaningful to a specific game client (even if it is a re-skin
     * of an existing maths module).
     */
    selectionId : number;

    /**
     * The RoundResult that was awarded with the given selection.
     */
    roundResult : BonusRoundResult;
}

/**
 * This interface, defines an object which can be used to check specific round results
 * to see if they match a certain category. We will use a dedicated class for the
 * actual implementation. Wiener use the term "ConstantsClass", which this would be
 * (the Result Types would be available as constants). However, it would also implement
 * a standard API, to check if a given result fits a generic category (this is because
 * we may want to show more than 1 prize type in a given game, so "isPrize" needs to
 * check against more than 1 possible enum value).
 */
interface BonusResultType
{
    /**
     * Checks if a specific Bonus Result Type enum is a prize.
     */
    isPrize(result : number) : boolean;

    isRealMultiplier (result : number) : boolean;

    isFakeMultiplier (result : number) : boolean;

    isLevelUp (result : number) : boolean;

    isLoseLife (result : number) : boolean;
}