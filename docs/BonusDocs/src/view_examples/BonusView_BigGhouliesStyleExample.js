///<reference path='../types.d.ts'/>
import {BonusModel} from '../BonusModel';

/**
 * Here is a rudimentary example of a BaseBonusView for a Big Ghoulies style game,
 * in particular, showing how I propose we access the model data for 
 */
export class BonusView
{
    constructor()
    {
        /**
         * @type {BonusModel}
         */
        this._model = null;

        /**
         * All window graphics, ordered by index.. each has a selection id of index+1
         * @type {Image[]}
         */
        this._windowGraphics = null;
    }

    // Some examples of updating BaseBonusView GUI....
    updateCurrentLevelGuiField()
    {
        this._currentLevelText.text = this._model.getLevelCurrent();
    }

    updateMultiplierCollectedGuiField()
    {
        this._multiplierCollectedText.text = this._model.getMultiplierCollected();
    }

    /**
     * Callback fired whenver a window graphic is pressed
     */
    onWindowPressed(windowGraphic)
    {
        let selectionId = windowGraphic.selectionId;
        this._controller.makeSelection(selectionId);

        // We do NOTHING else here, we wait for controller to indicate any state
        // changes.
    }

    // This is the method that would be called, once BonusController indicates that
    // the Bonus View State is changed to "show selection results"
    showWindowResult()
    {
        let selection = this._model.getLastSelection();
        let windowIndex = selection.selectionId - 1;
        let windowGraphic = this._windowGraphics[windowIndex];
    }

    /**
     * We would use this method for
     * 1) Restoring the Bonus phase
     * 2) If we ever need a simple re-sync (eg: recover from error) - we just step
     *    the view back to the last known state that it "should" have been in (eg:
     *    the state that the bonus model is currently in)
     */
    syncAllVisualStateToModel()
    {
        this.syncAllWindowsToModelState();
        this.syncAllGuiFieldsToModelState();
    }

    syncAllGuiFieldsToModelState()
    {
        this.updateCurrentLevelGuiField();
        this.updateMultiplierCollectedGuiField();
        // etc..
    }

    syncAllWindowsToModelState()
    {
        let selections = this._model.getSelections();

        // todo:
        // Maybe make sure all windows are reset here first

        for (let i = 0; i < selections.length; i ++)
        {
            let roundResult = selections[i].roundResult;
            let selectionIndex = selections[i].selectionId - 1;
            let windowGraphic = this._windowGraphics[i];
            this.setFinalVisualStateForWindow(windowGraphic, roundResult);
        }
    }

    /**
     * 
     * @param {*} windowGraphic 
     * @param {BonusRoundResult} roundResult 
     */
    showSelectionSequenceOnWindow(windowGraphic, roundResult)
    {
        // show the window openeing, and any sequence of results
        // that roundResult field describes,
        // then
        // this._controller.actionOnRoundResultShown();
    }

    /**
     * 
     * @param {*} windowGraphic 
     * @param {BonusRoundResult} roundResult 
     */
    setFinalVisualStateForWindow(windowGraphic, roundResult)
    {
        // Set visual state for a window, after the selection is complete.
        // Even for a window with prizes, we might not show any prize here.
        // This logic is really dependeny on how the BaseBonusView is supposed
        // to work....
    }
}