///<reference path='../types.d.ts'/>
import {BonusModel} from '../BonusModel';

// BaseBonusView example for a Fowl Play London style game.
// Shows how I propose we restore the view state, where we are only
// ever showing a sub-set of visual state at any one time ( ie: only
// showing the current room )
export class BonusView
{
    constructor()
    {
        /**
         * @type {BonusModel}
         */
        this._model = null;

        // The currently visible room graphic.
        this._currentRoomGraphic = null;
    };

    // EG:
    // Restore the state of the Bonus View
    syncAllVisualStateToModel()
    {
        // 1) Ensure that existing actions, tweens, etc, are killed

        // 2) Now, create a new room graphic (by checking against active level,
        //    you create the room in whatever level-dependent visual state you
        //    need), then restore the room graphic's visual state.
        let currentRoomId = this._model.getLevelCurrent();
        let currentRoomGraphic = this.createRoomGraphic(currentRoomId);
        let selections = this._model.getCurrentLevelSelections();
        this.syncVisualStateOfRoom(currentRoomGraphic, selections);
    };

    /**
     * Takes a room graphic, and a list of selections made for this room,
     * and syncs the visual state of this room graphic.
     * @param {*} roomGraphic 
     * @param {BonusSelection[]} selections 
     */
    syncVisualStateOfRoom(roomGraphic, selections)
    {
        for (let i = 0; i < selections.length; i ++)
        {
            let selection = selections[i];
            let roomItemIndex = selection.selectionId - 1;

            // Code this bit however you want, but you are
            // feeding it a roundResult object, which the
            // view can intepret however it needs in order
            // to show the correct visual state.
            roomGraphic.setFinalVisualStateForItem(roomItemIndex, selection.roundResult);
        }
    }

    /**
     * Meanwhile, here is the method that would be called whenever the controller
     * indicates that we are entering BonusState.ShowRoundResult
     * @param {BonusSelection} selection
     */
    showRoundResult(selection)
    {
        // NOTE:
        // This method would be implemented differently, if you actually pushed the
        // animation methods in to a "BonusRoomView" class. This example supposes that
        // all animations are just written here

        let selectionIndex = selection.selectionId - 1;
        let roundResult = selection.roundResult;
        let windowGraphic = this._currentRoomGraphic.windowGraphics[selectionIndex];
        
        let timeline = new TimelineMax();

        // In this game, we show multiple results in sequence for each selection:
        // when we select one room item, we may get some winnings, a lose life,
        // a level up... imagine also we might get a "multiplier accumulator" too
        // In my example, I make a timeline to show the results, and we will push
        // the animation for each new result, sequentially, here into the timeline.
        roundResult.results.forEach(result =>
        {
            // Set up single result being shown here
        });

        // Finally, pass back control to controller when all
        // results for this selection are shown
        timeline.call(this._controller.actionOnRoundResultsShown, null, this._controller, '+=0.5');
    }
}