//--------------------------------------------------------------------------------------------------
// For "BusinessConfig", we have 2 basic options for approach
// 1) Use extension
// 2) Use a fully separate config object for each deployment
//
// Here, in my opinion, are the advantages:
// EXTENSION
// pros
// - We can inherit options from other targets
// - so, "ItalianSpecificConfig" is a base target to any config for an italian game
// - we have common options shared for different licensees: in this case, the licensees
//   get our games from us, through an "integrator" - and we configure the games fairly
//   standardly for all targets on that integrator (this isn't 100% true, but almost)
// cons
// - because it relies on inheritance, you cannot just look at the config and guess exactly
//   what settings were enabled for a specific target (you have to think about the inheritance
//   chain)
// - more complicated to do a single deployment of game client code, with a config being
//   loaded that sets up the game for that licensee (because we would need a way at runtime
//   to select the correct extended class to load up).
//
// FULLY SEPERATE CONFIG
// pros
// - we can have a single game deployment (possibly even a single set of the project files)
//   deployed in one place, for all licensees
// - we just need the config file to get loaded when the common assets (everything else)
//   are loaded
// - with this approach, any bug fixes that are made to games get deployed once (latest
//   deployment of client), and every platform gets them straight away - we are nowhere near
//   this approach with the current generation of games.
// - you can see exactly what the client is meant to do just by looking at one file, you don't
//   need to think about the inheritance
// cons
// - you need continuous deployment of changes to game client to get the advantage of the
//   bug fixes..
// - it is more convoluted to share / inherit common settings ( because we did away with inheritance )
//--------------------------------------------------------------------------------------------------
export class BusinessConfig
{
    constructor()
    {
        /**
         * This one field is tricky.. because we need it to be different for
         * every game. And we prefix the licensee, eg:
         * "luckyaliens",
         * "vincituluckyaliens",
         * "sisalluckyaliens"
         * 
         * Consider the problem : I will need different configurations for
         * different licensees. I want them to be part of the core business
         * repository ( because they are generic ).
         * 
         * Now, I could instead make this field "client_id_prefix", which would
         * be generic for all licensees.
         */
        this.CLIENT_ID = "client_id";

        /**
         * Indicates if the "restart session when closed" feature is enabled.
         */
        this.SESSION_RESTART_ALLOWED = false;

        /**
         * Indicates if the "add credit to session" feature is enabled.
         */
        this.SESSION_ADD_CREDIT_ENABLED = false;

        /**
         * Indicates max credit that may be added to a game session.
         */
        this.SESSION_MAX_CREDIT = 100000;

        /**
         * Indicates if the "play for real money" advert should be enabled for
         * the forfun version of the game client.
         */
        this.FORFUN_ADVERT_ENABLED = false;

        /**
         * The url that needs to be opened for the ForFun version of the game client.
         * NOTE:
         * This one is tricky. Most italian licensees so far have not had this feature
         * enabled. One licensee does have it enabled, and makes us give deployments of
         * our game client for (currently) 40 different partner sites of theirs. So, we
         * we would have 40 extended variations of BusinessConfig for each of these
         * targets. The really fun part : 30 of these 40 partner sites require a different
         * url for each specific game.
         * 
         * Now, the future plan is to deploy a single client build, with a configuration
         * file loaded to get target behaviour, so this approach scales perfectly well:
         * it just means we need a lot of configuration options for certain licensees.
         */
        this.FORFUN_ADVERT_URL = "www.please_supply_correct_url.com";

        /**
         * If true, a button will be included in the menu (currently in the history tab)
         * for opening an external history page (its a html page)
         */
        this.EXTERNAL_HISTORY_ENABLED = false;

        /**
         * The url for external history.
         */
        this.EXTERNAL_HISTORY_URL = "www.please_supply_correct_url.com";

        /**
         * Id of the localization file that needs loading up. We already use a different
         * file for some licensees, to support some very minor language modifications.
         */
        this.LANGUAGE_FILE = "language_default.json";

        /**
         * The url to use for production release of this game.
         */
        this.PRODUCTION_SERVER_URL = "please_supply_correct_url";

        /**
         * Indicates a url to use for any "share on facebook" action (invoked from main GUI on desktop)
         * @type {boolean | string}
         */
        this.FACEBOOK_URL = false;

        /**
         * * Indicates a url to use for any "share on twitter" action (invoked from main GUI on desktop)
         * @type {boolean | string}
         */
        this.TWITTER_URL = false;
    }
}

//--------------------------------------------------------------------------------------------------
// Some examples. All of these are intended to be checked in to "core" modules,
// because although we will need to add more targets, the features enabled &
// disabled for different licensees
//--------------------------------------------------------------------------------------------------
export class ItalianBusinessConfig extends BusinessConfig
{
    constructor()
    {
        super();

        this.SESSION_MAX_CREDIT = 100000;
        this.SESSION_ADD_CREDIT_ENABLED = true;
    }
}

export class GanBusinessConfig extends ItalianBusinessConfig
{
    constructor()
    {
        super();

        this.SESSION_RESTART_ALLOWED = true;
        this.EXTERNAL_HISTORY_ENABLED = true;
        this.PRODUCTION_SERVER_URL = "https://downloadss.wmgaming.it:8443/rest/test/html5";
    }
}

export class SnaiBusinessConfig extends GanBusinessConfig
{
    constructor()
    {
        super();

        this.EXTERNAL_HISTORY_URL = "https://www.snaiabilita.it/accountWagers.do";
        this.LANGUAGE_FILE = "language_snai.json";
    }
}

export class SisalBusinessConfig extends GanBusinessConfig
{
    constructor()
    {
        super();

        this.EXTERNAL_HISTORY_URL = "https://slot.sisal.it/accountWagers.do";
        this.LANGUAGE_FILE = "language_sisal.json";
    }
}