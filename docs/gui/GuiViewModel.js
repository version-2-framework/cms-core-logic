/**
 * Is it a controller ? Is it a ViewModel ? Is it a ViewController ?
 * I think the important point is, the view takes a single dependency ( this class )
 * and asks it for all information ( rather than asking the model directly ). It forwards
 * its actions to here (instead of directly to a main GameController) It gives us the chance
 * to insert some extra logic here.
 */
export class GuiViewModel
{
    constructor()
    {
        this._model = null;
    }

    actionOnBtnSpin() {

    }

    /**
     * Returns the current message to show.
     */
    getMessage() {
        
    }
}

export class DesktopGuiViewModel
{

}

export class MobileGuiViewModel
{

}