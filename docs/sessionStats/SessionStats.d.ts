/**
 * The SessionStat interface, defines the data needed for rendering a single
 * statistics in the SessionStats view.
 */
interface SessionStat
{
    /**
     * The id of a text localization item to fetch for this session stat.
     */
    localizationId : string;

    /**
     * The value that should be entered into the session stat field.
     */
    value : string;
}

/**
 * Defines the configuration for a footer button.
 */
interface FooterButtonConfig
{
    /**
     * Id of the localization item - the text to show on the button
     */
    localizationId : string;

    /**
     * An optional list of modifications to make to the localization
     * (in the form of Key/Value pairs).
     */
    modifications ? : Object;

    /**
     * Action to be invoked when the button is pressed.
     */
    action : Function;
}

/**
 * Configuration to show how to render the Session Closed Field.
 */
interface SessionClosedFieldConfig
{
    /**
     * The localization id of the "session closed field".
     */
    localizationId : string;

    /**
     * An optional list of Key / Value pairs to apply as modifications.
     */
    modifications ? : Object;
}

/**
 * The public interface of SessionStatsViewModel (specifically, the part that SessionStatsView
 * is allowed to use). You could depend only on this defintion in the view code, and you would
 * get code prediction for the ViewModel (but ignoring anything that is an internal detail).
 */
interface SessionStatsViewModel
{
    /**
     * Returns the current active state of SessionStats view.
     */
    getState() : string;

    /**
     * Returns the configuration for the "Session Closed" message. This is an object,
     * that contains a LocalizationId (for the basic text item to show), and a list of
     * additional text modifications (as key/value pairs). By fetching this data from
     * SessionStatsViewModel, it means that we can quickly support additional messages
     * (which is a possible request from licensees), and that the modification is based
     * on "licensee specific business logic". This view simply has to render the information
     * (preferably in an interesting way)
     */
    getSessionClosedMessageConfig() : SessionClosedFieldConfig;

    /**
     * When session is closed, we put the "SessionStatsView" into "session_closed" state. This means
     * that we show a list of statistics about the closed game session to the player : amount won,
     * num games, etc. This method will return a list of data objects, that define what statistics to
     * show, and how. All SessionStatsView needs to do, is render a list of textfields, correctly
     * using the data returned. In these way, SessionStatsViewModel can make decisions related to
     * which statistics to show. If we ever get a licensee specific request to add a different statistic
     * here (I consider this a possibility), then no change will be required in the SessionStatsView
     * (it is a business logic change)
     */
    getSessionStats() : SessionStat[];

    /**
     * Returns a list of configuration objects, for all buttons that should be shown
     * in the footer in the "SessionClosed" state.
     */
    getFooterButtonConfigs() : FooterButtonConfig[];
}