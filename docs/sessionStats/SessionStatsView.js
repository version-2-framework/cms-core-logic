// This is included for an example

import {SessionStatsViewModel} from './SessionStatsViewModel';

export class SessionStatsView
{
    constructor()
    {
        /**
         * View doesn't have access to GameModel, only to its ViewModel ( which also serves
         * as a very simple controller ).
         * @private
         * @type {SessionStatsViewModel}
         * @todo: Name could be "_viewModel" or "_vm" if that made it clearer that it is
         * NOT the GameModel instance
         */
        this._model = null;
    }

    showSessionClosedState()
    {
        this.createSessionStatsFields();
        this.createFooterButtons();
        this.createReasonClosedField();
    }

    // Example of creating all of the session stats fields when they are requird
    createSessionStatsFields()
    {
        let stats = this._model.getSessionStats();
        let textFields = [];

        stats.forEach(stat =>
        {
            let textField = null; // Create new pixi textfield...

            // Exactly how this is done, depends on what the Language api is
            textField.text = Language.getText(
                // id of the item in language.xml / language.json
                stat.localizationId,

                // Key / value pairs for modifications : in this case,
                // for each line we will replace a single hard-coded
                // bit of text "[VALUE]" with the value returned.
                {
                    "[VALUE]" : stat.value
                }
            )
        });
    }

    // Footer buttons give the player choice of next actions. If the view gets its
    // config from the ViewModel, then WMG can support modifications fairly easily
    // (minimizing changes required in future to the view component)
    createFooterButtons()
    {
        let buttonConfigs = this._model.getFooterButtonConfigs();

        buttonConfigs.forEach(btnConfig =>
        {
            let btn = null; // create button
            btn.text = Language.getText(btnConfig.localizationId);
            btn.onInputUp = btnConfig.action;
        })
    }

    // TODO:
    // Decide how this field should be used, and if it should get it's data from the
    // ViewModel, or infer it locally ( I think probably the ViewModel )
    createReasonClosedField()
    {

    }
}