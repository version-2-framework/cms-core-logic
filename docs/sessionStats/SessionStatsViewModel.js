///<reference path='./SessionStats.d.ts'/>
import {SessionStatsViewState} from './SessionStatsViewState';
import {BusinessConfig} from '../BusinessConfig';

/**
 * In this example, I have used "ViewModel" instead of "Controller" / "Model".
 * ViewModel combines these 2 concepts. The SessionStatsView will not have
 * direct access to the main model - instead, SessionStatsViewModel exposes
 * getters for any data that is required. This allows us to implement some
 * additional logic that is very specific to this view here.
 * 
 * The ViewModel also exposes "actions" for the view to invoke, and has a "state",
 * just like BonusController did. ViewModel will decide internally when to change
 * state, and dispatch an event. The SessionStatsView simply has to listen for the
 * state change event - these are the main points at which it will show different
 * sequences.
 */
export class SessionStatsViewModel
{
    constructor()
    {
        /**
         * Current state of SessionStatsView.
         * @private
         */
        this._state = null;

        /**
         * @private
         * @type {SessionStatsViewState}
         */
        this.STATES = null;

        /**
         * This is the global model instance.
         * @private
         */
        this._model = null;

        /**
         * Our "business logic" configuration object
         * @type {BusinessConfig}
         */
        this._config = null;

        this._events = null;

        this._services = null;

        // Initial start up logic (this assumes view is fully created)
        if (this._model.isSessionOpen()) {
            this.enterStateClosingSession();
        } else {
            this.enterStateSessionClosed();
        }
    }

    /**
     * Returns the state of the Session Stats View.
     * @public
     * @return {string}
     */
    getState() {
        return this._state;
    }

    //--------------------------------------------------------------------------------------------------
    // Here, we have state based actions. ViewModel combines a Controller and a domain specific Model.
    //--------------------------------------------------------------------------------------------------
    /**
     * Enter the session closing state. We expect the view to bind to this visual state.
     * ViewModel will send the Close Session request to the server, and enter into
     * "Session Closed" state when it is complete. If we wanted to add a little animation
     * in here in the view, we could adopt the standard pattern of having the view inform
     * ViewModel when that animation is complete (so ViewModel waits until animation & request
     * are done, before executing a state transition)
     * @private
     */
    enterStateClosingSession() {
        this._state = this.STATES.CLOSING_SESSION;
        this._events.dispatch("state_changed");
        this._services.sendCloseSessionRequest(() => {
            this.enterStateSessionClosed();
        });
    }

    /**
     * Enters the "session closed" state. View should bind to this, and show the appropriate
     * visual state.
     * @private
     */
    enterStateSessionClosed() {
        this._state = this.STATES.SESSION_CLOSED;
        this._events.dispatch("state_changed");
    }

    //--------------------------------------------------------------------------------------------------
    // 
    //--------------------------------------------------------------------------------------------------
    /**
     * When session is closed, we put the "SessionStatsView" into "session_closed" state. This means
     * that we show a list of statistics about the closed game session to the player : amount won,
     * num games, etc. This method will return a list of data objects, that define what statistics to
     * show, and how. All SessionStatsView needs to do, is render a list of textfields, correctly
     * using the data returned. In these way, SessionStatsViewModel can make decisions related to
     * which statistics to show. If we ever get a licensee specific request to add a different statistic
     * here (I consider this a possibility), then no change will be required in the SessionStatsView
     * (it is a business logic change)
     * @public
     * @return {SessionStat[]}
     */
    getSessionStats() {
        // Below, I show some example statistics to show how the data
        // would be returned. If we need to change the statistics for
        // a licensee, we add the change to the Config for that licensee.

        let stats = [];

        // Shows the total amount they won during the session
        stats.push({
            localizationId : "sessionStat_totalWinnings",
            value : this._model.getCurrencyString(this._model.getSessionWinnings())
        });

        // Total amount player spent
        stats.push({
            localizationId : "sessionStat_totalSpent",
            value : this._model.getCurrencyString(this._model.getSessionSpent())
        });

        // Num games played in the session
        stats.push({
            localizationId : "sessionStat_numGames",
            value : this._model.getSessionNumGames()
        })

        return stats;
    }

    /**
     * Returns the configuration for the "Session Closed" message. This is an object,
     * that contains a LocalizationId (for the basic text item to show), and a list of
     * additional text modifications (as key/value pairs). By fetching this data from
     * SessionStatsViewModel, it means that we can quickly support additional messages
     * (which is a possible request from licensees), and that the modification is based
     * on "licensee specific business logic". This view simply has to render the information
     * (preferably in an interesting way)
     * @public
     * @return {SessionClosedFieldConfig}
     */
    getSessionClosedMessageConfig() {
        // todo:
        // implement
        return null;
    }

    /**
     * Returns a list of configuration objects, for all buttons that should be shown
     * in the footer in the "SessionClosed" state.
     * @public
     * @return {FooterButtonConfig[]}
     */
    getFooterButtonConfigs()
    {
        // So far, with the html games we have only shown a single button at a time,
        // and we have supported 3 options
        // 1) Session Restart button
        // 2) Close Client button
        // 3) nothing

        let configs = [];

        if (this._config.SESSION_RESTART_ALLOWED &&
            this._config.SESSION_MAX_CREDIT > this._model.getSessionCreditAdded())
        {
            configs.push({
                localizationId : "btn_restartSession",
                action : () => this.actionOnRestartSession()
            })
        }

        return configs;
    }

    //--------------------------------------------------------------------------------------------------
    // Public actions, which may be invoked by the view
    // NOTE: because we want to be able to vary which actions are available in our business logic,
    // we don't want the view to directly call these at the moment, instead the ViewModel basically
    // returns a list of footer button configs, which specify which action goes on which button.
    //--------------------------------------------------------------------------------------------------
    /**
     * @private
     */
    actionOnRestartSession() {
        // This would probably pass control back to main GameController
    }
}