export class SessionStatsViewState
{
    constructor()
    {
        /**
         * It is possible to enter the SessionStats view, when a Session is not yet closed.
         * In this case, we enter a special "closing session" state, and send the "close
         * session" request to the game server. Once we know that the session is closed, we
         * can enter the "SessionClosed" state.
         */
        this.CLOSING_SESSION = "ClosingSession";

        /**
         * 
         */
        this.SESSION_CLOSED = "SessionClosed";
    }
}