# CMS Core Logic

## Running Unit Tests
The unit tests are written using the Jasmine framework. They can be run from a simple npm command,
but this command is part of the cms-core-logic project (you must have npm dependencies installed
for cms-core-logic, and run this command from the root cms-core-logic project itself).

The actual setup for the unit-tests can be found under cms-core-logic/scripts/run-specs.js

Here is an overview of how to actually run them:

### Runnings All Specifications
To run a single spec (for example, "SlotModel.spec.js", found in the "models" folder)

npm run test -- --spec=model/SlotModel.spec.js

Please note:
- you don't need to add "spec" to the path, but this *is* where the script searchs for the spec files

### Running a single specification
npm