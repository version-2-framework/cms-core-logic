/**
 * Settings used for a single run of the "export business configs" operation.
 */
interface BusinessConfigCompilationSettings
{
    /**
     * The folder on disk that compiled v2 business configs should be exported to. The routine will
     * ensure that this folder exists before writing there.
     */
    outputFolder : string;

    /**
     * The mode in which the export should be performed.
     */
    mode : "v1" | "v2";

    /**
     * For V2 builds, id of optional partner. A value of WMG indicates its a standard WMG build, any
     * other value will be treated as an external partner (eg: "sgdigital")
     */
    partnerId : "wmg" | "sgdigital";
}

interface JsonConfigBuildInfo
{
    /**
     * Fully qualified root directory for sources, where we should find the file
     */
    sourceRootDirectory : string;

    /**
     * Ordered list (starting at base folder, moving to sub-folder) of all sub-folders (within "rouce
     * root directory") that lead us to the file to build.
     */
    sourceChildDirectories : string[];

    /**
     * Ordered list (starting at base folder, moving to sub-folder) of all sub-folders (within overall
     * output directory for the build) that the built file should be placed in. This list will (usually)
     * have the same length as sourceChildDirectories : however, it's quite possible some of the folder
     * names will have been changed (eg: we have a readable name in sources, and possible a simplified
     * enum name in build output)
     */
    outputChildDirectories : string[];

    /**
     * Name of source file on disk to be built
     */
    sourceFileName : string;

    /**
     * Name that the file should be saved under when built
     */
    outputFileName : string;
}

/**
 * Decibes how to build a licensee identifer file (a simple json, indicating the name of the licensee,
 * and what version core libs the file was built with). Useful for when we end up remapping output
 * folder names on disk to something abstract, like "licensee 15" (which has a more meaningful name,
 * which we might want to clock just by looking at the name of a file in that folder..)
 */
interface LicenseeIdentifierBuildInfo
{
    outputPath : string;

    outputFileName : string;
}