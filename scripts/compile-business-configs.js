/**
 * Script for building and exporting the set of Buiness Configs for all licensees (we select a
 * single mode at a time, eg: v1 business config exports, v2 business config exports). A specific
 * root output directory can also be specified, making this appropriate to incoporate as part of a
 * larger build script.
 * 
 * This build-script is a way of moving config files (BusinessConfig of LicenseeLocalizations)
 * into an output folder when building a game client. The files can either be copied exactly as they
 * are in source directories (but possibly renamed, and possibly in renamed output folders - as the
 * game client requires) - OR it can use JSON merging, to extend a root config file with some partial
 * config files (this is useful to have a COMMON config file with many settings, and to just override
 * a few settings for QA / STAGING / PRODUCTION). Both modes can be supported.
 * - if a json file to built includes an "_extends" field as a key on the root object, this indicates
 *   it must extend from another file. It can point to a file in the current directory, by using a
 *   value of "./FileName.json", or a file in the directory above, by using a value of "../FileName.json"
 *   Once built, the "_extends" key will be removed from the output json. This mechanic means that you
 *   can use arbitrary json extension
 * - if no "_extends" key is present, no extension takes place - the file is simply copied (possibly
 *   renamed as required, and possibly into a renamed output directory) - but the json in source, is a
 *   simple example of actual final json.
 * 
 * Here is an overview of how the build process works
 * - you invoke the "build configs" method, passing a "root source directory", and a "root output
 *   directory".
 * - the method traverses the root source directory (including all sub-folders)
 * - any file (found within the filetree coming from "rootSourceDirectory"), which has a name
 *   matching a given key, will be built, and will end up exported to the corresponding output
 *   hierarchy in rootOutputDirectory. The file names which are matched, are
 *   - anything starting with "Localizations_Licensee_"
 *   - any "leaf" BusinessConfig file ending in [QA, STAGING, PRODUCTION], and with the appropriate
 *     V1 or V2 token in its name
 *     eg:
 *     - BusinessConfig.V1.QA.json / BusinessConfig.V2.QA.json
 *     - BusinessConfig.V1.STAGING.json / BusinessConfig.V2.STAGING.json
 *     - BusinessConfig.V1.PRODUCTION.json / BusinessConfig.V2.PRODUCTION.json
 *     note that you run either a V1 or V2 configs build (they export output in slightly different
 *     formats), so EITHER the V1 or V2 configs will be picked for building (and not both)
 * 
 * To remap any licensee (or sub-licensees) in a folder, place a file called "OutputMappings.json"
 * in the folder's root, and add in the appropriate "V1" or "V2" tag. (On V1, we do not really use
 * this feature). So, the file name would be "OutputMappings.V1.json" or "OutputMappings.V2.json".
 * The file is a simple list of key/value pairs:
 * - key is "output name of folder"
 * - value is "input name of folder"
 * (This mechanic was chosen, because sometimes output names for main licensees are a set of enums)
 * 
 * Here is another way to think about it
 * - if you need a set of configs for a licensee, add a folder for it
 * - if you need sub-licensees, add a subfolder
 * - if you need a specific config file (BusinessConfig of LicenseeLocalizations) for any licensee
 *   or sublicensee (and really, you do), then you need a file in the folder with the appropriate
 *   name. The file has to specify the rules for its content : either by fully specifying all fields,
 *   or some (or no) fields + an "_extends" rule
 */

//--------------------------------------------------------------------------------------------------
// Imports
//--------------------------------------------------------------------------------------------------
const fs = require('fs-extra');
const path = require('path');
const semver = require('semver');
const winston = require('winston');

//--------------------------------------------------------------------------------------------------
// Logging
//--------------------------------------------------------------------------------------------------
const logger = winston.createLogger({
    level : "info",
    format : winston.format.json(),
    transports : [
        // TODO: Winston file logging not working for some reason ?
        //new winston.transports.File({ filename: 'compile-business-configs.log', level: 'info' }),
        new winston.transports.Console({
            level : "info",
            format : winston.format.simple()
        })
    ]
})

//--------------------------------------------------------------------------------------------------
// This build script depends on filesystem functionality not available in some older versions
// of node (Dan Mcneany was running the script on node v6.14.0, and it was failing, and causing
// some confusion!). Not 100% sure what node version would be mandatory for this to work: so,
// instead, we will enforce v13.9.0 (which for sure works)
//--------------------------------------------------------------------------------------------------
const requiredNodeVersion = '13.9.0';

if (!semver.gte(process.version, requiredNodeVersion)) {
    logger.error(`You are using node ${process.version}, but build script requires` +
        `node version of ${requiredNodeVersion} or greater to run properly. ` +
        `Please update your node version!`);

    process.exit(1);
}

//--------------------------------------------------------------------------------------------------
// Constants
//--------------------------------------------------------------------------------------------------
/**
 * Current core libs version being run
 */
const CORE_LIBS_VERSION = require('../package.json').version;

/**
 * Root folder, for all config files.
 */
let ROOT_SOURCE_CONFIGS_PATH = path.join(__dirname, '..', 'config');

/**
 * Root folder, contain all business configs.
 */
const ROOT_SOURCE_ONLINE_BUSINESS_CONFIGS_PATH = path.resolve(ROOT_SOURCE_CONFIGS_PATH, 'businessConfig', 'online');

logger.info(`compileBusinessConfigs: currDirectory:${__dirname}`)
logger.info(`compileBusinessConfigs: ROOT_SOURCE_CONFIGS_PATH:${ROOT_SOURCE_CONFIGS_PATH}`);
logger.info(`compileBusinessConfigs: ROOT_SOURCE_ONLINE_BUSINESS_CONFIGS_PATH:${ROOT_SOURCE_ONLINE_BUSINESS_CONFIGS_PATH}`);

//--------------------------------------------------------------------------------------------------
// V2 build process.
//--------------------------------------------------------------------------------------------------

/**
 * Compiles all business configs for a single mode (eg: v1, v2)
 * @param {BusinessConfigCompilationSettings} settings 
 */
function compileBusinessConfigs(settings)
{
    const MODE_TOKEN = settings.mode === "v1" ? "V1" : "V2";
    const IS_V1 = settings.mode === "v1";
    const IS_V2 = !IS_V1;

    /**
     * Builds all business configs that can be found in a root directory. This method will parse
     * the given directory, and all child directories (a single child directory, is treated as a
     * single output target). It looks for all json files which have a "leaf file" name, eg: have
     * a name that indicates that they are a buildable leaf file. A buildable leaf file can indicate
     * a file that it extends from (this allows us to use a mix of extension, or to simple have
     * final config files in a directory).
     * @param {string} rootSourceDirectory 
     * Fully qualified path to a root directory, from which we should read.
     * @param {string} rootOutputDirectory
     * Base output directory, into which to write.
     */
    function buildBusinessConfigs(rootSourceDirectory, rootOutputDirectory)
    {
        logger.info(`buildBusinessConfigs(rootSourceDir:${rootSourceDirectory}, rootOutputDir:${rootOutputDirectory})`);
    
        /**
         * List of meta-data objects, describing all files that we need to build. These files can
         * be any "licensee specific" json file (eg: BusinessConfig or Licensee_Localization files)
         * @type JsonConfigBuildInfo[]
         */
        let listOfFilesToBuild = [];

        /**
         * @type {LicenseeIdentifierBuildInfo[]}
         */
        let listOfLicenseeIdentifiersToBuild = [];

        /**
         * Parses our target directory (and all child directories), finding buildable files. For
         * each buildable file, generates meta-data ("FileInfo") which tell us where it is, what
         * list of child folders bring us to that file, and what the output directory would be
         * (taking into account remapping of output directories).
         * @param {string[]} sourceChildDirectories
         * @param {string[]} outputChildDirectories
         */
        let generateListOfFilesToBuild = (sourceChildDirectories, outputChildDirectories) =>
        {
            logger.debug(`readDirectory (currChildDirectories = ${sourceChildDirectories})`);

            let currDirectory = path.resolve(rootSourceDirectory, ...sourceChildDirectories);
            let mapFile = getOutputTargetMapFrom(currDirectory);

            if (mapFile) {
                logger.info(JSON.stringify(mapFile, null, 2));
            }

            fs.readdirSync(currDirectory, { withFileTypes:true })
                .forEach(itemInDirectory => {
                    // Found actual lead config file, which needs compiling
                    if (itemInDirectory.isFile() && isBuildableConfigFile(itemInDirectory.name))
                    {
                        logger.debug(`Found file called ${itemInDirectory.name}`);

                        let sourceFileName = itemInDirectory.name;
                        let outputFileName = sourceFileName.replace(`.${MODE_TOKEN}`, ""); // remove any mode tokens in filename
                    
                        listOfFilesToBuild.push({
                            sourceFileName,
                            sourceRootDirectory : rootSourceDirectory,
                            sourceChildDirectories,
                            outputChildDirectories,
                            outputFileName
                        });
                    }
                
                    // Found a child directory. If no mapfile present, we always build it. If a map file
                    // is present, then we must only build this sub-folder, if it is present in the map
                    // file.
                    if (itemInDirectory.isDirectory())
                    {
                        logger.debug(`Found directory called ${itemInDirectory.name}`);

                        let sourceDirName = itemInDirectory.name;
                        let shouldBuildSubFolder = ((mapFile && sourceDirName in mapFile) || !mapFile);
                        if (shouldBuildSubFolder)
                        {
                            let outputDirName = (mapFile && sourceDirName in mapFile) ? mapFile[sourceDirName] : sourceDirName;

                            let newSourceChildDirectories = sourceChildDirectories.concat(sourceDirName);
                            let newOutputChildDirectories = outputChildDirectories.concat(outputDirName);

                            // Make sure we get a licensee identifier added to list of stuff to build
                            // Don't bother with this for V1
                            if (IS_V2) {
                                listOfLicenseeIdentifiersToBuild.push({
                                    outputPath : path.resolve(rootOutputDirectory, ...newOutputChildDirectories),
                                    outputFileName : `${newSourceChildDirectories.join('.')}`
                                });
                            }

                            // Parse this output dir, which will push any buildable configs
                            // back into our list
                            return generateListOfFilesToBuild(
                                newSourceChildDirectories,
                                newOutputChildDirectories);
                        }
                    }
                });
        }
    
        generateListOfFilesToBuild([], []);
    
        // logging for good measure..
        logger.debug(listOfFilesToBuild);
    
        // Build the actual set of output files.
        listOfFilesToBuild.forEach(fileToBuild =>
        {
            /**
             * Series of JSON layers, that must be extended. This is in reverse-intuitive ordering,
             * eg: first item is "most leaf", and last item is "most-base" - so we must reverse
             * iterate over this list, when doing the actual extension.
             */
            let jsonLayers = [];
        
            let currBasePath = path.resolve(fileToBuild.sourceRootDirectory, ...fileToBuild.sourceChildDirectories);
            let currFileName = fileToBuild.sourceFileName;
            let foundRootJson = false;
        
            while (!foundRootJson)
            {
                let fullFilePath = path.resolve(currBasePath, currFileName);
                let actualJson = loadJsonFrom(fullFilePath);

                jsonLayers.push(actualJson);

                if ("_extends" in actualJson)
                {
                    let extensionToken = actualJson._extends;

                    logger.debug(`file ${fileToBuild.sourceFileName} must extend ${extensionToken}`);

                    // Look for file in curr directory..
                    if (extensionToken.startsWith("./")) {
                        // no change, we are still looking in this directory.
                        // currBasePath = currBasePath;
                        currFileName = extensionToken.split("./")[1];
                    }
                    else
                    if (extensionToken.startsWith("../")) {
                        // now we need to know how many layers upwards to go
                        // let's initially suppose, it can ONLY be one
                        currBasePath = path.resolve(currBasePath, '../');
                        currFileName = extensionToken.split("../")[1];
                    }
                    else
                    {
                        throw (new Error(`${fullFilePath}._extends = ${extensionToken}: needs to start with ./ or ../`));
                    }
                }
                else
                {
                    foundRootJson = true;
                }
            }
        
            // Finally, create the output json, and write it to disk.
            let mergedJson = {};
            for (let i = jsonLayers.length; i >= 0; i --) {
                mergedJson = Object.assign(mergedJson, jsonLayers[i]);
            }
            delete mergedJson._extends; // remove any custom "_extends" key from the output
            
            let outputPath = path.resolve(rootOutputDirectory, ...fileToBuild.outputChildDirectories);
            let outputFileName = path.join(outputPath, fileToBuild.outputFileName);

            fs.ensureDirSync(outputPath);
            fs.writeFileSync(outputFileName, JSON.stringify(mergedJson, null, 2));
        });

        if (IS_V1) {
            reorganizeOutputForV1(rootOutputDirectory);
        }

        listOfLicenseeIdentifiersToBuild.forEach(licenseeIdentifierToBuild => {
            writeLicenseeTargetIdentifierFileTo(
                licenseeIdentifierToBuild.outputPath,
                licenseeIdentifierToBuild.outputFileName
            )
        });
    };


    /**
     * This method is a convoluted hack. Because V1 cannot dynamically load configs, the easiest way to
     * make them available (for each licensee, for each QA / STAGING / PRODUCTION target) is to give them
     * a standard name (eg: always "BusinessConfig.json", and NOT "BusinessConfig.QA.json"), and store them
     * in sub-folders with the matching name. And the simplest way to achieve that in the current build
     * process (with its arbitrary searching of trees) is basically to run that process as normal, and then
     * re-scan the created output - each directory gets a blob of data - and move the files.
     * 
     * If a folder contains[BusinessConfig.QA.json, BusinessConfig.STAGING.json, BusinessConfig.PRODUCTION.json],
     * and then a bunch of localization files, we want to replace this with QA / STAGING / PRODUCTION subfolders.
     * We will push the renamed business config into the appropriate sub-folder, then we duplicate the localization
     * files into each sub-folder. We make sure all files we copied / moved, are removed from the original source
     * location, at the end of this process.
     * 
     * Convoluted!!
     * 
     * @param {string} rootOutputDirectory
     */
    function reorganizeOutputForV1(rootOutputDirectory)
    {
        /**
         * @type {{ directoryPath:string,localizationFileNames:string[],businessConfigFileNames:string[]}[]}
         */
        let filesToMove = [];

        /**
         * Scans a directory, generating a blob of data representing all files that need moving
         * (and then moves recursively to scan child directories)
         * @param {string} directoryPath 
         * Fully qualified path to directory that should be scanned.
         */
        let scanFolderForFilesToMove = directoryPath =>
        {
            let localizationFileNames = [];
            let businessConfigFileNames = [];

            fs.readdirSync(directoryPath, { withFileTypes:true })
                .forEach(itemInDirectory =>
                {
                    if (itemInDirectory.isDirectory())
                    {
                        scanFolderForFilesToMove(path.resolve(directoryPath, itemInDirectory.name));
                    }
                    else
                    {
                        let fileName = itemInDirectory.name;
                        
                        if (fileName.startsWith("Localizations_Licensee_")) {
                            localizationFileNames.push(fileName);
                        }
                        else
                        if (fileName.startsWith("BusinessConfig")) {
                            businessConfigFileNames.push(fileName);
                        }
                    }
                });

            filesToMove.push({ directoryPath, localizationFileNames, businessConfigFileNames });
        }

        let moveAllFiles = () =>
        {
            filesToMove.forEach(filesetToMove =>
            {
                // For each environment, we can move (and rename) the business config directly
                // (into the appropriately named subfolder). However, for localization files,
                // we don't move them : we actually have to recopy them into each! We can then
                // clear up the old localization files, once we have finished this whole step.
                ["QA", "STAGING", "PRODUCTION"].forEach(environment =>
                {
                    let businessConfigFileName = `BusinessConfig.${environment}.json`;

                    if (filesetToMove.businessConfigFileNames.indexOf(businessConfigFileName) > -1)
                    {
                        let newFolder = path.join(filesetToMove.directoryPath, environment);
                        let sourceFilePath = path.join(filesetToMove.directoryPath, businessConfigFileName);
                        let targetFilePath = path.join(newFolder, "BusinessConfig.json");

                        fs.ensureDirSync(newFolder);
                        fs.moveSync(sourceFilePath, targetFilePath);

                        filesetToMove.localizationFileNames.forEach(localizationFileName => {
                            let sourceLocalizationFilePath = path.join(filesetToMove.directoryPath, localizationFileName);
                            let targetLocalizationFilePath = path.join(newFolder, localizationFileName);

                            fs.copyFileSync(sourceLocalizationFilePath, targetLocalizationFilePath);
                        });
                    }
                });

                // NOW we can safely remove the old localization files
                filesetToMove.localizationFileNames.forEach(localizationFileName => {
                    let pathToFileToRemove = path.join(filesetToMove.directoryPath, localizationFileName);
                    fs.removeSync(pathToFileToRemove);
                });

                // NOTE! Eagle eyed maintainers, will spot that if we ever named any of our licensees as
                // one of [QA, STAGING, PRODUCTION], we would get into a right mess in this method. But
                // we are never going to do that, right ?
            });
        }

        scanFolderForFilesToMove(rootOutputDirectory);
        moveAllFiles();
    }


    /**
     * Reads an "output target map" from a given directory. This is a json file, which specifies
     * how the names of some folders on disk, should be remapped when generating exported output.
     * It's a simple map: keys represent "folder name in source", value represents "folder name
     * in output". This method checks if the requested file name exists in the folder: if it does,
     * the parsed map is returned, if not, a value of null is returned. The method can use a default
     * name for the actual file, so it can be used as a "fire and forget" method to check if any
     * remapping is required for any given source folder.
     * 
     * Why do we need to do this remapping step? Well, the game client loads business configs, based
     * on numerical ids, eg: it is told to load configs for licensee "13". And occasionally, the
     * set of licensee change (so licensee "13" might not always point to the same licensee). So,
     * in our game client build, we want to store licensee configs under their numerical enum.
     * But, in our project sources, this is not very helpful (we want a folder saying "licensee
     * name"). Therefore, the "outputTargetMap" is used to rename the output folders, as appropriate.
     * 
     * @param {string} pathToDirectory
     * @param {string} [fileName] Optional name of the file on disk to fetch: in certain cases,
     * we might grab this manually. Otherwise, we fall back to looking for the default file name
     * (which is what will be in use in most cases) - which is "OutputMappings.json"
     * @return {Object | null}
     * If a source map exists with the given file name, then the return value is a map, where key is
     * "sourceFolderName", and value is "folderName that is should be renamed to in the output game
     * client build folder". If no source map json exist under the given file name, then null is
     * returned.
     */
    function getOutputTargetMapFrom(pathToDirectory, fileName=`OutputMappings.${MODE_TOKEN}.json`)
    {
        /**
         * @param {string} mapFilePath 
         * @returns {Object}
         * The parsed map file.
         */
        function loadAndParseMappingsJson(mapFilePath)
        {
            let unparsedMap = JSON.parse(fs.readFileSync(mapFilePath, 'utf-8'));
            let parsedMap = {};
        
            let keys = Object.keys(unparsedMap);
        
            keys.forEach(key =>
            {
                let outputFolderName = key;
                let licenseeTargetKey = unparsedMap[key];
            
                parsedMap[licenseeTargetKey] = outputFolderName;
            });
        
            return parsedMap;
        }
    
        if (dirContainsFile(pathToDirectory, fileName))
        {
            let mapFilePath = path.join(pathToDirectory, fileName);
            return loadAndParseMappingsJson(mapFilePath);
        }
        else return null;
    }


    /**
     * Copies the appropriate default business configs into an output folder.
     * @param {string} targetFolder
     * Fully qualified path to target folder.
     */
    function copyDefaultBusinessConfigsTo(targetFolder)
    {
        let sourceFolder;

        if (settings.mode === "v1") {
            sourceFolder = path.resolve(ROOT_SOURCE_ONLINE_BUSINESS_CONFIGS_PATH, "default", "v1");
        }
        else
        {
            sourceFolder = path.resolve(ROOT_SOURCE_ONLINE_BUSINESS_CONFIGS_PATH, "default", "v2");
        }

        fs.copySync(sourceFolder, targetFolder);
    }


    /**
     * Checks if a given file name, indicates that it is a buildable config file. This method
     * takes into account whether we are doing a V1 or V2 build (certain buildable files have
     * names which incorporate these tokens)
     * @param {string} fileName
     * The string file name to check.
     * @return {boolean}
     * True if its a file that should be built, false if not.
     */
    function isBuildableConfigFile(fileName)
    {
        // TODO: regex might also be more appropriate ?
        let validFileNames = [
            `BusinessConfig.${MODE_TOKEN}.STAGING.json`,
            `BusinessConfig.${MODE_TOKEN}.PRODUCTION.json`,
            `BusinessConfig.${MODE_TOKEN}.QA.json`,
            "Localizations_Licensee_"];
        
        let isBuildable = false;
        
        for (let i = 0; i < validFileNames.length; i ++) {
            if (fileName.startsWith(validFileNames[i])) {
                isBuildable = true;
                break;
            }
        }
    
        return isBuildable;
    }

    throwIfCompilationSettingsInvalid(settings);

    // Do the actual build
    fs.removeSync(settings.outputFolder);

    let rootSourceFolder = getRootSourceFolder(settings);

    logger.info(`root source folder = ${rootSourceFolder}`);

    buildBusinessConfigs(
        rootSourceFolder,
        settings.outputFolder
    );

    if (settings.partnerId === "wmg") {
        copyDefaultBusinessConfigsTo(settings.outputFolder);
    }
}


/**
 * Throws an error, if the compilation settings are invalid
 * @param {BusinessConfigCompilationSettings} settings 
 */
function throwIfCompilationSettingsInvalid(settings)
{
    if (!settings) {
        throw new Error("BusinessConfig compilation settings invalid: cannot be null");
    }
    else
    {
        if (settings.partnerId === null || settings.partnerId === undefined) {
            throw new Error("BusinessConfig compilation settings invalid: partnerId must be supplied.");
        }
        else
        if (settings.mode !== "v1" && settings.mode !== "v2") {
            throw new Error("BusinessConfig compilation settings invalid: mode must be v1 or v2");
        }
        else
        if (settings.outputFolder === null || settings.outputFolder === undefined) {
            throw new Error("BusinessConfig compilation settings invalid: outputFolder must be supplied");
        }
    }
}


/**
 * Writes a licensee identifier filer into a specific folder. This is a simple file, which tells us some
 * useful info:
 * - name of file represents licensee name (useful when overall folder name gets remapped into something
 *   abstract)
 * - file contains meta-data, eg: core-libs version in use
 * The output file written is a simple json.
 * 
 * This method assumes that the output path folder already exists.
 * @param {string} outputPath 
 * @param {string} licenseeTargetName
 * The licensee targetname - better to provide in a form like "country.target", but if you pass in
 * "country/target", then the string will be sanitized for the filename on disk.
 */
 function writeLicenseeTargetIdentifierFileTo(outputPath, licenseeTargetName)
 {
    let outputData =
    {
        configFor : licenseeTargetName,
        libsVersion : CORE_LIBS_VERSION
    };
 
    // The "coutry/target" is how we currently expect this to be passed in.
    // This can get cleaned up a whole lot, but can be done later..
    let fileName = path.join(outputPath, `${licenseeTargetName}.json`);
 
    fs.writeFileSync(fileName, JSON.stringify(outputData, null, 2));
};


/**
 * Loads and returns a JSON file, from a file path
 * @param {string} filePath
 * Fully qualified file path
 * @returns {Object}
 * The parsed json file.
 */
function loadJsonFrom(filePath) {
    return JSON.parse(fs.readFileSync(filePath, "utf-8"));
}


/**
 * Synchronously checks if a directory contains a file with a given name.
 * @param {string} pathToDirectory
 * Fully qualified path to a directory: this is the dir we are going to check for a file with the
 * given name.
 * @param {string} fileName 
 * The file name to check for (including any extension)
 * @return {boolean}
 * True if the dir contains a file with that name, false if not.
 */
function dirContainsFile(pathToDirectory, fileName)
{
    let dirContainsFile = false;
    let dirFiles = fs.readdirSync(pathToDirectory, { withFileTypes:true });

    for (let i = 0; i < dirFiles.length; i ++)
    {
        let file = dirFiles[i];
        
        if (file.isFile() && file.name === fileName) {
            dirContainsFile = true;
            break;
        }
    };

    return dirContainsFile;
}


/**
 * Returns the string key, pointing to the root folder that needs compiling - based on input
 * settings flags. Basically, using this api, you request a "wmg" build, or a "partner" build,
 * and this method locates the specific location that the source business configs are located
 * (the folder which needs building)
 * @param {BusinessConfigCompilationSettings} settings 
 */
function getRootSourceFolder(settings)
{
    if (settings.partnerId === "wmg") {
        return path.join(ROOT_SOURCE_ONLINE_BUSINESS_CONFIGS_PATH, "wmg");
    }
    // We will use partner id directly as folder name
    else {
        return path.join(ROOT_SOURCE_ONLINE_BUSINESS_CONFIGS_PATH, "other", settings.partnerId);
    }
}


//--------------------------------------------------------------------------------------------------
// Exports
//--------------------------------------------------------------------------------------------------
module.exports =
{
    compileBusinessConfigs: compileBusinessConfigs
}