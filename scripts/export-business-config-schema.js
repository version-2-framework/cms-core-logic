const tsj = require("ts-json-schema-generator");
const fs = require("fs");

const outputPath = "BusinessConfig.schema.json";

const schema = tsj.createGenerator({
    path : "src/types/BusinessConfig.d.ts",
    expose : "all", // was "export"
    jsDoc : "extended",
    topRef : true
}).createSchema("BusinessConfig");

const schemaString = JSON.stringify(schema, null, 2);
fs.writeFile(outputPath, schemaString, err => {
    if (err) throw err;
});