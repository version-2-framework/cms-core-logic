// Simple script, which will run the compile business config script, and then save
// the output to disk within the core logic folder.

const BusinessConfig = require('./compile-business-configs');
const path = require('path');

BusinessConfig.compileBusinessConfigs({
    outputFolder : path.join(__dirname, '..', 'build' , 'v1'),
    mode : "v1"
});

BusinessConfig.compileBusinessConfigs({
    outputFolder : path.join(__dirname, '..', 'build', 'v2'),
    mode : "v2"
});