//--------------------------------------------------------------------------------------------------
// Run Specs
//--------------------------------------------------------------------------------------------------
// Runs unit tests for individual modules. There are various command line options available:
// --help
//   Will show the help for the test suite
// --test=id
// --spec=id
//   Will run a specific test file (the path/file specified in "id")
//--------------------------------------------------------------------------------------------------

const version = require('../package.json').version;
const currDir = process.argv[1];
const fs = require('fs');
const glob = require('glob');

console.log(`cms-core-logic [version ${version}] - unit tests`);

/**
 * Indicates if test suite help should be shown.
 */
let showHelp = false;

/**
 * @type {string[]}
 * Fetch node command line arguments. The first 2 are standard node arguments,
 * so we ignore them. We are looking for an argument prefixed "--test="
 */
let args = process.argv.slice(2);

/**
 * List of file names to run.
 * @type {string[]}
 */
let fileNames;

// Pre-process various arguments, work out what tests to run.
if (args.length > 0) {
    args.forEach(arg=>{
        if (arg.startsWith("--help")) {
            showHelp = true;
        }
        else
        if (arg.startsWith("--spec=")) {
            let testArgument;
            if (arg.startsWith("--test=")) {
                testArgument = arg.split("--test=")[1];
            }
            else {
                testArgument = arg.split("--spec=")[1];
            }

            fileNames = glob.sync(`${process.cwd()}/spec/${testArgument}`, null);

            if (fileNames.length === 0) {
                console.error(`No spec files could be found that matched provided argument ${testArgument}`);
                console.error(`Remember: the search for spec files based on fileName isn't smart yet:`);
                console.error(`If your spec file is in a sub-folder, you will have to provide the following:`);
                console.error(`--spec=pathToSpec/SpecFileName`);
                process.exit();
            }
        }
    });
}

if (showHelp)
{
    console.log('------------------------------------------------------------');
    console.log('manual:');
    console.log('------------------------------------------------------------');
    console.log('When run with no command line arguments, the entire test suite will be executed.\n');

    console.log('The following command line arguments may be passed:\n');

    console.log('--help');
    console.log('  Shows this help dialog');
    console.log();

    console.log('--test=path/fileId');
    console.log('--spec=path/fileId');
    console.log('  Will run the specific test file(s) specified.');
    console.log('  glob patterns are supported for fileId.');
    console.log();

    process.exit();
}

import Jasmine from 'jasmine'
let jasmine = new Jasmine();

if (fileNames && fileNames.length > 0) {
    console.log(`- executing ${fileNames.length} specs`);
    jasmine.specFiles = fileNames;
}
else {
    console.log('- running complete test suite');
    jasmine.loadConfigFile('jasmine.json');
}

jasmine.execute();