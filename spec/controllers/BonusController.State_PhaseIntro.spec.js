import EventEmitter from 'eventemitter3';
import BonusModel from "../../src/model/BonusModel";
import Model from "../../src/model/Model";

describe ("BonusController", ()=> {
    /** @type {BonusModel} */
    let bonusModel;

    /** @type {Dependencies} */
    let deps;

    beforeEach(()=>{
        deps = {};
        deps.dispatcher = new EventEmitter();
        deps.model = new Model(deps);
        deps.bonusModel = new BonusModel(deps);

        spyOn(deps.dispatcher, "emit").and.callThrough();
    });

    describe ("State.PhaseIntro", ()=> {

    });
});