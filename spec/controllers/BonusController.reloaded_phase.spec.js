import EventEmitter from 'eventemitter3';

describe("BonusController.reloadedPhase", ()=>
{
    let bonusController;
    let services;
    let model;

    /** @type {EventEmitter} */
    let events;

    describe("when a game is reloaded with all rounds played", ()=>
    {
        beforeEach(()=>
        {
            // TODO:
            // We have to create the conditions for the test here,
            // including manfuacturing the list of dependencies that
            // will be used by BonusController.

            events = new EventEmitter();
        });

        it ("should enter state TotalWinnings", ()=> {
            fail("test not implemented yet");
        });

        it ("should send a BonusPhaseComplete request to the server", ()=> {
            fail("test not implemented yet");
        });
    });
});