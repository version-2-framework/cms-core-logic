import { LocalResponseProvider } from "../../src/controllers/LocalResponseProvider";
import C_MessageField from "../../src/const/C_MessageField";

describe("LocalResponseProvider", ()=>
{
    /** @type {LocalResponseProvider} */
    let provider;

    beforeEach(()=>{
        provider = new LocalResponseProvider();
    });

    describe("createSpinResultsReply()", ()=> {
        let response;

        beforeEach(()=>{
            response = provider.createSpinResultsReply({});
        });

        it ("should return a response with a field for target symbols", ()=> {
            expect(response[C_MessageField.FINAL_SYMBOLS_DATA]).not.toBeUndefined();
        });

        it ("should return a response with a field for Winline Winnings", ()=> {
            expect(response[C_MessageField.WINLINE_WINNINGS]).not.toBeUndefined();
        });

        it ("should return a response with a field for Winline IDs", ()=> {
            expect(response[C_MessageField.WINLINE_IDS]).not.toBeUndefined();
        });

        it ("should return a response with a field for additional Winline Symbols Data", ()=> {
            expect(response[C_MessageField.WINLINE_SYMBOLS_DATA]).not.toBeUndefined();
        });

        it ("should return a response with a field for Play Bonus", ()=> {
            expect(response[C_MessageField.PLAY_BONUS]).not.toBeUndefined();
        });
    });

    describe("createSpin1ResultsReply()", ()=> {
        let response;

        beforeEach(()=>{
            response = provider.createSpin1ResultsReply({});
        });

        it ("should return a response with a field for target symbols", ()=> {
            expect(response[C_MessageField.FINAL_SYMBOLS_DATA]).not.toBeUndefined();
        });

        it ("should return a response with a field for Winline Winnings", ()=> {
            expect(response[C_MessageField.WINLINE_WINNINGS]).not.toBeUndefined();
        });

        it ("should return a response with a field for Winline IDs", ()=> {
            expect(response[C_MessageField.WINLINE_IDS]).not.toBeUndefined();
        });

        it ("should return a response with a field for additional Winline Symbols Data", ()=> {
            expect(response[C_MessageField.WINLINE_SYMBOLS_DATA]).not.toBeUndefined();
        });

        it ("should return a response with a field for Play Bonus", ()=> {
            expect(response[C_MessageField.PLAY_BONUS]).not.toBeUndefined();
        });
    });
});