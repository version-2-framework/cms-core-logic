///<reference path='../../src/types/Dependencies.d.ts'/>
import EventEmitter from 'eventemitter3';
import { SessionStatsViewModel } from '../../src/controllers/SessionStatsViewModel';
import BusinessConfig from '../../src/BusinessConfig';
import Model from '../../src/model/Model';
import Services from '../../src/controllers/Services';
import Comms from '../../src/controllers/Comms';
import { LocalResponseProvider } from '../../src/controllers/LocalResponseProvider';

describe ("SessionStatsViewModel", ()=> {
    /** @type {SessionStatsViewModel} */
    let vm;

    /** @type {Dependencies} */
    let deps;

    beforeAll(()=>{
        deps = {};
        deps.dispatcher = new EventEmitter();
        deps.model = new Model(deps);
        deps.businessConfig = new BusinessConfig();
    });

    beforeEach(()=>{
        deps.localResponseProvider = new LocalResponseProvider(deps);
        deps.comms = new Comms(deps);
        deps.services = new Services(deps);

        vm = new SessionStatsViewModel(deps);
    });

    // Here are some example tests (how they should be phrased)
    // Implementing these tests may be non-trivial, so i will
    // start with proposing how they look word-wise (then I can
    // figure out how to implement, and keep simple)

    describe ("when view is opened", ()=> {
        describe ("and Session is still open", ()=> {
            beforeEach(()=>{
                
            });

            it ("should enter closing session state", ()=> {

            });

            it ("should tell the server to close the session", ()=> {
                // we can test requests in one of 2 ways
                // 1) check that the service method was called
                // 2) have a "dummy server", and check it got its request
                // (its the same difference either way)
            });
        });

        describe ("and session is closed", ()=> {
            beforeEach(()=> {

            });

            it ("should enter SessionClosed state", ()=> {

            });

            it ("should not tell the server to close the session", ()=> {

            });
        });
    });

    describe("getFooterButtonConfigs()", ()=> {
        describe("when desktop", ()=> {
            /** @type {TextButtonConfig[]} */
            let btnConfigs;

            let foundSessionRestartBtn = false;
            let foundExitClientBtn = false;

            beforeEach(()=>{
                btnConfigs = vm.getFooterButtonConfigs();
                btnConfigs.forEach(btnConfig => {
                    if (btnConfig.localizationId === "T_button_restartSession") {
                        foundSessionRestartBtn = true;
                    }
                    else
                    if (btnConfig.localizationId === "T_button_exitClient") {
                        foundExitClientBtn = true;
                    }
                });
            });

            beforeAll(()=>{
                spyOn(deps.model, "isMobile").and.returnValue(false);
            });

            describe ("& Desktop Session Restart is enabled", ()=> {
                beforeAll(()=>{
                    deps.businessConfig.sessionRestartButtonDesktop.isEnabled = true;
                });

                describe("& maxSessionCredit rule is disabled", ()=>{
                    beforeAll(()=>{
                        deps.businessConfig.sessionMaxCredit.isEnabled = false;
                    });

                    it ("buttonConfigs should contain a session restart button", ()=> {
                        expect(foundSessionRestartBtn).toBeTruthy();
                    });
                });

                describe("maxSessionCredit is enabled", ()=> {
                    beforeAll(()=>{
                        deps.businessConfig.sessionMaxCredit.isEnabled = true;
                        deps.businessConfig.sessionMaxCredit.value = 1000;
                    });

                    describe("credit added is less than limit", ()=> {
                        beforeAll(()=>{
                            spyOn(deps.model, "getSessionCreditAdded").and.returnValue(990);
                        });

                        it ("buttonConfigs should contain a session restart button", ()=> {
                            expect(foundSessionRestartBtn).toBeTruthy();
                        });
                    });

                    describe("credit added is equal to limit", ()=> {
                        beforeAll(()=>{
                            spyOn(deps.model, "getSessionCreditAdded").and.returnValue(1000);
                        });

                        it ("buttonConfigs should not contain a session restart button", ()=> {
                            expect(foundSessionRestartBtn).toBeFalsy();
                        });
                    });
                });
            });

            describe ("when Desktop Close Client Button is enabled", ()=> {
                beforeAll(()=>{
                    deps.businessConfig.closeClientButtonDesktop.isEnabled = true;
                });

                it ("buttonConfigs should not be empty", ()=> {
                    expect(btnConfigs.length).toBeGreaterThan(0);
                });
            });
        });
    });

    describe("getSessionStats()", ()=> {

    });
});