import EventEmitter from 'eventemitter3';
import FpgBonusController from "../../../src/controllers/legacy/FpgBonusController";
import C_BonusState from '../../../src/const/C_BonusState';
import C_GameEvent from '../../../src/const/C_GameEvent';
import FpgBonusModel from '../../../src/model/legacy/FpgBonusModel';
import Model from '../../../src/model/Model';
import Services from '../../../src/controllers/Services';
import Comms from '../../../src/controllers/Comms';
import SlotConfig from '../../../src/config/DefaultSlotConfig';
import BusinessConfig from '../../../src/BusinessConfig';
import { LocalResponseProvider } from '../../../src/controllers/LocalResponseProvider';

describe ("FpgBonusController", ()=> {
    /** @type {FpgBonusController} */
    let controller;

    /** @type {Dependencies} */
    let deps;

    /** @type {FpgBonusModel} */
    let bonusModel;
    
    beforeEach(() => {
        deps = {};
        deps.dispatcher = new EventEmitter();
        deps.config = new SlotConfig();
        deps.businessConfig = new BusinessConfig();
        deps.model = new Model(deps);
        deps.bonusModel = bonusModel = new FpgBonusModel(deps);
        deps.localResponseProvider = new LocalResponseProvider(deps);
        deps.comms = new Comms(deps);
        deps.services = new Services(deps);

        controller = new FpgBonusController(deps);

        spyOn(deps.dispatcher, "emit").and.callThrough();
    });

    describe ("State_PhaseIntro", ()=> {
        describe ("after entering PhaseIntro state", ()=> {
            beforeEach(() => {
                controller.enterState(C_BonusState.PHASE_INTRO);
            })

            it ("should have broadcast EnterBonusState_PhaseIntro", ()=> {
                expect(deps.dispatcher.emit).toHaveBeenCalledWith(
                    `EnterBonusState_${C_BonusState.PHASE_INTRO}`
                );
            });

            describe ("when the BonusPhaseIntroAnimComplete event is broadcast", ()=> {
                describe ("for all cases", ()=> {
                    beforeEach(()=> {
                        deps.dispatcher.emit(C_GameEvent.BONUS_PHASE_INTRO_ANIM_SHOWN);
                    });

                    it ("should exit the BonusPhaseIntro state", ()=> {
                        expect (deps.dispatcher.emit).toHaveBeenCalledWith(
                            `ExitBonusState_${C_BonusState.PHASE_INTRO}`
                        );
                    });
                });

                describe ("if results have already been received", ()=> {
                    beforeEach(()=>{
                        spyOn(deps.bonusModel, "areResultsAvailable").and.returnValue(true);
                        deps.dispatcher.emit(C_GameEvent.BONUS_PHASE_INTRO_ANIM_SHOWN);
                    });

                    it ("should change state to RoundResults", ()=> {
                        expect (deps.dispatcher.emit).toHaveBeenCalledWith(
                            `EnterBonusState_${C_BonusState.ROUND_RESULTS}`
                        );
                    });
                });

                describe ("if results have not been received", ()=> {
                    beforeEach(()=> {
                        spyOn(deps.bonusModel, "areResultsAvailable").and.returnValue(false);
                        deps.dispatcher.emit(C_GameEvent.BONUS_PHASE_INTRO_ANIM_SHOWN);
                    })

                    it ("should change state to RoundInProgress", ()=> {
                        expect (deps.dispatcher.emit).toHaveBeenCalledWith(
                            `EnterBonusState_${C_BonusState.ROUND_IN_PROGRESS}`
                        );
                    })
                });
            });
        });
    });
});