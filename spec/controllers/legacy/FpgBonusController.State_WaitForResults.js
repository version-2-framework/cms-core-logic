import EventEmitter from 'eventemitter3';
import FpgBonusController from "../../../src/controllers/legacy/FpgBonusController";
import C_BonusState from '../../../src/const/C_BonusState';
import C_GameEvent from '../../../src/const/C_GameEvent';
import FpgBonusModel from '../../../src/model/legacy/FpgBonusModel';
import Model from '../../../src/model/Model';
import Services from '../../../src/controllers/Services';
import Comms from '../../../src/controllers/Comms';
import SlotConfig from '../../../src/config/DefaultSlotConfig';
import BusinessConfig from '../../../src/BusinessConfig';
import { LocalResponseProvider } from '../../../src/controllers/LocalResponseProvider';

describe ("FpgBonusController", ()=> {
    /** @type {FpgBonusController} */
    let controller;

    /** @type {Dependencies} */
    let deps;

    /** @type {FpgBonusModel} */
    let bonusModel;
    
    beforeEach(() => {
        deps = {};
        deps.dispatcher = new EventEmitter();
        deps.config = new SlotConfig();
        deps.businessConfig = new BusinessConfig();
        deps.model = new Model(deps);
        deps.bonusModel = bonusModel = new FpgBonusModel(deps);
        deps.localResponseProvider = new LocalResponseProvider(deps);
        deps.comms = new Comms(deps);
        deps.services = new Services(deps);

        controller = new FpgBonusController(deps);

        spyOn(deps.dispatcher, "emit").and.callThrough();
    });

    describe("State_WaitForResults", ()=> {
        describe ("after entering the state", ()=> {
            beforeEach(()=>{
                controller.enterState(C_BonusState.WAIT_FOR_RESULTS);
            });

            it ("should have broadcast EnterBonusState_WaitForResults", ()=> {
                expect(deps.dispatcher.emit).toHaveBeenCalledWith(
                    `EnterBonusState_${C_BonusState.WAIT_FOR_RESULTS}`
                );
            });

            describe ("when results are ready", ()=> {
                it ("should exit the WaitForResults state", ()=> {
                    expect(deps.dispatcher.emit).toHaveBeenCalledWith(
                        `ExitBonusState_${C_BonusState.WAIT_FOR_RESULTS}`
                    );
                });

                it ("should enter the RoundResults state", ()=> {
                    expect(deps.dispatcher.emit).toHaveBeenCalledWith(
                        `EnterBonusState_${C_BonusState.ROUND_RESULTS}`
                    );
                });
            });
        });
    });
});