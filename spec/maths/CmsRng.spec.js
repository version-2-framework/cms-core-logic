import { CmsRng } from "../../src/maths/CmsRng";

/**
 * The standard number of tests to run for any call to a "random" function.
 * We generally expect this number to return something quite representative.
 */
const NUM_TESTS = 1000;

describe ("CmsRng", ()=>{
    describe ("instantiation", ()=>{
        it ("should be instantiated without error", ()=>{
            expect(()=>new CmsRng()).not.toThrowError();
        });
    });

    describe("public methods", ()=>{
        /** @type {CmsRng} */
        let rng;

        beforeEach(()=> {
            rng = new CmsRng();
        });

        describe ("getSeedState", ()=> {
            it ("should return an object with ix,iy,iz parameters", ()=> {
                let seedState = rng.getSeedState();
                expect(seedState.ix).not.toBeUndefined();
                expect(seedState.iy).not.toBeUndefined();
                expect(seedState.iz).not.toBeUndefined();
            });

            it ("should return the values set by a call to setSeedState", ()=> {
                let ix = 45;
                let iy = 64;
                let iz = 2091;
                rng.setSeedState({ix,iy,iz});

                expect(rng.getSeedState().ix).toEqual(ix);
                expect(rng.getSeedState().iy).toEqual(iy);
                expect(rng.getSeedState().iz).toEqual(iz);
            });
        });

        describe ("getRandom()", ()=> {
            it ("should not throw an error", ()=> {
                expect(()=>rng.getRandom(1)).not.toThrowError();
            });

            it ("should always return 0 when range param is 0", ()=>{
                let numZeros = 0;
                for (let i = 0; i < NUM_TESTS; i ++) {
                    numZeros += rng.getRandom(0) === 0 ? 1 : 0;
                };

                expect(numZeros).toEqual(NUM_TESTS,
                    `in ${NUM_TESTS} tests, got ${NUM_TESTS-numZeros} non-zero values`);
            });

            it ("should not return a value equal or larger than range parameter", ()=>{
                let range = 4;
                let numOutsideRange = 0;
                for (let i = 0; i < NUM_TESTS; i ++) {
                    if (rng.getRandom(range) >= range) {
                        numOutsideRange += 1;
                    }
                }

                expect(numOutsideRange).toEqual(0,
                    `in ${NUM_TESTS} tests, got ${numOutsideRange} values >= ${range}`);
            });

            it ("should always return the same value after calls to setSeed with same seed parameter", ()=> {
                let range = 500;
                let seed = 6092;
                rng.setSeed(seed);
                let expectedRand = rng.getRandom(range);
                let numIncorrectRand = 0;

                for (let i = 0; i < NUM_TESTS; i ++) {
                    rng.setSeed(seed);
                    if (rng.getRandom(range) !== expectedRand) {
                        numIncorrectRand += 1;
                    }
                }

                expect(numIncorrectRand).toEqual(0,
                    `in ${NUM_TESTS} tests, got ${numIncorrectRand} values that didn't match expected value ${expectedRand}`);
            });

            it ("should always return the same value after calls to setSeedState with same parameter", ()=> {
                let range = 800;
                let seedState = rng.getSeedState();
                let expectedRand = rng.getRandom(range);
                let numIncorrectRand = 0;

                for (let i = 0; i < NUM_TESTS; i ++) {
                    rng.setSeedState(seedState);
                    if (rng.getRandom(range) !== expectedRand) {
                        numIncorrectRand += 1;
                    }
                }

                expect(numIncorrectRand).toEqual(0,
                    `in ${NUM_TESTS} tests, got ${numIncorrectRand} values that didn't match expected value ${expectedRand}`);
            });
        });

        describe ("getRandomBoolean()", ()=> {
            it ("should not throw an error", ()=> {
                expect(()=>rng.getRandomBoolean()).not.toThrowError();
            });

            it ("should always return true or false", ()=> {
                let numWrongType = 0;
                for (let i = 0; i < NUM_TESTS; i ++) {
                    let val = rng.getRandomBoolean();
                    if (val !== true && val !== false) {
                        numWrongType += 1;
                    }
                }

                expect(numWrongType).toEqual(0,
                    `in ${NUM_TESTS} tests, got ${numWrongType} results that were not boolean`);
            });
        });
    });
});