import C_SymbolType from "../../../../src/const/C_SymbolType";

export const SYM_1 = {
    id : 1,
    name : "Sym_1",
    type : C_SymbolType.PRIZE
};

export const SYM_2 = {
    id : 2,
    name : "Sym_2",
    type : C_SymbolType.PRIZE
};

export const SYM_3 = {
    id : 3,
    name : "Sym_3",
    type : C_SymbolType.PRIZE
};

export const SYM_4 = {
    id : 4,
    name : "Sym_4",
    type : C_SymbolType.PRIZE
};

export const SYM_5 = {
    id : 5,
    name : "Sym_5",
    type : C_SymbolType.PRIZE
};

export const SYM_6 = {
    id : 6,
    name : "Sym_6",
    type : C_SymbolType.PRIZE
};

export const SYM_7 = {
    id : 7,
    name : "Sym_7",
    type : C_SymbolType.PRIZE
};

export const SYM_8 = {
    id : 8,
    name : "Sym_8",
    type : C_SymbolType.WILD
};

export const SYM_9 = {
    id : 9,
    name : "Sym_9",
    type : C_SymbolType.PRIZE
};

export const SYM_10 = {
    id : 10,
    name : "Sym_10",
    type : C_SymbolType.SCATTER
};

/**
 * @type {SlotConfig}
 */
export const BgSlotConfig =
{
    gameId : "gameId",
    holdsMode : null, // not used !
    betConfig : {
        numHands : {
            isEnumerated : false,
            minValue : 1,
            maxValue : 1
        },
        betPerLine : {
            isEnumerated : true,
            values : [1,2,5,10,20,30,40,50,100]
        },
        numLines : {
            isEnumerated : true,
            values : [5,10,15,20]
        }
    },
    isTwoSpin : false,
    numHands : 1,
    numReels : 5,
    numSymsPerReel : [3,3,3,3,3],
    symbols : {
        [1]  : SYM_1,
        [2]  : SYM_2,
        [3]  : SYM_3,
        [4]  : SYM_4,
        [5]  : SYM_5,
        [6]  : SYM_6,
        [7]  : SYM_7,
        [8]  : SYM_8,
        [9]  : SYM_9,
        [10] : SYM_10
    },
    winlines : {
        [1]  : { id:1,  positions: [ 1, 1, 1, 1, 1 ] },
        [2]  : { id:2,  positions: [ 0, 0, 0, 0, 0 ] },
        [3]  : { id:3,  positions: [ 2, 2, 2, 2, 2 ] },
        [4]  : { id:4,  positions: [ 0, 1, 2, 1, 0 ] },
        [5]  : { id:5,  positions: [ 2, 1, 0, 1, 2 ] },
        [6]  : { id:6,  positions: [ 1, 0, 0, 0, 1 ] },
        [7]  : { id:7,  positions: [ 1, 2, 2, 2, 1 ] },
        [8]  : { id:8,  positions: [ 2, 1, 1, 1, 2 ] },
        [9]  : { id:9,  positions: [ 0, 1, 1, 1, 0 ] },
        [10] : { id:10, positions: [ 1, 1, 2, 1, 1 ] },
        [11] : { id:11, positions: [ 1, 1, 0, 1, 1 ] },
        [12] : { id:12, positions: [ 0, 0, 1, 2, 2 ] },
        [13] : { id:13, positions: [ 2, 2, 1, 0, 0 ] },
        [14] : { id:14, positions: [ 1, 2, 1, 0, 0 ] },
        [15] : { id:15, positions: [ 1, 0, 1, 2, 2 ] },
        [16] : { id:16, positions: [ 0, 1, 1, 1, 2 ] },
        [17] : { id:17, positions: [ 2, 1, 1, 1, 0 ] },
        [18] : { id:18, positions: [ 0, 1, 0, 1, 0 ] },
        [19] : { id:19, positions: [ 2, 1, 2, 1, 2 ] },
        [20] : { id:20, positions: [ 1, 0, 1, 0, 1 ] },
    }
}