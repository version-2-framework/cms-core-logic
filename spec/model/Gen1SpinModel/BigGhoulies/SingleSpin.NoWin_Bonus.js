import * as Bg from "./BgSlotConfig";
import * as C_RewardGroup from "../../../../src/const/C_RewardGroup";

/** @type {Gen1SpinParserSingleSpinTestDescriptor} */
export const BigGhoulies_SingleSpin_NoWin_Bonus_TestDescriptor =
{
    testId : "BigGhoulies.SingleSpin.NoWin_Bonus",

    // TODO: Decide if this SlotConfig should be part of a legacy package ??
    slotConfig : Bg.BgSlotConfig,

    // TODO: now i need a valid server result
    serverResult : {
        MID : "messageId",
        SID : "sessionId",
        PID : "ticketId",
        PPR : 14,
        RLD : false,
        FPL : false,
        WLT : 1000,
        PNT : 0,
        MTP : "WMG_GAME_RESULTS_REPLY",
        SNR : "1,2,1,4,3,1,1,1,4,6,1,4,8,1,2",
        RNR : "2,10,1,6,10,4,1,10,4,2,10,4,1,10,4",
        BET : 2000,
        LNS : 20,
        WLN : "0",
        WLV : "0",
        WSL : "0",
        PBN : "true"
    },

    targetResult : {
        totalCreditWon : 0,
        totalSuperBetWon : 0,
        numFreeGamesWon : 0,
        numFreeSpinsWon : 0,
        hasBonusWin : true,
        handsWithBonusWin : [1],
        hasBonusWinPerHand : [true],
        rounds : [{
            totalCreditWon : 0,
            totalSuperBetWon : 0,
            numFreeGamesWon : 0,
            spins : [{
                totalCreditWon : 0,
                totalSuperBetWon : 0,
                numFreeGamesWon : 0,
                numFreeSpinsWon : 0,
                progressiveMultiplier : 1,
                spinType : 1,
                symbolWins : [{
                    handId : 1,
                    lineId : 0,
                    rewardGroup : C_RewardGroup.BONUS,
                    rewardType : 1,
                    rewardValue : 0,
                    winningSymbol : Bg.SYM_10,
                    winningReels : [1,2,3,4,5],
                    winningPositions : [3,1,2,3,1],
                    winningPositionsMap : [
                        [ false, false, true  ],
                        [ true,  false, false ],
                        [ false, true,  false ],
                        [ false, false, true  ],
                        [ true,  false, false ]
                    ]
                }],
                specialWins : [],
                finalSymbolIds : [
                    [
                        [ Bg.SYM_2,  Bg.SYM_4,  Bg.SYM_10 ],
                        [ Bg.SYM_10, Bg.SYM_1,  Bg.SYM_4  ],
                        [ Bg.SYM_1,  Bg.SYM_10, Bg.SYM_1  ],
                        [ Bg.SYM_6,  Bg.SYM_4,  Bg.SYM_10 ],
                        [ Bg.SYM_10, Bg.SYM_2,  Bg.SYM_4  ]
                    ]
                ],
                stickySymbolsMap : [[
                    [ false, false, false ],
                    [ false, false, false ],
                    [ false, false, false ],
                    [ false, false, false ],
                    [ false, false, false ]
                ]],
                hasBonusWin : true,
                hasAnyWin : true
            }]
        }]
    }
}