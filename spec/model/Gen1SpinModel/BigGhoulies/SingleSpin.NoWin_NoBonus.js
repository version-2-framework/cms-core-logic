import * as Bg from "./BgSlotConfig";
import * as C_RewardGroup from "../../../../src/const/C_RewardGroup";

/** @type {Gen1SpinParserSingleSpinTestDescriptor} */
export const BigGhoulies_SingleSpin_NoWin_NoBonus_TestDescriptor =
{
    testId : "BigGhoulies.SingleSpin.NoWin_NoBonus",

    slotConfig : Bg.BgSlotConfig,
    
    serverResult : {
        MID : "messageId",
        SID : "sessionId",
        PID : "ticketId",
        PPR : 14,
        RLD : false,
        FPL : false,
        WLT : 1000,
        PNT : 0,
        MTP : "WMG_GAME_RESULTS_REPLY",
        SNR : "1,4,1,5,1,1,2,6,4,3,1,7,7,7,2",
        RNR : "2,6,1,4,3,4,10,7,8,2,5,9,5,1,1",
        BET : 2000,
        LNS : 20,
        WLN : "0",
        WLV : "0",
        WSL : "0",
        PBN : "false"
    },

    targetResult : {
        totalCreditWon : 0,
        totalSuperBetWon : 0,
        numFreeGamesWon : 0,
        numFreeSpinsWon : 0,
        hasBonusWin : false,
        handsWithBonusWin : [],
        hasBonusWinPerHand : [false],
        rounds : [{
            totalCreditWon : 0,
            totalSuperBetWon : 0,
            numFreeGamesWon : 0,
            spins : [{
                totalCreditWon : 0,
                totalSuperBetWon : 0,
                numFreeGamesWon : 0,
                numFreeSpinsWon : 0,
                progressiveMultiplier : 1,
                spinType : 1,
                symbolWins : [],
                specialWins : [],
                finalSymbolIds : [
                    [
                        [ Bg.SYM_2,  Bg.SYM_4,  Bg.SYM_5  ],
                        [ Bg.SYM_6,  Bg.SYM_10, Bg.SYM_9  ],
                        [ Bg.SYM_1,  Bg.SYM_7,  Bg.SYM_5  ],
                        [ Bg.SYM_4,  Bg.SYM_8,  Bg.SYM_1  ],
                        [ Bg.SYM_3,  Bg.SYM_2,  Bg.SYM_1  ]
                    ]
                ],
                stickySymbolsMap : [[
                    [ false, false, false ],
                    [ false, false, false ],
                    [ false, false, false ],
                    [ false, false, false ],
                    [ false, false, false ]
                ]],
                hasBonusWin : false,
                hasAnyWin : false
            }]
        }]
    }
}