import * as Bg from "./BgSlotConfig";
import * as C_RewardGroup from "../../../../src/const/C_RewardGroup";

/** @type {Gen1SpinParserSingleSpinTestDescriptor} */
export const BigGhoulies_SingleSpin_Wins_NoBonus_TestDescriptor =
{
    testId : "BigGhoulies.SingleSpin.Wins_NoBonus",

    // TODO: Decide if this SlotConfig should be part of a legacy package ??
    slotConfig : Bg.BgSlotConfig,

    // TODO: now i need a valid server result
    serverResult : {
        MID : "messageId",
        SID : "sessionId",
        PID : "ticketId",
        PPR : 14,
        RLD : false,
        FPL : false,
        WLT : 1000,
        PNT : 0,
        MTP : "WMG_GAME_RESULTS_REPLY",
        SNR : "9,10,9,1,4,3,3,6,2,4,2,5,7,7,7",
        RNR : "3,2,5,2,2,2,3,2,3,7,4,2,3,6,2",
        BET : 2000,
        LNS : 20,
        WLN : "4,20,15,14",
        WLV : "800,800,400,2000",
        WSL : "4,4,3,5",
        PBN : "false"
    },

    targetResult : {
        totalCreditWon : 4000,
        totalSuperBetWon : 0,
        numFreeGamesWon : 0,
        numFreeSpinsWon : 0,
        hasBonusWin : false,
        handsWithBonusWin : [],
        hasBonusWinPerHand : [false],
        rounds : [{
            totalCreditWon : 4000,
            totalSuperBetWon : 0,
            numFreeGamesWon : 0,
            spins : [{
                totalCreditWon : 4000,
                totalSuperBetWon : 0,
                numFreeGamesWon : 0,
                numFreeSpinsWon : 0,
                progressiveMultiplier : 1,
                spinType : 1,
                symbolWins : [{
                    lineId : 15,
                    handId : 1,
                    rewardGroup : C_RewardGroup.CREDIT,
                    rewardType : 1,
                    rewardValue : 400,
                    winningSymbol : Bg.SYM_2,
                    winningReels : [1,2,3],
                    winningPositions : [2,1,2],
                    winningPositionsMap : [
                        [ false, true,  false ],
                        [ true,  false, false ],
                        [ false, true,  false ],
                        [ false, false, false ],
                        [ false, false, false ]
                    ]
                },
                {
                    lineId : 20,
                    handId : 1,
                    rewardGroup : C_RewardGroup.CREDIT,
                    rewardType : 1,
                    rewardValue : 800,
                    winningSymbol : Bg.SYM_2,
                    winningReels : [1,2,3,4],
                    winningPositions : [2,1,2,1],
                    winningPositionsMap : [
                        [ false, true,  false ],
                        [ true,  false, false ],
                        [ false, true,  false ],
                        [ true,  false, false ],
                        [ false, false, false ]
                    ]
                },
                {
                    lineId : 4,
                    handId : 1,
                    rewardGroup : C_RewardGroup.CREDIT,
                    rewardType : 1,
                    rewardValue : 800,
                    winningSymbol : Bg.SYM_3,
                    winningReels : [1,2,3,4],
                    winningPositions : [1,2,3,2],
                    winningPositionsMap :
                    [
                        [ true,  false, false ],
                        [ false, true,  false ],
                        [ false, false, true  ],
                        [ false, true,  false ],
                        [ false, false, false ]
                    ]
                },
                {
                    lineId : 14,
                    handId : 1,
                    rewardGroup : C_RewardGroup.CREDIT,
                    rewardType : 1,
                    rewardValue : 2000,
                    winningSymbol : Bg.SYM_2,
                    winningReels : [1,2,3,4,5],
                    winningPositions : [2,3,2,1,1],
                    winningPositionsMap : 
                    [
                        [ false, true,  false ],
                        [ false, false, true  ],
                        [ false, true,  false ],
                        [ true,  false, false ],
                        [ true,  false, false ]
                    ]
                }],
                specialWins : [],
                finalSymbolIds : [
                    [
                        [ Bg.SYM_3,  Bg.SYM_2,  Bg.SYM_4  ],
                        [ Bg.SYM_2,  Bg.SYM_3,  Bg.SYM_2  ],
                        [ Bg.SYM_5,  Bg.SYM_2,  Bg.SYM_3  ],
                        [ Bg.SYM_2,  Bg.SYM_3,  Bg.SYM_6  ],
                        [ Bg.SYM_2,  Bg.SYM_7,  Bg.SYM_2  ]
                    ]
                ],
                stickySymbolsMap : [[
                    [ false, false, false ],
                    [ false, false, false ],
                    [ false, false, false ],
                    [ false, false, false ],
                    [ false, false, false ]
                ]],
                hasBonusWin : false,
                hasAnyWin : true
            }]
        }]
    }
}