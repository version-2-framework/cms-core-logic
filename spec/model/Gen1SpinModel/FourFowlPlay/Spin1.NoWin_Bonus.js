import C_SymbolType from "../../../../src/const/C_SymbolType";
import * as C_RewardGroup from "../../../../src/const/C_RewardGroup";
import * as Ffp from "./FfpSlotConfig";

/** @type {Gen1SpinParserSpin1TestDescriptor} */
export const FourFowlPlay_Spin1_NoWin_Bonus_TestDescriptor =
{
    testId : "FourFowlPlay.Spin1.NoWin_Bonus",

    slotConfig : Ffp.FfpSlotConfig,

    serverResult : {
        MID : "messageId",
        SID : "sessionId",
        PID : "ticketId",
        PPR : 14,
        RLD : false,
        FPL : false,
        WLT : 1000,
        PNT : 0,
        MTP : "WMG_SPIN1_RESULTS_REPLY",
        SNR : "2, 4,1,15,8,   2, 4,1,15,8,   2, 4,1,15,8,   2, 4,1,15,8",
        RNR : "16,1,5,24,12,  16,1,5,24,12,  16,1,5,24,12,  16,1,5,24,12",
        BET : 400,
        LNS : 10,
        WLN : "0",
        WLV : "0",
        WSL : "0",
        PBN : "true,true,false,false",
        AHD : "false,false,false,false,false, false,false,false,false,false, false,false,false,false,false, false,false,false,false,false"
    },

    targetResult : {
        totalCreditWon : 0,
        totalSuperBetWon : 0,
        numFreeGamesWon : 0,
        numFreeSpinsWon : 0,
        hasBonusWin : true,
        handsWithBonusWin : [1,2],
        hasBonusWinPerHand : [true,true,false,false],
        playSpin2 : false,
        suggestedAutoholds : {
            type : "holdReels",
            pattern : [
                [ false,false,false,false,false ],
                [ false,false,false,false,false ],
                [ false,false,false,false,false ],
                [ false,false,false,false,false ]
            ]
        },
        rounds : [{
            totalCreditWon : 0,
            totalSuperBetWon : 0,
            numFreeGamesWon : 0,
            spins : [{
                totalCreditWon : 0,
                totalSuperBetWon : 0,
                numFreeGamesWon : 0,
                numFreeSpinsWon : 0,
                progressiveMultiplier : 1,
                spinType : 1,
                symbolWins : [{
                    handId : 1,
                    lineId : 0,
                    rewardGroup : C_RewardGroup.BONUS,
                    rewardType : 1,
                    rewardValue : 0,
                    winningSymbol : Ffp.SYM_10,
                    winningReels : [1,3,4,5],
                    winningPositions : [2,1,1,2],
                    winningPositionsMap : [
                        [ false, true,  false ], // reel 1
                        [ false, false, false ], // reel 2
                        [ true,  false, false ], // reel 3
                        [ true,  false, false ], // reel 4
                        [ false, true,  false ], // reel 5
                    ]
                },
                {
                    handId : 2,
                    lineId : 0,
                    rewardGroup : C_RewardGroup.BONUS,
                    rewardType : 1,
                    rewardValue : 0,
                    winningSymbol : Ffp.SYM_10,
                    winningReels : [1,3,4,5],
                    winningPositions : [2,1,1,2],
                    winningPositionsMap : [
                        [ false, true,  false ], // reel 1
                        [ false, false, false ], // reel 2
                        [ true,  false, false ], // reel 3
                        [ true,  false, false ], // reel 4
                        [ false, true,  false ], // reel 5
                    ]
                }],
                specialWins : [],
                finalSymbolIds : [
                    [
                        [ Ffp.SYM_2,  Ffp.SYM_10, Ffp.SYM_1  ],
                        [ Ffp.SYM_3,  Ffp.SYM_9,  Ffp.SYM_2  ],
                        [ Ffp.SYM_10, Ffp.SYM_3,  Ffp.SYM_7  ],
                        [ Ffp.SYM_10, Ffp.SYM_6,  Ffp.SYM_5  ],
                        [ Ffp.SYM_1,  Ffp.SYM_10, Ffp.SYM_2  ]
                    ],
                    [
                        [ Ffp.SYM_2,  Ffp.SYM_10, Ffp.SYM_1  ],
                        [ Ffp.SYM_3,  Ffp.SYM_9,  Ffp.SYM_2  ],
                        [ Ffp.SYM_10, Ffp.SYM_3,  Ffp.SYM_7  ],
                        [ Ffp.SYM_10, Ffp.SYM_6,  Ffp.SYM_5  ],
                        [ Ffp.SYM_1,  Ffp.SYM_10, Ffp.SYM_2  ]
                    ],
                    [
                        [ Ffp.SYM_2,  Ffp.SYM_10, Ffp.SYM_1  ],
                        [ Ffp.SYM_3,  Ffp.SYM_9,  Ffp.SYM_2  ],
                        [ Ffp.SYM_10, Ffp.SYM_3,  Ffp.SYM_7  ],
                        [ Ffp.SYM_10, Ffp.SYM_6,  Ffp.SYM_5  ],
                        [ Ffp.SYM_1,  Ffp.SYM_10, Ffp.SYM_2  ]
                    ],
                    [
                        [ Ffp.SYM_2,  Ffp.SYM_10, Ffp.SYM_1  ],
                        [ Ffp.SYM_3,  Ffp.SYM_9,  Ffp.SYM_2  ],
                        [ Ffp.SYM_10, Ffp.SYM_3,  Ffp.SYM_7  ],
                        [ Ffp.SYM_10, Ffp.SYM_6,  Ffp.SYM_5  ],
                        [ Ffp.SYM_1,  Ffp.SYM_10, Ffp.SYM_2  ]
                    ]
                ],
                stickySymbolsMap : [
                    [
                        [ false, false, false ],
                        [ false, false, false ],
                        [ false, false, false ],
                        [ false, false, false ],
                        [ false, false, false ]
                    ],
                    [
                        [ false, false, false ],
                        [ false, false, false ],
                        [ false, false, false ],
                        [ false, false, false ],
                        [ false, false, false ]
                    ],
                    [
                        [ false, false, false ],
                        [ false, false, false ],
                        [ false, false, false ],
                        [ false, false, false ],
                        [ false, false, false ]
                    ],
                    [
                        [ false, false, false ],
                        [ false, false, false ],
                        [ false, false, false ],
                        [ false, false, false ],
                        [ false, false, false ]
                    ]
                ],
                hasBonusWin : true,
                hasAnyWin : true
            }]
        }]
    }
};