import C_SymbolType from "../../../../src/const/C_SymbolType";
import * as C_RewardGroup from "../../../../src/const/C_RewardGroup";
import * as Ffp from "./FfpSlotConfig";

/** @type {Gen1SpinParserSpin1TestDescriptor} */
export const FourFowlPlay_Spin1_NoWin_NoBonus_TestDescriptor =
{
    testId : "FourFowlPlay.Spin1.NoWin_NoBonus",

    slotConfig : Ffp.FfpSlotConfig,

    serverResult : {
        MID : "messageId",
        SID : "sessionId",
        PID : "ticketId",
        PPR : 14,
        RLD : false,
        FPL : false,
        WLT : 1000,
        PNT : 0,
        MTP : "WMG_SPIN1_RESULTS_REPLY",
        SNR : "1,2,1,1,4,1,2,3,16,4,1,2,12,0,4,1,2,19,20,4",
        RNR : "3,0,14,10,6, 3,0,14,10,6, 3,0,14,10,6, 3,0,14,10,6",
        BET : 400,
        LNS : 10,
        WLN : "0",
        WLV : "0",
        WSL : "0",
        PBN : "false,false,false,false",
        AHD : "1,1,0,1,1, 1,1,0,1,1, 1,1,0,1,1, 1,1,0,1,1"
    },

    targetResult : {
        totalCreditWon : 0,
        totalSuperBetWon : 0,
        numFreeGamesWon : 0,
        numFreeSpinsWon : 0,
        hasBonusWin : false,
        handsWithBonusWin : [],
        hasBonusWinPerHand : [false,false,false,false],
        suggestedAutoholds : {
            type : "holdReels",
            pattern : [
                [ true,true,false,true,true ],
                [ true,true,false,true,true ],
                [ true,true,false,true,true ],
                [ true,true,false,true,true ]
            ]
        },
        playSpin2 : true,
        rounds : [{
            totalCreditWon : 0,
            totalSuperBetWon : 0,
            numFreeGamesWon : 0,
            spins : [{
                totalCreditWon : 0,
                totalSuperBetWon : 0,
                numFreeGamesWon : 0,
                numFreeSpinsWon : 0,
                progressiveMultiplier : 1,
                spinType : 1,
                symbolWins : [],
                specialWins : [],
                finalSymbolIds : [
                    [
                        [ Ffp.SYM_6, Ffp.SYM_2, Ffp.SYM_1 ],
                        [ Ffp.SYM_2, Ffp.SYM_3, Ffp.SYM_9 ],
                        [ Ffp.SYM_1, Ffp.SYM_5, Ffp.SYM_8 ],
                        [ Ffp.SYM_5, Ffp.SYM_7, Ffp.SYM_4 ],
                        [ Ffp.SYM_7, Ffp.SYM_4, Ffp.SYM_6 ]
                    ],
                    [
                        [ Ffp.SYM_6, Ffp.SYM_2, Ffp.SYM_1 ],
                        [ Ffp.SYM_2, Ffp.SYM_3, Ffp.SYM_9 ],
                        [ Ffp.SYM_1, Ffp.SYM_5, Ffp.SYM_8 ],
                        [ Ffp.SYM_5, Ffp.SYM_7, Ffp.SYM_4 ],
                        [ Ffp.SYM_7, Ffp.SYM_4, Ffp.SYM_6 ]
                    ],
                    [
                        [ Ffp.SYM_6, Ffp.SYM_2, Ffp.SYM_1 ],
                        [ Ffp.SYM_2, Ffp.SYM_3, Ffp.SYM_9 ],
                        [ Ffp.SYM_1, Ffp.SYM_5, Ffp.SYM_8 ],
                        [ Ffp.SYM_5, Ffp.SYM_7, Ffp.SYM_4 ],
                        [ Ffp.SYM_7, Ffp.SYM_4, Ffp.SYM_6 ]
                    ],
                    [
                        [ Ffp.SYM_6, Ffp.SYM_2, Ffp.SYM_1 ],
                        [ Ffp.SYM_2, Ffp.SYM_3, Ffp.SYM_9 ],
                        [ Ffp.SYM_1, Ffp.SYM_5, Ffp.SYM_8 ],
                        [ Ffp.SYM_5, Ffp.SYM_7, Ffp.SYM_4 ],
                        [ Ffp.SYM_7, Ffp.SYM_4, Ffp.SYM_6 ]
                    ]
                ],
                stickySymbolsMap : [
                    [
                        [ false, false, false ],
                        [ false, false, false ],
                        [ false, false, false ],
                        [ false, false, false ],
                        [ false, false, false ]
                    ],
                    [
                        [ false, false, false ],
                        [ false, false, false ],
                        [ false, false, false ],
                        [ false, false, false ],
                        [ false, false, false ]
                    ],
                    [
                        [ false, false, false ],
                        [ false, false, false ],
                        [ false, false, false ],
                        [ false, false, false ],
                        [ false, false, false ]
                    ],
                    [
                        [ false, false, false ],
                        [ false, false, false ],
                        [ false, false, false ],
                        [ false, false, false ],
                        [ false, false, false ]
                    ]
                ],
                hasBonusWin : false,
                hasAnyWin : false
            }]
        }]
    }
};