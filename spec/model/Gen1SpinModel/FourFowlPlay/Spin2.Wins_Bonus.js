import C_SymbolType from "../../../../src/const/C_SymbolType";
import * as C_RewardGroup from "../../../../src/const/C_RewardGroup";
import * as Ffp from "./FfpSlotConfig";

/** @type {Gen1SpinParserSpin2TestDescriptor} */
export const FourFowlPlay_Spin2_Wins_Bonus_TestDescriptor =
{
    testId : "FourFowlPlay.Spin2.Wins_Bonus",

    slotConfig : Ffp.FfpSlotConfig,

    serverResult : {
        MID : "messageId",
        SID : "sessionId",
        PID : "ticketId",
        PPR : 14,
        RLD : false,
        FPL : false,
        WLT : 1000,
        PNT : 0,
        MTP : "WMG_SPIN2_RESULTS_REPLY",
        SNR : "1,4,0,7,18, 1,4,0,7,18, 1,4,0,7,18, 1,4,0,7,18",
        RNR : "15,14,5,22,12, 15,14,5,22,12, 15,14,5,22,12, 15,14,5,22,12",
        BET : 400,
        LNS : 10,
        WLN : "9,9,9",
        WLV : "250,250,250",
        WSL : "3,3,3",
        HWL : "1,2,3",
        PBN : "true,true,true,false",
        AHD : "0,1,1,0,1, 0,1,1,0,1, 0,1,1,0,1, 0,1,1,0,1"
    },

    targetResult : {
        totalCreditWon : 750,
        totalSuperBetWon : 0,
        numFreeGamesWon : 0,
        numFreeSpinsWon : 0,
        hasBonusWin : true,
        handsWithBonusWin : [1,2,3],
        hasBonusWinPerHand : [true,true,true,false],
        holdsUsed : {
            type : "holdReels",
            pattern : [
                [ false,true,true,false,true ],
                [ false,true,true,false,true ],
                [ false,true,true,false,true ],
                [ false,true,true,false,true ]
            ]
        },
        startSymbolIds : [
            [
                [ Ffp.SYM_1, Ffp.SYM_4, Ffp.SYM_6 ],
                [ Ffp.SYM_1, Ffp.SYM_6, Ffp.SYM_5 ],
                [ Ffp.SYM_1, Ffp.SYM_3, Ffp.SYM_8 ],
                [ Ffp.SYM_6, Ffp.SYM_1, Ffp.SYM_8 ],
                [ Ffp.SYM_3, Ffp.SYM_4, Ffp.SYM_8 ]
            ],
            [
                [ Ffp.SYM_1, Ffp.SYM_4, Ffp.SYM_6 ],
                [ Ffp.SYM_1, Ffp.SYM_6, Ffp.SYM_5 ],
                [ Ffp.SYM_1, Ffp.SYM_3, Ffp.SYM_8 ],
                [ Ffp.SYM_6, Ffp.SYM_1, Ffp.SYM_8 ],
                [ Ffp.SYM_3, Ffp.SYM_4, Ffp.SYM_8 ]
            ],
            [
                [ Ffp.SYM_1, Ffp.SYM_4, Ffp.SYM_6 ],
                [ Ffp.SYM_1, Ffp.SYM_6, Ffp.SYM_5 ],
                [ Ffp.SYM_1, Ffp.SYM_3, Ffp.SYM_8 ],
                [ Ffp.SYM_6, Ffp.SYM_1, Ffp.SYM_8 ],
                [ Ffp.SYM_3, Ffp.SYM_4, Ffp.SYM_8 ]
            ],
            [
                [ Ffp.SYM_1, Ffp.SYM_4, Ffp.SYM_6 ],
                [ Ffp.SYM_1, Ffp.SYM_6, Ffp.SYM_5 ],
                [ Ffp.SYM_1, Ffp.SYM_3, Ffp.SYM_8 ],
                [ Ffp.SYM_6, Ffp.SYM_1, Ffp.SYM_8 ],
                [ Ffp.SYM_3, Ffp.SYM_4, Ffp.SYM_8 ]
            ]
        ],
        rounds : [{
            totalCreditWon : 750,
            totalSuperBetWon : 0,
            numFreeGamesWon : 0,
            spins : [{
                totalCreditWon : 750,
                totalSuperBetWon : 0,
                numFreeGamesWon : 0,
                numFreeSpinsWon : 0,
                progressiveMultiplier : 1,
                spinType : 1,
                symbolWins : [{
                    handId : 1,
                    lineId : 9,
                    rewardGroup : C_RewardGroup.CREDIT,
                    rewardType : 1,
                    rewardValue : 250,
                    winningSymbol : Ffp.SYM_3,
                    winningPositions : [1,2,2],
                    winningReels : [1,2,3],
                    winningPositionsMap : [
                        [ true,  false, false ], // reel 1
                        [ false, true,  false ], // reel 2
                        [ false, true,  false ], // reel 3
                        [ false, false, false ], // reel 4
                        [ false, false, false ]  // reel 5
                    ]
                },
                {
                    handId : 2,
                    lineId : 9,
                    rewardGroup : C_RewardGroup.CREDIT,
                    rewardType : 1,
                    rewardValue : 250,
                    winningSymbol : Ffp.SYM_3,
                    winningPositions : [1,2,2],
                    winningReels : [1,2,3],
                    winningPositionsMap : [
                        [ true,  false, false ], // reel 1
                        [ false, true,  false ], // reel 2
                        [ false, true,  false ], // reel 3
                        [ false, false, false ], // reel 4
                        [ false, false, false ]  // reel 5
                    ]
                },
                {
                    handId : 3,
                    lineId : 9,
                    rewardGroup : C_RewardGroup.CREDIT,
                    rewardType : 1,
                    rewardValue : 250,
                    winningSymbol : Ffp.SYM_3,
                    winningPositions : [1,2,2],
                    winningReels : [1,2,3],
                    winningPositionsMap : [
                        [ true,  false, false ], // reel 1
                        [ false, true,  false ], // reel 2
                        [ false, true,  false ], // reel 3
                        [ false, false, false ], // reel 4
                        [ false, false, false ]  // reel 5
                    ]
                },
                {
                    handId : 1,
                    lineId : 0,
                    rewardGroup : C_RewardGroup.BONUS,
                    rewardType : 1,
                    rewardValue : 0,
                    winningSymbol : Ffp.SYM_10,
                    winningPositions : [3,1,3,2],
                    winningReels : [1,3,4,5],
                    winningPositionsMap : [
                        [ false, false, true  ], // reel 1
                        [ false, false, false ], // reel 2
                        [ true,  false, false ], // reel 3
                        [ false, false, true  ], // reel 4
                        [ false, true,  false ]  // reel 5
                    ]
                },
                {
                    handId : 2,
                    lineId : 0,
                    rewardGroup : C_RewardGroup.BONUS,
                    rewardType : 1,
                    rewardValue : 0,
                    winningSymbol : Ffp.SYM_10,
                    winningPositions : [3,1,3,2],
                    winningReels : [1,3,4,5],
                    winningPositionsMap : [
                        [ false, false, true  ], // reel 1
                        [ false, false, false ], // reel 2
                        [ true,  false, false ], // reel 3
                        [ false, false, true  ], // reel 4
                        [ false, true,  false ]  // reel 5
                    ]
                },
                {
                    handId : 3,
                    lineId : 0,
                    rewardGroup : C_RewardGroup.BONUS,
                    rewardType : 1,
                    rewardValue : 0,
                    winningSymbol : Ffp.SYM_10,
                    winningPositions : [3,1,3,2],
                    winningReels : [1,3,4,5],
                    winningPositionsMap : [
                        [ false, false, true  ], // reel 1
                        [ false, false, false ], // reel 2
                        [ true,  false, false ], // reel 3
                        [ false, false, true  ], // reel 4
                        [ false, true,  false ]  // reel 5
                    ]
                }],
                specialWins : [],
                finalSymbolIds : [
                    [
                        [ Ffp.SYM_3,  Ffp.SYM_2,  Ffp.SYM_10 ],
                        [ Ffp.SYM_1,  Ffp.SYM_3,  Ffp.SYM_5  ],
                        [ Ffp.SYM_10, Ffp.SYM_3,  Ffp.SYM_7  ],
                        [ Ffp.SYM_3,  Ffp.SYM_1,  Ffp.SYM_10 ],
                        [ Ffp.SYM_1,  Ffp.SYM_10, Ffp.SYM_2  ]
                    ],
                    [
                        [ Ffp.SYM_3,  Ffp.SYM_2,  Ffp.SYM_10 ],
                        [ Ffp.SYM_1,  Ffp.SYM_3,  Ffp.SYM_5  ],
                        [ Ffp.SYM_10, Ffp.SYM_3,  Ffp.SYM_7  ],
                        [ Ffp.SYM_3,  Ffp.SYM_1,  Ffp.SYM_10 ],
                        [ Ffp.SYM_1,  Ffp.SYM_10, Ffp.SYM_2  ]
                    ],
                    [
                        [ Ffp.SYM_3,  Ffp.SYM_2,  Ffp.SYM_10 ],
                        [ Ffp.SYM_1,  Ffp.SYM_3,  Ffp.SYM_5  ],
                        [ Ffp.SYM_10, Ffp.SYM_3,  Ffp.SYM_7  ],
                        [ Ffp.SYM_3,  Ffp.SYM_1,  Ffp.SYM_10 ],
                        [ Ffp.SYM_1,  Ffp.SYM_10, Ffp.SYM_2  ]
                    ],
                    [
                        [ Ffp.SYM_3,  Ffp.SYM_2,  Ffp.SYM_10 ],
                        [ Ffp.SYM_1,  Ffp.SYM_3,  Ffp.SYM_5  ],
                        [ Ffp.SYM_10, Ffp.SYM_3,  Ffp.SYM_7  ],
                        [ Ffp.SYM_3,  Ffp.SYM_1,  Ffp.SYM_10 ],
                        [ Ffp.SYM_1,  Ffp.SYM_10, Ffp.SYM_2  ]
                    ]
                ],
                stickySymbolsMap : [
                    [
                        [ false, false, false ],
                        [ false, false, false ],
                        [ false, false, false ],
                        [ false, false, false ],
                        [ false, false, false ]
                    ],
                    [
                        [ false, false, false ],
                        [ false, false, false ],
                        [ false, false, false ],
                        [ false, false, false ],
                        [ false, false, false ]
                    ],
                    [
                        [ false, false, false ],
                        [ false, false, false ],
                        [ false, false, false ],
                        [ false, false, false ],
                        [ false, false, false ]
                    ],
                    [
                        [ false, false, false ],
                        [ false, false, false ],
                        [ false, false, false ],
                        [ false, false, false ],
                        [ false, false, false ]
                    ]
                ],
                hasBonusWin : true,
                hasAnyWin : true
            }]
        }]
    }
};