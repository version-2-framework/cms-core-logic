import C_SymbolType from "../../../../src/const/C_SymbolType";
import * as C_RewardGroup from "../../../../src/const/C_RewardGroup";
import * as Ffp from "./FfpSlotConfig";

/** @type {Gen1SpinParserSpin2TestDescriptor} */
export const FourFowlPlay_Spin2_Wins_NoBonus_TestDescriptor =
{
    testId : "FourFowlPlay.Spin2.Wins_NoBonus",

    slotConfig : Ffp.FfpSlotConfig,

    serverResult : {
        MID : "messageId",
        SID : "sessionId",
        PID : "ticketId",
        PPR : 14,
        RLD : false,
        FPL : false,
        WLT : 1000,
        PNT : 0,
        MTP : "WMG_SPIN2_RESULTS_REPLY",
        SNR : "1,4,0,7,18, 1,4,0,7,18, 1,4,0,7,18, 1,4,0,7,18",
        RNR : "16,10,3,22,12, 16,10,3,22,12, 16,10,3,22,12, 16,10,3,22,12",
        BET : 400,
        LNS : 10,
        WLN : "7,9,7,9",
        WLV : "5000,50000,5000,50000",
        WSL : "3,5,3,5",
        HWL : "1,1,2,2",
        PBN : "false,false,false,false",
        AHD : "0,0,0,0,0, 0,0,0,0,0, 0,0,0,0,0, 0,0,0,0,0"
    },

    targetResult : {
        totalCreditWon : 110000,
        totalSuperBetWon : 0,
        numFreeGamesWon : 0,
        numFreeSpinsWon : 0,
        hasBonusWin : false,
        handsWithBonusWin : [],
        hasBonusWinPerHand : [false,false,false,false],
        holdsUsed : {
            type : "holdReels",
            pattern : [
                [ false,false,false,false,false ],
                [ false,false,false,false,false ],
                [ false,false,false,false,false ],
                [ false,false,false,false,false ]
            ]
        },
        startSymbolIds : [
            [
                [ Ffp.SYM_1, Ffp.SYM_4, Ffp.SYM_6 ],
                [ Ffp.SYM_1, Ffp.SYM_6, Ffp.SYM_5 ],
                [ Ffp.SYM_1, Ffp.SYM_3, Ffp.SYM_8 ],
                [ Ffp.SYM_6, Ffp.SYM_1, Ffp.SYM_8 ],
                [ Ffp.SYM_3, Ffp.SYM_4, Ffp.SYM_8 ]
            ],
            [
                [ Ffp.SYM_1, Ffp.SYM_4, Ffp.SYM_6 ],
                [ Ffp.SYM_1, Ffp.SYM_6, Ffp.SYM_5 ],
                [ Ffp.SYM_1, Ffp.SYM_3, Ffp.SYM_8 ],
                [ Ffp.SYM_6, Ffp.SYM_1, Ffp.SYM_8 ],
                [ Ffp.SYM_3, Ffp.SYM_4, Ffp.SYM_8 ]
            ],
            [
                [ Ffp.SYM_1, Ffp.SYM_4, Ffp.SYM_6 ],
                [ Ffp.SYM_1, Ffp.SYM_6, Ffp.SYM_5 ],
                [ Ffp.SYM_1, Ffp.SYM_3, Ffp.SYM_8 ],
                [ Ffp.SYM_6, Ffp.SYM_1, Ffp.SYM_8 ],
                [ Ffp.SYM_3, Ffp.SYM_4, Ffp.SYM_8 ]
            ],
            [
                [ Ffp.SYM_1, Ffp.SYM_4, Ffp.SYM_6 ],
                [ Ffp.SYM_1, Ffp.SYM_6, Ffp.SYM_5 ],
                [ Ffp.SYM_1, Ffp.SYM_3, Ffp.SYM_8 ],
                [ Ffp.SYM_6, Ffp.SYM_1, Ffp.SYM_8 ],
                [ Ffp.SYM_3, Ffp.SYM_4, Ffp.SYM_8 ]
            ]
        ],
        rounds : [{
            totalCreditWon : 110000,
            totalSuperBetWon : 0,
            numFreeGamesWon : 0,
            spins : [{
                totalCreditWon : 110000,
                totalSuperBetWon : 0,
                numFreeGamesWon : 0,
                numFreeSpinsWon : 0,
                progressiveMultiplier : 1,
                spinType : 1,
                symbolWins : [{
                    handId : 1,
                    lineId : 7,
                    rewardGroup : C_RewardGroup.CREDIT,
                    rewardType : 1,
                    rewardValue : 5000,
                    winningSymbol : Ffp.SYM_10,
                    winningPositions : [2,3,3],
                    winningReels : [1,2,3],
                    winningPositionsMap : [
                        [ false, true,  false ], // reel 1
                        [ false, false, true  ], // reel 2
                        [ false, false, true  ], // reel 3
                        [ false, false, false ], // reel 4
                        [ false, false, false ], // reel 5
                    ]
                },
                {
                    handId : 2,
                    lineId : 7,
                    rewardGroup : C_RewardGroup.CREDIT,
                    rewardType : 1,
                    rewardValue : 5000,
                    winningSymbol : Ffp.SYM_10,
                    winningPositions : [2,3,3],
                    winningReels : [1,2,3],
                    winningPositionsMap : [
                        [ false, true,  false ], // reel 1
                        [ false, false, true  ], // reel 2
                        [ false, false, true  ], // reel 3
                        [ false, false, false ], // reel 4
                        [ false, false, false ], // reel 5
                    ]
                },
                {
                    handId : 1,
                    lineId : 9,
                    rewardGroup : C_RewardGroup.CREDIT,
                    rewardType : 1,
                    rewardValue : 50000,
                    winningSymbol : Ffp.SYM_2,
                    winningPositions : [1,2,2,2,1],
                    winningReels : [1,2,3,4,5],
                    winningPositionsMap : [
                        [ true,  false, false ], // reel 1
                        [ false, true,  false ], // reel 2
                        [ false, true,  false ], // reel 3
                        [ false, true,  false ], // reel 4
                        [ true,  false, false ], // reel 5
                    ]
                },
                {
                    handId : 2,
                    lineId : 9,
                    rewardGroup : C_RewardGroup.CREDIT,
                    rewardType : 1,
                    rewardValue : 50000,
                    winningSymbol : Ffp.SYM_2,
                    winningPositions : [1,2,2,2,1],
                    winningReels : [1,2,3,4,5],
                    winningPositionsMap : [
                        [ true,  false, false ], // reel 1
                        [ false, true,  false ], // reel 2
                        [ false, true,  false ], // reel 3
                        [ false, true,  false ], // reel 4
                        [ true,  false, false ], // reel 5
                    ]
                }],
                specialWins : [],
                finalSymbolIds : [
                    [
                        [ Ffp.SYM_2,  Ffp.SYM_10, Ffp.SYM_1  ],
                        [ Ffp.SYM_3,  Ffp.SYM_2,  Ffp.SYM_8  ],
                        [ Ffp.SYM_4,  Ffp.SYM_2,  Ffp.SYM_10 ],
                        [ Ffp.SYM_3,  Ffp.SYM_1,  Ffp.SYM_10 ],
                        [ Ffp.SYM_1,  Ffp.SYM_10, Ffp.SYM_2  ]
                    ],
                    [
                        [ Ffp.SYM_2,  Ffp.SYM_10, Ffp.SYM_1  ],
                        [ Ffp.SYM_3,  Ffp.SYM_2,  Ffp.SYM_8  ],
                        [ Ffp.SYM_4,  Ffp.SYM_2,  Ffp.SYM_10 ],
                        [ Ffp.SYM_3,  Ffp.SYM_1,  Ffp.SYM_10 ],
                        [ Ffp.SYM_1,  Ffp.SYM_10, Ffp.SYM_2  ]
                    ],
                    [
                        [ Ffp.SYM_2,  Ffp.SYM_10, Ffp.SYM_1  ],
                        [ Ffp.SYM_3,  Ffp.SYM_2,  Ffp.SYM_8  ],
                        [ Ffp.SYM_4,  Ffp.SYM_2,  Ffp.SYM_10 ],
                        [ Ffp.SYM_3,  Ffp.SYM_1,  Ffp.SYM_10 ],
                        [ Ffp.SYM_1,  Ffp.SYM_10, Ffp.SYM_2  ]
                    ],
                    [
                        [ Ffp.SYM_2,  Ffp.SYM_10, Ffp.SYM_1  ],
                        [ Ffp.SYM_3,  Ffp.SYM_2,  Ffp.SYM_8  ],
                        [ Ffp.SYM_4,  Ffp.SYM_2,  Ffp.SYM_10 ],
                        [ Ffp.SYM_3,  Ffp.SYM_1,  Ffp.SYM_10 ],
                        [ Ffp.SYM_1,  Ffp.SYM_10, Ffp.SYM_2  ]
                    ]
                ],
                stickySymbolsMap : [
                    [
                        [ false, false, false ],
                        [ false, false, false ],
                        [ false, false, false ],
                        [ false, false, false ],
                        [ false, false, false ]
                    ],
                    [
                        [ false, false, false ],
                        [ false, false, false ],
                        [ false, false, false ],
                        [ false, false, false ],
                        [ false, false, false ]
                    ],
                    [
                        [ false, false, false ],
                        [ false, false, false ],
                        [ false, false, false ],
                        [ false, false, false ],
                        [ false, false, false ]
                    ],
                    [
                        [ false, false, false ],
                        [ false, false, false ],
                        [ false, false, false ],
                        [ false, false, false ],
                        [ false, false, false ]
                    ]
                ],
                hasBonusWin : false,
                hasAnyWin : true
            }]
        }]
    }
};