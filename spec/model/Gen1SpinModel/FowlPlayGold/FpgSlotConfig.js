import C_SymbolType from "../../../../src/const/C_SymbolType";

export const SYM_1 = {
    id : 1,
    name : "Sym_1",
    type : C_SymbolType.PRIZE
};

export const SYM_2 = {
    id : 2,
    name : "Sym_2",
    type : C_SymbolType.PRIZE
};

export const SYM_3 = {
    id : 3,
    name : "Sym_3",
    type : C_SymbolType.PRIZE
};

export const SYM_4 = {
    id : 4,
    name : "Sym_4",
    type : C_SymbolType.PRIZE
};

export const SYM_5 = {
    id : 5,
    name : "Sym_5",
    type : C_SymbolType.PRIZE
};

export const SYM_6 = {
    id : 6,
    name : "Sym_6",
    type : C_SymbolType.PRIZE
};

export const SYM_7 = {
    id : 7,
    name : "Sym_7",
    type : C_SymbolType.PRIZE
};

export const SYM_8 = {
    id : 8,
    name : "Sym_8",
    type : C_SymbolType.WILD
};

export const SYM_9 = {
    id : 9,
    name : "Sym_9",
    type : C_SymbolType.PRIZE
};

export const SYM_10 = {
    id : 10,
    name : "Sym_10",
    type : C_SymbolType.SCATTER
};

/** @type {SlotConfig} */
export const FpgSlotConfig =
{
    gameId : "gameId",
    holdsMode : "holdReels",
    betConfig : {
        numHands : {
            isEnumerated : false,
            minValue : 1,
            maxValue : 1
        },
        stakePerHand : [
            { betPerLine : 5,  numLines : 5  },
            { betPerLine : 10, numLines : 5  },
            { betPerLine : 20, numLines : 5  },
            { betPerLine : 20, numLines : 10 },
            { betPerLine : 40, numLines : 10 },
        ]
    },
    isTwoSpin : true,
    numHands : 1,
    numReels : 5,
    numSymsPerReel : [3,3,3,3,3],
    symbols : {
        [1]  : SYM_1,
        [2]  : SYM_2,
        [3]  : SYM_3,
        [4]  : SYM_4,
        [5]  : SYM_5,
        [6]  : SYM_6,
        [7]  : SYM_7,
        [8]  : SYM_8,
        [9]  : SYM_9,
        [10] : SYM_10,
    },
    winlines : {
        [1]  : { id:1,  positions: [ 1, 1, 1, 1, 1 ] },
        [2]  : { id:2,  positions: [ 0, 0, 0, 0, 0 ] },
        [3]  : { id:3,  positions: [ 2, 2, 2, 2, 2 ] },
        [4]  : { id:4,  positions: [ 0, 1, 2, 1, 0 ] },
        [5]  : { id:5,  positions: [ 2, 1, 0, 1, 2 ] },
        [6]  : { id:6,  positions: [ 1, 0, 0, 0, 1 ] },
        [7]  : { id:7,  positions: [ 1, 2, 2, 2, 1 ] },
        [8]  : { id:8,  positions: [ 2, 1, 1, 1, 2 ] },
        [9]  : { id:9,  positions: [ 0, 1, 1, 1, 0 ] },
        [10] : { id:10, positions: [ 1, 1, 2, 1, 1 ] }
    },
    reelbands : [
        // Reel 1
        [
            5,  // 0
            1,  // 1
            4,  // 2
            6,  // 3
            2,  // 4
            1,  // 5
            7,	// 6
            9,	// 7
            1,	// 8
            2,	// 9
            5,	// 10
            3,	// 11
            8,	// 12
            1,	// 13
            7,	// 14
            3,	// 15
            2,  // 16
            10, // 17
            1,	// 18
            9,	// 19
            4,	// 20
            1,	// 21
            3,	// 22
            9,	// 23
            6   // 24
        ],
        // Reel 2
        [
            2,	// 0
            3,	// 1
            9,	// 2
            2,	// 3
            1,	// 4
            6,	// 5
            5,	// 6
            4,	// 7
            1,	// 8
            9,	// 9
            3,	// 10
            2,	// 11
            8,	// 12
            5,	// 13
            1,	// 14
            3,	// 15
            5,	// 16
            10,	// 17
            2,	// 18
            4,	// 10
            7,	// 20
            2,	// 21
            3,	// 22
            4,	// 23
            6	// 24
        ],
        // Reel 3
        [
            1,	// 0
            3,	// 1
            8,	// 2
            4,	// 3
            2,	// 4
            10,	// 5
            3,	// 6
            7,	// 7
            2,	// 8
            4,	// 9
            9,	// 10
            5,	// 11
            8,	// 12
            2,	// 13
            1,	// 14
            5,	// 15
            8,	// 16
            3,	// 17
            4,	// 18
            5,	// 19
            1,	// 20
            4,	// 21
            2,	// 22
            6,	// 23
            5	// 24
        ],
        // Reel 4
        [
            6,	// 0
            5,	// 1
            1,	// 2
            6,	// 3
            7,	// 4
            9,	// 5
            5,	// 6
            6,	// 7
            1,	// 8
            8,	// 9
            5,	// 10
            7,	// 11
            4,	// 12
            6,	// 13
            9,	// 14
            2,	// 15
            6,	// 16
            8,	// 17
            1,	// 18
            7,	// 19
            5,	// 20
            2,	// 21
            3,	// 22
            1,	// 23
            10	// 24
        ],
        // Reel 5
        [
            3,	// 0
            4,	// 1
            1,	// 2
            6,	// 3
            2,	// 4
            1,	// 5
            7,	// 6
            4,	// 7
            6,	// 8
            1,	// 9
            4,	// 10
            3,	// 11
            1,	// 12
            10,	// 13
            2,	// 14
            3,	// 15
            5,	// 16
            6,	// 17
            3,	// 18
            4,	// 19
            8,	// 20
            6,	// 21
            8,	// 22
            1,	// 23
            9	// 24
        ]
    ]
};