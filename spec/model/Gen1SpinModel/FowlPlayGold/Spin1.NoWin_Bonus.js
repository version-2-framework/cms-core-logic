import C_SymbolType from "../../../../src/const/C_SymbolType";
import * as C_RewardGroup from "../../../../src/const/C_RewardGroup";
import * as Fpg from "./FpgSlotConfig";

/** @type {Gen1SpinParserSpin1TestDescriptor} */
export const FowlPlayGold_Spin1_NoWin_Bonus_TestDescriptor =
{
    testId : "FowlPlayGold.Spin1.NoWin_Bonus",
    
    slotConfig : Fpg.FpgSlotConfig,

    serverResult : {
        MID : "messageId",
        SID : "sessionId",
        PID : "ticketId",
        PPR : 14,
        RLD : false,
        FPL : false,
        WLT : 1000,
        PNT : 0,
        MTP : "WMG_SPIN1_RESULTS_REPLY",
        SNR : "2,4,1,15,8",
        RNR : "16,1,5,24,12",
        BET : 400,
        LNS : 10,
        WLN : "0",
        WLV : "0",
        WSL : "0",
        PBN : "true",
        AHD : "0, 0, 0, 0, 0"
    },

    targetResult : {
        totalCreditWon : 0,
        totalSuperBetWon : 0,
        numFreeGamesWon : 0,
        numFreeSpinsWon : 0,
        hasBonusWin : true,
        handsWithBonusWin : [1],
        hasBonusWinPerHand: [true],
        suggestedAutoholds : {
            type : "holdReels",
            pattern : [[
                false,false,false,false,false
            ]]
        },
        playSpin2: false,
        rounds : [{
            totalCreditWon : 0,
            totalSuperBetWon : 0,
            numFreeGamesWon : 0,
            spins : [{
                totalCreditWon : 0,
                totalSuperBetWon : 0,
                numFreeGamesWon : 0,
                numFreeSpinsWon : 0,
                progressiveMultiplier : 1,
                spinType : 1,
                symbolWins : [{
                    handId : 1,
                    lineId : 0,
                    rewardGroup : C_RewardGroup.BONUS,
                    rewardType : 1,
                    rewardValue : 0,
                    winningSymbol : Fpg.SYM_10,
                    winningReels : [1,3,4,5],
                    winningPositions : [2,1,1,2],
                    winningPositionsMap : [
                        [ false, true,  false ], // reel 1
                        [ false, false, false ], // reel 2
                        [ true,  false, false ], // reel 3
                        [ true,  false, false ], // reel 4
                        [ false, true,  false ], // reel 5
                    ]
                }],
                specialWins : [],
                finalSymbolIds : [
                    [
                        [ Fpg.SYM_2,  Fpg.SYM_10, Fpg.SYM_1  ],
                        [ Fpg.SYM_3,  Fpg.SYM_9,  Fpg.SYM_2  ],
                        [ Fpg.SYM_10, Fpg.SYM_3,  Fpg.SYM_7  ],
                        [ Fpg.SYM_10, Fpg.SYM_6,  Fpg.SYM_5  ],
                        [ Fpg.SYM_1,  Fpg.SYM_10, Fpg.SYM_2  ]
                    ]
                ],
                stickySymbolsMap : [[
                    [false,false,false],
                    [false,false,false],
                    [false,false,false],
                    [false,false,false],
                    [false,false,false]
                ]],
                hasBonusWin : true,
                hasAnyWin : true
            }]
        }]
    }
};