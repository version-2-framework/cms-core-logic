import C_SymbolType from "../../../../src/const/C_SymbolType";
import * as C_RewardGroup from "../../../../src/const/C_RewardGroup";
import * as Fpg from "./FpgSlotConfig";

/**
 * Test for Spin 1 phase results, where there are no wins (symbol or bonus) or any
 * kind. We want an autoholds pattern, we want playSpin2 to be true, etc.
 * @type {Gen1SpinParserSpin1TestDescriptor}
 */
export const FowlPlayGold_Spin1_NoWin_NoBonus_TestDescriptor =
{
    testId : "FowlPlayGold.Spin1.NoWin_NoBonus",

    slotConfig : Fpg.FpgSlotConfig,

    serverResult : {
        MID : "messageId",
        SID : "sessionId",
        PID : "ticketId",
        PPR : 14,
        RLD : false,
        FPL : false,
        WLT : 1000,
        PNT : 0,
        MTP : "WMG_SPIN1_RESULTS_REPLY",
        SNR : "0,0,0,0,0",
        RNR : "3,0,14,10,6",
        BET : 400,
        LNS : 10,
        WLN : "0",
        WLV : "0",
        WSL : "0",
        PBN : "false",
        AHD : "1,1,0,1,1"
    },

    targetResult : {
        totalCreditWon : 0,
        totalSuperBetWon : 0,
        numFreeGamesWon : 0,
        numFreeSpinsWon : 0,
        hasBonusWin : false,
        handsWithBonusWin : [],
        hasBonusWinPerHand : [false],
        suggestedAutoholds : {
            type : "holdReels",
            pattern : [[
                true,true,false,true,true
            ]]
        },
        playSpin2: true,
        rounds : [{
            totalCreditWon : 0,
            totalSuperBetWon : 0,
            numFreeGamesWon : 0,
            spins : [{
                totalCreditWon : 0,
                totalSuperBetWon : 0,
                numFreeGamesWon : 0,
                numFreeSpinsWon : 0,
                progressiveMultiplier : 1,
                spinType : 1,
                symbolWins : [],
                specialWins : [],
                finalSymbolIds : [
                    [
                        [ Fpg.SYM_6, Fpg.SYM_2, Fpg.SYM_1 ],
                        [ Fpg.SYM_2, Fpg.SYM_3, Fpg.SYM_9 ],
                        [ Fpg.SYM_1, Fpg.SYM_5, Fpg.SYM_8 ],
                        [ Fpg.SYM_5, Fpg.SYM_7, Fpg.SYM_4 ],
                        [ Fpg.SYM_7, Fpg.SYM_4, Fpg.SYM_6 ]
                    ]
                ],
                stickySymbolsMap : [[
                    [false,false,false],
                    [false,false,false],
                    [false,false,false],
                    [false,false,false],
                    [false,false,false]
                ]],
                hasBonusWin : false,
                hasAnyWin : false
            }]
        }]
    }
};