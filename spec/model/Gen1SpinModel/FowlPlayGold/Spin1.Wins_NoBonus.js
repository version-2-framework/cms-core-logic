import C_SymbolType from "../../../../src/const/C_SymbolType";
import * as C_RewardGroup from "../../../../src/const/C_RewardGroup";
import * as Fpg from "./FpgSlotConfig";

/** @type {Gen1SpinParserSpin1TestDescriptor} */
export const FowlPlayGold_Spin1_Wins_NoBonus_TestDescriptor =
{
    testId : "FowlPlayGold.Spin1.Wins_NoBonus",
    
    slotConfig : Fpg.FpgSlotConfig,

    serverResult : {
        MID : "messageId",
        SID : "sessionId",
        PID : "ticketId",
        PPR : 14,
        RLD : false,
        FPL : false,
        WLT : 1000,
        PNT : 0,
        MTP : "WMG_SPIN1_RESULTS_REPLY",
        SNR : "1,4,0,7,18",
        RNR : "16,10,3,22,12",
        BET : 400,
        LNS : 10,
        WLN : "7,9",
        WLV : "5000,50000",
        WSL : "3,5",
        PBN : "false",
        AHD : "0,0,0,0,0"
    },

    targetResult : {
        totalCreditWon : 55000,
        totalSuperBetWon : 0,
        numFreeGamesWon : 0,
        numFreeSpinsWon : 0,
        hasBonusWin : false,
        handsWithBonusWin : [],
        hasBonusWinPerHand : [false],
        suggestedAutoholds : {
            type : "holdReels",
            pattern : [[
                false,false,false,false,false
            ]]
        },
        playSpin2 : false,
        rounds : [{
            totalCreditWon : 55000,
            totalSuperBetWon : 0,
            numFreeGamesWon : 0,
            spins : [{
                totalCreditWon : 55000,
                totalSuperBetWon : 0,
                numFreeGamesWon : 0,
                numFreeSpinsWon : 0,
                progressiveMultiplier : 1,
                spinType : 1,
                symbolWins : [{
                    handId : 1,
                    lineId : 7,
                    rewardGroup : C_RewardGroup.CREDIT,
                    rewardType : 1,
                    rewardValue : 5000,
                    winningSymbol : Fpg.SYM_10, // TODO: change the pattern, this isnt a good symbol
                    winningPositions : [2,3,3],
                    winningReels : [1,2,3],
                    winningPositionsMap : [
                        [ false, true,  false ], // reel 1
                        [ false, false, true  ], // reel 2
                        [ false, false, true  ], // reel 3
                        [ false, false, false ], // reel 4
                        [ false, false, false ], // reel 5
                    ]
                },
                {
                    handId : 1,
                    lineId : 9,
                    rewardGroup : C_RewardGroup.CREDIT,
                    rewardType : 1,
                    rewardValue : 50000,
                    winningSymbol : Fpg.SYM_2,
                    winningPositions : [1,2,2,2,1],
                    winningReels : [1,2,3,4,5],
                    winningPositionsMap : [
                        [ true,  false, false ], // reel 1
                        [ false, true,  false ], // reel 2
                        [ false, true,  false ], // reel 3
                        [ false, true,  false ], // reel 4
                        [ true,  false, false ], // reel 5
                    ]
                }],
                specialWins : [],
                finalSymbolIds : [
                    [
                        [ Fpg.SYM_2,  Fpg.SYM_10, Fpg.SYM_1  ],
                        [ Fpg.SYM_3,  Fpg.SYM_2,  Fpg.SYM_8  ],
                        [ Fpg.SYM_4,  Fpg.SYM_2,  Fpg.SYM_10 ],
                        [ Fpg.SYM_3,  Fpg.SYM_1,  Fpg.SYM_10 ],
                        [ Fpg.SYM_1,  Fpg.SYM_10, Fpg.SYM_2  ]
                    ]
                ],
                stickySymbolsMap : [[
                    [false,false,false],
                    [false,false,false],
                    [false,false,false],
                    [false,false,false],
                    [false,false,false]
                ]],
                hasBonusWin : false,
                hasAnyWin : true
            }]
        }]
    }
};