import C_SymbolType from "../../../../src/const/C_SymbolType";
import * as C_RewardGroup from "../../../../src/const/C_RewardGroup";
import * as Fpg from "./FpgSlotConfig";

/** @type {Gen1SpinParserSpin2TestDescriptor} */
export const FowlPlayGold_Spin2_Wins_Bonus_TestDescriptor =
{
    testId : "FowlPlayGold.Spin2.Wins_Bonus",
    
    slotConfig : Fpg.FpgSlotConfig,

    serverResult : {
        MID : "messageId",
        SID : "sessionId",
        PID : "ticketId",
        PPR : 14,
        RLD : false,
        FPL : false,
        WLT : 1000,
        PNT : 0,
        MTP : "WMG_SPIN2_RESULTS_REPLY",
        SNR : "1,4,0,7,18",
        RNR : "15,14,5,22,12",
        BET : 400,
        LNS : 10,
        WLN : "9",
        WLV : "250",
        WSL : "3",
        PBN : "true",
        AHD : "1,0,0,0,1"
    },

    targetResult : {
        totalCreditWon : 250,
        totalSuperBetWon : 0,
        numFreeGamesWon : 0,
        numFreeSpinsWon : 0,
        hasBonusWin : true,
        handsWithBonusWin : [1],
        hasBonusWinPerHand : [true],
        holdsUsed : {
            type : "holdReels",
            pattern : [[
                true,false,false,false,true
            ]]
        },
        startSymbolIds : [
            [
                [ Fpg.SYM_1, Fpg.SYM_4, Fpg.SYM_6 ],
                [ Fpg.SYM_1, Fpg.SYM_6, Fpg.SYM_5 ],
                [ Fpg.SYM_1, Fpg.SYM_3, Fpg.SYM_8 ],
                [ Fpg.SYM_6, Fpg.SYM_1, Fpg.SYM_8 ],
                [ Fpg.SYM_3, Fpg.SYM_4, Fpg.SYM_8 ]
            ]
        ],
        rounds : [{
            totalCreditWon : 250,
            totalSuperBetWon : 0,
            numFreeGamesWon : 0,
            spins : [{
                totalCreditWon : 250,
                totalSuperBetWon : 0,
                numFreeGamesWon : 0,
                numFreeSpinsWon : 0,
                progressiveMultiplier : 1,
                spinType : 1,
                symbolWins : [{
                    handId : 1,
                    lineId : 9,
                    rewardGroup : C_RewardGroup.CREDIT,
                    rewardType : 1,
                    rewardValue : 250,
                    winningSymbol : Fpg.SYM_3,
                    winningPositions : [1,2,2],
                    winningReels : [1,2,3],
                    winningPositionsMap : [
                        [ true,  false, false ], // reel 1
                        [ false, true,  false ], // reel 2
                        [ false, true,  false ], // reel 3
                        [ false, false, false ], // reel 4
                        [ false, false, false ]  // reel 5
                    ]
                },
                {
                    handId : 1,
                    lineId : 0,
                    rewardGroup : C_RewardGroup.BONUS,
                    rewardType : 1,
                    rewardValue : 0,
                    winningSymbol : Fpg.SYM_10,
                    winningPositions : [3,1,3,2],
                    winningReels : [1,3,4,5],
                    winningPositionsMap : [
                        [ false, false, true  ], // reel 1
                        [ false, false, false ], // reel 2
                        [ true,  false, false ], // reel 3
                        [ false, false, true  ], // reel 4
                        [ false, true,  false ]  // reel 5
                    ]
                }],
                specialWins : [],
                finalSymbolIds : [
                    [
                        [ Fpg.SYM_3,  Fpg.SYM_2,  Fpg.SYM_10 ],
                        [ Fpg.SYM_1,  Fpg.SYM_3,  Fpg.SYM_5  ],
                        [ Fpg.SYM_10, Fpg.SYM_3,  Fpg.SYM_7  ],
                        [ Fpg.SYM_3,  Fpg.SYM_1,  Fpg.SYM_10 ],
                        [ Fpg.SYM_1,  Fpg.SYM_10, Fpg.SYM_2  ]
                    ]
                ],
                stickySymbolsMap : [[
                    [false,false,false],
                    [false,false,false],
                    [false,false,false],
                    [false,false,false],
                    [false,false,false]
                ]],
                hasBonusWin : true,
                hasAnyWin : true
            }]
        }]
    }
};