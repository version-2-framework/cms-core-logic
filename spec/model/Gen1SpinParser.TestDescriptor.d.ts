/**
 * Desribes a single test case for Gen 1 Spin Parser. The tests for Gen 1 Spin Parser are simple:
 * configure it with model, pass some input data to be parsed into V2 format, assert that output
 * v2 data matches expectations. The input data to the tests is what matters - because there are
 * many possible configurations, we base our test cases of some combination of input parameters
 * (eg: SlotConfig), specific Gen 1 server results, and expected parsed output result.
 * @type <SR> type of Server Result
 * @type <TR> type of Target Result (parsed)
 */
interface Gen1SpinParserTestDescriptor
    <SR, TR>
{
    /**
     * Id of this unit test case - used for debug (we can print it in any "test failed" messages)
     */
    testId : String;

    /**
     * Slot Config instance, which parmaterizes our Spin Result parser.
     */
    slotConfig : SlotConfig;

    /**
     * Input Gen 1 server result.
     */
    serverResult : SR;

    /**
     * Expected output result.
     */
    targetResult : TR;
}

/**
 * Describes a single unit test case for Single Spin
 * @inheritdoc
 */
 interface  Gen1SpinParserSingleSpinTestDescriptor
    extends
        Gen1SpinParserTestDescriptor<Gen1SingleSpinPhaseResultsReply, SingleSpinPhaseResult> {};

/**
 * Describes a single unit test case for Spin 1.
 * @inheritdoc
 */
interface  Gen1SpinParserSpin1TestDescriptor
    extends
        Gen1SpinParserTestDescriptor<Gen1Spin1PhaseResultsReply, Spin1PhaseResult> {};

/**
 * Describes a single unit test case for Spin 2.
 * @inheritdoc
 */
 interface  Gen1SpinParserSpin2TestDescriptor
    extends
        Gen1SpinParserTestDescriptor<Gen1Spin2PhaseResultsReply, Spin2PhaseResult> {};