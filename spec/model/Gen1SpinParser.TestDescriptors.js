import { FowlPlayGold_Spin1_NoWin_Bonus_TestDescriptor } from "./Gen1SpinModel/FowlPlayGold/Spin1.NoWin_Bonus";
import { FowlPlayGold_Spin1_NoWin_NoBonus_TestDescriptor } from "./Gen1SpinModel/FowlPlayGold/Spin1.NoWin_NoBonus";
import { FowlPlayGold_Spin1_Wins_Bonus_TestDescriptor } from "./Gen1SpinModel/FowlPlayGold/Spin1.Wins_Bonus";
import { FowlPlayGold_Spin1_Wins_NoBonus_TestDescriptor } from "./Gen1SpinModel/FowlPlayGold/Spin1.Wins_NoBonus";
import { FowlPlayGold_Spin2_NoWin_Bonus_TestDescriptor } from "./Gen1SpinModel/FowlPlayGold/Spin2.NoWin_Bonus";
import { FowlPlayGold_Spin2_NoWin_NoBonus_TestDescriptor } from "./Gen1SpinModel/FowlPlayGold/Spin2.NoWin_NoBonus";
import { FowlPlayGold_Spin2_Wins_Bonus_TestDescriptor } from "./Gen1SpinModel/FowlPlayGold/Spin2.Wins_Bonus";
import { FowlPlayGold_Spin2_Wins_NoBonus_TestDescriptor } from "./Gen1SpinModel/FowlPlayGold/Spin2.Wins_NoBonus";


import { BigGhoulies_SingleSpin_NoWin_NoBonus_TestDescriptor } from "./Gen1SpinModel/BigGhoulies/SingleSpin.NoWin_NoBonus";
import { BigGhoulies_SingleSpin_Wins_NoBonus_TestDescriptor } from "./Gen1SpinModel/BigGhoulies/SingleSpin.Wins_NoBonus";
import { BigGhoulies_SingleSpin_NoWin_Bonus_TestDescriptor } from "./Gen1SpinModel/BigGhoulies/SingleSpin.NoWin_Bonus";

import { FourFowlPlay_Spin1_NoWin_Bonus_TestDescriptor } from "./Gen1SpinModel/FourFowlPlay/Spin1.NoWin_Bonus";
import { FourFowlPlay_Spin1_NoWin_NoBonus_TestDescriptor } from "./Gen1SpinModel/FourFowlPlay/Spin1.NoWin_NoBonus";
import { FourFowlPlay_Spin1_Wins_Bonus_TestDescriptor } from "./Gen1SpinModel/FourFowlPlay/Spin1.Wins_Bonus";
import { FourFowlPlay_Spin1_Wins_NoBonus_TestDescriptor } from "./Gen1SpinModel/FourFowlPlay/Spin1.Wins_NoBonus";
import { FourFowlPlay_Spin2_NoWin_Bonus_TestDescriptor } from "./Gen1SpinModel/FourFowlPlay/Spin2.NoWin_Bonus";
import { FourFowlPlay_Spin2_NoWin_NoBonus_TestDescriptor } from "./Gen1SpinModel/FourFowlPlay/Spin2.NoWin_NoBonus";
import { FourFowlPlay_Spin2_Wins_Bonus_TestDescriptor } from "./Gen1SpinModel/FourFowlPlay/Spin2.Wins_Bonus";
import { FourFowlPlay_Spin2_Wins_NoBonus_TestDescriptor } from "./Gen1SpinModel/FourFowlPlay/Spin2.Wins_NoBonus";

// TODO: Needs explicit export statement..
export const Gen1SpinModelTestDescriptors =
[
    FowlPlayGold_Spin1_NoWin_Bonus_TestDescriptor,
    FowlPlayGold_Spin1_NoWin_NoBonus_TestDescriptor,
    FowlPlayGold_Spin1_Wins_Bonus_TestDescriptor,
    FowlPlayGold_Spin1_Wins_NoBonus_TestDescriptor,
    FowlPlayGold_Spin2_NoWin_Bonus_TestDescriptor,
    FowlPlayGold_Spin2_NoWin_NoBonus_TestDescriptor,
    FowlPlayGold_Spin2_Wins_Bonus_TestDescriptor,
    FowlPlayGold_Spin2_Wins_NoBonus_TestDescriptor,
    
    BigGhoulies_SingleSpin_NoWin_Bonus_TestDescriptor,
    BigGhoulies_SingleSpin_NoWin_NoBonus_TestDescriptor,
    BigGhoulies_SingleSpin_Wins_NoBonus_TestDescriptor,
    
    FourFowlPlay_Spin1_NoWin_Bonus_TestDescriptor,
    FourFowlPlay_Spin1_NoWin_NoBonus_TestDescriptor,
    FourFowlPlay_Spin1_Wins_Bonus_TestDescriptor,
    FourFowlPlay_Spin1_Wins_NoBonus_TestDescriptor,
    FourFowlPlay_Spin2_NoWin_Bonus_TestDescriptor,
    FourFowlPlay_Spin2_NoWin_NoBonus_TestDescriptor,
    FourFowlPlay_Spin2_Wins_Bonus_TestDescriptor,
    FourFowlPlay_Spin2_Wins_NoBonus_TestDescriptor
]