/**
 * @descriptor Unit tests for the Gen 1 Spin Model Parser.
 * @author Russell Wakely
 * 
 * This unit test is very simple: we take an input, parse it, and check the output
 * object against an expected output object. The grunt work for this test, comes
 * from external definitions: we have a Gen1SpinParserTestDescriptor interface, and
 * we can load a list of TestDescriptors that match this interface. We then iterate
 * through them, running the same unit test for each TestDescriptor. The TestDesctiptor
 * instances are stored in separate files (1 to a file), because they can be rather
 * large, and we want TestDescriptors for many different game maths setups.
 * 
 * The reason this approach was adopted: testing this class is basically data driven
 * (a TestDesciptor consists of input data, expected output data, and a SlotConfig,
 * that lets the GameClient work out any additional information that it is interested
 * in). To put all this in a single unit test code unit, would make that code unit
 * huge (imagine wanting 20 or more separate Test Descriptors).
 */

import {Gen1SpinResultParser} from '../../src/model/Gen1SpinResultParser';
import {SpinModelUtils} from '../../src/model/SpinModelUtils';
import {Gen1SpinModelTestDescriptors as TestDescriptors} from './Gen1SpinParser.TestDescriptors';

describe("Gen1SpinParser", () => {
    // Parse server result is the main unit test, and the one that we put a lot of effort into.
    // For this reason, we have many available test setups.
    describe("parseServerResult", () => {
        TestDescriptors.forEach(testDescriptor => {
            describe(`for a ${testDescriptor.testId} Result`, () => {
                /** @type {Gen1SpinPhaseResultsReply} */
                let serverResult;

                /** @type {SpinPhaseResult} */
                let targetResult;

                /** @type {SpinPhaseResult} */
                let parsedResult;

                let config = testDescriptor.slotConfig;
                let spinModelUtils = new SpinModelUtils({ config });

                beforeEach(() => {
                    serverResult = testDescriptor.serverResult;
                    targetResult = testDescriptor.targetResult;

                    parsedResult = new Gen1SpinResultParser({ config, spinModelUtils })
                        .parseServerResult(serverResult);
                });

                it (`parsedResult should match targetResult`, () => {
                    expect(parsedResult).toEqual(targetResult);
                });
            });
        });
    });
});