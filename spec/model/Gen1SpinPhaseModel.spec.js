/**
 * Unit tests covering the Gen 1 Spin Phase Model.
 * 
 * Most of this functioanlity is covered by the Gen 1 Spin Parser tests, but the Spin Phase Model
 * supplements the results returned by the parser, with some additional data. We can use the same
 * set of input setup (found under the Gen1SpinModel sub-directory) to drive these tests.
 */

import {Gen1SpinModelTestDescriptors as TestDescriptors} from './Gen1SpinParser.TestDescriptors';
import Gen1SpinPhaseModel from '../../src/model/Gen1SpinPhaseModel';

describe("Gen1SpinPhaseModel", () => {
    TestDescriptors.forEach(testDescriptor => {
        describe(`for a ${testDescriptor.testId} Result`, () => {
            /** @type {SpinPhaseModel} */
            let model;

            beforeEach(() => {
                model = new Gen1SpinPhaseModel({config:testDescriptor.slotConfig});
            });

            describe ("before any PhaseResult has been passed to the SpinPhaseModel", () => {
                describe("getFinalSymbolIds", () => {
                    it ("should not be null", () => {
                        expect(model.getFinalSymbolIds()).not.toBeNull();
                    });

                    it ("should return a set of symbols with the expected number of hands", () => {
                        let numHandsExpect = testDescriptor.slotConfig.numHands;
                        let numHandsActual = model.getFinalSymbolIds().length;
                        expect(numHandsExpect).toEqual(numHandsActual);
                    });

                    it ("each hand should have the expected number of reels", () => {
                        let numReelsExpect = testDescriptor.slotConfig.numReels;
                        model.getFinalSymbolIds().forEach(handSymbols => {
                            let numReelsActual = handSymbols.length;
                            expect(numReelsActual).toEqual(numReelsExpect);
                        });
                    });

                    it ("each reel on each hand, should have the expected number of symbols", () => {
                        model.getFinalSymbolIds().forEach(handSymbols => {
                            handSymbols.forEach((reelSymbols, reelIndex) => {
                                let numSymOnReelExpect = testDescriptor.slotConfig.numSymsPerReel[reelIndex];
                                let numSymOnReelActual = reelSymbols.length;
                                expect(numSymOnReelExpect).toEqual(numSymOnReelActual);
                            });
                        });
                    });
                });

                it ("totalCreditWon should be 0", () => {
                    expect(model.getTotalCreditWon()).toEqual(0);
                });
                
                it ("totalSuperbetWon should be 0", () => {
                    expect(model.getTotalSuperBetWon()).toEqual(0);
                });

                it ("numFreeSpins won should be 0", () => {
                    expect (model.getNumFreeSpinsWon()).toEqual(0);
                });

                it ("numPromoGamesWon should be 0", () => {
                    expect (model.getNumFreeGamesWon()).toEqual(0);
                });

                it ("isReloadedPhase should be false", () => {
                    expect (model.getIsReloadedPhase()).toEqual(false);
                })
            });
        });
    });
});