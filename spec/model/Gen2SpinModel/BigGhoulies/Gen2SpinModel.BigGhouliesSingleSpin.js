import C_SymbolType from "../../../../src/const/C_SymbolType";
import * as C_ServiceResponseType from "../../../../src/const/C_ServiceResponseTypeGen2";

// Example of a "test fixture" - eg just the data part of the test.
// NOTE ALSO that this is Single Spin test (the file name doesnt reflect this yet)

const SYM_1 = {
    id : 1,
    name : "Sym_1",
    type : C_SymbolType.PRIZE
};

const SYM_2 = {
    id : 2,
    name : "Sym_3",
    type : C_SymbolType.PRIZE
};

const SYM_3 = {
    id : 3,
    name : "Sym_3",
    type : C_SymbolType.PRIZE
};

const SYM_4 = {
    id : 4,
    name : "Sym_4",
    type : C_SymbolType.PRIZE
};

const SYM_5 = {
    id : 5,
    name : "Sym_5",
    type : C_SymbolType.PRIZE
};

const SYM_6 = {
    id : 6,
    name : "Sym_6",
    type : C_SymbolType.PRIZE
};

const SYM_7 = {
    id : 7,
    name : "Sym_7",
    type : C_SymbolType.PRIZE
};

const SYM_8 = {
    id : 8,
    name : "Sym_8",
    type : C_SymbolType.WILD
};

const SYM_9 = {
    id : 9,
    name : "Sym_9",
    type : C_SymbolType.PRIZE
};

const SYM_10 = {
    id : 10,
    name : "Sym_10",
    type : C_SymbolType.SCATTER
};

/** @type {Gen2SpinParserSingleSpinTestDescriptor} */
export const BigGhouliesSingleSpinTestDescriptor =
{
    testId : "Big Ghoulies Single Spin",

    slotConfig : {
        isTwoSpin : false,
        numHands : 1,
        numReels : 5,
        numSymsPerReel : [3,3,3,3,3],
        symbols : {
            [1] : SYM_1,
            [2] : SYM_2,
            [3] : SYM_3,
            [4] : SYM_4,
            [5] : SYM_5,
            [6] : SYM_6,
            [7] : SYM_7,
            [8] : SYM_8,
            [9] : SYM_9,
            [10] : SYM_10
        },
        winlines : null
    },

    serverResult :
    {
        phaseId : 1,
        resultType : 1,
        totalCreditWon : 4800,
        totalSuperBetWon : 0,
        numFreeGamesWon : 0,
        numFreeSpinsWon : 0,
        handsWithBonusWin : [],
        spins : [{
            totalCreditWon : 4800,
            totalSuperBetWon : 0,
            numFreeGamesWon : 0,
            numFreeSpinsWon : 0,
            spinType : 1,
            progressiveMultiplier: 1,
            symbolIds : [[7,7,7,7,7,7,7,7,7,7,2,1,3,3,4]],
            symbolWins : [{
                handId : 1,
                lineId : 1,
                rewardGroup : 1,
                rewardType : 1,
                rewardValue : 800,
                symbolId : 7,
                winningPositions : [1,4,7]
            },
            {
                handId : 1,
                lineId : 2,
                rewardGroup : 1,
                rewardType : 1,
                rewardValue : 1600,
                symbolId : 7,
                winningPositions : [2,5,8,11]
            },
            {
                handId : 1,
                lineId : 3,
                rewardGroup : 1,
                rewardType : 1,
                rewardValue : 800,
                symbolId : 7,
                winningPositions : [0,3,6]
            },
            {
                handId : 1,
                lineId : 4,
                rewardGroup : 1,
                rewardType : 1,
                rewardValue : 800,
                symbolId : 7,
                winningPositions : [0,4,8]
            },
            {
                handId : 1,
                lineId : 5,
                rewardGroup : 1,
                rewardType : 1,
                rewardValue : 800,
                symbolId : 7,
                winningPositions : [2,4,6]
            }],
            specialWins : [{
                rewardGroup : 1,
                rewardType : 2,
                rewardValue : 2,
                winType : 1
            }],
            stickySymbolPositions : [],
        }]
    },

    targetResult :
    {
        totalCreditWon : 4800,
        totalSuperBetWon : 0,
        numFreeGamesWon : 0,
        numFreeSpinsWon : 0,
        
        // TODO: this isnt on the spec, at the moment...
        //startSymbolIds : [[[SYM_1,SYM_1,SYM_1],[SYM_1,SYM_1,SYM_1],[SYM_1,SYM_1,SYM_1],[SYM_1,SYM_1,SYM_1],[SYM_1,SYM_1,SYM_1]]],
        
        hasBonusWin : false,
        handsWithBonusWin : [],
        rounds : [{
            totalCreditWon : 4800,
            totalSuperBetWon : 0,
            numFreeGamesWon : 0,
            spins : [{
                finalSymbolIds : [[
                    [SYM_7,SYM_7,SYM_7],
                    [SYM_7,SYM_7,SYM_7],
                    [SYM_7,SYM_7,SYM_7],
                    [SYM_7,SYM_2,SYM_1],
                    [SYM_3,SYM_3,SYM_4]
                ]],
                hasBonusWin : false,
                totalCreditWon : 4800,
                totalSuperBetWon : 0,
                numFreeGamesWon : 0,
                numFreeSpinsWon : 0,
                spinType : 1,
                progressiveMultiplier : 1,
                hasAnyWin : true,
                symbolWins : [{
                    handId : 1,
                    lineId : 1,
                    rewardGroup : 1,
                    rewardType : 1,
                    rewardValue : 800,
                    winningSymbol : SYM_7,
                    winningPositions : [2,2,2], //[1,4,7],
                    winningReels : [1,2,3]
                },
                {
                    handId : 1,
                    lineId : 2,
                    rewardGroup : 1,
                    rewardType : 1,
                    rewardValue : 1600,
                    winningSymbol : SYM_7,
                    winningPositions : [3,3,3,3], //[2,5,8,11]
                    winningReels : [1,2,3,4]
                },
                {
                    handId : 1,
                    lineId : 3,
                    rewardGroup : 1,
                    rewardType : 1,
                    rewardValue : 800,
                    winningSymbol : SYM_7,
                    winningPositions : [1,1,1], //[0,3,6]
                    winningReels : [1,2,3]
                },
                {
                    handId : 1,
                    lineId : 4,
                    rewardGroup : 1,
                    rewardType : 1,
                    rewardValue : 800,
                    winningSymbol : SYM_7,
                    winningPositions : [1,2,3], //[0,4,8]
                    winningReels : [1,2,3]
                },
                {
                    handId : 1,
                    lineId : 5,
                    rewardGroup : 1,
                    rewardType : 1,
                    rewardValue : 800,
                    winningSymbol : SYM_7,
                    winningPositions : [3,2,1], //[2,4,6]
                    winningReels : [1,2,3]
                }],
                specialWins : [{
                    rewardGroup : 1,
                    rewardType : 2,
                    rewardValue : 2,
                    winType : 1
                }]
            }]
        }]
    }
}