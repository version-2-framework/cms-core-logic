/**
 * Describes the setup for a single test.
 */
interface Gen2SpinParserTestDescriptor
{
    /**
     * A debug name for this test.
     */
    testId : string;

    /**
     * Slot Config, vital to accurately parsing the data.
     */
    slotConfig : SlotConfig;
}

/**
 * Describes the setup for a parsing test for a Single Spin result.
 */
interface Gen2SpinParserSingleSpinTestDescriptor extends Gen2SpinParserTestDescriptor
{
    /**
     * The server result for a Single Spin Phase, which must be parsed.
     */
    serverResult : Gen2SingleSpinPhaseResult;

    /**
     * The expected outcome of parsing the server result.
     */
    targetResult : SingleSpinPhaseResult;
}