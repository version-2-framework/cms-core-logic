/**
 * @descriptor Unit tests for the Gen 2 Spin Model Parser.
 * @author Russell Wakely
 * 
 * This unit test is very simple: we take an input, parse it, and check the output
 * object against an expected output object. The grunt work for this test, comes
 * from external definitions: we have a Gen2SpinModelTestDescriptor interface, and
 * we can load a list of TestDescriptors that match this interface. We then iterate
 * through them, running the same unit test for each TestDescriptor. The TestDesctiptor
 * instances are stored in separate files (1 to a file), because they can be rather
 * large.
 * 
 * The reason this approach was adopted: testing this class is basically data driven
 * (a TestDesciptor consists of input data, expected output data, and a SlotConfig,
 * that lets the GameClient work out any additional information that it is interested
 * in). To put all this in a single unit test code unit, would make that code unit
 * huge (imagine wanting 20 or more separate Test Descriptors).
 */

import {Gen2SpinResultParser} from '../../src/model/Gen2SpinResultParser';
import {Gen2SpinModelTestDescriptors} from "./Gen2SpinParser.TestDescriptors";

describe("Gen2SpinParser", () => {
    Gen2SpinModelTestDescriptors.forEach(testDescriptor => {
        describe(`for spec ${testDescriptor.testId}`, () => {
            /** @type {Gen2SingleSpinPhaseResult} */
            let serverResult;

            /** @type {SingleSpinPhaseResult} */
            let targetResult;

            /** @type {SingleSpinPhaseResult} */
            let parsedResult;

            beforeEach(() => {
                serverResult = testDescriptor.serverResult;
                targetResult = testDescriptor.targetResult;
                parsedResult = new Gen2SpinResultParser({config:testDescriptor.slotConfig}).parseSingleSpinPhaseResult(serverResult);
            });

            it (`parsedResult should match all expectations`, () => {
                expect(parsedResult).toEqual(targetResult);
            });
        });
    });
});