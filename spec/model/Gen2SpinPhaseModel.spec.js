import Gen2SpinPhaseModel from '../../src/model/Gen2SpinPhaseModel';
import { deepStrictEqual } from 'assert';
import { FpgSlotConfig } from './Gen1SpinModel/FowlPlayGold/FpgSlotConfig';

describe("Gen2SpinPhaseModel", ()=>
{
    /** @type {SpinPhaseModel} */
    let spinPhaseModel;

    beforeEach(()=>
    {
        let deps =
        {
            config : FpgSlotConfig
        }

        spinPhaseModel = new Gen2SpinPhaseModel(deps);
    });

    describe("getFinalSymbolIds", ()=>
    {
        it ("should return a structured SymbolMatrix object", ()=> {
            let finalSymbolIds = spinPhaseModel.getFinalSymbolIds();
            expect(finalSymbolIds.length).toEqual(1);
            expect(finalSymbolIds[0].length).toEqual(5);
            expect(finalSymbolIds[0][0].length).toEqual(3);
            expect(finalSymbolIds[0][1].length).toEqual(3);
            expect(finalSymbolIds[0][2].length).toEqual(3);
            expect(finalSymbolIds[0][3].length).toEqual(3);
            expect(finalSymbolIds[0][4].length).toEqual(3);
        })
    })
});