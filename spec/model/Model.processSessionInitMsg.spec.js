import C_MessageField from "../../src/const/C_MessageField";
import C_MessageType from "../../src/const/C_MessageType";
import Model from "../../src/model/Model";

describe ("Model", ()=> {
    /** @type {Model} */
    let model;

    describe ("processSessionInitMsg()", ()=> {
        const SESSION_ID = "DefJam198x213fB";
        const TICKET_ID = "HereIsATicketID";
        const SESSION_BALANCE = 150000;
        const SESSION_INACTIVITY_TIMEOUT = 15;

        beforeEach(()=>{
            let deps = {};

            let msg =
            {
                [C_MessageField.MESSAGE_TYPE] : C_MessageType.SESSION_INIT_REPLY,
                [C_MessageField.MESSAGE_ID] : "",
                [C_MessageField.SESSION_ID] : SESSION_ID,
                [C_MessageField.TICKET_ID] : TICKET_ID,
                [C_MessageField.SESSION_BALANCE] : SESSION_BALANCE,
                [C_MessageField.SESSION_IS_FREE_PLAY] : false,
                [C_MessageField.SESSION_INACTIVITY_TIMEOUT] : SESSION_INACTIVITY_TIMEOUT
            };

            model = new Model(deps);
            model.processSessionInitMsg(msg);
        });

        it ("sessionInProgress should be true", ()=> {
            expect(model.isSessionInProgress()).toBeTruthy();
        });

        it ("sessionIsFreeplay should be correct value", ()=> {
            expect(model.isSessionFreeplay()).toBeFalsy();
        });

        it ("sessionId should be expected value", ()=> {
            expect(model.getSessionId()).toEqual(SESSION_ID);
        });

        it ("ticketId should be expected value", ()=> {
            expect(model.getTicketId()).toEqual(TICKET_ID);
        });

        it ("ticketProgressive should be 0", ()=> {
            expect(model.getTicketProgressive()).toEqual("0");
        });

        it ("sessionCreditAdded should be correct value", ()=> {
            expect(model.getSessionCreditAdded()).toEqual(SESSION_BALANCE);
        });

        it ("sessionCreditSpent should be 0", ()=> {
            expect(model.getSessionCreditSpent()).toEqual(0);
        });

        it ("sessionNumGamesPlayed should be 0", ()=> {
            expect(model.getSessionNumGamesPlayed()).toEqual(0);
        });

        it ("sessionInactivityTimeout should be correct value", ()=> {
            expect(model.getSessionInactivityTimeout()).toEqual(SESSION_INACTIVITY_TIMEOUT);
        });

        it ("sessionWallet should be correct value", ()=> {
            expect(model.getSessionWallet()).toEqual(SESSION_BALANCE);
        });

        it ("sessionPoints should be 0", ()=> {
            expect(model.getSessionPoints()).toEqual(0);
        });

        it ("playerWallet should be correct value", ()=> {
            expect(model.getPlayerWallet()).toEqual(SESSION_BALANCE);
        });

        it ("playerPoints should be 0", ()=> {
            expect(model.getPlayerPoints()).toEqual(0);
        });

        it ("playerWinnings should be 0", ()=> {
            expect(model.getPlayerWinnings()).toEqual(0);
        });

        it ("autoplayWinnings should be 0", ()=> {
            expect(model.getAutoplayWinnings()).toEqual(0);
        });

        it ("autoplaySpent should be 0", ()=> {
            expect(model.getAutoplaySpent()).toEqual(0);
        });

        it ("autoplayLosses should be 0", ()=> {
            expect(model.getAutoplayLosses()).toEqual(0);
        });

        it ("autoplayNumGamesPlayed should be 0", ()=> {
            expect(model.getAutoplayNumGamesPlayed()).toEqual(0);
        });

        it ("autoplayNumGamesTotal should be 0", ()=> {
            expect(model.getAutoplayNumGamesTotal()).toEqual(0);
        });

        it ("autoplayNumGamesRemaining should be 0", ()=> {
            expect(model.getAutoplayNumGamesRemaining()).toEqual(0);
        });

        it ("isAutoplayInProgress should be false", ()=> {
            expect(model.isAutoplayInProgress()).toBeFalsy();
        });
    });
});