import Model from "../../src/model/Model";
import C_Currency from "../../src/const/C_Currency";
import C_SessionType from "../../src/const/C_SessionType";

describe ("Model", ()=> {
    /** @type {Model} */
    let model;

    let deps;

    beforeEach(()=> {
        deps = {};
        model = new Model(deps);
    });

    describe ("formatCurrency", ()=> {
        beforeEach(()=>{
            model._currency = C_Currency.EURO_ITALY;
        });

        it ("should return the expected value for realplay", ()=> {
            model._sessionType = C_SessionType.REAL_PLAY;
            expect(model.formatCurrency(10580)).toEqual('€105,80');
        });

        it ("should return a formatted value without currency symbol for freeplay", ()=> {
            model._sessionType = C_SessionType.FREE_PLAY;
            expect(model.formatCurrency(10580)).toEqual('105,80');
        });
    });
});