import FpgBonusModel from "../../../src/model/legacy/FpgBonusModel";

describe ("FpgBonusModel", ()=> {
    /** @type {FpgBonusModel} */
    let model;

    beforeEach(()=> {
        model = new FpgBonusModel();
    });

    describe ("setBonusResults()", ()=> {
        describe("after passing in a Bonus Results Reply", ()=> {
            beforeEach(()=>{
                let data =
                {
                    "MTP" : "WMG_BONUS_GET_RESULTS_REPLY",
                    "PZB" : "250",
                    "PZS" : "500",
                    "PZG" : "1500",
                    "PZD" : "2500",
                    "CKP" : "1,1,0,1,0"
                };

                model.setBonusResults(data);
            });

            it ("getPrizeValueLow should return correct value", ()=> {
                expect(model.getPrizeValueLow()).toEqual(250);
            });

            it ("getPrizeValueMedium should return correct value", ()=> {
                expect(model.getPrizeValueMedium()).toEqual(500);
            });

            it ("getPrizeValueHigh should return correct value", ()=> {
                expect(model.getPrizeValueHigh()).toEqual(1500);
            });

            it ("getPrizeValueBigWin should return correct value", ()=> {
                expect(model.getPrizeValueBigWin()).toEqual(2500);
            });

            it ("areResultsAvaulable should return false", ()=> {
                expect(model.areResultsAvailable()).toEqual(false);
            });

            it ("getTotalWinnings should return 0", ()=> {
                expect(model.getTotalWinnings()).toEqual(0);
            });

            it ("getWintype should return empty string", ()=> {
                expect(model.getWintype()).toEqual("");
            });

            it ("getScatterPositions should return correct data", ()=>{
                expect(model.getScatterPositions()).toEqual([1,1,0,1,0]);
            });
        });

        describe ("after passing in a reloaded Bonus Selection Reply", ()=> {
            beforeEach(()=> {
                let data =
                {
                    "MTP" : "WMG_PLAY_BONUS_SELECTION_REPLY",
                    "PZB" : "500",
                    "PZS" : "1000",
                    "PZG" : "5000",
                    "PZD" : "15000",
                    "CKD" : "4",
                    "CKP" : "1,1,1,1,1",
                    "WTP" : "diamond",
                    "RLD" : true
                };

                model.setBonusResults(data);
            });

            it ("getPrizeValueLow should return correct value", ()=> {
                expect (model.getPrizeValueLow()).toEqual(500);
            });

            it ("getPrizeValueMedium should return correct value", ()=> {
                expect (model.getPrizeValueMedium()).toEqual(1000);
            });

            it ("getPrizeValueHigh should return correct value", ()=> {
                expect (model.getPrizeValueHigh()).toEqual(5000);
            });

            it ("getPrizeValueBigWin should return correct value", ()=> {
                expect (model.getPrizeValueBigWin()).toEqual(15000);
            });

            it ("getScatterPositions should return the correct value", ()=> {
                expect (model.getScatterPositions()).toEqual([1,1,1,1,1]);
            });

            it ("getWintype should return the correct value", ()=> {
                expect (model.getWintype()).toEqual("diamond");
            });

            it ("getTotalWinnings should return the correct value", ()=> {
                expect (model.getTotalWinnings()).toEqual(15000);
            });

            it ("areResultsAvailable should return true", ()=> {
                expect (model.areResultsAvailable()).toEqual(true);
            });

            it ("getScatterSelected should return correct value", ()=> {
                expect(model.getScatterSelected()).toEqual(4);
            })
        });
    });
});