//--------------------------------------------------------------------------------------------------
// Unit tests for Parse Util
//--------------------------------------------------------------------------------------------------
// Parse util can be used to read values out of JSON responses (and occasionally, to provide
// utility methods to parse data back to the form required in JSON requests).
//--------------------------------------------------------------------------------------------------
import * as Parse from '../../src/utils/ParseUtil';
import * as C_MessageField from '../../src/const/C_MessageField';
import C_ServerErrorCode from '../../src/const/C_ServerErrorCode';
import * as C_ClientErrorCode from '../../src/const/C_ClientErrorCode';

describe("Parse Util", ()=> {
    describe("parseBoolean", ()=>{
        it ('should treat 1 as true', ()=> {
            expect(Parse.parseBoolean(1)).toEqual(true);
        });
        
        it ('should treat "1" as true', ()=> {
            expect(Parse.parseBoolean("1")).toEqual(true);
        });

        it ('should treate "true" as true', ()=>{
            expect(Parse.parseBoolean("true")).toEqual(true);
        });

        it ('should treate true as true', ()=>{
            expect(Parse.parseBoolean(true)).toEqual(true);
        });

        it ('should treat 0 as false', ()=> {
            expect(Parse.parseBoolean(0)).toEqual(false);
        });
        
        it ('should treat "0" as false', ()=> {
            expect(Parse.parseBoolean("0")).toEqual(false);
        });

        it ('should treate "false" as false', ()=>{
            expect(Parse.parseBoolean("false")).toEqual(false);
        });

        it ('should treate false as false', ()=>{
            expect(Parse.parseBoolean(false)).toEqual(false);
        });
    });
    
    describe ("extractBoolean", ()=>{
        describe("when the requested field is missing", ()=> {
            it ("should return false", ()=> {
                expect(Parse.extractBoolean("field", {})).toEqual(false);
            });
        });

        describe ("when the requested field is present", ()=> {
            it ("should return false if it has a falsey value", ()=> {
                expect(Parse.extractBoolean("field", { field:0 })).toEqual(false);
                expect(Parse.extractBoolean("field", { field:"0" })).toEqual(false);
                expect(Parse.extractBoolean("field", { field:"false" })).toEqual(false);
                expect(Parse.extractBoolean("field", { field:false })).toEqual(false);
            });

            it ("should return true if it has a truthy value", ()=> {
                expect(Parse.extractBoolean("field", { field:1 })).toEqual(true);
                expect(Parse.extractBoolean("field", { field:"1" })).toEqual(true);
                expect(Parse.extractBoolean("field", { field:"true" })).toEqual(true);
                expect(Parse.extractBoolean("field", { field:true })).toEqual(true);
            });
        });
    });

    describe ("extractString", ()=> {
        describe("when the requested field is missing", ()=> {
            it ("should return an empty string", ()=> {
                expect(Parse.extractString("field", {})).toEqual("");
            });
        });

        describe ("when the requested field is present", ()=> {
            it ("should return a raw string value correctly", ()=> {
                expect(Parse.extractString("field", { field:"cheese" })).toEqual("cheese");
            });

            it ("should parse a non-string field to a string", ()=> {
                expect(Parse.extractString("field", { field:1 })).toEqual("1");
                expect(Parse.extractString("field", { field: [1,4] })).toEqual("1,4");
            });
        });
    });

    describe ("extractStringArray", ()=> {
        describe ("when the requested field is missing", ()=> {
            it ("should return an empty array", ()=> {
                expect(Parse.extractStringArray("field", {})).toEqual([]);
            });
        });

        describe ("when the requested field is present", ()=> {
            it ("should return a raw string array correctly", ()=> {
                expect(Parse.extractStringArray("field", { field: ["14", "hold" ]})).toEqual(["14", "hold"]);
            });

            it ("should return a number array as an array of strings", ()=> {
                expect(Parse.extractStringArray("field", { field:[5,6] })).toEqual(["5", "6"]);
            });

            it ("should return a single string value, as a 1 element array", ()=> {
                expect(Parse.extractStringArray("field", { field:"cheese" })).toEqual(["cheese"]);
            });

            it ("should return a single number value, as a 1 element array", ()=> {
                expect(Parse.extractStringArray("field", { field:100 })).toEqual(["100"]);
            });

            it ("should parse an empty field as an empty array", ()=> {
                expect(Parse.extractStringArray("field", { field:"" })).toEqual([]);
            });
        });
    });

    describe ("extractInt", ()=> {
        describe ("when the requested field is missing", ()=> {
            it ("should return 0", ()=> {
                expect(Parse.extractInt("field", {})).toEqual(0);
            });
        });

        describe ("when the requested field is present", ()=> {
            it ("should return a raw integer value correctly", ()=> {
                expect(Parse.extractInt("field", { field:52 })).toEqual(52);
            });

            it ("should correctly parse a string to a number", ()=> {
                expect(Parse.extractInt("field", { field:"14" })).toEqual(14);
            });
        });
    });

    describe ("extractNumber", ()=> {
        describe ("when the requested field is missing", ()=> {
            it ("should return 0", ()=> {
                expect(Parse.extractInt("field", {})).toEqual(0);
            });
        });

        describe ("when the requested field is present", ()=> {
            it ("should return a raw number value correctly", ()=> {
                expect(Parse.extractNumber("field", { field:14.51 } )).toEqual(14.51);
            });

            it ("should correctly parse a string to a number", ()=> {
                expect(Parse.extractNumber("field", { field:"5.6" })).toEqual(5.6);
            });
        });
    });

    describe ("extractNumberArray", ()=> {
        describe ("when the requested field is missing", ()=> {
            it ("should return an empty array", ()=> {
                expect(Parse.extractNumberArray("field", {})).toEqual([]);
            });
        });

        describe ("when the requested field is present", ()=> {
            it ("should parse an empty array, as an empty array", () => {
                expect(Parse.extractNumberArray("field", { field:[] })).toEqual([]);
            });

            it ("should parse an empty string literal, as an empty array", ()=> {
                expect(Parse.extractNumberArray("field", { field:"" })).toEqual([]);
            });

            it ("should parse a single number as an array with 1 element", ()=> {
                expect(Parse.extractNumberArray("field", { field:4 })).toEqual([4]);
            });

            it ("should parse a single string number as a 1 element number array", ()=> {
                expect(Parse.extractNumberArray("field", { field:"52" })).toEqual([52]);
            });

            it ("should parse a string list of numbers, as a number array", ()=> {
                expect(Parse.extractNumberArray("field", { field:"5,0,6" })).toEqual([5,0,6]);
            });

            it ("should parse an actual array of numbers correctly", ()=> {
                expect(Parse.extractNumberArray("field", { field: [15,6] })).toEqual([15,6]);
            });
        });
    });

    describe ("parseHoldsStringArray", ()=> {
        it ("return the correct boolean values for a list of valid holds strings", ()=> {
            let expected = [ true, false, true, true, false ];
            let input = [ "H", "NH", "H", "H", "NH" ];
            expect(Parse.asHoldsArray(input)).toEqual(expected);
        });
    });

    describe("createHoldsStringArray", ()=> {
        it ("should create correct output for an input of boolean", ()=> {
            let input = [ true, false, false, true, true, false ];
            let expected = [ "H", "NH", "NH", "H", "H", "NH" ];
            expect(Parse.createHoldsStringArray(input)).toEqual(expected);
        });

        it ("should create the correct output for an input of numbers", ()=> {
            let input = [ 1, 0, 0, 1 ];
            let expected = [ "H", "NH", "NH", "H" ];
            expect(Parse.createHoldsStringArray(input)).toEqual(expected);
        });

        it ("should create the correct output for an input of holds strings", ()=> {
            let input = [ "NH", "NH", "NH", "NH", "H" ];
            expect(Parse.createHoldsStringArray(input)).toEqual(input);
        })
    });

    describe ("extractHoldsArray", ()=> {
        describe ("when the requested field is missing", ()=> {
            it ("should return an empty array", ()=> {
                expect(Parse.extractHoldsArray("field", {})).toEqual([]);
            });
        })

        describe ("when the requested field is present", ()=> {
            it ("should return the correct values, as an array of booleans", ()=> {
                expect(Parse.extractHoldsArray("field", { field:"H,NH,NH" })).toEqual([true,false,false]);
            });

            it ("should return an array, even if the field's value only had 1 element", ()=> {
                expect(Parse.extractHoldsArray("field", { field:"H" })).toEqual([true]);
            });
        });
    });

    describe("asHoldReelsPattern", () => {
        describe ("when given input data representing single hand", () => {
            it ("should return expected holdsPattern object", () => {
                let outputHoldsPattern = Parse.asHoldReelsPattern("H,NH,H,H,NH", 1, 5);
                expect(outputHoldsPattern).toEqual({
                    type : "holdReels",
                    pattern : [[true,false,true,true,false]]
                });
            });
        });

        describe ("when given input data representing single hand, but multiple hands pattern requested", () => {
            it ("should return correct holdsPattern with input pattern copied for each hand", () => {
                let outputHoldsPattern = Parse.asHoldReelsPattern("H,NH,H,H,NH", 4, 5);
                expect(outputHoldsPattern).toEqual({
                    type : "holdReels",
                    pattern : [
                        [true,false,true,true,false],
                        [true,false,true,true,false],
                        [true,false,true,true,false],
                        [true,false,true,true,false]
                    ]
                });
            });
        });
    });

    describe ("hasServerError", ()=> {
        describe ("when no ERROR field is present in a response", ()=> {
            it ("should return false", ()=> {
                expect(Parse.hasServerError({})).toBeFalsy();
            });
        });

        describe ("when the ERROR field is present", ()=> {
            describe ("and contains the NO_ERROR code", ()=> {
                it ("should return false", ()=> {
                    expect (Parse.hasServerError({
                        [C_MessageField.ERROR] : C_ServerErrorCode.NO_ERROR
                    })).toBeFalsy();
                })
            });

            describe ("and contains any other value", ()=> {
                it ("should return true", ()=> {
                    expect (Parse.hasServerError({
                        [C_MessageField.ERROR] : "bla bla bla"
                    })).toBeTruthy();
                });
            })
        });
    });

    describe ("getClientErrorCodeFromServerError", ()=> {
        describe ("when no ERROR field is present in a response", ()=> {
            it ("should return false", ()=> {
                expect(Parse.getGen1CommsErrorFor({})).toBeFalsy();
            });
        });

        describe ("when the ERROR field is present", ()=> {
            describe ("and contains the NO_ERROR code", ()=> {
                it ("should return false", ()=> {
                    expect(Parse.getGen1CommsErrorFor({
                        [C_MessageField.ERROR] : C_ServerErrorCode.NO_ERROR
                    })).toBeFalsy();
                });
            });

            describe ("and contains a miscellaneous value", ()=> {
                it ("should return the SERVER_RETURNED_GENERIC_ERROR ClientErrorCode", ()=> {
                    expect(Parse.getGen1CommsErrorFor({
                        [C_MessageField.ERROR] : "blablabla"
                    })).toEqual({
                        code : "SR00",
                        isFatal : true,
                        description : "Unknown server side error reported: (blablabla)"
                    });
                        //C_ClientErrorCode.SERVER_RETURNED_GENERIC_ERROR);
                });
            });
        });
    });
});