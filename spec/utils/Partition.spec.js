import { CmsRng } from "../../src/maths/CmsRng";
import { Partitioner } from "../../src/utils/Partition";

describe("Partition", () => {
    describe ("BellPartitioner", () => {
        let partitioner = new Partitioner(new CmsRng());

        describe("partitionValueIntoMaximumNValues", () => {
            describe("when asked to partition into exactly 1 value", () => {
                it ("should return an array with 1 value exactly equal to the whole amount", () => {
                    expect(partitioner.partitionValue(407, 1)).toEqual([407]);
                });
            });

            describe("when asked to partition an amount (X) into any arbitrary number of values (Y)", () => {
                let totalAmount = 501;
                let numPartitionValues = [400]; // [1,4,17,26,100,400,605,1000];

                numPartitionValues.forEach(numPartitionsRequested => {
                    let partitionedValues = partitioner.partitionValue(totalAmount, numPartitionsRequested);

                    if (totalAmount >= numPartitionsRequested) {
                        describe ("and X >= Y", () => {
                            it ("the list of partioned values should have the length requested", () => {
                                expect(partitionedValues.length).toEqual(numPartitionsRequested);
                            });
                        });
                    }
                    else
                    {
                        describe ("and X < 7", () => {
                            it ("the list of partitioned values should have a length shorter than requested", () => {
                                expect(partitionedValues.length).toBeLessThan(numPartitionsRequested);
                            });
                        });
                    }

                    it ("the sum of all partitioned values must equal the value of X", () => {
                        let sumOfPartitionedValues = partitionedValues.reduce((a,b)=>a+b);
                        expect(sumOfPartitionedValues).toEqual(totalAmount);
                    });

                    it ("every value in the output array must be a whole integer", () => {
                        let allValuesAreWholeInteger = true;
                        partitionedValues.forEach(value => {
                            let isWholeInteger = value === Math.round(value);
                            if (!isWholeInteger) {
                                allValuesAreWholeInteger = false;
                            }
                        });

                        expect(allValuesAreWholeInteger).toBeTruthy(`All values should be whole integers, but got ${partitionedValues}`);
                    });

                    it ("every value in output array must be larger than or equal to 1", () => {
                        let allValuesGreaterThanOrEqualToOne = true;
                        partitionedValues.forEach(value => {
                            if (value < 1) {
                                allValuesGreaterThanOrEqualToOne = false;
                            }
                        });

                        expect(allValuesGreaterThanOrEqualToOne).toBeTruthy(`All values should be greater or equal to 1, but got ${partitionedValues}`);
                    });
                });
            });
        });
    });
});