//--------------------------------------------------------------------------------------------------
// Unit tests for Stats Util
//--------------------------------------------------------------------------------------------------
// These tests are simple: they verify that the methods return some expected values,
// and handle errors gracefully.
//--------------------------------------------------------------------------------------------------
import * as StatsUtil from '../../src/utils/StatsUtil';

describe("StatsUtil", ()=> {
    describe("getRtp()", ()=> {
        describe("when inputs are larger than 0", ()=>{
            it ("should return the expected value (accurate to 2 decimal places)", ()=> {
                let amountWon = 9750;
                let amountSpent = 10500;
                let expectedRtp = 92.86;
                expect(StatsUtil.getRtp(amountWon, amountSpent)).toBeCloseTo(expectedRtp, 2);
            });
        });

        describe("when amountWon is 0", ()=> {
            it ("should return an RTP of 0", ()=> {
                expect(StatsUtil.getRtp(0, 99000)).toEqual(0);
            });
        });

        describe("when amountSpent is 0", ()=> {
            it ("should return an RTP of 0", ()=> {
                expect(StatsUtil.getRtp(1000, 0)).toEqual(0);
            });
        });
    });

    describe("getRtpString()", ()=> {
        describe("when inputs are larger than 0", ()=>{
            it ("should return the expected value, as a 2dp string", ()=> {
                let amountWon = 9750;
                let amountSpent = 10500;
                let expectedRtp = "92.86%";
                expect(StatsUtil.getRtpString(amountWon, amountSpent)).toEqual(expectedRtp);
            });
        });

        describe("when amountWon is 0", ()=> {
            it ("should return an RTP of 0.00%", ()=> {
                expect(StatsUtil.getRtpString(0, 99000)).toEqual("0.00%");
            });
        });

        describe("when amountSpent is 0", ()=> {
            it ("should return an RTP of 0.00%", ()=> {
                expect(StatsUtil.getRtpString(1000, 0)).toEqual("0.00%");
            });
        });
    });

    describe("getHitrate()", ()=> {
        describe("when inputs are larger than 0", ()=> {
            it ("should return the expected value, accurate to 2dp", ()=> {
                let numEvents = 11;
                let numGames = 50;
                expect(StatsUtil.getHitRate(numEvents, numGames)).toBeCloseTo(4.55, 2);
            });
        });

        describe ("when numEvents is 0", ()=>{
            it ("should return 0", ()=>{
                expect(StatsUtil.getHitRate(0, 500)).toEqual(0);
            });
        })

        describe("when numGames is 0", ()=>{
            it ("should return 0", ()=>{
                expect(StatsUtil.getHitRate(405, 0)).toEqual(0);
            });
        });
    });

    describe("getHitrateString()", ()=> {
        describe("when inputs are larger than 0", ()=> {
            it ("should return the expected string rendering of hitrate, as a 2dp string", ()=> {
                let numEvents = 11;
                let numGames = 50;
                expect(StatsUtil.getHitRateString(numEvents, numGames)).toEqual("1 in 4.55");
            });
        });

        describe ("when numEvents is 0", ()=>{
            it ("should return a hitrate string representing 0", ()=>{
                expect(StatsUtil.getHitRateString(0, 500)).toEqual("1 in 0");
            });
        })

        describe("when numGames is 0", ()=>{
            it ("should return a hitrate string representing 0", ()=>{
                expect(StatsUtil.getHitRateString(405, 0)).toEqual("1 in 0");
            });
        });
    });

    describe("getPercentage()", ()=> {
        describe("when inputs are larger than 0", ()=>{
            it ("should return the expected value (accurate to 2 decimal places)", ()=> {
                let amount = 9750;
                let outOf = 10500;
                let expectedPercentage = 92.86;
                expect(StatsUtil.getPercentage(amount, outOf)).toBeCloseTo(expectedPercentage, 2);
            });
        });

        describe("when amount is 0", ()=> {
            it ("should return a percentage of 0", ()=> {
                expect(StatsUtil.getPercentage(0, 99000)).toEqual(0);
            });
        });

        describe("when outOf is 0", ()=> {
            it ("should return a percentage of 0", ()=> {
                expect(StatsUtil.getPercentage(1000, 0)).toEqual(0);
            });
        });
    });

    describe("getPercentageString()", ()=> {
        describe("when inputs are larger than 0", ()=>{
            it ("should return the expected value, as a 2dp string", ()=> {
                let amount = 9750;
                let outOf = 10500;
                let expectedPercentage = "92.86%";
                expect(StatsUtil.getPercentageString(amount, outOf)).toEqual(expectedPercentage);
            });
        });

        describe("when amount is 0", ()=> {
            it ("should return a percentage of 0.00%", ()=> {
                expect(StatsUtil.getPercentageString(0, 99000)).toEqual("0.00%");
            });
        });

        describe("when outOf is 0", ()=> {
            it ("should return a percentage of 0.00%", ()=> {
                expect(StatsUtil.getPercentageString(1000, 0)).toEqual("0.00%");
            });
        });
    });
});