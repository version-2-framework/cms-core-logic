import * as StringUtil from '../../src/utils/StringUtil';
import C_SymbolType from '../../src/const/C_SymbolType';

describe ("StringUtil", ()=> {
    describe ("symbolsMatrixToString", ()=> {
        /** @type {SymbolsMatrix} */
        let symMatrix;
        
        /** @type {String} */
        let result;

        beforeEach(()=> {
            let sym1 = { id:1, name:"Symbol_1", type:C_SymbolType.PRIZE };
            let sym2 = { id:2, name:"Symbol_2", type:C_SymbolType.PRIZE };
            let sym3 = { id:3, name:"Symbol_3", type:C_SymbolType.PRIZE };
            let sym4 = { id:4, name:"Symbol_4", type:C_SymbolType.PRIZE };
            let sym5 = { id:5, name:"Symbol_5", type:C_SymbolType.PRIZE };
            let sym6 = { id:6, name:"Symbol_6", type:C_SymbolType.PRIZE };
            let sym7 = { id:7, name:"Symbol_7", type:C_SymbolType.PRIZE };
            let sym8 = { id:8, name:"Symbol_8", type:C_SymbolType.WILD };
            let sym9 = { id:9, name:"Symbol_9", type:C_SymbolType.PRIZE };
            let sym10 = { id:10, name:"Symbol_10", type:C_SymbolType.SCATTER };

            symMatrix =
            [
                // hand 1
                [
                    [ sym1, sym2,  sym1 ],           // reel 1
                    [ sym3, sym3,  sym4, sym5 ],     // reel 2
                    [ sym9, sym6,  sym7 ],           // reel 3
                    [ sym9, sym10, sym9, sym4 ],     // reel 4
                    [ sym3, sym2,  sym8 ],           // reel 5
                ]
            ];
        });

        describe ("when param long is set to false", ()=> {
            beforeEach(()=> {
                result = StringUtil.symbolsMatrixToString(symMatrix, false);
            });

            it ("should return the expected output string", ()=> {
                let expected = "  1  3  9  9  3\n  2  3  6 10  2\n  1  4  7  9  8\n     5     4   ";

                expect(result).toEqual(expected);
            });
        });
    });
});