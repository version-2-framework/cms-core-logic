import { WmgEncryptor as WmgEncryptor } from "../../src/utils/WmgEncryptor";

describe("WmgEncryptor", () => {
    /**
     * @type {Encryptor}
     */
    let encryptor;

    beforeEach(() => {
        encryptor = new WmgEncryptor();
    })

    describe("encrypt", () => {
        it ("should encrypt a string", () => {
            let encrypted = encryptor.encrypt("hello");
            expect(encrypted).toEqual("O4bJQiAVEW4zzR8l/z2uSA==");
        });
    });

    describe("decrypt", () => {
        it ("should correctly decrypt a string", () => {
            let decrypted = encryptor.decrypt("O4bJQiAVEW4zzR8l/z2uSA==");
            expect(decrypted).toEqual("hello");
        });
    });
});