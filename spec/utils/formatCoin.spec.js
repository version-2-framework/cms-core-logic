//--------------------------------------------------------------------------------------------------
// formatCoin unit tests
//--------------------------------------------------------------------------------------------------
// The following tests take each of the default money format objects, and test a series
// of values for each one. Most similar currencies, will have similar tests.
//--------------------------------------------------------------------------------------------------

import formatCoin from "../../src/currency/formatCoin";
import C_Currency from "../../src/const/C_Currency";

describe ("formatCoin", ()=> {
    describe ("for ITALIAN EURO format", ()=> {
        let format = C_Currency.EURO_ITALY;

        it ("should render a value of 0 correctly", ()=> {
            expect(formatCoin(0, format, true)).toEqual('€0,00');
        });

        it ("should render a value with many thousands correctly", ()=>{
            expect(formatCoin(1229560800, format, true)).toEqual('€12295608,00');
        });

        it ("should render any euro-cents part correctly", ()=> {
            expect(formatCoin(250, format, true)).toEqual('€2,50');
        });

        it ("should render a currency-less value as expected", ()=>{
            expect(formatCoin(1400, format, false)).toEqual('14,00');
        });
    });

    describe ("for UK POUND format", ()=> {
        let format = C_Currency.POUND_UK;

        it ("should render a value of 0 correctly", ()=> {
            expect(formatCoin(0, format, true)).toEqual('£0.00');
        });

        it ("should render a value with many thousands correctly", ()=>{
            expect(formatCoin(1229560800, format, true)).toEqual('£12,295,608.00');
        });

        it ("should render any pennies part correctly", ()=> {
            expect(formatCoin(250, format, true)).toEqual('£2.50');
        });

        it ("should render a currency-less value as expected", ()=>{
            expect(formatCoin(1400, format, false)).toEqual('14.00');
        });
    });
});