/**
 * The Build Version of the game.
 */
declare const BUILD_VERSION;

/**
 * The Game Static Version number. This is the version number that is certified by SIQ. This number references the version of the game assets (i.e graphics,fonts and sounds) 
 * that are certified by SIQ. This number should be updated each time there is an update to the game assets.
 */
declare const GAME_STATIC_VERSION

/**
 * Defines all possible BuildTypes: the the build must be one of these.
 */
type BuildType = 'testing' | 'develop' | 'production';

/**
 * The type of build.
 * @see C_BuildType
 */
declare const BUILD_TYPE : BuildType;

// TODO: A better name of this can exist: this is a server integration mechanism.

type BuildTarget = "v1" | "v2";

/**
 * The target of the build. Not all build targets are supported : it very much depends
 * on the game.
 */
declare const BUILD_TARGET : BuildTarget;

type BuildPlatform = "wmg" | "sgdigital";

/**
 * The "platform" of the build. The name of this symbol needs improving: at the moment, it refers
 * to the general platform we run the game on ("wmg (online)", "sgdigital (online)", "powerball4").
 * Whenever we want to completely swap certain module implementations (rather than using a json
 * BusinessConfig file to modify the behaviour, we want to select some strongly different behavior),
 * we can set a buildPlatform. Powerball4 is a good example. SGDigital is an example (although in
 * principle). Potentially, we can later expand businessConfig to specify this value, instead of
 * using a build setting: for now, its definitely a buildSetting.
 * 
 * This constant should ONLY be referenced in the initial start-up module of the game client, where
 * we select which modules to instantiate (the modules themselves
 * implement the behaviour).
 */
declare const BUILD_PLATFORM : BuildPlatform;

/**
 * The core logic version that the build was built with. This should be used only for debug purposes
 * (logging info about the build on start-up)
 */
declare const CORE_LOGIC_VERSION : string;

/**
 * The core view version that the build was built with. This should be used only for debug purposes
 * (logging info about the build on start-up)
 */
declare const CORE_VIEW_VERSION : string;

/**
 * An Object which describes the current deployed build. The values exposed here, are ones
 * which will be hard-coded into the build itself.
 */
interface BuildInfoApi
{
    /**
     * Prints a readable string, representing the Build Version of this game client.
     */
    getBuildVersion () : string;

    /**
     * Prints a readable string, representation the version number for the game clients static asset files. 
     */
    getGameStaticVersion() : string;

    /**
     * Returns the integer enum value of the Build Type for this game client.
     */
    getBuildType() : BuildType;

    /**
     * Returns the Build Target (ie: the underlying platform - v1 or v2) that this game client is for.
     */
    getBuildTarget() : BuildTarget;

    /**
     * Returns the Build Platform (eg: WMG, SGDigital) that the game client is for.
     */
    getBuildPlatform() : BuildPlatform;

    /**
     * Indicates if this is a release build.
     */
    isRelease() : boolean;

    /**
     * Returns the Client Config value (eg: production / staging / qa). This is not buildinfo-
     * so, i think BuildInfo needs to evolve a bit (and probably needs its name changing)
     */
    getClientConfig() : ClientConfigValue;

    getClientConfigReadableName() : string;
}