import C_SymbolType from "./const/C_SymbolType";
import C_SymbolFadeType from "../../cms-core-view/src/const/C_SymbolFadeType";
import MessageBar from "../../cms-core-view/src/views/MessageBar";

class Config
{
    constructor()
    {
        /**
         * A readable public name for this game client. Can be used when a game name needs to be
         * inserted into localization text strings displayed within the Game Client.
         * @public
         * @type {string}
         */
        this.gameName = "Default Game Name";

        /**
         * String id of the game. This information will be sent to the server when requesting
         * information. The value should be uniquely set for each new game.
         * @public
         * @type {string}
         */
        this.gameId = "defaultgameid";

        // TODO: Improve this doc. In what contexts, and why, do we need to do this via a config
        // setting ?
        /**
         * A list of languages to load.
         * @public
         * @type {string[]}
         */
        this.languages = ["en", "it"];

        /**
         * The number of reels on each hand in the game (for multi-hand games, this will always
         * be the same)
         * @public
         * @type {number}
         */
        this.numReels = 5;

        /**
         * The HoldsMode activate for the game client. Only relevant when the game is two spin,
         * this indicates the manner in which the player may change their holds pattern.
         * @public
         * @type {"holdReels" | "holdSymbols"}
         */
        this.holdsMode = "holdReels";

        // TODO: The comment "Symbols per Reel" (its a Wiener comment) indicates that this field
        // is either redundant, or incorrectly named. I suspect its redundant, because we have the
        // "numSymbolsPerReel" array to indicate the height of the reel matrix on screen.
        /**
         * "Symbols per reel"
         * @type{number}
         */
        this.numVisibleLines = 3;

        this.defaultMeasureText = "|MÉq";
        this.defaultMeasureBaseline = "M";
        this.defaultFonts =
        [
            {name:"Arial", url: "arial.ttf", format: "truetype", measureText: "|MÉq", measureBaseline: "M"},
            {name:"Comic", url: "comic.ttf", format: "truetype", measureText: "|MÉq", measureBaseline: "M"}
        ];

        /**
         * Default colour that will be used for textfields (when created by the assets.getTF factory method),
         * when no custom colour has been specified.
         * @public
         * @type {number | string}
         */
        this.defaultFontColor = 0xFFFFFF;

        /**
         * Colour (or array of colours, for a gradient) that will be used on the "Game Mode" buttons on the loading
         * screen. Game mode button is rendered using the game specific font.
         * @public
         * @type {number | number[] | string | string[]}
         */
        this.gameModeBtnColours = ["#ff9a12", "#fffc11"];
        this.gameModeBtnHoverColour = ["#ff9a12", "#fffc11"];
        this.gameModeBtnStrokeColour = {strokeColor: '#000000', strokeSize: 1};

        /**
         * Text style for the Loading Screen Message text.
         * @public
         * @type {GuiLoadingScreenTextStyle}
         */
        this.loadingScreenTextStyle =
        {
            fill : ["#ff9a12", "#fffc11"],
            fontWeight : "bold",
            strokeSize: 1,
            strokeColor: 0x000000
        };

        /**
         * Id of the art asset that is shown for a game logo, in the Session Stats view.
         * @public
         * @type {string}
         */
        this.sessionGameLogo = 'title_inGame';

        /**
         * Id of the art asset that is shown for a game logo, in the Loading Screen view.
         * @public
         * @type {string}
         */
        this.loadingScreenGameLogo = 'game_logo';

        /**
         * Configuration data for hints on the loadi
         * @public
         * @type {LoadingScreenHintConfig[]}
         */
        this.loadingScreenHintConfigs = null;

        // TODO: this shouldn't be in root config, as it doesn't really have anything
        // to do with a standard game. It's also unnecessary (as we use a custom
        // class for the ReelFrame in all games, which should simply know how to draw
        // its own graphics) - this field is not used (or should not be used) by any
        // class outside of the ReelFrame, so the field isn't doing anything useful

        // "For optimization purposes, sometimes the frame asset is being cut in multiple parts to fit better in the atlas."
        // "Here you can specify the number of parts.""
        // "IMPORTANT, the frame has to be named reels_foreground_0.png"
        this.reelFrameNumParts = 1;


        // Size of padding for text fields (relative width)
        // It needs to be different from 0 only for fonts that get cropped due to wrong measurement
        // if some char gets cropped just add it to text property...
        this.tfPadding = 0;
        
        /**
         * If true, this will be a 2 spin game.
         * @public
         * @type {boolean}
         */
        this.isTwoSpin = false;

        /**
         * If set to true this will bypass the the two spin phase
         * @public
         * @type {boolean}
         */
        this.isQuickSpin = false;



        /**
         * Indicates the maximum number of hands available in this game.
         * @public
         * @type {number}
         */
        this.numHands = 1;

        /**
         * Indicates the number of symbols on each reel (and the total number of
         * reels per hand)
         * @public
         * @type {number[]}
         */
        this.numSymsPerReel = [3, 3, 3, 3, 3];

        /**
         * Configuration for all of the symbols in the game.
         * @public
         * @type {{ [id:string]:SymbolConfig }}
         */
        this.symbols =
        {
              [1] : { id:1, name:"sym1", type:C_SymbolType.PRIZE },
              [2] : { id:2, name:"sym2", type:C_SymbolType.PRIZE },
              [3] : { id:3, name:"sym3", type:C_SymbolType.PRIZE },
              [4] : { id:4, name:"sym4", type:C_SymbolType.PRIZE },
              [5] : { id:5, name:"sym5", type:C_SymbolType.PRIZE },
              [6] : { id:6, name:"sym6", type:C_SymbolType.PRIZE },
              [7] : { id:7, name:"sym7", type:C_SymbolType.PRIZE },
              [8] : { id:8, name:"sym8", type:C_SymbolType.PRIZE },
              [9] : { id:9, name:"sym9", type:C_SymbolType.PRIZE },
              [10] : { id:10, name:"sym10", type:C_SymbolType.WILD }
        };

        /**
         * Defines how symbols are animated and behave. The default configuration sets up almost
         * nothing: it really is a requirement that you specify your required symbol behaviours
         * in detail for a new game client.
         * @public
         * @type {SymbolsConfiguration}
         */
        this.symbolsConfig =
        {
            defaultScale : 100,
            defaultAnimScale : 100,
            symbolFade : { type:"ALPHA", alpha:0.1 },
            useBlur : true,
            blurVerticalScale : 1,
            perSymbol : {},
            tallSymMaxNumSym : 3
        };

        /**
         * The main Winlines Configuration for the game. If the linePoints fields are set, then
         * custom positioning for the winline rendered on screen can be added. Otherwise, the
         * Winlines view will generate a default, simple rendering of each winline (where the
         * lines will likely overlap). The customized data is useful for the cases where you
         * want to show multiple line highlights simultaneously.
         * @public
         * @type {{[id:number] : GraphicalWinlineConfig }}
         */
        this.winlines = null;

        /**
         * The main Bet Settings configuration for the game.
         * @public
         * @type {BetSettingsConfig}
         */
        this.betConfig =
        {
            numHands :
            {
                minValue : 1,
                maxValue : 1,
                isEnumerated : false
            },
            numLines :
            {
                minValue : 10,
                maxValue : 10,
                isEnumerated : false
            },
            betPerLine :
            {
                isEnumerated : true,
                values : [1,5,10,20,30,40,50,100]
            }
        };

        /**
         * Indicates if Winnings Replay is supposed to be supported for this Game Client. This must
         * match the underlying value for the Game Maths Engine that is being used.
         * @public
         * @type {boolean}
         */
        this.allowWinningsReplay = false;

        /**
         * Indicates if the Value Texts shown at the bottom of the GUI, should include a Superbet
         * field. All game clients will work if a Superbet win is shown, however: we don't want to
         * show the Superbet field by default on a Game Client which uses a maths that doesn't
         * support superbet. Setting this field to true allows us to permanently add the Superbet
         * text to the bottom bar.
         * @public
         * @type {boolean}
         */
        this.permanentlyShowSuperbetField = false;

        // TODO: Is this used ? If so, it should be moved into the new SpinConfig object
        this.symAnimPerReelDelay = 0;

        // TODO: This variable name is not clear enough for my taste, but it's used in other
        // places (other games), so will need agreement before any change to the name gets
        // pushed.
        /**
         * A customizable delay that can be added to each reel stopping. When ReelGroup is told to
         * stop reels spinning, for each reel it will first trigger the "preReelStop" event, which
         * allows other layers to show timed animations which relate to the reel being stopped. For
         * example, we can show special symbols appearing above the reel at X seconds before the
         * actual "reel stop" for that reel begins.
         * @public
         * @type {number}
         */
        this.preReelStopTriggerDelay = 0;

        // TODO: I suspect all of these are long since redundant
        this.betTitleSizeLimits = {width: 12, height: 3.5};
        this.coinTitleSizeLimits = {width: 12, height: 3.5};
        this.linesTFLimits = {width: 6, height: 5};

        // logo data:
        //  -> String for texture id
        //  -> Object for AnimatedSprite - {prefix, padLen, startNum, endNum, animationSpeed}
        this.logo = "gfx_logo";

        // TODO: This actually makes sense, if we wanted to show a "FreeSpins" indicator in the place
        // of the game logo. However, that's not how we are doing it. Confirm this detail, remove this
        // field if its redundant.
        this.hideLogoForFreespins = true;

        // TODO: This looks redundant
        this.winLineTails = true;

        // TODO: Clearly this needs removing, but i will double check its not used anywhere first
        this.hasFreespins = true;

        this.coinValueBounds = {width: 6, height: 6};
        this.betMultiBounds = {width: 6, height: 6};
        

        // TODO: Confirm that this is actually used
        /**
         * The tint to which symbols fade when they are not winning during win line highlight
         * @public
         * @type {string | number}
         */
        this.symbolTintColor = 0xffffff;

        /**
         * Vertical offsets for each reel on the primary hand. This is usefull, if we need to show
         * variable numbers of symbols per reel (and we may want to vertically align each reel
         * differently)
         * @public
         * @type {number[]}
         */
        this.reelYOrigins = [0,0,0,0,0];

        /**
         * Duration (in seconds) that a single Special Win presentation should be shown for.
         * @public
         * @type {number}
         */
        this.specialWinDuration = 1.5;

        /**
         * Presents configuration for the spin phase, and its animated sequences.
         * @type {SlotSpinConfig}
         */
        this.spinConfig =
        {
            symbolWinDuration : 1.5,
            idleSymbolWinDuration : 2.0,
            scatterWinDuration : 2.0,
            spinTimings :
            {
                type : "simple",
                reelStartDelayTime : 0.05,
                reelStopDelayTime : 0.6,
                teaseReelStopDelayTime : 0.8,
                minSpinTime : 1.5
            }
        };

        /**
         * Configures the reelbacks.
         * @public
         * @type {SlotReelBacksConfig}
         */
        this.slotReelBacksConfig =
        {
            allowKeyboardInput : true,
            displayMode : "displayHeldAndNotHeld",
            useCrossFade: false
        };

        /**
         * Configures sounds used for the Spin Phase (including slot win presentations)
         * @public
         * @type {ConfigurableSpinSoundConfig}
         */
        this.spinSoundConfig =
        {
            // No sounds configured by default
        };

        /**
         * Configures Spin Win presentations.
         * @public
         * @type {SpinPhasePresentationConfig}
         */
        this.spinWinPresentationConfig =
        {
            bigWin : {
                type:"multiplier",
                multiplier:30
            },
            totalWin : {
                type:"default",
                useWhenSingleWin:false
            },
            symbolWin : {
                type : "showIndividually",
                orderBy : "winValue",
                countUpValue : true
            }
        };

        /**
         * Configuration for this Game Client, for the "Skip Big Win" functionality.
         * @public
         * @type {SkipWinPresentationConfig}
         */
        this.skipWinConfig =
        {
            enabled : true,
            allowSkipBigWin : false
        };

        /**
         * Configuration for the standard Slot Total Winnings Animation. ONLY use this, if you
         * are using one of the default Total Winnings animations from cms-core-view: if you
         * use a custom total winnings animation, just write that animation the way that you
         * want it to work.
         * @public
         * @type {SlotTotalWinAnimationConfig}
         */
        this.totalWinningsConfig = null;

        /**
         * Configuration for the standard Bonus Total Winnings animation. ONLY use this, if you
         * are using one of the default Bonus Total Winnings animations from cms-core-view: if
         * you use a custom total winnings animation, just write that animation the way that you
         * want it to work.
         * @public
         * @type {BonusTotalWinAnimationConfig}
         */
        this.totalWinningsBonusConfig = null;
        
        /**
         * Text style for the Total Winnings Message text.
         * @public
         * @type {SlotWinningsTextStyleConfig}
         */
        this.totalWinningsTextStyle =
        {
            useSizesRelativeToText : false,
            fill : ["#ff9a12", "#fffc11"],
            fontWeight : "bold",
            padding : 0,
            useStroke : true,
            strokeSize: 1.0,
            strokeColor: 0x000000,
            lineJoin : "miter",
            miterLimit : 1,
            useDropShadow : true,
            dropShadowColor : 0x000000,
            dropShadowAlpha : 0.3,
            dropShadowDistance : 1.0,
            dropShadowBlur : 1.0
        };

        /**
         * Text style for the Total Winnings Multiplier Message text.
         * @public
         * @type {SlotWinningsTextStyleConfig}
         */
        this.totalWinningsMultiplierTextStyle =
        {
            useSizesRelativeToText: false,
            fill : ["#ff9a12", "#fffc11"],
            fontWeight : "bold",
            padding : 0,
            useStroke : true,
            strokeSize: 1.0,
            strokeColor: 0x000000,
            lineJoin : "miter",
            miterLimit : 1,
            useDropShadow : true,
            dropShadowColor : 0x000000,
            dropShadowAlpha : 0.3,
            dropShadowDistance : 1.0,
            dropShadowBlur : 1.0
        };

        /**
         * Duration (in seconds) of the Big Win animation.
         * @public
         * @type {number}
         */
        this.bigWinDuration = 7;

        /**
         * Style configuration for the Game Menu view. These are mostly colour properties: there
         * are a lot of colour properties that we can change, allowing us to adjust the menu to match
         * the colour style of a particular game if required. However, leaving default standard
         * colours is perfectly acceptable (default colour scheme is greyscale)
         * @public
         * @type {GuiGameMenuStyle}
         */
        this.gameMenuStyle =
        {
            headerTextColour : 0xFFFFFF,
            textColour : 0xFFFFFF,
            titleTextColour : 0xFFFF00,
            secondaryTextColour : 0x888888,
            bgColour : 0x0f0f0f,
            headerFooterColour : 0x343434,
            btnColour : 0x454545,
            btnColourHighlight : 0x565656,
            toggleBtnBgColour : 0x232323,
            btnTextColour : 0xFFFFFF,
            toggleBtnTextColour : 0xFFFFFF,
            winlineWinningColour : 0xFFFFFF,
            winlineStandardColour : 0x666666,
            scrollbarBgColour : 0x666666,
            scrollbarBtnColour : 0xffffff
        };

        
        /**
         * Style configuration for the Dialog View.
         * @public
         * @type {GuiDialogStyle}
         */
        this.dialogStyle =
        {
            bgColour : 0x0f0f0f,
            btnColour : 0x343434,
            dialogHeight : 50,
            dialogAspect : 1.77, // eg: 16:9
            textHeight : 6,
            headerHeight : 8,
            footerHeight : 12,
            btnHeight : 10,
            font : "Arial",
            headerTxtColour: 0xffffff,
            bodyTxtColour: 0xffffff,
            buttonTxtColour : 0xffffff
        };

        /**
         * Style settings for the ui.
         * @public
         */
        this.uiStyle =
        {
            /**
             * Id of the font being used for ui.
             * @public
             * @type {string}
             */
            uiFont : 'Arial',

            /**
             * Id of the texture used for the main game logo within the gui.
             */
            gameLogoTexture : 'title_inGame',

            freeplayLogoTexture : 'wmg_freeplayIndicator_LARGE',

            // Russells note: this property makes no sense. 
            // I think it refers to the "UiStats" object, in which case, it needs refactoring.
            // This positioning is something that should not be getting overridden.
            // "Message Bar dimensions in percentage" - this comment is obviously wrong.
            statsOffsets : [-30, -5, 20, 40], //from the center of each stats bg in percentage

            /**
             * Defines the z order of the 3 game buttons (Autoplay, Spin, Bet) for desktop mode.
             * Later entries in this list will be placed above earlier entries. For example, place
             * "spin" as index 2, for it to be rendered above the bet and autoplay buttons (this
             * is the default behaviour)
             * @public
             * @type string[]
             */
            desktopGameButtonsZOrder : ["auto", "bet", "spin"],

            /**
             * Indicates if the message bar should be placed above the game buttons, for desktop ui.
             * @public
             * @type {boolean}
             */
            desktopPlaceMessageBarAboveGameButtons : false,

            /**
             * @type {TooltipStyle}
             */
            tooltip :
            {
                bgColour : 0x000000,
                textColour : 0xffffff,
                lineColour : 0xffff00
            },

            /**
             * @type {GuiUiStatsStyle}
             */
            uiStats :
            {
                clockColor : 0xfafa00,
                titleColor : 0xffffff
            },

            /**
             * Configures properties for the visual style of the ui stats background.
             * @type {GuiUiStatsBgStyle}
             */
            uiStatsBg :
            {
                lineThickness: 0.5,
                lineColor:0x006db3,
                lineAlpha:1,
                bgColor:0x000000,
                bgAlpha:1,

                // TODO: These want moving out
                w:100,
                h:4.5,
                angle:15,
                centerOffset:0,
                bottomOffset:0
            },

            /**
             * Style properties for the message bar.
             * @public
             * @type {GuiMessageBarStyle}
             */
            messageBar :
            {
                font : "Arial",
                fontColor : 0xFFFFFF,
                fontWeight : "normal",
                useBackground : false,
                useStroke : true,
                strokeColor : 0x320000,
                strokeSize : 0.80,
                miterLimit : 1,
                bgColor : 0x000000,
                bgAlpha : 0,
                lineThickness : 0.3,
                lineColour : 0x000000,
                angle:15,
                textScaleFactor : 0.75
            }
        };

        this.maxFullScreenHandShown = 3;

        
        /**
         * The expected width of a symbol, within Reel-Group space (in pixels)
         * @public
         * @type {number}
         */
        this.symWidth = 240;

        /**
         * The expect height of a symbol, within Reel-Group space (in pixels)
         * @public
         * @type {number}
         */
        this.symHeight = 240;

        /**
         * The width of any reels separator that may be shown on reelbacks (or other equivalent default
         * components). 
         * @public
         * @type {number}
         */
        this.reelsSeparatorWidth = 5;

        /**
         * Indicates if a Reels Bg graphic should be shown.
         * @public
         * @type {boolean}
         */
        this.showReelsBg = false;

        /**
         * X position (in pixels) of the reels bg graphic. The ReelsBg is positioned as part of
         * the statically laid out reels view component.
         * @public
         * @type {number}
         */
        this.reelsBgPosX = 0;

        /**
         * Y position (in pixels) of the reels bg graphic. The ReelsBg is positioned as part of
         * the statically laid out reels view component.
         * @public
         * @type {number}
         */
        this.reelsBgPosY = 0;

        /**
         * Width (in pixels) that the reels bg graphic should be set to. The ReelsBg is positioned as
         * part of the statically laid out reels view component.
         * @public
         * @type {number}
         */
        this.reelsBgWidth = 1600;

        /**
         * Height (in pixels) that the reels bg graphic should be set to. The ReelsBg is positioned as
         * part of the statically laid out reels view component.
         * @public
         * @type {number}
         */
        this.reelsBgHeight = 900;
        
        /**
         * The total width (in pixels) that the statically laid out content is expected to be.
         * This value is used for horizontal scaling on-screen: if the actual pixel width of
         * any display objects might be larger than this (for example, some content may be pushed
         * to the left or right of our core area, or may be masked), then this value is critical
         * for measuring the intended width.
         * @public
         * @type {number}
         */
        this.reelsWidth = 1600;
        
        /**
         * The total height (in pixels) that the statically laid out content is expected to be.
         * This value is used for horizontal scaling on-screen: if the actual pixel height of
         * any display objects might be larger than this (for example, some content may be pushed
         * above or below our core area, or may be masked), then this value is critical for
         * measuring the intended height.
         * @public
         * @type {number}
         */
        this.reelsHeight = 900;

        /**
         * The x position (in pixels) within the statically laid out reels content, that should be
         * treated as our anchor position. When we try and position the reelsGroup on screen, and
         * target a specific position in screenspace, this is horizontal position within reelsGroup
         * which will be placed upon that position. For example, if reelsWidth is 1600, and you want
         * the horizontal anchor to fall at 50%, then you would set a value of 800.
         * @public
         * @type {number}
         */
        this.reelsAnchorPosX = 800;

        /**
         * The y position (in pixels) within the statically laid out reels content, that should be
         * treated as our anchor position. When we try and position the reelsGroup on screen, and
         * target a specific position in screenspace, this is vertical position within reelsGroup
         * which will be placed upon that position. For example, if reelsHeight is 900, and you want
         * the vertical anchor to fall at 50%, then you would set a value of 450.
         * @public
         * @type {number}
         */
        this.reelsAnchorPosY = 450;

        /**
         * X position (in pixels) which the reels start from. This is not a coordinate in screen
         * space: instead, it is a coordinate within the Reels view from which we start drawing
         * the reels. Its primary purpose is to allow us to add some offset, dedicated for any
         * reels frame (or winline numers which we may wish to show to the left of the reels).
         * It is valid for this value to be 0.
         * @public
         * @type {number}
         */
        this.reelsPosX = 10;

        /**
         * Y position (in pixels) which the reels start from. This is not a coordinate in screen
         * space: instead, it is a coordinate within the Reels view from which we start drawing
         * the reels. Its primary purpose is to allow us to add some offset, dedicated for any
         * reels frame. It is valid for this value to be 0.
         * @public
         * @type {number}
         */
        this.reelsPosY = 10;

        /**
         * Additional padding (in pixels) between the top edge of the reelsGroup, and the top edge
         * of the first (top-most) symbol. It is valid for this padding to be 0.
         * @public
         * @type {number}
         */
        this.reelsMarginTop = 0;

        /**
         * Additional padding (in pixels) between the left edge of the reelsGroup, and the left edge
         * of the first (left-most) symbol. It is valid for this padding to be 0.
         * @public
         * @type {number}
         */
        this.reelsMarginLeft = 0;

        /**
         * Additional padding (in pixels) between the right edge of the reelsGroup, and the right edge
         * of the last (right-most) symbol. It is valid for this padding to be 0.
         * @public
         * @type {number}
         */
        this.reelsMarginRight = 0;

        /**
         * Additional padding (in pixels) between the bottom edge of the reelsGroup, and the bottom edge
         * of the last (bottom-most) symbol. It is valid for this padding to be 0.
         * @public
         * @type {number}
         */
        this.reelsMarginBottom = 0;
        
        /**
         * Horizontal padding (in pixels) between 2 successive reels. This value is only added between
         * reels which are not the first and last reels (it is NOT added to the left and right of the
         * first and final reels).
         * @public
         * @type {number}
         */
        this.reelGap = 30;

        /**
         * The vertical gap, in pixels, between successive symbols on a reel. This gap does not occur
         * before the first (top-most) symbol, or after the last (bottom-most) symbol: it only occurs
         * between 2 successive symbols. It is valid for the gap to be 0.
         * @public
         * @type {number}
         */
        this.symGap = 0;
        
        /**
         * Indicates if a separator between 2 successive reels should be shown (its width will
         * be the value of reelGap).
         * @public
         * @type {boolean}
         */
        this.showReelSeparator = false;

        /**
         * Spin indicator configuration settings for desktop.
         * @public
         * @type {SpinIndicatorConfig}
         */
        this.spinIndicatorDesktop = 
        {
            posX : 800,
            posY : 20,
            height : 60
        };

        /**
         * Spin indicator configuration settings for mobile.
         * @public
         * @type {SpinIndicatorConfig}
         */
        this.spinIndicatorMobile = 
        {
            posX : 800,
            posY : 20,
            height : 60
        };

        /**
         * Height (in pixels) of all winline buttons (either left or right hand-side buttons). This height is a
         * value within the coordinate space of the ReelsGroup (winline buttons are placed within ReelsGroup).
         * @public
         * @type {number}
         */
        this.winLineButHeight = 6;
        
        /**
         * Horizontal position (in pixels) of all winline buttons placed on the left of the reels. This posisition
         * is within the coordinate space of the ReelsGroup (winline buttons are placed within ReelsGroup).
         * @public
         * @type {number}
         */
        this.winlineButPosXLeft = 30;

        /**
         * Horizontal position (in pixels) of all winline buttons placed on the right of the reels. This posisition
         * is within the coordinate space of the ReelsGroup (winline buttons are placed within ReelsGroup).
         * @public
         * @type {number}
         */
        this.winlineButPosXRight = 1570;

        /**
         * Configures sounds played from standard GUI buttons.
         * @public
         */
        this.uiSounds =
        {
            /**
             * Configures the sounds which will be played for the desktop GUI (for any form of the desktop GUI)
             * @type {GuiSoundsConfig}
             */
            desktop :
            {
                guiButtonSound : "btn",
                guiButtonVolume : 1,
                spinButtonSound : "btn",
                spinButtonVolume : 1,
                betButtonSound : "btn",
                betButtonVolume : 1,
                autoplayButtonSound : "btn",
                autoplayButtonVolume : 1
            },

            /**
             * Configures the sounds which will be played for the mobile GUI (whether on mobile or a table)
             * @type {GuiSoundsConfig}
             */
            mobile :
            {
                guiButtonSound : "btn",
                guiButtonVolume : 1,
                spinButtonSound : "btn",
                spinButtonVolume : 1,
                betButtonSound : "btn",
                betButtonVolume : 1,
                autoplayButtonSound : "btn",
                autoplayButtonVolume : 1
            }
        };

        // TODO: The actual settings available for configurating layout probably want
        // deprecating (and then the documentation needs updating) - it makes sense to
        // do this as part of the SG Digital changes, because its in doing these changes
        // that the actual use case should become clearer.
        /**
         * Defines layout for the standard Wmg Desktop Slot Gui. The properties expressed here
         * are those which may be overriden for a new game client.
         * @public
         * @type {GuiLayoutConfig}
         */
        this.uiLayoutDesktop =
        {
            gameLogoHeight : 8,
            gameLogoCenterPosX : 50,
            gameLogoCenterPosY : 5,
            reelsPos : { x:50, y:50, height:70 },
            spinButtonPos : { x:50, y:82, height:10 },
            autoplayButtonPos : { x:40, y:82, height:10 },
            betButtonPos : { x:60, y:82, height:10 },
            uiStats : { width:100, height:4.5, centerOffset:0, bottomOffset:0 },
            messageBar : { width:60, height:5.0, centerOffset:0, bottomOffset:5.0 }
        };

        /**
         * Defines layout for the Sg Digital Desktop Slot Gui. The properties expressed here
         * are those which may be overriden for a new game client.
         * @public
         * @type {GuiLayoutConfig}
         */
        this.uiLayoutDesktopSgd =
        {
            gameLogoHeight : 8,
            gameLogoCenterPosX : 50,
            gameLogoCenterPosY : 4,
            reelsPos : { x:50, y:50, height:70 },
            spinButtonPos : { x:50, y:82, height:10 },
            autoplayButtonPos : { x:40, y:82, height:10 },
            betButtonPos : { x:60, y:82, height:10 },
            uiStats : { width:100, height:4.5, centerOffset:0, bottomOffset:0 },
            messageBar : { width:60, height:5.5, centerOffset:0, bottomOffset:4.5 }
        };

        /**
         * Defines layout for the standard Wmg Mobile Slot Gui. The properties expressed here
         * are those which may be overriden for a new game client.
         * @public
         * @type {GuiLayoutConfig}
         */
        this.uiLayoutMobile =
        {
            gameLogoHeight : 7,
            gameLogoCenterPosX : 50,
            gameLogoCenterPosY : 3.5,
            reelsPos : { x:50, y:50, height:70 },
            spinButtonPos : { x:94, y:45, height:20 },
            autoplayButtonPos : { x:6, y:30, height:20 },
            betButtonPos : { x:6, y:60, height:20 },
            uiStats : { width:100, height:4.5, centerOffset:0, bottomOffset:0 },
            messageBar : { width:80, height:7, centerOffset:0, bottomOffset:4.5 }
        };

        /**
         * Defines size of the logo on the preloading screen in percents.
         * @public
         * @type {Number}
         */
        this.preloaderLogoHeight = 12;

        /**
         * Specifies how the main game's background music will work. By default, there isn't any.
         * @public
         * @type {MusicConfig}
         */
        this.music = 
        {
            
        }
    }
}


export default Config