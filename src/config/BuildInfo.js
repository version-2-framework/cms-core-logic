import * as C_BuildType from "../const/C_BuildType";
import * as C_ClientConfigValue from "../const/C_ClientConfigValues";

const VALID_BUILD_TYPES = [C_BuildType.TESTING, C_BuildType.DEVELOP, C_BuildType.PRODUCTION];

/**
 * @implements {BuildInfoApi}
 */
export class BuildInfo
{
    /**
     * @param {string} buildVersion 
     * @param {string} gameStaticVersion 
     * @param {BuildType} buildType 
     * @param {BuildTarget} buildTarget
     * @param {BuildPlatform} buildPlatform
     * @param {ClientConfigValue} clientConfigValue
     */
    constructor(buildVersion, gameStaticVersion, buildType, buildTarget, buildPlatform, clientConfigValue) {

        /**
         * @private
         * @type {string}
         */
        this._buildVersion = buildVersion;

        /**
         * @private
         * @type {string}
         */
        this._gameStaticVersion = gameStaticVersion;

        /**
         * @private
         * @type {BuildType}
         */
        this._buildType = VALID_BUILD_TYPES.indexOf(buildType) > -1? buildType : C_BuildType.PRODUCTION;

        /**
         * @private
         * @type {BuildTarget}
         */
        this._buildTarget = buildTarget;

        /**
         * @private
         * @type {BuildPlatform}
         */
        this._buildPlatform = buildPlatform;

        /**
         * @private
         * @type {ClientConfigValue}
         */
        this._clientConfigValue = clientConfigValue;
    };


    /**
     * @public
     * @inheritDoc
     * @return {string}
     */
    getBuildVersion() {
        return this._buildVersion;
    };

    /**
     * @public
     * @inheritDoc
     * @return {string}
     */
    getGameStaticVersion() {
        return this._gameStaticVersion;
    }

    /**
     * @public
     * @inheritDoc
     * @return {BuildType}
     */
    getBuildType() {
        return this._buildType;
    };


    /**
     * @public
     * @inheritDoc
     * @return {BuildPlatform}
     */
    getBuildPlatform() {
        return this._buildPlatform;
    };


    /**
     * @public
     * @inheritDoc
     * @return {boolean}
     */
    isRelease() {
        return this._buildType === C_BuildType.PRODUCTION;
    };


    /**
     * @public
     * @inheritDoc
     * @return {BuildTarget}
     */
    getBuildTarget() {
        return this._buildTarget;
    };


    /**
     * @public
     * @inheritdoc
     * @return {ClientConfigValue}
     */
    getClientConfig() {
        return this._clientConfigValue;
    }


    /**
     * @public
     * @inheritdoc
     * @return {string}
     */
    getClientConfigReadableName() {
        return {
            [C_ClientConfigValue.PRODUCTION] : "production",
            [C_ClientConfigValue.STAGING] : "staging",
            [C_ClientConfigValue.QA] : "qa"
        }[this._clientConfigValue];
    }


    /**
     * @public
     * @return {string}
     */
    toString() {
        return `Build:{version:${this._buildVersion}, game static version:${this._gameStaticVersion}, buildType:${this._buildType},` +
            `buildTarget:${this._buildTarget},buildPlatform:${this._buildPlatform},` +
            `clientConfig:${this.getClientConfigReadableName()}}`;
    };
};