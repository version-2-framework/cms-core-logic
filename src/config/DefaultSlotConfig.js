/// <reference path='../types/types.d.ts'/>

import C_SymbolType from "../const/C_SymbolType";

// Here follows an example of setting up a SlotConfig object.

/**
 * @type {SlotConfig}
 */
class DefaultSlotConfig
{
    constructor()
    {
        /**
         * Indicates if this is a 2 spin slot game.
         * @public
         * @type {boolean}
         */
        this.isTwoSpin = false;

        /**
         * Indicates the maximum number of hands that exists in the game. This will
         * be used for parsing data returned from the server.
         * @public
         * @type {number}
         */
        this.numHands = 1;

        /**
         * For a game with multiple hands available, this array indicates the list of
         * values that may be set for the "NumHands" bet option.
         * @public
         * @type {number[]}
         */
        this.numHandsOptions = [];

        /**
         * The number of reels (per hand). We are currently assuming that we will
         * never want to make a game with variable numbers of reels per hand. This
         * might be an un-safe assumption.
         * @public
         * @type {number}
         */
        this.numReels = 5;

        /**
         * Indicates the number of symbols on each reel-band. We assume that this is
         * constant for all hands available in the game. For most games this will be
         * the same value for each reel, but in the future we are considering to
         * develop games that use different numbers for each reel (iSoftBet are doing
         * this a lot in their recent games circa 2018)
         * @public
         * @type {number[]}
         */
        this.numSymsPerReel = [3,3,3,3,3];

        /**
         * Defines the list of symbols that are available. Each symbol is stored
         * under its numerical id. The value in each position is a configuration
         * object, which tells you about the symbol itself.
         * @public
         * @type {{ [number:string] : SymbolConfig }}
         */
        this.symbols =
        {
            [1] : { id:1, name:"Sym_1", type:C_SymbolType.PRIZE },
            [2] : { id:2, name:"Sym_2", type:C_SymbolType.PRIZE },
            [3] : { id:3, name:"Sym_3", type:C_SymbolType.SCATTER },
            [4] : { id:4, name:"Sym_4", type:C_SymbolType.PRIZE },
            [5] : { id:5, name:"Sym_5", type:C_SymbolType.PRIZE },
            [6] : { id:6, name:"Sym_6", type:C_SymbolType.PRIZE },
            [7] : { id:7, name:"Sym_7", type:C_SymbolType.PRIZE },
            [8] : { id:8, name:"Sym_8", type:C_SymbolType.PRIZE },
            [9] : { id:9, name:"Sym_9", type:C_SymbolType.PRIZE },
            [10] : { id:10, name:"Sym_10", type:C_SymbolType.WILD },
            [11] : { id:11, name:"Sym_11", type:C_SymbolType.PRIZE },
            [12] : { id:12, name:"Sym_12", type:C_SymbolType.PRIZE },

        };

        this.wildSymId = 10;

        /**
         * Defines the symbols for a set of reel-bands. We use numerical id here.
         * Spin Model will map this numerical id, to a value from SYMBOLS, as and
         * when its required.
         * @public
         * @type {number[][]}
         */
        this.reelbands = null;

        /**
         * @public
         * @todo: Would we want winlines defined this way ??
         * @todo: Should some of this stuff come from the "maths" internal module ??
         */
        this.winlines =
        {
            [1]  : { id:1,  positions: [ 1, 1, 1, 1, 1 ] },
            [2]  : { id:2,  positions: [ 0, 0, 0, 0, 0 ] },
            [3]  : { id:3,  positions: [ 2, 2, 2, 2, 2 ] },
            [4]  : { id:4,  positions: [ 0, 1, 2, 1, 0 ] },
            [5]  : { id:5,  positions: [ 2, 1, 0, 1, 2 ] },
            [6]  : { id:6,  positions: [ 1, 0, 0, 0, 1 ] },
            [7]  : { id:7,  positions: [ 1, 2, 2, 2, 1 ] },
            [8]  : { id:8,  positions: [ 2, 1, 1, 1, 2 ] },
            [9]  : { id:9,  positions: [ 0, 1, 1, 1, 0 ] },
            [10] : { id:10, positions: [ 1, 1, 2, 1, 1 ] },
        };
        
        // This would be an attempt to combine all bet options together...
        // it is unlikely to succeed !!
        this.betOptions =
        {
            
        }
    }
}

export default DefaultSlotConfig;

// This could actually come straight from the maths layer... ??
//class FpgConfig extends SlotConfig
//{
//    constructor()
//    {
//        super();
//
//        this.IS_TWO_SPIN = true;
//
//        this.NUM_REELS = 5;
//
//        this.NUM_SYM_PER_REEL = [ 3, 3, 3, 3, 3 ];
//
//        this.SYMBOLS =
//        {
//            1  : { id:1,  name:"corn",      type:C_SymbolType.PRIZE     },
//            2  : { id:2,  name:"rooster",   type:C_SymbolType.PRIZE     },
//            3  : { id:3,  name:"bar",       type:C_SymbolType.PRIZE     },
//            4  : { id:4,  name:"chick",     type:C_SymbolType.PRIZE     },
//            5  : { id:5,  name:"sack",      type:C_SymbolType.PRIZE     },
//            6  : { id:6,  name:"seven",     type:C_SymbolType.PRIZE     },
//            7  : { id:7,  name:"haystack",  type:C_SymbolType.PRIZE     },
//            8  : { id:8,  name:"fox_wild",  type:C_SymbolType.WILD      },
//            9  : { id:9,  name:"egg",       type:C_SymbolType.PRIZE     },
//            10 : { id:10, name:"scatter",   type:C_SymbolType.SCATTER   },
//        };
//
//        this.REELBANDS = 
//        [
//            // reel 1
//            [
//                5,
//                1,
//                // todo: complete the example !!
//            ],
//            // reel 2
//            [
//
//            ],
//            // reel 3
//            [
//
//            ],
//            // reel 4
//            [
//
//            ],
//            // reel 5
//            [
//
//            ]
//        ];
//
//        /**
//         * @todo:
//         * I think there is a case for making this a more sophisticated data object
//         * (which also includes the array), eg: sticking id and name into it as well.
//         */
//        this.WINLINES = 
//        [
//            [ 1, 1, 1, 1, 1 ],
//            [ 0, 0, 0, 0, 0 ],
//            [ 2, 2, 2, 2, 2 ],
//            [ 0, 1, 2, 1, 0 ],
//            [ 2, 1, 0, 1, 2 ],
//            
//        ]
//    }
//}