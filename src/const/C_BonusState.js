/**
 * Indicates the list of States that BonusController may be in.
 */
class C_BonusState
{
    constructor()
    {
        /**
         * State to indicate no bonus is in progress.
         */
        this.NOT_IN_PROGRESS = "NotInProgress";

        /**
         * State to indicate that the Bonus Phase has been suspended
         * (but the actual BaseBonusView may still be on screen, and paused)
         */
        this.SUSPENDED = "Suspended";

        /**
         * State to show introduction to the whole bonus phase.
         * NOTE:
         * I think "scatter win" animation in the SlotView, and
         * "Scatter to Bonus transition" states are something we
         * invoke outside of BaseBonusView / BonusController.
         */
        this.PHASE_INTRO = "PhaseIntro";

        /**
         * Introduction to a new Bonus Round. Some games could skip
         * through this (it would be invoked, but would be a "dummy"
         * state). In other games, we may want to show a start-up
         * animation sequence before letting the player select any
         * thing.
         */
        this.ROUND_INTRO = "RoundIntro";

        /**
         * Round in progress state: this is the only state in which
         * the player can make a bonus selection. An automatic timer
         * is also started in this state (to make a selection for
         * the player, if they take too long).
         */
        this.ROUND_IN_PROGRESS = "RoundInProgress";

        /**
         * Shows the results of the last round to the player. In the
         * background, we send the "BonusSelectionRequest" to the 
         * server.
         */
        this.ROUND_RESULTS = "RoundResults";

        /**
         * POSSIBLE Round outro state (I haven't actually used this in
         * the Controller yet).
         */
        this.ROUND_OUTRO = "RoundOutro";

        /**
         * This state is used specifically for a transition from one level
         * to the next (in Haunted House we scroll up to the next level, in
         * Fowl Play London we change to the next room).
         */
        this.START_NEW_LEVEL = "StartNewLevel";

        /**
         * Additional state used within certain games.
         */
        this.WAIT_FOR_RESULTS = "WaitForResults";

        /**
         * Shows the total results from the Bonus Phase (total winnings,
         * multipliers) to the player. Behind the scenes, we send the
         * "Bonus Phase Complete" message to the server. We enter this state
         * when all rounds are played.
         */
        this.PHASE_RESULTS = "PhaseResults";

        /**
         * Bonus Outro sequence, shown after PhaseResults. NOTE: initially,
         * I do not propose that we include the "Bonus to Slot" animated
         * transition in this state (I consider this a seperate action
         * performed at the level of the main GameController).
         */
        this.PHASE_OUTRO = "PhaseOutro";
    }
}

export default (new C_BonusState())