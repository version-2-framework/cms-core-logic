/**
 * @type {BuildPlatform}
 */
export const WMG = "wmg";

/**
 * @type {BuildPlatform}
 */
export const SG_DIGITAL = "sgdigital";