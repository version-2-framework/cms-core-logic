/**
 * @type {BuildTarget}
 */
export const V1 = "v1";

/**
 * @type {BuildTarget}
 */
export const V2 = "v2";