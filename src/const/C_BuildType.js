/**
 * @type {BuildType}
 */
export const DEVELOP = "develop";

/**
 * @type {BuildType}
 */
export const TESTING = "testing";

/**
 * @type {BuildType}
 */
export const PRODUCTION = "production";