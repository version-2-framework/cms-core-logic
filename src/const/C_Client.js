// TODO: This is completely redundant for WMG games.
/**
 * Client constants , used for communication with sever
 */
class C_Client
{
    constructor()
    {
        this.GET_LIMITS = "get_limits";
        this.GET_PAYTABLE = "get_paytable";
        this.ACTIVATE_PRELOADER_PLAY = "refresh";
        this.SPIN = "spin";
        this.BONUS = "bonus";
    }
}

export default (new C_Client);