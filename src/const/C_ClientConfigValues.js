/**
 * The value that will be passed for the "config" url param on V2 game clients,
 * to indicate that the Game Client is deployed in a production environment.
 * @type {ClientConfigValue}
 */
export const PRODUCTION = 1;

/**
 * The value that will be passed for the "config" url param on V2 game clients,
 * to indicate that the Game Client is deployed in a staging environment.
 * @type {ClientConfigValue}
 */
export const STAGING = 2;

/**
 * The value that will be passed for the "config" url param on V2 game clients,
 * to indicate that the Game Client is deployed in a qa environment.
 * @type {ClientConfigValue}
 */
export const QA = 3;