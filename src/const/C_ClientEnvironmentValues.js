/**
 * Value indicates that the client environment variable is not applicable in this case.
 */
export const NOT_APPLICABLE = 0;

/**
 * Value indicates that the game client is being run on a desktop computer/device.
 */
export const DESKTOP = 1;

/**
 * Value indicates that the game client is being run on a mobile device.
 */
export const MOBILE = 2;

/**
 * Value indicates that the game client is being run in a mobile application via a web-view.
 */
export const MOBILE_APP = 3;