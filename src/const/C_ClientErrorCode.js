/**
 * Lists all Error Codes that we may show to the player, and their meaning.
 * 
 * We use error codes in the Error popup, so as not to alarm the player ( by telling 
 * them specific information, that we don't really want them to know ). Instead we
 * add a code, to assist with debug ( a little visual hint of what the cause of the
 * error was ). These error codes are NOT the same as any error code that may be
 * returned by the Server in a message ( see C_ServerErrorCodes.js for information on
 * these: in fact, the Server Error Codes are badly documented, and possibly not fully
 * implemented ).
 */

//--------------------------------------------------------------------------------------------------
// Generic error codes
//--------------------------------------------------------------------------------------------------
/**
 * Generic fatal error: this is the code to return, when there really isn't
 * any other useful information available.
 */
export const GENERIC_ERROR = "CL00";
        
//--------------------------------------------------------------------------------------------------
// 10 to 19 - request data errors
//--------------------------------------------------------------------------------------------------
/**
 * 10
 * There was an error preparing request data to be sent to the server.
 */
export const REQUEST_DATA_ERROR = "CL10";
        
/**
 * 11
 * Request data could not be turned into valid JSON.
 */
export const REQUEST_DATA_WAS_MALFORMED = "CL11";
        
/**
 * 12
 * There was an error attempting to encrypt the request data before sending.
 */
export const REQUEST_DATA_COULD_NOT_BE_ENCRYPTED = "CL12";

//--------------------------------------------------------------------------------------------------
// 20 to 29 - request send errors
//--------------------------------------------------------------------------------------------------
/**
 * 20
 * Request send failed error. This is a generic form of this error
 * ( we will set this error code when no further data for the issue is available ).
 */
export const REQUEST_SEND_FAILED = "CL20";

/**
 * 21 - no internet connection
 */
export const REQUEST_SEND_FAILED_BECAUSE_NO_INTERNET = "CL21";

/**
 * 22 - request timed out in some way (server too busy?)
 */
export const REQUEST_SEND_FAILED_BECAUSE_TIMEOUT = "CL22";

//--------------------------------------------------------------------------------------------------
// 30 to 39 - response data errors
//--------------------------------------------------------------------------------------------------
// We got some kind of response from the server, but, for various reasons,
// we cannot use the response (badly formed, cannot decrupt, invalid data, etc)
//--------------------------------------------------------------------------------------------------
/**
 * 30
 * The response from the server was un-usable. This is a generic form of this error
 * ( we will set this error code when no further data for the issue is available ).
 */
export const RESPONSE_UNUSEABLE = "CL30";

/**
 * 31
 * Response could not be decrypted.
 */
export const RESPONSE_COULD_NOT_BE_DECRYPTED = "CL31";

/**
 * 32 
 * The (decrypted) response was malformed (ie: not valid JSON)
 */
export const RESPONSE_DATA_WAS_MALFORMED = "CL32";

//--------------------------------------------------------------------------------------------------
// 40 to 49 - response parsing errors
//--------------------------------------------------------------------------------------------------
// We have a decrypted response, that was valid JSON.
// However, maybe we need to log a specific error that the client determines,
// eg: something was missing in the response data. This was used a lot in the
// flash clients, but is now increasingly deprecated for HTML (we simply put
// confidence in the idea that the data is all there).
//--------------------------------------------------------------------------------------------------
/**
 * 40
 * Response parsing error. This is a generic form of this error
 * ( we will set this error code when no further data for the issue is available ).
 */
export const SERVER_RESPONSE_ERROR = "CL40";

/**
 * 41
 * Response had no message type field.
 */
export const SERVER_RESPONSE_IS_MISSING_MESSAGE_TYPE = "CL41";

/**
 * 42
 * Response type was not supported ( or recognised ) by the Game Client.
 */
export const SERVER_RESPONSE_TYPE_UNSUPPORTED = "CL42";

/**
 * 43
 * Response was missing some expected fields.
 * NOTE:
 * As of the HTML generation of clients, we no longer validate if all expected fields were
 * received. ( However, this is still the error code to show, if we ever did ).
 */
export const SERVER_RESPONSE_WAS_MISSING_FIELDS = "CL43";

/**
 * 44
 * Response type sent by server, was somehow not what the client expected
 */
export const SERVER_RESPONSE_TYPE_UNEXPECTED = "CL44";

//--------------------------------------------------------------------------------------------------
// 50 to 59 - server returned errors
//--------------------------------------------------------------------------------------------------
/**
 * 50
 * Server related errors ( generic ) - the server spat back some kind of error information
 * to us, but in the Game Client, we really don't have a clue what it was saying the problem
 * was.
 */
export const SERVER_RETURNED_GENERIC_ERROR = "CL50";

/**
 * 51
 * Server already processed this request (and we don't have anything else to fall back on)
 */
export const SERVER_ALREADY_PROCESSED_REQUEST = "CL51";

/**
 * 52
 * Server rejected the request ( and this is the only response we have )
 */
export const SERVER_REJECTED_REQUEST = "CL52";

/**
 * 53
 * Server indicates that we reached our credit limit.
 * This *probably* refers to how much credit we may add to a session, however: this is a poor
 * way to return this information ( and generally we improve player experience by informing 
 * them before hand ). Due to lack of server error documentation, it's not currently clear if
 * this is actually what the error means.
 */
export const SERVER_SAYS_CREDIT_LIMIT_REACHED = "CL53"

/**
 * 54
 * The server indicated that the session has been automatically closed.
 */
export const SERVER_CLOSED_SESSION = "CL54";

//--------------------------------------------------------------------------------------------------
// 60 to 69 - Game error codes (possibly due to crappy programming...)
//--------------------------------------------------------------------------------------------------
/**
 * 60
 * Generic game error
 */
export const CLIENT_ERROR = "CL60";

/**
 * When checking to restore a game session, the server returned a message type that
 * is unexpected / unsupported for game restoration.
 */
export const GAME_RESTORATION_MSG_UNSUPPORTED = "CL61";

/**
 * The client is trying to use a game service that is unupported considering the server
 * type that is being used.
 */
export const GAME_SERVICE_UNSUPPORTED = "CL62";