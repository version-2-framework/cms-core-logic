/**
 * General communication constants - methods, paths.
 */
class C_Comm
{
    constructor()
    {
        this.GET = "GET";
        this.POST = "POST";

        const apiVer = 'v1';

        this.GET_LIMITS = "api/json/slots/" + apiVer + "/get-limits";
        this.GET_PAYOUTS = "api/json/slots/" + apiVer + "/get-payouts";
        this.ACTIVATE_PRELOADER_PLAY = "api/json/slots/" + apiVer + "/refresh";
        this.PLACE_BET = "api/json/slots/" + apiVer + "/place-bet";
        this.BONUS = "api/json/bonus-round/" + apiVer + "/bonus";
    }

}

export default (new C_Comm);