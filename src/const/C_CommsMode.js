/**
 * Defines the available modes that Comms may be set to.
 */
class C_CommsMode
{
    constructor()
    {
        /**
         * Indicates that all comms requests are being sent to a remote server.
         */
        this.SERVER = "CommsMode_Server";

        /**
         * Indicates that all comms requests, and being processed with LocalResponseProvider.
         */
        this.LOCAL = "CommsMode_Local";

        /**
         * Indicates that we are in a special "debug" version of Comms ( we can cycle through
         * a preset group of messages, to test a specific set of cases ).
         */
        this.DEBUG = "CommsMode_Debug";
    }
}

export default (new C_CommsMode);