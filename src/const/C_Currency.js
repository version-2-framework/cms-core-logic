///<reference path='../currency/CurrencyFormat.d.ts'/>

/**
 * Standard currency formats.
 */
class C_Currency
{
    constructor()
    {
        /**
         * @public
         * @type {CurrencyFormat}
         */
        this.POINTS = 
        {
            id : "Points",
            useThousands : false,
            thousandsDivider : "",
            useDecimal : false,
            decimalAmount : 0,
            decimalDivider : "",
            currencyCode : "",
            currencyChar : "",
            currencyFirst : false,
            currencySpace : false
        };

        /**
         * @public
         * @type {CurrencyFormat}
         */
        this.EURO_ITALY =
        {
            id : "Italian Euro",
            useThousands : false,
            thousandsDivider : '',
            useDecimal : true,
            decimalAmount : 2,
            decimalDivider : ',',
            currencyCode : "EUR",
            currencyChar : '€',
            currencyFirst : true,
            currencySpace : false
        };

        /**
         * @public
         * @type {CurrencyFormat}
         */
        this.POUND_UK =
        {
            id : "british pound",
            useThousands : true,
            thousandsDivider : ',',
            useDecimal : true,
            decimalAmount : 2,
            decimalDivider : '.',
            currencyCode : 'GBP',
            currencyChar : '£',
            currencyFirst : true,
            currencySpace : false
        };

        /**
         * @public
         * @type {CurrencyFormat}
         */
        this.JAPANESE_YEN = 
        {
            id : "Japanaese Yen",
            useThousands:true,
            thousandsDivider:',',
            useDecimal:false,
            decimalAmount:0,
            decimalDivider:'',
            currencyCode:'JPY',
            currencyChar:'¥',
            currencyFirst:true,
            currencySpace:true
        };
    }
}

export default (new C_Currency);