/**
 * Then constant value of Credit Currency Type (as returned by and sent to, the V2
 * server). This value is also used internally to indicate Credit Currency Type
 * within our code base.
 * @type {CurrencyType}
 */
export const CREDIT = "CREDIT";

/**
 * Then constant value of SueprBet Currency Type (as returned by and sent to, the V2
 * server). This value is also used internally to indicate SuperBet Currency Type
 * within our code base.
 * @type {CurrencyType}
 */
export const SUPER_BET = "SUPER_BET";

/**
 * Then constant value of Winnings Currency Type (as returned by and sent to, the V2
 * server). This value is also used internally to indicate Winnings Currency Type
 * within our code base.
 * @type {CurrencyType}
 */
export const WINNINGS = "WINNINGS";

/**
 * Then constant value of PromoGame Currency Type (as returned by and sent to, the V2
 * server). This value is also used internally to indicate PromoGame Currency Type
 * within our code base.
 * @type {CurrencyType}
 */
export const PROMO_GAME = "PROMO_GAME";