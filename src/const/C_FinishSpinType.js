class C_FinishSpinType
{
    constructor()
    {
        /**
         * The standard "finish spin" sequence.
         */
        this.NORMAL = "FinishSpin_Normal";

        /**
         * Tease a possible winline win, with the maximum number
         * of symbols possible in the game client.
         * 
         * In this routine, the view should show all reels stopping
         * in sequence, and insert a large delay before the final
         * reel is stopped.
         * 
         * On the first spin of a 2 spin game, this kind of win
         * would be sufficient to end the game on spin 1 (with
         * no second spin).
         */
        this.TEASE_MAX_SYMBOL_WINLINE_WIN = "FinishSpin_TeaseMaxSymbolWinlineWin";

        //this.TEASE_MAX_SYMBOL_BONUS_WIN = "FinishSpin_TeaseMaxSymbolBonusWin";

        //this.TEASE_FULL_RACK_WIN = "FinishSpin_TeaseFullRackWin";
    }
}

export default (new C_FinishSpinType());