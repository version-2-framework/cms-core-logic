class C_GameEvent
{
    constructor()
    {
        /**
         * Dispatched when the game resizes.
         */
        this.RESIZE  = "game-resize";

        //------------------------------------------------------------------------------
        // Comms events
        //------------------------------------------------------------------------------
        /**
         * Event dispatched, when any comms request is sent. A CommsRequestInfo object
         * will normally be passed with this event./
         */
        this.COMMS_REQUEST_SENT = "CommsRequestSent";

        //------------------------------------------------------------------------------
        // Session events
        //------------------------------------------------------------------------------
        /**
         * Indicates that a Session has been opened.
         */
        this.SESSION_OPENED = "SessionOpened";

        /**
         * Indicates that a Session has been closed.
         */
        this.SESSION_CLOSED = "SessionClosed";

        /**
         * Indicates that the active ticket for current session has been updated. 
         */
        this.SESSION_TICKET_UPDATED = "SessionTicketUpdated";

        /**
         * Indicates that info about the game session has changed.
         */
        this.SESSION_CHANGED = "SessionChanged";

        /**
         * Event raised, when the Session Inacitivity Timeout has fired.
         * The Game Client must enter an inactive state when this occurs.
         */
        this.SESSION_INACTIVITY_TIMEOUT = "SessionInactivityTimeout";

        //------------------------------------------------------------------------------
        // Controller / state changes
        //------------------------------------------------------------------------------
        /**
         * Indicates that the main Game Controller State has changed.
         */
        this.GAME_STATE_CHANGED = "GameStateChanged";

        this.GAME_STARTED = "GameStarted";
        this.GAME_FINISHED = "GameFinished";

        /**
         * Indicates that the state of BonusController has changed.
         */
        this.BONUS_STATE_CHANGED = "BonusStateChanged";

        /**
         * Proposed fatal error event: this is more for GameController to handle
         * ( because the event could be triggered from any number of places ).
         * To show the "fatal error" popup, this is an action invoked in GameController.
         * NOTE:
         * I am saying that this event might need to change, once the functionality is
         * finalized.
         */
        this.FATAL_ERROR = "FatalError";

        //------------------------------------------------------------------------------
        // The following actions indicate that a button was pressed in the main GUI.
        // These are events that should be dispatched by the View.
        //------------------------------------------------------------------------------
        this.BUTTON_SPIN_PRESSED = "ButtonSpinPressed";
        
        /**
         * Fired on any input action that is a command to open / close the Autoplay Menu.
         */
        this.BUTTON_TOGGLE_AUTOPLAY_MENU_PRESSED = "ButtonToggleAutoplayMenuPressed";

        /**
         * Fired on any input action that is an explicit command to close the Autoplay Menu
         * (eg: can be fired from within the Autoplay Menu itself)
         */
        this.BUTTON_CLOSE_AUTOPLAY_MENU_PRESSED = "ButtonCloseAutoplayMenuPressed";
        
        /**
         * Fired on any input action that is a command to stop any active autoplay session.
         */
        this.BUTTON_STOP_AUTOPLAY_PRESSED = "ButtonStopAutoplayPressed";
        
        /**
         * Fired on any input action that is a command to open / close the Bet Menu.
         */
        this.BUTTON_TOGGLE_BET_MENU_PRESSED = "ButtonToggleBetMenuPressed";

        /**
         * Fired on any input action that is an explicit command to close the Bet Menu (eg:
         * can be fired from within the Bet Menu itself).
         */
        this.BUTTON_CLOSE_BET_MENU_PRESSED = "ButtonCloseBetMenuPressed";

        /**
         * Fired from any input action that is a command to Add Credit to the existing game
         * session. (Generally can open a Cashier, or can automatically execute the Add
         * Credit action)
         */
        this.BUTTON_ADD_CREDIT_PRESSED = "ButtonAddCreditPressed";
        
        /**
         * Fired on any input action that is a command to open / close the game menu (generally
         * this will mean "open" it, but it can also serve as a toggle)/
         */
        this.BUTTON_TOGGLE_GAME_MENU_PRESSED = "ButtonToggleGameMenuPressed";

        /**
         * Fired on any input action which is an explicit command to close the game menu (eg: can
         * be fired from within the GameMenu itself).
         */
        this.BUTTON_CLOSE_GAME_MENU_PRESSED = "ButtonCloseGameMenuPressed";

        this.BUTTON_CLOSE_SESSION_PRESSED = "ButtonCloseSessionPressed";
        this.BUTTON_CYCLE_STAKE_PRESSED = "ButtonCycleStakePressed";
        this.BUTTON_CYCLE_NUM_HANDS_PRESSED = "ButtonCycleNumHandsPressed";
        this.BUTTON_CYCLE_NUM_LINES_PRESSED = "ButtonCycleNumLinesPressed";
        this.BUTTON_CYCLE_STAKE_PER_HAND_PRESSED = "ButtonCycleStakePerHandPressed";
        this.BUTTON_CYCLE_STAKE_PER_LINE_PRESSED = "ButtonCycleStakePerLinePressed";
        this.BUTTON_RESTART_SESSION_PRESSED = "ButtonRestartSessionPressed";

        /**
         * Should be fired when any input action which toggles sound is pressed. This is
         * distinct from "SOUND_TOGGLED", which indicates that sound status has actually
         * changed. Its important to distinguish the 2 events, because there are cases
         * where they may be an external effect on sound (eg: an external hui button which
         * will toggle sound)
         */
        this.BUTTON_TOGGLE_SOUND_PRESSED = "ButtonToggleSoundPressed";

        //------------------------------------------------------------------------------
        // Loading Screen events
        //------------------------------------------------------------------------------
        /**
         * Dispatched during the life-time of the loading screen, when all game assets have
         * finished loading (but before the loading screen has completed its life-cycle).
         */
        this.ALL_GAME_ASSETS_AVAILABLE = "AllGameAssetsAvailable";

        /**
         * Dispatched when the Loading Screen is complete
         */
        this.LOADING_SCREEN_COMPLETE = "LoadingScreenComplete";

        /**
         * Load progress update.
         */
        this.LOAD_PROGRESS = "LoadProgress";
        
        /**
         * Action to be dispatched by Cashier when its operations are completed.
         * This notifies GameController to take back control.
         */
        this.CASHIER_COMPLETE = "CashierComplete";
        
        /**
         * Indicates that the App has entered a Game Idle state.
         */
        this.GAME_IDLE_STARTED = "GameIdleStarted";

        /**
         * Indicates that the App has exited a Game Idle state.
         */
        this.GAME_IDLE_ENDED = "GameIdleEnded";

        /**
         * Indicates that the App has entered a FreeRounds Idle state.
         */
        this.FREEROUNDS_IDLE_STARTED = "FreeRoundsIdleStarted";

        /**
         * Indicates that the App has exited a FreeRounds Idle state.
         */
        this.FREEROUNDS_IDLE_ENDED = "FreeRoundsIdleEnded";

        /**
         * Command to set the GameView (eg:SlotView) to IDLE visual state. This may not necessarily
         * happen at the same time as "GameIdleStarted" - which is a very specific game input state.
         * For example, GameIdle may be exited when we open a game menu, and re-entered when we exit
         * the game menu. However, "GameViewIdle" is triggered only when a game ends (so Game View
         * Idle can still be in progress while we are in a Game Menu)
         */
        this.START_GAME_VIEW_IDLE = "StartSlotGameIdle";

        /**
         * Command to stop any IDLE visual state on the GameView (eg: SLotView). Again, this may not
         * happen at exactly the same time that GAME_IDLE app state is exited.
         */
        this.END_GAME_VIEW_IDLE = "EndSlotGameIdle";

        //------------------------------------------------------------------------------
        // Autoplay events dispatched by GameModel
        //------------------------------------------------------------------------------
        /**
         * Indicates that Autoplay mode has started. Dispatched by the Game Model.
         */
        this.AUTOPLAY_STARTED = "AutoplayStarted";

        /**
         * Indicates that Autoplay mode has ended. Dispatched by the Game Model.
         */
        this.AUTOPLAY_ENDED = "AutoplayEnded";

        /**
         * Dispatched by GameModel, to indicate that any data related to Autoplay has changed.
         */
        this.AUTOPLAY_CHANGED = "AutoplayChanged";

        //------------------------------------------------------------------------------
        // Bet Events disaptched by GameModel
        //------------------------------------------------------------------------------
        /**
         * Dispatched by GameModel, to indicate that any Bet Setting has changed.
         */
        this.BET_SETTINGS_CHANGED = "BetSettingsChanged";

        /**
         * Dispatched by GameModel, to indicate that the Bet.CurrencyType has changed.
         */
        this.CURRENCY_TYPE_CHANGED = "CurrencyTypeChanged";

        //------------------------------------------------------------------------------
        // 
        //------------------------------------------------------------------------------
        /**
         * Indicates that any data related to "Player Wallet" has changed.
         */
        this.WALLET_CHANGED = "WalletChanged";

        /**
         * Indicates that any data related to "Player Winnings" has changed.
         */
        this.WINNINGS_CHANGED = "WinningsChanged";

        // TODO: Decide if we will use these "FreeSpins started / ended" events,
        // or remove them
        /**
         * Indicates that a FreeSpins phase has started.
         */
        this.FREESPINS_STARTED = "FreeSpinsStarted";

        /**
         * Indicates that a FreeSpins phase has ended.
         */
        this.FREESPINS_ENDED = "FreeSpinsEnded";

        /**
         * Indicates that any Model data relating to FreeSpins has changed
         */
        this.FREESPINS_CHANGED = "FreeSpinsChanged";

        /**
         * Indicates that any data related to "Player Superbet" has changed.
         */
        this.SUPERBET_CHANGED = "SuperbetChanged";

        //------------------------------------------------------------------------------
        // Instructions from (business layer) to view
        // All of these actions, will start
        //------------------------------------------------------------------------------
        /**
         * The Set Message command will come packaged with a MessageBarConfig object,
         * indicating what the message should be.
         */
        this.SET_MESSAGE = "SetMessage";

        /**
         * Clears the MessageBar.
         */
        this.CLEAR_MESSAGE = "ClearMessage";

        // TODO: This can probably be deprecated
        this.SET_SPIN_BUTTON_TO_TIMER_STATE = "SetSpinButtonToTimerState";

        /**
         * Request from GameController to enable the Spin button
         */
        this.ENABLE_SPIN_BUTTON = "EnableSpinButton";

        /**
         * Request from GameController to disable the Spin Button
         */
        this.DISABLE_SPIN_BUTTON = "DisableSpinButton";

        /**
         * Request from GameController to enable all game related Gui buttons (Autoplay menu,
         * Bet menu, Main Menu, Close Session, Add Credit).
         */
        this.ENABLE_GUI_BUTTONS = "EnableGuiButtons";

        /**
         * Request from GameController to disable all game related Gui buttons
         */
        this.DISABLE_GUI_BUTTONS = "DisableGuiButtons";

        /**
         * Request from GameController to show an AutoSpin countdown timer. The number of seconds
         * that the timer will last for, is passed in the even's data packet.
         */
        this.SHOW_AUTOSPIN_TIMER = "ShowAutopspinTimer";

        /**
         * Request from GameController to clear the AutoSpin countdown timer.
         */
        this.HIDE_AUTOSPIN_TIMER = "HideAutopspinTimer";

        /**
         * Request from GameController to enable Holds Input
         */
        this.ENABLE_HOLDS_INPUT = "EnableHoldsInput";

        /**
         * Request from GameController to disable Holds Input
        */
        this.DISABLE_HOLDS_INPUT = "DisableHoldsInput";

        /**
         * Request to enable any input controls which will skip the current win presentation.
         */
        this.ENABLE_SKIP_SPIN_WIN_PRESENTATION = "EnableSkipSpinWinPresentation";

        /**
         * Request to disable any input controls which will skip the current win presentation.
         */
        this.DISABLE_SKIP_SPIN_WIN_PRESENTATION = "DisableSkipSpinWinPresentation";
        
        /**
         * Request from GameController to show the Spin 2 Holds Pattern
         */
        this.SHOW_SPIN2_HOLDS_PATTERN = "ShowSpin2HoldsPattern";

        /**
         * Request from GameController to hide the Spin 2 Holds Pattern
         */
        this.HIDE_SPIN2_HOLDS_PATTERN = "HideSpin2HoldsPattern";

        /**
         * Event trigger, to indicate that any Spin 2 Idle visual state should be shown.
         */
        this.SHOW_SPIN2_IDLE_STATE = "ShowSpin2IdleState";

        /**
         * Event trigger, to indicate that any Spin 2 Idle visual state should be hidden.
         */
        this.HIDE_SPIN2_IDLE_STATE = "HideSpin2IdleState";

        /**
         * Request from GameController to show the spin indicator. The SpinState that it should
         * be shown in, is passed as an argument. This event may be broadcast more than once, so
         * listeners should be prepared for this.
         */
        this.SHOW_SPIN_INDICATOR = "ShowSpinIndicator";

        /**
         * Request from GameController, to hide the Spin Indicator.
         */
        this.HIDE_SPIN_INDICATOR = "HideSpinIndicator";

        //------------------------------------------------------------------------------
        // Requests from core-logic to show / hide certain views.
        //------------------------------------------------------------------------------
        /**
         * Dispatched by core-logic, to request to show the loading screen to the player.
         */
        this.SHOW_LOADING_SCREEN = "ShowLoadingScreen";

        /**
         * AppController confirms that it is now OK to hide the loading screen.
         */
        this.HIDE_LOADING_SCREEN = "HideLoadingScreen";

        /**
         * Dispatched by core-logic, to request to show a new Dialog View.
         * This event should also package a DialogViewConfig instance.
         */
        this.SHOW_DIALOG_VIEW = "ShowDialogView";

        this.DIALOG_VIEW_DISMISSED = "DialogViewDismissed";

        /**
         * Dispatched by core-logic, to request to hide the Dialog view.
         */
        this.HIDE_DIALOG_VIEW = "HideDialogView";

        /**
         * Dispatched by core-logic, to request to show the Menu View.
         */
        this.SHOW_MENU = "ShowMenuView";

        /**
         * Dispatched by core-logic, to request to hide the Menu View.
         */
        this.HIDE_MENU_VIEW = "HideMenuView";

        /**
         * Dispatched by core-logic, to request to show the Cashier View.
         */
        this.SHOW_CASHIER = "ShowCashierView";

        /**
         * Dispatched by core-logic, to request to hide the Cashier View.
         */
        this.HIDE_CASHIER_VIEW = "HideCashierView";

        /**
         * Dispatched by core-logic, to request that Autoplay Menu be shown.
         */
        this.SHOW_AUTOPLAY_MENU = "ShowAutoplayMenuView";

        /**
         * Dispatched by core-logic, to request that Autoplay Menu be hidden.
         */
        this.HIDE_AUTOPLAY_MENU = "HideAutoplayMenuView";

        /**
         * Event fired by GameController, to request that the Bet Settings Menu be shown to the player.
         */
        this.SHOW_BET_MENU = "ShowBetMenuView";

        /**
         * Event fired by GameController, to request that the Bet Settings Menu be removed from the
         * screen.
         */
        this.HIDE_BET_MENU = "HideBetMenuView";

        /**
         * Event fired by GameController, to request that the Start FreeSpin Phase Notification by
         * shown to the player.
         */
        this.SHOW_FREESPIN_START_NOTIFICATION = "ShowFreeSpinStartNotification";

        /**
         * Explicit instruction (from GameController) to hide any FreeSpin Start notification that
         * may be shown currently.
         */
        this.HIDE_FREESPIN_START_NOTIFICATION = "HideFreeSpinStartNotification";

        /**
         * Event fired, when the Start FreeSpin Phase notification has been dismissed by the player.
         */
        this.FREESPIN_START_NOTIFICATION_CLOSED = "FreeSpinStartNotificationClosed";

        /**
         * Dispatched by core-logic, to request to show the Session Stats view.
         */
        this.SHOW_SESSION_STATS = "ShowSessionStatsView";

        /**
         * Dispatched by core-logic, to request to hide the Session Stats view.
         */
        this.HIDE_SESSION_STATS_VIEW = "HideSessionStatsView";

        /**
         * A command issued to show the "loading spinner" - a generic "slow loading" /
         * "slow comms" spinner avaiable in the MainView. As many views may implement
         * their own custom "slow loading / comms" animation, this is presented for use
         * generically (and is best called by the main Game or App Controllers)
         */
        this.SHOW_LOADING_SPINNER = "ShowLoadingSpinner";

        /**
         * A command issued to hide the generic "loading spinner".
         */
        this.HIDE_LOADING_SPINNER = "HideLoadingSpinner";

        //------------------------------------------------------------------------------
        // View related spin events
        //------------------------------------------------------------------------------
        /**
         * Generic message, broadcast by the view layer, to indicate that a Spin animation
         * has started. This event will be broadcast when any Spin or Re-spin starts (eg:
         * a Spin within a Spin Round). It does not guarantee that results are available
         * (it is also broadcast when an infinite Spin starts) - it is basically just a
         * notification that the reels started spinning. This event will also carry a data
         * packet, which will indicate if the Spin is part of a Spin 1 or Spin 2 phase.
         */
        this.SPIN_ANIMATION_STARTED = "SpinAnimationStarted";

        /**
         * Generic message, broadcast by the view layer, to indicate the a Spin animation
         * has finished.
         */
        this.SPIN_ANIMATION_FINISHED = "SpinAnimationFinished";

        /**
         * Event broadcast for each reel that has started spinning. This is broadcast only
         * for reels on the Primary hand. The index of the reel is attached to the event data,
         * under the name "reelIndex"
         */
        this.SINGLE_REEL_SPIN_ANIMATION_STARTED = "SingleReelSpinAnimationStarted";

        /**
         * Event broadcast for each reel that has stopped spinning. This is broadcast only
         * for reels on the Primary hand. The index of the reel is attached to the event data,
         * under the name "reelIndex"
         */
        this.SINGLE_REEL_SPIN_ANIMATION_FINISHED = "SingleReelSpinAnimationFinished";

        //------------------------------------------------------------------------------
        // 
        //------------------------------------------------------------------------------
        /**
         * Event fired by GameController, to indicate that it is fetching Spin 1 Results.
         */
        this.FETCHING_SPIN_1_PHASE_RESULTS = "FetchingSpin1PhaseResults";

        /**
         * Event fired by GameController, to indicate that it is fetching Spin 2 Results.
         */
        this.FETCHING_SPIN_2_PHASE_RESULTS = "FetchingSpin2PhaseResults";

        /**
         * Event fired by GameController, to indicate that it is fetching FreeSpin Results.
         */
        this.FETCHING_FREESPIN_PHASE_RESULTS = "FetchingFreeSpinPhaseResults";

        /**
         * Event fired by GameController, to indicate that fetching Spin 1 results has failed.
         */
        this.FETCH_SPIN_1_PHASE_RESULTS_FAILED = "FetchSpin1PhaseResultsFailed";

        /**
         * Event fired by GameController, to indicate that fetching Spin 2 results has failed.
         */
        this.FETCH_SPIN_2_PHASE_RESULTS_FAILED = "FetchSpin2PhaseResultsFailed";

        /**
         * Event fired by GameController, to indicate that fetching FreeSpin results has failed.
         */
        this.FETCH_FREESPIN_PHASE_RESULTS_FAILED = "FetchFreeSpinPhaseResultsFailed";

        /**
         * Instructiom from controller to view, to abort any infinite spin animation in progress.
         * This may not happen synchronously (there may be a sequence still shown, and some tidy
         * up taking place). For this reason, there is a corresponding response event
         * @see INFINITE_SPIN_ANIMATION_ABORTED
         */
        this.ABORT_INFINITE_SPIN_ANIMATION = "AbortInfiniteSpinAnimation";

        /**
         * Event fired by View Layer, to indicate that infinite spin animations are aborted. This
         * happens in response to a command from Controller, to abort an infinite spin animation.
         */
        this.INFINITE_SPIN_ANIMATION_ABORTED = "InfiniteSpinAnimationAborted";

        /**
         * Request by the Game Controller, to show the Spin 1 Phase Results sequence.
         */
        this.SHOW_SPIN_1_PHASE_RESULTS = "ShowSpin1PhaseResults";
        
        /**
         * Notification dispatched by the View layer, that Spin 1 Phase Results have been shown.
         */
        this.SPIN_1_PHASE_RESULTS_SHOWN = "Spin1PhaseResultsShown";

        /**
         * Request by the Game Controller, to show the Spin 2 Phase Results sequence.
         */
        this.SHOW_SPIN_2_PHASE_RESULTS = "ShowSpin2PhaseResults";

        /**
         * Notification dispatched by the View layer, that Spin 2 Phase Results have been shown.
         */
        this.SPIN_2_PHASE_RESULTS_SHOWN = "Spin2PhaseResultsShown";

        /**
         * Request by the Game Controller, to show the FreeSpin Phase Results sequence.
         */
        this.SHOW_FREESPIN_PHASE_RESULTS = "ShowFreeSpinPhaseResults";
            
        /**
         * Notification dispatched by the View Layer, that FreeSpin Phase Results have been shown.
         */
        this.FREESPIN_PHASE_RESULTS_SHOWN = "FreeSpinPhaseResultsShown";

        this.SPIN_PHASE_RESULTS_SHOWN = "SpinPhaseResultsShown";

         /**
         * Request by the Game Controller, to show the Quick Results sequence.
         */
        this.SHOW_QUICK_PHASE_RESULTS = "ShowQuickPhaseResults";

        /**
         * Notification dispatched by the View layer to indicate that the quick phase results have been shown.
         */
        this.QUICK_PHASE_RESULTS_SHOWN = "QuickPhaseResultsShown";

        /**
         * Command, issued by GameController, to skip to the end of any active Slot Win Presentation.
         */
        this.SKIP_TO_END_OF_SLOT_WIN_PRESENTATION = "SkipToEndOfSlotWinPresentation";

        this.SHOW_SCATTER_WIN_SEQUENCE = "ShowScatterWinSequence";
        this.SCATTER_WIN_SEQUENCE_SHOWN = "ScatterWinSequenceShown";

        this.SPIN_WIN_PRESENTATION_STARTED = "SpinWinPresentationStarted";
        this.SPIN_WIN_PRESENTATION_FINISHED = "SpinWinPresentationFinished";
        this.ALL_REELS_STOPPED = "AllReelsStopped";
        
        this.TOTAL_WIN_SHOWN = 'FinishedShowingTotalWin';

        /**
         * Game Controller will fire this event, when it wants the Bonus Phase Visualization
         * to be shown. Game Controller will then sit in "Bonus Phase Visualization" state,
         * until Bonus View fires the event BONUS_VISUALIZATION_COMPLETE
         */
        this.SHOW_BONUS_VISUALIZATION = 'ShowBonusVisualization';

        /**
         * Event to be fired by the BonusViewr, to indicate that the Bonus Phase visualization
         * has completed, and the Bonus View has been closed. Game Controller will take back
         * control at this point, exit the Bonus Phase State, and choose the appropriate follow
         * on state for the game.
         */
        this.BONUS_VISUALIZATION_COMPLETE = "BonusVisualizationComplete";


        //------------------------------------------------------------------------------
        // Bonus View events
        //------------------------------------------------------------------------------
        /**
         * Should be broadcast by BaseBonusView, when a "Make Selection" input action is triggered.
         * This triggering can be done by clicking on something in the screen, or pressing a
         * keyboard button ( or any other view related user input action ).
         */
        this.BONUS_MAKE_SELECTION_PRESSED = "BonusMakeSelectionPressed";

        /**
         * Should be broadcast by BaseBonusView, when a "Auto Select" input action is triggered.
         * This triggering can be done by clicking on something in the screen, or pressing a
         * keyboard button ( or any other view related user input action ).
         */
        this.BONUS_AUTO_SELECT_PRESSED = "BonusAutoSelectPressed";

        this.FETCHING_BALANCE_COMPLETE = 'FETCHING_BALANCE_COMPLETE';

        /**
         * Dispatched whenever the active spin 2 holds pattern has changed.
         */
        this.HOLDS_PATTERN_CHANGED = 'HoldsPatternChanged';

        this.PLAY_REEL_STOP_SOUND = "PlayReelStopSound";

        /**
         * Dispatched by the url selectio nview, to indicate that it should be closed.
         */
        this.HIDE_URL_SELECTION_VIEW = "HideURLSelectionView";

        this.SOUND_TOGGLED = 'SoundToggled';

        /**
         * Event fired when the model has processed the message
         * */
        this.GAME_RESULT_PROCESSED = "GameResultProcessed";
    }
}

export default (new C_GameEvent());