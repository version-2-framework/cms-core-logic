/**
 * Indicates the type of a message, eg: "BonusResultsRequest".
 */
export const MESSAGE_TYPE = 'MTP';

/**
 * Holds the unique id of a message (this is initially generated in the game client)
 */
export const MESSAGE_ID = 'MID';

/**
 * Wmg Session Id is a value injected to the client through a Url Parameter. It is
 * important for the client to send this value to the server (although I have no
 * documentation available at all, as to its meaning).
 */
export const WMG_SESSION_ID = 'WID';

/**
 * Id of the game client.
 */
export const WMG_GAME_ID = 'WGD';

/**
 * Version of the game client.
 */
export const WMG_GAME_VERSION = 'WGV';

/**
 * The id of the current game session ( the field will be blank if no session is open ).
 */
export const SESSION_ID = 'SID';

/**
 * Indicates how many minutes that the Session Inactivity Timer should run for.
 */
export const SESSION_INACTIVITY_TIMEOUT = 'TMT';

/**
 * Initial starting balance of the game session.
 * @todo:
 * Confirm that this is its meaning.
 */
export const SESSION_BALANCE = 'SBL';

/**
 * Field to indicate if this is a FreePlay game session.
 */
export const SESSION_IS_FREE_PLAY = 'FPL';

/**
 * Field to indicate if this is a FunBonus game session.
 * NOTE:
 * (A session cannot be FunBonus AND Freeplay, however, both fields are present.
 * Really, we should have had an enum for SessionType)
 */
export const SESSION_IS_FUN_BONUS = 'SBF';

/**
 * The ID associated with any special bonus that is awarded to the player.
 */
export const SPECIAL_BONUS_ID = 'BID';

/**
 * The id of the current "Ticket". Ticket is a child of Session (the same session,
 * can progress through multiple tickets: a new ticket is initialized by the server,
 * each time credit is added).
 */
export const TICKET_ID = 'PID';

/**
 * The progressive value of the current "Ticket". When a new Session is opened, a
 * Ticket Id is assigned (but not Ticket Progressive is returned). When a new Ticket
 * is started (eg: credit is added to the session), the server will assign a new
 * Ticket Progressive value (normally, it just increments this value by 1).
 */
export const TICKET_PROGRESSIVE = 'PPR';

/**
 * Field that contains the amount of credit that should be transferred to
 * a new session. Used in Session Init Request.
 */
export const REQUESTED_SESSION_BALANCE = 'RAM';

/**
 * Field that indicates the amount of credit that the player wants to add in a Buy
 * Ticket (Add Credit To Session) request. Also indicates (in a Buy Ticket reply),
 * how much was actually transferred.
 */
export const TICKET_TRANSFER_AMOUNT = 'PAM';

/**
 * Field that indicates how much credit the player has available for transfer (either
 * to a new session, or an open session). This can be found in the CheckBalanceReply
 * message.
 */
export const TRANSFER_BALANCE = 'TBL';

/**
 * Indicates the currency active in the game session.
 */
export const CURRENCY = 'CUR';

/**
 * Current value of player "Wallet" (often written as "Coin" in the client)
 */
export const WALLET = 'WLT';

/**
 * Current value of player "Points".
 */
export const POINTS = 'PNT';

/**
 * Current vaue of Session.SuperBet
 */
export const SUPERBET = 'SBT'; // TODO: Confirm the field nam

/**
 * Current value of Session.NumFreeGames
 */
export const NUM_FREE_GAMES = 'NFG'; // TODO: Confirm the field nam

/**
 * The TotalStake the current game is being played at (an integer, in points).
 */
export const TOTAL_STAKE = 'BET';

/**
 * The number of lines active in the game's bet settings.
 */
export const NUM_LINES = 'LNS';

/**
 * The number of hands active in the game's bet settings.
 */
export const NUM_HANDS = 'NHD';

/**
 * Indicates the current active game phase.
 */
export const GAME_PHASE = 'PHS';

// TODO: 2 parameters here is crap, but thats largely because the field
// means something different if its a request / reply. Unfortunately, this
// is a general problem with a lot of our services that are available to
// the Game Client.
/**
 * Indicates if a given response message from the server, is a game reload request.
 * (Does NOT mean that the session is reloaded). This will only be true, on that
 * initial game reload message.
 */
export const IS_GAME_RELOAD_MSG = 'RLD'
/**
 * Indicates if a request is coming from a Session that was reloaded
 */
export const SESSION_IS_RELOADED = 'RLD'
/**
 * Indicates an AutoHolds pattern returned from the GameServer
 */
export const AUTO_HOLDS = 'AHD'
/**
 * Contains the holds pattern selected by the player (can be different to AutoHolds)
 */
export const HOLDS_SELECTED = 'HLD'
/**
 * Contains data about symbols shown at the start of a spin. This field could contain
 * a set of reelband positions (for fixed reels), or a set of symbol ids to show (the
 * meaning must be intepreted correctly by the game client, as it is not explicitly
 * provided in the results returned from the server).
 */
export const START_SYMBOLS_DATA = 'SNR'
/**
 * Contains data about symbols shown at the end of a spin. This field could contain
 * a set of reelband positions (for fixed reels), or a set of symbol ids to show (the
 * meaning must be intepreted correctly by the game client, as it is not explicitly
 * provided in the results returned from the server).
 */
export const FINAL_SYMBOLS_DATA = 'RNR'
/**
 * Contains a list of ids of winlines that are winning. If no winlines are returned
 * for a Slot phase, we normally expect this field to contain a single value of '0'.
 * Otherwise, it is a truncated list of all winning lines, eg: [1,3,9]. For a multi
 * hand game, this list may contain the same id several times over, eg: [1,3,1,6].
 * This is because we can get wins on the same line (but over different hands), and
 * those wins can be different (@see WINLINE_HAND field)
 */
export const WINLINE_IDS = 'WLN'
/**
 * The amount won (in points) for each winning winline specified in WINLINE_IDS. If
 * no winlines are returned for a Slot phase, we expect this field to contain a single
 * value of '0'. Otherwise, we expect it to be the same length as WINLINE_IDS. Each
 * item corresponds directly to an index in WINLINE_IDS.
 */
export const WINLINE_WINNINGS = 'WLV'
/**
 * The number of symbols for each winning winline. If no winlines are returned for a
 * Slot Phase, we expect this field to contain a single value of '0'. Otherwise, we
 * expect it to be the same length as WINLINE_IDS. Each item corresponds directly to
 * an index in WINLINE_IDS, and represents the number of winning symbols.
 */
export const WINLINE_SYMBOLS_DATA = 'WSL'
/**
 * The hand that each winning winline is on, for a multi-hand game. If no winlines are
 * returned for a Slot Phase, we expect this field to contain a single value of '0'.
 * Otherwise, we expect it to be the same length as WINLINE_IDS. Each item corresponds
 * directly to an index in WINLINE_IDS, and represents the id of the hand that the win
 * is on. This field will usually be absent for games that do not support the multi-hand
 * bet feature.
 */
export const WINLINE_HAND = 'HWL'
export const WINNINGS_PER_HAND = 'WFH'
/**
 * Indicates whether each available hand in the game triggers a bonus. The field will
 * be a boolean array, and will contain as many elements as there are possible hands
 * (even if not all hands are active).
 */
export const PLAY_BONUS = 'PBN'
/**
 * Nickname of the player. The server can return this in early responses
 * (but it is non critical information).
 */
export const PLAYER_NICKNAME = 'NNM';

/**
 * @todo:
 * Verify meaning, name, use-cases
 */
export const DATE = 'DTE'

export const CLIENT_START_DATE = 'DTE';

/**
 * @todo:
 * Verify meaning, name, use-cases
 */
export const DATE_OF_REQUEST = 'DTF';

/**
 * @todo:
 * Verify meaning, name, use-cases
 */
export const DATE_OF_REQUEST_GENERATED = 'DTG';

/**
 * The ERROR field in a response indicates an error that the server might have returned.
 * If it is absent, we can assume no error. If it is present, we must check it against a
 * default value ( which indicates no error ). Otherwise, the server *is* indicating an
 * error.
 */
export const ERROR = 'ERS';

/**
 * @todo:
 * Verify meaning, name, use-cases
 */
export const ERROR_CODE = 'ERC';

/**
 * @todo:
 * Verify meaning, name, use-cases
 */
export const ERROR_KEY_CODE = 'ERROR_KEY_CODE';

/**
 * @todo:
 * Verify meaning, name, use-cases
 */
export const GAME_CLIENT_START_DATE_TIME = 'SDT';

/**
 * Field used to indicate time that the Game Client was started.
 */
export const GAME_CLIENT_START_TIME = 'STM';

/**
 * Field used to indicate date that the Game Client was started.
 */
export const GAME_CLIENT_START_DATE = 'STD';
export const TIME = 'TIM';
export const TIME_OF_REQUEST = 'TFQ';
export const TIME_OF_REQUEST_GENERATED = 'TMG';

/**
 * Starting wallet value for history.
 * @todo: Verify the meaning a bit better.
 */
export const HISTORY_START_WALLET = 'WLT';

/**
 * Final wallet value for history.
 */
export const HISTORY_FINAL_WALLET = 'FWL';

/**
 * Contains data for a Spin 1 Phaes in a History response/
 */
export const HISTORY_FIRST_SPIN = 'FSP';

/**
 * Contains data for a Spin 2 Phase in a History response.
 */
export const HISTORY_SECOND_SPIN = 'SSP';

/**
 * Contains data for a Bonus Phase in a History response.
 */
export const HISTORY_BONUS_PHASE = 'BON';

/**
 * Total amount won, in a Game Phase results object. This is normally return
 * only in a Bonus Results message ( the field is missing in the Spin results
 * messages for older game clients ).
 */
export const TOTAL_WINNINGS = 'WIN';

/**
 * A list of wintype enums: usually used for the Bonus Phase.
 */
export const WIN_TYPE = 'WTP';

/**
 * The positions of scatters in the Bonus Phase.
 */
export const BONUS_SCATTER_POSITIONS = 'CKP';

/**
 * Contains the last selection id(s) made for the Bonus Phase.
 */
export const BONUS_LAST_SELECTION = 'CKD';

export const BONUS_ALL_SELECTIONS = 'NCK';

export const BONUS_WIN_TYPE = 'WTP';