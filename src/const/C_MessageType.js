class C_MessageType
{
    constructor()
    {
        /**
         * Download Statistics Request is used to send information about the device
         * that the player is using to the server.
         */
        this.DOWNLOAD_STATISTICS_REQUEST = "WMG_DOWNLOAD_STATISTICS";

        /**
         * Download statistics reply; contains the nickname of user, but nothing
         * of any real value that the client needs.
         */
        this.DOWNLOAD_STATISTICS_REPLY = "WMG_DOWNLOAD_STATISTICS_REPLY";

        //==================================================================================================
        // Message Types pertaining to Session Management.
        //==================================================================================================
        /**
         * Session Init Request: used to start a new Game Session. We must send the
         * server the a value for amount of credit to add to the session.
         */
        this.SESSION_INIT_REQUEST = "WMG400";

        /**
         * Session Init Reply: indicates information about the session that was opened.
         */
        this.SESSION_INIT_REPLY = "WMG400R";

        /**
         * Request to close the active game session.
         */
        this.SESSION_END_REQUEST = 'WMG_SESSION_END_500_REQUEST';

        /**
         * Response indicating that the active game session was closed.
         */
        this.SESSION_END_REPLY = 'WMG_SESSION_END_500R';

        /**
         * Send a keep alive request to the server. We are supposed to send this periodically
         * (once a minute while the session is open), to inform the Game Server that the client
         * hasn't crashed.
         */
        this.KEEP_ALIVE_REQUEST = "WMG_GAME_KEEP_ALIVE_REQUEST";

        /**
         * The keep alive response from the server.
         */
        this.KEEP_ALIVE_REPLY = "WMG_GAME_KEEP_ALIVE_REPLY";

        /**
         * When the player wants to start any session that is not "Freeplay", then we must check
         * if they have an open session that requires restoring, by sending this request. If there
         * is an open session, the server will reply with a Game Phase results message of some kind
         * (the client must then determine where to resume the session). Otherwise, the server will
         * return a CHECK_FOR_OPEN_SESSION_REPLY (which indicates no session to reload).
         */
        this.CHECK_FOR_OPEN_SESSION_REQUEST = "WMG_CHECK_PENDING_REQUEST";

        /**
         * The generic 'Check For Open Session' reply indicates that there is no session to reload.
         */
        this.CHECK_FOR_OPEN_SESSION_REPLY = "WMG_CHECK_PENDING_REPLY";

        /**
         * Requet player balance. This can be used whether a Session is open, or not.
         */
        this.BALANCE_REQUEST = "WMG_BALANCE_REQUEST";

        /**
         * Balance Reply indicates player balance (what is available on their account for transfer,
         * and any balance that they already have on an open session).
         */
        this.BALANCE_REPLY = "WMG_BALANCE_REPLY";

        /**
         * Buy ticket request (used for adding credit to an open session).
         */
        this.BUY_TICKET_REQUEST = "WMG420";

        /**
         * Buy ticket reply.
         */
        this.BUY_TICKET_REPLY = "WMG420R";
        
        /**
         * Request to save a high score value. WMG originally asked us to implement this feature
         * (and it was added to the Flash game clients), but the placing of it (in SessionStats)
         * was a terrible mechanism. Instead, if we ever re-introduce it, it should be handled
         * differently.
         */
        this.SAVE_HIGH_SCORE_REQUEST = 'WMG_HIGH_SCORE';

        /**
         * Response from server, acknowledging that a high score has been saved.
         */
        this.SAVE_HIGH_SCORE_REPLY = 'WMG_HIGH_SCORE_REPLY';

        /**
         * Request to activate a fun bonus session.
         */
        this.FUN_BONUS_ACTIVATION_REQUEST = 'WMG_FUN_BONUS_ACTIVATION_REQUEST';

        /**
         * Response from the server, indicating whether a fun bonus session has been activated.
         */
        this.FUN_BONUS_ACTIVATION_REPLY = 'WMG_FUN_BONUS_ACTIVATION_REPLY';

        /**
         * Request for game history.
         */
        this.GAME_HISTORY_REQUEST = 'WMG_RECENT_GAMES_REQUEST';

        /**
         * Game history response.
         */
        this.GAME_HISTORY_REPLY = 'WMG_RECENT_GAMES_REPLY';

        //==================================================================================================
        // Message Types relating to game results
        //==================================================================================================
        // Slot phase messages, for a 1 spin game.
        //--------------------------------------------------------------------------------------------------
        /**
         * Request for results for a Spin phase, (used in a single spin game).
         */
        this.SPIN_RESULTS_REQUEST = "WMG_GAME_START_REQUEST";

        /**
         * Spin results, for a single spin game.
         */
        this.SPIN_RESULTS_REPLY = "WMG_GAME_RESULTS_REPLY";

        /**
         * Spin 1 phase complete request, for a multi-spin game.
         */
        this.SPIN_PHASE_COMPLETE_REQUEST = 'WMG_SPIN_PHASE_COMPLETE_REQ';

        /**
         * Spin 1 phase complete reply, for a multi-spin game.
         */
        this.SPIN_PHASE_COMPLETE_REPLY = 'WMG_SPIN_DATA_SAVED';

        //--------------------------------------------------------------------------------------------------
        // Slot phase messages, for a 2 spin game.
        //--------------------------------------------------------------------------------------------------
        /**
         * Request for results for a Spin 1 game phase.
         */
        this.SPIN_1_RESULTS_REQUEST = "WMG_GAME_START1_REQUEST";

        /**
         * Response, containing spin 1 results.
         */
        this.SPIN_1_RESULTS_REPLY = 'WMG_SPIN1_RESULTS_REPLY';

        /**
         * Spin 1 phase complete request, for a multi-spin game.
         */
        this.SPIN_1_PHASE_COMPLETE_REQUEST = 'WMG_SPIN1_PHASE_COMPLETE';

        /**
         * Spin 1 phase complete reply, for a multi-spin game.
         */
        this.SPIN_1_PHASE_COMPLETE_REPLY = 'WMG_SPIN1_DATA_SAVED';

        /**
         * Request for spin 2 results.
         */
        this.SPIN_2_RESULTS_REQUEST = 'WMG_GAME_START2_REQUEST';

        /**
         * Response, containing spin 2 results.
         */
        this.SPIN_2_RESULTS_REPLY = 'WMG_SPIN2_RESULTS_REPLY';

        /**
         * Spin 2 phase complete request, for a multi-spin game.
         */
        this.SPIN_2_PHASE_COMPLETE_REQUEST = 'WMG_SPIN2_PHASE_COMPLETE';

        /**
         * Spin 2 phase complete reply, for a multi-spin game.
         */
        this.SPIN_2_PHASE_COMPLETE_REPLY = 'WMG_SPIN2_DATA_SAVED';

        //--------------------------------------------------------------------------------------------------
        // Bonus messages, for a 2 spin game.
        //--------------------------------------------------------------------------------------------------
        /**
         * Bonus results request, for a 2 spin game.
         */
        this.BONUS_2_SPIN_RESULTS_REQUEST = 'WMG_BONUS_GET_RESULTS_REQUEST';

        /**
         * Bonus results reply, for a 2 spin game.
         */
        this.BONUS_2_SPIN_RESULTS_REPLY = 'WMG_BONUS_GET_RESULTS_REPLY';

        /**
         * Bonus selection request, for a 2 spin game.
         */
        this.BONUS_2_SPIN_SELECTION_REQUEST = 'WMG_PLAY_BONUS_SELECTION_REQ';

        /**
         * Bonus selection reply, for a 2 spin game.
         */
        this.BONUS_2_SPIN_SELECTION_REPLY = 'WMG_PLAY_BONUS_SELECTION_REPLY';

        /**
         * Bonus phase complete request, for a 2 spin game.
         */
        this.BONUS_2_SPIN_PHASE_COMPLETE_REQUEST = 'WMG_BONUS_PHASE_COMPLETE';

        /**
         * Bonus phase complete reply, for a 2 spin game.
         */
        this.BONUS_2_SPIN_PHASE_COMPLETE_REPLY = 'WMG_BONUS_PHASE_DATA_SAVED';

        //--------------------------------------------------------------------------------------------------
        // Bonus messages, for a single spin game.
        //--------------------------------------------------------------------------------------------------
        /**
         * Bonus results request, for a game which is single spin.
         */
        this.BONUS_1_SPIN_RESULTS_REQUEST = 'WMG_SINGLE_SPIN_BONUS_GET_RESULTS_REQUEST';

        /**
         * Bonus results reply, for a game which is single spin.
         */
        this.BONUS_1_SPIN_RESULTS_REPLY = 'WMG_SINGLE_SPIN_BONUS_GET_RESULTS_REPLY';

        /**
         * Bonus selection request, for a game which is single spin.
         */
        this.BONUS_1_SPIN_SELECTION_REQUEST = 'WMG_SINGLE_SPIN_PLAY_BONUS_SELECTION_REQ';

        /**
         * Bonus selection reply, for a game which is single spin.
         */
        this.BONUS_1_SPIN_SELECTION_REPLY = 'WMG_SINGLE_SPIN_PLAY_BONUS_SELECTION_REPLY';

        /**
         * Bonus phase complete request, for a single spin game.
         */
        this.BONUS_1_SPIN_PHASE_COMPLETE_REQUEST = 'WMG_BONUS_PHASE_COMPLETE_REQ';

        /**
         * Bonus phase complete reply, for a single spin game.
         */
        this.BONUS_1_SPIN_PHASE_COMPLETE_REPLY = 'WMG_BONUS_PHASE_COMPLETE_REPLY';

        //--------------------------------------------------------------------------------------------------
        // Messages for "double up phase" (possible for 2 spin game only..)
        // NOTE: the selection request / reply for this phase, is probably
        // not at all needed, if we ever add this phase to a game.
        //--------------------------------------------------------------------------------------------------
        /**
         * Request results for an (optonal) double up (gamble) phase
         */
        this.DOUBLE_UP_RESULTS_REQUEST = 'WMG_GAME_DOUBLE_UP_REQUEST';

        /**
         * Results for an (optonal) double up (gamble) phase
         */
        this.DOUBLE_UP_RESULTS_REPLY = 'WMG_GAME_DOUBLE_UP_REPLY';

        /**
         * Request to make a selection for an (optonal) double up (gamble) phase
         */
        this.DOUBLE_UP_SELECTION_REQUEST = 'WMG_GAME_DOUBLE_UP_SELECTION_REQUEST';

        /**
         * Response to a selection, for an (optonal) double up (gamble) phase
         */
        this.DOUBLE_UP_SELECTION_REPLY = 'WMG_GAME_DOUBLE_UP_SELECTION_REPLY';

        /**
         * Request to complete an (optonal) double up (gamble) phase
         */
        this.DOUBLE_UP_PHASE_COMPLETE_REQUEST = 'WMG_GAME_SHOWN_RESULT_DOUBLE_UP';

        /**
         * Acknowledgement that the (optonal) double up (gamble) phase is completed
         */
        this.DOUBLE_UP_PHASE_COMPLETE_REPLY = 'WMG_GAME_DOUBLE_UP_DATA_SAVED';
    }
}

export default (new C_MessageType());