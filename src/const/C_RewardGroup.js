// I think these values make sense, because they also correspond to the logical
// sequence that we would expect each type of win to be shown in. In addition,
// in the highly unlikely event that we were to design a game that could award
// all 4 types of wins at once, then the win sequence from the spin should work
// fairly elegantly:
// 1) if CREDIT & SUPERBET
//    - we see SUPERBET wins last, and can start the SUPERBET sub-games
// 2) if CREDIT & SUPERBET & FREESPIN
//    - we see FREESPIN wins last, and start the FREESPIN phase
//    - now we can start SUPERBET sub-games
// 3) if CREDIT & SUPERBET & FREESPIN & BONUS
//    - we see BONUS last, then start the BONUS phase
//    - next we get FREESPIN phase
//    - finally, we can start SUPERBET sub-games

export const MULTIPLIER = 1;
export const CREDIT = 2;
export const SUPERBET = 3;
export const FREESPIN = 4;
export const BONUS = 5;
export const BONUS_MULTIPLIER = 6;
export const PROMO_GAME = 7;