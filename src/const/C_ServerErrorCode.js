/**
 * List of error codes known to be retuned by the server, in the field
 * "ERR" ( ERROR ).
 * 
 * Unfortunately, this is poorly documented functionality ( CMS have never received
 * any actual documenation describing what is expected to be returned here ), and in
 * some cases it appears that the ERR field returns a string value, instead of an
 * actual error code.
 * 
 * TODO:
 * Chase WMG up for some proper documentation about this functionality.
 */
class C_ServerErrorCode
{
    constructor()
    {
        /**
         * The server always returns an ERR field ( which is a bit confusing ).
         * However, to indicate "no error", it is populated with this value.
         */
        this.NO_ERROR = "0000";

        /**
         * Code to indicate that session credit limit is reached (??)
         */
        this.CREDIT_LIMIT_REACHED = "0066";

        /**
         * Error code, to indicate that the server already closed the session.
         */
        this.SERVER_CLOSED_SESSION = "GES -> ERROR SESSION IS CLOSED";

        /**
         * Error code, to indicate that a request has already been handled.
         */
        this.REQUEST_ALREADY_PROCESSED = "Error Repeated message request";
    }
}

export default (new C_ServerErrorCode());