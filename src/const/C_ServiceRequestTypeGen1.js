//--------------------------------------------------------------------------------------------------
// Common Service request message types
//--------------------------------------------------------------------------------------------------
/**
 * Download Statistics Request is used to send information about the device
 * that the player is using to the server.
 */
export const DOWNLOAD_STATISTICS = "WMG_DOWNLOAD_STATISTICS"

/**
 * Session Init Request: used to start a new Game Session. We must send the
 * server the a value for amount of credit to add to the session.
 */
export const SESSION_INIT = "WMG400";

/**
 * Request to close the active game session.
 */
export const SESSION_END = 'WMG_SESSION_END_500_REQUEST';

/**
 * Send a keep alive request to the server. We are supposed to send this periodically
 * (once a minute while the session is open), to inform the Game Server that the client
 * hasn't crashed.
 */
export const KEEP_ALIVE = "WMG_GAME_KEEP_ALIVE_REQUEST";

/**
 * When the player wants to start any session that is not "Freeplay", then we must check
 * if they have an open session that requires restoring, by sending this request. If there
 * is an open session, the server will reply with a Game Phase results message of some kind
 * (the client must then determine where to resume the session). Otherwise, the server will
 * return a CHECK_FOR_OPEN_SESSION_REPLY (which indicates no session to reload).
 */
export const CHECK_FOR_OPEN_SESSION = "WMG_CHECK_PENDING_REQUEST";

/**
 * Requet player balance. This can be used whether a Session is open, or not.
 */
export const CHECK_BALANCE = "WMG_BALANCE_REQUEST"

/**
 * Buy ticket request (used for adding credit to an open session).
 */
export const BUY_TICKET = "WMG420";

/**
 * Request to save a high score value. WMG originally asked us to implement this feature
 * (and it was added to the Flash game clients), but the placing of it (in SessionStats)
 * was a terrible mechanism. Instead, if we ever re-introduce it, it should be handled
 * differently.
 */
export const SAVE_HIGH_SCORE = 'WMG_HIGH_SCORE';

/**
 * Request to activate a fun bonus session.
 */
export const FUN_BONUS_ACTIVATION = 'WMG_FUN_BONUS_ACTIVATION_REQUEST';

/**
 * Request for game history.
 */
export const GAME_HISTORY = 'WMG_RECENT_GAMES_REQUEST';

//--------------------------------------------------------------------------------------------------
// Slot Game specific service request message types for the Gen 1 maths model
//--------------------------------------------------------------------------------------------------

/**
 * Request for results for a Spin phase, (used in a single spin game).
 */
export const SINGLE_SPIN_PHASE_RESULTS = "WMG_GAME_START_REQUEST";

/**
 * Spin 1 phase complete request, for a multi-spin game.
 */
export const SINGLE_SPIN_PHASE_COMPLETE = 'WMG_SPIN_PHASE_COMPLETE_REQ';

/**
 * Request for results for a Spin 1 game phase.
 */
export const SPIN_1_PHASE_RESULTS = "WMG_GAME_START1_REQUEST";

/**
 * Spin 1 phase complete request, for a multi-spin game.
 */
export const SPIN_1_PHASE_COMPLETE = 'WMG_SPIN1_PHASE_COMPLETE';

/**
 * Request for spin 2 results.
 */
export const SPIN_2_PHASE_RESULTS = 'WMG_GAME_START2_REQUEST';

/**
 * Spin 2 phase complete request, for a multi-spin game.
 */
export const SPIN_2_PHASE_COMPLETE = 'WMG_SPIN2_PHASE_COMPLETE';

/**
 * Bonus results request, for a game which is single spin.
 */
export const BONUS_1_SPIN_PHASE_RESULTS = 'WMG_SINGLE_SPIN_BONUS_GET_RESULTS_REQUEST';

/**
 * Bonus single selection request, for a game which is single spin.
 */
export const BONUS_1_SPIN_SINGLE_SELECTION = 'WMG_SINGLE_SPIN_PLAY_BONUS_SELECTION_REQ';

/**
 * Bonus phase complete request, for a single spin game.
 */
export const BONUS_1_SPIN_PHASE_COMPLETE = 'WMG_BONUS_PHASE_COMPLETE_REQ';

/**
 * Bonus results request, for a 2 spin game.
 */
export const BONUS_2_SPIN_PHASE_RESULTS = 'WMG_BONUS_GET_RESULTS_REQUEST';

/**
 * Bonus single selection request, for a 2 spin game.
 */
export const BONUS_2_SPIN_SINGLE_SELECTION = 'WMG_PLAY_BONUS_SELECTION_REQ';

/**
 * Bonus multi-selection request, for a 2 spin game.
 */
export const BONUS_2_SPIN_MULTI_SELECTION = 'WMG_PLAY_BONUS_MULTIPLE_SELECTION_REQ';

/**
 * Bonus phase complete request, for a 2 spin game.
 */
export const BONUS_2_SPIN_PHASE_COMPLETE = 'WMG_BONUS_PHASE_COMPLETE';

/**
 * Request results for an (optonal) double up (gamble) phase
 */
export const GAMBLE_PHASE_RESULTS = 'WMG_GAME_DOUBLE_UP_REQUEST';

/**
 * Request to make a selection for an (optonal) double up (gamble) phase
 */
export const GAMBLE_SELECTION = 'WMG_GAME_DOUBLE_UP_SELECTION_REQUEST';

/**
 * Request to complete an (optonal) double up (gamble) phase
 */
export const GAMBLE_PHASE_COMPLETE = 'WMG_GAME_SHOWN_RESULT_DOUBLE_UP';