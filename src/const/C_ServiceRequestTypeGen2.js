//--------------------------------------------------------------------------------------------------
// Common Service requests
//--------------------------------------------------------------------------------------------------
/**
 * Message Type for a Download Statistics request.
 */
export const DOWNLOAD_STATISTICS = "WMG_DOWNLOAD_STATISTICS_REQUEST";

export const CHECK_PENDING = "WMG_CHECK_PENDING_REQUEST";

export const CHECK_BALANCE = "WMG_BALANCE_REQUEST";

export const SESSION_INIT = "WMG_SESSION_INIT_REQUEST";

export const ADD_CREDIT = "WMG_ADD_CREDIT_REQUEST";

export const GAME_COMPLETE = "WMG_GAME_COMPLETE_REQUEST";

export const KEEP_ALIVE = "WMG_KEEP_ALIVE_REQUEST";

export const SESSION_END = "WMG_SESSION_END_REQUEST";

//--------------------------------------------------------------------------------------------------
// Slot Game specific service requests
//--------------------------------------------------------------------------------------------------
/**
 * Message Type for a Single Spin Phase Results request.
 */
export const SINGLE_SPIN_PHASE_RESULTS = "WMG_SINGLE_SPIN_PHASE_RESULTS_REQUEST";

/**
 * Message type for a Single Spin Phase Complete request.
 */
export const SINGLE_SPIN_PHASE_COMPLETE = "WMG_SINGLE_SPIN_PHASE_COMPLETE_REQUEST";

/**
 * Message Type for a Spin 1 Phase Results request.
 */
export const SPIN_1_PHASE_RESULTS = "WMG_SPIN_1_PHASE_RESULTS_REQUEST";

/**
 * Message Type for a Spin 1 Phase Complete request.
 */
export const SPIN_1_PHASE_COMPLETE = "WMG_SPIN_1_PHASE_COMPLETE_REQUEST";

/**
 * Message Type for a Spin 2 Phase Results request.
 */
export const SPIN_2_PHASE_RESULTS = "WMG_SPIN_2_PHASE_RESULTS_REQUEST";

/**
 * Message Type for a Spin 2 Phase Complete request.
 */
export const SPIN_2_PHASE_COMPLETE = "WMG_SPIN_2_PHASE_COMPLETE_REQUEST";

/**
 * Message Type for a FreeSpin Phase Results request.
 */
export const FREESPIN_PHASE_RESULTS = "WMG_FREESPIN_PHASE_RESULTS_REQUEST";

/**
 * Message Type for a FreeSpin Phase Complete request.
 */
export const FREESPIN_PHASE_COMPLETE = "WMG_FREESPIN_PHASE_COMPLETE_REQUEST";

/**
 * Message Type for a Bonus Phase Results request.
 */
export const BONUS_PHASE_RESULTS = "WMG_BONUS_PHASE_RESULTS_REQUEST";

/**
 * Message Type for a Bonus Phase Complete request.
 */
export const BONUS_PHASE_COMPLETE = "WMG_BONUS_PHASE_COMPLETE_REQUEST";

/**
 * Message Type for a Gamble Phase Results request.
 */
export const GAMBLE_PHASE_RESULTS = "WMG_GAMBLE_PHASE_RESULTS_REQUEST";

/**
 * Message Type for a Gamble Phase Complete reply.
 */
export const GAMBLE_PHASE_COMPLETE = "WMG_GAMBLE_PHASE_COMPLETE_REQUEST";

/**
 * Message Type for a quick spin phase request.
 */
export const QUICK_SPIN_PHASE_RESULTS = "WMG_QUICK_SPIN_PHASE_RESULTS_REQUEST";