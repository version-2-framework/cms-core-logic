//--------------------------------------------------------------------------------------------------
// Common Service response message types
//--------------------------------------------------------------------------------------------------
/**
 * Download statistics reply; contains the nickname of user, but nothing
 * of any real value that the client needs.
 */
export const DOWNLOAD_STATISTICS = "WMG_DOWNLOAD_STATISTICS_REPLY";

/**
 * Session Init Reply: indicates information about the session that was opened.
 */
export const SESSION_INIT = "WMG400R";

/**
 * Response indicating that the active game session was closed.
 */
export const SESSION_END = 'WMG_SESSION_END_500R';

/**
 * The keep alive response from the server.
 */
export const KEEP_ALIVE = "WMG_GAME_KEEP_ALIVE_REPLY";

/**
 * The generic 'Check For Open Session' reply indicates that there is no session to reload.
 */
export const CHECK_FOR_OPEN_SESSION = "WMG_CHECK_PENDING_REPLY";

/**
 * Balance Reply indicates player balance (what is available on their account for transfer,
 * and any balance that they already have on an open session).
 */
export const CHECK_BALANCE = "WMG_BALANCE_REPLY";

/**
 * Buy ticket reply.
 */
export const BUY_TICKET = "WMG420R";

/**
 * Response from server, acknowledging that a high score has been saved.
 */
export const SAVE_HIGH_SCORE = 'WMG_HIGH_SCORE_REPLY';

/**
 * Response from the server, indicating whether a fun bonus session has been activated.
 */
export const FUN_BONUS_ACTIVATION = 'WMG_FUN_BONUS_ACTIVATION_REPLY';

/**
 * Game history response.
 */
export const GAME_HISTORY = 'WMG_RECENT_GAMES_REPLY';

//--------------------------------------------------------------------------------------------------
// Slot Game specific service response message types for the Gen 1 maths model
//--------------------------------------------------------------------------------------------------
/**
 * Spin results, for a single spin game.
 */
export const SINGLE_SPIN_PHASE_RESULTS = "WMG_GAME_RESULTS_REPLY";

/**
 * Spin 1 phase complete reply, for a multi-spin game.
 */
export const SINGLE_SPIN_PHASE_COMPLETE = 'WMG_SPIN_DATA_SAVED';

/**
 * Response, containing spin 1 results.
 */
export const SPIN_1_PHASE_RESULTS = 'WMG_SPIN1_RESULTS_REPLY';

/**
 * Spin 1 phase complete reply, for a multi-spin game.
 */
export const SPIN_1_PHASE_COMPLETE = 'WMG_SPIN1_DATA_SAVED';

/**
 * Response, containing spin 2 results.
 */
export const SPIN_2_PHASE_RESULTS = 'WMG_SPIN2_RESULTS_REPLY';

/**
 * Spin 2 phase complete reply, for a multi-spin game.
 */
export const SPIN_2_PHASE_COMPLETE = 'WMG_SPIN2_DATA_SAVED';

/**
 * Bonus results reply, for a game which is single spin.
 */
export const BONUS_1_SPIN_PHASE_RESULTS = 'WMG_SINGLE_SPIN_BONUS_GET_RESULTS_REPLY';

/**
 * Bonus single selection reply, for a game which is single spin.
 */
export const BONUS_1_SPIN_SINGLE_SELECTION = 'WMG_SINGLE_SPIN_PLAY_BONUS_SELECTION_REPLY';

/**
 * Bonus phase complete reply, for a single spin game.
 */
export const BONUS_1_SPIN_PHASE_COMPLETE = 'WMG_BONUS_PHASE_COMPLETE_REPLY';

/**
 * Bonus results reply, for a 2 spin game.
 */
export const BONUS_2_SPIN_PHASE_RESULTS = 'WMG_BONUS_GET_RESULTS_REPLY';

/**
 * Bonus single selection reply, for a 2 spin game.
 */
export const BONUS_2_SPIN_SINGLE_SELECTION = 'WMG_PLAY_BONUS_SELECTION_REPLY';

/**
 * Bonus Multi selection reply, for a 2 spin game.
 */
export const BONUS_2_SPIN_MULTI_SELECTION = 'WMG_PLAY_BONUS_MULTIPLE_SELECTION_REPLY';

/**
 * Bonus phase complete reply, for a 2 spin game.
 */
export const BONUS_2_SPIN_PHASE_COMPLETE = 'WMG_BONUS_PHASE_DATA_SAVED';

/**
 * Results for an (optonal) gamble phase
 */
export const GAMBLE_PHASE_RESULTS = 'WMG_GAME_DOUBLE_UP_REPLY';

/**
 * Response to a selection, for an (optonal) double up (gamble) phase
 */
export const GAMBLE_SELECTION = 'WMG_GAME_DOUBLE_UP_SELECTION_REPLY';

/**
 * Acknowledgement that the (optonal) double up (gamble) phase is completed
 */
export const GAMBLE_PHASE_COMPLETE = 'WMG_GAME_DOUBLE_UP_DATA_SAVED';