//--------------------------------------------------------------------------------------------------
// Common service response types
//--------------------------------------------------------------------------------------------------
export const DOWNLOAD_STATISTICS = "WMG_DOWNLOAD_STATISTICS_REPLY";

export const CHECK_PENDING = "WMG_CHECK_PENDING_REPLY";

export const CHECK_BALANCE = "WMG_BALANCE_REPLY";

export const SESSION_INIT = "WMG_SESSION_INIT_REPLY";

export const ADD_CREDIT = "WMG_ADD_CREDIT_REPLY";

export const GAME_COMPLETE = "WMG_GAME_COMPLETE_REPLY";

export const KEEP_ALIVE = "WMG_KEEP_ALIVE_REPLY";

export const SESSION_END = "WMG_SESSION_END_REPLY";

//--------------------------------------------------------------------------------------------------
// Slot Game specific service response message types
//--------------------------------------------------------------------------------------------------
export const SINGLE_SPIN_PHASE_RESULTS = "WMG_SINGLE_SPIN_PHASE_RESULTS_REPLY";

export const SINGLE_SPIN_PHASE_COMPLETE = "WMG_SINGLE_SPIN_PHASE_COMPLETE_REPLY";

export const SPIN_1_PHASE_RESULTS = "WMG_SPIN_1_PHASE_RESULTS_REPLY";

export const SPIN_1_PHASE_COMPLETE = "WMG_SPIN_1_PHASE_COMPLETE_REPLY";

export const SPIN_2_PHASE_RESULTS = "WMG_SPIN_2_PHASE_RESULTS_REPLY";

export const SPIN_2_PHASE_COMPLETE = "WMG_SPIN_2_PHASE_COMPLETE_REPLY";

export const FREESPIN_PHASE_RESULTS = "WMG_FREESPIN_PHASE_RESULTS_REPLY";

export const FREESPIN_PHASE_COMPLETE = "WMG_FREESPIN_PHASE_COMPLETE_REPLY";

export const BONUS_PHASE_RESULTS = "WMG_BONUS_PHASE_RESULTS_REPLY";

export const BONUS_PHASE_COMPLETE = "WMG_BONUS_PHASE_COMPLETE_REPLY";

export const GAMBLE_PHASE_RESULTS = "WMG_GAMBLE_PHASE_RESULTS_REPLY";

export const GAMBLE_PHASE_COMPLETE = "WMG_GAMBLE_PHASE_COMPLETE_REPLY";

export const QUICK_PHASE_RESULTS = "WMG_QUICK_PHASE_RESULTS_REPLY";