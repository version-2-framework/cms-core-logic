// TODO: I hate this style of constants exports that Wiener insist on.
// Convery it to a proper ES6 set of constant exports.
/**
 * Indicates the available types for a game session.
 */
class SessionType
{
    constructor()
    {
        /**
         * @type {GameplayMode}
         */
        this.FREE_PLAY = "free";

        /**
         * @type {GameplayMode}
         */
        this.REAL_PLAY = "real";

        /**
         * @type {GameplayMode}
         */
        this.FUN_BONUS = "funbonus"; // TODO: This key neesd confirming
    }
}

export default (new SessionType());