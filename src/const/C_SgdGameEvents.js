export const OGS_CONFIG_READY = "Sgd_OgsConfigReady";

export const POPUP_DISMISSED = "Sgd_PopupDismissed";

export const GAME_REVEALED = "Sgd_GameRevealed";

export const FREEROUNDS_DATA_UPDATED = "Sgd_FreeRoundsDataUpdated";

export const FREEROUNDS_SELECTION_MADE = "Sgd_FreeRoundsSelectionMade";

export const ALL_FREEROUNDS_COMPLETED = "Sgd_AllFreeRoundsComplete";