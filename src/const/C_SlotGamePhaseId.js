//--------------------------------------------------------------------------------------------------
// Set of String constants denoting the various game phases available for a Slot Game.
//--------------------------------------------------------------------------------------------------
/**
 * String id of the SingleSpin phase.
 */
export const SINGLE_SPIN = "SingleSpin";

/**
 * String id of the Spin 1 Phase.
 */
export const SPIN_1 = "Spin1";

/**
 * String id of the Spin 2 phase.
 */
export const SPIN_2 = "Spin2";

/**
 * String id of the Bonus Phase.
 */
export const BONUS = "Bonus";

/**
 * String id of the Gamble Phase.
 */
export const FREESPIN = "FreeSpin";

/**
 * String id of the Gamble phase.
 */
export const GAMBLE = "Gamble";

/**
 * String id of the Quick phase.
 */
export const QUICK = "Quick";