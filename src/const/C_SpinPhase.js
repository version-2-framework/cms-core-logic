/**
 * Simple set of constants, used ffor tracking which possible Spin Phase type is being
 * referred to.
 */
class C_SpinPhase
{
    constructor()
    {
        /**
         * @public
         * @type {SpinPhaseType}
         */
        this.SPIN_1 = 1;

        /**
         * @public
         * @type {SpinPhaseType}
         */
        this.SPIN_2 = 2;

        /**
         * @public
         * @type {SpinPhaseType}
         */
        this.FREESPIN = 3;
    }
};

export default new C_SpinPhase();