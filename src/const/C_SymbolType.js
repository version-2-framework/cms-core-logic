/**
 * Contains all flags to indicate basic type of a Symbol.
 */
class C_SymbolSpecialType
{
    constructor()
    {
        /**
         * Indicates that this symbol is a normal, "prize awarding" symbol.
         */
        this.PRIZE = "prize";
        
        /**
         * The Wild symbol can sub-stitute for Prize symbols, but not Scatter symbols.
         * The wild symbol can also award a prize in its own right - suppose a winline
         * has [wild,wild,wild,seven,seven] on it. In this case, we pay out whichever
         * combination of symbols gives the highest prize (either 3 wilds, or 5 sevens).
         */
        this.WILD = "wild";

        /**
         * Scatter symbols trigger bonus phases.
         */
        this.SCATTER = "scatter";
    }
}

export default (new C_SymbolSpecialType());