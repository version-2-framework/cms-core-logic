/**
 * The url parameter containing the value of Wmg Web Session Id
 */
export const WMG_WEB_SESSION_ID = "wmgWebSessionId";

/**
 * A url parameter, containing a value for Game Id.
 */
export const GAME_INTEGER_ID = "gameId";

/**
 * The Licensee Id for the game client.
 */
export const LICENSEE_ID = "licenseeId";