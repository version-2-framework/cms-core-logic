//--------------------------------------------------------------------------------------------------
// For the V1 platform, it is possible to configure an optional url parameter, which provides
// information about what GameModes (SessionTypes) should be available when the player is asked
// to start a session. This url parameter based configuration is optional (and the key of the url
// param may change). The value can be one of several possible strings, but represents an enum:
// it may specify a single mode, it may specify multiple modes.
//--------------------------------------------------------------------------------------------------

/**
 * The set of values which would indicate that only "Realplay" game mode should be offered.
 */
export const REALPLAY_ONLY_VALUES = ["cash","real","2","realplay_only"];

/**
 * The set of values which would indicate that only "Freeplay" game mode should be offered.
 */
export const FREEPLAY_ONLY_VALUES = ["freeplay_only","free","1"];

/**
 * The set of values which would indicate that both "Freeplay" and "Realplay" should be offered.
 */
export const FREEPLAY_AND_REALPLAY_VALUES = ["freeplay_and_realplay","3"];