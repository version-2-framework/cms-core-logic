class C_FpgBonusWintype
{
    constructor()
    {
        this.LOW = "white";

        this.MEDIUM = "silver";

        this.HIGH = "gold";

        this.BIG_WIN = "diamond";
    };
}

export default (new C_FpgBonusWintype());