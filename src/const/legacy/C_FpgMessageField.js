class C_FpgMessageField
{
    constructor()
    {
        this.BONUS_PRIZE_LOW = "PZB";

        this.BONUS_PRIZE_MEDIUM = "PZS";

        this.BONUS_PRIZE_HIGH = "PZG";

        this.BONUS_PRIZE_BIG_WIN = "PZD";
    }
};

export default (new C_FpgMessageField());