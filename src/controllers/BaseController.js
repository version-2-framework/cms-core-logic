///<reference path='../types/types.d.ts'/>

import * as LogManager from "../logging/LogManager";
import * as C_ClientErrorCode from "../const/C_ClientErrorCode";

/**
 * @typedef {Object} ControllerState
 * @property {string} id
 * @property {Function} [enter]
 * @property {Function} [exit]
 */

const defaultLog = LogManager.getLogger('BaseController');

// TODO: Verify which dependencies are really required
/**
 * @typedef BaseControllerDependencies
 * @property {EventEmitter} dispatcher
 * @property {DialogController} dialogController
 */

/**
 * Base class for a Controller / ViewModel.
 */
class BaseController
{
    /**
     * 
     * @param {BaseControllerDependencies} dependencies 
     */
    constructor(dependencies)
    {
        /**
         * @protected
         */
        this.log = defaultLog;

        /**
         * ID associated with this controller.
         * @protected
         * @type {string}
         */
        this._id = "";

        /**
         * @protected
         * @type {EventEmitter}
         */
        this._events = dependencies.dispatcher;

        /**
         * Table of active states.
         * @protected
         * @type {{ [id:string] : ControllerState }}
         */
        this._states = {};

        /**
         * Currently active state.
         * @private
         * @type {ControllerState}
         */
        this._state = null;

        /**
         * The state event that will be broadcast when state changes.
         * @protected
         * @type {string}
         */
        this._stateChangedEventId = null;

        /**
         * The state event that will be broadcast, when a state is entered.
         * Set this in sub-classes, in order to dispatch a specific enter
         * state hook. If the value is 'EnterGameState', and new state id
         * is 'Idle', then the event broadcast will be 'EnterGameState_Idle'
         * @protected
         * @type {string}
         */
        this._enterStateEventId = null;

        /**
         * The state event that will be broadcast, when a state is exited.
         * Set this in sub-classes, in order to dispatch a specific enter
         * state hook. If the value is 'ExitGameState', and old state id
         * is 'Idle', then the event broadcast will be 'ExitGameState_Idle'
         * @protected
         * @type {string}
         */
        this._exitStateEventId = null;

        /**
         * @protected
         * @type {DialogController}
         */
        this._dialogController = dependencies.dialogController;
    }


    // TODO: I don't know if these 2 methods are at all useful in BaseCOntroller
    // (in fact, they don't relate to controller at all directly..). They both need
    // using or losing.
    
    /**
     * Handles a comms error. Will present a dialog view to the player. If the commsError
     * is a connection error, and a retry action is supplied, then some Dialog COntroller's (and
     * their implementation of "comms error dialog") may offer the player the change to try the
     * failed comms action again. For this reason, "actionOnRetry" parameter is availabel: this
     * will be invoked when the "Try Again" option is selected, allowing you to re-perform some
     * action.
     * @protected
     * @param {ServiceExecutionError} error
     * @param {() => void} [actionOnRetry]
     * A "retry" method: if the Service Execution Error passed to param error indicates that it
     * is a simple comms error (eg: connection timeout), and a value is passed to actionOnRetry,
     * then the Dialog View shown will offer the player a button labelled something like "Try Again":
     * pressing it will close the Dialog View, and invoke actionOnRetry. The actionOnRetry should
     * invoke a series of steps that will reattempt the failed comms operation (and any associated
     * on-screen animations).
     */
    showCommsErrorDialog(error, actionOnRetry = null)
    {
        this._dialogController.showServiceExecutionErrorDialog(error, actionOnRetry);
    };


    // TODO: It's highly likely that this is redundant and never presently called
    handleSessionTimeout()
    {

    }


    /**
     * Registers a state.
     * @protected
     * @param {String} id 
     * @param {Function} [enter]
     * @param {Function} [exit]
     */
    addState(id, enter, exit)
    {
        if (id in this._states) {
            this.log.debug(`[${this._id}]: replacing existing state with id ${id}`);
        }

        enter = enter? enter.bind(this) : undefined;
        exit = exit? exit.bind(this) : undefined;

        this._states[id] = { id, enter, exit };
    }


    /**
     * Checks if there is a state registered under a specific id.
     * @protected
     * @param {string} id
     * @return {boolean}
     */
    hasState(id)
    {
        return (id in this._states);
    }


    /**
     * Returns the number of registered states.
     * @protected
     * @return {number}
     */
    getNumStates()
    {
        return Object.keys(this._states).length;
    }


    /**
     * Enters a specific state (registered by string id). The state transition
     * will only be executed if a state exists for the id requested. Any current
     * state will be exited, before the new state's enter action is called. It is
     * also valid to enter the current state again.
     * @protected
     * @param {string} newStateId 
     */
    enterState(newStateId)
    {
        if (newStateId in this._states)
        {
            let oldState = this._state;
            let newState = this._states[newStateId];

            this.log.debug(`[${this._id}].changingState(from ${oldState?oldState.id:"NO_STATE"},to ${newState.id})`);

            if (oldState && oldState.exit) {
                oldState.exit();
            }

            // TODO: Move "exitStateEvent" here instead ?

            this._state = null;

            //setTimeout(() =>
            //{
                this._state = newState;

                if (newState && newState.enter) {
                    newState.enter();
                }

                if (this._stateChangedEventId) {
                    let oldStateId = oldState ? oldState.id : null;
                    this._events.emit(this._stateChangedEventId, { oldStateId, newStateId});
                }

                if (this._enterStateEventId) {
                    this._events.emit(`${this._enterStateEventId}_${newState.id}`);
                }

                if (this._exitStateEventId && oldState) {
                    this._events.emit(`${this._exitStateEventId}_${oldState.id}`);
                }
                
            //}, 0);
        }
        else
        {
            this.log.debug(`[${this._id}]: cannot change state to ${newStateId}, not registered`);
        }
    }


    /**
     * Returns the id of the currently active state. If no state is active,
     * then a blank string is returned.
     * @public
     * @return {string}
     */
    getState()
    {
        return this._state ? this._state.id : null;
    }
}

export default BaseController;