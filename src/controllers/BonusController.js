import C_Comm from "../const/C_Comm";
import C_Client from "../const/C_Client";
import C_GameEvent from "../const/C_GameEvent";
import C_BonusState from "../const/C_BonusState";
import BaseController from "./BaseController";
// import BonusModel from '../model/';

/**
 * Bonus game controller.
 */
class BonusController extends BaseController
{

    constructor(dependencies)
    {
        super(dependencies);

        /**
         * @type {BonusModel}
         */
        this._bonusModel = null;

        this.addState(C_BonusState.NOT_IN_PROGRESS);
        this.addState(C_BonusState.SUSPENDED, this.enterStateSuspended, this.exitStateSuspended);
        this.addState(C_BonusState.PHASE_INTRO, this.enterStatePhaseIntro, this.exitStatePhaseIntro);
        this.addState(C_BonusState.ROUND_INTRO, this.enterStateRoundIntro, this.exitStateRoundIntro);
        this.addState(C_BonusState.ROUND_IN_PROGRESS, this.enterStateRoundInProgress, this.exitStateRoundInProgress);
        this.addState(C_BonusState.ROUND_RESULTS, this.enterStateRoundResults, this.exitStateRoundResults);
        //this.addState(C_BonusState.ROUND_OUTRO, this.enterStateRoundOutro, this.exitStateRoundOutro);
        this.addState(C_BonusState.START_NEW_LEVEL, this.enterStateLevelUp, this.exitStateLevelUp);
        this.addState(C_BonusState.PHASE_RESULTS, this.enterStatePhaseResults, this.exitStatePhaseResults);
        this.addState(C_BonusState.PHASE_OUTRO, this.enterStatePhaseOutro, this.exitStatePhaseOutro);
    }
    
    
    //--------------------------------------------------------------------------------------------------
    // TODO:
    // Need a defined set of events to handle here that relate to outside interrupts
    // eg: we may need to kill a state in progress when these outside events occur.
    //--------------------------------------------------------------------------------------------------
    /**
     * @inheritDoc
     */
    handleSessionTimeout() {
        this.enterState(C_BonusState.SUSPENDED);
        super.handleSessionTimeout();
    }

    //--------------------------------------------------------------------------------------------------
    // State transition actions
    //--------------------------------------------------------------------------------------------------
    /**
     * "Suspended" means that there has been some kind of error (application may be showing
     * an overlay). Bonus itself is still visible on screen (but paused in some way).
     * @protected
     * @todo: The use of this state neede properly defining.
     */
    enterStateSuspended() {

    }

    /**
     * @protected
     */
    exitStateSuspended() {

    }

    /**
     * Phase intro is any intro sequence that the BaseBonusView shows. I would NOT normally include in this
     * the "scatter win sequence" that we show in the SlotView. Also, I would not include any "transition
     * from Slot View to Bonus View" animation sequences.
     * @protected
     */
    enterStatePhaseIntro() {
        
    }

    /**
     * @protected
     */
    exitStatePhaseIntro() {

    }

    /**
     * Round intro means that we can show indicators, such as "starting round 3 out of 20", or we can
     * move pieces into place (for example: in Fowl Play games, we have between 3 to 5 chickens that can
     * be selected in each round. We might want to play an animation moving them into place). We can do
     * it here. Round is not in progress, so no selections can take place here. We can of course have
     * this be a "dummy" state, and skip it.
     * @protected
     */
    enterStateRoundIntro() {
        
    }

    /**
     * @protected
     */
    exitStateRoundIntro() {

    }

    /**
     * This state could also be called "Select Scatter". It is the only state that accepts user input
     * from the player (for scatter selection).
     * @protected
     */
    enterStateRoundInProgress()
    {
        let autoSelectionTimeout = this._model.isAutoplayInProgress() ? 10 : 20;

        this._autoSelectionTimer = setTimeout(() => {
            this.makeRandomSelection();
        }, autoSelectionTimeout);
    }

    /**
     * @protected
     */
    exitStateRoundInProgress() {
        clearTimeout(this._autoSelectionTimer);
    }

    /**
     * In the RoundResults state, we show all results for the selection made by the player. This can
     * include prizes, accumulated multipliers, lose lives, level ups. We also send the "Bonus Selection
     * Request" message to the Game Engine Server. The view action that we initiated (showing the round
     * results) and the request message sent to the server are both async events: we can only advance
     * out of this state, when both of those events are completed.
     * @protected
     */
    enterStateRoundResults() {   
        this._roundResultsAnimComplete = false;
        this._roundResultsRequestComplete = false;
        
        this._services.sendBonusSelectionRequest(() => {
            this._roundResultsRequestComplete = true;
            this.tryAndEndRoundResults();
        });
    }

    /**
     * @protected
     */
    exitStateRoundResults() {
        // TODO:
        // We could forcibly kill any event listeners on the comms action here
        // This would help us clean up in the event of interruption by outside
        // forces, eg: fatal error, session timeout.

        this._roundResultsAnimComplete = false;
        this._roundResultsRequestComplete = false;
    }

    /**
     * We add a dedicated state to indicate that a "level up" animated transition is taking place.
     * @protected
     */
    enterStateLevelUp() {
        
    }

    exitStateLevelUp() {

    };

    /**
     * Phase Results really means "Total Winnings Sequence". During this state, we ask the view to show
     * the Total Winnings animation, and we also send a "Bonus Phase Complete" request to the server.
     * Once this 2 asynchronous actions are completed, we may start to end the bonus state.
     * @protected
     */
    enterStatePhaseResults()
    {
        this._phaseResultsAnimComplete = false;
        this._phaseResultsRequestComplete = false;

        this._services.sendBonusPhaseCompleteRequest(() => {
            this._phaseResultsRequestComplete = true;
            this.tryAndEndPhaseResults();
        });
    }

    /**
     * @protected
     */
    exitStatePhaseResults() {
        this._phaseResultsAnimComplete = false;
        this._phaseResultsRequestComplete = false;
    }

    /**
     * Show Phase Outro to the player. Again, for some games, this can be a dummy state where nothing
     * happens (and is over immediately), but in others, we may show a final outro sequence / play a
     * final outro tune.
     * @protected
     */
    enterStatePhaseOutro() {
        
    }

    /**
     * @protected
     */
    exitStatePhaseOutro() {

    }

    //--------------------------------------------------------------------------------------------------
    // Public API (the part accessed by BaseBonusView)
    // We can either
    // a) Give BaseBonusView access to controller, or
    // b) Have BaseBonusView talk to controller through events
    // 
    // In both cases, I propose NOT to give Controller access to view. Instead, the BaseBonusView
    // simply has to sync to the state changes instigated by BonusController. BaseBonusView is going
    // to get ALL data it needs from the model directly - I don't like the idea of passing data
    // to BaseBonusView through method calls (because those method calls quickly become huge and hard
    // to read).
    //
    // However, if Wiener can suggest a good reason to have BaseBonusView directly owned by BonusController
    // I am happy to listen (I would still insist that almost no data is passed to BaseBonusView through
    // any public BaseBonusView API, however)
    //--------------------------------------------------------------------------------------------------
    /**
     * Action to be invoked by the view, when a Phase Intro sequence is completed.
     * @public
     */
    actionOnPhaseIntroComplete() {
        if (this.getState() === C_BonusState.PHASE_INTRO) {
            if (this._bonusModel.getNumRoundsRemaining() > 0) {
                this.enterState(C_BonusState.PHASE_RESULTS);
            }
            else {
                this.enterState(C_BonusState.ROUND_INTRO);
            }
        }
    }

    /**
     * Action to be invoked by the view, when a Round Intro sequence is completed.
     * @public
     */
    actionOnRoundIntroComplete() {
        if (this.getState() === C_BonusState.ROUND_INTRO) {
            this.enterState(C_BonusState.ROUND_IN_PROGRESS);
        }
    }

    /**
     * Makes a selection.
     * @public
     * @param {number} selectionId
     * The id of the selection to make.
     */
    makeSelection(selectionId) {
        if (this.getState() === C_BonusState.ROUND_IN_PROGRESS) {
            this._bonusModel.makeSelection(selectionId);
            this.enterState(C_BonusState.ROUND_RESULTS);
        }
    }

    /**
     * Makes a random selection. This will get called by BonusController automatically
     * after a certain time has elapsed in the "RoundInProgress" state. This could also
     * be called by BaseBonusView (we may allow a "random selection" action to the player,
     * even if there is no on-screen button to do it, on a game like Big Ghoulies where
     * the player has 20 windows they could select, pressing the 1 to 5 keys on the
     * keyboard on desktop can route to "makeRandomSelection").
     * @public
     */
    makeRandomSelection() {
        if (this.getState() === C_BonusState.ROUND_IN_PROGRESS) {
            this._bonusModel.makeRandomSelection();
            this.enterState(C_BonusState.ROUND_RESULTS);
        }
    }

    /**
     * Action to be invoked by the view, when a Round Results sequence is completed
     * (ie: all individual results for the round shown, any winnings for the round
     * are shown).
     * @public
     */
    actionOnRoundResultsComplete() {
        if (this.getState() === C_BonusState.ROUND_RESULTS) {
            this._roundResultsAnimComplete = true;
            this.tryAndEndRoundResults();
        }
    }

    /**
     * @private
     */
    tryAndEndRoundResults () {
        if (this.getState() ===  C_BonusState.PHASE_RESULTS &&
            this._roundResultsAnimComplete && this._roundResultsRequestComplete) {
            if (this._bonusModel.getNumRoundsRemaining() > 0) {
                if (this._bonusModel.getLastSelection().roundResult.isLevelUp) {
                    this.enterState(C_BonusState.START_NEW_LEVEL);
                }
                else {
                    this.enterState(C_BonusState.ROUND_INTRO);
                }
            }
            else {
                this.enterState(C_BonusState.PHASE_RESULTS);
            }
        }
    }

    /**
     * Action to be invoked by the view, when a Level Up animation sequence has been completed.
     * @public
     */
    actionOnLevelUpComplete() {
        if (this.getState() === C_BonusState.START_NEW_LEVEL) {
            this.enterState(C_BonusState.ROUND_INTRO);
        }
    }

    /**
     * Action to be invoked by the view, when a Phase Results (total winnings & multiplier)
     * animation sequence is completed.
     * @public
     */
    actionOnPhaseResultsComplete() {
        if (this.getState() === C_BonusState.PHASE_RESULTS) {
            this._phaseResultsAnimComplete = true;
            this.tryAndEndPhaseResults();
        }
    }

    /**
     * Attempts to end the Phase Results state (requires both animation to be shown,
     * and for the Phase Results Complete reply to have been received from the server).
     * @private
     */
    tryAndEndPhaseResults () {
        if (this.getState() === C_BonusState.PHASE_RESULTS) {
            if (this._phaseResultsAnimComplete && this._phaseResultsRequestComplete) {
                this.enterState(C_BonusState.PHASE_OUTRO);
            }
        }
    }

    /**
     * Action to be invoked by the view, when a Phase Outro animation sequence is completed.
     * @public
     */
    actionOnPhaseOutroComplete() {
        if (this.getState() === C_BonusState.PHASE_OUTRO) {
            this.enterState(C_BonusState.NOT_IN_PROGRESS);
        }
    }

}

export default BonusController;