// This module will list all of the possible notifications i can think of. It might still be
// neater to handle notifications abstractly, even if, in fact, we do have to use separate
// app controllers for WMG / Sgd / other future integrations. Any by listing all the notifications
// (and specify exactly what is done on each), it might be possible to see this task from the
// bigger picture.

/**
 * Notification to the player (after a game is completed), that their autoplay session has
 * been aborted (because it was not possible to continue). The exact reason why this has happened,
 * will be described in the text of the notification, eg:
 * - player had insufficient money to continue
 * - player breached their max loss limits
 */
export const AUTOPLAY_ABORTED = "NotificationAutoplayAborted";

/**
 * Notification to the player (after a game is completed), that their autoplay session has
 * completed successfully. The player is given 2 choices
 * - dimiss the notification
 * - start a new autoplay session, with the same settings as the one just completed.
 */
export const AUTOPLAY_COMPLETED = "NotificationAutoplayCompleted";

/**
 * Notification to the player, when they press SPIN from IDLE, informing them that their bet settings
 * are too high to start a new game (but they do have enough credit to play another round). The player
 * is given 2 choices 
 * - dismiss the notification
 * - decrease their bet settings to min
 * 
 * TODO: We don't actually need to provide the "decrease to min" option
 */
export const BET_TOO_HIGH = "NotificationBetTooHigh";

/**
 * Notification to the player, when they press SPIN from IDLE, informing them that they do not have
 * enough credit to start a new game (eg: min total bet settings are more than they have available).
 * 
 * If it is allowed for the player to add more credit to their current game session, then 2 options
 * are show to the player
 * - dimiss the dialog
 * - add more credit to their session
 * 
 * If the player cannot add more credit to their session, then the player is presented with a single
 * option
 * - dimiss the dialog
 */
export const OUT_OF_CREDIT = "NotificationOutOfCredit";