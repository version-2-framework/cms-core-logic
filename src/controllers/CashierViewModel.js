import BaseController from "./BaseController";
import C_GameEvent from "../const/C_GameEvent";
import * as LogManager from "../logging/LogManager";
import { ExitClientUtil } from "../utils/ExitClientUtil";

const log = LogManager.getLogger("CashierViewModel");

export const CashierViewState =
{
    /**
     * Indicates that there is no active state in progress for Cashier.
     */
    NONE : "None",

    /**
     * Indicates that the Cashier is currently fetching balance data.
     */
    FETCHING_BALANCE : "FetchingBalance",

    /**
     * The Cashier allows user input: the user can confirm the amounts that
     * they want to transfer to a new session / existing session.
     */
    SELECT_TRANSFER_AMOUNT : "SelectTransferAmount",

    /**
     * After pressing "transfer", the Cashier is now executing the transfer
     * request with the Game Engine Server.
     */
    TRANSFERRING_CREDIT : "TransferringCredit",

    /**
     * A special warning state, indicating that the transfer requested is
     * not possible.
     */
    TRANSFER_IMPOSSIBLE : "TransferImpossible"
};

export const CashierViewEvents =
{
    /**
     * Event fired when the ViewModel enters the "Fetching Balance" state
     */
    ENTER_STATE_FETCHING_BALANCE : "CashierView.EnterStateFetchingBalance",

    /**
     * Event fired when the ViewModel exits the "Fetching Balance" staet
     */
    EXIT_STATE_FETCHING_BALANCE : "CashierView.ExitStateFetchingBalance",

    ENTER_STATE_SELECT_TRANSFER_AMOUNT : "CashierView.EnterSelectTransferAmount",

    EXIT_STATE_SELECT_TRANSFER_AMOUNT : "CashierView.ExitSelectTransferAmount",

    ENTER_STATE_TRANSFERRING_CREDIT : "CashierView.EnterTransferringCredit",

    EXIT_STATE_TRANSFERRING_CREDIT : "CashierView.ExitTransferringCredit"
};

/**
 * @typedef CashierViewModelDependencies
 * @property {PlatformApi} platform
 * @property {BusinessConfig} businessConfig
 * @property {BuildInfoApi} buildInfo
 * @property {Model} model
 * @property {EventEmitter} dispatcher
 * @property {ServicesApi} services
 * @property {DialogController} dialogController
 * @property {ExitClientUtil} exitClientUtil
 */

/**
 * ViewModel class to be used by the Cashier View.
 */
export class CashierViewModel extends BaseController
{
    /**
     * @inheritDoc
     * @param {CashierViewModelDependencies} dependencies
     */
    constructor(dependencies)
    {
        super(dependencies);

        this._id = "CashierViewModel";

        /**
         * @private
         * @type {BusinessConfig}
         */
        this._businessConfig = dependencies.businessConfig;

        /**
         * @private
         * @type {PlatformApi}
         */
        this._platform = dependencies.platform;

        /**
         * @private
         * @type {CashierViewConfig}
         */
        this._cashierViewConfig = dependencies.businessConfig.cashierView || {};

        /**
         * @private
         * @type {ExitClientUtil}
         */
        this._exitClientUtil = dependencies.exitClientUtil;

        /**
         * @private
         * @type {BuildInfoApi}
         */
        this._buildInfo = dependencies.buildInfo;

        /**
         * @private
         * @type {Model}
         */
        this._model = dependencies.model;

        /**
         * @private
         * @type {ServicesApi}
         */
        this._services = dependencies.services;

        /**
         * Stores the current proposed transfer value.
         * @private
         * @type {number}
         */
        this._transferAmount = 0;

        /**
         * @private
         * @type {BalanceData}
         */
        this._balanceData = {
            accountBalance : 0,
            maxTransfer : 0,
            maxTransferInSession : Number.POSITIVE_INFINITY
        };

        // Set up states
        this.addState(CashierViewState.NONE);
        this.addState(CashierViewState.FETCHING_BALANCE, this.enterFetchingBalance, this.exitFetchingBalance);
        this.addState(CashierViewState.SELECT_TRANSFER_AMOUNT, this.enterSelectTransferAmount, this.exitSelectTransferAmount);
        this.addState(CashierViewState.TRANSFERRING_CREDIT, this.enterTransferringCredit, this.exitTransferringCredit);
        this.addState(CashierViewState.TRANSFER_IMPOSSIBLE, this.enterTransferImpossible);

        //this._enterStateEventId = 'EnterCashierState';
        //this._exitStateEventId = 'ExitCashierState';

        //------------------------------------------------------------------------------
        // Configure features
        //------------------------------------------------------------------------------
        /**
         * @private
         * @type {boolean}
         */
        this._isExitClientButtonAvailable = false;

        /**
         * @private
         * @type {ExitClientAction | null}
         */
        this._exitClientAction = null;

        // Determine whether Exit client is allowed, and how it works..
        if (this._cashierViewConfig.exitClientButton)
        {
            let exitClientButtonConfig = this._cashierViewConfig.exitClientButton;

            if (exitClientButtonConfig.isEnabled)
            {
                this._isExitClientButtonAvailable = true;

                if (this._platform.isMobileOrTablet())
                {
                    if (exitClientButtonConfig.customActionMobile) {
                        this._exitClientAction = exitClientButtonConfig.customActionMobile;
                    }
                    else this._exitClientAction = this._businessConfig.exitClientActionMobile;
                }
                else
                {
                    if (exitClientButtonConfig.customActionDesktop) {
                        this._exitClientAction = exitClientButtonConfig.customActionDesktop;
                    }
                    else this._exitClientAction = this._businessConfig.exitClientActionDesktop;
                }
            }
        }        

        //------------------------------------------------------------------------------
        // Start the view running
        //------------------------------------------------------------------------------
        // Start in state "none" - its a place-holder to indicate that we are waiting for
        // the view to tell us that it is fully created / shown!
        this.enterState(CashierViewState.NONE);
    };


    //--------------------------------------------------------------------------------------------------
    // Fetching Balance state
    //--------------------------------------------------------------------------------------------------
    // In this state, the Cashier is asking the Game Engine Server for information about the balance
    // on the player's account. The Game Engine Server will also return data indicating any balance
    // added to an open session.
    //--------------------------------------------------------------------------------------------------
    /**
     * @private
     */
    enterFetchingBalance() {
        this._events.emit(CashierViewEvents.ENTER_STATE_FETCHING_BALANCE);

        let gameplayMode = this._model.getSessionType();

        // Fetch balance from server, then either enter a state to indicate that a
        // transfer is impossible, or let the player select their transfer amount.
        this._services.sendCheckBalanceRequest(gameplayMode, balanceData => {
            this._balanceData = balanceData;
            this._transferAmount = this.getMaxTransferAmount();
            let minTransferAllowed = this.getMinTransferAmount();
            if (minTransferAllowed > balanceData.maxTransfer) {
                this.enterState(CashierViewState.TRANSFER_IMPOSSIBLE);
            }
            else
            {
                this.enterState(CashierViewState.SELECT_TRANSFER_AMOUNT);

                // TODO : This event should be deprecated, as it has no relevant to how the CashDesk
                // view works. I suspect Igor added this (because he doesn't understand how the code
                // is meant to work), but i am not certain.
                this._events.emit(C_GameEvent.FETCHING_BALANCE_COMPLETE);
            }

        }, err => this.handleFetchingBalanceCommsError(err));
    };


    /**
     * @private
     * @param {ServiceExecutionError} error 
     */
    handleFetchingBalanceCommsError(error) {
        log.error(`commsError when fetching balance`);

        this.showCommsErrorDialog(error, () => {
            this.log.info("Attempting to fetch balance again");
            this.enterState(CashierViewState.FETCHING_BALANCE);
        });
    };


    /**
     * @private
     */
    exitFetchingBalance() {
        this._events.emit(CashierViewEvents.EXIT_STATE_FETCHING_BALANCE);
    };


    //--------------------------------------------------------------------------------------------------
    // Select Transfer Amount state
    //--------------------------------------------------------------------------------------------------
    /**
     * Enter action for the Select Transfer Amount state.
     * @private
     */
    enterSelectTransferAmount() {
        this._events.emit(CashierViewEvents.ENTER_STATE_SELECT_TRANSFER_AMOUNT);
        this.setTransferAmountToMax();
    };


    /**
     * @private
     */
    exitSelectTransferAmount() {
        this._events.emit(CashierViewEvents.EXIT_STATE_SELECT_TRANSFER_AMOUNT)
    };


    //--------------------------------------------------------------------------------------------------
    // Transferring Credit state
    //--------------------------------------------------------------------------------------------------
    /**
     * Enter action for the Transferring credit state.
     * @private
     */
    enterTransferringCredit() {
        this._events.emit(CashierViewEvents.ENTER_STATE_TRANSFERRING_CREDIT);

        if (this.isAddCreditCashier()) {
            this.sendAddCreditRequest();
        }
        else this.sendSessionInitRequest();
    };


    /**
     * Sends the Session Init request to the server, and handles its outcome.
     * @private
     */
    sendSessionInitRequest() {
        let transferAmount = this.getTransferAmount();
        let gameplayMode = this._model.getSessionType();

        this._services.sendSessionInitRequest(
            transferAmount, gameplayMode,
            reply => {
                this._model.processSessionInitMsg(reply);
                this.closeCashierAndReturnControlToGame();
            },
            error => this.handleSessionInitCommsError(error));
    };


    /**
     * Sends an Add Credit request to the server, and handles its outcome.
     */
    sendAddCreditRequest() {
        this._services.sendAddCreditRequest(this.getTransferAmount(), reply => {
            this._model.processAddCreditMsg(reply);
            this.closeCashierAndReturnControlToGame();
        },
        error => this.handleTransferCreditCommsError(error));
    };


    /**
     * @private
     * @param {ServiceExecutionError} error 
     */
    handleTransferCreditCommsError(error) {
        log.error("CommsError when transferring credit");

        this.showCommsErrorDialog(error, () => this.sendAddCreditRequest());
    };


    /**
     * @private
     * @param {ServiceExecutionError} error 
     */
    handleSessionInitCommsError(error) {
        log.error("CommsError during SessionInit");

        this.showCommsErrorDialog(error, () => this.sendSessionInitRequest());
    };


    /**
     * @private
     */
    exitTransferringCredit() {
        this._events.emit(CashierViewEvents.EXIT_STATE_TRANSFERRING_CREDIT);
    };


    //--------------------------------------------------------------------------------------------------
    /**
     * Action to be invoked by CashierViewModel once all Cashier actions are completed
     * (either we finished our methods to add credit / start a session, or the player
     * has requested to close the cashier).
     * @private
     */
    closeCashierAndReturnControlToGame() {
        // TODO: verify if this is really a good flow ?
        this._events.emit(C_GameEvent.HIDE_CASHIER_VIEW);
        this._events.emit(C_GameEvent.CASHIER_COMPLETE);          
    };

    //--------------------------------------------------------------------------------------------------
    // Transfer impossible state
    //--------------------------------------------------------------------------------------------------
    /**
     * Enter action for the Transfer Impossible state.
     * @private
     */
    enterTransferImpossible() {
        let playerWantsToTrayAgain = false;

        /** @type {MultiChoiceDialogViewModel} */
        let dialogVm = {
            type : "multiChoice",
            header : { localizationId:"T_dialog_TransferImpossible_Title" },
            message : {
                localizationId:"T_dialog_TransferImpossible_Content",
                localizationMods : {
                    "[MAX_TRANSFER_AMOUNT]" : this._balanceData.maxTransfer,
                    "[MIN_TRANSFER_AMOUNT]" : this.getMinTransferAmount()
                }
            },
            options : [
                { localizationId:"T_button_tryAgain", action: () => playerWantsToTrayAgain=true }
            ]
        };

        if (this.isAddCreditCashier()) {
            dialogVm.options.push({ localizationId : "T_button_cancel" });
        }

        // We generally don't manually invoke an action like a state change from a dialog button -
        // we wait until all dialogs are cleared, before executing a post dialog action, based on
        // flags set from the dialogs. This is convoluted for a single dialog, but required for
        // cases where we have several dialogs in a row, all of which might present the player an
        // option of some kind, where we have to build flags for the final "post dialog" action.
        let onDialogsShown = () => {
            if (playerWantsToTrayAgain) {
                this.enterState(CashierViewState.FETCHING_BALANCE);
            }
            else this.closeCashierAndReturnControlToGame();
        }

        this._dialogController.showDialogs([dialogVm], onDialogsShown);
    };
    

    //--------------------------------------------------------------------------------------------------
    // Public API for consumption by Cash Desk View
    //--------------------------------------------------------------------------------------------------
    /**
     * Action to be called by CashierView when the Cashier is shown (and has finished initializing
     * itself).
     * @public
     */
    actionOnViewShown() {
        if (this.getState() !== CashierViewState.NONE) {
            return;
        }

        this.log.debug(`CashierViewModel.actionOnViewShown: isAddCreditCashier = ${this.isAddCreditCashier()}`);

        this.enterState(CashierViewState.FETCHING_BALANCE);
    };


    /**
     * Sets the transfer amount to use.
     * @public
     * @param {number} value 
     */
    setTransferAmount(value) {
        if (this.getState() !== CashierViewState.SELECT_TRANSFER_AMOUNT) {
            return;
        }

        let minValue = this.getMinTransferAmount();
        let maxValue = this.getMaxTransferAmount();
        let increment = this.getMinTransferIncrement();
        
        value = value - (value % increment);
        
        if (value > maxValue) {
            value = maxValue;
        }
        else
        if (value < minValue) {
            value = minValue;
        }

        this._transferAmount = value;
    };


    /**
     * Sets the transfer amount to the minimum value possible.
     * @public
     */
    setTransferAmountToMin() {
        if (this.getState() !== CashierViewState.SELECT_TRANSFER_AMOUNT) {
            return;
        }

        this._transferAmount = this.getMinTransferAmount();
    };


    /**
     * Sets the transfer amount to the maxmimum value possible.
     * @public
     */
    setTransferAmountToMax() {
        if (this.getState() !== CashierViewState.SELECT_TRANSFER_AMOUNT) {
            return;
        }
        this._transferAmount = this.getMaxTransferAmount();
    };


    /**
     * Returns the current transfer amount.
     * @public
     * @return {number}
     */
    getTransferAmount() {
        return this._transferAmount;
    };


    /**
     * Returns the current transfer amount, formatted as a currency string.
     * @public
     * @return {string}
     */
    getTransferAmountString() {
        return this._model.formatCurrency(this._transferAmount);
    };


    /**
     * Returns the minimum amount which may be transferred (in points). This is the
     * minimum value that the slider in the CashDesk screen can be set to.
     * @public
     * @return {number}
     */
    getMinTransferAmount() {
        return this._model.getMinSessionTransfer();
    };


    /**
     * Returns the maximum amount that the player may transfer in the current instance
     * of the cashdesk.
     * @public
     * @return {number}
     */
    getMaxTransferAmount() {
        // TODO: Round it off based on transfer increment ?
        return this._balanceData.maxTransfer;
    };


    /**
     * Returns the increment by which transfer amount is allowed to be changed.
     * @public
     * @return {number}
     */
    getMinTransferIncrement() {
        return this._model.getTotalBetMin();
    };


    // TODO: This is now indicated by the data returned by the server
    // (although currently, not very well for the V2 model..)
    /**
     * Returns the maximum amount of credit allowed that a player may transfer in total
     * to any game session. This is across multiple transfers, and includes starting a
     * session, and adding more credit to it. Some countries impose rules here (eg: Italy).
     * If no rule is currently in use (and there is no limit to how much a player may
     * transfer), then this will return a value of Number.POSITIVE_INFINITY
     * @public
     * @return {number}
     */
    getMaxTransferPerSession() {
        return this._balanceData.maxTransferInSession;
    };


    /**
     * Action to be invoked by the "Transfer" button in the Cashier View's footer.
     * @public
     */
    transferCredit() {
        if (this.getState() === CashierViewState.SELECT_TRANSFER_AMOUNT) {
            this.enterState(CashierViewState.TRANSFERRING_CREDIT);
        }
    };


    // TODO: This would be better replaced with a "top right button config"
    // type obbject being returned.
    /**
     * Indicates if the Exit Client button in the top right corner should be available.
     * If true, then the close button will not be available.
     * @public
     * @return {boolean}
     */
    isExitClientButtonAvailable() {
        return this._isExitClientButtonAvailable;
    };


    /**
     * Action to invoke when the exit client button is pressed.
     * @public
     */
    actionOnExitClientButtonPressed() {
        if (this._exitClientAction) {
            this.log.info('CashierView.actionOnExitClientButtonPressed()');
            this._exitClientUtil.exitClient(this._exitClientAction);
        }
        else
        {
            this.log.warn('CashierViewModel.actionExitClientButtonPressed: exit client action is not configured!');
        }
    };


    /**
     * Indicates if the Close button in the top right corner should be visible.
     * @public
     * @return {boolean}
     */
    isCloseButtonAvailable() {
        return this.isAddCreditCashier() && this.isExitClientButtonAvailable() === false;
    };


    /**
     * Action to be invoked, when the "Close" button in the top right corner is pressed.
     * @public
     */
    actionOnCloseCashierPressed() {
        if (this.isCloseButtonAvailable() &&
            this.getState() === CashierViewState.SELECT_TRANSFER_AMOUNT) {
            this.closeCashierAndReturnControlToGame();
        }
    };


    /**
     * Action to check if the CashierViewModel is in add credit mode.
     * @public
     */
    isAddCreditCashier() {
        return this._model.isSessionInProgress();
    };


    /**
     * Action if transfer of credit possible 
     * @public
     */
    isAddCreditPossible() {
        //When maximum amount player can transfer than the minimimum allowed transfered amount

        // TODO: Implement / document this...
        return true;
    };
};