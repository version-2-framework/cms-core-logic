import * as Parse from '../utils/ParseUtil';
import C_GameEvent from '../const/C_GameEvent';
import C_CommsMode from '../const/C_CommsMode';
import * as C_MessageField from '../const/C_MessageField';
import * as C_ClientErrorCode from '../const/C_ClientErrorCode';
import noop from '../utils/noop';
import { WmgEncryptor } from '../utils/WmgEncryptor';
import * as LogManager from "../logging/LogManager";

const log = LogManager.getCommsLogger();

/**
 * Default amount of time to wait, before declaring a request as having failed.
 */
const DEFAULT_REQUEST_TIMEOUT_MS = 60000;


let internalTargetUrl = null;

/**
 * Sets the url to use, when the build is internal.
 * @param {string} url 
 */
export function setInternalTargetUrl(url) {
    log.info(`Comms.setInternalTargetUrl to ${url}`);
    internalTargetUrl = url;
}

/**
 * @implements {CommsApi}
 */
class Comms
{
    /**
     * 
     * @param {Dependencies} dependencies 
     */
    constructor(dependencies)
    {
        this._events = dependencies.dispatcher;

        this._buildInfo = dependencies.buildInfo;
        
        // TODO: Model should no longer be required here
        this._model = dependencies.model;
        
        // TODO: Do this differently (if its ever done at all)
        this._localResponseProvider = dependencies.localResponseProvider;
        
        /**
         * @type {Encryptor}
         */
        this._encryptor = new WmgEncryptor();
    };

    /**
     * Sends a service request. Internally, this can be directed to a real server,
     * or a local response provider.
     * @param {string} targetUrl 
     * @param {Object} requestData 
     * @param {CustomXmlHttpHeaders} customHeaders
     * @param {CommsSuccessHandler} onSuccess 
     * @param {CommsFailureHandler} onFailure
     */
    sendRequest(targetUrl, requestData, customHeaders, onSuccess, onFailure)
    {
        let requestStartTime = new Date().getTime();
        let url = internalTargetUrl? internalTargetUrl : targetUrl;

        log.debug(`starting service request @ ${requestStartTime}`);

        // TODO: The API documentation might want changing: but right now, the idea
        // of defaulting to fatal error or no error handler is supplied, seems quite
        // neat. It means we can jump into the game clients default fatal error state
        // always, OR we can supply a custom handler (ie: allowing the possibility of
        // specific error messages in certain contexts, including the option of "try
        // again". However, "try again" needs defining better: for example, a simple
        // comms failure should probably allow this (although its not required at first).
        // To implement such, would require more detail in the error objects returned.
        /** @param {ServiceExecutionError} clientError */
        let actionOnFailure = clientError => {
            if (onFailure) {
                log.debug('Invoking custom service failure handler');
                onFailure(clientError);
            }
            else
            {
                // TODO: Maybe change this event to "UnhandledCommsError" ?
                log.debug('No failureHandler supplied, resorting to custom Fatal Error failure handler');
                this._events.emit(C_GameEvent.FATAL_ERROR, {clientError});
            }
        }

        // We might get a response from the server, where an error is attached.
        // This means that comms is ok, and there is no other runtime error, but
        // the server reported that the operation could not be carried out for
        // some reason. We intercept this case in advance within the comms layer,
        // so that we can invoke onSuccess or onFailure separately.
        let actionOnResponse = response => {
            let requestFinishTime = new Date().getTime();
            let responseTime = requestFinishTime - requestStartTime;
            let serverError = this.getServerErrorFor(response);

            log.debug(`service response received @ ${requestFinishTime}, after ${responseTime} ms`);

            if (serverError) {
                log.error(`Server returned error: ${JSON.stringify(serverError)}`);
                log.error(`Full response = ${JSON.stringify(response)}`);
                actionOnFailure(serverError);
            }
            else {
                log.debug('Server has executed the requested service without error');
                if (onSuccess) {
                    onSuccess(response);
                }
            }
        };

        let commsMode = this._model.getCommsMode();
        if (commsMode === C_CommsMode.SERVER) {
            return this.sendRequestToServer(url, requestData, customHeaders, actionOnResponse, actionOnFailure);
        }
        else if (commsMode === C_CommsMode.LOCAL) {
            return this.getLocalResponse(requestData, actionOnResponse, actionOnFailure);
        }
        else if (commsMode === C_CommsMode.DEBUG) {
            // todo:
            // implement this one.
            log.error('Debug comms mode not implemented');
            actionOnFailure({
                code : "CL00",
                description : "Attempting to use DEBUG comms, but not implemented",
                isFatal : true
            });
        }
        else
        {
            log.error(`unrecognized comms mode ${commsMode}`);
            actionOnFailure({
                code : "CL00",
                description : `Unknown comms mode ${commsMode} selected`,
                isFatal : true
            });
        }
    };


    /**
     * Sends a request directly to the server. For this operation, the request will
     * be turned to JSON and encrypted. The response must also be decrypted, and
     * parsed back to a data object. There are a number of additional errors which
     * could happen in this process.
     * @private
     * @param {string} targetUrl 
     * @param {Object} requestData 
     * @param {CustomXmlHttpHeaders} customHeaders
     * @param {CommsSuccessHandler} onSuccess 
     * @param {CommsFailureHandler} onFailure 
     */
    sendRequestToServer(targetUrl, requestData, customHeaders, onSuccess, onFailure)
    {
        log.info(`sendRequestToServer(target:${targetUrl}, request:${JSON.stringify(requestData)})`);

        /** @type {ServiceExecutionError} */
        let clientError = null;
        let jsonRequestString, encryptedRequestString;
        let requestIsComplete = false, requestIsAborted = false;
        let commsAction =
        {
            abort : noop
        };

        // 1) Convert the request data object into JSON
        try {
            jsonRequestString = JSON.stringify(requestData);
            log.debug("request converted to Json string OK");
        }
        catch (e) {
            log.error("Comms: Failed to convert request into valid JSON");
            clientError = {
                code : C_ClientErrorCode.REQUEST_DATA_WAS_MALFORMED,
                description : "Failed to convert request into valid JSON",
                isFatal : true
            };
        }

        // 2) If request was converted to a JSON string, then encrypt it.
        if (!clientError) {
            try {
                encryptedRequestString = this._encryptor.encrypt(jsonRequestString);
                log.debug("request encrypted OK");
            }
            catch (e) {
                log.error("Comms: failed to encrypt JSON message");
                clientError = {
                    code : C_ClientErrorCode.REQUEST_DATA_COULD_NOT_BE_ENCRYPTED,
                    description : "Failed to encrypt request data before sending",
                    isFatal : true
                }
            }
        }

        // 3) If request was succesfully prepared, then send it to the server.
        if (!clientError)
        {
            let req = new XMLHttpRequest();
            req.timeout = DEFAULT_REQUEST_TIMEOUT_MS;
            req.open("POST", targetUrl, true);
            req.setRequestHeader("Content-type", "application/json");

            // add additional request headers
            Object.keys(customHeaders).forEach(headerKey => {
                req.setRequestHeader(headerKey, customHeaders[headerKey]);
            });

            req.onreadystatechange = () => {
                if (req.readyState === XMLHttpRequest.DONE) {
                    if (req.status === 200) {
                        let decryptedResponseString, finalResponseString, rawResponseString = req.responseText;

                        log.info("got response from server");
                        log.debug(`encrypted response = ${rawResponseString}`);
                        
                        // 1) Try and decrypt the response string.
                        try {
                            decryptedResponseString = this._encryptor.decrypt(rawResponseString);
                            log.debug(`Successfully decrypted response: ${decryptedResponseString}`);
                        }
                        catch (e) {
                            log.error(`Could not decrypt response received from the server: ${e}`, e);
                            clientError = {
                                code : C_ClientErrorCode.RESPONSE_COULD_NOT_BE_DECRYPTED,
                                description : "Failed to decrypt response message",
                                isFatal : true
                            };
                        }
                    
                        // 2) If response string was decrypted, then parse it to JSON.
                        if (!clientError) {
                            try {
                                finalResponseString = JSON.parse(decryptedResponseString);
                                log.debug("Response parsed from Json OK");
                            }
                            catch (e) {
                                log.error(`Response was decrypted, but could not be parsed as JSON: ${e}`);
                                clientError = {
                                    code : C_ClientErrorCode.RESPONSE_DATA_WAS_MALFORMED,
                                    description : "Decrypted response data was not valid JSON",
                                    isFatal : true
                                }
                            }
                        }

                        // 3) If response string was turned in to valid JSON, then this
                        //    is success, else its failure.
                        if (!clientError) {
                            log.debug(`received valid JSON response: ${JSON.stringify(finalResponseString)}`);
                            onSuccess(finalResponseString);
                        }
                        else {
                            onFailure(clientError);
                        }
                    }
                    // This is a comms error.
                    else {
                        if (requestIsAborted) {
                            log.debug(`succesfully aborted request`);
                        }
                        else {
                            log.error(`request failed with status ${req.status}`);
                            onFailure({
                                code : C_ClientErrorCode.REQUEST_SEND_FAILED,
                                description : `Failed to send request packet (status:${req.status})`,
                                isConnectionError : true,
                                isFatal : false
                            });
                        }
                    }
                }
            };

            commsAction.abort = () =>
            {
                if (!requestIsComplete) {
                    requestIsAborted = true;
                    req.abort();
                }
                else {
                    log.warn('[Comms].getServerResponse: cannot abort action, is already completed');
                }
            };

            req.send(encryptedRequestString);
        }
        // Otherwise, handle the failure.
        else
        {
            onFailure(clientError);
        }

        return commsAction;
    };


    /**
     * Fetches a response from the Local Response Provider.
     * @param {*} requestData 
     * @param {*} onSuccess 
     * @param {*} onFailure 
     */
    getLocalResponse(requestData, onSuccess, onFailure)
    {
        let messageType = requestData[C_MessageField.MESSAGE_TYPE];
        let responseData = this._localResponseProvider.getResponseFor(messageType, requestData);
        let abort = noop;
        let requestIsComplete = false;
        let simulateSlowResponse = false;
        
        log.debug(`[Comms].local.sending: ${JSON.stringify(requestData)}`);
        log.debug(`[Comms].local.received: ${JSON.stringify(responseData)}`);

        // For development builds, we force everything to have slow response
        // times. It's unpleasant, but it allows us to check if all of the
        // behaviour we want thrown in (to trick player, when there is slow
        // internet) works as intended.
        if (simulateSlowResponse) {
            let timeout = setTimeout(() => {
                if (onSuccess) {
                    requestIsComplete = true;
                    onSuccess(responseData);
                }
            }, 3000);

            abort = () => {
                if (requestIsComplete) {
                    log.warn('[Comms].getLocalResponse.abort(): cannot abort an already completed action');
                }
                else {
                    log.info('[Comms].getLocalResponse.abort(): action not completed, aborting');
                    clearTimeout(timeout);
                }
            };
        }
        // Any other build, just return the results immediately.
        else if (onSuccess) {
            onSuccess(responseData);
        }

        return { abort };
    };


    /**
     * Fetches the server error in a response. Implementation changes according to comms
     * module in use. (ie: the structure of the basic data returned by the server).
     * Override in concrete-sub-classes of Comms, to implement the correct check.
     * @protected
     * @param {Object} response 
     * @return {ServiceExecutionError | null}
     * A object indicating the error with executing the server, or null, if no error exists
     * in the response received from the server.
     */
    getServerErrorFor(response) {
        return null;
    }
}

/**
 * Noop implementation of the Encryptor interface.
 * @implements {Encryptor}
 */
class NoopEncryptor
{
    encrypt(input) { return input };
    decrypt(input) { return input };
};

/**
 * Comms module to use for Gen 1 platform.
 */
export class CommsGen1 extends Comms
{
    /**
     * Extracts any Comms Error the Gen1 platform.
     * @param {Gen1ServiceReply} response 
     * @return {ServiceExecutionError | null}
     */
    getServerErrorFor(response) {
        return Parse.getGen1CommsErrorFor(response);
    }
}

/**
 * Comms module to use for Gen 2 platform.
 */
export class CommsGen2 extends Comms
{
    /**
     * Extracts any Comms Error for the Gen2 platform.
     * @param {Gen2ServiceReply} response 
     * @return {ServiceExecutionError | null}
     */
    getServerErrorFor(response) {
        return response.error;
    }
}