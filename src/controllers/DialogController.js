/**
 * Dependencies required for default notificaiton controller.
 * @typedef DefaultDialogControllerDependencies
 * @property {Model} model
 * @property {EventEmitter} dispatcher
 * @property {DialogManager} dialogManager
 */

import { getAppLogger } from "../logging/LogManager";
import { SgdDialogManager, WmgDialogManager } from "./DialogManager";
import * as C_SgdGameEvent from "../const/C_SgdGameEvents";
import * as C_ClientErrorCode from "../const/C_ClientErrorCode";

const log = getAppLogger();

/**
 * Default implementation of a Dialog Controller. Think of this Controller as something rather like
 * a view: our main AppController can ask it to do something (eg: "show post game notifications") -
 * and can then wait for an asynchronous event to indicate that this has been done.
 * 
 * The actual DialogController is expected, therefore, to respond to a limited, well defined set of
 * input events - and to fire output events when it has completed its actions. How it achieves those
 * actions, is a detail of the implementation. It may well be that only one implementation of the
 * DialogController will ever be required (because we may neve need to change the logic about what
 * notifications it will show, or we can achieve sufficient customization with a few configuration
 * parameters).
 * 
 * What is a good justification for a different Dialog Controller implementation? If the logic of
 * what dialogs to show (eg: what notifications to show post game) changes so dramatically that it
 * would be cleaner to write a separate implementation, instead of trying to configure the existing
 * implementation. This is obviously not a clear cut answer, so we will have to excercise a bit of
 * discretion here as we move forward.
 * 
 * There is another small factor here: the actual output (HOW we show a dialog) does change. The
 * method chosen to implement this, is that DialogController has direct access to an instance
 * of a "DialogManager". This is a tiny api, that allows us to directly ask to show a sequence of
 * dialogs (we could do this at this level with events, for various reasons the direct call approach
 * has been chosen). This might scale better if we ever need more specific things to be available on
 * our notification api.
 * 
 * Under the hood, DialogManager DOES change implementation
 * - we have a default WMG DialogManager
 * - we have separate implementations whenever we need to integrate an external game GUI, eg:
 *   SGDigital gets its own DialogManager implementation, because the way it has to show
 *   notifications is quite different.
 * 
 * The above is the old comments. In addition, "post game" notifications, and handling of error messages
 * differs between the 2 cases. So, the currently tweaked implementation works as follows:
 * - DialogManager is "how" we show a dialog: its an api to the underlying mechanism
 * - DialogController is "what" we show for certain points
 * - we have WmgDialogController, and SgdDialogController
 * - these both create the appropriate DialogManager by default, so currently they are strongly linked
 * - in future, its definitely possible to have different DialogControllers, but using the default
 *   WmgDialogManager (this kinda makes sense - I can see poetntial use cases for this)
 * - we can still, in principle, have the same DialogController, using different (injected) Dialog
 *   Manager implementations
 * For now, the last 2 points are not yet needed: when there is a real requirement to move in that direction,
 * its possible, for now, the basic architecture of the abstraction exists.
 */

/**
 * Abstract base class, for specific Dialog Controller implementations to use.
 */
export class AbstractDialogController
{
    constructor(dependencies)
    {
        /**
         * @protected
         * @type {EventEmitter}
         */
        this._events = dependencies.dispatcher;

        /**
         * @protected
         */
        this.log = log;

        /**
         * @protected
         * @type {Model}
         */
        this._model = dependencies.model;

        /**
         * @type {LocaleApi}
         * @protected
         */
        this._locale = dependencies.locale;

        /**
         * The Dialog Manager Api that will be used by the dialog controller.
         * @protected
         * @type {DialogManager}
         */
        this._dialogManager = null;
    };


    /**
     * Shows a default service execution error dialog.
     * @public
     * @param {ServiceExecutionError} error 
     * @param {()=>void} [actionOnRetry]
     */
    showDefaultServiceExecutionErrorDialog(error, actionOnRetry=null)
    {
        this.log.info(`showDefaultServiceExecutionErrorDialog(${JSON.stringify(error)},retryOptionSupplied:${actionOnRetry!=null})`);

        /** @type {DialogViewModel} */
        let dialog;

        /** @type {()=>void} */
        let postDialogAction;

        // TODO: In theory, DialogController ought to be handling this?
        // eg: we just send an abstract instruction (show error), passing along
        // a possible callback. Let the implementation decide what actual message
        // should be shown there - we will need notifications of all types in any
        // case
        if (error && error.isConnectionError && actionOnRetry)
        {
            dialog = {
                type : "connectionError",
                header : { localizationId:"T_dialog_NoInternetError_Title" },
                message : { localizationId:"T_dialog_NoInternetError_Content" },
                buttonText : { localizationId:"T_button_tryAgain" }
            }

            postDialogAction = actionOnRetry;
        }
        else
        {
            let errorCode = error && error.code? error.code : C_ClientErrorCode.SERVER_RETURNED_GENERIC_ERROR;
            
            // TODO: Now we need to support a "fatalError" dialog view type
            dialog = {
                type : "fatalError",
                errorCode,
                header : {
                    localizationId:"T_dialog_FatalError_Title",
                    localizationMods : {
                        "[ERROR_CODE]" : error.code
                    }
                },
                message : { localizationId:"T_dialog_FatalError_Content" }
            }
        }

        this._dialogManager.showDialogs([dialog], postDialogAction);
    }
}


/**
 * Standard Wmg implementation of the Dialog Controller.
 * @implements {DialogController}
 */
export class WmgDialogController extends AbstractDialogController
{
    constructor(dependencies)
    {
        super(dependencies);

        /**
         * @type {DialogManager}
         */
        this._dialogManager = new WmgDialogManager(dependencies);
    };


    /**
     * @public
     * @inheritDoc
     * @param {()=>void} onDismissed 
     */
    showResumeGameNotification(onDismissed)
    {
        this._dialogManager.showDialogs([{
            type : "dismissableNotification",
            message : {
                localizationId : "T_dialog_GameInProgress_Content"
            }
        }], onDismissed);
    };


    /**
     * @public
     * @inheritDoc
     * @param {DialogViewModel[]} dialogConfigs 
     * @param {() => void} postDialogAction 
     */
    showDialogs(dialogConfigs, postDialogAction)
    {
        this._dialogManager.showDialogs(dialogConfigs, postDialogAction);
    };


    /**
     * @public
     * @param {ServerToClientNotification[]} notifications 
     * @param {() => void} postDialogAction 
     */
    showServerNotifications(notifications, postDialogAction)
    {
        log.debug(`DialogCOntroller.showServerNotifications()`);

        this._dialogManager.showServerNotifications(notifications, postDialogAction);
    };


    /**
     * @public
     * @inheritDoc
     * @param {():void} onComplete
     */
    showPostGameNotifications(onComplete)
    {
        /** @type {DialogViewModel[]} */
        let notifications = [];

        let autoplaySessionData = this._model.getAutoplaySessionData();
        let freeGamesSessionData = this._model.getFreeGamesSessionData();

        // Why did the autoplay session finished ? If it ended because the player
        // cancelled it, we don't need to show any notification. If it ended due to 
        // autoplay settings being breached (eg: breaching loss limits) we inform
        // the player of the reason why (no further action can be taken by them,
        // though)
        if (autoplaySessionData && autoplaySessionData.aborted)
        {
            /** @type {TextConfig} */
            let message = { localizationId:"T_dialog_AutoplayAborted_Content" };

            let description = autoplaySessionData.abortedDescription;
            if (description.exceededMaxLosses) {
                message.localizationId = "T_dialog_AutoplayAborted_Content_MaxLossesExceeded";
                message.localizationMods = {
                    "[MAX_LOSSES]" : autoplaySessionData.maxLosses,
                    "[ACTUAL_LOSSES]" : autoplaySessionData.losses
                };
            }
            else
            if (description.exceededMaxWinnings)
            {
                message.localizationId = "T_dialog_AutoplayAborted_Content_MaxWinningsExceeded";
                message.localizationMods = {
                    "[MAX_WINNINGS]" : autoplaySessionData.maxWinnings,
                    "[ACTUAL_WINNINGS]" : autoplaySessionData.winnings
                };
            }
            else
            if (description.cannotAffordNewGame)
            {
                message.localizationId = "T_dialog_AutoplayAborted_Content_InsufficientCredit";
            }

            notifications.push({
                type : "dismissableNotification",
                header : { localizationId:"T_dialog_AutoplayAborted_Title" },
                message
            });
        }
        // Autoplay completed naturally. Offer the player the chance to restart their autoplay
        // session, with the same settings as before. This should be the last notification of
        // the sequence (we would normally expect the new autoplay session to start immediately,
        // it would be weird if we asked for that, but another notification was shown afterwards)
        else
        if (autoplaySessionData && autoplaySessionData.completed)
        {
            // player can afford at least 1 game with current bet settings, so we allow them to
            // start a new autoplay session (repeating the last one). They may not be able to afford
            // all games, but it will abort if they cannot - and they can afford at least 1 game
            // (and might win money allowing it to complete)
            if (this._model.getPlayerWallet() >= this._model.getTotalBet())
            {
                // "Autoplay Completed" - would you like to play again ?
                notifications.push({
                    type : "multiChoice",
                    header : { localizationId : "T_dialog_AutoplayCompleted_Title" },
                    message : { localizationId : "T_dialog_AutoplayCompleted_Content" },
                    options : [{
                        localizationId : "T_button_restartAutoplay",
                        action : () => {
                            this._model.setAutoplayNumGamesToLastSelectedValue();
                        }
                    },
                    {
                        localizationId : "T_button_ok"
                    }]
                });
            }
            // Cannot afford even 1 game with current bet settings. Inform player autoplay session
            // completed, do not offer them chance to start a new one.
            else
            {
                notifications.push({
                    type : "dismissableNotification",
                    header : { localizationId : "T_dialog_AutoplayCompleted_Title" },
                    message : { localizationId : "T_dialog_AutoplayCompleted_Content" },
                });
            }
        }
        else
        if (freeGamesSessionData)
        {
            // TODO: Let's add in a "FreeRounds completed" notification here
            notifications.push({
                type: "dismissableNotification",
                header : { localizationId : "T_dialog_FreeGamesComplete_Title" },
                message : { localizationId : "T_dialog_FreeGamesComplete_Content" },
            });
        }

        log.debug(`DialogController.showPostGameNotifications: got ${notifications.length} notifications to show`);

        // Shows the sequence of all post game notifications, lets app controller know
        // when we are finished.
        this._dialogManager.showDialogs(notifications, () => {
            onComplete();
        });
    };


    /**
     * @public
     * @inheritDoc
     * @param {ServiceExecutionError} error 
     * @param {()=>void} [actionOnRetry]
     */
    showServiceExecutionErrorDialog(error, actionOnRetry=null)
    {
        this.showDefaultServiceExecutionErrorDialog(error, actionOnRetry);
    };
}


/**
 * @typedef SgdServiceExecutionErrorMapping
 * A mapping of a service execution error, to some error data to be be passed to GCM.
 * @property {SgdErrorCategory} errorCategory
 * Error category value, to pass on to the gcm show error call
 * @property {SgdErrorSeverity} errorSeverity
 * Error severity value, to pass on to the gcm show error call
 * @property {string} [errorLocalization]
 * The id of the localization to use for the main message
 */

/**
 * SGDigital implementation of Dialog Controller.
 * @implements {DialogController}
 */
export class SgdDialogController extends AbstractDialogController
{
    constructor(dependencies)
    {
        super(dependencies);

        // There is only one dialog manager instance that is usable here.
        this._dialogManager = new SgdDialogManager(dependencies);

        /**
         * Reference to the GCM api.
         * @private
         * @type {SgdGcmCoreApi}
         */
        this._gcm = dependencies.gcm;

        // TODO: Instead of storing this in memory, we could just create / access it when
        // we actually need to use it ?
        /**
         * Maps service execution error codes, to the specific error data which should be passed
         * to GCM (as well as our own custom localizations of those error messages).
         * @private
         * @type {{[id:string]:SgdServiceExecutionErrorMapping}}
         */
        this._customSgdErrorCodeMap = 
        {
            // Technical error (SGD Error code 1)
            "LM_36" :
            {
                errorCategory : "NON_RECOVERABLE_ERROR",
                errorSeverity : "ERROR"
            },

            // OGS Error (SGD error code 2)
            "LM_37" :
            {
                errorCategory : "NON_RECOVERABLE_ERROR",
                errorSeverity : "ERROR"
            },

            // Wager not found (sgd error code 102)
            "LM_38" :
            {
                errorCategory : "RECOVERABLE_ERROR",
                errorSeverity : "ERROR",
                errorLocalization : "T_dialog_errorSgd_pleaseTryAgain"
            },

            // Operation not allowed (sgd error code 110)
            "LM_39" :
            {
                errorCategory : "CRITICAL",
                errorSeverity : "ERROR"
            },

            // Invalid expired session id (sgd error code 1000)
            "LM_40" :
            {
                errorCategory : "LOGIN_ERROR",
                errorSeverity : "ERROR",
                errorLocalization : "T_dialog_errorSgd_notLoggedIn"
            },

            // Authentification failed (SGD error code 1003)
            "LM_41" :
            {
                errorCategory : "CRITICAL",
                errorSeverity : "ERROR"
            },

            // Out of money (SGD error code 1006)
            "LM_42" :
            {
                errorCategory : "INSUFFICIENT_FUNDS",
                errorSeverity : "INFO",
                errorLocalization : "T_dialog_errorSgd_insufficientFunds"
            },

            // Unknown currency (SGD error code 1007)
            "LM_43" :
            {
                errorCategory : "CRITICAL",
                errorSeverity : "ERROR"
            },

            // Missing parameter (SGD Error code 1008)
            "LM_44" :
            {
                errorCategory : "CRITICAL",
                errorSeverity : "ERROR"
            },

            // Gaming Limits (SGD Error code 1019)
            "LM_45" :
            {
                errorCategory : "CRITICAL",
                errorSeverity : "ERROR",
                errorLocalization : "T_dialog_errorSgd_responsibleGamingSessionExpired"
            },

            // Account blocked (SGD Error code 1035)
            "LM_46" :
            {
                errorCategory : "CRITICAL",
                errorSeverity : "ERROR",
                errorLocalization : "T_dialog_errorSgd_accountBlocked"
            },

            // Geolocation Failure (SGD Error code 1041)
            "LM_47" :
            {
                errorCategory : "CRITICAL",
                errorSeverity : "ERROR"
            },

            // Temporarily busy (SGD Error Code 3)
            "LM_48" :
            {
                errorCategory : "NON_RECOVERABLE_ERROR",
                errorSeverity : "ERROR"
            },

            // "Excluded" (SGD Error Code 1109)
            "LM_49" :
            {
                errorCategory : "CRITICAL",
                errorSeverity : "ERROR"
            }
        };
    }


    /**
     * @public
     * @inheritDoc
     * @param {()=>void} onDismissed 
     */
    showResumeGameNotification(onDismissed)
    {
        // NOTE: The SGDigital "other game in progress" error category causes the
        // game client to get reloaded, which is... weird. I couldn't find anything
        // in their documentation to suggest that this would happen, although if I
        // understand right, it maybe that they have a different concept of how games
        // are reloaded. In any case, the second block of code here works the way I
        // *thought* it was supposed to work (with the dedicated notification), the
        // currently active block of code works the way that it *should* work.
        this._dialogManager.showDialogDirect(
            "RECOVERABLE_ERROR", "INFO", "", "T_dialog_GameInProgress_Content", null, onDismissed);

        //------------------------------------------------------------------------------
        // NOTE: This is deprecated, until I am told otherwise about how it must work
        //------------------------------------------------------------------------------
        // SGDigital have a specific "error category" for resuming a game, so we use
        // the special "showDialogDirect" call: this allows us to call the dialog
        // exactly how SGDigital want it, but tap into our custom "onDismissed" event
        // pipeline (and also, if GCM is not yet initialized, it will automatically
        // be initialized to allow this to be shown)
        /*
        this._dialogManager.showDialogDirect(
            "OTHER_GAME_IN_PROGRESS", "INFO", "", "T_dialog_GameInProgress_Content", null, onDismissed);
        */
    };


    /**
     * @public
     * @inheritDoc
     * @param {DialogViewModel[]} dialogConfigs 
     * @param {()=>void} postDialogAction 
     */
    showDialogs(dialogConfigs, postDialogAction)
    {
        this._dialogManager.showDialogs(dialogConfigs, postDialogAction);
    };


    /**
     * @public
     * @inheritDoc
     * @param {()=>void} onComplete
     */
    showPostGameNotifications(onComplete)
    {
        // No custom dialogs (yet) to show for SG Digital.
        onComplete();
    };


    /**
     * @public
     * @inheritDoc
     * @param {ServiceExecutionError} error 
     * @param {()=>void} [actionOnRetry]
     */
    showServiceExecutionErrorDialog(error, actionOnRetry=null)
    {
        // This was here for manually testing out some of the custom error cases
        // [ "LM_38", "LM_40", "LM_42", "LM_45", "LM_46" ]
        // TESTING
        /*
        error = 
        {
            isConnectionError : false,
            code : "LM_45",
            description : "",
            isFatal : false
        }
        */

        // When we get a ServiceExecutionError that uses on of the custom SGDigital error
        // codes, then we need to show a custom error message according to their specification.
        // Unfortunately, it seems we basically have to implement exactly what they want (even
        // though their code base could do this for us). Dialog Manager's mechanism for showing
        // dialogs is deliberately abstract: it can handle errors, but it will not map them
        // exactly as SGD want for the cases of the specific error codes we must handle - nor
        // should its api be expanded to allow this (as this will bloat the api, especially when
        // we need to integrate other similar platforms in the future!!). So in this certain case,
        // we don't use the abstract api and call the GCM methods directly (if its not one of this
        // specific subset of errors, then we just use our own, standard error mechanism)

        if (error.isConnectionError)
        {
            this.log.debug("SgdDialogController.showServiceExecutionErrorDialog: it's a connection error");

            this._dialogManager.showDialogDirect(
                "CONNECTION_ERROR",
                "ERROR",
                null,
                "T_dialog_errorSgd_connectionError",
                null,
                () => {
                    this.log.debug('SgdDialogController: connection error dialog dismissed');

                    // Apparently, we MUST reload: also, it seems that by default, SGD Connection
                    // Error doesn't do this for us (in other cases, it seems to), so it's added
                    // manually here, when the dialog is dismissed.
                    window.location.reload();
                });

        }
        // The error code is one of the custom Sgd error codes that we must handle
        else
        if (error.code in this._customSgdErrorCodeMap)
        {
            let errorMapping = this._customSgdErrorCodeMap[error.code];
            let errorCategory = errorMapping.errorCategory;
            let errorSeverity = errorMapping.errorSeverity;
            let errorLocalization = errorMapping.errorLocalization? errorMapping.errorLocalization : "T_dialog_errorSgd_generic";
            
            // TODO: Does this list need expanding ??
            /** @type {SgdErrorCategory[]} */
            let fatalGcmErrorCategories = [ "CRITICAL", "LOGIN_ERROR", "NON_RECOVERABLE_ERROR" ] ;
            let isFatalGcmErrorCategory = fatalGcmErrorCategories.indexOf(errorCategory) > -1;

            // SGDigital's notification always shows an "OK" button, even when its a non-recoverable
            // fatal error (or rather, it always seems to from the implementations I have seen, and
            // their documentation doesn't explain if this is expected). It's also not explained in
            // their documentation if pressing the OK button to dismiss the popup (even when it was
            // a fatal error type) will always trigger gcm's handle popup dismissed method. For this
            // reason, we will only listen out for that event (to call the action on retry) when we
            // are certain that we really want to respond to it.
            let onDismissed = () => {
                if (error.isFatal === false && actionOnRetry && !isFatalGcmErrorCategory) {
                    this._events.once(C_SgdGameEvent.POPUP_DISMISSED, () => {
                        actionOnRetry();
                    });
                }
            }

            this.log.debug(`SgdDialogController.showServiceExecutionErrorDialog: errorCode ${error.code} has a custom SGD mapping`);

            this._dialogManager.showDialogDirect(errorCategory, errorSeverity, "", errorLocalization, null, onDismissed);
        }
        // The error code is not one of the custom SGD ones: fallback to default error dialog
        else
        {
            this.log.debug(`SgdDialogController.showServiceExecutionErrorDialog: errorCode ${error.code} has not custom SGD mapping, showing default error`);

            this.showDefaultServiceExecutionErrorDialog(error, actionOnRetry);
        }
    };


    /**
     * Shows the custom SGDigital "insufficient credit" error notification.
     * @public
     * @param {()=>void} onDismissed
     * An action to be invoked when the dialog is dismissed.
     */
    showInsufficientCreditDialog(onDismissed)
    {
        this._dialogManager.showDialogDirect(
            "INSUFFICIENT_FUNDS",
            "INFO",
            "",
            "T_dialog_errorSgd_insufficientFunds",
            null,
            onDismissed
        );
    }
}