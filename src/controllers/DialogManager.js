import C_GameEvent from "../const/C_GameEvent";
import * as C_SgdGameEvent from "../const/C_SgdGameEvents";
import { getGameViewLogger } from "../logging/LogManager";
import { ExitClientUtil } from "../utils/ExitClientUtil";

// TODO: This code unit really doesnt belong in controllers. The main problem is Wiener's
// arbitrary insistence on packaging by layer rather than feature: its hard to categorize
// a "DialogManager" (integrating some arbitrary functionality) by this token. If we packaged
// by feature, we would have a DialogManager interface defined somewhere high up in our hierarchy,
// and 

const log = getGameViewLogger();

/**
 * @typedef AbstractDialogManagerDependencies
 * @property {EventEmitter} dispatcher
 * @property {ExitClientUtil} exitClientUtil
 * @property {Locale} locale
 */

/**
 * An abstract class that can be used as a base for a Dialog Manager implementation.
 * Provides basic functionality for looping through a sequence of notifications: cannot
 * actually show a notification, however. Extended implementations need to override the
 * method "showDialogs".
 */
class AbstractDialogManager
{
    /**
     * 
     * @param {AbstractDialogManagerDependencies} dependencies 
     */
    constructor(dependencies)
    {
        /**
         * @private
         * @type {BusinessConfig}
         */
        this._businessConfig = dependencies.businessConfig;

        /**
         * @private
         * @type {PlatformApi}
         */
        this._platformUtil = dependencies.platform;

        /**
         * @protected
         * @type {EventEmitter}
         */
        this._events = dependencies.dispatcher;

        /**
         * @private
         * @type {ExitClientUtil}
         */
        this._exitClientUtil = dependencies.exitClientUtil;

        /**
         * @private
         * @type {DialogViewModel[]}
         */
        this._dialogConfigs = [];

        /**
         * The action to be invoked after all Dialogs have been shown. This must be configured,
         * if the user of a Notification Handler wants something to actually happen at the end.
         * @private
         * @type {() => void}
         */
        this._postDialogAction = null;

        /**
         * @private
         * @type {LocaleApi}
         */
        this._locale = dependencies.locale;
    };


    // TODO: Look at better name
    // TODO: Called from event, or through public api ?
    /**
     * @public
     * @param {DialogViewModel[]} dialogConfigs 
     * @param {() => void} postDialogAction
     */
    showDialogs(dialogConfigs, postDialogAction)
    {
        this._dialogConfigs = dialogConfigs;
        this._postDialogAction = postDialogAction;
        this.showNextDialog();
    };


    /**
     * Shows a set of dialogs - as received from the server - which are in the WMG server format
     * (which must be parsed to our internal format)
     * @public
     * @param {ServerToClientNotification[]} notifications
     * @param {() => void} postDialogAction 
     */
    showServerNotifications(notifications, postDialogAction)
    {
        /** @type {DialogViewModel[]} */
        let dialogViewModels = [];

        notifications.forEach(notification =>
        {
            if (notification && notification.type === "notification")
            {
                dialogViewModels.push(this.parseSimpleNotification(notification));
            }
        });

        log.debug(`DialogManager.showServerNotifications: got ${notifications.length} notifications from server, showing ${dialogViewModels.length}`);

        this.showDialogs(dialogViewModels, postDialogAction);
    };


    /**
     * @private
     * @param {ServerToClientSimpleNotification} notification 
     * @return {DialogViewModel}
     */
    parseSimpleNotification(notification)
    {
        if (notification.actionOnDismiss)
        {
            if (notification.actionOnDismiss === "closePopup")
            {
                return {
                    type : "dismissableNotification",
                    headerString : notification.title,
                    messageString : notification.body
                };
            }
            else
            if (notification.actionOnDismiss === "closeClient")
            {
                // We will fall back to a default exit client action
                let exitClientConfig =
                    this._platformUtil.isDesktop()?
                        this._businessConfig.exitClientActionDesktop :
                        this._businessConfig.exitClientActionMobile;

                // TODO: Using "multiChoice", we always expect the dialog to be dimissed
                // when the button is pressed, which is not exactly the desired behaviour
                // (eg: in the case that "exitClient" fails). I think some of this code -
                // and bits of the dialogmanager / dialogcontrollers - can do with a bit
                // of TLC to improve functionality
                if (exitClientConfig)
                {
                    return {
                        type : "multiChoice",
                        headerString : notification.title,
                        messageString : notification.body,
                        options : [{
                            localizationId : "T_button_ok",
                            action : () => {
                                log.info('DialogManager: attempting to close game client from notification');

                                // TODO: We need to find a default action.. or let the util pick one as
                                // a fallback ??
                                this._exitClientUtil.exitClient(exitClientConfig);
                            }
                        }]
                    }
                }
                else
                {
                    return {
                        type : "undismissableNotification",
                        headerString : notification.title,
                        messageString : notification.body
                    };
                }
            }
            else
            {
                return {
                    type : "dismissableNotification",
                    headerString : notification.title,
                    messageString : notification.body
                };
            }
        }
        else
        {
            return {
                type : "undismissableNotification",
                headerString : notification.title,
                messageString : notification.body
            };
        }
    };


    /**
     * Shows the next available dialog, from the list that need showing. This should be
     * invoked whenever the current dialog is dismissed.
     * @protected
     * @final
     */
    showNextDialog()
    {
        if (this._dialogConfigs.length > 0)
        {
            let nextDialogConfig = this._dialogConfigs.shift();

            this.showDialog(nextDialogConfig);
        }
        else
        {
            log.debug(`all dialogs shown. got post dialogs action? ${this._postDialogAction?'true':'false'}`);

            if (this._postDialogAction) {
                this._postDialogAction();
            }
        }
    };


    /**
     * The method which does the heavy lifting of showing a single dialog. Override this method in
     * sub-classes, in order
     * @protected
     * @param {DialogViewModel} config 
     */
    showDialog(config)
    {
        // TODO: Base implementation should do something semi-useful here - we could simply skip
        // to the next step (if thats possible), or auto-select an option for a multi-choice dialog.
    };


    /**
     * @protected
     * @final
     * @param {TextConfig} textConfig 
     * @return {string}
     */
    getLocalizedString(textConfig)
    {
        return this._locale.getString(textConfig.localizationId, textConfig.localizationMods);
    };
};


/**
 * @typedef {AbstractDialogManagerDependencies} WmgDialogManagerDependencies
 */

/**
 * Wmg specific implementation for showing a notification.
 */
export class WmgDialogManager extends AbstractDialogManager
{
    /**
     * 
     * @param {WmgDialogManagerDependencies} dependencies 
     */
    constructor(dependencies)
    {
        super(dependencies);

        this._events.on(C_GameEvent.DIALOG_VIEW_DISMISSED, this.onDialogDismissed, this);
    };


    /**
     * @private
     */
    onDialogDismissed()
    {
        this.showNextDialog();
    };


    /**
     * @inheritDoc
     * @public
     * @param {DialogViewModel} config 
     */
    showDialog(config)
    {
        // We don't need to do anything special here - we are already passing the callbacks
        // to execute directly.
        this._events.emit(C_GameEvent.SHOW_DIALOG_VIEW, config);
    }
};


/**
 * @typedef SgdDialogManagerDependencies
 * @property {SgdGcmCoreApi} gcm
 * @property {EventEmitter} dispatcher
 * @property {Locale} locale
 */

/**
 * Sg Digital specific implementation of something which shows a notification.
 */
export class SgdDialogManager extends AbstractDialogManager
{
    /**
     * @param {SgdDialogManagerDependencies} dependencies
     */
    constructor(dependencies)
    {
        super(dependencies);

        /**
         * Indicates if GCM.gameReady has already ben called (if it hasn't, we cannot show any dialog
         * to the player).
         * @private
         * @type {boolean}
         */
        this.isGcmReadyToShowDialogs = false;

        // TODO: In fact, i think there is no strict need for us to have access to GCM here - we can
        // call the "showDialog" method via an event
        /**
         * @private
         * @type {SgdGcmCoreApi}
         */
        this.gcm = dependencies.gcm;

        /**
         * @private
         * @type {(errorParamIndex:number)=>void}
         */
        this._actionOnCurrPopupDismissed = null;

        // Listen out for a GCM popup being dismissed.
        this._events.on(C_SgdGameEvent.POPUP_DISMISSED, this.onGcmPopupDismissed, this);

        // Also, check if game has been revealed: this tells us that gcm.gameReady has
        // been called (TODO: It might be better to use a separate "gcm.gameReady has
        // been called" event, but in practise, I *think* that the game is revealed
        // immediately after gcm.gameReady is called - in which case, this is fine)
        this._events.on(C_SgdGameEvent.GAME_REVEALED, this.onGameRevealed, this);
    };


    /**
     * @private
     * @param {number} errorParamIndex 
     */
    onGcmPopupDismissed(errorParamIndex)
    {
        log.debug(`SgdDialogManager.onGcmPopupDismissed(errorParamIndex:${errorParamIndex})`);

        if (this._actionOnCurrPopupDismissed) {
            this._actionOnCurrPopupDismissed(errorParamIndex);
        }
    };


    /**
     * When the game has been revealed, we know we can show any dialog through GCM, without
     * needing to call "gcm.gameReady" (which is something we have to do in certain edge cases,
     * where we need to show some kind of dialog before the game has been shown).
     * @private
     */
    onGameRevealed()
    {
        log.debug('SgdDialogManager.onGameRevealed()');

        this.isGcmReadyToShowDialogs = true;
    };


    /**
     * @private
     */
    ensureGcmReadyToShowDialogs()
    {
        // There are special cases, where an error may need to be shown early on in the life
        // time of the SGDigital Game Client. The only condition in which we should be able
        // to reach this, is one which is "fatal", simply because we are going to enable the
        // GUI (in order to show the dialog) - and the GUI has already been enabled (eg: we
        // have not reached the step in the Game Client's life-cycle where it should be
        // enabled). In practise, "non-fatal" comms errors may occur at this point, implying
        // that we might prematurely enable the GUI. Will need to seek guidance from the SGD
        // guys about this edge case - they confirmed its ok to enable GCM to show the error
        // dialog in this condition, but the conversation only really touched on the idea of
        // "fatal errors".
        // Russell - 22.07.2020
        if (this.isGcmReadyToShowDialogs === false) {
            this.isGcmReadyToShowDialogs = true;
            this.gcm.gameReady();
        }
    }


    /**
     * Does the actual task of showing a dialog/
     * @override
     * @param {DialogViewModel} config 
     */
    showDialog(config)
    {
        this.ensureGcmReadyToShowDialogs();

        // For SG Digital integrations, we call a method on the GCM instance to show a dialog.
        // The Sgd dialog api has its own concept of input parameters: because we use our own
        // abstract format for dialog configuration (DialogViewModel), we have to perform some
        // mapping from our design, to the Sgd design.

        let dialogMessage = config.messageString ? config.messageString : this.getLocalizedString(config.message);
        let dialogParams = null;

        /** @type {SgdErrorCategory} */
        let sgdErrorCategory = null;

        /** @type {SgdErrorSeverity} */
        let sgdErrorSeverity = null;

        let errorCode = null;

        if (config.type === "multiChoice")
        {
            dialogParams = {
                options: config.options.map(option => {
                    return this.getLocalizedString(option);
                })
            };

            sgdErrorCategory = "MULTI_CHOICE_DIALOG";
            sgdErrorSeverity = "INFO";

            this._actionOnCurrPopupDismissed = errorParamIndex => {
                // There may be a custom action linked to the button (its not guaranteed, because
                // it is not 100% essential to have - we always dismiss the dialog, based on the
                // button being pressed.
                let action = config.options[errorParamIndex].action;
                if (action) {
                    action();
                }

                this.showNextDialog();
            }
        }
        else
        {
            if (config.type === "connectionError") {
                
                // In principle we should map this to a CONNECTION_ERROR, but I want to use a retry
                // button, so i am using "multi-choice dialog" instead

                let buttonText = config.buttonText?
                    this.getLocalizedString(config.buttonText) : this.getLocalizedString({ localizationId:"T_button_OK" });

                //sgdErrorCategory = "CONNECTION_ERROR";
                sgdErrorCategory = "MULTI_CHOICE_DIALOG";
                sgdErrorSeverity = "INFO";
                dialogParams = { options : [buttonText] }
            }
            else
            if (config.type === "dismissableNotification" ||
                config.type === "undismissableNotification") {
                sgdErrorCategory = "RECOVERABLE_ERROR";
                sgdErrorSeverity = "INFO";
            }
            else
            if (config.type === "fatalError") {
                sgdErrorCategory = "NON_RECOVERABLE_ERROR";
                sgdErrorSeverity = "ERROR";
            }
            // TODO: Map other future dialog types here

            this._actionOnCurrPopupDismissed = () => {
                this.showNextDialog();
            }
        }

        log.debug(`showing Gcm Dialog: (errorCategory:${sgdErrorCategory},errorSeverity:${sgdErrorSeverity})`);

        this.gcm.handleError(sgdErrorCategory, sgdErrorSeverity, errorCode, dialogMessage, dialogParams);
    };


    /**
     * A direct call to GCM's show dialog method. This method is not part of the standard DialogManager API:
     * it's presented only for use by components (eg: SGDigitalDialogController) which may need direct access
     * to the actual api call. This COULD be done through GCM, however: by placing it here, we can guarantee
     * that it takes care of initializing GCM for us (if it's not already initialixed), just like the standard
     * "showDialogs" method does.
     * @public
     * @param {SgdErrorCategory} errorCategory
     * @param {SgdErrorSeverity} errorSeverity
     * @param {string} errorCode
     * @param {string} errorLocalizationId
     * @param {Object} errorParams
     * @param {()=>void} onDismissed
     */
    showDialogDirect(errorCategory, errorSeverity, errorCode, errorLocalizationId, errorParams, onDismissed)
    {
        let errorMessage = this.getLocalizedString({ localizationId: errorLocalizationId });

        this.ensureGcmReadyToShowDialogs();

        log.debug(`SgdDialogManager.showDialogDirect(errorCategory:${errorCategory}, errorSeverity:${errorSeverity}, errorCode:${errorCode})`);

        this.gcm.handleError(errorCategory, errorSeverity, errorCode, errorMessage, errorParams);

        this._actionOnCurrPopupDismissed = onDismissed;
    }
};