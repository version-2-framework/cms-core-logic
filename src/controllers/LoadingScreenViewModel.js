//--------------------------------------------------------------------------------------------------
// Loading Screen View Model
//--------------------------------------------------------------------------------------------------
// This is a ViewModel style controller, for the LoadingScreenView (the very first thing shown in
// the game client, to the player). It is also one of the few classes that we have slightly
// different extended implementations for, depending on the platform targeted. Although we pass
// a business configuration object for the game client, most of these options are platform agnostic.
// 
// There are a few dedicated use cases, where we must handle things slightly different according to
// the platform that the game client is being deployed to, and only using business config to achieve
// this becomes complicated (even if we wanted to load alternateive business configs depending on
// whether the game client is on V1 or V2 platform, we must still specify which is loaded!)
//
// The simplest solution, proves to be to deploy slight variations on LoadingScreenViewModel: its
// at exactly this point in the Game Client's life-cycle, that we
// - load assets
// - let the player select session type
// - configure information that is static for the life-cycle of the game client
// By using Loading Screen View Model to achieve this, we centralize a lot of this variance into
// a single changing point, AND it feels semantically appropriate for it to be handled here.
//
// Once initial game assets are loaded, the Loading Screen View (or whichever component is taking
// charge of loading assets) must call the public method "actionOnAssetsLoaded": this triggers the
// initial set of state transitions for Loading Screen View Model.
//--------------------------------------------------------------------------------------------------

///<reference path='../types/types.d.ts'/>
import BaseController from "./BaseController";
import C_GameEvent from "../const/C_GameEvent";
import C_SessionType from "../const/C_SessionType";
import * as GamePhase from "../const/C_SlotGamePhaseId";
import * as C_V1GameModeEnum from "../const/C_V1GameModeEnum";

/**
* View States for the Loading / Splash Screen.
*/
export const LoadingScreenViewState =
{
    /**
     * This state indicates that game assets are being loaded. The Game Client
     * may send some messages to the server in the background, while we are in
     * this state.
     */
    LOADING_ASSETS : "LoadingState.LoadingAssets",

    /**
     * On certain internal testing builds, we want to show the url selection
     * screen to the user (so they can pick a custom endpoint).
     */
    SELECT_SERVER_URL : "LoadingState.SelectServerUrl",

    /**
     * Special state (not always shown), where we show an advert to the player for
     * another game.
     */
    ADVERT : "LoadingState.Advert",
       
    /**
     * In this state, the Loading Screen presents the player which choices about
     * which Session Type they would like to play. Some deployments of the game
     * client may offer more than once choice here. Others may only offer "start
     * freeplay" mode.
     */
    SELECT_SESSION_TYPE : "LoadingState.SelectSessionType",

    /**
     * In this state, the Loading Screen will show a message to the player: "starting
     * a session". The View Model performs a "handshake" with the server of some kind
     * (potentially initializing a new session, or possible requesting info about if
     * there is a game to restore). The Loading Screen View implementation should
     * visually disable user input (as the user may not interact at this point).
     */
    STARTING_SESSION : "LoadingState.StartingSession",


    /**
     * In this state, the loading screen will briefly show the session type as text to the player and fade out. 
     * Once the session type text has faded out the loading state will be changed to starting session. The label for only one 
     * session type will be shown.
     */
    SHOW_QUICK_LOAD : "LoadingState.ShowQuickLoad"


};

export const LoadingScreenViewEvent =
{
    /**
     * Event fired when the ViewModel enters Select Server State. In this state,
     * the Loading Screen View implementation should show a screen that allows
     * the user to select a url as a server endpoint. This feature tends to exist
     * on testing builds (but definitely not in production builds)
     */
    ENTER_SELECT_SERVER_URL : "LoadingScreen.EnterSelectServerUrl",

    /**
     * Event fired when the ViewModel exits the Select Server state. When this
     * event is received, the Loading Screen View implementation should hide the
     * select url view.
     */
    EXIT_SELECT_SERVER_URL : "LoadingScreen.ExitSelectServerUrl",

    /**
     * Event fired when the ViewModel enters the Advert State. This shows a special
     * custom advert (generally for another game) over the top of the loading screen.
     */
    ENTER_ADVERT_STATE : "LoadingScreen.EnterAdvertState",

    /**
     * Event fired when the ViewModel exits the Advert State.
     */
    EXIT_ADVERT_STATE : "LoadingScreen.ExitAdvertState",

    /**
     * Event fired when the ViewModel enters the Select Game Mode state. In this
     * visualm state, the View Implementation must show a selection of buttons to
     * the player, allowing them to select Session Type (also known as Game Mode).
     * The list of buttons are returned by the ViewModel.getStartSessionButtonConfigs.
     * In some cases, only one option will be selected, in which case the button
     * config will likely return text simply saying "play".
     */
    ENTER_SELECT_GAME_MODE : "LoadingScreen.EnterSelectGameMode",

    /**
     * Event fired when the View Model exits the Select Game Mode state. When this
     * event is fired, the Loading Screen View implementation should hide the
     * Select Session Type buttons.
     */
    EXIT_SELECT_GAME_MODE : "LoadingScreen.ExitSelectGameMode",

    /**
     * Event fired when the View Model enters the Starting Session state. When this
     * event is fired, the Loading Screen View implementation should ideally show
     * some kind of "waiting" icon, as asynchronous server operations are taking
     * place (and we would like the user to be confident that the game has not crashed)
     */
    ENTER_STARTING_SESSION : "LoadingScreen.EnterStartingSession",
    EXIT_STARTING_SESSION : "LoadingScreen.ExitStartingSession",

    /**
     * Event fired when the View Model enters the Show Quick Load state. When this event
     * is fired, the session type will briefly be displayed before the event for starting the
     * session is emitted.
     */
    ENTER_QUICK_LOAD_MODE:"LoadingScreen.EnterQuickLoadMode",
    EXIT_QUICK_LOAD_MODE:"LoadingScreen.ExitQuickLoadMode"
};

/**
 * @typedef LoadingScreenViewModelDependencies
 * @property {Object} urlParams
 * @property {BuildInfoApi} buildInfo
 * @property {Model} model
 * @property {MutableSpinPhaseModel} spinModel
 * @property {MutableBonusPhaseModel} bonusModel
 * @property {BusinessConfig} businessConfig
 * @property {ServicesApi} services
 * @property {EventEmitter} dispatcher
 * @property {DialogController} dialogController
 */

/**
 * ViewModel (Controller) instance to be used by the Splash / Loading Screen view.
 */
export class LoadingScreenViewModel extends BaseController
{
    /**
     * Constructs a new Loading Screen View Model instance.
     * @param {LoadingScreenViewModelDependencies} dependencies 
     */
    constructor(dependencies)
    {
        super(dependencies);

        this._id = "LoadingScreenViewModel";

        /**
         * @protected
         * @type {BuildInfoApi}
         */
        this._buildInfo = dependencies.buildInfo;

        /**
         * @protected
         * @type {Object}
         */
        this._urlParams = dependencies.urlParams;

        /**
         * @protected
         * @type {Model}
         */
        this._model = dependencies.model;

        /**
         * @protected
         * @type {MutableSpinPhaseModel}
         */
        this._spinModel = dependencies.spinModel;

        /**
         * @protected
         * @type {MutableBonusPhaseModel}
         */
        this._bonusModel = dependencies.bonusModel;

        /**
         * @protected
         */
        this._businessConfig = dependencies.businessConfig;

        /**
         * Business config specticically for the splash screen: this field will always have a semantically correct
         * splash screen view config, even if one was not assigned in business config.
         * @protected
         * @type {SplashScreenViewConfig}
         */
        this._splashScreenConfig = this._businessConfig.splashScreenView?
            this._businessConfig.splashScreenView :
            {
                allowUserInteraction : true
            };

        /**
         * @protected
         * @type {ServicesApi}
         */
        this._services = dependencies.services;

        this.log.info(JSON.stringify(this._businessConfig, null, 2));

        this.addState(LoadingScreenViewState.LOADING_ASSETS);
        this.addState(LoadingScreenViewState.SELECT_SERVER_URL, this.enterSelectTestUrl, this.exitSelectTestUrl);
        this.addState(LoadingScreenViewState.ADVERT, this.enterAdvertState, this.exitAdvertState);
        this.addState(LoadingScreenViewState.SELECT_SESSION_TYPE, this.enterSelectSessionType, this.exitSelectSessionType);
        this.addState(LoadingScreenViewState.STARTING_SESSION, this.enterStartingSession, this.exitStartingSession);
        this.addState(LoadingScreenViewState.SHOW_QUICK_LOAD, this.enterShowQuickLoad, this.exitShowQuickLoad);

        this.enterState(LoadingScreenViewState.LOADING_ASSETS);
    };

    /**
     * Checks if showing the url selection view is required.
     * @private
     * @return {boolean}
     */
    isUrlSelectionRequired()
    {
        let urlSelectionIsRequired = false;
        
        let urlSelectionConfig = this._businessConfig.urlSelectionView;
        if (urlSelectionConfig && urlSelectionConfig.isEnabled) {
            urlSelectionIsRequired = true;
        }
        
        return urlSelectionIsRequired;
    };


    /**
     * Checks if showing the Advert state is required.
     * @private
     * @return {boolean}
     */
    isAdvertRequired()
    {
        // TODO: Implement fully
        return false;
    };

    /**
     * Checks to see if quick load has been enabled
     */
    isQuickLoadEnabled()
    {
        if (this._businessConfig.quickLoad && this._businessConfig.quickLoad.isEnabled)
        {
            return true;
        } else {
            return false;
        }
        
    }


    //--------------------------------------------------------------------------------------------------
    // Loading Assets
    //--------------------------------------------------------------------------------------------------
    /**
     * Action to be called when all assets are loaded.
     * @public
     */
    actionOnAssetsLoaded()
    {
        if (this.getState() === LoadingScreenViewState.LOADING_ASSETS)
        {
            this.log.info(`LoadingScreenBVM.actionOnAssetsLoaded()`);

            this._events.emit(C_GameEvent.ALL_GAME_ASSETS_AVAILABLE);

            let nextState = null;

            if (this.isUrlSelectionRequired()) {
                nextState = LoadingScreenViewState.SELECT_SERVER_URL;
            }
            else
            if (this.isAdvertRequired()) {
                nextState = LoadingScreenViewState.ADVERT;
            }
            else 
            if (this.isQuickLoadEnabled())
            {
                nextState = LoadingScreenViewState.SHOW_QUICK_LOAD
            }
            else
            nextState = LoadingScreenViewState.SELECT_SESSION_TYPE;

            this.enterState(nextState);
        }
        else
        {
            this.log.warn(`LoadingScreenBVM.actionOnAssetsLoaded: state is ${this.getState()}`);
        }
    };


    //--------------------------------------------------------------------------------------------------
    // Select Server Url
    //--------------------------------------------------------------------------------------------------
    /**
     * @private
     */
    enterSelectTestUrl()
    {
        this.log.info(`LoadingScreenVM.enterSelectTestUrl()`);

        this._events.addListener(C_GameEvent.HIDE_URL_SELECTION_VIEW, this.selectStateAfterUrlSelection, this);
        this._events.emit(LoadingScreenViewEvent.ENTER_SELECT_SERVER_URL);
    };


    /**
     * @private
     */
    selectStateAfterUrlSelection() {
        let nextState = null;
        if (this.isAdvertRequired()) {
            nextState = LoadingScreenViewState.ADVERT;
        }
        else if (this.isQuickLoadEnabled())
        {
            nextState = LoadingScreenViewState.SHOW_QUICK_LOAD;
        }
        else
        nextState = LoadingScreenViewState.SELECT_SESSION_TYPE;

        this.enterState(nextState);
    };


    /**
     * @private
     */
    exitSelectTestUrl() {
        this._events.removeListener(C_GameEvent.HIDE_URL_SELECTION_VIEW, this.selectStateAfterUrlSelection, this);
        this._events.emit(LoadingScreenViewEvent.EXIT_SELECT_SERVER_URL);
    };


    //--------------------------------------------------------------------------------------------------
    // Advert State
    //--------------------------------------------------------------------------------------------------
    /**
     * @private
     */
    enterAdvertState()
    {
        this.log.info(`LoadingScreenVM.enterAdvertState()`);

        this._events.emit(LoadingScreenViewEvent.ENTER_ADVERT_STATE);
    };


    /**
     * @private
     */
    exitAdvertState() {
        this._events.emit(LoadingScreenViewEvent.EXIT_ADVERT_STATE);
    };


    //--------------------------------------------------------------------------------------------------
    // Select Session Type (also known as Game Mode)
    //--------------------------------------------------------------------------------------------------
    /**
     * @private
     */
    enterSelectSessionType()
    {
        this.log.info(`LoadingScreenVM.enterSelectSessionType()`);

        // If user interaction is allowed, the player must explicitly press one of the "start session type"
        // buttons. Usually there will be only 1 session type allowed (and therefore only one button shown),
        // sometimes there may be more than 1.
        if (this.allowUserInteraction())
        {
            this.log.info(`LoadingScreenVM.selectSessionType: user interaction allowed, use view for feedback`);

            this._events.emit(LoadingScreenViewEvent.ENTER_SELECT_GAME_MODE);
        }
        // User interaction not allowed, so we are going to enter the "check for open session" state automatically.
        // This scenario was added to support PokerStars (and SGDigital integration). SGD supports a custom loading
        // screen, as of PokerStars integration, PokerStars incorrectly implement the SGD api (they dont show the
        // custom loading screen). So we have to hack this fix in.
        else
        {
            let sessionTypes = this.getEnabledGameplayModes();
            let sessionType = C_SessionType.FREE_PLAY;
            
            if (sessionTypes.length === 1) {
                sessionType = sessionTypes[0];
            }
            else
            {
                this.log.warn(`LoadingScreenVM.selectSessionType: userInteraction diabled, but got ${sessionTypes.length} sessionTypes: expected only 1`);
            }

            this.log.info(`LoadingScreenVM.selectSessionType: user interaction not allowed, setting SessionType to ${sessionType}`);

            this._model.setSessionType(sessionType);
            this.enterState(LoadingScreenViewState.STARTING_SESSION);
        }
    };


    /**
     * @private
     */
    exitSelectSessionType()
    {
        if (this.allowUserInteraction())
        {
            this._events.emit(LoadingScreenViewEvent.EXIT_SELECT_GAME_MODE);
        }
    };

     //--------------------------------------------------------------------------------------------------
    // Quick Load Screen displays a loading message and then starts the session
    //--------------------------------------------------------------------------------------------------
    enterShowQuickLoad()
    {
        this._events.emit(LoadingScreenViewEvent.ENTER_QUICK_LOAD_MODE);
    }

    exitShowQuickLoad()
    {
        
    }


    //--------------------------------------------------------------------------------------------------
    // Check for Open Session state
    //--------------------------------------------------------------------------------------------------
    // Once Session Type is selected, we can check if there is an open game session (if that's
    // required for the selected Session Type). We then return control to the App Controller when
    // appropriate. We do not attempt to open the game session during the Loading Screen.
    //--------------------------------------------------------------------------------------------------
    /**
     * Enter action for the "Check for Open Session" state.
     * @private
     */
    enterStartingSession()
    {
        this.log.info(`LoadingScreenVM.enterStartSessionState()`);

        this._events.emit(LoadingScreenViewEvent.ENTER_STARTING_SESSION);

        this._services.sendDownloadStatisticsRequest(reply => {
            this._model.processDownloadStatisticsMsg(reply);

            let sessionType = this._model.getSessionType();
            if (sessionType === C_SessionType.REAL_PLAY ||
                sessionType === C_SessionType.FUN_BONUS)
            {
                this.log.debug(`LoadingScreenVM: sessionType=${sessionType}, => check if session is open`);

                this._services.sendCheckForOpenSessionRequest(
                    sessionType,
                    gamePhaseResults => {
                        if (gamePhaseResults)
                        {
                            this.log.debug(`LoadingScreenVM: got session to restore`);
                            this.configureSessionFromGameReloadMessage(gamePhaseResults);
                        }
                        else this.log.debug(`LoadingScreenVM: no session to restore`);

                        this.returnControlToAppController();
                    },
                    error => this.handleCheckForOpenSessionCommsError(error));
            }
            else
            if (sessionType === C_SessionType.FREE_PLAY)
            {
                this.log.debug(`LoadingScreenVM: sessionType=${sessionType}, => returnControlToAppController`);

                this.returnControlToAppController();
            }
        },
        error => this.handleCheckForOpenSessionCommsError(error));
    };


    /**
     * Handles the Check for Open Session operation failing.
     * @private
     * @param {ServiceExecutionError} error 
     */
    handleCheckForOpenSessionCommsError(error) {
        this.log.error("ServiceExecutionError when checking for an open session");

        this.showCommsErrorDialog(error, () => {
            this.log.debug("Re-attempting session initialization comms with server");

            // This wants changing..
            this.enterState(LoadingScreenViewState.SELECT_SESSION_TYPE);
        });
    };


    /**
     * @private
     * @param {RestoredGameResultPacket<?>} gameReloadMsg
     */
    configureSessionFromGameReloadMessage(gameReloadMsg) {
        this.log.info("LoadingScreenVM: configure session from game reload message");

        this._model.processGameRestorationMsg(gameReloadMsg);

        // TODO: This is slot game specific stuff - the process should be more abstract, for sure.
        if (gameReloadMsg.restoreGamePhase === GamePhase.SINGLE_SPIN) {
            this._spinModel.setSingleSpinResult(gameReloadMsg);
        }
        else
        if (gameReloadMsg.restoreGamePhase === GamePhase.SPIN_1) {
            this._spinModel.setSpin1Result(gameReloadMsg);
        }
        else
        if (gameReloadMsg.restoreGamePhase === GamePhase.SPIN_2) {
            this._spinModel.setSpin2Result(gameReloadMsg);
        }
        else
        if (gameReloadMsg.restoreGamePhase === GamePhase.FREESPIN) {
            this._spinModel.setFreeSpinResult(gameReloadMsg);
        }
        else
        if (gameReloadMsg.restoreGamePhase === GamePhase.QUICK) {
            this._spinModel.setQuickPhaseResult(gameReloadMsg);
        }
        else
        if (gameReloadMsg.restoreGamePhase === GamePhase.BONUS) {
            this._bonusModel.setPhaseResults(gameReloadMsg);
            this._spinModel.setFinalSymbolIdsAfterBonus(gameReloadMsg.previousSymbols);
        }
        

    };


    /**
     * Exit action for the "Check for Open Session" state.
     * @private
     */
    exitStartingSession() {
        this._events.emit(LoadingScreenViewEvent.EXIT_STARTING_SESSION);
    };


    //--------------------------------------------------------------------------------------------------
    //
    //--------------------------------------------------------------------------------------------------
    /**
     * Returns the set of Start Session Button configurations to use, when we are in
     * "Select Session Mode" state.
     * @public
     * @return {TextButtonConfig[]}
     */
    getStartSessionButtonConfigs() {
        /** @type {TextButtonConfig[]} */
        let buttonConfigs = [];

        /** @param {GameplayMode} sessionType */
        let setSessionType = sessionType => {
            this._model.setSessionType(sessionType);
            this.enterState(LoadingScreenViewState.STARTING_SESSION);
        };

        // TODO: THis now needs rethinking.
        // - On V2 platform, we are passed a url parameter, which indicates the mode
        //   that was already selected by the player (the option is not available in
        //   the game client)
        // - On V1 platform, its hard-coded, and some licensees may have hard coded
        //   options about which configuration is allowed
        // - On V1 platform, at least 1 licensee passes a url parameter to the game
        //   client. WMG asked CMS to specify a url parameter, we told them what it
        //   was, the licensee rejected it, WMG buckled, and we were forced into
        //   implementing something non-standard for that licensee.

        // TODO: if this is a ForFun enabled client, should we return only
        // that option - or all enabled options ?
        
        
        let addRealPlay = false;
        let addFunBonus = false;
        let addFreePlay = false;

        let enabledGameplayModes = this.getEnabledGameplayModes();
        if (enabledGameplayModes) {
            addRealPlay = enabledGameplayModes.indexOf(C_SessionType.REAL_PLAY) > -1;
            addFunBonus = enabledGameplayModes.indexOf(C_SessionType.FUN_BONUS) > -1;
            addFreePlay = enabledGameplayModes.indexOf(C_SessionType.FREE_PLAY) > -1;
        }

        // If we have failed to configure any other GameplayMode, we will allow
        // Freeplay as a default fallback.
        if (!addRealPlay && !addFunBonus && !addFreePlay) {
            this.log.warn('LoadingScreen: Failed to add any GamePlay modes. Enabling FreePlay as a default fallback');
            addFreePlay = true;
        }

        if (addFreePlay) {
            buttonConfigs.push({
                localizationId : "T_button_freeplay",
                action : () => {
                    setSessionType(C_SessionType.FREE_PLAY);
                }
            });
        }

        if (addRealPlay) {
            buttonConfigs.push({
                localizationId : "T_button_realplay",
                action : () => {
                    setSessionType(C_SessionType.REAL_PLAY);
                }
            });
        }

        if (addFunBonus) {
            buttonConfigs.push({
                localizationId : "T_button_funbonus",
                action : () => {
                    setSessionType(C_SessionType.FUN_BONUS);
                }
            });
        }
        
        return buttonConfigs;
    };

    /**
     * Returns the set of Loading Screen Session Type Labels configurations
     * @public
     * @return {LoadingScreenSessionLabelConfig[]}
     */
    getSessionTypeText()
    {
        /** @param {GameplayMode} sessionType */
        let setSessionType = sessionType => {
            this._model.setSessionType(sessionType);
            this.enterState(LoadingScreenViewState.STARTING_SESSION);
        };

        /** @type {LoadingScreenSessionLabelConfig[]} */
        let displayLabelConfigs = []; 

        let addRealPlay = false;
        let addFunBonus = false;
        let addFreePlay = false;

        let enabledGameplayModes = this.getEnabledGameplayModes();
        if (enabledGameplayModes) {
            addRealPlay = enabledGameplayModes.indexOf(C_SessionType.REAL_PLAY) > -1;
            addFunBonus = enabledGameplayModes.indexOf(C_SessionType.FUN_BONUS) > -1;
            addFreePlay = enabledGameplayModes.indexOf(C_SessionType.FREE_PLAY) > -1;
        }

        if (addFreePlay) {
            displayLabelConfigs.push({
                localizationId : "T_loading_screen_text_freeplay",
                action : () => {
                    setSessionType(C_SessionType.FREE_PLAY);
                }
            });
        }

        if (addFunBonus) {
            displayLabelConfigs.push({
                localizationId : "T_loading_screen_text_funbonus",
                action : () => {
                    setSessionType(C_SessionType.FUN_BONUS);
                }
            });
        }

        if (addRealPlay) {
            displayLabelConfigs.push({
                localizationId : "T_loading_screen_text_realplay",
                action : () => {
                    setSessionType(C_SessionType.REAL_PLAY);
                }
            });
        }

        return displayLabelConfigs;


    }


    /**
     * Returns a list of Gameplay Modes enabled for this Game Client. This method is
     * intended for overriding, on different implementations of the Loading Screen View
     * Model (ie: to allow us to integrate the game differently on different platforms).
     * @protected
     * @return {GameplayMode[]}
     */
    getEnabledGameplayModes() {
        return [C_SessionType.REAL_PLAY, C_SessionType.FREE_PLAY];
    };


    /**
     * Clears any active state of the Splash Screen, and passes control back to
     * the GameController (by informing it that the Loading Screen has done its
     * job!)
     * @private
     */
    returnControlToAppController() {
        this.log.info("LoadingScreenVM.returnControlToAppController()");
        this.enterState(null);
        this._events.emit(C_GameEvent.LOADING_SCREEN_COMPLETE);
    };


    /**
     * Returns the list of Responsible Gaming Links that should be shown in the
     * Loading Screen View. The links are returned in the order that they should
     * be shown, from left to right.
     * @public
     * @return {ResponsibleGamingLinkConfig[]}
     */
    getResponsibleGamingLinks() {
        return this._businessConfig.responsibleGamingLinks;
    };

    
    /**
     * Indicates if user interaction is enabled for the loading Screen view. if not, we skip
     * states which might involve it (or handle them differently)
     * @protected
     * @returns boolean
     */
    allowUserInteraction() {
        return this._splashScreenConfig.allowUserInteraction;
    }
};


/**
 * Extended version of Loading Screen View Model for V1 compliant game clients. Has custom
 * functionality for V1 platform, related to determining what GameModes (SessionTypes) are
 * enabled for this game client instance.
 */
export class Gen1LoadingScreenViewModel extends LoadingScreenViewModel
{
    /**
     * @override
     * @inheritdoc
     */
    getEnabledGameplayModes()
    {
        if (this._businessConfig.gameModeUrlParameter)
        {
            // On v1 platform, if we are fetching game modes from a url parameter, then
            // its a special implementation: V1 server provides a single numerical enum,
            // which maps to 1 or 2 configured game modes.
            let urlParamId = this._businessConfig.gameModeUrlParameter;
            let gameModeEnum = this._urlParams[urlParamId];

            this.log.debug(`Gen1LoadingScreenViewModel.getEnabledGameplayModes: fetched enum ${gameModeEnum} from urlParam ${urlParamId}`);

            let freeplayEnabled =
                C_V1GameModeEnum.FREEPLAY_ONLY_VALUES.indexOf(gameModeEnum) > -1 ||
                C_V1GameModeEnum.FREEPLAY_AND_REALPLAY_VALUES.indexOf(gameModeEnum) > -1;

            let realplayEnabled =
                C_V1GameModeEnum.REALPLAY_ONLY_VALUES.indexOf(gameModeEnum) > -1 ||
                C_V1GameModeEnum.FREEPLAY_AND_REALPLAY_VALUES.indexOf(gameModeEnum) > -1;

            /** @type {GameplayMode[]} */
            let enabledGameModes = [];

            if (freeplayEnabled) {
                enabledGameModes.push("free");
            }

            if (realplayEnabled) {
                enabledGameModes.push("real");
            }

            return enabledGameModes;
        }
        else
        {
            this.log.debug('Gen1LoadingScreenViewModel.getEnabledGameplayModes: returning explicit set of enabledGameModes');

            return this._businessConfig.enabledGameModes;
        }
    }
};


/**
 * Extended version of Loading Screen View Model for V2 compliant game clients. Has custom
 * functionality for V2 platform, related to determining what GameModes (SessionTypes) are
 * enabled for this game client instance.
 * 
 * For V2 platform, it's simple: we have exactly 1 mode available, and the game client is
 * supposed to be explicitly passed this mode through a url parameter (with a consistent
 * key) on launch.
 */
export class Gen2LoadingScreenViewModel extends LoadingScreenViewModel
{
    /**
     * @override
     * @inheritDoc
     */
    getEnabledGameplayModes()
    {
        /**
         * @type {UrlParamsGen2}
         */
        let urlParams = this._urlParams;

        return [urlParams.gameplayMode];
    }
};


// TODO: Is this serving any purpose ?
export class SGDigitalLoadingScreenViewModel extends Gen2LoadingScreenViewModel
{
    
}