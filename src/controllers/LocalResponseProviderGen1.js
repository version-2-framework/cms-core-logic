import * as RequestType from "../const/C_ServiceRequestTypeGen1";
import * as ResponseType from "../const/C_ServiceResponseTypeGen1";
import * as C_MessageField from "../const/C_MessageField";
import { CmsRng } from '../maths/CmsRng.js';

/**
 * Used to track data about the simulated Session that is in progress.
 * Its exportable, so that we can extend it for a specific game client
 * (this is generally required on older, legacy games).
 */
export class LocalModel
{
    constructor()
    {
        this.wmgGameId = "DefaultWmgGameId";
        this.wmgGameVersion = "DefaultWmgGameVersion",
        this.wmgSessionId = "DefaultWmgSessionId",
        this.playerNickname = "DefaultPlayerNickname",
        this.sessionId = "DefaultSessionId",
        this.sessionIsFreeplay = false;
        this.sessionInProgress = false;
        this.sessionIsReloaded = false;
        this.ticketId = "DefaultTicketId";
        this.ticketProgressive = 0;
        this.funBonusActivated = false;
        this.availableTransferBalance = 100000;
        this.currentWallet = 0;
        this.currentPoints = 0;
    }
}

/**
 * Local Response Provider implementation.
 */
export class LocalResponseProvider
{
    // @ts-check

    /**
     * @param {Dependencies} dependencies 
     */
    constructor(dependencies)
    {
        /**
         * Model instance
         * @private
         */
        this._model = this.createModel();

        /**
         * Simulated maths engine instance
         * @private
         */
        this._maths = this.createMaths();

        /**
         * Rng instance
         * @private
         */
        this._rng = new CmsRng();

        /**
         * Look up table of simulated services
         * @private
         */
        this._services = {};
        // Common services
        this._services[RequestType.DOWNLOAD_STATISTICS] = this.createDownloadStatisticsResponse;
        this._services[RequestType.CHECK_BALANCE] = this.createCheckBalanceResponse;
        this._services[RequestType.CHECK_FOR_OPEN_SESSION] = this.createCheckForOpenSessionResponse;
        this._services[RequestType.SESSION_INIT] = this.createSessionInitResponse;
        this._services[RequestType.BUY_TICKET] = this.createBuyTicketResponse;
        this._services[RequestType.SESSION_END] = this.createSessionEndResponse;
        this._services[RequestType.FUN_BONUS_ACTIVATION] = this.createFunBonusActivationResponse;
        // Slot game services
        this._services[RequestType.GAME_HISTORY] = this.createGameHistoryResponse;
        this._services[RequestType.SINGLE_SPIN_PHASE_RESULTS] = this.createSingleSpinPhaseResultsResponse;
        this._services[RequestType.SINGLE_SPIN_PHASE_COMPLETE] = this.createSingleSpinPhaseCompleteResponse;
        this._services[RequestType.SPIN_1_PHASE_RESULTS] = this.createSpin1PhaseResultsResponse;
        this._services[RequestType.SPIN_1_PHASE_COMPLETE] = this.createSpin1PhaseCompleteResponse;
        this._services[RequestType.SPIN_2_PHASE_RESULTS] = this.createSpin2PhaseResultsResponse;
        this._services[RequestType.SPIN_2_PHASE_COMPLETE] = this.createSpin2PhaseCompleteResponse;
        this._services[RequestType.BONUS_1_SPIN_PHASE_RESULTS] = this.createBonus1SpinPhaseResultsResponse;
        this._services[RequestType.BONUS_1_SPIN_SINGLE_SELECTION] = this.createBonus1SpinSelectionResponse;
        this._services[RequestType.BONUS_1_SPIN_PHASE_COMPLETE] = this.createBonus1SpinPhaseCompleteResponse;
        this._services[RequestType.BONUS_2_SPIN_PHASE_RESULTS] = this.createBonus2SpinPhaseResultsResponse;
        this._services[RequestType.BONUS_2_SPIN_SINGLE_SELECTION] = this.createBonus2SpinSelectionResponse;
        this._services[RequestType.BONUS_2_SPIN_MULTI_SELECTION] = this.createBonus2SpinMultiSelectionResponse;
        this._services[RequestType.BONUS_2_SPIN_PHASE_COMPLETE] = this.createBonus2SpinPhaseCompleteResponse;
    };


    /**
     * Creates a new model instance. Override this in sub-classes, in order to
     * return a customized model implementation for a specific game client.
     * @protected
     */
    createModel()
    {
        return new LocalModel();
    };


    /**
     * Creates a new maths instance. Override this in sub-classes, in order to
     * return a customized maths implementation for a specific game client.
     * @protected
     */
    createMaths()
    {

    };


    /**
     * Returns a locally generated response, for a given request. The request
     * is accepted as an object (the form that the request is generated in,
     * before it is turned to encrypted string and sent to the Game Engine
     * Server). The response is returned as a JSON string (the form we expect
     * to receive from the server, after decryption). This allows us to double
     * test the process for parsing the results objects locally.
     * (so it can )
     * @public
     * @param {string} requestType
     * The type of request to process. 
     * @param {Object} request
     * The request data object.
     * @return {Object}
     */
    getResponseFor(requestType, request)
    {
        let service = this.createBadRequestResponse;

        if (requestType in this._services)
        {
            service = this._services[requestType].bind(this);
        }

        return service(request);
    };


    //------------------------------------------------------------------------------
    // Response generating functions
    //------------------------------------------------------------------------------
    /**
     * Generates a reply simulating a bad request (eg: unsopported type)
     * @private
     * @param {Gen1ServiceRequest} request
     * @return {Gen1ServiceReply}
     */
    createBadRequestResponse(request)
    {
        /** @type {Gen1ServiceReply} */
        let response =
        {
            // todo:
            // Implement a dummy error response here, that indicates
            // its a bad request type.
        };

        return response;
    };


    /**
     * Generates a simulated Download Statistics reply.
     * @private
     * @param {Gen1DownloadStatisticsRequest} request
     * The Download Statistics request
     * @return {Gen1DownloadStatisticsReply}
     */
    createDownloadStatisticsResponse(request)
    {
        let model = this._model;
        let messageId = request[C_MessageField.MESSAGE_ID];

        /** @type {Gen1DownloadStatisticsReply} */
        let response =
        {
            [C_MessageField.MESSAGE_TYPE]    : ResponseType.DOWNLOAD_STATISTICS,
            [C_MessageField.MESSAGE_ID]      : messageId,
            [C_MessageField.PLAYER_NICKNAME] : model.playerNickname
        };

        return response;
    };


    /**
     * Generates a smiulated Check Pending Reply. When Check Pending Reply is returned
     * as a response to Check Pending Request, it means "no game session to restore".
     * @private
     * @param {Gen1CheckPendingRequest} request
     * The Check Pending (Check for Open Session to Restore) request
     * @return {Gen1CheckPendingReply}
     */
    createCheckForOpenSessionResponse(request)
    {
        let model = this._model;
        let messageId = request[C_MessageField.MESSAGE_ID];
        

        /** @type {Gen1CheckPendingReply} */
        let response =
        {
            [C_MessageField.MESSAGE_TYPE]      : ResponseType.CHECK_FOR_OPEN_SESSION,
            [C_MessageField.MESSAGE_ID]        : messageId,
            [C_MessageField.WMG_GAME_ID]       : model.wmgGameId,
            [C_MessageField.WMG_GAME_VERSION]  : model.wmgGameVersion,
            [C_MessageField.PLAYER_NICKNAME]   : model.playerNickname
        };
        
        return response;
    };


    /**
     * Generates a simulated Check Balance reply.
     * @private
     * @param {Gen1CheckBalanceRequest} request
     * The Check Balance request
     * @return {Gen1CheckBalanceReply}
     */
    createCheckBalanceResponse(request)
    {
        let model = this._model;
        let messageId = request[C_MessageField.MESSAGE_ID];
        
        /** @type {Gen1CheckBalanceReply} */
        let response =
        {
            [C_MessageField.MESSAGE_ID]           : messageId,
            [C_MessageField.MESSAGE_TYPE]         : ResponseType.CHECK_BALANCE,
            [C_MessageField.WMG_SESSION_ID]       : model.wmgSessionId,
            [C_MessageField.SESSION_ID]           : model.sessionId,
            [C_MessageField.TRANSFER_BALANCE]     : model.availableTransferBalance,
            [C_MessageField.SESSION_BALANCE]      : model.currentWallet,
            [C_MessageField.SESSION_IS_FREE_PLAY] : model.sessionIsFreeplay
        };

        return response;
    };


    /**
     * Generates a simulated Session Init Reply
     * @private
     * @param {Gen1SessionInitRequest} request
     * A Session Init request.
     * @return {Gen1SessionInitReply}
     */
    createSessionInitResponse(request)
    {
        let model = this._model;
        let openFreeplaySession = request[C_MessageField.SESSION_IS_FREE_PLAY];
        let transferAmount = parseInt(request[C_MessageField.REQUESTED_SESSION_BALANCE]);
        let messageId = request[C_MessageField.MESSAGE_ID];

        model.sessionIsFreeplay = openFreeplaySession;
        model.currentWallet = transferAmount;
        model.sessionInProgress = true;

        /** @type {Gen1SessionInitReply} */
        let response =
        {
            [C_MessageField.MESSAGE_ID]                 : messageId,
            [C_MessageField.MESSAGE_TYPE]               : ResponseType.SESSION_INIT,
            [C_MessageField.SESSION_ID]                 : model.sessionId,
            [C_MessageField.SESSION_BALANCE]            : model.currentWallet,
            [C_MessageField.SESSION_IS_FREE_PLAY]       : model.sessionIsFreeplay,
            [C_MessageField.TICKET_ID]                  : model.ticketId,
            [C_MessageField.SESSION_INACTIVITY_TIMEOUT] : 20
        };

        return response;
    };


    /**
     * Generates a simulated Buy Ticket reply.
     * @private
     * @param {Gen1AddCreditRequest} request
     * A Buy Ticket request.
     * @return {Gen1AddCreditReply}
     */
    createBuyTicketResponse(request)
    {
        let model = this._model;
        let messageId = request[C_MessageField.MESSAGE_ID];
        let transferAmount = request[C_MessageField.TICKET_TRANSFER_AMOUNT];

        model.currentWallet += transferAmount;

        /** @type {Gen1AddCreditReply} */
        let response =
        {
            [C_MessageField.MESSAGE_ID]               : messageId,
            [C_MessageField.MESSAGE_TYPE]             : ResponseType.BUY_TICKET,
            [C_MessageField.SESSION_ID]               : model.sessionId,
            [C_MessageField.TICKET_ID]                : model.ticketId,
            [C_MessageField.TICKET_PROGRESSIVE]       : model.ticketProgressive,
            [C_MessageField.TICKET_TRANSFER_AMOUNT]   : transferAmount,
            //[C_MessageField.TICKET_AMOUNT_BONUS]      : 0,
            //[C_MessageField.TICKET_AMOUNT_PLAY_BONUS] : 0,
            [C_MessageField.WALLET]                   : model.currentWallet,
            [C_MessageField.POINTS]                   : model.currentPoints,
            [C_MessageField.SESSION_IS_FREE_PLAY]     : model.sessionIsFreeplay
        };

        return response;
    };


    /**
     * Generates a simulated Session End reply.
     * @private
     * @param {Gen1SessionEndRequest} request
     * A Session End request.
     * @retun {SessionEndServiceResponse}
     */
    createSessionEndResponse(request)
    {
        let model = this._model;
        let messageId = request[C_MessageField.MESSAGE_ID];

        model.sessionInProgress = false;

        /** @type {Gen1SessionEndReply} */
        let response =
        {
            [C_MessageField.MESSAGE_ID]             : messageId,
            [C_MessageField.MESSAGE_TYPE] 		    : ResponseType.SESSION_END,
            [C_MessageField.WMG_SESSION_ID] 		: model.wmgSessionId,
            [C_MessageField.SESSION_ID] 			: model.sessionId,
            [C_MessageField.SESSION_IS_FREE_PLAY]   : model.sessionIsFreeplay
        };

        return response;
    };


    /**
     * Generates a simulated Fun Bonus Activation reply.
     * @private
     * @param {Gen1FunBonusActivationRequest} request
     * A Fun Bonus Activation request.
     * @return {Gen1FunBonusActivationReply}
     */
    createFunBonusActivationResponse(request)
    {
        let model = this._model;
        let messageId = request[C_MessageField.MESSAGE_ID];

        this._model.funBonusActivated = true;

        /** @type {Gen1FunBonusActivationReply} */
        let response =
        {
            [C_MessageField.MESSAGE_ID]     : messageId,
            [C_MessageField.MESSAGE_TYPE]   : ResponseType.FUN_BONUS_ACTIVATION,
            [C_MessageField.WMG_SESSION_ID] : model.wmgSessionId
        };

        return response;
    };


    /**
     * Generates a simulated Session Keep Alive reply.
     * @private
     * @param {Gen1KeepAliveRequest} request
     * A Session Keep Alive request.
     * @return {Gen1KeepAliveReply}
     */
    createKeepAliveReply(request)
    {
        let model = this._model;
        let messageId = request[C_MessageField.MESSAGE_ID];
        
        /** @type {Gen1KeepAliveReply} */
        let response =
        {
            [C_MessageField.MESSAGE_ID]             : messageId,
            [C_MessageField.MESSAGE_TYPE]           : ResponseType.KEEP_ALIVE,
            [C_MessageField.SESSION_ID]             : model.sessionId,
            [C_MessageField.SESSION_IS_FREE_PLAY]   : model.sessionIsFreeplay,
            [C_MessageField.TICKET_ID]              : model.ticketId
        };

        return response;
    };


    /**
     * Creates and returns a new generic game History Reply. This contains no actual game data
     * (which is client-specific by design). Instead, it just has the stock overview data for
     * the message.
     * @private
     * @param {GameHistoryServiceRequest} request
     * A Session Game History request.
     * @return {GameHistoryServiceResponse}
     */
    createGameHistoryResponse(request)
    {
        let model = this._model;
        let messageId = request[C_MessageField.MESSAGE_ID];

        /** @type {GameHistoryServiceResponse} */
        let response =
        {
            [C_MessageField.MESSAGE_ID]                  : messageId,
            [C_MessageField.MESSAGE_TYPE]                : ResponseType.GAME_HISTORY,
            [C_MessageField.SESSION_ID]                  : model.sessionId,
            [C_MessageField.TICKET_ID]                   : model.ticketId,
            [C_MessageField.TIME_OF_REQUEST_GENERATED]   : "1515",
            [C_MessageField.DATE_OF_REQUEST_GENERATED]   : "16022018",
            [C_MessageField.SESSION_IS_FREE_PLAY]        : false
        };

        // Generate some game specific game results,
        // and push them into a series of fields on
        // the response, labelled "GAME1", "GAME2",
        // etc.
        let results = this.createResultsForGameHistory();
        for (let i = 0; i < results.length; i ++) {
            response[`GAME${i+1}`] = results[i];
        }

        return response;
    };


    /**
     * Return a list of results objects for individual games in history.
     * The intended use case for this method, is to load a set of dummy
     * data with which to populate the history message when performing
     * local testing. (NOTE: in actual Show or ForFun releases of a client,
     * the history tab will not be present, so this feature is purely for
     * testing the history menu of a game client).
     * @protected
     * @todo:
     * I think we can re-examine this one.
     */
    createResultsForGameHistory()
    {
        return [];
    };


    /**
     * Creates a simulated Single Spin Results reply.
     * @private
     * @param {Gen1SingleSpinPhaseResultsRequest} request
     * A Single Spin Results request.
     * @return {Gen1SingleSpinPhaseResultsReply}
     */
    createSingleSpinPhaseResultsResponse(request)
    {
        let model = this._model;
        let messageId = request[C_MessageField.MESSAGE_ID];

        // At the moment, we are using hard-coded replies
        // soon, I will implement a proper maths module.
        // Unlike for previous games, the maths module will
        // be "common", and it will use the config object
        // for the game to pass down results.
        let results = {};
        let rand = this._rng.getRandom(3);

        rand = 1;
        if (rand === 0) {
            results.finalSymbols = [
                1,2,3,
                4,5,6,
                10,7,8,
                9,11,4,
                3,9,1];
            results.winlineIds = [0];
            results.winlineWinnings = [0];
            results.winlineSymbolsData = [0];
            results.playBonus = false;
        }
        else if (rand === 1) {
            results.finalSymbols = [
                11,7,9,
                11,7,9,
                11,7,9,
                11,7,9,
                11,7,9
            ];
            results.winlineIds = [1,2,3];//[1,2,3];
            results.winlineWinnings = [500,500,500,500,500];//[500,500,500,500,500];
            results.winlineSymbolsData = [5,5,5,5,5];//[5,5,5,5,5];
            results.playBonus = false;
        }
        else if (rand === 2) {
            results.finalSymbols = [
                1,8,2,
                2,3,4,
                1,6,7,
                7,3,4,
                3,7,1];
            results.winlineIds = [0];
            results.winlineWinnings = [0];
            results.winlineSymbolsData = [0];
            results.playBonus = false;
        }

        /** @type {Gen1SingleSpinPhaseResultsReply} */
        let response =
        {
            [C_MessageField.MESSAGE_ID]             : messageId,
            [C_MessageField.MESSAGE_TYPE]           : ResponseType.SINGLE_SPIN_PHASE_RESULTS,
            [C_MessageField.SESSION_ID]             : model.sessionId,
            [C_MessageField.TICKET_ID]              : model.ticketId,
            [C_MessageField.START_SYMBOLS_DATA]     : "1,1,1,1,1,1,1,1,1,1,1,1,1,1,1",
            [C_MessageField.FINAL_SYMBOLS_DATA]     : results.finalSymbols.toString(),
            [C_MessageField.WINLINE_IDS]            : results.winlineIds.toString(),
            [C_MessageField.WINLINE_WINNINGS]       : results.winlineWinnings.toString(),
            [C_MessageField.WINLINE_SYMBOLS_DATA]   : results.winlineSymbolsData.toString(),
            [C_MessageField.PLAY_BONUS]             : false
        };

        return response;
    };


    /**
     * Generates a simulated Single Spin Phase complete reply.
     * @private
     * @param {Gen1SingleSpinPhaseCompleteRequest} request
     * A Single Spin Phase Complete request.
     * @return {Gen1SingleSpinPhaseCompleteReply}
     */
    createSingleSpinPhaseCompleteResponse(request)
    {
        /**
         * @type {Gen1SingleSpinPhaseCompleteReply}
         */
        let response =
        {

        };

        return response;
    };


    /**
     * This implementation is mostly duplicated from "createSpinResultsReply",
     * except we also add holds information.
     * @private
     * @param {Gen1Spin1PhaseResultsRequest} request
     * A Spin 1 Results request.
     * @return {Gen1Spin1PhaseResultsReply}
     */
    createSpin1PhaseResultsResponse(request)
    {
        let model = this._model;
        let messageId = request[C_MessageField.MESSAGE_ID];
        let totalStake = request[C_MessageField.TOTAL_STAKE];
        let numLines = request[C_MessageField.NUM_LINES];

        this._model.currentWallet -= totalStake;
        if (this._model.currentWallet < 0) {
            this._model.currentWallet = 0;
        }

        // At the moment, we are using hard-coded replies
        // soon, I will implement a proper maths module.
        // Unlike for previous games, the maths module will
        // be "common", and it will use the config object
        // for the game to pass down results.
        let results = {};
        
        // Return a bonus
        // results.finalSymbols = [ 1, 10, 2, 3, 10, 10, 2, 10, 4, 4, 3, 2, 1, 10, 4 ];
        // results.winlineIds = [0];
        // results.winlineWinnings = [0];
        // results.winlineSymbolsData = [0];
        // results.holdsPattern = ["NH","NH","NH","NH","NH"];
        // results.playBonus = true;

        let rand = this._rng.getRandom(1);
        if (rand === 0) {
            results.finalSymbols = [7,2,7,3,7,2,3,1,4,5,7,9,2,1,6];
            results.winlineIds = [0];
            results.winlineWinnings = [0];
            results.winlineSymbolsData = [0];
            results.holdsPattern = ["H","H","NH","NH","H"];
            results.playBonus = false;
            results.totalWinnings = 0;
        }
        else if (rand === 1) {
            results.finalSymbols = [3,3,3,3,3,3,3,3,3,3,3,3,3,3,3];
            results.winlineIds = [1,2,3,4,5];
            results.winlineWinnings = [500,500,500,500,500];
            results.winlineSymbolsData = [5,5,5,5,5];
            results.holdsPattern = ["NH","NH","NH","NH","NH"];
            results.playBonus = false;
            results.totalWinnings = 2500;
        }
        else if (rand === 2) {
            results.finalSymbols = [2,5,1,3,5,2,3,10,5,4,5,1,1,2,1];
            results.winlineIds = [10];
            results.winlineWinnings = [2000];
            results.winlineSymbolsData = [4];
            results.holdsPattern = ["NH","NH","NH","NH","NH"];
            results.playBonus = false;
            results.totalWinnings = 200;
        }

        this._model.currentWallet += results.totalWinnings;

        /** @type {Gen1Spin1PhaseResultsReply} */
        let response =
        {
            [C_MessageField.MESSAGE_ID]             : messageId,
            [C_MessageField.MESSAGE_TYPE]           : ResponseType.SPIN_1_PHASE_RESULTS,
            [C_MessageField.SESSION_ID]             : model.sessionId,
            [C_MessageField.SESSION_IS_FREE_PLAY]   : model.sessionIsFreeplay,
            [C_MessageField.SESSION_IS_RELOADED]    : model.sessionIsReloaded,
            [C_MessageField.TICKET_ID]              : model.ticketId,
            [C_MessageField.TICKET_PROGRESSIVE]     : model.ticketProgressive,
            [C_MessageField.TOTAL_STAKE]            : totalStake,
            [C_MessageField.NUM_LINES]              : numLines,
            [C_MessageField.WALLET]                 : model.currentWallet,
            [C_MessageField.POINTS]                 : model.currentPoints,
            [C_MessageField.GAME_PHASE]             : 1,
            [C_MessageField.START_SYMBOLS_DATA]     : "1,1,1,1,1,1,1,1,1,1,1,1,1,1,1",
            [C_MessageField.FINAL_SYMBOLS_DATA]     : results.finalSymbols.toString(),
            [C_MessageField.WINLINE_IDS]            : results.winlineIds.toString(),
            [C_MessageField.WINLINE_WINNINGS]       : results.winlineWinnings.toString(),
            [C_MessageField.WINLINE_SYMBOLS_DATA]   : results.winlineSymbolsData.toString(),
            [C_MessageField.AUTO_HOLDS]             : results.holdsPattern.toString(),
            [C_MessageField.PLAY_BONUS]             : results.playBonus.toString()
        };

        return response;
    };


    /**
     * Generates a simulated Spin 1 Phase complete reply.
     * @private
     * @param {Gen1Spin1PhaseCompleteRequest} request
     * A Spin 1 Phase complete request.
     * @return {Gen1SpinPhaseCompleteReply}
     */
    createSpin1PhaseCompleteResponse(request)
    {
        // TODO:
        // Implement the stock reply.. we don't need to deviate from it at all.
        /** @type {Gen1Spin1PhaseCompleteReply} */
        let response =
        {

        };

        return response;
    };


    /**
     * Generates a simulated Spin 2 Results reply.
     * @private
     * @param {Gen1Spin2PhaseResultsRequest} request
     * A Spin 2 Results request.
     * @return {Gen1Spin2PhaseResultsReply}
     */
    createSpin2PhaseResultsResponse(request)
    {
        let model = this._model;
        let messageId = request[C_MessageField.MESSAGE_ID];
        let totalStake = request[C_MessageField.TOTAL_STAKE];
        let numLines = request[C_MessageField.NUM_LINES];
        
        let results = {};
        let rand = this._rng.getRandom(2);

        // NOTE:
        // This params are hard-coded right now, to assume that we ONLY get here,
        // when the dummy first spin results returned the 1 hard coded option
        // that did not have a win in it. The results that we randomly select from
        // below are also based on these assumptions.
        results.startSymbols = [7,2,7,3,7,2,3,1,4,5,7,9,2,1,6];
        results.holdsPattern = ["NH","NH","NH","NH","NH"];

        if (rand === 0) {
            results.finalSymbols = [7,2,7,3,7,2,4,9,1,5,7,9,3,2,2];
            results.winlineIds = [0];
            results.winlineWinnings = [0];
            results.winlineSymbolsData = [0];
            results.playBonus = false;
            results.totalWinnings = 0;
        }
        else if (rand === 1) {
            results.finalSymbols = [7,2,7,3,7,2,7,7,2,5,7,9,7,3,1];
            results.winlineIds = [9,5,8,7];
            results.winlineWinnings = [2500,1000,1000,200];
            results.winlineSymbolsData = [5,4,4,3];
            results.playBonus = false;
            results.totalWinnings = 4700;
        }

        this._model.currentWallet += results.totalWinnings;

        /** @type {Gen1Spin2PhaseResultsReply} */
        let response = 
        {
            [C_MessageField.MESSAGE_ID]             : messageId,
            [C_MessageField.MESSAGE_TYPE]           : ResponseType.SPIN_2_PHASE_RESULTS,
            [C_MessageField.SESSION_ID]             : model.sessionId,
            [C_MessageField.SESSION_IS_FREE_PLAY]   : model.sessionIsFreeplay,
            [C_MessageField.SESSION_IS_RELOADED]    : model.sessionIsReloaded,
            [C_MessageField.TICKET_ID]              : model.ticketId,
            [C_MessageField.TICKET_PROGRESSIVE]     : model.ticketProgressive,
            [C_MessageField.TOTAL_STAKE]            : totalStake,
            [C_MessageField.NUM_LINES]              : numLines,
            [C_MessageField.WALLET]                 : model.currentWallet,
            [C_MessageField.POINTS]                 : model.currentPoints,
            [C_MessageField.GAME_PHASE]             : 2,
            [C_MessageField.START_SYMBOLS_DATA]     : results.startSymbols.toString(),
            [C_MessageField.FINAL_SYMBOLS_DATA]     : results.finalSymbols.toString(),
            [C_MessageField.AUTO_HOLDS]             : results.holdsPattern.toString(),
            [C_MessageField.WINLINE_IDS]            : results.winlineIds.toString(),
            [C_MessageField.WINLINE_WINNINGS]       : results.winlineWinnings.toString(),
            [C_MessageField.WINLINE_SYMBOLS_DATA]   : results.winlineSymbolsData.toString(),
            [C_MessageField.PLAY_BONUS]             : false
        }

        return response;
    };


    /**
     * Generates a simulated Spin 2 Phase Complete reply.
     * @private
     * @param {Gen1Spin2PhaseCompleteRequest} request
     * A Spin 2 Phase request.
     * @return {Gen1Spin2PhaseCompleteReply}
     */
    createSpin2PhaseCompleteResponse(request)
    {
        // TODO:
        // Implement the stock reply.. we don't need to deviate from it at all.
        /** @type {Gen1Spin2PhaseCompleteReply} */
        let response =
        {

        };

        return response;
    };


    /**
     * Generates a simulated (Single Spin) Bonus Phase Results reply.
     * @private
     * @param {Object} request
     * A (Single Spin) Bonus Phase Results request.
     * @return {Object}
     */
    createBonus1SpinPhaseResultsResponse(request)
    {
        // TODO:
        // Implement the stock reply.. we don't need to deviate from it at all.
        
        // NOTE:
        // The following is hard-coded simulation data for FPG.
        /** @type {Gen1BonusOneSpinPhaseResultsReply} */
        let result =
        {
            [C_MessageField.MESSAGE_TYPE] : ResponseType.BONUS_2_SPIN_PHASE_RESULTS,
            ["PZB"] : "250",
            ["PZS"] : "500",
            ["PZG"] : "1000",
            ["PZD"] : "10000",
            ["CKP"] : "1,1,1,1,1",
        };

        return result;
    };


    /**
     * Generates a simulated (Single Spin) Bonus Selection reply.
     * @private
     * @param {Gen1Bonus1SpinSelectionServiceRequest} request
     * A (Single Spin) Bonus Selection request.
     * @return {Gen1BonusOneSpinSingleSelectionReply}
     */
    createBonus1SpinSelectionResponse(request)
    {
        // TODO:
        // Implement the stock reply.. we don't need to deviate from it at all.
        //return {};

        // TODO: Check the specification
        /** @type {Gen1BonusOneSpinSingleSelectionReply} */
        let result =
        {
            [C_MessageField.MESSAGE_TYPE] : ResponseType.BONUS_2_SPIN_PHASE_RESULTS,
            ["PZB"] : "250",
            ["PZS"] : "500",
            ["PZG"] : "1000",
            ["PZD"] : "10000",
            ["CKP"] : "1,1,1,1,1",
            ["CKD"] : "4",
            ["WTP"] : "diamond"
        };

        return result;
    };


    /**
     * Generates a simulated (Single Spin) Bonus Phase Complete reply
     * @private
     * @param {Gen1Bonus1SpinPhaseCompleteServiceRequest} request 
     * A (Single Spin) Bonus Phase Complete request
     * @return {Gen1BonusOneSpinPhaseCompleteReply}
     */
    createBonus1SpinPhaseCompleteResponse(request)
    {
        // TODO:
        // Implement the stock reply.. we don't need to deviate from it at all.
        /** @type {Gen1BonusOneSpinPhaseCompleteReply} */
        let response =
        {

        };

        return response;
    };


    /**
     * Generates a simulated (2 Spin) Bonus Results reply.
     * @private
     * @param {Gen1Bonus2SpinPhaseResultsServiceRequest} request 
     * A (2 Spin) Bonus Results request.
     * @return {Gen1BonusTwoSpinPhaseResultsReply}
     */
    createBonus2SpinPhaseResultsResponse(request)
    {
        // TODO:
        // Implement the stock reply.. we don't need to deviate from it at all.
        /** @type {Gen1BonusTwoSpinPhaseResultsReply} */
        let response =
        {

        };

        return response;
    };
    

    /**
     * Generates a simulated (2 Spin) Bonus Selection reply.
     * @private
     * @param {Gen1Bonus2SpinSelectionServiceRequest} request 
     * A (2 Spin) Bonus Selection request.
     * @return {Gen1BonusTwoSpinSingleSelectionReply}
     */
    createBonus2SpinSelectionResponse(request)
    {
        // TODO:
        // Implement the stock reply.. we don't need to deviate from it at all.
        /** @type {Gen1BonusTwoSpinSingleSelectionReply} */
        let response = 
        {

        };

        return response;
    };


    /**
     * Generates a simulated (2 Spin) Bonus Multi Selection reply.
     * @private
     * @param {Gen1Bonus2SpinMultiSelectionServiceRequest} request 
     * A (2 Spin) Bonus Selection request.
     * @return {Gen1BonusTwoSpinMultiSelectionReply}
     */
    createBonus2SpinMultiSelectionResponse(request)
    {
        // TODO:
        // Implement the stock reply.. we don't need to deviate from it at all.
        /** @type {Gen1BonusTwoSpinMultiSelectionReply} */
        let response = 
        {

        }

        return response;
    };


    /**
     * Generates a simulated (2 Spin) Bonus Phase Complete reply
     * @private
     * @param {Gen1Bonus2SpinPhaseCompleteServiceRequest} request 
     * A (2 Spin) Bonus Phase Complete request
     * @return {Gen1BonusTwoSpinPhaseCompleteReply}
     */
    createBonus2SpinPhaseCompleteResponse(request)
    {
        // TODO:
        // Implement the stock reply.. we don't need to deviate from it at all.
        /** @type {Gen1BonusTwoSpinPhaseCompleteReply} */
        let response =
        {

        };

        return response;
    };
};

export default LocalResponseProviderGen1;