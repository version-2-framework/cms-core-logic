import * as RequestType from "../const/C_ServiceRequestTypeGen2";
import * as ResponseType from "../const/C_ServiceResponseTypeGen2";
import * as CurrencyType from "../const/C_CurrencyType";

const DUMMY_WEB_SESSION_ID = "DummyWebSessionId";
const DEFAULT_ACCOUNT_BALANCE = 50000;
const DEFAULT_START_WALLET = 50000;
const DEFAULT_TIMEOUT_MINS = 20;

export class LocalModel
{
    constructor()
    {
        this.isSessionInProgress = false;
        this.wallet = 0;
        this.winnings = 0;
        this.superbet = 0;
        this.numFreeSpins = 0;
        this.numPromoGames = 0;
        this.wmgRoundRef = -1;
        this.wmgSpinRef = -1;
        this.wmgBonusRef = -1;
        this.wmgFreespinRef = -1;

        /**
         * @type {SessionInfo}
         */
        this.regulation = null;

        /**
         * @type {string}
         */
        this.sessionType = null;
    }
};


/**
 * A simple local response provider, for a Gen 2 game.
 */
export class LocalResponseProviderGen2
{
    // TODO: The methods of this class are sketcged in, but no functionality is added.

    /**
     * @param {Dependencies} dependencies 
     */
    constructor(dependencies)
    {
        let resultsFileName = "results.json";
        let resultsFile = fetch(resultsFileName).then(response => response.json())

        this._model = new LocalModel();

        this._services = {};
        // Default services
        this._services[RequestType.DOWNLOAD_STATISTICS] = this.createDownloadStatisticsReply;
        this._services[RequestType.CHECK_PENDING] = this.createCheckPendingReply;
        this._services[RequestType.CHECK_BALANCE] = this.createCheckBalanceReply;
        this._services[RequestType.SESSION_INIT] = this.createSessionInitReply;
        this._services[RequestType.ADD_CREDIT] = this.createAddCreditReply;
        this._services[RequestType.GAME_COMPLETE] = this.createGameCompleteReply;
        this._services[RequestType.KEEP_ALIVE] = this.createKeepAliveReply;
        this._services[RequestType.SESSION_END] = this.createSessionEndReply;
        // Slot game services
        this._services[RequestType.SINGLE_SPIN_PHASE_RESULTS] = this.createSingleSpinPhaseResultsResponse;
        this._services[RequestType.SINGLE_SPIN_PHASE_COMPLETE] = this.createSingleSpinPhaseCompleteResponse;
        this._services[RequestType.SPIN_1_PHASE_RESULTS] = this.createSpin1PhaseResultsResponse;
        this._services[RequestType.SPIN_1_PHASE_COMPLETE] = this.createSpin1PhaseCompleteResponse;
        this._services[RequestType.SPIN_2_PHASE_RESULTS] = this.createSpin2PhaseResultsResponse;
        this._services[RequestType.SPIN_2_PHASE_COMPLETE] = this.createSpin2PhaseCompleteResponse;
        this._services[RequestType.BONUS_PHASE_RESULTS] = this.createBonusPhaseResultsResponse;
        this._services[RequestType.BONUS_PHASE_COMPLETE] = this.createBonusPhaseCompleteResponse;
        this._services[RequestType.FREESPIN_PHASE_RESULTS] = this.createFreeSpinPhaseResultsResponse;
        this._services[RequestType.FREESPIN_PHASE_COMPLETE] = this.createFreeSpinPhaseCompleteResponse;
        this._services[RequestType.GAMBLE_PHASE_RESULTS] = this.createGamblePhaseResultsResponse;
        this._services[RequestType.GAMBLE_PHASE_COMPLETE] = this.createGamblePhaseCompleteResponse;
    };


    /**
     * Synchronously loads a results json from disk. Because we store the info in memory,
     * large json files will not be appropriate.
     * @param {string} resultsFileName 
     * @return {Gen2JsonResultsFile}
     */
    loadResultsFile(resultsFileName)
    {
        fetch(resultsFileName).then(response => response.json());
    }


    /**
     * Returns a locally generated response, for a given request. The request
     * is accepted as an object (the form that the request is generated in,
     * before it is turned to encrypted string and sent to the Game Engine
     * Server). The response is returned as a JSON string (the form we expect
     * to receive from the server, after decryption). This allows us to double
     * test the process for parsing the results objects locally.
     * (so it can )
     * @public
     * @param {string} requestType
     * The type of request to process. 
     * @param {Object} request
     * The request data object.
     * @return {Object}
     */
    getResponseFor(requestType, request)
    {
        let service = this.createBadRequestResponse;

        if (requestType in this._services)
        {
            service = this._services[requestType].bind(this);
        }

        return service(request);
    };


    /**
     * @private
     * @param {*} request 
     * @return {Object}
     */
    createBadRequestResponse(request)
    {
        // TODO: Fully implement
        return null;
    };


    //--------------------------------------------------------------------------------------------------
    // Common request operations
    //--------------------------------------------------------------------------------------------------
    /**
     * @private
     * @param {Gen2DownloadStatisticsRequest} request 
     * @return {Gen2DownloadStatisticsReply}
     */
    createDownloadStatisticsReply(request)
    {
        /** @type {Gen2DownloadStatisticsReply} */
        let reply =
        {
            messageType : "WMG_DOWNLOAD_STATISTICS_REPLY",
            wmgWebSessionId : DUMMY_WEB_SESSION_ID
        };

        return reply;
    };


    /**
     * @private
     * @param {Gen2CheckPendingRequest} request 
     * @return {Gen2CheckPendingReply}
     */
    createCheckPendingReply(request)
    {
        /** @type {Gen2CheckPendingReply} */
        let reply =
        {
            messageType : "WMG_CHECK_PENDING_REPLY",
            wmgWebSessionId : DUMMY_WEB_SESSION_ID
        };

        return reply;
    };


    /**
     * @private
     * @param {Gen2CheckBalanceRequest} request 
     * @return {Gen2CheckBalanceReply}
     */
    createCheckBalanceReply(request)
    {
        /** @type {Gen2CheckBalanceReply} */
        let reply =
        {
            messageType : "WMG_BALANCE_REPLY",
            wmgWebSessionId : DUMMY_WEB_SESSION_ID,
            accountBalance : DEFAULT_ACCOUNT_BALANCE
        };

        return reply;
    };


    /**
     * @private
     * @param  {Gen2SessionInitRequest} request 
     * @return {Gen2SessionInitReply}
     */
    createSessionInitReply(request)
    {
        this._model.isSessionInProgress = true;
        this._model.sessionType = request.gameplayMode;
        this._model.wallet = DEFAULT_START_WALLET;
        this._model.winnings = 0;
        this._model.superbet = 0;
        this._model.numFreeSpins = 0;
        this._model.numPromoGames = 0;
        this._model.wmgRoundRef = -1;
        this._model.regulation =  { country : "NONE" };

        /** @type {Gen2SessionInitReply} */
        let reply =
        {
            messageType : "WMG_SESSION_INIT_REPLY",
            wmgWebSessionId : DUMMY_WEB_SESSION_ID,
            currentWallet : this._model.wallet,
            winningsAmount : this._model.winnings,
            superbetAmount : this._model.superbet,
            numFreeSpins : this._model.numFreeSpins,
            numPromoGames : this._model.numPromoGames,
            wmgRoundRef : this._model.wmgRoundRef,
            regulation : this._model.regulation,
            timeout : DEFAULT_TIMEOUT_MINS
        };

        return reply;
    };


    /**
     * @private
     * @param  {Gen2AddCreditRequest} request 
     * @return {Gen2AddCreditReply}
     */
    createAddCreditReply(request)
    {
        this._model.wallet += request.transfMoney;

        /** @type {Gen2AddCreditReply} */
        let reply =
        {
            messageType : "WMG_ADD_CREDIT_REPLY",
            wmgWebSessionId : DUMMY_WEB_SESSION_ID,
            regulation : this._model.regulation,
            currentWallet : this._model.wallet
        };

        return reply;
    };


    /**
     * @private
     * @param  {Gen2GameCompleteRequest} request 
     * @return {Gen2GameCompleteReply}
     */
    createGameCompleteReply(request)
    {
        // Transfer winnings, and clear any old game state
        this._model.wallet = this._model.winnings;
        this._model.winnings = 0;
        this._model.superbet = 0;
        this._model.numFreeSpins = 0;

        // TODO: Do we need to track any other info about game state in local mode ?

        /** @type {Gen2GameCompleteReply} */
        let reply =
        {
            messageType : "WMG_GAME_COMPLETE_REPLY",
            wmgWebSessionId : DUMMY_WEB_SESSION_ID,
            currentWallet : this._model.wallet
        };

        return reply;
    };


    /**
     * @private
     * @param  {Gen2KeepAliveRequest} request 
     * @return {Gen2KeepAliveReply}
     */
    createKeepAliveReply(request)
    {
        /** @type {Gen2KeepAliveReply} */
        let reply =
        {
            messageType : "WMG_KEEP_ALIVE_REPLY",
            wmgWebSessionId : DUMMY_WEB_SESSION_ID
        };

        return reply;
    };


    /**
     * @private
     * @param  {Gen2SessionEndRequest} request 
     * @return {Gen2SessionEndReply}
     */
    createSessionEndReply(request)
    {
        let finalWallet = this._model.wallet + this._model.winnings + this._model.superbet;

        this._model.isSessionInProgress = false;
        this._model.wallet = 0;
        this._model.winnings = 0;
        this._model.superbet = 0;
        this._model.numFreeSpins = 0;
        this._model.numPromoGames = 0;

        /** @type {Gen2SessionEndReply} */
        let reply =
        {
            messageType : "WMG_SESSION_END_REPLY",
            wmgWebSessionId : DUMMY_WEB_SESSION_ID,
            currentWallet : finalWallet,
            regulation : this._model.regulation
        };

        return reply;
    };


    //--------------------------------------------------------------------------------------------------
    // Slot Game Specific request operations
    //--------------------------------------------------------------------------------------------------
    /**
     * Deducts the cost of a new SubBet from the appropriate currency type.
     * This method does not validation at all on whether the bet is affordable
     * (we are not getting over-complicated within this local results generation
     * module). It will, however, ensure that the values of the relevant field
     * are not dropped below 0.
     * @private
     * @param {SlotBetSettings} betSettings 
     */
    deductCostOfSubBet(betSettings)
    {
        let totalStake = betSettings.stakePerLine * betSettings.numLines * betSettings.numHands;
        switch (betSettings.currencyType)
        {
            case CurrencyType.CREDIT:
                this._model.wallet -= totalStake;
                if (this._model.wallet < 0) {
                    this._model.wallet = 0;
                }
                break;

            case CurrencyType.SUPER_BET:
                this._model.superbet -= totalStake;
                if (this._model.superbet < 0) {
                    this._model.superbet = 0;
                }
                break;

            case CurrencyType.WINNINGS:
                this._model.winnings -= totalStake;
                if (this._model.winnings < 0) {
                    this._model.winnings = 0;
                }
                break;

            case CurrencyType.PROMO_GAME:
                this._model.numPromoGames -= 1;
                if (this._model.numPromoGames < 0) {
                    this._model.numPromoGames = 0;
                }
                break;
        }
    }

    /**
     * @private
     * @param {Gen2SingleSpinPhaseResultsRequest} request 
     * @return {Gen2SingleSpinPhaseResultsReply}
     */
    createSingleSpinPhaseResultsResponse(request)
    {
        /** @type {Gen2SingleSpinPhaseResult} */
        let phaseResult = null;

        this.deductCostOfSubBet(request.betSettings);
        this._model.winnings += phaseResult.totalCreditWon
        this._model.superbet += phaseResult.totalSuperBetWon;
        this._model.numPromoGames += phaseResult.numFreeGamesWon;
        this._model.numFreeSpins += phaseResult.numFreeSpinsWon;

        /** @type {Gen2SingleSpinPhaseResultsReply} */
        let response =
        {
            messageType : "WMG_SINGLE_SPIN_PHASE_RESULTS_REPLY",
            wmgWebSessionId : DUMMY_WEB_SESSION_ID,
            wmgRoundRef : this._model.wmgRoundRef,
            wmgSpinRef : this._model.wmgSpinRef,
            result : phaseResult,
            betSettings : request.betSettings,
            currentWallet : this._model.wallet,
            winningsAmount : this._model.winnings,
            superbetAmount : this._model.superbet,
            numPromoGames : this._model.numPromoGames,
            numFreeSpins : this._model.numFreeSpins
        };

        return response;
    };


    /**
     * @private
     * @param {Gen2SingleSpinPhaseCompleteRequest} request 
     * @return {Gen2SingleSpinPhaseCompleteReply}
     */
    createSingleSpinPhaseCompleteResponse(request)
    {
        /** @type {Gen2SingleSpinPhaseCompleteReply} */
        let response =
        {

        };

        return response;
    };


    /**
     * @private
     * @param {Gen2Spin1PhaseResultsReply} request 
     * @return {Gen2Spin1PhaseResultsReply}
     */
    createSpin1PhaseResultsResponse(request)
    {
        /** @type {Gen2Spin1PhaseResultsReply} */
        let response =
        {
            messageType : "WMG_SPIN_1_PHASE_RESULTS_REPLY",
            
            currentWallet : this._model.wallet,
            "SBT":0,
            "FGM":0,
            "RES":
            {
                "totalCreditWon":0,
                "totalSuperBetWon":0,
                "numFreeGamesWon":0,
                "resultType":1,
                "phaseId":1,
                "spins":[{
                    "totalCreditWon":0,
                    "totalSuperBetWon":0,
                    "numFreeGamesWon":0,
                    "symbolIds":[1,4,6,4,1,9,1,3,8,9,2,6,7,4,6],
                    "stickySymbolPositions":[],
                    "symbolWins":[],
                    "specialWins":[]
                }],
                "numFreeSpinsWon":0,
                "playSpin2":true,
                "spin2StartSymbolIds":[1,4,6,4,1,9,1,3,8,9,2,6,7,4,6],
                "autoholdsPattern":[1,0,1,1,1]
            }
        };

        return response;
    };


    /**
     * @private
     * @param {Gen2Spin1PhaseCompleteServiceRequest} request 
     * @return {Gen2Spin1PhaseCompleteServiceResponse}
     */
    createSpin1PhaseCompleteResponse(request)
    {
        /** @type {Gen2Spin1PhaseCompleteServiceResponse} */
        let response =
        {
            "MTP":"SPIN_1_COMPLETE",
            "MID":"dummy_message_id",
            "ERR":null,
            "SID":"yi8f9hr4hhej3n4d",
            "PID":"je7f06naf080rk5l",
            "PPR":0,
            "FPL":false,
            "NST":"SPIN_2"
        };

        return response;
    };


    /**
     * @private
     * @param {Gen2Spin2PhaseResultsServiceRequest} request 
     * @return {Gen2Spin2PhaseResultsServiceResponse}
     */
    createSpin2PhaseResultsResponse(request)
    {
        /** @type {Gen2Spin2PhaseResultsServiceResponse} */
        let response =
        {
            "MTP":"SPIN_2_RESULTS",
            "MID":"dummy_message_id",
            "ERR":null,
            "SID":"yi8f9hr4hhej3n4d",
            "PID":"je7f06naf080rk5l",
            "PPR":0,
            "FPL":false,
            "WLT":5000,
            "SBT":0,
            "FGM":0,
            "RES":
            {
                "totalCreditWon":0,
                "totalSuperBetWon":0,
                "numFreeGamesWon":0,
                "resultType":1,
                "phaseId":2,
                "spins":[{
                    "totalCreditWon":0,
                    "totalSuperBetWon":0,
                    "numFreeGamesWon":0,
                    "symbolIds":[1,4,6,2,1,6,1,3,8,9,2,6,7,4,6],
                    "stickySymbolPositions":[],
                    "symbolWins":[{
                        "rewardGroup":1,
                        "rewardType":1,
                        "rewardValue":400,
                        "handId":1,
                        "lineId":4,
                        "symbolId":1,
                        "winningPositions":[1,5,9]
                    },
                    {
                        "rewardGroup":1,
                        "rewardType":1,
                        "rewardValue":10000,
                        "handId":1,
                        "lineId":2,
                        "symbolId":6,
                        "winningPositions":[3,6,9,12,15]
                    }],
                    "specialWins":[],
                    "symbolTransforms":[]
                }],
                "numFreeSpinsWon":0,
                "startSymbolIds":[1,4,6,4,1,9,1,3,8,9,2,6,7,4,6],
                "holdsPatternUsed":[1,0,1,1,1],
                "playerUsedAutoholdsPattern":true
            }
        };

        return response;
    };


    /**
     * @private
     * @param {Gen2Spin2PhaseCompleteServiceRequest} request 
     * @return {Gen2Spin2PhaseCompleteServiceResponse}
     */
    createSpin2PhaseCompleteResponse(request)
    {
        /** @type {Gen2Spin2PhaseCompleteServiceResponse} */
        let response =
        {
            "MTP":"SPIN_2_COMPLETE",
            "MID":"dummy_message_id",
            "ERR":null,
            "SID":"yi8f9hr4hhej3n4d",
            "PID":"je7f06naf080rk5l",
            "PPR":0,
            "FPL":false,
            "NST":"IDLE"
        };

        return response;
    };


    /**
     * @private
     * @param {Gen2BonusPhaseResultsServiceRequest} request 
     * @return {Gen2BonusPhaseResultsServiceResponse}
     */
    createBonusPhaseResultsResponse(request)
    {
        /** @type {Gen2BonusPhaseResultsServiceResponse} */
        let response =
        {

        };

        return response;
    };


    /**
     * @private
     * @param {Gen2BonusPhaseCompleteServiceRequest} request 
     * @return {Gen2BonusPhaseCompleteServiceResponse}
     */
    createBonusPhaseCompleteResponse(request)
    {
        /** @type {Gen2BonusPhaseCompleteServiceResponse} */
        let response =
        {

        };

        return response;
    };


    /**
     * @private
     * @param {Gen2FreeSpinPhaseResultsServiceRequest} request 
     * @return {Gen2FreeSpinPhaseResultsServiceResponse}
     */
    createFreeSpinPhaseResultsResponse(request)
    {
        /** @type {Gen2FreeSpinPhaseResultsServiceResponse} */
        let response =
        {

        };

        return response;
    };


    /**
     * @private
     * @param {Gen2FreeSpinPhaseCompleteServiceRequest} request 
     * @return {Gen2FreeSpinPhaseCompleteServiceResponse}
     */
    createFreeSpinPhaseCompleteResponse(request)
    {
        /** @type {Gen2FreeSpinPhaseCompleteServiceResponse} */
        let response =
        {

        };

        return response;
    };


    /**
     * @private
     * @param {Gen2GamblePhaseResultsServiceRequest} request 
     * @return {Gen2GamblePhaseResultsServiceResponse}
     */
    createGamblePhaseResultsResponse(request)
    {
        /** @type {Gen2GamblePhaseResultsServiceResponse} */
        let response =
        {

        };

        return response;
    };


    /**
     * @private
     * @param {Gen2GamblePhaseCompleteServiceRequest} request 
     * @return {Gen2GamblePhaseCompleteServiceResponse}
     */
    createGamblePhaseCompleteResponse(request)
    {
        /** @type {Gen2GamblePhaseCompleteServiceResponse} */
        let response =
        {

        };

        return response;
    };
}