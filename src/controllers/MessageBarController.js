import C_GameEvent from "../const/C_GameEvent";
import { getGameViewLogger } from "../logging/LogManager";
import * as C_CurrencyType from "../const/C_CurrencyType";

// Abstract sketch of how MessageBar control would now work. This needs a little more work
// to be complete (and for it to be possible to integrate into our main game), but its not
// at all bad. Note, that what we have here, presently, is a "SlotGameMessageBarController".
// We might want a different implementation for other games.

// The cons of this approach
// - its much harder to be granular over messages and events
// - we basically break this up into multiple implementations, where previously it was
//   all just handled in the controller (and elsewhere, if we wanted)
// - to add new message hooks, we have to make sure implicit but well designed events are
//   available for us to latch on tp

// The pros;
// - it handles priority trees of message state much more cleanly (and this is basically
//   what we actually want the message bar for)
// - we can actually swap the implementation, if we want to change what information we show
//   at a given time, in regard to messages (although, i doubt we will want to)
// - its more straightforward to judge what events are shown, and whn
// - the requirement for better message hooks to latch onto, could itself lead to better
//   overall design (in terms of more clearly defining what messages are broadcast in our
//   system, and when)
// - If we want to pick custom smart messages based on certain game model data, its neater
//   to handle that here. For example: game in progress, but no other messages to show,
//   if winnings are very large, we can show "wow! youre winning a lot" state, which could
//   look nice in some circumstances

// TODO: Add some logic to support PromoGames (freeGames) - its almost certain that we will
// need to show some specialist information here, for SG Digital integration (which has Promo
// Games - although, the exact details are so far a little unclear to me)

const log = getGameViewLogger();

/**
 * @typedef DefaultMessageBarControllerDependencies
 * @property {Model} model
 * @property {EventEmitter} dispatcher
 */

export class DefaultMessageBarController
{
    /**
     * 
     * @param {DefaultMessageBarControllerDependencies} dependencies 
     */
    constructor(dependencies)
    {
        /**
         * @private
         * @type {MessageBarConfig}
         */
        this._currMessage = null;

        /**
         * @private
         * @type {Model}
         */
        this._model = dependencies.model;

        /**
         * @type {EventEmitter}
         */
        this._events = dependencies.dispatcher;

        /**
         * Indicates if a Game Session is currently in progress.
         * @private
         * @type {boolean}
         */
        this._isSessionInProgress = false;

        /**
         * Indicates that a Game Idle state is currently active.
         * @private
         * @type {boolean}
         */
        this._isGameIdle = false;

        /**
         * Indicates if a FreeRounds Idle state is currently active.
         * @private
         * @type {boolean}
         */
        this._isFreeRoundsIdle = false;

        /**
         * Indicates if a Game is currently in progress.
         * @private
         * @type {boolean}
         */
        this._isGameInProgress = false;

        /**
         * Indicates if Autoplay is currently in progress.
         * @private
         * @type {boolean}
         */
        this._isAutoplayInProgress = false;

        /**
         * Indicates if a FreeSpins phase is currently in progress.
         * @private
         * @type {boolean}
         */
        this._isFreeSpinsInProgress = false;

        /**
         * Indicates if we are currently playing from Credit.
         * @private
         * @type {boolean}
         */
        this._playingFromCredit = false;

        /**
         * Indicates if we are currently playing from SuperBet.
         * @private
         * @type {boolean}
         */
        this._playingFromSuperBet = false;

        /**
         * Indicates if we are currently playing from Winnings.
         * @private
         * @type {boolean}
         */
        this._playingFromWinnings = false;

        /**
         * Indicates if we are currently playing from PromoGames (FreeRounds / FreeGames)
         * @private
         * @type {boolean}
         */
        this._playingFromPromoGames = false;

        // Events that we need to respond to
        // - game idle
        // - game started
        // - autoplay started
        // - select holds
        // - show bonus win

        // TODO: More event listeners are needed here
        this._events.on(C_GameEvent.SESSION_OPENED, this.onSessionOpened, this);
        this._events.on(C_GameEvent.SESSION_CLOSED, this.onSessionClosed, this);
        this._events.on(C_GameEvent.GAME_IDLE_STARTED, this.onGameIdleStarted, this);
        this._events.on(C_GameEvent.GAME_IDLE_ENDED, this.onGameIdleEnded, this);
        this._events.on(C_GameEvent.FREEROUNDS_IDLE_STARTED, this.onFreeRoundsIdleStarted, this);
        this._events.on(C_GameEvent.FREEROUNDS_IDLE_ENDED, this.onFreeRoundsIdleEnded, this);
        this._events.on(C_GameEvent.GAME_STARTED, this.onGameStarted, this);
        this._events.on(C_GameEvent.GAME_FINISHED, this.onGameFinished, this);
        this._events.on(C_GameEvent.AUTOPLAY_STARTED, this.onAutoplayStarted, this);
        this._events.on(C_GameEvent.AUTOPLAY_ENDED, this.onAutoplayEnded, this);
        this._events.on(C_GameEvent.FREESPINS_STARTED, this.onFreeSpinsStarted, this);
        this._events.on(C_GameEvent.FREESPINS_ENDED, this.onFreeSpinsEnded, this);
        this._events.on(C_GameEvent.ENABLE_HOLDS_INPUT, this.onHoldsInputEnabled, this);
        this._events.on(C_GameEvent.DISABLE_HOLDS_INPUT, this.onHoldsInputDisabled, this);
        this._events.on(C_GameEvent.CURRENCY_TYPE_CHANGED, this.onCurrencyTypeChanged, this);
        this._events.on(C_GameEvent.WINNINGS_CHANGED, this.onWinningsChanged, this);
        this._events.on(C_GameEvent.SUPERBET_CHANGED, this.onSuperbetChanged, this);

        // TODO: Do we need a "winnings idle" event ?
        // TODO: Do we need a "superbet idle" event ?
        // TODO: We probably need some events which relate to PromoGames (but exactly what we have to do with that is not yet clear)
    };


    /**
     * @private
     */
    onSessionOpened()
    {
        this._isSessionInProgress = true;
        this.updateMessageShown();
    };


    /**
     * @private
     */
    onSessionClosed()
    {
        this._isSessionInProgress = false;
        this.updateMessageShown();
    };


    /**
     * @private
     */
    onGameIdleStarted()
    {
        this._isGameIdle = true;
        this.updateMessageShown();
    };


    /**
     * @private
     */
    onGameIdleEnded()
    {
        this._isGameIdle = false;
        this.updateMessageShown();
    };


    /**
     * @private
     */
    onFreeRoundsIdleStarted()
    {
        this._isFreeRoundsIdle = true;
        this.updateMessageShown();
    };


    /**
     * @private
     */
    onFreeRoundsIdleEnded()
    {
        this._isFreeRoundsIdle = false;
        this.updateMessageShown();
    };


    /**
     * @private
     */
    onGameStarted()
    {
        this._isGameInProgress = true;
        this.updateMessageShown();
    };


    /**
     * @private
     */
    onGameFinished()
    {
        this._isGameInProgress = false;
    };


    /**
     * @private
     */
    onAutoplayStarted()
    {
        this._isAutoplayInProgress = true;
        this._events.on(C_GameEvent.AUTOPLAY_CHANGED, this.updateMessageShown, this);
        this.updateMessageShown();
    };


    /**
     * @private
     */
    onAutoplayEnded()
    {
        this._events.off(C_GameEvent.AUTOPLAY_CHANGED, this.updateMessageShown, this);
        this._isAutoplayInProgress = false;
    };


    /**
     * @private
     */
    onHoldsInputEnabled()
    {
        this._holdsInputEnabled = true;
        this.updateMessageShown();
    };


    /**
     * @private
     */
    onHoldsInputDisabled()
    {
        this._holdsInputEnabled = false;
        this.updateMessageShown();
    };


    /**
     * @private
     */
    onCurrencyTypeChanged()
    {
        this._playingFromCredit = false;
        this._playingFromSuperBet = false;
        this._playingFromWinnings = false;
        this._playingFromPromoGames = false;

        if (this._model.getActiveCurrencyType() === C_CurrencyType.SUPER_BET) {
            this._playingFromSuperBet = true;
        }
        else
        if (this._model.getActiveCurrencyType() === C_CurrencyType.WINNINGS) {
            this._playingFromWinnings = true;
        }
        else
        if (this._model.getActiveCurrencyType() === C_CurrencyType.PROMO_GAME) {
            this._playingFromPromoGames = true;
        }
        else this._playingFromCredit = true;

        this.updateMessageShown();
    };


    /**
     * @private
     */
    onWinningsChanged()
    {
        this.updateMessageShown();
    };


    /**
     * @private
     */
    onSuperbetChanged()
    {
        this.updateMessageShown();
    };


    /**
     * @private
     */
    onFreeSpinsStarted()
    {
        this._isFreeSpinsInProgress = true;
        this._events.on(C_GameEvent.FREESPINS_CHANGED, this.updateMessageShown, this);
        this.updateMessageShown();
    };


    /**
     * @private
     */
    onFreeSpinsEnded()
    {
        this._events.off(C_GameEvent.FREESPINS_CHANGED, this.updateMessageShown, this);
        this._isFreeSpinsInProgress = false;
    };


    //--------------------------------------------------------------------------------------------------
    // The heavy lifting of what messages we actually want to show.
    //--------------------------------------------------------------------------------------------------
    /**
     * Selects the actual message to show right now. Basically, one big decision tree, based on what
     * we know about the game going on. Sometimes, more than 1 important thing is true (for example,
     * we are playing from winnings, AND we have a FreeSpin phase in progress). So, we have to make
     * decisions about the priority of those pieces of information, in terms of the messages that we
     * will show.
     * @protected
     * @final
     */
    updateMessageShown()
    {
        let textConfig = this.getMessageToShow();

        if (textConfig && textConfig.localizationId)
        {
            log.debug(`MessageBarController: set message to ${JSON.stringify(textConfig)}`);

            this._events.emit(C_GameEvent.SET_MESSAGE, textConfig);
        }
        else
        {
            log.debug('MessageBarController: clear the MessageBar');

            this._events.emit(C_GameEvent.CLEAR_MESSAGE);
        }
    };


    /**
     * Picks the most appropriate message to show, for a given update, and returns a TextConfig instance
     * (indicating id of any text to pick out of the localization file, and all appropriate modifications
     * to apply). The method works something like a decision tree (it is called whenever we need to update
     * the text shown). The method is method is intended for overriding in sub-classes : simply apply
     * any extra decisions you want and return a custom textconfig, or call through to the bass class method,
     * in order to get a standard text message.
     * @protected
     * @return {TextConfig}
     */
    getMessageToShow()
    {
        let localizationId, localizationMods;

        if (this._isSessionInProgress === false)
        {
            // Perhaps we shouldnt have a "noSession" message.. unless a session was closed?
            // Showing last session data might be ok here.
            //localizationId = "T_message_noSession";
        }
        else
        if (this._isGameInProgress === false)
        {
            if (this._model.isNewGameAffordable() === false)
            {
                localizationId = "T_message_outOfCredit";
            }
            else
            if (this._model.getLastGameWinnings() > 0)
            {
                let lastGameWinnings = this._model.getLastGameWinnings();
                let lastGameWinningsString = this._model.formatCurrency(lastGameWinnings);
                localizationId = "T_message_lastGameWinnings";
                localizationMods = { "[WINNINGS]" : lastGameWinningsString }
            }
            else
            if (this._isGameIdle)
            {
                localizationId = "T_message_startNewGame";
            }
            else
            if (this._isFreeRoundsIdle)
            {
                localizationId = "T_message_startNewPromoGame";
                localizationMods = {
                    "[NUM_PROMO_GAMES]" : this._model.getFreeGamesNumRemaining(),
                    "[PROMO_GAMES_WINNINGS]" : this._model.getFreeGamesWinnings()
                };
            }
        }
        else
        {
            // FreeSpins phase information must supersede any other status
            if (this._isFreeSpinsInProgress)
            {
                localizationId = 'T_message_freeSpinsStats';
                localizationMods = {
                    "[NUM_FREESPINS]" : this._model.getFreeSpinsNumRemaining(),
                    "[FREESPINS_WINNINGS]" : this._model.getFreeSpinsWinnings()
                };
            }
            // Holds enabled status supersedes autoplay state data (in practise, if autoplay is
            // in progress, you will never see this message: holds input will not be enabled!).
            // It also supersedes Superbet info (we may need to select holds when playing superbet)
            else
            if (this._holdsInputEnabled)
            {
                localizationId = 'T_message_startSpin2';
            }
            else
            if (this._playingFromSuperBet)
            {
                localizationId = "T_message_playingFromSuperbet";
                localizationMods = {
                    "[SUPERBET]" : this._model.getPlayerSuperbet()
                };
            }
            else
            if (this._playingFromWinnings)
            {
                localizationId = "T_message_playingFromWinnings";
                localizationMods = {
                    "[WINNINGS]" : this._model.getPlayerWinnings()
                };
            }
            else
            if (this._playingFromPromoGames)
            {
                localizationId = "T_message_playingFromPromoGames";
                localizationMods = {
                    "[NUM_PROMO_GAMES]" : this._model.getFreeGamesNumRemaining(),
                    "[PROMO_GAMES_WINNINGS]" : this._model.getFreeGamesWinnings()
                }
            }
            else
            if (this._isAutoplayInProgress)
            {
                localizationId = 'T_message_autoplayStats';
                localizationMods = {
                    "[NUM_AUTOPLAYS]" : this._model.getAutoplayNumGamesRemaining(),
                    "[AUTOPLAY_WINNINGS]" : this._model.getAutoplayWinnings()
                };
            }
            else
            {
                localizationId = 'T_message_gameInProgress';
                localizationMods = {
                    "[WINNINGS]" : this._model.getPlayerWinnings()
                };
            }
        }

        return { localizationId, localizationMods };
    }
};