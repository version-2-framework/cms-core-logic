import { getCommsLogger } from "../logging/LogManager";

let log = getCommsLogger();

/**
 * Injects additional properties for requests for the Wmg Casino App. This can be used
 * with any Services implementation (the casino app will need to use either the Gen1
 * or Gen2 services implementation).
 * @implements {RequestPropertyInjector}
 */
export class WmgCasinoAppRequestPropertyInjector
{
    /**
     * @param {Dependencies} dependencies 
     */
    constructor(dependencies)
    {
        /**
         * Game client id, which is injected into all outgoing request objects as an additional
         * field.
         * @private
         * @type {string}
         */
        this._gameClientId = dependencies.config.gameId;

        /**
         * User token to be sent for the Authorization header in outgoing Xml http requests.
         * @private
         * @type {string}
         */
        this._userToken = dependencies.urlParams.userToken;

        log.info(`WmgCasinoAppRequestPropertInjector.constructor(gameClientId:${this._gameClientId}, userToken:${this._userToken})`);
    };


    /**
     * @inheritDoc
     * @public
     * @return {CustomXmlHttpHeaders}
     */
    getRequestHeaderFields()
    {
        /** @type {CustomXmlHttpHeaders} */
        let customHeaders =
        {
            Authorization : `Bearer ${this._userToken}`,
            Accept : 'application/json'
        };

        return customHeaders;
    }


    /**
     * @inheritDoc
     * @public
     * @param {Object} request 
     */
    injectCommonProperties(request)
    {
        request.game_client = this._gameClientId;
    };
}