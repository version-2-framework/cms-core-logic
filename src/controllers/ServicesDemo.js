import Assets from "../../../cms-core-view/src/utils/Assets";
import { getCommsLogger } from "../logging/LogManager";
import { Gen2SpinResultParser } from "../model/Gen2SpinResultParser";

const DEFAULT_WALLET = 100000;
const DEFAULT_WEB_SESSION_ID = "DummyWebSessionId";

/**
 * @type {SessionInfo}
 */
const DEFAULT_SESSION_INFO = { country:"" };

/**
 * @implements {ServicesApi}
 */
export class ServicesDemo
{
    constructor(dependencies)
    {
        this.log = getCommsLogger();

        /**
         * @protected
         */
        this._model =
        {
            isSessionInProgress : false,
            wallet : 0,
            winnings : 0,
            superbet : 0,
            numFreeSpins : 0,
            numPromoGames : 0,
            wmgRoundRef : -1,
            wmgSpinRef : -1,
            wmgBonusRef : -1,
            wmgFreeSpinRef : -1,
            currBetSettings : null
        }   
    };


    /**
     * @public
     * @inheritDoc
     * @param  {...any} injectors 
     */
    addRequestPropertyInjectors(...injectors)
    {
        // do nothing
    };


    /**
     * @public
     * @inheritDoc
     * @param {CommsSuccessHandler<DownloadStatisticsData>} onSuccess 
     * @param {CommsFailureHandler} onFailure 
     */
    sendDownloadStatisticsRequest(onSuccess, onFailure)
    {
        onSuccess({
            playerNickName : "demoPlayer"
        });
    };


    /**
     * @public
     * @inheritdoc
     * @param {GameplayMode} gameplayMode 
     * @param {(reply?:RestoredGameResultPacket<any>)=>void} onSuccess 
     * @param {CommsFailureHandler} onFailure 
     */
    sendCheckForOpenSessionRequest(gameplayMode, onSuccess, onFailure)
    {
        onSuccess(); // EG: no game restoration
    };


    /**
     * @public
     * @inheritDoc
     * @param {GameplayMode} gameplayMode 
     * @param {CommsSuccessHandler<BalanceData>} onSuccess 
     * @param {CommsFailureHandler} onFailure 
     * @param {Object} [customRequestParameters]
     */
    sendCheckBalanceRequest(gameplayMode, onSuccess, onFailure, customRequestParameters)
    {
        onSuccess({
            accountBalance : this._model.wallet,
            maxTransfer : DEFAULT_WALLET,
            maxTransferInSession : DEFAULT_WALLET
        });
    };


    /**
     * @public
     * @inheritDoc
     * @param {number} requestedBalance 
     * @param {GameplayMode} gameplayMode 
     * @param {CommsSuccessHandler<Gen2SessionInitReply>} onSuccess 
     * @param {CommsFailureHandler} onFailure 
     */
    sendSessionInitRequest(requestedBalance, gameplayMode, onSuccess, onFailure)
    {
        if (this._model.isSessionInProgress === false)
        {
            this._model.isSessionInProgress = true;
            this._model.wallet = DEFAULT_WALLET;
            this._model.winnings = 0;
            this._model.superbet = 0;
            this._model.numFreeSpins = 0;
            this._model.numPromoGames = 0;
            this._model.wmgRoundRef = -1;
            this._model.wmgSpinRef = -1;
            this._model.wmgBonusRef = -1;
            this._model.wmgFreeSpinRef = 01;

            onSuccess({
                messageType : "WMG_SESSION_INIT_REPLY",
                wmgWebSessionId : DEFAULT_WEB_SESSION_ID,
                gameplayMode : "free",
                currentWallet : this._model.wallet,
                superbetAmount : this._model.superbet,
                winningsAmount : this._model.winnings,
                numFreeSpins : this._model.numFreeSpins,
                wmgRoundRef : -1,
                promoGamesAwarded : false,
                regulation : DEFAULT_SESSION_INFO,
                timeout : 20, 
            });
        }
        else
        {
            onFailure({
                code : "CL00",
                description : "Session already open",
                isFatal : true
            });
        }
    }


    /**
     * @public
     * @inheritDoc
     * @param {GameplayMode} gameplayMode 
     * @param {CommsSuccessHandler<Gen2SessionInitReply>} onSuccess 
     * @param {CommsFailureHandler} onFailure 
     * @param {Object} [customRequestParameters]
     */
    sendSessionInitWithMaxBalanceRequest(gameplayMode, onSuccess, onFailure, customRequestParameters)
    {
        this.sendSessionInitRequest(DEFAULT_WALLET, gameplayMode, onSuccess, onFailure);
    };


    /**
     * @public
     * @inheritdoc
     * @param {GameplayMode} gameplayMode 
     * @param {CommsSuccessHandler<Gen2SessionInitReply>} onSuccess 
     * @param {CommsFailureHandler} onFailure 
     */
    sendGetOpenSessionRequest(gameplayMode, onSuccess, onFailure)
    {
        throw new Error("Method not implemented.");
    };


    /**
     * @public
     * @inheritdoc
     * @param {number} amountOfCreditToAdd 
     * @param {CommsSuccessHandler<Gen2AddCreditReply>} onSuccess 
     * @param {CommsFailureHandler} onFailure 
     */
    sendAddCreditRequest(amountOfCreditToAdd, onSuccess, onFailure)
    {
        // Don't execute any actual addition to session wallet,
        // not needed for demo mode. We can reset the wallet to
        // default value (although this should have no side effect)

        this._model.wallet = DEFAULT_WALLET;

        onSuccess({
            messageType : "WMG_ADD_CREDIT_REPLY",
            wmgWebSessionId : DEFAULT_WEB_SESSION_ID,
            regulation : DEFAULT_SESSION_INFO,
            currentWallet : this._model.wallet
        });
    };


    /**
     * @public
     * @inheritdoc
     * @param {CommsSuccessHandler<Gen2SessionEndReply>} onSuccess 
     * @param {CommsFailureHandler} onFailure 
     */
    sendSessionEndRequest(onSuccess, onFailure)
    {
        if (this._model.isSessionInProgress)
        {
            this._model.isSessionInProgress = false;
            
            onSuccess({
                messageType : "WMG_SESSION_END_REPLY",
                wmgWebSessionId : DEFAULT_WEB_SESSION_ID,
                regulation : DEFAULT_SESSION_INFO,
                currentWallet : this._model.wallet
            });
        }
        else
        {
            onFailure({
                code : "CL00"   ,
                description : "No session in progress, so cannot EndSession",
                isFatal : false
            });
        }
    };


    /**
     * @public
     * @inheritDoc
     * @param {(reply: Gen2GameCompleteReply, onLmsCustomData?: Object) => void} onSuccess 
     * @param {CommsFailureHandler} onFailure 
     */
    sendGameCompleteRequest(onSuccess, onFailure)
    {
        // Reset wallet at end of every game
        this._model.wallet = DEFAULT_WALLET;
        this._model.currBetSettings = null;

        onSuccess({
            messageType : "WMG_GAME_COMPLETE_REPLY",
            wmgWebSessionId : DEFAULT_WEB_SESSION_ID,
            currentWallet : this._model.wallet
        });
    };


    /**
     * @public
     * @inheritdoc
     * @param {() => void} onSuccess 
     * @param {CommsFailureHandler} onFailure 
     */
    sendKeepAliveRequest(onSuccess, onFailure)
    {
        onSuccess();
    };


    /**
     * @protected
     * @param {SlotBetSettings} betSettings 
     */
    deductCostOfBet(betSettings)
    {
        let totalBet = betSettings.numHands * betSettings.numLines * betSettings.stakePerLine;

        if (betSettings.currencyType === "CREDIT") {
            this._model.wallet = Math.max(0, this._model.wallet - totalBet);
        }
    }
};

/**
 * @typedef OneSpinServerResult
 * @property {SlotBetSettings} betSettings
 * @property {Gen2SingleSpinPhaseResult} singleSpin
 * @property {Gen2BonusPhaseResult} [bonus]
 * @property {Gen2FreeSpinPhaseResult} [freeSpin]
 */

/**
 * @typedef TwoSpinServerResult
 * @property {SlotBetSettings} betSettings
 * @property {Gen2Spin1PhaseResult} spin1
 * @property {Gen2Spin2PhaseResult} [spin2]
 * @property {Gen2BonusPhaseResult} [bonus]
 * @property {Gen2FreeSpinPhaseResult} [freeSpin]
 */

/**
 * Demo implementation of the whole Slot Game Services api.
 * @implements {SlotGameServicesApi}
 */
export class SlotGameServicesDemo
    extends ServicesDemo
{
    constructor(dependencies)
    {
        super(dependencies);

        // We would need assets, just to grab our "demo results" json
        // However, we have no ability to dynamically load this, at all.
        /**
         * @type {Assets}
         */
        let assets = dependencies.assets;

        /**
         * @private
         */
        this._spinResultParser = new Gen2SpinResultParser(dependencies);
    };


    /**
     * Gets the current Game Result - in server format.
     * @private
     * @return {OneSpinServerResult | TwoSpinServerResult}
     */
    getCurrGameResult()
    {
        return null;
    };


    /**
     * Returns a raw server phase result, according to its key.
     * @private
     * @param {"singleSpin" | "spin1" | "spin2" | "bonus" | "freeSpin"} phaseKey 
     * @returns 
     */
    getRawPhaseResult(phaseKey)
    {
        let gameRes = this.getCurrGameResult();

        if (gameRes) {
            return gameRes[phaseKey];
        }
        else return null;
    };


    /**
     * Gets the current Single Spin Phase Result (if one is available), in parsed client format.
     * Returns null if no such result is available.
     * @private
     * @return {SingleSpinPhaseResult | null}
     */
    getSingleSpinPhaseResult()
    {
        let rawPhaseResult = this.getRawPhaseResult("singleSpin");

        if (rawPhaseResult)
        {
            return this._spinResultParser.parseSingleSpinPhaseResult(rawPhaseResult);
        }
        else return null;
    };


    /**
     * Gets the current Spin 1 Phase Result (if one is available), in parsed client format.
     * Returns null if no such result is available.
     * @private
     * @return {Spin1PhaseResult | null}
     */
    getSpin1PhaseResult()
    {
        let rawPhaseResult = this.getRawPhaseResult("spin1");

        if (rawPhaseResult)
        {
            return this._spinResultParser.parseSpin1PhaseResult(rawPhaseResult);
        }
        else return null;
    };


    /**
     * Gets the current Spin 2 Phase Result (if one is available), in parsed client format.
     * Returns null if no such result is available.
     * @private
     * @return {Spin2PhaseResult | null}
     */
    getSpin2PhaseResult()
    {
        let rawPhaseResult = this.getRawPhaseResult("spin2");
 
        if (rawPhaseResult)
        {
           return this._spinResultParser.parseSpin2PhaseResult(rawPhaseResult);
        }
        else return null;
    };


    /**
     * Gets the current FreeSpin Phase Result (if one is available), in parsed client format.
     * Returns null if no such result is available.
     * @private
     * @return {FreeSpinPhaseResult | null}
     */
    getFreeSpinPhaseResult()
    {
        let rawPhaseResult = this.getRawPhaseResult("freeSpin");
 
        if (rawPhaseResult)
        {
           return this._spinResultParser.parseFreeSpinPhaseResult(rawPhaseResult);
        }
        else return null;
    };


    /**
     * @public
     * @inheritDoc
     * @param {SlotBetSettings} betSettings 
     * @param {GameStartParams} gameStartParams 
     * @param {CommsSuccessHandler<GameResultPacket<SingleSpinPhaseResult>>} onSuccess 
     * @param {CommsFailureHandler} onFailure 
     */
    sendSingleSpinPhaseResultsRequest(betSettings, gameStartParams, onSuccess, onFailure)
    {
        let result = this.getSingleSpinPhaseResult();

        if (result)
        {
            this.deductCostOfBet(betSettings);

            this._model.currBetSettings = betSettings;
            this._model.winnings += result.totalCreditWon;
            this._model.superbet += result.totalSuperBetWon;
            this._model.numFreeSpins += result.numFreeSpinsWon;
            this._model.numPromoGames += result.numFreeGamesWon;

            onSuccess({
                betSettings,
                currentWallet : this._model.wallet,
                winningsAmount : this._model.winnings,
                superbetAmount : this._model.superbet,
                numPromoGames : this._model.numPromoGames,
                numFreeSpins : this._model.numFreeSpins,
                result
            });
        }
        else
        {
            onFailure({
                code : "CL00",
                description : "No Single Spin Phase result was available",
                isFatal : true
            });
        }
    };


    /**
     * @public
     * @inheritdoc
     * @param {() => void} onSuccess 
     * @param {CommsFailureHandler} onFailure 
     */
    sendSingleSpinPhaseCompleteRequest(onSuccess, onFailure)
    {
        onSuccess();
    };


    /**
     * @public
     * @inheritdoc
     * @param {SlotBetSettings} betSettings 
     * @param {GameStartParams} gameStartParams 
     * @param {CommsSuccessHandler<GameResultPacket<Spin1PhaseResult>>} onSuccess 
     * @param {CommsFailureHandler} onFailure 
     */
    sendSpin1PhaseResultsRequest(betSettings, gameStartParams, onSuccess, onFailure)
    {
        let result = this.getSpin1PhaseResult();

        if (result)
        {
            this.deductCostOfBet(betSettings);

            this._model.currBetSettings = betSettings;
            this._model.winnings += result.totalCreditWon;
            this._model.superbet += result.totalSuperBetWon;
            this._model.numFreeSpins += result.numFreeSpinsWon;
            this._model.numPromoGames += result.numFreeGamesWon;

            onSuccess({
                betSettings,
                currentWallet : this._model.wallet,
                winningsAmount : this._model.winnings,
                superbetAmount : this._model.superbet,
                numPromoGames : this._model.numPromoGames,
                numFreeSpins : this._model.numFreeSpins,
                result
            });
        }
        else
        {
            onFailure({
                code : "CL00",
                description : "No Spin1 Phase result was available",
                isFatal : true
            });
        }
    };


    /**
     * @public
     * @inheritdoc
     * @param {() => void} onSuccess 
     * @param {CommsFailureHandler} onFailure 
     */
    sendSpin1PhaseCompleteRequest(onSuccess, onFailure)
    {
        onSuccess();
    };


    /**
     * @public
     * @inheritdoc
     * @param {HoldsPattern} holdsPattern 
     * @param {CommsSuccessHandler<GameResultPacket<Spin2PhaseResult>>} onSuccess 
     * @param {CommsFailureHandler} onFailure 
     */
    sendSpin2PhaseResultsRequest(holdsPattern, onSuccess, onFailure)
    {
        let result = this.getSpin2PhaseResult();

        if (result)
        {
            this._model.winnings += result.totalCreditWon;
            this._model.superbet += result.totalSuperBetWon;
            this._model.numFreeSpins += result.numFreeSpinsWon;
            this._model.numPromoGames += result.numFreeGamesWon;

            onSuccess({
                betSettings : this._model.currBetSettings,
                currentWallet : this._model.wallet,
                winningsAmount : this._model.winnings,
                superbetAmount : this._model.superbet,
                numPromoGames : this._model.numPromoGames,
                numFreeSpins : this._model.numFreeSpins,
                result
            });
        }
        else
        {
            onFailure({
                code : "CL00",
                description : "No Spin 2 Phase result was available",
                isFatal : true
            });
        }
    };


    /**
     * @public
     * @inheritdoc
     * @param {CommsSuccessHandler} onSuccess 
     * @param {CommsFailureHandler} onFailure 
     */
    sendSpin2PhaseCompleteRequest(onSuccess, onFailure) {
        onSuccess();
    };

    
    // Bonus is now a special case. We could
    // 1) Allow normal bonus result + parsing (we would need to override something)
    // 2) It would be legit to just have the parsed results ready in the source json
    //    (possibly preferable, in fact).
    // I think it would be fine to support both, actually.
    /**
     * @public
     * @inheritDoc
     * @param {CommsSuccessHandler<GameResultPacket<?>>} onSuccess 
     * @param {CommsFailureHandler} onFailure 
     */
    sendBonusPhaseResultsRequest(onSuccess, onFailure)
    {
        let result = this.getRawPhaseResult("bonus");

        if (result)
        {
            this._model.winnings += result.totalCreditWon;
            this._model.superbet += result.totalSuperBetWon;
            this._model.numFreeSpins += result.numFreeSpinsWon;
            this._model.numPromoGames += result.numFreeGamesWon;

            onSuccess({
                betSettings : this._model.currBetSettings,
                currentWallet : this._model.wallet,
                winningsAmount : this._model.winnings,
                superbetAmount : this._model.superbet,
                numPromoGames : this._model.numPromoGames,
                numFreeSpins : this._model.numFreeSpins,
                result : this.parseBonusPhaseResult(result)
            });
        }
        else onFailure({
            code : "CL00",
            description : "No Bonus Phase result was available",
            isFatal : true
        });
    };


    /**
     * @protected
     * @param {*} rawResult 
     * @returns {Object} 
     */
    parseBonusPhaseResult(rawResult) {
        return rawResult;
    };


    /**
     * @public
     * @inheritdoc
     * @param {number} selectionId 
     * @param {() => void} onSuccess 
     * @param {CommsFailureHandler} onFailure 
     */
    sendBonusPhaseSelectionRequest(selectionId, onSuccess, onFailure) {
        onSuccess();
    };


    /**
     * @public
     * @inheritdoc
     * @param {number[]} selectionIds 
     * @param {() => void} onSuccess 
     * @param {CommsFailureHandler} onFailure 
     */
    sendBonusPhaseMultipleSelectionRequest(selectionIds, onSuccess, onFailure) {
        onSuccess();
    };


    /**
     * @public
     * @inheritdoc
     * @param {() => void} onSuccess 
     * @param {CommsFailureHandler} onFailure 
     */
    sendBonusPhaseCompleteRequest(onSuccess, onFailure) {
        onSuccess();
    };


    /**
     * @public
     * @inheritDoc
     * @param {CommsSuccessHandler<GameResultPacket<FreeSpinPhaseResult>>} onSuccess 
     * @param {CommsFailureHandler} onFailure 
     */
    sendFreeSpinPhaseResultsRequest(onSuccess, onFailure) {
        let result = this.getFreeSpinPhaseResult();

        if (result)
        {
            this._model.winnings += result.totalCreditWon;
            this._model.superbet += result.totalSuperBetWon;
            this._model.numPromoGames += result.numFreeGamesWon;

            onSuccess({
                betSettings : this._model.currBetSettings,
                currentWallet : this._model.wallet,
                winningsAmount : this._model.winnings,
                superbetAmount : this._model.superbet,
                numPromoGames : this._model.numPromoGames,
                numFreeSpins : this._model.numFreeSpins,
                result
            });
        }
        else
        {
            onFailure({
                code : "CL00",
                description : "No FreeSpin Phase result was available",
                isFatal : true
            });
        }
    };


    /**
     * @public
     * @inheritDoc
     * @param {() => void} onSuccess 
     * @param {CommsFailureHandler} onFailure 
     */
    sendFreeSpinPhaseCompleteRequest(onSuccess, onFailure) {
        onSuccess();
    };


    /**
     * @public
     * @inheritDoc
     * @param {CommsSuccessHandler<GameResultPacket<?>>} onSuccess
     * @param {CommsFailureHandler} onFailure
     */
    sendGamblePhaseResultsRequest(onSuccess, onFailure) {
        onFailure({
            code : "CL00",
            description : "Gamble no yet implemented",
            isFatal : true
        })
    };


    /**
     * @public
     * @inheritdoc
     * @param {() => void} onSuccess 
     * @param {CommsFailureHandler} onFailure 
     */
    sendGamblePhaseCompleteRequest(onSuccess, onFailure) {
        onSuccess();
    };
}