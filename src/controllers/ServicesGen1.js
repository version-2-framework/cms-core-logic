import * as Parse from "../utils/ParseUtil";
import * as UrlParams from "../const/C_UrlParamsGen2";
import * as StringUtil from "../utils/StringUtil";
import * as ResponseType from "../const/C_ServiceResponseTypeGen1";
import * as ClientErrorCode from "../const/C_ClientErrorCode";
import * as CurrencyType from "../const/C_CurrencyType";
import { CommsGen1 } from './Comms';
import * as LogManager from "../logging/LogManager";
import { Gen1SpinResultParser } from "../model/Gen1SpinResultParser";
import { UAParser } from "ua-parser-js";
import * as GamePhase from "../const/C_SlotGamePhaseId";
import C_GameEvent from "../const/C_GameEvent";

const log = LogManager.getCommsLogger();

/**
 * This is a rule for italy.
 */
const MAX_SESSION_CREDIT = 100000;

/**
 * Base services class (containing only the raw operations that are generic to
 * all future maths types).
 * @implements ServicesApi
 */
export class ServicesGen1
{
    /**
     * Creates a new services object.
     * @param {Dependencies} dependencies 
     */
    constructor(dependencies)
    {
        /**
         * @private
         * @type {BusinessConfig}
         */
        this._businessConfig = dependencies.businessConfig;

        /**
         * @private
         * @type {Model}
         */
        this._model = dependencies.model;

        /**
         * @private
         * @type {CommsApi}
         */
        this._comms = new CommsGen1(dependencies);

        /**
         * Stores the URL being used for Comms.
         * @private
         * @type {string}
         * @todo:
         * For Internal builds, this needs to be configurable.
         */
        this._targetUrl = this._businessConfig.productionServerUrl;

        /**
         * @private
         * @type {EventEmitter}
         */
        this._events = dependencies.dispatcher;

        /**
         * @type {UrlParamsGen1}
         */
        let urlParams = dependencies.urlParams;

        /**
         * The licensee id, used as a prefix for Wmg Game Client Id
         * @type {string}
         */
        let clientIdPrefix = this._businessConfig.clientIdPrefix;

        // We must do the check like this, because an empty string evaluates to
        //  falsey, but an empty string is a completely valid client id prefix.
        if (this._businessConfig.clientIdPrefixV1 !== undefined &&
            this._businessConfig.clientIdPrefixV1 !== null) {
            clientIdPrefix = this._businessConfig.clientIdPrefixV1;
        }

        log.debug(`ServicesGen1: clientIdPrefix set to ${clientIdPrefix}`);

        /**
         * Simple cache of data which is irrelevant to the rest of the game client,
         * but vital for correctly implementing the Gen 1 platform's over-complicated
         * and badly specified comms. Some of this data will be progressively deprecated
         * from the main model class.
         */
        this._data =
        {
            /**
             * Wmg Web session id. This is always configured as a url parameter.
             * @type {string}
             */
            wmgWebSessionId : urlParams.JSESSIONID,

            /**
             * The Game Client id, that the Game Client must report to the server. This
             * is a special string constant for the name of the game (in lower case), prefixed
             * usually with the licensee id (although in the case of merkur italy, nothing is
             * prefixed). The licensee prefix is also in lower case. The game must report this
             * info, because this is how the server knows what game client is being played
             * (there are not special endpoints for each game maths engine)
             * @type {string}
             */
            wmgGameClientId : `${clientIdPrefix}${dependencies.config.gameId}`,
            
            /**
             * The Game Client Version, which the game client should report to the server. This
             * isn't critical to the server, but provides general info in logs about which version
             * of a game client was being played.
             */
            wmgGameClientVersion : dependencies.buildInfo.getBuildVersion(),

            /**
             * Current value of Session Id.
             * @type {string}
             */
            sessionId : "defaultSessionId",

            /**
             * Current value of ticket id.
             * @type {string}
             */
            ticketId : "defaultTicketId",

            /**
             * Current value of ticket progressive.
             * @type {number}
             */
            ticketProgressive : 0,

            /**
             * Total amount of credit added to the current game session.
             * @type {number}
             */
            amountOfCreditAddedToSession : 0,

            /**
             * @type {GameplayMode}
             */
            gameplayMode : "free",
            
            /**
             * Indicates if the current game session is a restored session.
             * @type {boolean}
             */
            isRestoredSession : false,

            /**
             * Indicates if a game session is currently open.
             * @type {boolean}
             */
            isSessionOpen : false,

            /**
             * Checks if the session is freeplay.
             * @return boolean
             */
            isSessionFreeplay() {
                return this.gameplayMode == "free";
            },

            /**
             * Generes a new Session Info object. The Gen1 platform only exists for Italy,
             * so only the Italian implementation of this object will be returned.
             * @return {SessionInfoItaly}
             */
            getRegulation()
            {
                return {
                    country : "ITA",
                    aamsSessionId : this.sessionId,
                    aamsTicketId : this.ticketId,
                    ticketProgr : this.ticketProgressive,
                    amountAlreadyImported  : this.amountOfCreditAddedToSession,
                    regMoneyLimit : 100000
                };
            },

            /**
             * @type {SlotBetSettings}
             */
            currentBetSettings : null,

            /**
             * Current "real" value of wallet, as understood by the server
             */
            realWallet : 0,

            /**
             * Current simulation value of wallet.
             */
            wallet : 0,

            /**
             * Current value of winnings.
             */
            winnings : 0,

            /**
             * Current value of superbet.
             */
            superbet : 0,

            /**
             * Current number of Promogames.
             */
            numPromoGames : 0,

            /**
             * Current number of Freespins.
             */
            numFreeSpins : 0,

            // TODO: This might have to be configured differently for 4FP support.
            /**
             * The most recent set of reel positions (or symbol ids) received.
             * @type {string}
             */
            currentSymbolsData : "0,0,0,0,0",

            /**
             * List of all bonus selections made.
             * @type {number[]}
             */
            bonusSelections : [],

            /**
             * @type {*}
             */
            bonusRoundResults : ""
        };

        /**
         * @private
         * @type {RequestPropertyInjector[]}
         */
        this._requestPropertyInjectors = [];
    };


    /**
     * Adds one or more request property injectors.
     * @public
     * @param {RequestPropertyInjector[]} requestPropertyInjectors 
     */
    addRequestPropertyInjectors(...requestPropertyInjectors)
    {
        this._requestPropertyInjectors.push(...requestPropertyInjectors);
    };


    /**
     * Sends a request to the server, on the currently configured target url.
     * @protected
     * @param {Object} request 
     * The request object to send.
     * @param {(reply? : Object) => void} onSuccess 
     * @param {CommsFailureHandler} onFailure 
     * @param {CommsRequestInfo} commsRequestInfo
     * A data object, describing the comms action being executed. Before the request is sent to
     * the server, this data object will be dispatched with the COMMS_REQUEST_SENT event.
     */
    sendRequest(request, onSuccess, onFailure, commsRequestInfo) {
        let customHeaders = {};

        this._requestPropertyInjectors.forEach(propertyInjector => {
            // TODO: This mechanism is awkward, because it implies that we can override
            // certain request header properties... for now, though, the mechanism seems
            // to be ok.
            if (propertyInjector.getRequestHeaderFields) {
                Object.assign(customHeaders, propertyInjector.getRequestHeaderFields());
            }
            
            if (propertyInjector.injectCommonProperties) {
                propertyInjector.injectCommonProperties(request);
            }
        });

        this._events.emit(C_GameEvent.COMMS_REQUEST_SENT, commsRequestInfo);

        this._comms.sendRequest(this._targetUrl, request, customHeaders, onSuccess, onFailure);
    };


    /**
     * Generates a new message id, to be sent with an outgoing request.
     * @protected
     * @return {string}
     * The message id generated.
     */
    getNewMessageId()
    {
        let messageId;
        let date = new Date();
        let sessionIsOpen = this._data.isSessionOpen;
        
        // If session is open, use method based off session id and ticket id
        if (sessionIsOpen)
        {
            let sessionId = this._data.sessionId;
            let ticketId = this._data.ticketId;
            messageId = sessionId.substring(sessionId.length - 4, sessionId.length) +
                ticketId.substring(ticketId.length - 4, ticketId.length) +
                StringUtil.zeroPadString(date.getUTCFullYear().toString(16), 3) +
                StringUtil.zeroPadString((date.getUTCMonth() + 1).toString(16), 2) +
                StringUtil.zeroPadString(date.getUTCDate().toString(16), 2) +
                StringUtil.zeroPadString(date.getHours().toString(16), 2) +
                StringUtil.zeroPadString(date.getUTCMinutes().toString(16), 2) +
                StringUtil.zeroPadString(date.getUTCSeconds().toString(16), 2) +
                StringUtil.zeroPadString(date.getMilliseconds().toString(16), 3);
        }
        // If session not open, use method based off sessionID
        else
        {
            let wmgSessionId = this._data.wmgWebSessionId || "                    ";
            messageId = wmgSessionId.substring(wmgSessionId.length - 8, wmgSessionId.length) +
                StringUtil.zeroPadString(date.getUTCFullYear().toString(16), 3) +
                StringUtil.zeroPadString((date.getUTCMonth() + 1).toString(16), 2) +
                StringUtil.zeroPadString(date.getUTCDate().toString(16), 2) +
                StringUtil.zeroPadString(date.getHours().toString(16), 2) +
                StringUtil.zeroPadString(date.getUTCMinutes().toString(16), 2) +
                StringUtil.zeroPadString(date.getUTCSeconds().toString(16), 2) +
                StringUtil.zeroPadString(date.getMilliseconds().toString(16), 3);
        }

        return messageId;
    };


    /**
     * 
     * @param {(reply:DownloadStatisticsData) => void} onSuccess 
     * @param {CommsFailureHandler} onFailure 
     */
    sendDownloadStatisticsRequest(onSuccess, onFailure)
    {
        log.info(`sendDownloadStatisticsRequest()`);

        let uaParser = new UAParser(navigator.userAgent);
        let date = new Date();

        /** @type {Gen1DownloadStatisticsRequest} */
        let request =
        {
            MTP : "WMG_DOWNLOAD_STATISTICS",
            MID : this.getNewMessageId(),
            WID : this._data.wmgWebSessionId,
            WGD : this._data.wmgGameClientId,
            WGV : this._data.wmgGameClientVersion,
            BRN  : uaParser.getBrowser().name,
            BRV  : uaParser.getBrowser().version,
            IPA	 : "192.168.10.20",
            IPV  : "IPv4",
            OS	 : uaParser.getOS().name,
            OSV	 : uaParser.getOS().version,
            PRT  : 80,
            DAY	 : StringUtil.zeroPadString(date.getUTCDate().toString(), 2),
            MTH	 : StringUtil.zeroPadString(date.getUTCMonth().toString(), 2),
            YEAR : date.getUTCFullYear().toString(),
            HOUR : StringUtil.zeroPadString(date.getUTCHours().toString(), 2),
            MNT  : StringUtil.zeroPadString(date.getUTCMinutes().toString(), 2),
		    SEC  : StringUtil.zeroPadString(date.getUTCSeconds().toString(), 2)
        };

        /**
         * Parse the reply returned by the server, and call onSuccess
         * @param {Gen1DownloadStatisticsReply} reply 
         */
        let parseReply = reply =>
        {
            /** @type {DownloadStatisticsData} */
            let parsedResponse =
            {
                playerNickName : reply.NNM
            }

            onSuccess(parsedResponse);
        }
        
        this.sendRequest(request, parseReply, onFailure, { messageType:"DownloadStatistics", isSessionRequest:false });
    };


    /**
     * @inheritDoc
     * @param {GameplayMode} gameplayMode
     * @param {(reply? : RestoredGameResultPacket<?>) => void} onSuccess
     * @param {CommsFailureHandler} onFailure
     */
    sendCheckForOpenSessionRequest(gameplayMode, onSuccess, onFailure)
    {
        log.info(`sendCheckForOpenSessionRequest(gameplayMode:${gameplayMode})`);

        /** @type {Gen1CheckPendingRequest} */
        let request =
        {
            MTP : "WMG_CHECK_PENDING_REQUEST",
            MID : this.getNewMessageId(),
            WID : this._data.wmgWebSessionId,
            SID : this._data.sessionId,
            FPL : false,
            WGD : this._data.wmgGameClientId,
            WGV : this._data.wmgGameClientVersion
        };

        /**
         * Parses the raw reply received from the Gen1 server.
         * @param {Gen1CheckPendingReply | Gen1ServiceReply} rawReply 
         */
        let parseReply = rawReply =>
        {
            // If the reply is of type "check pending reply", this is the GES V1
            // platform's way of saying "nothing to restore"
            if (rawReply.MTP === ResponseType.CHECK_FOR_OPEN_SESSION) {
                onSuccess();
            }
            // If the reply is anything else, its a game result message... or
            // just possibly a bonus seletion message. Unfortunately, the exact
            // meaning changes between the different maths integrations, so here,
            // we must defer to an implementation specific method for handling
            // the object returned.
            else
            {
                this.processGameRestorationReply(rawReply, onSuccess, onFailure);
            }
        };

        this.sendRequest(request, parseReply, onFailure, { messageType:"CheckPending", isSessionRequest:false });
    };


    /**
     * @protected
     * @param {*} rawReply 
     * The raw reply received from the server. The nature of this can change between
     * different Gen1 implementations.
     * @param {(reply : RestoredGameResultPacket<?>) => void} onSuccess 
     * @param {CommsFailureHandler} onFailure 
     */
    processGameRestorationReply(rawReply, onSuccess, onFailure)
    {
        log.fatal("No implementation of processGameRestorationReply has been provided");

        onFailure({
            code : "CL00",  // TODO: Use valid code
            description : "No implementation of processGameRestorationReply has been provided",
            isFatal : true
        });
    };


    /**
     * @public
     * @param {GameplayMode} gameplayMode
     * @param {(reply:BalanceData)=>void} onSuccess
     * @param {CommsFailureHandler} onFailure
     */
    sendCheckBalanceRequest(gameplayMode, onSuccess, onFailure)
    {
        log.info(`sendCheckBalanceRequest(mode:${gameplayMode})`);

        /** @type {Gen1CheckBalanceRequest} */
        let request =
        {
            MTP : "WMG_BALANCE_REQUEST",
            MID : this.getNewMessageId(),
            WID : this._data.wmgWebSessionId,
            FPL : gameplayMode == "free",
            CUR : "EUR",    // TODO ?
            DTE : ""        // TODO
        };

        // TODO : should be returned a "parsed" data type, with info about
        // "max session credit possible"
        // TODO : account balance here, should already take int account total added.
        // This logic is previously implemented in the CashierViewController, and
        // should be now pushed here (as on the V2 platform, )

        /**
         * Parses the raw reply returned by the server into a Gen2 Check Balance
         * Reply, and invokves onSuccess (passing the reply along).
         * @param {Gen1CheckBalanceReply} rawReply 
         */
        let parseReply = rawReply =>
        {
            // Enforce rules for Italy, about max session transfers
            // The V1 platform returns "the amount of credit the player has",
            // with no indication of wether rules exist.
            let accountBalance = Parse.asInt(rawReply.TBL);
            let amountAlreadyTransferred = this._data.amountOfCreditAddedToSession;
            let amountThatCanBeAddedToSession = MAX_SESSION_CREDIT - amountAlreadyTransferred;
            let maxTransfer = Math.min(accountBalance, amountThatCanBeAddedToSession);

            /** @type {BalanceData} */
            let reply =
            {
                accountBalance,
                maxTransfer,
                maxTransferInSession : MAX_SESSION_CREDIT
            };

            onSuccess(reply);
        }

        this.sendRequest(request, parseReply, onFailure, { messageType:"CheckBalance", isSessionRequest:true });
    };


    /**
     * @public
     * @param {number} requestedBalance
     * @param {GameplayMode} gameplayMode
     * @param {(reply:Gen2SessionInitReply) => void} onSuccess 
     * @param {CommsFailureHandler} onFailure 
     */
    sendSessionInitRequest(requestedBalance, gameplayMode, onSuccess, onFailure)
    {
        log.info(`sendSessionInitRequest(requestedBalance:${requestedBalance},mode:${gameplayMode})`);

        /** @type {Gen1SessionInitRequest} */
        let request =
        {
            MTP : "WMG400",
            MID : this.getNewMessageId(),
            WID : this._data.wmgWebSessionId,
            RAM : requestedBalance,
            FPL : gameplayMode == "free"? true : false,
            WGD : this._data.wmgGameClientId,
            WGV : this._data.wmgGameClientVersion
        };

        /**
         * Parses the raw reply received from the Gen1 server, and passes the
         * parsed result to the onSuccess callback.
         * @param {Gen1SessionInitReply} rawReply
         */
        let parseReply = rawReply =>
        {
            let newBalance = Parse.asInt(rawReply.SBL);
            let timeout = Parse.asInt(rawReply.TMT);
            
            this._data.sessionId = rawReply.SID;
            this._data.ticketId = rawReply.PID;
            this._data.ticketProgressive = 0;
            this._data.isSessionOpen = true;
            this._data.isRestoredSession = false;
            this._data.gameplayMode = gameplayMode;
            this._data.amountOfCreditAddedToSession = newBalance;
            this._data.realWallet = newBalance;
            this._data.wallet = newBalance;
            this._data.superbet = 0;
            this._data.winnings = 0;
            this._data.numPromoGames = 0;
            this._data.numFreeSpins = 0;

            /** @type {Gen2SessionInitReply} */
            let parsedReply =
            {
                messageType         : "WMG_SESSION_INIT_REPLY",
                wmgWebSessionId     : this._data.wmgWebSessionId,
                gameplayMode        : this._data.gameplayMode,
                currentWallet       : this._data.wallet,
                winningsAmount      : this._data.winnings,
                superbetAmount      : this._data.superbet,
                numPromoGames       : this._data.numPromoGames,
                numFreeSpins        : this._data.numFreeSpins,
                wmgRoundRef         : -1,
                timeout             : timeout,
                regulation          : this._data.getRegulation(),
                promoGamesAwarded   : false
            };

            onSuccess(parsedReply);
        }

        this.sendRequest(request, parseReply, onFailure, { messageType:"SessionInit", isSessionRequest:true });
    };


    /**
     * @public
     * @inheritDoc
     * @param {GameplayMode} gameplayMode 
     * The mode the player wants for their new game session.
     * @param {(reply:Gen2SessionInitReply) => void} onSuccess 
     * @param {CommsFailureHandler} onFailure 
     * Optional Comms Failure Handler, invoked in the case that the service fails due
     * to some kind of comms error, or the server returns a response packet that has an
     * error field (ie: the server refused to execute the service). If a failure handler
     * is not supplied, the service will default to throwing a Fatal Error event.
     */
    sendSessionInitWithMaxBalanceRequest(gameplayMode, onSuccess, onFailure)
    {
        log.info(`sendSessionInitWithMaxBalanceRequest(gameplayMode:${gameplayMode})`);

        /** @type {V1StartSessionWithoutCashierConfig} */
        let startSessionConfig = this._businessConfig.startSession;

        if (startSessionConfig.checkBalance)
        {
            log.debug(`ServicesGen1.sessionInitMaxBalance: checking balance first`);

            /**
             * @param {BalanceData} reply 
             */
            let handleBalanceReply = reply =>
            {
                let maxBalance = reply.maxTransferInSession;

                log.debug(`balance operation complete, attempting to start session with max credit of ${maxBalance}`);

                this.sendSessionInitRequest(maxBalance, gameplayMode, onSuccess, onFailure);
            }

            // This operation is basically a composite, of check balance, and session init
            this.sendCheckBalanceRequest(gameplayMode, handleBalanceReply, onFailure);
        }
        else
        {
            log.debug(`ServicesGen1.sessionInitMaxBalance: not checking balance first`);

            let isRealplay = gameplayMode === "real";
            let balanceToRequest = isRealplay?
                startSessionConfig.balanceValueToRequestRealplay : startSessionConfig.balanceValueToRequestFreeplay;

            this.sendSessionInitRequest(balanceToRequest, gameplayMode, onSuccess, onFailure);
        }
    };


    /**
     * 
     * @param {number} amountOfCreditToAdd 
     * @param {(reply:Gen2AddCreditReply) => void} onSuccess 
     * @param {CommsFailureHandler} onFailure 
     */
    sendAddCreditRequest(amountOfCreditToAdd, onSuccess, onFailure)
    {
        log.info(`sendAddCreditRequest(amountToAdd:${amountOfCreditToAdd})`);

        /**
         * @type {Gen1AddCreditRequest}
         */
        let request =
        {
            MTP : "WMG420",
            MID : this.getNewMessageId(),
            FPL : this._data.isSessionFreeplay(),
            WID : this._data.wmgWebSessionId,
            SID : this._data.sessionId,
            PID : this._data.ticketId,
            PPR : this._data.ticketProgressive,
            RLD : this._data.isRestoredSession,
            PAM : amountOfCreditToAdd,
            WLT : this._data.realWallet,
            STD : "",   // TODO
            STM : "",   // TODO
            WGD : this._data.wmgGameClientId,
            WGV : this._data.wmgGameClientVersion
        };

        /**
         * Parses the raw reply received from the Gen1 server, and passes the
         * parsed result to the onSuccess callback.
         * @param {Gen1AddCreditReply} rawReply
         */
        let parseReply = rawReply =>
        {
            let newWallet = Parse.asInt(rawReply.WLT);
            let amountAdded = Parse.asInt(rawReply.PAM);
            
            // Update out data cache
            this._data.sessionId = rawReply.SID;
            this._data.ticketId = rawReply.PID;
            this._data.ticketProgressive = rawReply.PPR;
            this._data.realWallet = newWallet;
            this._data.wallet = newWallet;
            this._data.amountOfCreditAddedToSession += amountAdded;

            /** @type {Gen2AddCreditReply} */
            let parsedReply =
            {
                messageType     : "WMG_ADD_CREDIT_REPLY",
                wmgWebSessionId : this._data.wmgWebSessionId,
                currentWallet   : this._data.realWallet,
                regulation      : this._data.getRegulation()
            };

            onSuccess(parsedReply);
        };

        this.sendRequest(request, parseReply, onFailure, { messageType:"AddCredit", isSessionRequest:true });
    };


    /**
     * @public
     * @param {(reply:Gen2SessionEndReply) => void} onSuccess
     * @param {CommsFailureHandler} onFailure
     */
    sendSessionEndRequest(onSuccess, onFailure)
    {
        log.info(`sendSessionEndRequest()`);

        let date = new Date();

        /** @type {Gen1SessionEndRequest} */
        let request =
        {
            MTP  : "WMG_SESSION_END_500_REQUEST",
            MID  : this.getNewMessageId(),
            WID  : this._data.wmgWebSessionId,
            SID  : this._data.sessionId,
            PID  : this._data.ticketId,
            PPR  : this._data.ticketProgressive,
            FPL  : this._data.isSessionFreeplay(),
            WLT  : this._data.realWallet,
            PNT  : 0,
            DAY  : StringUtil.zeroPadString(date.getUTCDay().toString(), 2),
            MTH  : StringUtil.zeroPadString(date.getUTCMonth().toString(), 2),
            YEAR : date.getUTCFullYear().toString(),
            HOUR : StringUtil.zeroPadString(date.getUTCHours().toString(), 2),
            MNT  : StringUtil.zeroPadString(date.getUTCMinutes().toString(), 2),
            SEC  : StringUtil.zeroPadString(date.getUTCSeconds().toString(), 2),

            // Bet information is something that the server has no legitimate
            // business requesting in a "Session End" request. Nonetheless, the
            // server expects it. Its not clear if the server actually validates
            // the bet data it receives in any way (there is almost no documentation
            // available for the Gen1 Game Engine Server, and although we requested
            // clarification on these details, Wmg Italy have failed to provide it).
            // So, sadly, it seems we must continue to tangle up this module with
            // data that has no business being here (we cannot fully decouple it).
            // It's also not 100% clear if BET here means "total bet" or "bet per hand"
            // (again, there isn't any documentation to explain it)
            BET  : 0,
            LNS  : 0,
        };

        /**
         * Parses the raw reply received from the Gen1 server, and passes the
         * parsed result to the onSuccess callback.
         * @param {Gen1SessionEndReply} rawReply
         */
        let parseReply = rawReply =>
        {
            // Clear some of the key cached session data.
            this._data.isSessionOpen = false;
            this._data.isRestoredSession = false;
            this._data.amountOfCreditAddedToSession = 0;            

            /** @type {Gen2SessionEndReply} */
            let parsedReply =
            {
                messageType     : "WMG_SESSION_END_REPLY",
                wmgWebSessionId : rawReply.WID,
                currentWallet   : 0 // TODO: this field isn't available in the old result..
            };

            onSuccess(parsedReply);
        }

        this.sendRequest(request, parseReply, onFailure, { messageType:"SessionEnd", isSessionRequest:true });
    };


    /**
     * @public
     * @param {(reply:Gen2GameCompleteReply) => void} onSuccess 
     * @param {CommsFailureHandler} onFailure 
     */
    sendGameCompleteRequest(onSuccess, onFailure)
    {
        log.info(`sendGameCompleteRequest()`);

        // V1 platform doesn't have an operation here.
        // So, we can complete instantly, and pass back
        // a generated reply object.

        // "pay out" winnings
        this._data.wallet = this._data.realWallet;

        // Game is over, so reset all the winnings stats
        this._data.winnings = 0;
        this._data.superbet = 0;
        this._data.numPromoGames = 0;
        this._data.numFreeSpins = 0;

        /** @type {Gen2GameCompleteReply} */
        let reply =
        {
            messageType     : "WMG_GAME_COMPLETE_REPLY",
            wmgWebSessionId : this._data.wmgWebSessionId,
            currentWallet   : this._data.realWallet
        };

        onSuccess(reply);
    };


    /**
     * @public
     * @param {() => void} onSuccess 
     * @param {CommsFailureHandler} onFailure 
     */
    sendKeepAliveRequest(onSuccess, onFailure)
    {
        log.info(`sendKeepAliveRequest()`);

        /**
         * @type {Gen1KeepAliveRequest}
         */
        let request =
        {
            MTP : "WMG_GAME_KEEP_ALIVE_REQUEST",
            MID : this.getNewMessageId(),
            WID : this._data.wmgWebSessionId,
            SID : this._data.sessionId,
            PID : this._data.ticketId,
            PPR : this._data.ticketProgressive,
            FPL : this._data.isSessionFreeplay()
        };

        this.sendRequest(request, onSuccess, onFailure, { messageType:"KeepAlive", isSessionRequest:false });
    };
};


//--------------------------------------------------------------------------------------------------
// Slot game Serviuce Adapter for Gen 1 platform (although, this is the only basic implementation
// that will actually exist, it was still useful conceptually to tease the classes apart).
//--------------------------------------------------------------------------------------------------
/**
 * Slot game services for a Gen 1 game.
 */
export class SlotServicesGen1 extends ServicesGen1
{
    /**
     * @param {Dependencies} dependencies 
     */
    constructor(dependencies) {
        super(dependencies);

        /**
         * @protected
         */
        this._spinResultParser = new Gen1SpinResultParser(dependencies);
    };


    /**
     * Extracts Bet Settings from a Game phase Results message: useful for restoration.
     * @protected
     * @param {Gen1GamePhaseResultsReply} rawReply 
     * @return {SlotBetSettings}
     */
    extractBetSettings(rawReply)
    {
        let stakePerHand = rawReply.BET ? Parse.asInt(rawReply.BET) : 1;
        let numHands = rawReply.NHD ? Parse.asInt(rawReply.NHD) : 1;
        let numLines = rawReply.LNS ? Parse.asInt(rawReply.LNS) : 1;
        let stakePerLine = stakePerHand / numLines;
        let specialWagerMultiplier = 1; // always the default of 1 for a v1 maths engine
        let currencyType = CurrencyType.CREDIT; // always CREDIT for a v1 maths engine

        return { currencyType, stakePerLine, numLines, numHands, specialWagerMultiplier };
    };


    /**
     * @inheritDoc
     * @param {Gen1GamePhaseResultsReply} rawReply 
     * @param {(reply : RestoredGameResultPacket<?>) => void} onSuccess 
     * @param {CommsFailureHandler} onFailure 
     */
    processGameRestorationReply(rawReply, onSuccess, onFailure)
    {
        // Extract common data
        this._data.isSessionOpen = true;
        this._data.sessionId = rawReply.SID;
        this._data.ticketId = rawReply.PID;
        this._data.ticketProgressive = rawReply.PPR;
        this._data.isRestoredSession = true;
        this._data.gameplayMode = "real"; // it's always real on a restored game (for now - unless FunBonus appears on Gen1 platform)
        this._data.currentBetSettings = this.extractBetSettings(rawReply);
        this._data.realWallet = Parse.asInt(rawReply.WLT);
        this._data.winnings = 0;
        this._data.superbet = 0;
        this._data.numFreeSpins = 0;
        this._data.numPromoGames = 0;

        // For V1 platform, "amount of credit already added" is not returned here, which
        // means that the AddCredit operation is always available when we resume a game
        // session. This is a problem: the V1 solution, is therefore to disable the Add
        // Credit button. The neat solution to this, is to actually indicate here, that
        // for the restored session, we already added the maximum amount of credit. This
        // means that nothing else changes for the rest of our logic.
        let regulation = this._data.getRegulation();
        regulation.amountAlreadyImported = 100000; // 1000 euro, V1 is always italy

        // All Gen1 games implement Spin Phase Restoration in basically the
        // same way, so we may provide common functionality for the case where
        // the restoration reply is of one of the Spin Phase Results types.
        if (rawReply.MTP === ResponseType.SINGLE_SPIN_PHASE_RESULTS) {
            /** @type {Gen1SingleSpinPhaseResultsReply} */
            let singleSpinReply = rawReply;
            let parsedPhaseResult = this._spinResultParser.parseServerResult(singleSpinReply);

            this._data.wallet = this._data.realWallet - parsedPhaseResult.totalCreditWon;
            this._data.winnings = parsedPhaseResult.totalCreditWon;
            this._data.currentSymbolsData = singleSpinReply.RNR;

            /** @type {RestoredGameResultPacket<SingleSpinPhaseResult>} */
            let resultPacket = 
            {
                isReloaded          : true,
                betSettings         : this._data.currentBetSettings,
                currentWallet       : this._data.wallet,
                winningsAmount      : this._data.winnings,
                superbetAmount      : this._data.superbet,
                numPromoGames       : this._data.numPromoGames,
                numFreeSpins        : this._data.numFreeSpins,
                regulation          : regulation,
                result              : parsedPhaseResult,
                restoreGamePhase    : GamePhase.SINGLE_SPIN
            };

            onSuccess(resultPacket);
        }
        // TODO: For both Spin1 and Spin2, it looks like we are not parsing the correct
        // result type - we are extracting a generic spin phase result, whereas we need
        // the specific Spin1 / Spin2 phase results (with the extra holds and start
        // symbols data)
        else
        if (rawReply.MTP === ResponseType.SPIN_1_PHASE_RESULTS) {
            /** @type {Gen1Spin1PhaseResultsReply} */
            let spin1Reply = rawReply;
            let parsedPhaseResult = this._spinResultParser.parseServerResult(spin1Reply);

            this._data.wallet = this._data.realWallet - parsedPhaseResult.totalCreditWon;
            this._data.winnings = parsedPhaseResult.totalCreditWon;
            this._data.currentSymbolsData = spin1Reply.RNR;

            /** @type {RestoredGameResultPacket<Spin1PhaseResult>} */
            let resultPacket = 
            {
                isReloaded          : true,
                betSettings         : this._data.currentBetSettings,
                currentWallet       : this._data.wallet,
                winningsAmount      : this._data.winnings,
                superbetAmount      : this._data.superbet,
                numPromoGames       : this._data.numPromoGames,
                numFreeSpins        : this._data.numFreeSpins,
                regulation          : regulation,
                result              : parsedPhaseResult,
                restoreGamePhase    : GamePhase.SPIN_1
            };

            onSuccess(resultPacket);
        }
        else
        if (rawReply.MTP === ResponseType.SPIN_2_PHASE_RESULTS) {
            /** @type {Gen1Spin2PhaseResultsReply} */
            let spin2Reply = rawReply;
            let parsedPhaseResult = this._spinResultParser.parseServerResult(spin2Reply);

            this._data.wallet = this._data.realWallet - parsedPhaseResult.totalCreditWon;
            this._data.winnings = parsedPhaseResult.totalCreditWon;
            this._data.currentSymbolsData = spin2Reply.RNR;

            /** @type {RestoredGameResultPacket<Spin2PhaseResult>} */
            let resultPacket = 
            {
                isReloaded          : true,
                betSettings         : this._data.currentBetSettings,
                currentWallet       : this._data.wallet,
                winningsAmount      : this._data.winnings,
                superbetAmount      : this._data.superbet,
                numPromoGames       : this._data.numPromoGames,
                numFreeSpins        : this._data.numFreeSpins,
                regulation          : regulation,
                result              : parsedPhaseResult,
                restoreGamePhase    : GamePhase.SPIN_2
            };

            onSuccess(resultPacket);
        }
        // If its not a Spin phase, we assume its a bonus phase
        else
        {
            this.processBonusRestorationReply(rawReply, regulation, onSuccess, onFailure);
        }
    };


    /**
     * Processes a game restoration message, which is assumed to be a Bonus Phase Results reply
     * (because it was not tracked as a Spin Phase Results reply). The implementation allowed for
     * Bonus restoration changes between different games, so this method is intended as overridable
     * for specific Services implementations.
     * @protected
     * @param {Gen1GamePhaseResultsReply} rawReply
     * @param {SessionInfoItaly} regulation
     * @param {(reply : RestoredGameResultPacket<?>) => void} onSuccess 
     * @param {CommsFailureHandler} onFailure 
     */
    processBonusRestorationReply(rawReply, regulation, onSuccess, onFailure)
    {
        log.fatal("No processBonusRestorationReply implementation provided for this game client");

        // Unless this method is overriden and implemented,
        // we have no choice but to report a fatal error.
        onFailure({
            code : "CL00",  // TODO: Set better error code
            description : "No processBonusRestorationReply implementation provided",
            isFatal : true
        });
    };


    /**
     * @param {SlotBetSettings} betSettings
     * @param {GameStartParams | null} gameStartParams
     * Not used in this V1 implementation
     * @param {(reply : GameResultPacket<SingleSpinPhaseResult>) => void} onSuccess 
     * @param {CommsFailureHandler} onFailure 
     */
    sendSingleSpinPhaseResultsRequest(
        betSettings,
        gameStartParams,
        onSuccess, onFailure)
    {
        log.info(`sendSingleSpinPhaseResultsRequest(betSettings:${betSettings})`);

        let gameStartWallet = this._data.realWallet;
        let totalStake = betSettings.stakePerLine * betSettings.numHands * betSettings.numLines;

        /** @type {Gen1SingleSpinPhaseResultsRequest} */
        let request =
        {
            MTP : "WMG_GAME_START_REQUEST",
            MID : this.getNewMessageId(),
            WID : this._data.wmgWebSessionId,
            SID : this._data.sessionId,
            PID : this._data.ticketId,
            PPR : this._data.ticketProgressive,
            FPL : this._data.isSessionFreeplay(),
            RLD : this._data.isRestoredSession,
            WLT : this._data.realWallet,
            PNT : 0,
            PHS : 1,
            
            // TODO :not 100% sure if "stake per hand" is semantically correct,
            // but it will work for all games supported...
            BET : betSettings.numLines * betSettings.stakePerLine,
            LNS : betSettings.numLines,
            NHD : betSettings.numHands,
            RNR : this._data.currentSymbolsData.toString()  // TODO: Verify this is the correct encoding
        };

        /**
         * @param {Gen1SingleSpinPhaseResultsReply} rawReply 
         */
        let parseReply = rawReply =>
        {
            /** @type {SingleSpinPhaseResult} */
            let parsedPhaseResult = this._spinResultParser.parseServerResult(rawReply);

            this._data.currentBetSettings = betSettings;
            this._data.sessionId = rawReply.SID;
            this._data.ticketId = rawReply.PID;
            this._data.ticketProgressive = rawReply.PPR;
            this._data.realWallet = Parse.asInt(rawReply.WLT);
            this._data.wallet = gameStartWallet - totalStake;
            this._data.winnings += parsedPhaseResult.totalCreditWon;
            this._data.currentSymbolsData = rawReply.RNR;

            /** @type {GameResultPacket<SingleSpinPhaseResult>} */
            let parsedReply =
            {
                betSettings : betSettings,
                currentWallet : this._data.wallet,
                winningsAmount : this._data.winnings,
                superbetAmount : this._data.superbet,
                numPromoGames : this._data.numPromoGames,
                numFreeSpins : this._data.numFreeSpins,
                result : parsedPhaseResult
            };

            onSuccess(parsedReply);
        }

        this.sendRequest(request, parseReply, onFailure, { messageType:"SingleSpinResult", isSessionRequest:true });
    };


    /**
     * 
     * @param {() => void} onSuccess 
     * @param {CommsFailureHandler} onFailure 
     */
    sendSingleSpinPhaseCompleteRequest(onSuccess, onFailure)
    {
        log.info(`sendSingleSpinPhaseCompleteRequest()`);

        /** @type {Gen1SingleSpinPhaseCompleteRequest} */
        let request =
        {
            MTP : "WMG_SPIN_PHASE_COMPLETE_REQ",
            MID : this.getNewMessageId(),
            WID : this._data.wmgWebSessionId,
            SID : this._data.sessionId,
            PID : this._data.ticketId,
            PPR : this._data.ticketProgressive,
            FPL : this._data.isSessionFreeplay(),
            RLD : this._data.isRestoredSession
        };

        this.sendRequest(request, onSuccess, onFailure, { messageType:"SingleSpinComplete", isSessionRequest:true });
    };


    /**
     * 
     * @param {SlotBetSettings} betSettings 
     * @param {GameStartParams | null} gameStartParams
     * Not used in this V1 implementation
     * @param {(reply : GameResultPacket<Spin1PhaseResult>) => void} onSuccess 
     * @param {CommsFailureHandler} onFailure 
     */
    sendSpin1PhaseResultsRequest(betSettings, gameStartParams, onSuccess, onFailure)
    {
        log.info(`sendSpin1PhaseResultsRequest(betSettings:${betSettings})`);

        let gameStartWallet = this._data.realWallet;
        let totalStake = betSettings.stakePerLine * betSettings.numHands * betSettings.numLines;

        /** @type {Gen1Spin1PhaseResultsRequest} */
        let request =
        {
            MTP : "WMG_GAME_START1_REQUEST",
            MID : this.getNewMessageId(),
            WID : this._data.wmgWebSessionId,
            SID : this._data.sessionId,
            PID : this._data.ticketId,
            PPR : this._data.ticketProgressive,
            FPL : this._data.isSessionFreeplay(),
            RLD : this._data.isRestoredSession,
            WLT : this._data.realWallet,
            PNT : 0,
            PHS : 1,
            
            // TODO :not 100% sure if "stake per hand" is semantically correct,
            // but it will work for all games supported...
            BET : betSettings.numLines * betSettings.stakePerLine,
            LNS : betSettings.numLines,
            NHD : betSettings.numHands,
            RNR : this._data.currentSymbolsData.toString()  // TODO: Verify this is the correct encoding
        };

        /**
         * @param {Gen1Spin1PhaseResultsReply} rawReply 
         */
        let parseReply = rawReply =>
        {
            /** @type {Spin1PhaseResult} */
            let phaseResult = this._spinResultParser.parseServerResult(rawReply);

            this._data.currentBetSettings = betSettings;
            this._data.sessionId = rawReply.SID;
            this._data.ticketId = rawReply.PID;
            this._data.ticketProgressive = rawReply.PPR;
            this._data.realWallet = Parse.asInt(rawReply.WLT);
            this._data.wallet = gameStartWallet - totalStake;
            this._data.winnings += phaseResult.totalCreditWon;
            this._data.currentSymbolsData = rawReply.RNR;

            /** @type {GameResultPacket<Spin1PhaseResult>} */
            let parsedReply =
            {
                betSettings : betSettings,
                currentWallet : this._data.wallet,
                winningsAmount : this._data.winnings,
                superbetAmount : this._data.superbet,
                numPromoGames : this._data.numPromoGames,
                numFreeSpins : this._data.numFreeSpins,
                result : phaseResult
            };

            onSuccess(parsedReply);
        }

        this.sendRequest(request, parseReply, onFailure, { messageType:"Spin1Results", isSessionRequest:true });
    };


    /**
     * 
     * @param {() => void} onSuccess 
     * @param {CommsFailureHandler} onFailure 
     */
    sendSpin1PhaseCompleteRequest(onSuccess, onFailure)
    {
        log.info(`sendSpin1PhaseCompleteRequest()`);

        /** @type {Gen1Spin1PhaseCompleteRequest} */
        let request =
        {
            MTP : "WMG_SPIN1_PHASE_COMPLETE",
            MID : this.getNewMessageId(),
            WID : this._data.wmgWebSessionId,
            SID : this._data.sessionId,
            PID : this._data.ticketId,
            PPR : this._data.ticketProgressive,
            FPL : this._data.isSessionFreeplay(),
            RLD : this._data.isRestoredSession
        };

        this.sendRequest(request, onSuccess, onFailure, { messageType:"Spin1Complete", isSessionRequest:true });
    };


    /**
     * @param {HoldsPattern} holdsPattern
     * For the Gen1 platform, we only ever expect a "hold reels" type holds pattern.
     * @param {(reply : GameResultPacket<Spin2PhaseResult>) => void} onSuccess 
     * @param {CommsFailureHandler} onFailure 
     */
    sendSpin2PhaseResultsRequest(holdsPattern, onSuccess, onFailure)
    {
        log.info(`sendSpin2PhaseResultsRequest(holdsPattern:${holdsPattern})`);

        let betSettings = this._data.currentBetSettings;

        /** @type {Gen1Spin2PhaseResultsRequest} */
        let request =
        {
            MTP : "WMG_GAME_START2_REQUEST",
            MID : this.getNewMessageId(),
            WID : this._data.wmgWebSessionId,
            SID : this._data.sessionId,
            PID : this._data.ticketId,
            PPR : this._data.ticketProgressive,
            FPL : this._data.isSessionFreeplay(),
            RLD : this._data.isRestoredSession,
            WLT : this._data.realWallet,
            PNT : 0,
            PHS : 2,
            
            // TODO :not 100% sure if "stake per hand" is semantically correct,
            // but it will work for all games supported...
            BET : betSettings.numLines * betSettings.stakePerLine,
            LNS : betSettings.numLines,
            NHD : betSettings.numHands,
            HLD : this.encodeHoldsPattern(holdsPattern),
            RNR : this._data.currentSymbolsData.toString()  // TODO: Verify this is the correct encoding
        };

        /**
         * @param {Gen1Spin2PhaseResultsReply} rawReply 
         */
        let parseReply = rawReply =>
        {
            /** @type {Spin2PhaseResult} */
            let phaseResult = this._spinResultParser.parseServerResult(rawReply);

            this._data.currentBetSettings = betSettings;
            this._data.sessionId = rawReply.SID;
            this._data.ticketId = rawReply.PID;
            this._data.ticketProgressive = rawReply.PPR;
            this._data.realWallet = Parse.asInt(rawReply.WLT);
            this._data.winnings += phaseResult.totalCreditWon;
            this._data.currentSymbolsData = rawReply.RNR;

            /** @type {GameResultPacket<Spin2PhaseResult>} */
            let parsedReply =
            {
                betSettings : this._data.currentBetSettings,
                currentWallet : this._data.wallet,
                winningsAmount : this._data.winnings,
                superbetAmount : this._data.superbet,
                numPromoGames : this._data.numPromoGames,
                numFreeSpins : this._data.numFreeSpins,
                result : phaseResult
            };

            onSuccess(parsedReply);
        }

        this.sendRequest(request, parseReply, onFailure, { messageType:"Spin2Request", isSessionRequest:true });
    };


    /**
     * @public
     * @inheritdoc
     * @param {() => void} onSuccess 
     * @param {CommsFailureHandler} onFailure 
     */
    sendSpin2PhaseCompleteRequest(onSuccess, onFailure)
    {
        log.info(`sendSpin2PhaseCompleteRequest()`);

        /** @type {Gen1Spin2PhaseCompleteRequest} */
        let request =
        {
            MTP : "WMG_SPIN2_PHASE_COMPLETE",
            MID : this.getNewMessageId(),
            WID : this._data.wmgWebSessionId,
            SID : this._data.sessionId,
            PID : this._data.ticketId,
            PPR : this._data.ticketProgressive,
            FPL : this._data.isSessionFreeplay(),
            RLD : this._data.isRestoredSession
        };

        this.sendRequest(request, onSuccess, onFailure, { messageType:"Spin2Complete", isSessionRequest:true });
    };


    /**
     * @public
     * @inheritDoc
     * @param {(reply : GameResultPacket<BonusPhaseResult>) => void} onSuccess
     * @param {CommsFailureHandler} onFailure 
     */
    sendBonusPhaseResultsRequest(onSuccess, onFailure)
    {
        log.error(`sendBonusPhaseResultsRequest: no game specific service is implemented`);

        // The bonus operations need implementing by an extended class.
        // We leave in a default method that will invoke a ServerError if
        // called

        onFailure({
            code : ClientErrorCode.GAME_SERVICE_UNSUPPORTED,
            description : "Client is attempting to get BonusPhaseResults, but no game specific service is implemented.",
            isFatal : true
        });
    };


    /**
     * @public
     * @inheritdoc
     * @param {number} selectionId 
     * @param {() => void} onSuccess 
     * @param {CommsFailureHandler} onFailure 
     */
    sendBonusPhaseSelectionRequest(selectionId, onSuccess, onFailure)
    {
        log.error(`sendBonusPhaseSelectionRequest: no game specific service is implemented!`);

        // Bonus Selection Request also needs to be handled by the game
        // specific services implementation, as for V1 games, there are
        // always subtle differences between each game

        onFailure({
            code : ClientErrorCode.GAME_SERVICE_UNSUPPORTED,
            description : "Client is attempting to send Bonus Selection request, but no game specific sercice is implemented",
            isFatal : true
        });
    };


    /**
     * @public
     * @inheritdoc
     * @param {number[]} selectionIds 
     * @param {() => void} onSuccess 
     * @param {CommsFailureHandler} onFailure 
     */
    sendBonusPhaseMultipleSelectionRequest(selectionIds, onSuccess, onFailure)
    {
        log.error(`sendBonusPhaseMultipleSelectionRequest: no game specific service is implemented!`);

        // Bonus Multi Selection Request also needs to be handled by the
        // game specific services implementation, as for V1 games, there
        // are always subtle differences between each game

        onFailure({
            code : ClientErrorCode.GAME_SERVICE_UNSUPPORTED,
            description : "Client is attempting to send Bonus Multi Selection request, but no game specific sercice is implemented",
            isFatal : true
        });
    };


    /**
     * @public
     * @inheritDic
     * @param {() => void} onSuccess
     * @param {CommsFailureHandler} onFailure 
     */
    sendBonusPhaseCompleteRequest(onSuccess, onFailure)
    {
        log.error(`sendBonusPhaseCompleteRequest: no game specific sercice is implemented`);

        // The bonus operations need implementing by an extended class.
        // We leave in a default method that will invoke a ServerError if
        // called

        onFailure({
            code : ClientErrorCode.GAME_SERVICE_UNSUPPORTED,
            description : "Client is attempt to send BonusPhaseComplete, but no game specific service is implemented.",
            isFatal : true
        });
    };


    /**
     * @public
     * @inheritdoc
     * @param {(reply : GameResultPacket<FreeSpinPhaseResult>) => void} onSuccess
     * @param {CommsFailureHandler} onFailure 
     */
    sendFreeSpinPhaseResultsRequest(onSuccess, onFailure)
    {
        log.error(`sendFreeSpinPhaseResultsRequest: not supported on Gen1 platform`);

        // Gen1 platform supports no freespin operation, and unlike "GameComplete",
        // we cannot just implement a dummy request. If a gen1 game attempts to enter
        // FreeSpins, then its a straight up failure!

        onFailure({
            code : ClientErrorCode.GAME_SERVICE_UNSUPPORTED,
            description : "Client is attempting to fetch FreeSpin Phase Results: cannot play a FreeSpin phase on the Gen1 platform!",
            isFatal : true
        });
    };


    /**
     * @public
     * @inheritdoc
     * @param {() => void} onSuccess 
     * @param {CommsFailureHandler} onFailure 
     */
    sendFreeSpinPhaseCompleteRequest(onSuccess, onFailure)
    {
        log.error(`sendFreeSpinPhaseCompleteRequest: not supported on Gen1 platform`);

        // Gen1 platform supports no freespin operation, and unlike "GameComplete",
        // we cannot just implement a dummy request. If a gen1 game attempts to enter
        // FreeSpins, then its a straight up failure!

        onFailure({
            code : ClientErrorCode.GAME_SERVICE_UNSUPPORTED,
            description : "Client is attempting to send FreeSpin Phase Complete: cannot play a FreeSpin phase on the Gen1 platform!",
            isFatal : true
        });
    };


    /**
     * @public
     * @inheritdoc
     * @param {(reply : GameResultPacket<GamblePhaseResult>) => void} onSuccess 
     * @param {CommsFailureHandler} onFailure 
     */
    sendGamblePhaseResultsRequest(onSuccess, onFailure)
    {
        // Gen1 platform supports no gamble operation, and unlike "GameComplete",
        // we cannot just implement a dummy request. If a gen1 game attempts to play
        // a Gamble, then its a straight up failure!

        onFailure({
            code : ClientErrorCode.GAME_SERVICE_UNSUPPORTED,
            description : "Client is attempting to fetch GamblePhaseResults: cannot play a Gamble phase on the Gen1 platform!",
            isFatal : true
        });
    };


    /**
     * @public
     * @inheritdoc
     * @param {() => void} onSuccess 
     * @param {CommsFailureHandler} onFailure 
     */
    sendGamblePhaseCompleteRequest(onSuccess, onFailure)
    {
        // Gen1 platform supports no gamble operation, and unlike "GameComplete",
        // we cannot just implement a dummy request. If a gen1 game attempts to play a
        // Gamble, then its a straight up failure!

        onFailure({
            code : ClientErrorCode.GAME_SERVICE_UNSUPPORTED,
            description : "Client is attempting to send GamblePhaseComplete: cannot play a Gamble phase on the Gen1 platform!",
            isFatal : true
        });
    };


    // TODO: This needs to handle holds in the new, abstract manner
    /**
     * Encodes a holds pattern into the format that the V1 Game Engine Server expects.
     * @private
     * @param {HoldsPattern} holdsPattern
     * @return {string}
     */
    encodeHoldsPattern(holdsPattern)
    {
        let parsedPat = [];

        if (holdsPattern && holdsPattern.type === "holdReels")
        {
            holdsPattern.pattern.forEach(hand => {
                hand.forEach(reel => {
                    let reelIsHeld = reel;
                    parsedPat.push(reelIsHeld? "H" : "NH");
                });
            });
        }

        return parsedPat.toString();
    };
}

/**
 * Fowl play gold specific implementation.
 */
export class Gen1FpgServices extends SlotServicesGen1
{
    // TODO: it's not 100% clear that this should be handled in this way.
    // Should we still get back an instance of the "old" fpg response object ?
    /**
     * 
     * @param {(reply : Gen2BonusPhaseResultsReply) => void} onSuccess
     * @param {CommsFailureHandler} onFailure 
     */
    sendBonusPhaseResultsRequest(onSuccess, onFailure)
    {
        log.info(`sendBonusPhaseResultsRequest()`);

        let betSettings = this._data.currentBetSettings;

        /** @type {Gen1FpgBonusPhaseResultsRequest} */
        let request = 
        {
            MTP : "WMG_BONUS_GET_RESULTS_REQUEST",
            MID : this.getNewMessageId(),
            WID : this._data.wmgWebSessionId,
            SID : this._data.sessionId,
            PID : this._data.ticketId,
            PPR : this._data.ticketProgressive,
            FPL : this._data.isSessionFreeplay(),
            RLD : this._data.isRestoredSession,
            WLT : this._data.realWallet,
            PNT : 0,
            PHS : 3,
            BET : betSettings.stakePerLine * betSettings.numLines,
            LNS : betSettings.numLines,
            NHD : betSettings.numHands
        };

        // TODO: This is the wrong type of object to be returned.
        // However, we could parse the data into a useful format,
        // that is specific to Fowl Play Gold. Then any game skins
        // that continue to run off the old platform, can use a
        // readable JSON object, instead of the crap they get.
        /**
         * @param {Gen1FpgBonusPhaseResultsReply} rawReply
         */
        let parseReply = rawReply =>
        {
            /** @type {Gen2FpgBonusPhaseResultsReply} */
            let parsedReply =
            {

            };

            onSuccess(parsedReply);
        };

        this.sendRequest(request, parseReply, onFailure, { messageType:"FpgBonusResults", isSessionRequest:true });
    };


    /**
     * @param {() => void} onSuccess 
     * @param {CommsFailureHandler} onFailure 
     */
    sendBonusPhaseCompleteRequest(onSuccess, onFailure)
    {
        log.info("sendFpgBonusPhaseCompleteRequest()");

        /**
         * @type {Gen1FpgBonusPhaseCompleteRequest}
         */
        let request =
        {
            MTP : "WMG_BONUS_PHASE_COMPLETE",
            MID : this.getNewMessageId(),
            WID : this._data.wmgWebSessionId,
            SID : this._data.sessionId,
            PID : this._data.ticketId,
            PPR : this._data.ticketProgressive,
            FPL : this._data.isSessionFreeplay(),
            RLD : this._data.isRestoredSession,
            PHS : 3
        };

        this.sendRequest(request, onSuccess, onFailure, { messageType:"FpgBonusComplete", isSessionRequest:true });
    };
};


/**
 * Gen1 Haunted House specific implementation of Services. This is still effectively an abstrac
 * class: it provides an implementation of "sendBonusPhaseCompleteRequest", but no implenentation
 * of "sendBonusSelectionRequest" or "sendBonusPhaseMultiSelectionRequest"
 * - TODO: The selection requests probably SHOULD be supported for standard HH..
 * It also does not provide an implementation of "sendBonusPhaseResultsRequest"
 */
export class Gen1HhServices extends SlotServicesGen1
{
    


    // TODO: Now, is this a standard "two spin" phase complete request ?
    // I think that it might be.
    /**
     * @param {() => void} onSuccess 
     * @param {CommsFailureHandler} onFailure 
     */
    sendBonusPhaseCompleteRequest(onSuccess, onFailure)
    {
        log.info("sendHhBonusPhaseCompleteRequest()");

        /**
         * @type {Gen1HhBonusPhaseCompleteRequest}
         */
        let request =
        {
            MTP : "WMG_BONUS_PHASE_COMPLETE",
            MID : this.getNewMessageId(),
            WID : this._data.wmgWebSessionId,
            SID : this._data.sessionId,
            PID : this._data.ticketId,
            PPR : this._data.ticketProgressive,
            FPL : this._data.isSessionFreeplay(),
            RLD : this._data.isRestoredSession,
            PHS : 3
        };

        this.sendRequest(request, onSuccess, onFailure, { messageType:"HhBonusComplete", isSessionRequest:true });
    };
};


/**
 * Base implementation of HH Generic Services for Gen 1 platform: partially provides the messages required
 * for bonus phase (but not the Bonus Phase Results request !)
 * 
 * Generic Gen 1 Haunted House still requires the selection request to be sent : it is always a multi selection
 * request (indicating nonsense values... TODO: Confirm!!). Both the single, and multi, selection requests are
 */
export class Gen1HhGenericServices extends Gen1HhServices
{
    /**
     * Parses the Bonus Phase Result msg, into something usable for the game layer. For
     * the Gen 1 platform, this generally means making it more readable, and appending
     * standard data that may be missing in other contexts (eg: total amount won plus
     * other standard winnings fields).
     * @protected
     * @param {Gen1HhGenericBonusPhaseResultsReply} msg 
     * @return {GamePhaseResult}
     * @throws
     * An exception, if the msg returned does not match expectations for the game in use.
     */
    parseBonusPhaseResultMsg(msg) {
        throw ("No implementation of parseBonusPhaseResultMsg has been provided!");
    }


    /**
     * @inheritDoc
     * @public
     * @param {(reply : GameResultPacket<Object>) => void} onSuccess 
     * @param {CommsFailureHandler} onFailure 
     */
    sendBonusPhaseResultsRequest(onSuccess, onFailure)
    {
        log.info("sendBonusPhaseResultsRequest()");

        /**
         * @type {Gen1HhGenericBonusPhaseResultsRequest}
         */
        let request = 
        {
            MTP : "WMG_BONUS_GET_RESULTS_REQUEST",
            MID : this.getNewMessageId(),
            WID : this._data.wmgWebSessionId,
            SID : this._data.sessionId,
            PID : this._data.ticketId,
            PPR : this._data.ticketProgressive,
            FPL : this._data.isSessionFreeplay(),
            RLD : this._data.isRestoredSession,
            PHS : 3,
            WLT : this._data.realWallet,
            PNT : 0,
            BET : this._data.currentBetSettings.stakePerLine * this._data.currentBetSettings.numLines,
            LNS : this._data.currentBetSettings.numLines,
            NHD : 1
        };

        /**
         * @param {Gen1HhGenericBonusPhaseResultsReply} rawReply 
         */
        let parseReply = rawReply =>
        {
            this._data.realWallet = Parse.asInt(rawReply.WLT);
            this._data.winnings += Parse.asInt(rawReply.PZG);
            this._data.bonusSelections = [];
            this._data.bonusRoundResults = rawReply.CKP;

            try
            {
                let parsedPhaseResult = this.parseBonusPhaseResultMsg(rawReply);

                /** @type {GameResultPacket<?>} */
                let resultPacket =
                {
                    betSettings     : this._data.currentBetSettings,
                    currentWallet   : this._data.wallet,
                    winningsAmount  : this._data.winnings,
                    superbetAmount  : this._data.superbet,
                    numFreeSpins    : this._data.numFreeSpins,
                    numPromoGames   : this._data.numPromoGames,
                    regulation      : this._data.getRegulation(),
                    result          : parsedPhaseResult
                };

                onSuccess(resultPacket);
            }
            catch (e)
            {
                onFailure({
                    code : "CL00",  // TODO: Add better error code
                    description : `Could not parse Bonus Phase result message: ${e.toString()}`,
                    isFatal : true
                });
            }
        };

        this.sendRequest(
            request,
            parseReply,
            onFailure,
            { messageType:"HhGenericBonusResultsRequest", isSessionRequest:true });
    };


    /**
     * For Generic HH, we only support the multi-selection reuqest, so underneat, this method
     * calls through to it.
     * @public
     * @inheritdoc
     * @param {number} selectionId 
     * @param {() => void} onSuccess 
     * @param {CommsFailureHandler} onFailure 
     */
    sendBonusPhaseSelectionRequest(selectionId, onSuccess, onFailure) {
        this.sendBonusPhaseMultipleSelectionRequest([selectionId], onSuccess, onFailure)
    };


    /**
     * @public
     * @inheritdoc
     * @param {number[]} selectionIds 
     * @param {() => void} onSuccess 
     * @param {CommsFailureHandler} onFailure 
     */
    sendBonusPhaseMultipleSelectionRequest(selectionIds, onSuccess, onFailure) {
        // selection ids actually not at all important here. We are not even
        // going to use it.

        log.info("Send BonusPhaseSelectionRequest");

        // For generic hh, we send a single multi selection request, so its safe
        // to just cache that list of data (which is unimportant in any case)
        this._data.bonusSelections = selectionIds;

        // And for the "list of new selections made that the server needs to know about" -
        // we can just send a hard coded value of 1. The V1 server will ignore the values
        // in this field for generic HH (this is our understanding)
        let selectionsDataToSend = "1";

        /**
         * @type {Gen1HhGenericBonusMultiSelectionRequest}
         */
        let request =
        {
            MTP : "WMG_PLAY_BONUS_MULTIPLE_SELECTION_REQ",
            MID : this.getNewMessageId(),
            WID : this._data.wmgWebSessionId,
            SID : this._data.sessionId,
            PID : this._data.ticketId,
            PPR : this._data.ticketProgressive,
            RLD : this._data.isRestoredSession,
            FPL : this._data.isSessionFreeplay(),
            PHS : 3,
            CKP : this._data.bonusRoundResults,
            CKD : selectionsDataToSend,
            NCK : this._data.bonusSelections.toString()
        };

        // We don't need to do anything with the response data from the server:
        // it's sufficient to simply bubble up "success" status to the user of
        // the services api, and be done.
        let handleReply = () =>
        {
            onSuccess();
        };

        this.sendRequest(
            request,
            handleReply,
            onFailure,
            { messageType:"HhGenericBonusMultiSelection", isSessionRequest:true });
    };


    // TODO: Now, is this a standard "two spin" phase complete request ?
    // I think that it might be. If so, can be refactoerd as such. Note,
    // that on original HH, there are a LOT more fields sent in this 
    // message, and it is quite clear that none of them are required
    // for any meaningful reason (that does not mean that the server does
    // not demand them, however...)
    // In fact, if those fields are NOT required, then this is a generic
    // Gen1 Bonus Phase Complete request
    /**
     * @param {() => void} onSuccess 
     * @param {CommsFailureHandler} onFailure 
     */
    sendBonusPhaseCompleteRequest(onSuccess, onFailure)
    {
        log.info("sendHhGenericBonusPhaseCompleteRequest()");

        /**
         * @type {Gen1HhGenericBonusPhaseCompleteRequest}
         */
        let request =
        {
            MTP : "WMG_BONUS_PHASE_COMPLETE",
            MID : this.getNewMessageId(),
            WID : this._data.wmgWebSessionId,
            SID : this._data.sessionId,
            PID : this._data.ticketId,
            PPR : this._data.ticketProgressive,
            FPL : this._data.isSessionFreeplay(),
            RLD : this._data.isRestoredSession,
            PHS : 3
        };

        this.sendRequest(request, onSuccess, onFailure, { messageType:"HhGenericBonusComplete", isSessionRequest:true });
    };
};


/**
 * Gen1 Big Ghoulies specific implementation of Slot Services.
 */
export class Gen1BgServices extends SlotServicesGen1
{
    /**
     * Parses the Bonus Phase Result msg, into something usable for the game layer. For
     * the Gen 1 platform, this generally means making it more readable, and appending
     * standard data that may be missing in other contexts (eg: total amount won plus
     * other standard winnings fields).
     * @protected
     * @param {Gen1BgBonusPhaseResultsReply} msg 
     * @return {GamePhaseResult}
     * @throws
     * An exception, if the msg returned does not match expectations for the game in use.
     */
    parseBonusPhaseResultMsg(msg) {
        throw ("No implementation of parseBonusPhaseResultMsg has been provided!");
    };


    /**
     * @inheritDoc
     * @protected
     * @param {Gen1BgBonusPhaseResultsReply} rawReply
     * @param {SessionInfoItaly} regulation
     * @param {(reply : RestoredGameResultPacket<?>) => void} onSuccess 
     * @param {CommsFailureHandler} onFailure 
     */
    processBonusRestorationReply(rawReply, regulation, onSuccess, onFailure) {
        if (rawReply.MTP !== ResponseType.BONUS_1_SPIN_PHASE_RESULTS) {
            log.fatal(`Services.processBonusRestorationReply called for BG, but passed a ${rawReply.MTP}`);

            onFailure({
                code : "CL00",  // TODO: insert better error code
                description : `processBonusRestorationReply cannot handle a ${rawReply.MTP}`,
                isFatal : true
            });
        }
        else
        {
            // For all BG integrations, wallet reported back is the start wallet of the
            // bonus phase, even if some bonus selections have been played. We don't need
            // to do anything like "subtract the amount won".
            this._data.realWallet = Parse.asInt(rawReply.WLT);
            this._data.winnings = Parse.asInt(rawReply.WIN);
            this._data.wallet = this._data.realWallet;
            this._data.currentSymbolsData = rawReply.RNR;
            this._data.bonusSelections = Parse.asIntegerArray(rawReply.NCK);
            this._data.bonusRoundResults = rawReply.CKP;

            try {
                let parsedPhaseResult = this.parseBonusPhaseResultMsg(rawReply);
            
                /** @type {RestoredGameResultPacket<?>} */
                let resultPacket =
                {
                    betSettings         : this._data.currentBetSettings,
                    currentWallet       : this._data.wallet,
                    winningsAmount      : parsedPhaseResult.totalCreditWon, // we cannot know this accurately in the "real" sense
                    superbetAmount      : 0,
                    numPromoGames       : 0,
                    numFreeSpins        : 0,
                    regulation          : regulation,
                    result              : parsedPhaseResult,
                    isReloaded          : true,
                    restoreGamePhase    : GamePhase.BONUS,
                    previousSymbols     : this._spinResultParser.extractSymbols(Parse.asIntegerArray(rawReply.RNR)),
                    previousSelections  : this._data.bonusSelections
                };

                onSuccess(resultPacket);
            }
            catch (e)
            {
                onFailure({
                    code : "CL00",  // TODO: Add better error code
                    description : `Could not parse Bonus Phase result message: ${e.toString()}`,
                    isFatal : true
                });
            }
        }
    };


    /**
     * @inheritDoc
     * @public
     * @param {(reply : GameResultPacket<Object>) => void} onSuccess 
     * @param {CommsFailureHandler} onFailure 
     */
    sendBonusPhaseResultsRequest(onSuccess, onFailure)
    {
        log.info("sendBonusPhaseResultsRequest()");

        /**
         * @type {Gen1BgBonusPhaseResultsRequest}
         */
        let request = 
        {
            MTP : "WMG_SINGLE_SPIN_BONUS_GET_RESULTS_REQUEST",
            MID : this.getNewMessageId(),
            WID : this._data.wmgWebSessionId,
            SID : this._data.sessionId,
            PID : this._data.ticketId,
            PPR : this._data.ticketProgressive,
            FPL : this._data.isSessionFreeplay(),
            RLD : this._data.isRestoredSession,
            PHS : 3,
            WLT : this._data.realWallet,
            PNT : 0,
            BET : this._data.currentBetSettings.stakePerLine * this._data.currentBetSettings.numLines,
            LNS : this._data.currentBetSettings.numLines,
            NHD : 1
        };

        /**
         * @param {Gen1BgBonusPhaseResultsReply} rawReply 
         */
        let parseReply = rawReply =>
        {
            // TODO: Any more parsing that needs doing here ?
            this._data.realWallet = Parse.asInt(rawReply.WLT);
            this._data.winnings += Parse.asInt(rawReply.WIN);
            this._data.bonusSelections = [];
            this._data.bonusRoundResults = rawReply.CKP;

            try
            {
                let parsedPhaseResult = this.parseBonusPhaseResultMsg(rawReply);

                /** @type {GameResultPacket<?>} */
                let resultPacket =
                {
                    betSettings     : this._data.currentBetSettings,
                    currentWallet   : this._data.wallet,
                    winningsAmount  : this._data.winnings,
                    superbetAmount  : this._data.superbet,
                    numFreeSpins    : this._data.numFreeSpins,
                    numPromoGames   : this._data.numPromoGames,
                    regulation      : this._data.getRegulation(),
                    result          : parsedPhaseResult
                };

                onSuccess(resultPacket);
            }
            catch (e)
            {
                onFailure({
                    code : "CL00",  // TODO: Add better error code
                    description : `Could not parse Bonus Phase result message: ${e.toString()}`,
                    isFatal : true
                });
            }
        };

        this.sendRequest(request, parseReply, onFailure, { messageType:"BgBonusRequest", isSessionRequest:true });
    };


    /**
     * @public
     * @param {number} selection
     * The id of the item selected.
     * @param {() => void} onSuccess 
     * @param {CommsFailureHandler} onFailure 
     */
    sendBonusPhaseSelectionRequest(selection, onSuccess, onFailure)
    {
        log.info("Send BonusPhaseSelectionRequest");

        this._data.bonusSelections.push(selection);

        /**
         * @type {Gen1BgBonusSelectionRequest}
         */
        let request =
        {
            MTP : "WMG_SINGLE_SPIN_PLAY_BONUS_SELECTION_REQ",
            MID : this.getNewMessageId(),
            WID : this._data.wmgWebSessionId,
            SID : this._data.sessionId,
            PID : this._data.ticketId,
            PPR : this._data.ticketProgressive,
            RLD : this._data.isRestoredSession,
            FPL : this._data.isSessionFreeplay(),
            PHS : 3,
            CKP : this._data.bonusRoundResults,
            CKD : selection,
            NCK : this._data.bonusSelections.toString()
        };

        // TODO: Do we need, presently, to do any parsing of the response ?
        // I think that we do not..
        let handleReply = () =>
        {
            onSuccess();
        };

        this.sendRequest(request, handleReply, onFailure, { messageType:"BgBonusSelection", isSessionRequest:true });
    };


    /**
     * @param {() => void} onSuccess 
     * @param {CommsFailureHandler} onFailure 
     */
    sendBonusPhaseCompleteRequest(onSuccess, onFailure)
    {
        log.info("sendBonusPhaseCompleteRequest()");

        /**
         * @type {Gen1BgBonusPhaseCompleteRequest}
         */
        let request =
        {
            MTP : "WMG_BONUS_PHASE_COMPLETE_REQ",
            MID : this.getNewMessageId(),
            WID : this._data.wmgWebSessionId,
            SID : this._data.sessionId,
            PID : this._data.ticketId,
            PPR : this._data.ticketProgressive,
            FPL : this._data.isSessionFreeplay(),
            RLD : this._data.isRestoredSession,
            PHS : 3
        };

        /**
         * @param {Gen1BgBonusPhaseResultsReply} reply 
         */
        let handleReply = reply =>
        {
            this._data.realWallet = Parse.asInt(reply.WLT);
            onSuccess();
        }

        this.sendRequest(request, handleReply, onFailure, { messageType:"BgBonusComplete", isSessionRequest:true });
    };
};


/**
 * Gen 1 Four Fowl Play specific implementation of Slot Services.
 */
export class Gen1FfpServices extends SlotServicesGen1
{
    /**
     * @param {() => void} onSuccess 
     * @param {CommsFailureHandler} onFailure 
     */
    sendBonusPhaseCompleteRequest(onSuccess, onFailure)
    {
        log.info("sendFfpBonusPhaseCompleteRequest()");

        /**
         * @type {Gen1FfpBonusPhaseCompleteRequest}
         */
        let request =
        {
            MTP : "WMG_BONUS_PHASE_COMPLETE",
            MID : this.getNewMessageId(),
            WID : this._data.wmgWebSessionId,
            SID : this._data.sessionId,
            PID : this._data.ticketId,
            PPR : this._data.ticketProgressive,
            FPL : this._data.isSessionFreeplay(),
            RLD : this._data.isRestoredSession,
            PHS : 3
        };

        this.sendRequest(request, onSuccess, onFailure, { messageType:"FfpBonusComplete", isSessionRequest:true });
    };
};