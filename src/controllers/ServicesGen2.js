import * as RequestType from "../const/C_ServiceRequestTypeGen2";
import * as UrlParams from "../const/C_UrlParamsGen2";
import * as ResponseType from "../const/C_ServiceResponseTypeGen2";
import * as StringUtil from "../utils/StringUtil";
import * as ClientErrorCode from "../const/C_ClientErrorCode";
import * as GamePhase from "../const/C_SlotGamePhaseId";
import * as LicenseeData from "../model/Gen2LicenseeData";
import { CommsGen2 } from './Comms';
import * as LogManager from "../logging/LogManager";
import { Gen2SpinResultParser } from "../model/Gen2SpinResultParser";
import { UAParser } from "ua-parser-js";
import C_GameEvent from "../const/C_GameEvent";

const log = LogManager.getCommsLogger();

/**
 * @implements ServicesApi
 */
export class ServicesGen2
{
    /**
     * Creates a new services object.
     * @param {Dependencies} dependencies 
     */
    constructor(dependencies)
    {
        /**
         * @private
         * @type {BusinessConfig}
         */
        this._config = dependencies.businessConfig;

        /**
         * Identifier of the game client.
         * @private
         * @type {string}
         */
        this._wmgGameClientId = `${this._config.clientIdPrefix}${dependencies.config.gameId}`;

        /**
         * Game Client version.
         * @private
         * @type {BuildInfoApi}
         */
        this._buildInfo = dependencies.buildInfo;

        /**
         * @private
         * @type {CommsApi}
         */
        this._comms = new CommsGen2(dependencies);

        /**
         * Stores the URL being used for Comms.
         * @private
         * @type {string}
         */
        this._targetUrl = this._config.productionServerUrl;

        /**
         * @private
         * @type {EventEmitter}
         */
        this._events = dependencies.dispatcher;

        /** @type {UrlParamsGen2} */
        let urlParams = dependencies.urlParams;

        /**
         * Data cache that relates to the Gen 2 Game Engine Server comms. Instead of polluting our
         * main game model with this data (and requiring a dependency on model here), we have our
         * own custom data cache. This allows services to chat with the V2 server independently,
         * and lets us be sneaky about implementing any custom parsing that is required in this
         * tier.
         */
        this._data =
        {
            /**
             * Wmg Web session id. This is always configured as a url parameter.
             * @type {string}
             */
            wmgWebSessionId : urlParams.wmgWebSessionId || "DummyWebSessionId",

            /**
             * The maximum amount that a player may add to a session.
             */
            maxSessionTransfer : urlParams.regMoneyLimit || Number.POSITIVE_INFINITY,

            /**
             * The amount a player has already transferred to their session.
             */
            transferredToSession : 0,

            /**
             * @type {WmgGameDescription}
             */
            wmgGameDescription : 
            {
                gameId : urlParams.gameId || 0,
                licenseeId : urlParams.licenseeId || 0,
                //gameLabel : `${LicenseeData.getCommsIdForLicenseeId(urlParams.licenseeId)}${dependencies.config.gameId}`
                gameLabel : this._wmgGameClientId
            },

            /**
             * Tracks whether a game session is currently in progress.
             */
            isSessionOpen : false,

            /**
             * @type {GameplayMode}
             */
            gameplayMode : urlParams.gameplayMode || "free",

            /**
             * @type {SlotBetSettings}
             */
            currentBetSettings : {
                currencyType : "CREDIT",
                numHands : 1,
                numLines : 1,
                stakePerLine : 1,
                specialWagerMultiplier : 1
            },

            /**
             * Tracks the current balance of the game session.
             * @type {number}
             */
            wallet : 0,

            /**
             * Tracks the current value of winnings.
             * @type {number}
             */
            winnings : 0,

            /**
             * Tracks the current value of Superbet
             * @type {number}
             */
            superbet : 0,

            /**
             * Tracks the current number of FreeGames available to the player
             * @type {number}
             */
            numFreeGames : 0,

            /**
             * Tracks the current number of FreeSpins available to the player
             * @type {number}
             */
            numFreeSpins : 0,

            /**
             * Value of Wmg Round Ref. This will probably be a string, but its not 100% clear yet.
             * @type {number | string}
             */
            wmgRoundRef : -1,

            /**
             * Value of Wmg Spin Ref. This will probably be a string, but its not 100% clear yet.
             * @type {number | string}
             */
            wmgSpinRef : -1,

            /**
             * Value of Wmg Bonus Ref. This will probably be a string, but its not 100% clear yet.
             * @type {number | string}
             */
            wmgBonusRef : -1,

            /**
             * Value of Wmg FreeSpin Ref. This will probably be a string, but its not 100% clear yet.
             * @type {number | string}
             */
            wmgFreespinRef : -1,

            /**
             * @type {SessionInfo}
             */
            regulation : null,

            startSymbolIds : null
        }

        /**
         * @private
         * @type {RequestPropertyInjector[]}
         */
        this._requestPropertyInjectors = [];
    };


    /**
     * Adds one or more request property injectors.
     * @public
     * @param {RequestPropertyInjector[]} requestPropertyInjectors 
     */
    addRequestPropertyInjectors(...requestPropertyInjectors)
    {
        this._requestPropertyInjectors.push(...requestPropertyInjectors);
    };


    /**
     * Sends a request to the server, using the currently configured target url.
     * @protected
     * @param {Object} request 
     * @param {(reply? : Object) => void} onSuccess 
     * @param {CommsFailureHandler} onFailure 
     * @param {CommsRequestInfo} commsRequestInfo
     * A data object, describing the comms action being executed. Before the request is sent to
     * the server, this data object will be dispatched with the COMMS_REQUEST_SENT event.
     */
    sendRequest(request, onSuccess, onFailure, commsRequestInfo) {
        let customHeaders = {};

        this._requestPropertyInjectors.forEach(propertyInjector => {
            // TODO: This mechanism is awkward, because it implies that we can override
            // certain request header properties... for now, though, the mechanism seems
            // to be ok.
            if (propertyInjector.getRequestHeaderFields) {
                Object.assign(customHeaders, propertyInjector.getRequestHeaderFields());
            }
            
            if (propertyInjector.injectCommonProperties) {
                propertyInjector.injectCommonProperties(request);
            }
        });

        this._events.emit(C_GameEvent.COMMS_REQUEST_SENT, commsRequestInfo);
        
        this._comms.sendRequest(this._targetUrl, request, customHeaders, onSuccess, onFailure);
    };


    /**
     * Generates a new message id, to be sent with an outgoing request.
     * @protected
     * @return {string}
     * The message id generated.
     */
    getNewMessageId() {
        let messageId;
        let date = new Date();
        let sessionIsOpen = this._data.isSessionOpen;
        let sessionInfo = this._data.regulation;
        
        // TODO: I think we can tidy this up, by simply caching a reference to the required
        // method, instead of having a big switch statement.

        // If session is open, use method based off session id and ticket id
        if (sessionIsOpen && sessionInfo && sessionInfo.country === "ITA")
        {
            let sessionId = sessionInfo.aamsSessionId;
            let ticketId = sessionInfo.aamsTicketId;
            messageId = sessionId.substring(sessionId.length - 4, sessionId.length) +
                ticketId.substring(ticketId.length - 4, ticketId.length) +
                StringUtil.zeroPadString(date.getUTCFullYear().toString(16), 3) +
                StringUtil.zeroPadString((date.getUTCMonth() + 1).toString(16), 2) +
                StringUtil.zeroPadString(date.getUTCDate().toString(16), 2) +
                StringUtil.zeroPadString(date.getHours().toString(16), 2) +
                StringUtil.zeroPadString(date.getUTCMinutes().toString(16), 2) +
                StringUtil.zeroPadString(date.getUTCSeconds().toString(16), 2) +
                StringUtil.zeroPadString(date.getMilliseconds().toString(16), 3);
        }
        // If session not open, use method based off sessionID
        else
        {
            let wmgSessionId = this._data.wmgWebSessionId;
            messageId = wmgSessionId.substring(wmgSessionId.length - 8, wmgSessionId.length) +
                StringUtil.zeroPadString(date.getUTCFullYear().toString(16), 3) +
                StringUtil.zeroPadString((date.getUTCMonth() + 1).toString(16), 2) +
                StringUtil.zeroPadString(date.getUTCDate().toString(16), 2) +
                StringUtil.zeroPadString(date.getHours().toString(16), 2) +
                StringUtil.zeroPadString(date.getUTCMinutes().toString(16), 2) +
                StringUtil.zeroPadString(date.getUTCSeconds().toString(16), 2) +
                StringUtil.zeroPadString(date.getMilliseconds().toString(16), 3);
        }

        return messageId;
    };


    /**
     * @public
     * @inheritDoc
     * @param {(reply : DownloadStatisticsData) => void} onSuccess 
     * @param {CommsFailureHandler} onFailure 
     */
    sendDownloadStatisticsRequest(onSuccess, onFailure)
    {
        let uaParser = new UAParser(navigator.userAgent);

        /**
         * @type {Gen2DownloadStatisticsRequest}
         */
        let request = 
        {
            id : this.getNewMessageId(),
            messageType : "WMG_DOWNLOAD_STATISTICS_REQUEST",
            wmgWebSessionId : this._data.wmgWebSessionId,
            wmgGame :this._data.wmgGameDescription,
            gameplayMode : this._data.gameplayMode,
            gameClientVersion : this._buildInfo.getBuildVersion(),
            userAgent : `${uaParser.getOS().name}.${uaParser.getBrowser().name}`,
            userAgentVersion : uaParser.getBrowser().version
        };

        /**
         * Parses the data returned, and calls onSuccess
         * @param {Gen2DownloadStatisticsReply} reply 
         */
        let parseReply = reply =>
        {
            /** @type {DownloadStatisticsData} */
            let parsedData =
            {
                playerNickName : reply.userId
            }

            onSuccess(parsedData);
        }

        this.sendRequest(request, parseReply, onFailure, { messageType:"DownloadStatistics", isSessionRequest:false });
    };


    /**
     * @public
     * @inheritDoc
     * @param {GameplayMode} gameplayMode
     * @param {(reply ? : RestoredGameResultPacket<?>) => void} onSuccess 
     * @param {CommsFailureHandler} onFailure 
     */
    sendCheckForOpenSessionRequest(gameplayMode, onSuccess, onFailure)
    {
        log.info(`sendCheckForOpenSessionRequest(gameplayMode:${gameplayMode})`);

        /** @type {Gen2CheckPendingRequest} */
        let request =
        {
            messageType : "WMG_CHECK_PENDING_REQUEST",
            id : this.getNewMessageId(),
            gameplayMode : gameplayMode,
            wmgWebSessionId : this._data.wmgWebSessionId,
            wmgGame : this._data.wmgGameDescription
        };

        /**
         * 
         * @param {Gen2ServiceReply} rawReply 
         */
        let handleReply = rawReply =>
        {
            // TODO: Confirm how this is meant to work for V2 platform.
            if (rawReply.messageType === "WMG_CHECK_PENDING_REPLY")
            {
                // If it's a check pending reply, there is nothing to restore
                onSuccess();
            }
            else
            {
                // Defer to maths engine specific implementation for how to handle the response
                this.parseOpenSessionResponse(rawReply, onSuccess, onFailure);
            }
        };

        this.sendRequest(request, handleReply, onFailure, { messageType:"CheckPending", isSessionRequest:false });
    };


    /**
     * @protected
     * @param {Object} rawReply 
     * @param {(reply ? : RestoredGameResultPacket<?>) => void} onSuccess 
     * @param {CommsFailureHandler} onFailure 
     */
    parseOpenSessionResponse(rawReply, onSuccess, onFailure)
    {
        onFailure({
            code : ClientErrorCode.CLIENT_ERROR,
            isFatal : true,
            description : "No parsing method supplied for Open Session Response"
        });
    };


    /**
     * Fetches balance information from the server.
     * @public
     * @inheritDoc
     * @param {GameplayMode} gameplayMode
     * @param {(reply : BalanceData) => void} onSuccess 
     * @param {CommsFailureHandler} onFailure 
     * @param {Object} customRequestFields
     */
    sendCheckBalanceRequest(gameplayMode, onSuccess, onFailure, customRequestFields=null)
    {
        log.info(`sendCheckBalanceRequest(mode:${gameplayMode})`);

        /**
         * @type {Gen2CheckBalanceRequest}
         */
        let request =
        {
            id : this.getNewMessageId(),
            messageType : "WMG_BALANCE_REQUEST",
            wmgWebSessionId : this._data.wmgWebSessionId,
            gameplayMode : gameplayMode,
            wmgGame : this._data.wmgGameDescription
        };

        // Append any custom request fields required.
        if (customRequestFields) {
            for (let key in customRequestFields) {
                request[key] = customRequestFields[key];
            }
        }

        /**
         * @param {Gen2CheckBalanceReply} reply 
         */
        let parseReply = reply =>
        {
            let accountBalance = reply.accountBalance;
            let maxTransferInSession = this._data.maxSessionTransfer;
            let amountTransferred = this._data.transferredToSession;
            let maxTransfer = accountBalance;
            
            if (maxTransferInSession !== Number.POSITIVE_INFINITY) {
                maxTransfer = Math.min(accountBalance, maxTransferInSession - amountTransferred);
            }

            /**
             * @type {BalanceData}
             */
            let balanceData =
            {
                accountBalance,
                maxTransfer,
                maxTransferInSession
            };

            onSuccess(balanceData);
        }

        this.sendRequest(request, parseReply, onFailure, { messageType:"CheckBalance", isSessionRequest:true });
    };


    /**
     * Opens a new session on the game server.
     * @public
     * @inheritDoc
     * @param {number} requestedBalance 
     * @param {GameplayMode} gameplayMode 
     * @param {(reply : Gen2SessionInitReply) => void} onSuccess 
     * @param {CommsFailureHandler} onFailure 
     */
    sendSessionInitRequest(requestedBalance, gameplayMode, onSuccess, onFailure)
    {
        log.info(`sendSessionInitRequest(requestedBalance:${requestedBalance},mode:${gameplayMode})`);

        /**
         * @type {Gen2SessionInitRequest}
         */
        let request =
        {
            id : this.getNewMessageId(),
            messageType : "WMG_SESSION_INIT_REQUEST",
            gameplayMode : gameplayMode,
            transfMoney : requestedBalance,
            wmgGame : this._data.wmgGameDescription,
            wmgWebSessionId : this._data.wmgWebSessionId
        };

        /** @param {Gen2SessionInitReply} reply */
        let handleSuccessReply = reply =>
        {
            this._data.isSessionOpen = true;
            this._data.gameplayMode = reply.gameplayMode;
            this._data.wallet = reply.currentWallet;
            this._data.winnings = reply.winningsAmount;
            this._data.superbet = reply.superbetAmount;
            this._data.numFreeSpins = reply.numFreeSpins;
            this._data.regulation = reply.regulation;
            this._data.wmgRoundRef = reply.wmgRoundRef;

            onSuccess(reply);
        };

        this.sendRequest(request, handleSuccessReply, onFailure, { messageType:"SessionInit", isSessionRequest:true });
    };


    /**
     * Sends a Session Init Request, asking the server to open a new session with max
     * balance.
     * @public
     * @inheritDoc
     * @param gameplayMode 
     * The mode the player wants for their new game session.
     * @param {(reply:Gen2SessionInitReply) => void} onSuccess 
     * @param {CommsFailureHandler} onFailure 
     * Optional Comms Failure Handler, invoked in the case that the service fails due
     * to some kind of comms error, or the server returns a response packet that has an
     * error field (ie: the server refused to execute the service). If a failure handler
     * is not supplied, the service will default to throwing a Fatal Error event.
     * @param {Object} [customCheckBalanceRequestParameters]
     */
    sendSessionInitWithMaxBalanceRequest(gameplayMode, onSuccess, onFailure, customCheckBalanceRequestParameters)
    {
        log.info(`sendSessionInitWithMaxBalanceRequest(gameplayMode:${gameplayMode})`);

        /**
         * @param {BalanceData} balanceData 
         */
        let handleBalanceReply = balanceData =>
        {
            let transferAmount = balanceData.maxTransfer;

            log.debug(`balance operation complete, attempting to start session with max credit of ${transferAmount}`);

            this.sendSessionInitRequest(transferAmount, gameplayMode, onSuccess, onFailure);
        }

        // This operation is basically a composite, of check balance, and session init
        this.sendCheckBalanceRequest(gameplayMode, handleBalanceReply, onFailure, customCheckBalanceRequestParameters);
    };


    /**
     * @public
     * @inheritDoc
     * @param {number} amountOfCreditToAdd
     * @param {(reply : Gen2AddCreditReply) => void} onSuccess
     * @param {CommsFailureHandler} onFailure 
     */
    sendAddCreditRequest(amountOfCreditToAdd, onSuccess, onFailure)
    {
        log.info(`sendAddCreditRequest(amountToAdd:${amountOfCreditToAdd})`);

        /**
         * @type {Gen2AddCreditRequest}
         */
        let request = 
        {
            id : this.getNewMessageId(),
            messageType : "WMG_ADD_CREDIT_REQUEST",
            wmgWebSessionId : this._data.wmgWebSessionId,
            wmgGame : this._data.wmgGameDescription,
            regulation : this._data.regulation,
            currentWallet : this._data.wallet,
            gameplayMode : this._data.gameplayMode,
            transfMoney : amountOfCreditToAdd
        };

        /** @param {Gen2AddCreditReply} reply */
        let handleSuccessReply = reply =>
        {
            this._data.wallet = reply.currentWallet;
            this._data.regulation = reply.regulation;

            onSuccess(reply);
        };

        this.sendRequest(request, handleSuccessReply, onFailure, { messageType:"AddCredit", isSessionRequest:true });
    };


    /**
     * @public
     * @inheritDoc
     * @param {(reply : Gen2SessionEndReply) => void} onSuccess 
     * @param {CommsFailureHandler} onFailure 
     */
    sendSessionEndRequest(onSuccess, onFailure)
    {
        log.info('sendSessionEndRequest()');

        /**
         * @type {Gen2SessionEndRequest}
         */
        let request =
        {
            messageType : "WMG_SESSION_END_REQUEST",
            id : this.getNewMessageId(),
            gameplayMode : this._data.gameplayMode,
            wmgWebSessionId : this._data.wmgWebSessionId,
            wmgGame : this._data.wmgGameDescription,
            regulation : this._data.regulation
        };

        // TODO: This is a good case for parsing the data into something useful
        /**
         * @param {Gen2SessionEndReply} reply
         */
        let handleSuccessReply = reply =>
        {
            log.debug('Server has closed the Session');

            this._data.isSessionOpen = false;

            onSuccess(reply);
        };

        this.sendRequest(request, handleSuccessReply, onFailure, { messageType:"SessionEnd", isSessionRequest:true });
    };


    /**
     * @public
     * @inheritDoc
     * @param {(reply:Gen2GameCompleteReply, lmsCustomData?:Object) => void} onSuccess 
     * @param {CommsFailureHandler} onFailure 
     */
    sendGameCompleteRequest(onSuccess, onFailure)
    {
        log.info('sendGameCompleteRequest');

        /**
         * @type {Gen2GameCompleteRequest}
         */
        let request =
        {
            messageType : "WMG_GAME_COMPLETE_REQUEST",
            id : this.getNewMessageId(),
            wmgWebSessionId : this._data.wmgWebSessionId,
            gameplayMode : this._data.gameplayMode,
            wmgRoundRef : this._data.wmgRoundRef,
            wmgGame : this._data.wmgGameDescription,
            regulation : this._data.regulation
        };

        /**
         * @param {Gen2GameCompleteReply} reply 
         */
        let handleSuccessReply = reply =>
        {
            this._data.wallet = reply.currentWallet;
            this._data.winnings = 0;
            this._data.superbet = 0;
            this._data.numFreeGames = 0;
            this._data.numFreeSpins = 0;

            // Reset roundRef data
            this._data.wmgRoundRef = -1;
            this._data.wmgSpinRef = -1;
            this._data.wmgBonusRef = -1;
            this._data.wmgFreespinRef = -1;

            let lmsCustomData = reply.lmsExtraInfo;
            
            onSuccess(reply, lmsCustomData);
        };

        this.sendRequest(request, handleSuccessReply, onFailure, { messageType:"GameComplete", isSessionRequest:true });
    };


    /**
     * @public
     * @inheritDoc
     * @param {() => void} onSuccess 
     * @param {CommsFailureHandler} onFailure 
     */
    sendKeepAliveRequest(onSuccess, onFailure)
    {
        log.info(`sendKeepAliveRequest()`);

        /**
         * @type {Gen2KeepAliveRequest}
         */
        let request =
        {
            messageType : "WMG_KEEP_ALIVE_REQUEST",
            id : this.getNewMessageId(),
            wmgGame : this._data.wmgGameDescription,
            wmgWebSessionId : this._data.wmgWebSessionId,
            gameplayMode : this._data.gameplayMode
        };

        this.sendRequest(request, onSuccess, onFailure, { messageType:"KeepAlive", isSessionRequest:false });
    };
};


/**
 * Services implementation for a Slot Game on the V2 platform.
 */
export class SlotServicesGen2 extends ServicesGen2
{
    /**
     * @param {Dependencies} dependencies 
     */
    constructor(dependencies)
    {
        super(dependencies);

        /**
         * @private
         */
        this._spinResultParser = new Gen2SpinResultParser(dependencies);

        /**
         * @private
         */
        this._spinUtils = dependencies.spinModelUtils;
    };


    /**
     * @inheritDoc
     * @param {Object} rawReply 
     * @param {(reply ? : RestoredGameResultPacket<?>) => void} onSuccess 
     * @param {CommsFailureHandler} onFailure 
     */
    parseOpenSessionResponse(rawReply, onSuccess, onFailure)
    {
        let resultHandlers = 
        {
            [ResponseType.SINGLE_SPIN_PHASE_RESULTS] : {
                /** @param {Gen2SingleSpinPhaseResultsReply} rawReply */
                parseResult : rawReply => this._spinResultParser.parseSingleSpinPhaseResult(rawReply.result),
                gamePhaseId : GamePhase.SINGLE_SPIN
            },

            [ResponseType.SPIN_1_PHASE_RESULTS] : {
                /** @param {Gen2Spin1PhaseResultsReply} rawReply */
                parseResult : rawReply => this._spinResultParser.parseSpin1PhaseResult(rawReply.result),
                gamePhaseId : GamePhase.SPIN_1
            },
                    
            [ResponseType.SPIN_2_PHASE_RESULTS] : {
                /** @param {Gen2Spin2PhaseResultsReply} rawReply */
                parseResult : rawReply => this._spinResultParser.parseSpin2PhaseResult(rawReply.result),
                gamePhaseId : GamePhase.SPIN_2
            },

            [ResponseType.FREESPIN_PHASE_RESULTS] : {
                /** @param {Gen2FreeSpinPhaseResultsReply} rawReply */
                parseResult : rawReply =>  this._spinResultParser.parseFreeSpinPhaseResult(rawReply.result),
                gamePhaseId : GamePhase.FREESPIN
            },

            [ResponseType.BONUS_PHASE_RESULTS] : {
                /** @param {Gen2BonusPhaseResultsReply} rawReply */
                parseResult : rawReply => this.parseBonusPhaseResult(rawReply),
                gamePhaseId : GamePhase.BONUS
            },

            [ResponseType.QUICK_PHASE_RESULTS] : {
                /**
                 * @param {Gen2QuickPhaseResultsReply} rawReply */
                parseResult: rawReply => this._spinResultParser.parseQuickPhaseResult(rawReply.result),
                gamePhaseId: GamePhase.QUICK
            }
        };

        // TODO: Do we pass back a "GameResultPacket" or a "RestoredGameResultPacket" ?
        // In any case, the packet should indicate the following data:
        // - final values for all win types
        // - total wins for all win types
        // This is sufficient for us to set something like the correct game state,
        // even for restored Gen 1 games (although i still need to think about this)



        if (Object.keys(resultHandlers).indexOf(rawReply.messageType) > -1)
        {
            log.debug(`Session restoration message is ${rawReply.messageType}, and it's a message we can restore to.`);

            /** @type {Gen2SlotGamePhaseResultsReply} */
            let typedRawReply = rawReply;

            this.cacheBetSettings(typedRawReply.betSettings);
            this._data.regulation = typedRawReply.regulation;
            this._data.wallet = typedRawReply.currentWallet;
            this._data.winnings = typedRawReply.winningsAmount;
            this._data.superbet = typedRawReply.superbetAmount;
            this._data.numFreeGames = typedRawReply.numPromoGames;
            this._data.numFreeSpins = typedRawReply.numFreeSpins;
            this._data.wmgRoundRef = typedRawReply.wmgRoundRef;
            this._data.wmgSpinRef = typedRawReply.wmgSpinRef;
            this._data.wmgBonusRef = typedRawReply.wmgBonusRef;
            this._data.wmgFreespinRef = typedRawReply.wmgFreespinRef;

            let previousSymbols;

            if (typedRawReply.messageType === ResponseType.SPIN_1_PHASE_RESULTS) {
                this._data.startSymbolIds = typedRawReply.result.spin2StartSymbolIds;
            }
            else
            if (typedRawReply.messageType === ResponseType.BONUS_PHASE_RESULTS) {
                // TODO: We currently don't have this data! And for V2, we are not
                // looking to add it to the generic results messages. Instead, we
                // need to access it from the BonusPhaseResult. So this will need
                // to be updated. For now, we add some default meta-data.
                // IN FACT: on v2, we only expect to restore to the most recent
                // Single Spin / Spin 1 / Spin 2 (and never directly to Bonus or
                // FreeSpin). With some tidying up, this logic can be left in (in
                // principle, it can still work), but adding to the local documenation
                // that in practise, we only need to consider the main scenarios.
                previousSymbols = this._spinUtils.generateRandomSymbolsMatrix();
            }

            let resultHandler = resultHandlers[typedRawReply.messageType];
            let parsedPhaseResult = resultHandler.parseResult(typedRawReply);
            let restoredGamePhaseId = resultHandler.gamePhaseId;

            /**
             * @type {RestoredGameResultPacket<?>}
             */
            let gameResultPacket = 
            {
                regulation : this._data.regulation,
                
                betSettings : {
                    currencyType : this._data.currentBetSettings.currencyType,
                    numHands : this._data.currentBetSettings.numHands,
                    numLines : this._data.currentBetSettings.numLines,
                    stakePerLine : this._data.currentBetSettings.stakePerLine,
                    specialWagerMultiplier : this._data.currentBetSettings.specialWagerMultiplier
                },

                currentWallet : this._data.wallet,
                winningsAmount : this._data.winnings,
                superbetAmount : this._data.superbet,
                numPromoGames : this._data.numFreeGames,
                numFreeSpins : this._data.numFreeSpins,
                result : parsedPhaseResult,
                isReloaded : true,
                restoreGamePhase : restoredGamePhaseId,
                previousSymbols : previousSymbols
            };

            onSuccess(gameResultPacket);
        }
        else
        if (rawReply.messageType === ResponseType.GAMBLE_PHASE_RESULTS)
        {
            log.error("Session restoration message is Gamble Phase Results, but this is not currently supported");

            onFailure({
                code : ClientErrorCode.GAME_RESTORATION_MSG_UNSUPPORTED,
                description : `Gamble Phase restoration is not currently supported`,
                isFatal : true
            });
        }
        else
        {
            log.error(`Session sestoration message is of type ${rawReply.messageType}. This is not currently supported!`);

            onFailure({
                code : ClientErrorCode.GAME_RESTORATION_MSG_UNSUPPORTED,
                description : `This Game Client does not support restoring a session using a message of type ${rawReply.messageType}`,
                isFatal : true
            });
        }
    }

    sendQuickPhaseResultsRequest(betSettings, gameStartParams, onSuccess, onFailure)
    {
        log.info(`sendQuickPhaseResultsRequest()`);

        let numPromoGames = 0;
        if (gameStartParams && gameStartParams.numPromoGames) {
            numPromoGames = gameStartParams.numPromoGames;
        }

        /**
         * @type {Gen2QuickSpinPhaseResultsRequest}
         */
        let request = {
            messageType : "WMG_QUICK_PHASE_RESULTS_REQUEST",
            id : this.getNewMessageId(),
            wmgWebSessionId : this._data.wmgWebSessionId,
            wmgGame : this._data.wmgGameDescription,
            gameplayMode : this._data.gameplayMode,
            betSettings : betSettings,
            regulation : this._data.regulation,
            currentWallet : this._data.wallet,
            winningsAmount : this._data.winnings,
            superbetAmount : this._data.superbet,
            numPromoGames : numPromoGames,
            numFreeSpins : this._data.numFreeSpins,
            wmgRoundRef : this._data.wmgRoundRef
        }

        /**
         * @param {Gen2QuickPhaseResultsReply} reply 
         */
        let handleSuccessReply = reply =>
        {
            this.cacheBetSettings(betSettings);
            this._data.wallet = reply.currentWallet;
            this._data.winnings = reply.winningsAmount;
            this._data.superbet = reply.superbetAmount;
            this._data.numFreeGames = reply.numPromoGames;
            this._data.numFreeSpins = reply.numFreeSpins;
            this._data.wmgRoundRef = reply.wmgRoundRef;
            this._data.wmgBonusRef = reply.wmgBonusRef;
            this._data.wmgSpinRef = reply.wmgSpinRef;
            

            /** @type {GameResultPacket<import(QuickPhaseResult>} */
            let parsedReply =
            {
                betSettings : betSettings,
                currentWallet : reply.currentWallet,
                winningsAmount : reply.winningsAmount,
                superbetAmount : reply.superbetAmount,
                numPromoGames : reply.numPromoGames,
                numFreeSpins : reply.numFreeSpins,
                result: this._spinResultParser.parseQuickPhaseResult(reply.result)
            }

            // let lmsCustomData = reply;

            onSuccess(parsedReply);
        }


        this.sendRequest(request, handleSuccessReply, onFailure, { messageType:"QuickSpinResults", isSessionRequest:true });

    }


    /**
     * @public
     * @inheritDoc
     * @param {SlotBetSettings} betSettings
     * @param {GameStartParams | null} gameStartParams
     * @param {(reply : GameResultPacket<SingleSpinPhaseResult>, lmsCustomData?:Object) => void} onSuccess 
     * @param {CommsFailureHandler} onFailure 
     */
    sendSingleSpinPhaseResultsRequest(betSettings, gameStartParams, onSuccess, onFailure)
    {
        let numPromoGames = 0;
        if (gameStartParams && gameStartParams.numPromoGames) {
            numPromoGames = gameStartParams.numPromoGames;
        }

        /**
         * @type {Gen2SingleSpinPhaseResultsRequest}
         */
        let request =
        {
            messageType : "WMG_SINGLE_SPIN_PHASE_RESULTS_REQUEST",
            id : this.getNewMessageId(),
            wmgWebSessionId : this._data.wmgWebSessionId,
            wmgGame : this._data.wmgGameDescription,
            gameplayMode : this._data.gameplayMode,
            betSettings : betSettings,
            regulation : this._data.regulation,
            currentWallet : this._data.wallet,
            winningsAmount : this._data.winnings,
            superbetAmount : this._data.superbet,
            numPromoGames : numPromoGames,
            numFreeSpins : this._data.numFreeSpins,
            wmgRoundRef : this._data.wmgRoundRef
        };

        /**
         * @param {Gen2SingleSpinPhaseResultsReply} reply 
         */
        let handleSuccessReply = reply =>
        {
            this.cacheBetSettings(betSettings);
            this._data.wallet = reply.currentWallet;
            this._data.winnings = reply.winningsAmount;
            this._data.superbet = reply.superbetAmount;
            this._data.numFreeGames = reply.numPromoGames;
            this._data.numFreeSpins = reply.numFreeSpins;
            this._data.wmgRoundRef = reply.wmgRoundRef;
            this._data.wmgSpinRef = reply.wmgSpinRef;
            this._data.wmgBonusRef = reply.wmgBonusRef;
            this._data.wmgFreespinRef = reply.wmgFreespinRef;

            /** @type {GameResultPacket<SingleSpinPhaseResult>} */
            let parsedReply =
            {
                betSettings : betSettings,
                currentWallet : reply.currentWallet,
                winningsAmount : reply.winningsAmount,
                superbetAmount : reply.superbetAmount,
                numPromoGames : reply.numPromoGames,
                numFreeSpins : reply.numFreeSpins,
                result : this._spinResultParser.parseSingleSpinPhaseResult(reply.result)
            }

            let lmsCustomData = reply.lmsExtraInfo;

            onSuccess(parsedReply, lmsCustomData);
        };
        
        this.sendRequest(request, handleSuccessReply, onFailure, { messageType:"SingleSpinResults", isSessionRequest:true });
    };


    /**
     * @public
     * @inheritDoc
     * @param {() => void} onSuccess 
     * @param {CommsFailureHandler} onFailure 
     */
    sendSingleSpinPhaseCompleteRequest(onSuccess, onFailure)
    {
        /**
         * @type {Gen2SingleSpinPhaseCompleteRequest}
         */
        let request =
        {
            id : this.getNewMessageId(),
            messageType : "WMG_SINGLE_SPIN_PHASE_COMPLETE_REQUEST",
            wmgWebSessionId : this._data.wmgWebSessionId,
            wmgGame : this._data.wmgGameDescription,
            wmgRoundRef : this._data.wmgRoundRef,
            wmgSpinRef : this._data.wmgSpinRef,
            gameplayMode : this._data.gameplayMode,
            regulation : this._data.regulation,

            // WMG Italy are still requesting completely redundant information
            // to be sent to the server by the game client. I still don't understand
            // the reason for any of this
            betSettings : this._data.currentBetSettings,

            
        };

        /**
         * @param {Gen2SingleSpinPhaseCompleteReply} reply 
         */
        let handleSuccess = reply =>
        {
            onSuccess();
        };

        this.sendRequest(request, handleSuccess, onFailure, { messageType:"SingleSpinComplete", isSessionRequest:true });
    };


    /**
     * @public
     * @inheritDoc
     * @param {SlotBetSettings} betSettings
     * @param {GameStartParams | null} gameStartParams
     * @param {(reply : GameResultPacket<Spin1PhaseResult>, lmsCustomData?:Object) => void} onSuccess 
     * @param {CommsFailureHandler} onFailure
     */
    sendSpin1PhaseResultsRequest(betSettings, gameStartParams, onSuccess, onFailure)
    {
        log.info(`sendSpin1PhaseResultsRequest(betSettings:${betSettings})`);

        let numPromoGames = 0;
        if (gameStartParams && gameStartParams.numPromoGames) {
            numPromoGames = gameStartParams.numPromoGames;
        }

        /**
         * @type {Gen2Spin1PhaseResultsRequest}
         */
        let request =
        {
            id : this.getNewMessageId(),
            messageType : "WMG_SPIN_1_PHASE_RESULTS_REQUEST",
            wmgWebSessionId : this._data.wmgWebSessionId,
            wmgGame : this._data.wmgGameDescription,
            gameplayMode : this._data.gameplayMode,
            betSettings : betSettings,
            regulation : this._data.regulation,
            wmgRoundRef : this._data.wmgRoundRef,
            currentWallet : this._data.wallet,
            winningsAmount : this._data.winnings,
            superbetAmount : this._data.superbet,
            numPromoGames : numPromoGames,
            numFreeSpins : this._data.numFreeSpins
        };

        /**
         * @param {Gen2Spin1PhaseResultsReply} reply 
         */
        let handleSuccessReply = reply =>
        {
            this.cacheBetSettings(reply.betSettings);
            this._data.wallet = reply.currentWallet;
            this._data.winnings = reply.winningsAmount;
            this._data.superbet = reply.superbetAmount;
            this._data.numFreeGames = reply.numPromoGames;
            this._data.numFreeSpins = reply.numFreeSpins;
            this._data.wmgRoundRef = reply.wmgRoundRef;
            this._data.wmgSpinRef = reply.wmgSpinRef;
            this._data.wmgBonusRef = reply.wmgBonusRef;
            this._data.wmgFreespinRef = reply.wmgFreespinRef;
            this._data.startSymbolIds = reply.result.spin2StartSymbolIds;

            /** @type {GameResultPacket<Spin1PhaseResult>} */
            let parsedReply =
            {
                betSettings : betSettings,
                currentWallet : reply.currentWallet,
                winningsAmount : reply.winningsAmount,
                superbetAmount : reply.superbetAmount,
                numPromoGames : reply.numPromoGames,
                numFreeSpins : reply.numFreeSpins,
                result : this._spinResultParser.parseSpin1PhaseResult(reply.result)
            }

            let lmsCustomData = reply.lmsExtraInfo;

            onSuccess(parsedReply, lmsCustomData);
        }

        this.sendRequest(request, handleSuccessReply, onFailure, { messageType:"Spin1Results", isSessionRequest:true })
    };


    /**
     * @public
     * @inheritDoc
     * @param {() => void} onSuccess 
     * @param {CommsFailureHandler} onFailure 
     */
    sendSpin1PhaseCompleteRequest(onSuccess, onFailure)
    {
        log.info(`sendSpin1PhaseCompleteRequest()`);

        /**
         * @type {Gen2Spin1PhaseCompleteRequest}
         */
        let request =
        {
            id : this.getNewMessageId(),
            messageType : "WMG_SPIN_1_PHASE_COMPLETE_REQUEST",
            wmgWebSessionId : this._data.wmgWebSessionId,
            wmgGame : this._data.wmgGameDescription,
            wmgRoundRef : this._data.wmgRoundRef,
            wmgSpinRef : this._data.wmgSpinRef,
            gameplayMode : this._data.gameplayMode,
            regulation : this._data.regulation,
            betSettings : this._data.currentBetSettings
        };

        /**
         * @param {Gen2Spin1PhaseCompleteReply} reply 
         */
        let handleSuccess = reply =>
        {
            onSuccess();
        }

        this.sendRequest(request, handleSuccess, onFailure, { messageType:"Spin1Complete", isSessionRequest:true });
    };


    /**
     * @param {HoldsPattern} holdsPatternSelected
     * @param {(reply : GameResultPacket<Spin2PhaseResult>) => void} onSuccess 
     * @param {CommsFailureHandler} onFailure 
     */
    sendSpin2PhaseResultsRequest(holdsPatternSelected, onSuccess, onFailure)
    {
        log.info(`sendSpin2PhaseResultsRequest(holdsPattern:${JSON.stringify(holdsPatternSelected)})`);

        let parsedHoldsPattern = this.encodeHoldsPattern(holdsPatternSelected);

        /**
         * @type {Gen2Spin2PhaseResultsRequest}
         */
        let request =
        {
            id : this.getNewMessageId(),
            messageType : "WMG_SPIN_2_PHASE_RESULTS_REQUEST",
            wmgWebSessionId : this._data.wmgWebSessionId,
            wmgGame : this._data.wmgGameDescription,
            wmgRoundRef : this._data.wmgRoundRef,
            gameplayMode : this._data.gameplayMode,
            regulation : this._data.regulation,
            betSettings : this._data.currentBetSettings,
            currentWallet : this._data.wallet,
            winningsAmount : this._data.winnings,
            superbetAmount : this._data.superbet,
            numPromoGames : this._data.numFreeGames,
            numFreeSpins : this._data.numFreeSpins,
            startSymbolIds : this._data.startSymbolIds,
            selectedHolds : parsedHoldsPattern
        };

        /**
         * @param {Gen2Spin2PhaseResultsReply} reply 
         */
        let handleSuccessReply = reply =>
        {
            this._data.wallet = reply.currentWallet;
            this._data.winnings = reply.winningsAmount;
            this._data.superbet = reply.superbetAmount;
            this._data.numFreeGames = reply.numPromoGames;
            this._data.numFreeSpins = reply.numFreeSpins;
            this._data.wmgRoundRef = reply.wmgRoundRef;
            this._data.wmgSpinRef = reply.wmgSpinRef;
            this._data.wmgBonusRef = reply.wmgBonusRef;
            this._data.wmgFreespinRef = reply.wmgFreespinRef;

            /** @type {GameResultPacket<Spin2PhaseResult>} */
            let parsedReply =
            {
                betSettings : this._data.currentBetSettings,
                currentWallet : reply.currentWallet,
                winningsAmount : reply.winningsAmount,
                superbetAmount : reply.superbetAmount,
                numPromoGames : reply.numPromoGames,
                numFreeSpins : reply.numFreeSpins,
                result : this._spinResultParser.parseSpin2PhaseResult(reply.result)
            }

            onSuccess(parsedReply);
        }

        this.sendRequest(request, handleSuccessReply, onFailure, { messageType:"Spin2Complete", isSessionRequest:true });
    };


    /**
     * 
     * @param {() => void} onSuccess 
     * @param {CommsFailureHandler} onFailure 
     */
    sendSpin2PhaseCompleteRequest(onSuccess, onFailure)
    {
        log.info(`sendSpin2PhaseCompleteRequest()`);

        /**
         * @type {Gen2Spin2PhaseCompleteRequest}
         */
        let request =
        {
            id : this.getNewMessageId(),
            messageType : "WMG_SPIN_2_PHASE_COMPLETE_REQUEST",
            wmgWebSessionId : this._data.wmgWebSessionId,
            wmgGame : this._data.wmgGameDescription,
            gameplayMode : this._data.gameplayMode,
            wmgRoundRef : this._data.wmgRoundRef,
            wmgSpinRef : this._data.wmgSpinRef,
            regulation : this._data.regulation,
            betSettings : this._data.currentBetSettings
        };

        /**
         * @param {Gen2Spin2PhaseCompleteReply} reply 
         */
        let handleSuccess = reply =>
        {
            onSuccess();
        }

        this.sendRequest(request, handleSuccess, onFailure, { messageType:"Spin2Complete", isSessionRequest:true });
    };


    /**
     * @public
     * @inheritDoc
     * @param {(reply : GameResultPacket<?>) => void} onSuccess 
     * @param {CommsFailureHandler} onFailure 
     */
    sendBonusPhaseResultsRequest(onSuccess, onFailure)
    {
        log.info(`sendBonusPhaseResultsRequest()`);

        /**
         * @type {Gen2BonusPhaseResultsRequest}
         */
        let request =
        {
            id : this.getNewMessageId(),
            messageType : "WMG_BONUS_PHASE_RESULTS_REQUEST",
            wmgWebSessionId : this._data.wmgWebSessionId,
            wmgGame : this._data.wmgGameDescription,
            wmgRoundRef : this._data.wmgRoundRef,
            wmgBonusRef : this._data.wmgBonusRef,
            gameplayMode : this._data.gameplayMode,
            betSettings : this._data.currentBetSettings,
            currentWallet : this._data.wallet,
            winningsAmount : this._data.winnings,
            superbetAmount : this._data.superbet,
            numPromoGames : this._data.numFreeGames,
            numFreeSpins : this._data.numFreeSpins,
            regulation : this._data.regulation
        };

        /**
         * @param {Gen2BonusPhaseResultsReply} reply 
         */
        let handleSuccessReply = reply =>
        {
            this._data.wallet = reply.currentWallet;
            this._data.winnings = reply.winningsAmount;
            this._data.superbet = reply.superbetAmount;
            this._data.numFreeGames = reply.numPromoGames;
            this._data.numFreeSpins = reply.numFreeSpins;
            this._data.wmgRoundRef = reply.wmgRoundRef;
            this._data.wmgBonusRef = reply.wmgBonusRef;

            /** @type {GameResultPacket<?>} */
            let parsedReply =
            {
                betSettings : this._data.currentBetSettings,
                currentWallet : reply.currentWallet,
                winningsAmount : reply.winningsAmount,
                superbetAmount : reply.superbetAmount,
                numPromoGames : reply.numPromoGames,
                numFreeSpins : reply.numFreeSpins,
                result : this.parseBonusPhaseResult(reply)
            };

            log.debug(`BonusPhaseResult ready for use`);
            log.debug(JSON.stringify(parsedReply, null, 2));

            onSuccess(parsedReply);
        };

        this.sendRequest(request, handleSuccessReply, onFailure, { messageType:"BonusResults", isSessionRequest:true });
    };


    /**
     * Parses a Gen2 Bonus Phase Result, into some format that is more useful to the game client.
     * By default, this does nothing : it returns the standard Gen2 Bonus Phase Result object.
     * However, parsing can be implemented here, so that the object returned by the Services is
     * already in a standard form. This is useful for game clients where we need to maintain both
     * Gen1 and Gen2 support: the Services implementation can guarantee to return the same data
     * structure for both platforms.
     * @protected
     * @param {Gen2BonusPhaseResultsReply} resultsReply
     * @return {*}
     */
    parseBonusPhaseResult(resultsReply) {
        return resultsReply;
    };


    /**
     * The Bonus Phase Single Selection request is not required (or supported) for the Gen2 platform.
     * However, we provide it as part of the public API of the services object: we can implement
     * it as a simple noop, where the onSuccess callback is executed immediately.
     * @public
     * @inheritDoc
     * @param selectionId
     * The id of the selection that has just been made.
     * @param onSuccess 
     * A callback that will be invoked when the server acknowledges the selection request.
     * @param onFailure 
     * A failure handler that will be invoked if the service fails for any reason.
     */
    sendBonusPhaseSelectionRequest(selectionId, onSuccess, onFailure) {
        log.info('ServicesGen2.sendBonusPhaseSelectionRequest (dummy operation)');
        onSuccess();
    };


    /**
     * The Bonus Phase Multi Selection request is not required (or supported) for the Gen2 platform.
     * However, we provide it as part of the public API of the services object: we can implement
     * it as a simple noop, where the onSuccess callback is executed immediately.
     * @public
     * @inheritDoc
     * @param selectionIds
     * The ordered list of selections that have been made since the last selection
     * request was sent to the server. This should have at least 1 item present.
     * @param onSuccess 
     * A callback that will be invoked when the server acknowledges the selection request.
     * @param onFailure 
     * A failure handler that will be invoked if the service fails for any reason.
     */
    sendBonusPhaseMultipleSelectionRequest(selectionIds, onSuccess, onFailure) {
        log.info('ServicesGen2.sendBonusPhaseMultipleSelectionRequest (dummy operation)');
        onSuccess();
    };


    /**
     * @public
     * @inheritDoc
     * @param {() => void} onSuccess 
     * @param {CommsFailureHandler} onFailure 
     */
    sendBonusPhaseCompleteRequest(onSuccess, onFailure)
    {
        log.info(`sendBonusPhaseCompleteRequest()`);

        /**
         * @type {Gen2BonusPhaseCompleteRequest}
         */
        let request =
        {
            id : this.getNewMessageId(),
            messageType : "WMG_BONUS_PHASE_COMPLETE_REQUEST",
            wmgWebSessionId : this._data.wmgWebSessionId,
            wmgGame : this._data.wmgGameDescription,
            wmgRoundRef : this._data.wmgRoundRef,
            wmgBonusRef : this._data.wmgBonusRef,
            gameplayMode : this._data.gameplayMode,
            regulation : this._data.regulation,
            betSettings : this._data.currentBetSettings
        };

        /**
         * @param {Gen2BonusPhaseCompleteReply} reply 
         */
        let handleSuccess = reply =>
        {
            onSuccess();
        };

        this.sendRequest(request, handleSuccess, onFailure, { messageType:"BonusComplete", isSessionRequest:true });
    };


    /**
     * @public
     * @inheritDoc
     * @param {(reply : GameResultPacket<FreeSpinPhaseResult>) => void} onSuccess 
     * @param {CommsFailureHandler} onFailure 
     */
    sendFreeSpinPhaseResultsRequest(onSuccess, onFailure)
    {
        log.info(`sendFreeSpinPhaseResultsRequest()`);

        /**
         * @type {Gen2FreeSpinPhaseResultsRequest}
         */
        let request =
        {
            id : this.getNewMessageId(),
            messageType : "WMG_FREESPIN_PHASE_RESULTS_REQUEST",
            wmgWebSessionId : this._data.wmgWebSessionId,
            wmgGame : this._data.wmgGameDescription,
            wmgRoundRef : this._data.wmgRoundRef,
            wmgFreespinRef : this._data.wmgFreespinRef,
            gameplayMode : this._data.gameplayMode,
            regulation : this._data.regulation,
            betSettings : this._data.currentBetSettings,
            currentWallet : this._data.wallet,
            winningsAmount : this._data.winnings,
            superbetAmount : this._data.superbet,
            numPromoGames : this._data.numFreeGames,
            numFreeSpins : this._data.numFreeSpins
        };

        /**
         * @param {Gen2FreeSpinPhaseResultsReply} reply 
         */
        let handleSuccessReply = reply =>
        {
            this._data.wallet = reply.currentWallet;
            this._data.winnings = reply.winningsAmount;
            this._data.superbet = reply.superbetAmount;
            this._data.numFreeGames = reply.numPromoGames;
            this._data.numFreeSpins = reply.numFreeSpins;
            this._data.wmgRoundRef = reply.wmgRoundRef;
            this._data.wmgFreespinRef = reply.wmgFreespinRef;
            
            /** @type {GameResultPacket<FreeSpinPhaseResult>} */
            let parsedReply =
            {
                betSettings : this._data.currentBetSettings,
                currentWallet : reply.currentWallet,
                winningsAmount : reply.winningsAmount,
                superbetAmount : reply.superbetAmount,
                numPromoGames : reply.numPromoGames,
                numFreeSpins : reply.numFreeSpins,
                result : this._spinResultParser.parseFreeSpinPhaseResult(reply.result)
            }

            onSuccess(parsedReply);
        };

        this.sendRequest(request, handleSuccessReply, onFailure, { messageType:"FreeSpinResults", isSessionRequest:true });
    };


    /**
     * @public
     * @inheritDoc
     * @param {() => void} onSuccess 
     * @param {CommsFailureHandler} onFailure 
     */
    sendFreeSpinPhaseCompleteRequest(onSuccess, onFailure)
    {
        log.info(`sendFreeSpinPhaseCompleteRequest()`);

        /**
         * @type {Gen2FreeSpinPhaseCompleteRequest}
         */
        let request =
        {
            id : this.getNewMessageId(),
            messageType : "WMG_FREESPIN_PHASE_COMPLETE_REQUEST",
            wmgWebSessionId : this._data.wmgWebSessionId,
            wmgGame : this._data.wmgGameDescription,
            wmgRoundRef : this._data.wmgRoundRef,
            wmgFreespinRef : this._data.wmgFreespinRef,
            gameplayMode : this._data.gameplayMode,
            regulation : this._data.regulation,
            betSettings : this._data.currentBetSettings
        };

        /**
         * @param {Gen2FreeSpinPhaseResultsReply} reply 
         */
        let handleSuccess = reply =>
        {
            onSuccess();
        }

        this.sendRequest(request, handleSuccess, onFailure, { messageType:"FreeSpinComplete", isSessionRequest:true });
    };


    /**
     * @public
     * @inheritDoc
     * @param {(reply : Gen2GamblePhaseResultsReply) => void} onSuccess 
     * @param {CommsFailureHandler} onFailure 
     */
    sendGamblePhaseResultsRequest(onSuccess, onFailure)
    {
        /**
         * @type {Gen2GamblePhaseResultsRequest}
         */
        let request =
        {
            
        };

        //this.sendRequest(request, onSuccess, onFailure);

        onFailure({
            code : ClientErrorCode.GAME_SERVICE_UNSUPPORTED,
            description : 'The Gamble Phase Results operation is not yet implemented',
            isFatal : true
        });
    };


    /**
     * @public
     * @inheritDoc
     * @param {(reply : Gen2GamblePhaseCompleteReply) => void} onSuccess 
     * @param {CommsFailureHandler} onFailure 
     */
    sendGamblePhaseCompleteRequest(onSuccess, onFailure)
    {
        /**
         * @type {Gen2GamblePhaseCompleteRequest}
         */
        let request =
        {
            
        };

        // this.sendRequest(request, onSuccess, onFailure);

        onFailure({
            code : ClientErrorCode.GAME_SERVICE_UNSUPPORTED,
            description : 'The Gamble Phase Complete operation is not yet implemented',
            isFatal : true
        });
    };


    /**
     * Encodes a holds pattern into the format that the V2 Game Engine Server expects.
     * @private
     * @param {HoldsPattern} holdsPattern 
     * @return {boolean[][]}
     * A boolean array, where outer array represents "per hand data" and inner array is a set of
     * values pertaining to each hand.
     */
    encodeHoldsPattern(holdsPattern)
    {
        let parsedPattern = [];

        if (holdsPattern)
        {
            if (holdsPattern.type === "holdReels") {
                let pattern = holdsPattern.pattern;

                pattern.forEach(handPattern => {
                    let parsedPatternForHand = [];

                    handPattern.forEach(reelHoldState => {
                        parsedPatternForHand.push(reelHoldState);
                    });

                    parsedPattern.push(parsedPatternForHand);
                });
            }
            else
            if (holdsPattern.type === "holdSymbols") {
                let pattern = holdsPattern.pattern;

                pattern.forEach(handPattern => {
                    let parsedPatternForHand = [];

                    handPattern.forEach(reelPattern => {
                        reelPattern.forEach(symbolHoldState => {
                            parsedPatternForHand.push(symbolHoldState);
                        });
                    });

                    parsedPattern.push(parsedPatternForHand);
                });
            }
        }

        return parsedPattern;
    }


    /**
     * Quick method for copying all params from a received bet settings object, over to the
     * local bet settings object.
     * @private
     * @param {SlotBetSettings} betSettings 
     */
    cacheBetSettings(betSettings)
    {
        this._data.currentBetSettings.currencyType = betSettings.currencyType;
        this._data.currentBetSettings.numHands = betSettings.numHands;
        this._data.currentBetSettings.numLines = betSettings.numLines;
        this._data.currentBetSettings.stakePerLine = betSettings.stakePerLine;

        // NOTE: Server may NOT have "specialWagerMultiplier" implemented yet in all deployments
        this._data.currentBetSettings.specialWagerMultiplier =
            betSettings.specialWagerMultiplier >= 1? betSettings.specialWagerMultiplier : 1;
    }

    /**
     * @public
     * @inheritDoc
     * @param {() => void} onSuccess 
     * @param {CommsFailureHandler} onFailure 
     */
    sendQuickPhaseCompleteRequest(onSuccess, onFailure)
    {
        log.info(`sendQuickPhaseCompleteRequest()`);

        /**
         * @type {Gen2QuickPhaseCompleteRequest}
         */
        let request =
        {
            id : this.getNewMessageId(),
            messageType : "WMG_QUICK_PHASE_COMPLETE_REQUEST",
            wmgWebSessionId : this._data.wmgWebSessionId,
            wmgGame : this._data.wmgGameDescription,
            wmgRoundRef : this._data.wmgRoundRef,
            gameplayMode : this._data.gameplayMode,
            regulation : this._data.regulation,
            betSettings : this._data.currentBetSettings,
            wmgSpinRef: this._data.wmgSpinRef
        };

        /**
         * @param {Gen2QuickPhaseCompleteReply} reply 
         */
        let handleSuccess = reply =>
        {
            onSuccess();
        }

        this.sendRequest(request, handleSuccess, onFailure, { messageType:"QuickPhaseComplete", isSessionRequest:true });
    };
};