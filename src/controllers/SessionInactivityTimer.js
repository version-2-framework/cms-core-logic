import C_GameEvent from "../const/C_GameEvent";

// The behaviour here, is basically
// - start the timer when a session opens
// - end the timer when a session closes
// - while a session is open, puase the timer during a game

/**
 * @typedef SessionInactivityTimerDependencies
 */

export class SessionInactivityTimer
{
    /**
     * 
     * @param {number} timeoutMins
     */
    constructor(dependencies, timeoutMins)
    {
        /**
         * @private
         * @type {boolean}
         */
        this._timerActive = false;

        /**
         * @private
         * @type {number}
         */
        this._timeoutMins = timeoutMins;

        /**
         * @private
         * @type {EventEmitter}
         */
        this._events = dependencies.dispatcher;

        /**
         * @private
         * @type {boolean}
         */
        this._isSessionInProgress = false;

        this._events.on(C_GameEvent.SESSION_OPENED, this.onSessionOpened, this);
        this._events.on(C_GameEvent.SESSION_CLOSED, this.onSessionClosed, this);
    };


    /**
     * @private
     */
    onSessionOpened() {
        this._isSessionInProgress = true;
        this._events.on(C_GameEvent.GAME_STARTED, this.onGameStarted, this);
        this._events.on(C_GameEvent.GAME_FINISHED, this.onGameFinished, this);
        this.startTimer();
    };


    /**
     * @private
     */
    onSessionClosed() {
        this._isSessionInProgress = false;
        this._events.off(C_GameEvent.GAME_STARTED, this.onGameStarted, this);
        this._events.off(C_GameEvent.GAME_FINISHED, this.onGameFinished, this);
        this.killTimer();
    };


    /**
     * @private
     */
    onGameStarted() {
        this.killTimer();
    };


    /**
     * @private
     */
    onGameFinished() {
        this.startTimer();
    };


    /**
     * @private
     */
    startTimer() {
        if (this._timer) {
            this.killTimer();
        }

        let timeoutMs = this._timeoutMins * 1000;
        this._timer = setTimeout(() => this.onTimerFinished(), timeoutMs);
    };


    /**
     * @private
     */
    killTimer() {
        clearTimeout(this._timer);
        this._timer = null;
    };


    /**
     * @private
     */
    onTimerFinished() {
        // TODO: Dispatch inactivity timeout signal
    }
}