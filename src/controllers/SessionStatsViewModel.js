import BaseController from './BaseController';
import C_GameEvent from "../const/C_GameEvent";
import * as LogManager from "../logging/LogManager";
import { ExitClientUtil } from "../utils/ExitClientUtil";

export const SessionStatsViewState =
{
    CLOSING_SESSION : "ClosingSession",

    CLOSE_SESSION_FAILED : "CloseSessionFailed",

    SESSION_CLOSED : "SessionClosed"
};

export const SessionStatsViewEvents =
{
    /**
     * Fired when we enter the "Closing Session" view state.
     */
    ENTER_STATE_CLOSING_SESSION : "SessionStats.EnterStateClosingSession",
    
    /**
     * Fired when we exit the "Closing Session" view state
     */
    EXIT_STATE_CLOSING_SESSION : "SessionStats.ExitStateClosingSession",

    /**
     * Fired when we enter the "Close Session Failed" view state
     */
    ENTER_STATE_CLOSE_SESSION_FAILED : "SessionStats.EnterStateCloseSessionFailed",

    /**
     * Fired when we exit the "Close Session Failed" view state
     */
    EXIT_STATE_CLOSE_SESSION_FAILED : "SessionStats.ExitStateCloseSessionFailed",

    /**
     * Fired when we enter the "SessionClosed" view state
     */
    ENTER_STATE_SESSION_CLOSED : "SessionStats.EnterStateSessionClosed",

    /**
     * Fired when we exit the "SessionClosed" view state
     */
    EXIT_STATE_SESSION_CLOSED : "SessionStats.ExitStateSessionClosed"
}

/**
 * @typedef SessionStatsViewModelDependencies
 * @property {EventEmitter} dispatcher
 * @property {Model} model
 * @property {DialogController} dialogController
 * @property {ServicesApi} services
 * @property {BusinessConfig} businessConfig
 * @property {PlatformApi} platform
 */

/**
 * In this example, I have used "ViewModel" instead of "Controller" / "Model".
 * ViewModel combines these 2 concepts. The SessionStatsView will not have
 * direct access to the main model - instead, SessionStatsViewModel exposes
 * getters for any data that is required. This allows us to implement some
 * additional logic that is very specific to this view here.
 * 
 * The ViewModel also exposes "actions" for the view to invoke, and has a "state",
 * just like BonusController did. ViewModel will decide internally when to change
 * state, and dispatch an event. The SessionStatsView simply has to listen for the
 * state change event - these are the main points at which it will show different
 * sequences.
 */
export class SessionStatsViewModel extends BaseController
{
    /**
     * Creates a new instance of the Session Stats View Model.
     * @param {SessionStatsViewModelDependencies} dependencies 
     */
    constructor(dependencies)
    {
        super(dependencies);

        this.log = LogManager.getLogger('SessionStats');

        /**
         * @private
         * @type {Model}
         */
        this._model = dependencies.model;

        /**
         * @private
         * @type {ServicesApi}
         */
        this._services = dependencies.services;

        /**
         * @private
         * @type {BusinessConfig}
         */
        this._businessConfig = dependencies.businessConfig;

        /**
         * @private
         * @type {PlatformApi}
         */
        this._platform = dependencies.platform;

        this._id = 'SessionStats';
        this._enterStateEventId = 'EnterSessionStatsState';
        this._exitStateEventId  = 'ExitSessionStatsState';

        // TODO: This is a weird bit of implementation - is it still correct ??
        this._exitClientUtil = new ExitClientUtil(dependencies.urlParams);

        this.addState(SessionStatsViewState.CLOSING_SESSION, this.enterStateClosingSession, this.exitStateClosingSession);
        this.addState(SessionStatsViewState.CLOSE_SESSION_FAILED, this.enterStateCloseSessionFailed, this.exitStateCloseSessionFailed);
        this.addState(SessionStatsViewState.SESSION_CLOSED, this.enterStateSessionClosed, this.exitStateSessionClosed);

        // Initial start up logic (this assumes view is fully created)
        // TODO:
        // Agree with Wiener, where this will get placed. EG: if Session
        // Stats View creates the ViewModel, then we can make this call
        // right here.
        if (this._model.isSessionInProgress())
        {
            this.enterState(SessionStatsViewState.CLOSING_SESSION);
        }
        else
        {
            this.enterState(SessionStatsViewState.SESSION_CLOSED);
        }
    };

    
    //--------------------------------------------------------------------------------------------------
    // Here, we have state based actions. ViewModel combines a Controller and a domain specific Model.
    //--------------------------------------------------------------------------------------------------
    /**
     * Enter the session closing state. We expect the view to bind to this visual state.
     * ViewModel will send the Close Session request to the server, and enter into
     * "Session Closed" state when it is complete. If we wanted to add a little animation
     * in here in the view, we could adopt the standard pattern of having the view inform
     * ViewModel when that animation is complete (so ViewModel waits until animation & request
     * are done, before executing a state transition)
     * @private
     */
    enterStateClosingSession()
    {
        this._events.emit(SessionStatsViewEvents.ENTER_STATE_CLOSING_SESSION);

        this._services.sendSessionEndRequest(
            (reply) => this.onSessionClosed(reply),
            (error) => this.onSessionClosedFailure(error));
    };


    /**
     * @private
     */
    exitStateClosingSession() {
        this._events.emit(SessionStatsViewEvents.EXIT_STATE_CLOSING_SESSION);
    };


    /**
     * @private
     * @param {Gen2SessionEndReply} reply 
     */
    onSessionClosed(reply) {
        this.log.info("Server has acknowledged Session Closed");
        this._model.processSessionClosedMsg(reply);
        this.enterState(SessionStatsViewState.SESSION_CLOSED);
    }

    
    /**
     * @private
     * @param {ServiceExecutionError} error
     */
    onSessionClosedFailure(error) {
        this.log.error("Failed to end the current session with the server!");

        /**
         * The error returned when trying to close the session.
         * @private
         */
        this.closeSessionError = error;

        this.enterState(SessionStatsViewState.CLOSE_SESSION_FAILED);
    };


    //--------------------------------------------------------------------------------------------------
    // Session Closed Failure state
    //--------------------------------------------------------------------------------------------------
    /**
     * @private
     */
    enterStateCloseSessionFailed()
    {
        this._events.emit(SessionStatsViewEvents.ENTER_STATE_CLOSE_SESSION_FAILED);
    };


    /**
     * @private
     */
    exitStateCloseSessionFailed()
    {
        this._events.emit(SessionStatsViewEvents.EXIT_STATE_CLOSE_SESSION_FAILED);
    };


    /**
     * @public
     * @return {boolean}
     */
    canTryCloseSession() {
        let canRetry = false;

        if (this.getState() === SessionStatsViewState.CLOSE_SESSION_FAILED &&
            this.closeSessionError.isConnectionError)
        {
            canRetry = true;
        }

        return canRetry;
    };


    /**
     * @public
     */
    retryCloseSession()
    {
        if (this.getState() === SessionStatsViewState.CLOSE_SESSION_FAILED) {
            this.log.info('SessionStatsViewModel.retryCloseSession()');
            this.enterState(SessionStatsViewState.CLOSING_SESSION);
        }
    };


    //--------------------------------------------------------------------------------------------------
    // 
    //--------------------------------------------------------------------------------------------------
    /**
     * Enters the "session closed" state. View should bind to this, and show the appropriate
     * visual state.
     * @private
     */
    enterStateSessionClosed()
    {
        this._events.emit(SessionStatsViewEvents.ENTER_STATE_SESSION_CLOSED);
    };


    /**
     * @private
     */
    exitStateSessionClosed()
    {
        this._events.emit(SessionStatsViewEvents.EXIT_STATE_SESSION_CLOSED);
    };


    //--------------------------------------------------------------------------------------------------
    // 
    //--------------------------------------------------------------------------------------------------
    /**
     * When session is closed, we put the "SessionStatsView" into "session_closed" state. This means
     * that we show a list of statistics about the closed game session to the player : amount won,
     * num games, etc. This method will return a list of data objects, that define what statistics to
     * show, and how. All SessionStatsView needs to do, is render a list of textfields, correctly
     * using the data returned. In these way, SessionStatsViewModel can make decisions related to
     * which statistics to show. If we ever get a licensee specific request to add a different statistic
     * here (I consider this a possibility), then no change will be required in the SessionStatsView
     * (it is a business logic change)
     * @public
     * @return {TextConfig[]}
     */
    getSessionStats() {
        // Below, I show some example statistics to show how the data
        // would be returned. If we need to change the statistics for
        // a licensee, we add the change to the Config for that licensee.

        let stats = [];

        // TODO: I think these stats should be returned by the services layer ?
        // And not the model ?

        // Shows the total amount they won during the session
        stats.push({
            localizationId : "T_sessionStats_totalWon",
            localizationMods : {
                "[SESSION_WINNINGS]" : this._model.formatCurrency(this._model.getSessionCreditWon())
            }
        });

        // Total amount player spent
        stats.push({
            localizationId : "T_sessionStats_totalSpent",
            localizationMods : {
                "[SESSION_SPENT]" : this._model.formatCurrency(this._model.getSessionCreditSpent())
            }
        });

        // Num games played in the session
        stats.push({
            localizationId : "T_sessionStats_numGames",
            localizationMods : {
                "[SESSION_NUM_GAMES]" : this._model.getSessionNumGamesPlayed()
            }
        });

        return stats;
    };


    /**
     * Returns the configuration for the "Session Closed" message. This is an object,
     * that contains a LocalizationId (for the basic text item to show), and a list of
     * additional text modifications (as key/value pairs). By fetching this data from
     * SessionStatsViewModel, it means that we can quickly support additional messages
     * (which is a possible request from licensees), and that the modification is based
     * on "licensee specific business logic". This view simply has to render the information
     * (preferably in an interesting way)
     * @public
     * @return {TextConfig}
     */
    getSessionClosedMessageConfig() {
        // TODO: implement the message returned correctly
        return {localizationId:"T_sessionStats_sessionClosed"};
    };


    // TODO: This method should be changed, so that we have 2 methods
    // - getMobileFooterButtonConfigs
    // - getDesktopFooterButtonConfigs
    /**
     * Returns a list of configuration objects, for all buttons that should be shown
     * in the footer in the "SessionClosed" state.
     * @public
     * @return {TextButtonConfig[]}
     */
    getFooterButtonConfigs()
    {
        // So far, with the html games we have only shown a single button at a time,
        // and we have supported 3 options
        // 1) Session Restart button
        // 2) Close Client button
        // 3) nothing

        let configs = [];

        let viewConfig = this._businessConfig.sessionStatsView;
        let isMobileOrTablet = this._platform.isMobileOrTablet();
        let isDesktop = !isMobileOrTablet;

        if (!viewConfig) {
            this.log.error(`SessionStatsViewModel: no SessionStatsViewConfig available!`);
        }
        else
        {
            // Exit Client button has been enabled for the footer, but we only add a footer
            // button if the config exists and is valid.
            let exitClientButtonDesktop = viewConfig.exitClientButtonDesktop;
            let exitClientButtonMobile = viewConfig.exitClientButtonMobile;
            if ((isDesktop && exitClientButtonDesktop && exitClientButtonDesktop.isEnabled) ||
                (isMobileOrTablet  && exitClientButtonMobile  && exitClientButtonMobile.isEnabled ))
            {
                // Fetch the appropriate exit client action to use. 
                let exitClientAction;
                let exitClientButtonConfig = isMobileOrTablet? exitClientButtonMobile : exitClientButtonDesktop;
                if (exitClientButtonConfig.customAction) {
                    exitClientAction = exitClientButtonConfig.customAction;
                }
                else
                if (isMobileOrTablet)
                {
                    exitClientAction = this._businessConfig.exitClientActionMobile;
                }
                else
                {
                    exitClientAction = this._businessConfig.exitClientActionDesktop;
                }

                let validation = this._exitClientUtil.isExitClientActionValid(exitClientAction);

                // If the exit client action we found is valid, we can show the button
                if (validation.isValid) {
                    configs.push({
                        localizationId : "T_button_exitClient",
                        action : () => {
                            this._exitClientUtil.exitClient(exitClientAction);
                        }
                    })
                }
                // If the exit client action was in some way invalid, we don't show the
                // button, and log out the validation error.
                else
                {
                    let deviceType = isMobileOrTablet? 'mobile' : 'desktop';
                    this.log.error(`ExitClientButton configured for ${deviceType} view, but configuration is invalid: ${validation.error}`);
                }
            }
        
            // Should we include the Session Restart button ?
            let canRestartSession = this._model.canRestartSession();
            let restartSessionButtonDesktop = viewConfig.restartSessionButtonDesktop;
            let restartSessionButtonMobile = viewConfig.restartSessionButtonMobile;
            if ((isDesktop && restartSessionButtonDesktop && restartSessionButtonDesktop.isEnabled) ||
                (isMobileOrTablet  && restartSessionButtonMobile  && restartSessionButtonMobile.isEnabled ) &&
                canRestartSession)
            {
                configs.push({
                    localizationId : "T_button_restartSession",
                    action : () => this.actionOnRestartSession()
                });
            }
        }

        return configs;
    };


    /**
     * @private
     * @return {TextButtonConfig[]}
     */
    getMobileFooterButtonConfigs()
    {
        
    };


    /**
     * @private
     * @return {TextButtonConfig[]}
     */
    getDesktopFooterButtonConfigs()
    {

    }

    //--------------------------------------------------------------------------------------------------
    // Public actions, which may be invoked by the view
    // NOTE: because we want to be able to vary which actions are available in our business logic,
    // we don't want the view to directly call these at the moment, instead the ViewModel basically
    // returns a list of footer button configs, which specify which action goes on which button.
    //--------------------------------------------------------------------------------------------------
    /**
     * Called by the Restart Session button.
     * @private
     */
    actionOnRestartSession() {
        this.log.info("restartSession button pressed: invoke new Session functionality");

        // TODO:
        // Here, we need to pass this action over to main controller.
        // In the future, we may need to support some variability in
        // what is executed here.

        //Igor: I added event fire for destroying the SessionStatsView
        this._events.emit(C_GameEvent.HIDE_SESSION_STATS_VIEW);
        this._events.emit(C_GameEvent.BUTTON_RESTART_SESSION_PRESSED);
    }

    /**
     * Called by the Close Client button.
     * @private
     */
    actionOnCloseClient() {
        if (this._platform.isMobileOrTablet()) {

        }
        else {

        }
    }
}