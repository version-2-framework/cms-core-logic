import BaseController from "./BaseController";
import C_GameEvent from "../const/C_GameEvent";
import * as C_SgdGameEvent from "../const/C_SgdGameEvents";
import SoundController from "../../../cms-core-view/src/utils/SoundController";
import { SgdDialogController } from "./DialogController";
import { ExitClientUtil } from "../utils/ExitClientUtil";

// For SG Digital, this would be our list of possible states outside of a Game (which, if we break
// up GameController, means that Game states go in their own dedicated controller). Its fairly
// simple and minimal (we may add some additional menu controllers)
const AppState =
{
    PRE_LOADING : "SGDAppState_PreLoading",
    LOADING : "SGDAppState_Loading",
    FREE_ROUNDS_NOTIFICATION : "SGDAppState_FreeRoundsNotification",
    RESUME_GAME_NOTIFICATION : "SGDAppState_ResumeGameNotification",
    OPEN_SESSION : "SGDAppState_OpenSession",
    IDLE : "SGDAppState_Idle",
    FREEROUNDS_IDLE : "SGDAppState_FreeRoundsIdle",
    CHECK_BALANCE : "SGDAppState_CheckBalance",
    GAME_IN_PROGRESS : "SGDAppState_GameInProgress",
    AUTOPLAY_MENU : "SGDAppState_AutoplayMenu",
    BET_MENU : "SGDAppState_BetMenu",
    GAME_MENU : "SGDAppState_GameMenu",
    POST_GAME_NOTIFICATIONS : "SGDAppState_PostGameNotifications"
};

export {AppState as SgDigitalAppState};

/**
 * Dependencies required by the Sg Digital App Controller
 * @typedef SgdAppControllerDependencies
 * @property {GameController} gameController
 * @property {EventEmitter} dispatcher
 * @property {Model} model
 * @property {DialogController} dialogController
 * @property {ServicesApi} services
 * @property {Object} urlParams
 * @property {BusinessConfig} businessConfig
 * @property {PlatformApi} platform
 * Url parameters are required, because configuration data (including FreeRounds) will be passed
 * to our game clients through them.
 */

// TODO: If we need to inherit some common functionality, then we could inherit from a base class
// here.
class SgdAppController extends BaseController
{
    /**
     * Contructs a new GameController instance.
     * @param {SgdAppControllerDependencies} dependencies
     */
    constructor(dependencies)
    {
        super(dependencies);

        /**
         * Game Client id. We need to return this when GCM asks our GameWrapper for config data.
         * @private
         * @type {string}
         */
        this.gameNamePrintFriendly = dependencies.config.gameName;

        /**
         * @private
         * @type {GameController}
         */
        this._gameController = dependencies.gameController;

        /**
         * @private
         * @type {SoundController}
         */
        this._soundController = dependencies.sound;

        /**
         * @private
         * @type {Model}
         */
        this._model = dependencies.model;

        /**
         * @private
         * @type {ServicesApi}
         */
        this._services = dependencies.services;

        /**
         * Indicates the string id of any game state that must be restored to. If this flag is non-null,
         * it tells us that a game must be restored, and it tells us the id of the phase (required by the
         * Game Controller instance in use). Clear this flag back to null, to indicate that restoration
         * has been done.
         * @private
         * @tyhpe {string}
         */
        this._restoredGamePhase = null;

        /**
         * @private
         * @type {PlatformApi}
         */
        this._platform = dependencies.platform;
        
        /**
         * @private
         * @type {ExitClientUtil}
         */
        this._exitClientUtil = dependencies.exitClientUtil;

        /**
         * @private
         * @type {BusinessConfig}
         */
        this._businessConfig = dependencies.businessConfig;

        // TODO: The way options are enabled was originally cribbed from their sample game, but
        // it doesn't make much sense in context of our game. This can be tidied up.
        /**
         * @private
         * @type {SgdGcmOptionMap}
        */
        this._options =
        {
            GAME_PREFERENCES : { enabled:false, value:false }, // I don't think we want this one
            MUTE     : { enabled:false, value:false },
            ABOUT    : { enabled:false, value:false },
            TURBO    : { enabled:false, value:false },
            HELP     : { enabled:false, value:false },
            PAYTABLE : { enabled:false, value:false } // I don't think we want this one
        };

        // FreeRounds data is supplied through a url parameter, which we have to parse.
        let freeRoundsData = null;
        if (dependencies.urlParams.freerounds) {
            this.log.info('freerounds url parameter provided to the game client');
            freeRoundsData = parseFreeRoundsData(dependencies.urlParams.freerounds);
        }
        else this.log.info('no freeruonds url parameter provided to the game client');

        if (freeRoundsData) {
            this.log.info(`SGDigital FreeRounds data = ${JSON.stringify(freeRoundsData)}`);

            // Hack - because the SG Digital backend apparently doesn't return this field ??
            // TODO: Their documentation suggests that this needs to be set to "COMPLETE".
            // Now... why on earth did i end up with value of "pending" ? I think i had to
            // add this during my week off, when Alexander was having problems. This is under
            // item "FR2 Integration Game: Loading" in their checklist.
            freeRoundsData.GAMEPLAY = "complete";
            
            /**
             * @private
             * @type {SgdPromoInfo}
             */
            this._launchFreeRoundsData = { PROMOTIONS:freeRoundsData };
        }

        // TODO: We must also pass along some urls. I don't really know if integrating
        // the construction of this here, is the right place to handle it. Having a
        // separate class seems redundant - the functionality can be thought of as hooks
        // into the main AppControl cycle - and as the GameWrapper and SgDigital AppController
        // are therefore tightly coupled, they may as well exist in the same code-unit.
        /**
         * @type {SgdGcmGameWrapper}
         */
        let gameWrapper =
        {
            gcmReady : gcm => {
                this.log.info(`GameWrapper.gcmReady()`);
            },

            configReady : ogsParams => {

                this.log.info(`GameWrapper.configReady(ogsParams:${JSON.stringify(ogsParams)})`);
                
                // Register game options with GCM
                /**
                 * @type {SgdGcmOptionType[]}
                 */
                let options = ["MUTE", "TURBO", "HELP", "GAME_PREFERENCES"];
                options.forEach(option => {
                    this._options[option].enabled = true;
                    this._options[option].value = this._gcm.regOption(option);
                });
    
                this._events.emit(C_SgdGameEvent.OGS_CONFIG_READY);
               
            },

            balancesHasChanged : balanceData => {
                // This was fixed for PokerStars (11.0.1.2022). According to SGD integration, we only expect
                // "balanceHasChanged" to get called when some kind of "addCredit" operation got invoked thourhg
                // the licensee GUI. PokerStars - unusually - seem to call it on GameAnimationStart (csondering
                // that we ultimately end up feeding this back to their GUI, it looks like the PokerStars GUI
                // doesnt call this when informed of a balanceChange directly - otherwise we would be stuck in
                // an endless loop). The bug (incorrect multiplication by 100) didnt manifest on other SGD
                // integrations - presumably noone we integrated with before 2022 actually called this method ?
                let updatedBalance = (balanceData.CASH.amount + (balanceData.BONUS? balanceData.BONUS.amount : 0)) * 100;

                this.log.debug(`GameWrapper.balancesHasChanged(balanceData:${JSON.stringify(balanceData)},combinedBalanceInPoints:${updatedBalance})`);

                // This method may get called when balance is updated through their own GUI.
                // We call a special model method for this scenario, which takes care of the
                // update for us.
                this._model.setUpdatedWallet(updatedBalance);
            },

            gameRevealed : () => {
                this.log.info(`GameWrapper.gameRevealed()`);
                this._events.emit(C_SgdGameEvent.GAME_REVEALED);
            },

            resume : errorParamIndex => {
                this.log.info(`GameWrapper.resume(errorParamIndex:${errorParamIndex})`);

                this._events.emit(C_SgdGameEvent.POPUP_DISMISSED, errorParamIndex);
            },

            reload : () => {
                this.log.info(`GameWrapper.reload()`);
                window.location.reload();
            },

            redirect : targetUrl => {
                this.log.info(`GameWrapper.redirect(to:${targetUrl})`);
                window.location.replace(targetUrl);
            },

            optionHasChanged: (optionType, newValue) => {
                this.log.debug(`GameWrapper.optionHasChanged(optionType:${optionType}, newValue:${newValue})`);

                // This may need changing : our help and paytable are the same thing, and all options
                // may not be simultaneously enabled
                if (optionType === "HELP" || optionType === "PAYTABLE") {
                    this.log.debug(`GameWrapper.optionHasChanged: player wants to open Help / Paytable`);

                    // It's most appropriate to fire the "Toggle Game Menu" event, as this will trigger
                    // the Game Menu to either open or close (if that is currently possible).
                    this._events.emit(C_GameEvent.BUTTON_TOGGLE_GAME_MENU_PRESSED, true);
                }
                else
                if (optionType === "GAME_PREFERENCES") {
                    this.log.debug(`GameWrapper.optionHasChanged: player wants to change Game Preferences (eg: bet)`);

                    this._events.emit(C_GameEvent.BUTTON_TOGGLE_BET_MENU_PRESSED, true);
                }
                else
                if (optionType === "TURBO") {
                    this.log.debug(`GameWrapper.optionHasChanged: player wants to toggle AutoplayMenu`);
                    
                    // It's most appropriate to fire the "Toggle Autoplay Menu" event
                    this._events.emit(C_GameEvent.BUTTON_TOGGLE_AUTOPLAY_MENU_PRESSED, true);
                }
                else
                if (optionType === "MUTE") {
                    this.log.debug(`GameWrapper.optionHasChanged: sound has been toggled.`);

                    this._soundController.setSoundEnabled(!newValue);
                }
                else
                if (optionType === "ABOUT") {
                    // TODO: Verify what we are meant to do here
                }
            },

            getConfig: ogsParams => {
                
                // ogsParams.sessionId -> this is "webSessionId", and the Wmg Server will need this information.
                // Alexander seems to be saying that this IS webSessionId (or "wmgSessionId"), and that we will
                // not have a normal aamsSessionId - but this detail needs clarifying

                return {
                    // TODO: This is a "print Friendly" version of the Game client name.
                    // It's probably not the correct value to return for getConfig - but
                    // i do not know yet if this is the correct value to return here. It
                    // probably is not.
                    gameName : this.gameNamePrintFriendly, 
                    cuiPosition : "right",
                    gameLoadingScreen : true
                };
            },

            // TODO: This can now be documented better
            handleFreeRoundsUpdate : freeRoundsInfo => {
                this.log.debug(`GameWrapper.handleFreeRoundsUpdate: freeRoundsInfo:${JSON.stringify(freeRoundsInfo)})`);

                if (freeRoundsInfo.STATUS === "notstarted") {
                    this.log.debug('GameWrapper.handleFreeRoundsUpdate: fire "FREEROUNDS_SELECTION_MADE" event');

                    this._events.emit(C_SgdGameEvent.FREEROUNDS_SELECTION_MADE, freeRoundsInfo);
                }
                
                else
                if (freeRoundsInfo.STATUS === "inprogress") {
                    this.log.debug('GameWrapper.handleFreeRoundsUpdate: fire FREEROUNDS_DATA_UPDATED event')

                    // "inprogress" state can be present in the updated FreeRounds data, in 2 scenarios
                    // (that we are aware of - I only know this from manually playing, the sg digital
                    // documentation doesn't really explain what data we expect to receive and when)
                    // 1) When a single freeround has been completed (but there are further freerounds
                    //    to play)
                    // 2) When a FreeRounds session is being restored

                    this._events.emit(C_SgdGameEvent.FREEROUNDS_DATA_UPDATED, freeRoundsInfo);
                }
                else
                if (freeRoundsInfo.STATUS === "completed")
                {
                    this.log.debug('GameWrapper.handleFreeRoundsUpdate: freeRounds is apparently completed');

                    this._events.emit(C_SgdGameEvent.ALL_FREEROUNDS_COMPLETED);
                }
                else
                {
                    this.log.warn(`GameWrapper.handleFreeRoundsUpdate: unknown FreeRounds status ${freeRoundsInfo.STATUS}`);
                }
            }
        };

        this._events.addListener(C_GameEvent.SESSION_OPENED, this.startSessionTimers, this);
        this._events.addListener(C_GameEvent.SESSION_CLOSED, this.killSessionTimers, this);
        this._events.addListener(C_GameEvent.BUTTON_STOP_AUTOPLAY_PRESSED, this.stopAutoplay, this);
        this._events.addListener(C_GameEvent.BUTTON_TOGGLE_SOUND_PRESSED, this.onToggleSoundPressed, this);
        this._events.addListener(C_GameEvent.WALLET_CHANGED, this.updateGcmWalletValue, this);
        this._events.addListener(C_GameEvent.BET_SETTINGS_CHANGED, this.updateGcmStakeValue, this);
        this._events.addListener(C_GameEvent.WINNINGS_CHANGED, this.updateGcmWinningsValue, this);

        this.addState(AppState.PRE_LOADING, this.enterPreLoadingState, this.exitPreLoadingState);
        this.addState(AppState.LOADING, this.enterLoadingState, this.exitLoadingState);
        this.addState(AppState.FREE_ROUNDS_NOTIFICATION, this.enterFreeRoundsNotificationState, this.exitFreeRoundsNotificationState);
        this.addState(AppState.RESUME_GAME_NOTIFICATION, this.enterResumeGameNotificationState, this.exitResumeGameNotificationState);
        this.addState(AppState.OPEN_SESSION, this.enterOpenSessionState, this.exitOpenSessionState);
        this.addState(AppState.IDLE, this.enterIdleState, this.exitIdleState);
        this.addState(AppState.FREEROUNDS_IDLE, this.enterFreeRoundsIdleState, this.exitFreeRoundsIdleState);
        this.addState(AppState.AUTOPLAY_MENU, this.enterAutoplayMenuState, this.exitAutoplayMenuState);
        this.addState(AppState.BET_MENU, this.enterBetMenuState, this.exitBetMenuState);
        this.addState(AppState.GAME_MENU, this.enterGameMenuState, this.exitGameMenuState);
        this.addState(AppState.CHECK_BALANCE, this.enterCheckBalanceState, this.exitCheckBalanceState);
        this.addState(AppState.GAME_IN_PROGRESS, this.enterGameInProgressState, this.exitGameInProgressState);
        this.addState(AppState.POST_GAME_NOTIFICATIONS, this.enterPostGameNotificationsState, this.exitPostGameNotificationsState);

        // We need to be in pre-loading state BEFORE we call GCM.init. Preloading State is a
        // simple marker state - it doesn't actually do anything, but when GCM tells us that
        // "config is ready", we need to be in PRE_LOADING state - and will change into the
        // LOADING state (PRE_LOADING is basically a guard, in-case GCM trys to call config
        // Ready more than once)
        this.enterState(AppState.PRE_LOADING);

        // Initialize GCM. GCM has been loaded dynamically by our parent html page, which has
        // expose a dedicated method (under the "wmg" object) which allows us to initialize
        // it (GCM also needs a reference to our GameWrapper instance when its initialized).
        this.log.info('SgdAppController: initializing GCM');

        /**
         * @private
         * @type {SgdGcmCoreApi}
         */
        this._gcm = window.wmg.getGcmInstance();
        window.wmg.initGcmInstance(gameWrapper);       
    };

    /**
     * This method performs a bit of data synchronization: any options flags which we load
     * up from localStorage (eg: sound status), we want to inform GCM about when it is ready.
     * @private
     */
    syncLocalOptionsFlagsWithGcm()
    {
        this.log.info(`SgdAppController.syncLocalOptionsFlagsWithGcm()`);

        // 1) We may have loaded the game with sound muted. We should tell their GUI about this
        //   (TODO: are we absolutely certain that their GUI doesn't have its own preferences?
        //    if it does, presumably it will tell us via the "optionHasChanged" method)
        let initialSoundEnabledState = this._soundController.getSoundEnabled();

        this.log.info(`SgdAppController.syncLocalOptionsFlagsWithGcm: initialSound = ${initialSoundEnabledState?"enabled":"disabled"}`);

        // In GCM's case, MUTE=true, means "sound is muted", and not "sound is enabled"
        this._gcm.optionHasChanged("MUTE", "GAME", !initialSoundEnabledState);
    };


    /**
     * Sets the values of some initial fields (eg: Total Stake) with GCM.
     * @private
     */
    setInitialValuesWithGcm()
    {
        this.log.info('SgdAppController.setInitialValuesWithGcm()');

        this.updateGcmWinningsValue();
        this.updateGcmStakeValue();
        this.updateGcmWalletValue();
    };


    //--------------------------------------------------------------------------------------------------
    // Stateless methods.. 
    //--------------------------------------------------------------------------------------------------
    /**
     * Updates the value of Wallet shown within GCM.
     * @private
     */
    updateGcmWalletValue() {
        let walletInPoints = this._model.getPlayerWallet();

        /** @type {SgdBalancesMap} */
        let newBalancesMap =
        {
            CASH  : { amount : walletInPoints / 100 },
            BONUS : { amount : 0 }
        };

        this.log.debug(`SgdAppController.updateGcmWalletValue: walletInPoints:${walletInPoints}, balancesMap:${JSON.stringify(newBalancesMap)}`);

        // My understanding of this, is that their "cash" is scaled as points / 100.
        // However, there is no documentation at all to explain how they really handle
        // cash values, so the actual mechanism for mapping could be incorrect. I have
        // no idea whatsoever what BONUS balance refers to - again, their documentatoon
        // simply doesn't explain it
        this._gcm.balancesUpdate(newBalancesMap);
    };


    /**
     * Updates the value of Winnings shown within GCM, to the current value from our
     * Model.
     * @private
     */
    updateGcmWinningsValue() {
        let winningsInPoints = this._model.getPlayerWinnings();
        let winningsInGcmRange = winningsInPoints / 100;
        
        this.log.debug(`SgdAppController.updateGcmWinningsValues: winningsInPoints:${winningsInPoints}, winningsInGcmRange:${winningsInGcmRange}`);

        this._gcm.paidUpdate(winningsInGcmRange);
    };


    /**
     * Updates the value of Total Stake shown within the GCM, to the current value
     * from our Model.
     * @private
     */
    updateGcmStakeValue() {
        let totalBetInPoints = this._model.getTotalBet();
        let totalBetInGcmRange = totalBetInPoints / 100;
        
        this.log.debug(`SgdAppController.updateGcmStakeValue: stakeInPoints:${totalBetInPoints}, stakeInGcmRange:${totalBetInGcmRange}`);
        
        this._gcm.stakeUpdate(totalBetInGcmRange);
    };


    /**
     * Aborts any autoplay session that is currently in progress.
     * @private
     */
    stopAutoplay() {
        this._model.cancelAutoplay();
    };


    /**
     * Called when the "Toggle Sound" button is pressed in the regular GUI / menu (or any other
     * input action within our own gui). We listen out explicitly for the BUTTON_TOGGLE_SOUND_PRESSED
     * event (and NOT the SOUND_TOGGLED event): SOUND_TOGGLED will get triggered whenever sound status
     * is changed (including by a call from the GCM to optionHasChanged), whereas the BUTTON TOGGLE
     * PRESSED event only comes from our GUI, when a toggle sound input is invoked. This is the only
     * time (as far as i understand it, as of 11.09.2020) that we need to tell their API that we have
     * changed sound status (if i understand correctly, we do not have to tell them that sound status
     * changed when THEIR gui button action was pressed??)
     * it changed sound status.
     * @private
     * @param {boolean} newData
     */
    onToggleSoundPressed(newData) {
        let isSoundEnabled = this._soundController.getSoundEnabled();
        let isSoundMuted = !isSoundEnabled;

        this.log.debug(`SgdAppController.onToggleSoundPressed: soundEnabled:${isSoundEnabled}, set GCM.mute to ${isSoundMuted}`);

        this._gcm.optionHasChanged("MUTE", "GAME", isSoundMuted);
    };


    //------------------------------------------------------------------------------
    // Session Timer functionality
    //------------------------------------------------------------------------------
    // It is very useful to centralize the implementation of these timers here in
    // GameController. However, other modules will raise events that GameController
    // must listen to ( because these may trigger the timers to be stopped, or
    // restarted ).
    //------------------------------------------------------------------------------
    /**
     * Starts all required session timers running. If any timer is already running,
     * it will be cancelled / reset as appropriate. Only timers required for the
     * current game deployment (eg: that have been explicitly configured to be
     * required) will be started by this method call.
     * @private
     */
     startSessionTimers() {
        this.log.info(`SgdAppController.startSessionTimers()`);

        this.startSessionKeepAliveTimer();
    };


    /**
     * Stops all running session related timers.
     * @private
     */
    killSessionTimers() {
        this.log.info('SgdAppController.killSessionTimers()');

        this.killSessionKeepAliveTimer();
    };


    /**
     * Starts the Session Keep Alive timer running. If the timer is already in
     * progress, it will be cancelled before being restarted. This method will
     * only start the timer if it is explicitly configured to be used for the
     * current game deployment.
     * @private
     */
    startSessionKeepAliveTimer() {
        this.killSessionKeepAliveTimer();

        let timeMs = 60000;

        this.log.info("SgdAppController.startSessionKeepAliveTimer: next keep alive request in 1 minute");

        /**
         * Reference to the currently active Session Inactivity Timer
         * (if one is running). When not running, this will be null.
         * @private
         */
        this._sessionKeepAliveTimer = setInterval(() => {
            this.log.debug('SgdAppController: sending new KeepAlive request');

            this._services.sendKeepAliveRequest(() => {
                this.log.debug('SgdAppController: KeepAlive acknowledged by the server')
            },
            // Keep Alive error handling is tricky: its the one place where
            // we cannot meaningfully reset the current state, because it
            // doesn't represent a state transition. For now, we will warn
            // about a comms error on keep alive (the next "real" operation
            // should trigger that comms error), and we will enter fatal error
            // state in the event that its any other kind of error.
            // TODO: "non fatal" error handling still needs definining for the
            // V2 platform: the specification is far too vague about what it
            // means. Currently, the spec seems to say "the client will
            // decide" which defeats the point of the field.
            err => {
                if (err.isConnectionError) {
                    this.log.warn('SgdAppController: KeepAlive failed due to connection error')
                }
                else
                {
                    this.enterState(AppState.FATAL_ERROR);
                }
            });
        }, timeMs);
    };


    /**
     * Stops the Session Keep Alive timer ( if it is running )
     * @private
     */
    killSessionKeepAliveTimer() {
        if (this._sessionKeepAliveTimer) {
            this.log.info('SgdAppController.killSessionKeepAliveTimer()');
            clearInterval(this._sessionKeepAliveTimer);
            this._sessionKeepAliveTimer = null;
        }
        else
        {
            this.log.info('SgdAppController.killSessionKeepAliveTimer: timer not running');
        }
    };


    //--------------------------------------------------------------------------------------------------
    // Pre Loading State
    //--------------------------------------------------------------------------------------------------
    /**
     * @private
     */
    enterPreLoadingState()
    {
        this._events.on(C_SgdGameEvent.OGS_CONFIG_READY, this.onOgsConfigReady, this);
    };


    /**
     * @private
     */
    exitPreLoadingState()
    {
        this._events.off(C_SgdGameEvent.OGS_CONFIG_READY, this.onOgsConfigReady, this);
    };


    /**
     * @private
     */
    onOgsConfigReady()
    {
        this.log.debug('SgdAppController.preloading.onConfigReady()');

        this.enterState(AppState.LOADING);
    };


    //--------------------------------------------------------------------------------------------------
    // Loading State
    //--------------------------------------------------------------------------------------------------
    /**
     * @private
     */
    enterLoadingState()
    {
        // It should be appropriate to do this here: GCM should be ready for us to use ?
        this.syncLocalOptionsFlagsWithGcm();
        this.setInitialValuesWithGcm();

        this._events.once(C_GameEvent.LOADING_SCREEN_COMPLETE, () => {
            this.log.info('GameController.loadingComplete: inform GCM');
            
            // TODO: Not sure if i need to call this here ?
            this._gcm.loadProgressUpdate(100);

            // We enter the freerounds notification state regardless, it will
            // either show a notification, or launch the game. (TODO: This is
            // maybe a bit weird, but initially cleaner in terms of code flow.
            // Some renaming might help make t look less weird)
            this.enterState(AppState.FREE_ROUNDS_NOTIFICATION);
            
        });

        this._events.on(C_GameEvent.LOAD_PROGRESS, this.onLoadProgress, this);
        
        this._events.emit(C_GameEvent.SHOW_LOADING_SCREEN);
    };


    /**
     * @private
     */
    exitLoadingState()
    {
        this._events.off(C_GameEvent.LOAD_PROGRESS, this.onLoadProgress, this);

        this._events.emit(C_GameEvent.HIDE_LOADING_SCREEN);
    };


    /**
     * Called during loading state, whenever load progress gets update. Forwards the loading event
     * on to GCM: so, we should see updates on any GCM run loading screen.
     * @private
     * @param {number} progressPercentage
     * Load progress (from 0 to 100)
     */
    onLoadProgress(progressPercentage)
    {
        this._gcm.loadProgressUpdate(progressPercentage);
    };


    //--------------------------------------------------------------------------------------------------
    // FreeRounds notification state
    //--------------------------------------------------------------------------------------------------
    // This state exists for initial FreeRound notifications (before we open a session). Its not yet
    // clear to me, if we ever need to show FreeRonds at a different point than this. In fact.. we could
    // go through this state regardless of wether we have freerounds, it will work the same in both
    // scenarios
    //--------------------------------------------------------------------------------------------------
    /**
     * @private
     */
    enterFreeRoundsNotificationState()
    {
        this._events.on(C_SgdGameEvent.FREEROUNDS_SELECTION_MADE, this.onInitialFreeRoundsSelectionMade, this);
        this._events.on(C_SgdGameEvent.FREEROUNDS_DATA_UPDATED, this.onInitialFreeRoundsSelectionMade, this);
        this._events.on(C_SgdGameEvent.GAME_REVEALED, this.onGameRevealed, this);

        // TODO: Do we really need the conditional checks ?
        let sessionIsReloaded = this._model.isSessionReloaded();
        let gameLaunchPromoInfo = this._launchFreeRoundsData;

        if (gameLaunchPromoInfo) {
            if (sessionIsReloaded) {
                let phaseToRestore = this._model.getRestoredGamePhase();
                this.log.debug(`SgdAppController.enterFreeRoundsNotification: freeRounds data provided, but we must resume an open game at ${phaseToRestore}`);
            }
            else
            {
                this.log.info(`SgdAppController.enterFreeRoundsNotification: got freerounds data: ${JSON.stringify(this._launchFreeRoundsData)}`);

                this._model.setCurrencyTypeToPromoGame(); // TODO: Does this belong here ??
                this._gcm.setPromoInfo(gameLaunchPromoInfo);
            }
        }
        else
        {
            this.log.info('SgdAppController: no freerounds data provided to the gameClient');
        }

        // According to documentation, we need to call this after setting promo info.
        this._gcm.gameReady();
    };


    /**
     * @private
     */
    exitFreeRoundsNotificationState()
    {
        this._events.off(C_SgdGameEvent.FREEROUNDS_SELECTION_MADE, this.onInitialFreeRoundsSelectionMade, this);
        this._events.off(C_SgdGameEvent.FREEROUNDS_DATA_UPDATED, this.onInitialFreeRoundsSelectionMade, this);
        this._events.off(C_SgdGameEvent.GAME_REVEALED, this.onGameRevealed, this);
    };


    // TODO: Should we accept data in the event, so that we can cache it (data about the selection made)?
    // Doing it that way, might eventually allow us to refactor away the "GameWrapper" (or keep it at
    // arms length, as far as possible)
    /**
     * @private
     * @param {SgdFreeRoundCampaign} selectedCampaign
     */
    onInitialFreeRoundsSelectionMade(selectedCampaign)
    {
        this.log.info(`SgdAppController.onFreeRoundsSelectionMade()`);

        /**
         * Description of the freerounds campaign selected by the player.
         * @private
         * @type {SgdFreeRoundCampaign}
         */
        this._selectedFreeroundsCampaign = selectedCampaign;

        // OK: I can only see that this needs to be called according to the flow chart in
        // "OGS FreeRounds (FR2) - Front End Integration Guide using GCM v1.4.pdf". The
        // documentation for this method (in jsdoc) doesn't imply that this is something
        // that the game client should event be calling.
        this._gcm.resume();
    };


    /**
     * Action called during the LOADING state, when GCM indicates that the game has been revealed.
     * At this point, we must change from LOADING state, to the next appropriate operation:
     * - if there is an open game session, resume it (if there was also FreeRounds data provided,
     *   we should have ignored it, in order to allow the open game to be resumed)
     * - otherwise, start a new session
     * @private
     */
    onGameRevealed()
    {
        if (this._model.isSessionReloaded()) {
            this.log.info(`SgdAppController.onGameRevealed: resume a game in progress`);
            this._restoredGamePhase = this._model.getRestoredGamePhase();
            this.enterState(AppState.RESUME_GAME_NOTIFICATION);
        }
        else
        {
            this.log.info(`SgdAppController.onGameRevealed: start a new game session`);
            this.enterState(AppState.OPEN_SESSION);
        }
    };


    //--------------------------------------------------------------------------------------------------
    // Game Restore notification
    //--------------------------------------------------------------------------------------------------
    // Shows a notification to the player that a game is being resumed.
    //--------------------------------------------------------------------------------------------------
    /**
     * Enter the "resume game" notification state.
     * @private
     */
    enterResumeGameNotificationState()
    {
        this.log.info('SgdAppController.resumeGameNotification.enter()');

        this._dialogController.showResumeGameNotification(() => {
            this.log.debug('SgdAppController.resumeGameNotification.dismissed()');
            this.enterState(AppState.GAME_IN_PROGRESS);
        });
    };


    /**
     * Exits the "resume game" notification state.
     */
    exitResumeGameNotificationState()
    {
        this.log.info('SgdAppController.resumeGameNotification.exit()');
    };


    //--------------------------------------------------------------------------------------------------
    // Open Session state (loading screen is hidden)
    //--------------------------------------------------------------------------------------------------
    /**
     * @private
     */
    enterOpenSessionState()
    {
        this.openSession();
    };


    /**
     * @private
     */
    exitOpenSessionState()
    {
        this._events.emit(C_GameEvent.HIDE_LOADING_SPINNER);
    };


    /**
     * Opens a new game session.
     * @private
     */
    openSession()
    {
        this.log.info('SgdAppController.openSession()');

        this._events.emit(C_GameEvent.SHOW_LOADING_SPINNER);

        let optionalParams = null;
        if (this._selectedFreeroundsCampaign) {
            // For SGD, additional parameters are expected in the CheckBalanceRequest (which
            // is the first stage of the composite "sendSessionInitWithMaxBalacneRequest").
            // These extra parameters can be safely excluded when there is no FreeRounds
            // session to play.
            optionalParams =
            {
                activationId : this._selectedFreeroundsCampaign.ACTIVATIONID,
                campaignId : this._selectedFreeroundsCampaign.CAMPAIGNID,
                betLevel : this._selectedFreeroundsCampaign.OPTIONS[0].BETLEVEL * 100,
                totalRounds : this._selectedFreeroundsCampaign.OPTIONS[0].TOTALROUNDS
            }
        }

        this._services.sendSessionInitWithMaxBalanceRequest(
            this._model.getSessionType(),
            sessionInitReply =>
            {
                this._model.processSessionInitMsg(sessionInitReply);
                this.updateGcmStakeValue();
                this.updateGcmWalletValue();
                this.updateGcmWinningsValue();
                
                if (this._selectedFreeroundsCampaign)
                {
                    this.log.info('SgdAppController: session opened, freeRounds available, start freerounds');

                    // TODO: This is quite a large conditional block to have here, the logic needs teasing apart
                    // into methods which are triggered

                    // TODO: I *think* the multiplication by 100 here is correct. They return us 2.5, internally,
                    // that's the same as our own value of 250 (this is how i undertand their documentation, but
                    // until i see it in action, i don't know for certain that this is correct!!)
                    // 17.07.2020
                    let totalBetForCampaign = this._selectedFreeroundsCampaign.OPTIONS[0].BETLEVEL * 100;
                    let numRoundsInCampaign = this._selectedFreeroundsCampaign.OPTIONS[0].REMAININGROUNDS;
                    let totalBetIsValid = this._model.isTotalBetValid(totalBetForCampaign);

                    if (totalBetIsValid)
                    {
                        this.log.info(`SGD FreeRounds campaign specifies TotalBet of ${totalBetForCampaign}, which is valid`);

                        // For now, we pick the highest value bet settings
                        let allBetSettings = this._model.getAllBetSettingsMatchingTotalBet(totalBetForCampaign);
                        let numBetSettings = allBetSettings.length;
                        let selectedBetSetting = allBetSettings[numBetSettings - 1];
                        
                        this._model.setBetSettings(selectedBetSetting);
                        this._model.setCurrencyTypeToPromoGame();
                        this._model.setPromoGamesSessionNumGamesSelected(numRoundsInCampaign);

                        this.enterState(AppState.FREEROUNDS_IDLE);
                    }
                    else
                    {
                        this.log.error(`SGD FreeRounds campaign specifies TotalBet of ${totalBetForCampaign}, which is not valid for this game`);;
                        this.log.error(`SGD FreeRounds campaign: possible TotalBet values are ${this._model.getAllPossibleTotalBetValues()}`);

                        // Show an error notification, to indicate that bet values were invalid.
                        this._dialogController.showDialogs([{
                            type : "fatalError",
                            errorCode : "FREEROUNDS_INVALID_BET_LEVEL",
                            header : { localizationId:"T_dialog_InvalidFreeRoundsTotalBet_Title" },
                            message : {
                                localizationId:"T_dialog_InvalidFreeRoundsTotalBet_Content",
                                localizationMods : {
                                    "[TOTAL_BET]" : totalBetForCampaign,
                                    "[GAME_NAME]" : this.gameNamePrintFriendly
                                }
                            }
                        }]);
                    }
                }
                else
                {
                    this.log.info('SgdAppController: session opened, no freerounds, enter idle state');
                
                    this.enterState(AppState.IDLE);
                }
            },
            err =>
            {
                this.log.error('SgdAppController: open session has failed');
                this.log.info('SgdAppController.showSessionOpenedFailErrorDialog()');

                this._events.emit(C_GameEvent.HIDE_LOADING_SPINNER);
                this.showCommsErrorDialog(err, () => this.openSession());
            },
            // Custom values for launching the session
            optionalParams
        );
    };
    

    //--------------------------------------------------------------------------------------------------
    // IDLE
    //--------------------------------------------------------------------------------------------------
    /**
     * @private
     */
    enterIdleState()
    {
        this.log.debug('SgdAppController.Idle.enter()');

        this._model.setCurrencyTypeToCredit();

        this._events.on(C_GameEvent.BUTTON_SPIN_PRESSED, this.actionOnButtonSpin, this);
        this._events.on(C_GameEvent.BUTTON_TOGGLE_AUTOPLAY_MENU_PRESSED, this.actionOnButtonAutoplay, this);
        this._events.on(C_GameEvent.BUTTON_TOGGLE_BET_MENU_PRESSED, this.actionOnButtonBet, this);
        this._events.on(C_GameEvent.BUTTON_TOGGLE_GAME_MENU_PRESSED, this.actionOnButtonOpenGameMenu, this);
        this._events.on(C_GameEvent.BUTTON_CLOSE_SESSION_PRESSED, this.actionOnButtonCloseSession, this);

        this._events.emit(C_GameEvent.GAME_IDLE_STARTED);
        this._events.emit(C_GameEvent.ENABLE_GUI_BUTTONS);
        this._events.emit(C_GameEvent.ENABLE_SPIN_BUTTON);
    };


    /**
     * @private
     */
    exitIdleState()
    {
        this.log.debug('SgdAppController.Idle.exit()');

        this._events.off(C_GameEvent.BUTTON_SPIN_PRESSED, this.actionOnButtonSpin, this);
        this._events.off(C_GameEvent.BUTTON_TOGGLE_AUTOPLAY_MENU_PRESSED, this.actionOnButtonAutoplay, this);
        this._events.off(C_GameEvent.BUTTON_TOGGLE_BET_MENU_PRESSED, this.actionOnButtonBet, this);
        this._events.off(C_GameEvent.BUTTON_TOGGLE_GAME_MENU_PRESSED, this.actionOnButtonOpenGameMenu, this);
        this._events.off(C_GameEvent.BUTTON_CLOSE_SESSION_PRESSED, this.actionOnButtonCloseSession, this);

        this._events.emit(C_GameEvent.GAME_IDLE_ENDED);
        this._events.emit(C_GameEvent.DISABLE_GUI_BUTTONS);
        this._events.emit(C_GameEvent.DISABLE_SPIN_BUTTON);
    };


    /**
     * Action invoke, when the "spin" button is pressed in IDLE state.
     * @private
     */
    actionOnButtonSpin() {
        // SG Digital request that we show no error notification in the scenario where it seems that either
        // - player's bet is too high, or
        // - player cannot afford any bet
        // HOWEVER: We have problems integrating this for the backend. The solution arrived at, is
        // to discretely perform the add credit operation in the scenario where the game client thinks
        // that the player cannot afford to continue play.
        // the player cannot afford a new game (we must go to the server first for this to be confirmed).
        // So, we always attempt to play the next game.

        
        let isNewGameAffordable = this._model.isNewGameAffordable();
        let isBetTooHigh = this._model.getTotalBet() > this._model.getPlayerWallet();
        let isBalanceCheckRequired = !isNewGameAffordable || isBetTooHigh;

        if (isBalanceCheckRequired)
        {
            this.enterState(AppState.CHECK_BALANCE);
        }
        else
        {    
            this.enterState(AppState.GAME_IN_PROGRESS);
        }
    };


    /**
     * Action invoked, when the "open autoplay menu" button is pressed
     * in IDLE state.
     * @private
     * @param {boolean} triggeredByGcm
     */
    actionOnButtonAutoplay(triggeredByGcm=false) {
        this.log.debug(`SgdAppController.idle.actionOnButtonAutoplay(triggeredByGcm:${triggeredByGcm})`);

        if (!triggeredByGcm) {
            this.log.debug(`SgdAppController.Idle: inform GCM that AutoplayMenu is being opened by the game`);

            this._gcm.optionHasChanged("TURBO", "GAME", true);
        }

        this.enterState(AppState.AUTOPLAY_MENU);
    };


    /**
     * @private
     * @param {boolean} [triggeredByGcm]
     * Indicates if the call to close the game menu was triggered by the GMC "optionHasChanged" call.
     * The standard events do not dispatch any data, so this is is only going to be true, if it was
     * called within our custom Game Wrapper.
     */
    actionOnButtonOpenGameMenu(triggeredByGcm=false) {
        this.log.debug(`SgdAppController.idle.actionOnButtonOpenGameMenu(triggeredByGcm:${triggeredByGcm})`);

        if (!triggeredByGcm) {
            this.log.debug(`SgdAppController.Idle: inform GCM that GameMenu is being opened by the game`);

            this._gcm.optionHasChanged("HELP", "GAME", true);
        }

        this.enterState(AppState.GAME_MENU);
    };


    /**
     * Action invoked when the Bet button is pressed in the GUI.
     * @private
     * @param {boolean} triggeredByGcm
     */
    actionOnButtonBet(triggeredByGcm=false) {
        this.log.debug(`SgdAppController.idle.actionOnButtonBet(triggeredByGcm:${triggeredByGcm})`);

        if (!triggeredByGcm) {
            this.log.debug(`SgdAppController.Idle: inform GCM that BetMenu is being opened by the game`);

            this._gcm.optionHasChanged("GAME_PREFERENCES", "GAME", true);
        }

        this.enterState(AppState.BET_MENU);
    };


    /**
     * Action invoked, when the Close Session button is pressed in IDLE state.
     * @private
     */
    actionOnButtonCloseSession() {
        this.log.debug('[SgdAppController].actionOnButtonCloseSession()');

        let closeSessionButtonConfig = this.getCloseSessionButtonConfig();

        if (closeSessionButtonConfig)
        {
            if (closeSessionButtonConfig.action === "closeSession" ||
                closeSessionButtonConfig.action === "closeSessionThenExitClient") {
                this.log.error('[SgdAppController].closeSession action not supported for SGDigital');
            }
            else
            if (closeSessionButtonConfig.action === "exitClient") {
                this.log.debug('[SgdAppController].actionOnButtonCloseSession: attempting to close client');
                let exitClientAction = closeSessionButtonConfig.customAction || this.getDefaultExitClientAction();
                this._exitClientUtil.exitClient(exitClientAction);
            }
        }
    };


    /**
     * @private
     * @return {CloseSessionButtonConfig}
     */
    getCloseSessionButtonConfig()
    {
        if (this._platform.isMobile()) {
            return this._businessConfig.mobileGui.closeSessionButton;
        }
        else return this._businessConfig.desktopGui.closeSessionButton;
    };
 
 
    /**
     * @private
     * @return {ExitClientAction}
     */
    getDefaultExitClientAction() {
        if (this._platform.isMobile()) {
            return this._businessConfig.exitClientActionMobile;
        }
        else return this._businessConfig.exitClientActionDesktop;
    }


    //--------------------------------------------------------------------------------------------------
    // Balance Check state
    //--------------------------------------------------------------------------------------------------
    /**
     * @private
     */
    enterCheckBalanceState()
    {
        let gameplayMode = this._model.getSessionType();

        this._events.emit(C_GameEvent.SHOW_LOADING_SPINNER);

        this._services.sendCheckBalanceRequest(
            gameplayMode,
            balanceData => {
                this._events.emit(C_GameEvent.HIDE_LOADING_SPINNER);

                if (balanceData.accountBalance >= this._model.getTotalBet()) {
                    this.enterState(AppState.GAME_IN_PROGRESS);
                }
                else
                {
                    /** @type {SgdDialogController} */
                    let dialogController = this._dialogController;
                    
                    dialogController.showInsufficientCreditDialog(() => {
                        this.enterState(AppState.IDLE);
                    });
                }
            },
            err => {
                this.log.error('SgdAppController: check balance has failed');
                this.log.info('SgdAppController.showCheckBalanceFailedErrorDialog()');

                this._events.emit(C_GameEvent.HIDE_LOADING_SPINNER);
                this.showCommsErrorDialog(err, () => this.openSession());
            }
        );
    };


    /**
     * @private
     */
    exitCheckBalanceState()
    {

    };


    //--------------------------------------------------------------------------------------------------
    // Autoplay Settings Menu
    //--------------------------------------------------------------------------------------------------
    /**
     * @private
     */
    enterAutoplayMenuState() {
        this._events.on(C_GameEvent.BUTTON_CLOSE_AUTOPLAY_MENU_PRESSED, this.closeAutoplayMenu, this);
        this._events.on(C_GameEvent.BUTTON_TOGGLE_AUTOPLAY_MENU_PRESSED, this.closeAutoplayMenu, this);

        this._events.emit(C_GameEvent.SHOW_AUTOPLAY_MENU);
    };


    /**
     * @private
     */
    exitAutoplayMenuState() {
        this._events.off(C_GameEvent.BUTTON_CLOSE_AUTOPLAY_MENU_PRESSED, this.closeAutoplayMenu, this);
        this._events.off(C_GameEvent.BUTTON_TOGGLE_AUTOPLAY_MENU_PRESSED, this.closeAutoplayMenu, this);

        this._events.emit(C_GameEvent.HIDE_AUTOPLAY_MENU);
    };


    /**
     * @private
     * @param {boolean} [triggeredByGcm]
     * Indicates if the call to close the Bet Menu was triggered by the GMC "optionHasChanged" call.
     * The standard events do not dispatch any data, so this is is only going to be true, if it was
     * called within our custom Game Wrapper.
     */
    closeAutoplayMenu(triggeredByGcm=false) {
        this.log.info(`SgdAppController.AutoplayMenu.closeAutoplayMenu(triggeredByGcm:${triggeredByGcm})`);

        if (!triggeredByGcm) {
            this.log.debug(`SgdAppController.AutoplayMenu: inform GCM that AutoplayMenu is being closed by the game`);
            this._gcm.optionHasChanged("TURBO", "GAME", false);
        }

        // This behaviour is already subtly different to "WmgAppController", where we also check
        // if the player can supposedly afford at least 1 game from their new autoplay session.
        if (this._model.getAutoplayNumGamesRemaining() > 0) {
            this.enterState(AppState.GAME_IN_PROGRESS);
        }
        else {
            this.enterState(AppState.IDLE);
        }
    };


    //--------------------------------------------------------------------------------------------------
    // Bet Settings Menu
    //--------------------------------------------------------------------------------------------------
    /**
     * @private
     */
    enterBetMenuState() {
        this._events.on(C_GameEvent.BUTTON_CLOSE_BET_MENU_PRESSED, this.closeBetMenu, this);
        this._events.on(C_GameEvent.BUTTON_TOGGLE_BET_MENU_PRESSED, this.closeBetMenu, this);

        this._events.emit(C_GameEvent.SHOW_BET_MENU);
    };


    /**
     * @private
     */
    exitBetMenuState() {
        this._events.off(C_GameEvent.BUTTON_CLOSE_BET_MENU_PRESSED, this.closeBetMenu, this);
        this._events.off(C_GameEvent.BUTTON_TOGGLE_BET_MENU_PRESSED, this.closeBetMenu, this);

        this._events.emit(C_GameEvent.HIDE_BET_MENU);
    };


    /**
     * @private
     * @param {boolean} [triggeredByGcm]
     * Indicates if the call to close the Bet Menu was triggered by the GMC "optionHasChanged" call.
     * The standard events do not dispatch any data, so this is is only going to be true, if it was
     * called within our custom Game Wrapper.
     */
    closeBetMenu(triggeredByGcm=false) {
        this.log.info(`SgdAppController.BetMenu.closeBetMenu(triggeredByGcm:${triggeredByGcm})`);

        if (!triggeredByGcm) {
            this.log.debug(`SgdAppController.BetMenu: inform GCM that BetMenu is being closed by the game`);
            this._gcm.optionHasChanged("GAME_PREFERENCES", "GAME", false);
        }

        this.enterState(AppState.IDLE);
    };


    //--------------------------------------------------------------------------------------------------
    // Game menu
    //--------------------------------------------------------------------------------------------------
    /**
     * @private
     */
    enterGameMenuState()
    {
        this._events.on(C_GameEvent.BUTTON_CLOSE_GAME_MENU_PRESSED, this.closeGameMenu, this);
        this._events.on(C_GameEvent.BUTTON_TOGGLE_GAME_MENU_PRESSED, this.closeGameMenu, this);

        this._events.emit(C_GameEvent.SHOW_MENU);
    };


    /**
     * @private
     */
    exitGameMenuState() {
        this._events.off(C_GameEvent.BUTTON_CLOSE_GAME_MENU_PRESSED, this.closeGameMenu, this);
        this._events.off(C_GameEvent.BUTTON_TOGGLE_GAME_MENU_PRESSED, this.closeGameMenu, this);

        this._events.emit(C_GameEvent.HIDE_MENU_VIEW);
    };


    /**
     * @private
     * @param {boolean} [triggeredByGcm]
     * Indicates if the call to close the Game Menu was triggered by the GMC "optionHasChanged" call.
     * The standard events do not dispatch any data, so this is is only going to be true, if it was
     * called within our custom Game Wrapper.
     */
    closeGameMenu(triggeredByGcm=false)
    {
        this.log.info(`SgdAppController.GameMenu.closeGameMenu(triggeredByGcm:${triggeredByGcm})`);

        if (!triggeredByGcm) {
            this.log.debug(`SgdAppController.BetMenu: inform GCM that GameMenu is being closed by the game`);
            this._gcm.optionHasChanged("HELP", "GAME", false);
        }

        this.enterState(AppState.IDLE);
    };


    //--------------------------------------------------------------------------------------------------
    // FreeRounds IDLE
    //--------------------------------------------------------------------------------------------------
    /**
     * Enter action for the FreeRounds idle state.
     * @private
     */
    enterFreeRoundsIdleState() {
        this.log.debug('SgdAppController.FreeRoundsIdle.enter()');

        this._events.on(C_GameEvent.BUTTON_SPIN_PRESSED, this.onStartFreeRoundPressed, this);
        
        this._events.emit(C_GameEvent.ENABLE_SPIN_BUTTON);
        this._events.emit(C_GameEvent.FREEROUNDS_IDLE_STARTED);
    };


    /**
     * Exit action for the FreeRounds idle state.
     * @private
     */
    exitFreeRoundsIdleState() {
        this.log.debug('SgdAppController.FreeRoundsIdle.exit()');

        this._events.off(C_GameEvent.BUTTON_SPIN_PRESSED, this.onStartFreeRoundPressed, this);

        this._events.emit(C_GameEvent.DISABLE_SPIN_BUTTON);
        this._events.emit(C_GameEvent.FREEROUNDS_IDLE_ENDED);
    };


    /**
     * Invoked when any "Start Spin" action is pressed in the FreeRounds idle state.
     * @private
     */
    onStartFreeRoundPressed() {
        this.log.debug('SgdAppController.FreeRoundsIdle.startFreeRoundPressed()');

        this.enterState(AppState.GAME_IN_PROGRESS);
    };


    //--------------------------------------------------------------------------------------------------
    // Game In Progress
    //--------------------------------------------------------------------------------------------------
    // TODO: I think we DO want some kind of post-game notifications, and we probably
    // need them to be adapted for the case of SGDigital ? Because some notifications
    // we want, others we do not.
    /**
     * @private
     */
    enterGameInProgressState() {
        this.log.debug(`SgdAppController.gameInProgress.enter()`);

        /*
        // TEMPORARILY PRESENT for debugging the FreeRounds Complete notification issue
        let isFreeGame = this._model.getSessionNumFreeGames() > 0;
        let isFinalFreeGame = this._model.getSessionNumFreeGames() === 1;
        */

        /**
         * Callback invoked when the game has been completed.
         */
        let onGameComplete = () => {
            this.log.debug('SgdAppController.gameInProgress.onGameComplete()');

            // Callback that will be executed at some point after we call GCM.gameAnimationComplete
            // GCM's GUI may be showing some kind of notifications to the player after the game
            // animation complete call, but before invoking this resume game callback.
            let onGcmGameResumed = () =>
            {
                this.log.debug('SgdAppController.gameInProgress.gcmGameAnimationComplete.resumeGameCallback()');

                // TODO: We don't want post game notifications for FreeRounds at all, i think    
                //this.enterState(State.POST_GAME_NOTIFICATIONS);

                return this.selectStateAfterGameFinished();
            }

            this._gcm.gameAnimationComplete(onGcmGameResumed);
        };

        // TODO: Currently, the GameController shows error notifications itself. This does
        // seem more logical in a way, and the mechanism is entirely abstract - so it will
        // scale for future use cases. Therefore, we don't need this "onGameFailed" callback
        // (I think) - but that's a detail which is going to need finalizing.
        /**
         * Callback invoked when the game has failed.
         */
        let onGameFailed = () => {
            // TODO: This here error notification is certainly not correct,
            // but it's also not currently getting fired.
            this._gcm.handleError("INSUFFICIENT_FUNDS", "ERROR", "", "Not enough money");
        };

        /**
         * Callback invoked when some custom licensee data is returned from starting or
         * completing the game. In this case, that means promoInfo data, which GCM is
         * interested in.
         * 
         * "Licensee Management Server" returns us some custom data for SGD: that's how we
         * are able to invoke "setPromoInfo". SGD documentation indicates that this must
         * be set after gameAnimationStart is called: this rule is conformed to (game
         * animation start is called before GameController is told to start the new game).
         * 
         * @type {GameLmsDataReceivedCallback<SgdFreeRoundOffer>}
         */
        let onLmsCustomData = (promoData, resumeGameFlow) =>
        {
            this.log.debug(`SgdAppController.onLmsCustomData: ${JSON.stringify(promoData)}`);

            if (promoData) {
                this.log.debug('SgdAppController.onLmsCustomData: got promoInfo, passing it to GCM');
                
                // I *think* we need to add this manually. It's not returned by their backend,
                // and its unclear from their documentation whether its required or not.
                promoData.GAMEPLAY = "complete";

                /** @type {SgdPromoInfo} */
                let fullPromoInfo = { PROMOTIONS:promoData };

                this.log.debug(`SgdAppController.onLmsCustomData: set newPromoInfo to ${JSON.stringify(fullPromoInfo)}`);

                this._gcm.setPromoInfo(fullPromoInfo);
            }
            
            // NOTE: Ordinarily, we might pause control flow during a notification, but in this case,
            // we do not. This was how the SgdAppController behaved, before the "resumeGameFlow" property
            // existed, so currently, automatically calling resumeGameFlow is simply to keep old behaviour.
            // I believe that no notification is shown here. Its hard to reason about how the SGD Gui behaves,
            // as its not terribly clearly documented.
            // Russell - 04.10.2021
            // TODO : Anything done to explicitly make this clear, would be a god-send....
            resumeGameFlow();
        }

        // Inform GCM that a game animation has started: this call basically sets GCM to a "game
        // in progress" visual state, where its GUI may behave slightly differently. There is a
        // corresponding gameAnimationComplete call to be made as well.
        this._gcm.gameAnimationStart();

        if (this._restoredGamePhase)
        {
            this.log.debug(`SgdAppController.gameInProgress.enter: we need to restore a game, to ${this._restoredGamePhase}`);

            // Important to clear this field back to null, because we are only doing a
            // simple check here.
            let restoredGamePhase = this._restoredGamePhase;
            this._restoredGamePhase = null;

            this._gameController.resumeGame(
                restoredGamePhase,
                onGameComplete,
                onGameFailed,
                onLmsCustomData);
        }
        else
        {   
            // This should be a sufficient check, for now. That's only if Alexander does
            // get the server side return of this data fully working, however.
            let isFreeGame = this._model.getSessionNumFreeGames() > 0;
            let isFinalFreeGame = this._model.getSessionNumFreeGames() === 1;

            this.log.debug(`SgdAppController.gameInProgress.enter: isFreeGame:${isFreeGame}, isFinalFreeGame:${isFinalFreeGame}`);

            if (isFreeGame)
            {
                //this._events.on(C_SgdGameEvent.SINGLE_FREEROUND_COMPLETED, this.onSingleFreeRoundCompleted, this);
                this._events.on(C_SgdGameEvent.FREEROUNDS_DATA_UPDATED, this.onSingleFreeRoundCompleted, this);
                this._events.on(C_SgdGameEvent.ALL_FREEROUNDS_COMPLETED, this.onAllFreeRoundsCompleted, this);
                this._events.on(C_SgdGameEvent.FREEROUNDS_SELECTION_MADE, this.onNewFreeRoundsSelectionMade, this);
            }

            // If this is a freeGame, we must call gcm.setPromoInfo - after we have called game
            // animation start.
            
            /*
            if (isFreeGame)
            {
                let currPromoInfo = this._gcm.getPromoInfo();

                // @type {SgdPromoInfo}
                let newPromoInfo =
                {
                    PROMOTIONS :
                    {
                        // This seems like it should be correct, BUT.. their checklist explicitly tells
                        // us to use COMPLETE when setting the data initially. Something I simply cannot
                        // understand. If "pending" doesnt't mean "the FreeRounds session is pending",
                        // then... what on earth does it mean? You certainly cannot pass in a value of
                        // pending on the "freerounds in progress" update calls to setPromoInto either:
                        // GCM just seems to ignore it completely.
                        //GAMEPLAY : "complete",
                        FREEROUNDS :
                        [{
                            VERSION:1.4,
                            CAMPAIGNID : currPromoInfo.PROMOTIONS.FREEROUNDS[0].CAMPAIGNID,
                            ACTIVATIONID : currPromoInfo.PROMOTIONS.FREEROUNDS[0].ACTIVATIONID,
                            TOTALBET : currPromoInfo.PROMOTIONS.FREEROUNDS[0].TOTALBET,
                            //ENDDATE : currPromoInfo.PROMOTIONS.FREEROUNDS[0].ENDDATE,
                            //TOTALWIN : this._model.getFreeGamesWinnings(),
                            //CAMPAIGNVALUE : currPromoInfo.PROMOTIONS.FREEROUNDS[0].CAMPAIGNVALUE,
                            //REJECTABLE : currPromoInfo.PROMOTIONS.FREEROUNDS[0].REJECTABLE,
                            //STATUS : currPromoInfo.PROMOTIONS.FREEROUNDS[0].STATUS,
                            OPTIONS : [{
                                BETLEVEL : currPromoInfo.PROMOTIONS.FREEROUNDS[0].OPTIONS[0].BETLEVEL,
                                FEATURE : currPromoInfo.PROMOTIONS.FREEROUNDS[0].OPTIONS[0].FEATURE,
                                REMAININGROUNDS: currPromoInfo.PROMOTIONS.FREEROUNDS[0].OPTIONS[0].REMAININGROUNDS - 1,
                                TOTALROUNDS : currPromoInfo.PROMOTIONS.FREEROUNDS[0].OPTIONS[0].TOTALROUNDS
                            }]
                        }]
                    }
                };

                this.log.debug(`SgdAppController.gameInProgress: set (dummy) newPromoInfo to ${JSON.stringify(newPromoInfo)}`);

                this._gcm.setPromoInfo(newPromoInfo);
            }
            */

            this._gameController.startNewGame(onGameComplete, onGameFailed, onLmsCustomData);
        }
    };


    /**
     * @private
     */
    exitGameInProgressState() {
        this.log.debug(`SgdAppController.gameInProgress.exit()`);
        //this._events.off(C_SgdGameEvent.SINGLE_FREEROUND_COMPLETED, this.onSingleFreeRoundCompleted, this);
        this._events.off(C_SgdGameEvent.FREEROUNDS_DATA_UPDATED, this.onSingleFreeRoundCompleted, this);
        this._events.off(C_SgdGameEvent.ALL_FREEROUNDS_COMPLETED, this.onAllFreeRoundsCompleted, this);
        this._events.off(C_SgdGameEvent.FREEROUNDS_SELECTION_MADE, this.onNewFreeRoundsSelectionMade, this);
    };


    /**
     * @private
     */
    onSingleFreeRoundCompleted()
    {
        this.log.debug(`SgdAppController.gameInProgress.onSingleFreeRoundCompleted`);

        // I *think* that all this resume does, is cause our onGameComplete callback to be triggered.
        // In practise, there is nothing meaningfull we need to do here (considering how our games
        // are designed)

        this._gcm.resume();
    };


    /**
     * @private
     */
    onAllFreeRoundsCompleted()
    {
        this.log.debug(`SgdAppController.gameInProgress.onAllFreeRoundsCompleted`);

        // With this commented out, dismissing the "FreeRoundsComplete" notification shown by
        // GCM won't lead us back into the normal game
        this._gcm.resume();
    };


    /**
     * @private
     * @param {SgdFreeRoundCampaign} selectedCampaign
     */
    onNewFreeRoundsSelectionMade(selectedCampaign)
    {
        this.log.debug(`SgdAppController.gameInProgress.onNewFreeRoundsSelectionMade()`);
        
        // TODO: This needs handling !! From talking to John Adamu @ SGDigital, it sounds like
        // - in theory - a licensee could tack a fresh freerounds award on to an existing session.
        // Which means that we might get this notification appearing during a game in progress.

        // This might, in theory, be enough ? Or is there more logic that needs to be added here,
        // relating to bet settings and other values ?
        this._selectedFreeroundsCampaign = selectedCampaign;
        this._gcm.resume();
    };

    
    //--------------------------------------------------------------------------------------------------
    // Notifications shown after the game is completed
    //--------------------------------------------------------------------------------------------------
    // TODO: I think we will need a custom "notification controller", with its own concept of
    // post game notifications. Maybe ?
    //--------------------------------------------------------------------------------------------------
    /**
     * @private
     */
    enterPostGameNotificationsState() {
        this._events.on(C_GameEvent.POST_GAME_NOTIFICATIONS_SHOWN, this.selectStateAfterGameFinished, this);
        this._events.emit(C_GameEvent.SHOW_POST_GAME_NOTIFICATIONS);
    };


    /**
     * @private
     */
    exitPostGameNotificationsState() {
        this._events.off(C_GameEvent.POST_GAME_NOTIFICATIONS_SHOWN, this.selectStateAfterGameFinished, this);
    };


    /**
     * @private
     */
    selectStateAfterGameFinished() {
        let numFreeGamesRemaining = this._model.getSessionNumFreeGames();

        // Finally, select the correct state transition.
        this.log.info(`Sgd.selectStateAfterGameFinished: numReeRounds left = ${numFreeGamesRemaining}`);
        if (numFreeGamesRemaining > 0) {

            this.enterState(AppState.FREEROUNDS_IDLE);
        }
        else
        if (this._model.getAutoplayNumGamesRemaining() > 0 || this._model.isAutoplayInProgress()) {
            this.enterState(AppState.GAME_IN_PROGRESS);
        }
        else this.enterState(AppState.IDLE);
    };


    //--------------------------------------------------------------------------------------------------
    // TODO: Not 100% sure how the error state will play out yet. I need to better understand how
    // we will integrate the various pop-ups / error popups.
    //--------------------------------------------------------------------------------------------------
    /**
     * @private
     */
    enterErrorState() {

    };


    /**
     * @private
     */
    exitErrorState() {

    };
};

export default SgdAppController;

// TODO: Not 100% sure if "SgdFreeRoundDescription" is the correct return type - i THINK
// it is - if that's true, should i be parsing this into a more useful data object instead ?
/**
 * 
 * @param {string} rawFreeRoundsData
 * A string in Base64 - url encoded - then its a json
 * @return {SgdFreeRoundOffer}
 */
function parseFreeRoundsData(rawFreeRoundsData) {
    return JSON.parse(decodeURIComponent(atob(rawFreeRoundsData)));
}