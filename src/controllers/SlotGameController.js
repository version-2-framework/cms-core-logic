import BaseController from "./BaseController";
import C_GameEvent from "../const/C_GameEvent";
import * as ClientErrorCode from "../const/C_ClientErrorCode";
import * as GamePhase from "../const/C_SlotGamePhaseId";
import * as LogManager from "../logging/LogManager";
import C_SpinPhase from "../const/C_SpinPhase";

const log = LogManager.getLogger('SlotGameController');

// This is just sketching out what a dedicated SlotGameController would look like, when
// we strip it back to just the essentials. We can more-or-less create an instance of
// one of these for each new game that gets started (although, we don't have to). If we
// did, a few bits of functionality might need some refactoring (its not yet clear if
// "numGamesUntilForFunAdvert" goes here, or elsewhere..)

// TODO: Continue tidying this module up. Look at if we can break it up into "Base"
// functionality, and extended, game specific functionality.

const SlotGameState =
{
    IDLE : "SlotGameState_Idle",
    SUPERBET_IDLE : "SlotGameState_SuperBetIdle",
    FETCHING_SPIN_1_RESULTS : "SlotGameState_FetchingSpin1Results",
    SHOW_SPIN_1_RESULTS : "SlotGameState_ShowSpin1Results",
    IDLE_2 : "SlotGameState_Idle2", // TODO: This can probably be named better
    FETCHING_SPIN_2_RESULTS : "SlotGameState_FetchingSpin2Results",
    SHOW_SPIN_2_RESULTS : "SlotGameState_ShowSpin2Results",
    FETCH_QUICK_PHASE_RESULTS : "SlotGameState_FetchingQuickPhaseResults",
    FETCHING_FREESPIN_RESULTS : "SlotGameState_FetchingFreeSpinResults",
    FREESPIN_START_NOTIFICATION : "SlotGameState_FreeSpinStartNotification",
    SHOW_FREESPIN_RESULTS : "SlotGameState_ShowFreeSpinResults",
    SHOW_SCATTER_WIN : "SlotGameState_ShowScatterWin",
    SHOW_QUICK_PHASE_RESULT: "SlotGameState_ShowQuickPhaseResults",
    BONUS : "SlotGameState_Bonus",
    SUBBET_FINISHED : "SlotGameState_SubBetFinished",
    GAME_FINISHED : "SlotGameState_GameFinished",
    FORFUN_ADVERT : "SlotGameState_ForFunAdvert",
};

/**
 * @typedef SlotGameControllerDependencies
 * @property {EventEmitter} dispatcher
 * @property {DialogController} dialogController
 * @property {SlotConfig} config
 * @property {Model} model
 * @property {MutableSpinPhaseModel} spinModel
 * @property {MutableBonusPhaseModel} bonusModel
 * @property {SlotGameServicesApi} services
 */

/**
 * @implements {GameController}
 */
class SlotGameController extends BaseController
{
    /**
     * Contructs a new GameController instance.
     * @param {SlotGameControllerDependencies} dependencies
     * The standard dependencies object.
     */
    constructor(dependencies)
    {
        super(dependencies);
        
        this._id = "SlotGameController";
        
        /**
         * @private
         * @type {SlotConfig}
         */
        this._slotConfig = dependencies.config;

        /**
         * @private
         * @type {Model}
         */
        this._model = dependencies.model;
        
        /**
         * @private
         * @type {MutableSpinPhaseModel}
         */
        this._spinModel = dependencies.spinModel;

        /**
         * @private
         * @type {MutableBonusPhaseModel}
         */
        this._bonusModel = dependencies.bonusModel;

        /**
         * @private
         * @type {SlotGameServicesApi}
         */
        this._services = dependencies.services;

        /**
         * Tracks how many games are remaining, until the "ForFun" advert should be shown.
         * This is not enabled for all client.
         * @private
         * @type {number}
         */
        this._numGamesUntilForFunAdvert = 0;

        // TODO: Document this !!!
        /**
         * @private
         */
        this._gameInProgress = false;

        // TODO: Document this !!!
        /**
         * @private
         */
        this._gamePaused = false;

        /**
         * Callback to be invoked when the game is completed.
         * @private
         * @type {GameCompleteCallback}
         */
        this._onGameComplete = null;

        /**
         * Callback to be invoked when the game hits an error.
         * @private
         * @type {GameErrorCallback}
         */
        this._onGameError = null;

        /**
         * Callback executed when some custom LMS data is received.
         * @private
         * @type {GameLmsDataReceivedCallback}
         */
        this._onLmsCustomData = null;

        /**
         * Tracks whether the current game being played is a restored game. Once a restored game
         * is completed, this must be set back to false. Must also be false when no game is in
         * progress.
         * @private
         * @type {boolean}
         */
        this._isRestoredGame = false;

        /**
         * @private
         * @type {boolean}
         */
        this._isRestoredSpin2 = false;

        this.addState(SlotGameState.IDLE, this.enterIdleState, this.exitIdleState);

        // TODO: This state feels perhaps - out of place? But maybe "GameController" needs
        // to be extensible. Or maybe, genuinely, if we need a RouletteGameController, we
        // just have to bite the bullet, and accept code duplication. There is a loopable
        // element here though - perhaps more suited to hierarchical controller.
        this.addState(SlotGameState.SUPERBET_IDLE, this.enterSuperbetIdleState, this.exitSuperbetIdleState);

        this.addState(SlotGameState.FETCH_QUICK_PHASE_RESULTS,this.enterFetchQuickPhaseResultsState,this.exitFetchQuickPhaseResultsState);
        
        this.addState(SlotGameState.FETCHING_SPIN_1_RESULTS, this.enterFetchSpin1ResultsState, this.exitFetchSpin1ResultsState);
        this.addState(SlotGameState.SHOW_SPIN_1_RESULTS, this.enterShowSpin1ResultsState, this.exitShowSpin1ResultsState);
        this.addState(SlotGameState.IDLE_2, this.enterSpin2IdleState, this.exitSpin2IdleState);
        this.addState(SlotGameState.FETCHING_SPIN_2_RESULTS, this.enterFetchSpin2ResultsState, this.exitFetchSpin2ResultsState);
        this.addState(SlotGameState.SHOW_SPIN_2_RESULTS, this.enterShowSpin2ResultsState, this.exitShowSpin2ResultsState);
        this.addState(SlotGameState.FETCHING_FREESPIN_RESULTS, this.enterFetchFreeSpinResultsState, this.exitFetchFreeSpinResultsState);
        this.addState(SlotGameState.FREESPIN_START_NOTIFICATION, this.enterFreeSpinNotificationState, this.exitFreeSpinNotificationState);
        this.addState(SlotGameState.SHOW_FREESPIN_RESULTS, this.enterShowFreeSpinResultsState, this.exitShowFreeSpinResultsState);
        this.addState(SlotGameState.SHOW_SCATTER_WIN, this.enterScatterWinState, this.exitScatterWinState);
        this.addState(SlotGameState.BONUS, this.enterBonusState, this.exitBonusState);
        this.addState(SlotGameState.SUBBET_FINISHED, this.enterSubBetFinishedState, this.exitSubBetFinishedState);
        this.addState(SlotGameState.GAME_FINISHED, this.enterGameFinishedState, this.exitGameFinishedState);
        this.addState(SlotGameState.FORFUN_ADVERT, this.enterForFunAdvertState, this.exitForFunAdvertState);

        this.addState(SlotGameState.SHOW_QUICK_PHASE_RESULT,this.enterQuickPhaseResultsState,this.exitQuickPhaseResultsState);


        // TODO: This state might not be of any relevance now
        this.addState(SlotGameState.FATAL_ERROR, this.enterFatalErrorState);

        this.enterState(SlotGameState.IDLE);

        this._events.on(C_GameEvent.SESSION_CLOSED, this.clearCachedResultsData, this);
    };


    /**
     * Clears any cached results data, when a session is closed. This prevents us from resumig
     * the new session in an idle state with any previous win simulations showing.
     * @private
     */
    clearCachedResultsData()
    {
        this.log.debug('[SlotGameController].clearCachedResultsData()');

        this._spinModel.clearCachedData();
    };


    //------------------------------------------------------------------------------
    // Idle state
    //------------------------------------------------------------------------------
    // For Slot Game Controller, IDLE is a special state, that we enter regardless
    // of whether the main AppController is in IDLE (or has entered some other
    // "idle-like" state)
    //------------------------------------------------------------------------------
    /**
     * @private
     */
    enterIdleState() {
        this.log.info('SlotGameController.enterIdleState()');

        this._events.emit(C_GameEvent.START_SLOT_GAME_IDLE);
    };


    /**
     * @private
     */
    exitIdleState() {
        this.log.info('SlotGameController.exitIdleState()');

        this._events.emit(C_GameEvent.END_SLOT_GAME_IDLE);
    };


    /**
     * @public
     * @inheritDoc
     * @param {GameCompleteCallback} onComplete
     * @param {GameErrorCallback} onError
     * @param {GameLmsDataReceivedCallback} onLmsCustomData
     */
    startNewGame(onComplete, onError, onLmsCustomData) {
        if (this.getState() === SlotGameState.IDLE) {
            this._isRestoredGame = false;
            this._isRestoredSpin2 = false;
            this._onGameComplete = onComplete;
            this._onGameError = onError;
            this._onLmsCustomData = onLmsCustomData;

            if (this._slotConfig.isQuickSpin)
            {
                this.enterState(SlotGameState.FETCH_QUICK_PHASE_RESULTS);
            }
            else
            {
                this.enterState(SlotGameState.FETCHING_SPIN_1_RESULTS);
            }
            
        }
    };


    /**
     * @public
     * @inheritDoc
     * @param {string} restoredGamePhase
     * @param {GameCompleteCallback} onComplete
     * @param {GameErrorCallback} onError
     * @param {GameLmsDataReceivedCallback<*>} onLmsCustomData
     */
    resumeGame(restoredGamePhase, onComplete, onError, onLmsCustomData) {
        if (this.getState() === SlotGameState.IDLE) {
            this._isRestoredGame = true;
            this._isRestoredSpin2 = false;
            this._onGameComplete = onComplete;
            this._onGameError = onError;
            this._onLmsCustomData = onLmsCustomData;
            
            if (restoredGamePhase === GamePhase.SINGLE_SPIN ||
                restoredGamePhase === GamePhase.SPIN_1)
            {
                this.enterState(SlotGameState.SHOW_SPIN_1_RESULTS);
            }
            else
            if (restoredGamePhase === GamePhase.SPIN_2) {
                this._isRestoredSpin2 = true;
                this.enterState(SlotGameState.SHOW_SPIN_2_RESULTS);
            }
            else
            if (restoredGamePhase === GamePhase.BONUS) {
                this.enterState(SlotGameState.BONUS);
            }
            else
            if (restoredGamePhase === GamePhase.FREESPIN) {
                this.enterState(SlotGameState.SHOW_FREESPIN_RESULTS);
            }
            else
            if (restoredGamePhase === GamePhase.QUICK) {
                this.enterState(SlotGameState.SHOW_QUICK_PHASE_RESULT);
            }
        }
    };


    /**
     * @public
     */
    pauseGame() {
        if (this._gameInProgress && !this._gamePaused) {
            this._gamePaused = true;
            // TODO: How would we actually handle this, in practise ??
        }
    }


    unPauseGame() {
        if (this._gamePaused) {
            this._gamePaused = false;
            if (this._nextGameAction) {
                this._nextGameAction();
                this._nextGameAction = null;
            }
        }
    }


    // NEW FUNCTIONALITY FOR EVENT QUEUEING
    // TODO: This concept probably wants a better name. We could embed in it, the
    // idea that game actions are selected from each state.
    executeGameAction(action) {
        if (this._gamePaused) {
            this._nextGameAction = action;
        }
        else action();
    }





    //------------------------------------------------------------------------------
    // Superbet idle
    //------------------------------------------------------------------------------
    /**
     * @private
     */
    enterSuperbetIdleState() {
        this._model.setCurrencyTypeToSuperbet();

        this._events.emit(C_GameEvent.ENABLE_SPIN_BUTTON);

        this._events.addListener(C_GameEvent.BUTTON_SPIN_PRESSED, this.actionOnButtonSuperbetSpin, this);

        // TODO: Should a timer exist, to automatically start Superbet sub-bet ?
    };


    /**
     * @private
     */
    exitSuperbetIdleState() {
        this._events.emit(C_GameEvent.DISABLE_SPIN_BUTTON);

        this._events.removeListener(C_GameEvent.BUTTON_SPIN_PRESSED, this.actionOnButtonSuperbetSpin, this);
    };


    /**
     * @private
     */
    actionOnButtonSuperbetSpin() {
        log.debug('SuperbetSpin pressed, starting new Superbet SubBet');

        this.enterState(SlotGameState.FETCHING_SPIN_1_RESULTS);
    };
    

    //------------------------------------------------------------------------------
    // Game (spin) states
    //------------------------------------------------------------------------------
    // TODO: Now, i need to look closely at places like this, in order to understand
    // exactly how we will "pause" a game that is in progress. This case is probably
    // simple enough: we ask to Show Spin 1 Results when we get back our spin results
    // data. It suggests, that the next action should be "queued" rather than directly
    // invoked: this way, we could
    // a) get the next action suspended automatically, if game is paused
    // b) get the next action invoked automatically, when game is un-paused
    //------------------------------------------------------------------------------

   

    /**
     * @private
     */
    enterFetchSpin1ResultsState()
    {
        let is2SpinGame = this._slotConfig.isTwoSpin;
        let betSettings = this._model.getCurrentBetSettings();   

        /** @type {GameStartParams}  */
        let gameStartParams =
        {
            numPromoGames : this._model.getSessionNumFreeGames()
        }

        /** Caches the time that a game was started */
        this._gameStartTime = Date.now();

        // Records the game starting.
        // NOTE: This also handles autoplay starting / autoplay stats
        // ( we don't need to do that explicitly in GameController )
        this._model.logSubBetStarted();

        /**
         * Cache whether autoplay is currently in progress.
         * @private
         */
        this._currentGameWasAutoplayGame = this._model.isAutoplayInProgress();

        this._events.emit(C_GameEvent.FETCHING_SPIN_1_PHASE_RESULTS);

        if (is2SpinGame) {
            this._events.emit(C_GameEvent.SHOW_SPIN_INDICATOR, C_SpinPhase.SPIN_1);

            this._services.sendSpin1PhaseResultsRequest(
                betSettings,
                gameStartParams,
                (results, lmsCustomData) => {
                    this._model.processGameResultMsg(results);
                    this._spinModel.setSpin1Result(results);

                    let continueGameFlow = () => this.enterState(SlotGameState.SHOW_SPIN_1_RESULTS);

                    if (lmsCustomData && this._onLmsCustomData) {
                        this._onLmsCustomData(lmsCustomData, continueGameFlow);
                    }
                    else continueGameFlow();
                    
                },
                error => this.handleSpin1ResultsCommsError(error));
        }
        else {
            this._services.sendSingleSpinPhaseResultsRequest(
                betSettings,
                gameStartParams,
                (results, lmsCustomData) => {
                    console.log(results);
                    this._model.processGameResultMsg(results);
                    this._spinModel.setSingleSpinResult(results);

                    let continueGameFlow = () => this.enterState(SlotGameState.SHOW_SPIN_1_RESULTS);

                    if (lmsCustomData && this._onLmsCustomData) {
                        this._onLmsCustomData(lmsCustomData, continueGameFlow);
                    }
                    else continueGameFlow();
                },
                error => this.handleSpin1ResultsCommsError(error));
        }
    };


    /**
     * @private
     */
    exitFetchSpin1ResultsState() {
        // nothing at the moment..
    };


    /**
     * @private
     * @param {ServiceExecutionError} error 
     */
    handleSpin1ResultsCommsError(error) {
        log.error("Error in Spin 1 Phase Results server operation");
        
        this._events.once(C_GameEvent.INFINITE_SPIN_ANIMATION_ABORTED, () => {
            this._model.revertSubBetStarted();
        
            this.showCommsErrorDialog(error, () => {
                this.enterState(SlotGameState.FETCHING_SPIN_1_RESULTS);
            });
        });

        // Inform the world that we failed to get Spin 1 results.
        this._events.emit(C_GameEvent.FETCH_SPIN_1_PHASE_RESULTS_FAILED);
        this._events.emit(C_GameEvent.ABORT_INFINITE_SPIN_ANIMATION);
    };


    //------------------------------------------------------------------------------
    // Enter Fetch Quick Phase Results States
    //------------------------------------------------------------------------------
    
    /**
     * @private
     */
     enterFetchQuickPhaseResultsState()
     {
          let betSettings = this._model.getCurrentBetSettings();
         
         /** @type {GameStartParams}  */
         let gameStartParams =
         {
             numPromoGames : this._model.getSessionNumFreeGames()
         }
 
         /** Caches the time that a game was started */
         this._gameStartTime = Date.now();
 
         // Records the game starting.
         // NOTE: This also handles autoplay starting / autoplay stats
         // ( we don't need to do that explicitly in GameController )
         this._model.logSubBetStarted();
 
          /**
          * Cache whether autoplay is currently in progress.
          * @private
          */
          this._currentGameWasAutoplayGame = this._model.isAutoplayInProgress();
 
          this._services.sendQuickPhaseResultsRequest(betSettings,gameStartParams,(results) => {
 
             this._model.processGameResultMsg(results);
 
             this._spinModel.setQuickPhaseResult(results);
 
             this.enterState(SlotGameState.SHOW_QUICK_PHASE_RESULT);
 
            
 
          },error => this.handleQuickSpinResultsCommsError(error))
 
     }
 
    /**
     * @private 
     */
    exitFetchQuickPhaseResultsState()
    {
 
    }
 
    /**
      * @private
      * @param {ServiceExecutionError} error 
      */
    handleQuickSpinResultsCommsError(error) {
     log.error("Error in Quick Spin Phase Results server operation");
    }; 


    //------------------------------------------------------------------------------
    // Finish first spin state
    //------------------------------------------------------------------------------
    // In this state, we
    // - get the view to show the "finish first spin sequence"
    // - once the view reports that all First Spin results are shown, we send a "spin
    //   phase complete request" to the server
    // Once these 2 sequenced actions are complete, we may proceed
    // to the next state
    //------------------------------------------------------------------------------
    /**
     * Enter action for Finish Spin 1 state.
     * @private
     */
    enterShowSpin1ResultsState() {
        let is2SpinGame = this._slotConfig.isTwoSpin;
        if (is2SpinGame) {
            this._events.emit(C_GameEvent.SHOW_SPIN_INDICATOR, C_SpinPhase.SPIN_1);
        }

        this._events.on(C_GameEvent.SPIN_1_PHASE_RESULTS_SHOWN, this.onSpin1ResultsShown, this);
        this._events.emit(C_GameEvent.SHOW_SPIN_1_PHASE_RESULTS);
    };


    /**
     * Exit action for Finish Spin 1 state.
     * @private
     */
    exitShowSpin1ResultsState() {
        this._events.removeListener(C_GameEvent.SPIN_1_PHASE_RESULTS_SHOWN, this.onSpin1ResultsShown, this);
    };


    /**
     * Action invoked when Spin 1 Phase Results have been fully shown to the player.
     * @private
     */
    onSpin1ResultsShown() {
        log.info("Spin 1 results shown to player, send Spin1PhaseComplete notification to server");
        this._events.removeListener(C_GameEvent.SPIN_1_PHASE_RESULTS_SHOWN, this.onSpin1ResultsShown, this);
        this.sendSpin1PhaseCompleteNotification();
    };


    /**
     * Sends the Spin 1 Phase Complete notification to the server.
     * @private
     */
    sendSpin1PhaseCompleteNotification() {
        let onSuccess = () => this.selectStateAfterSpin1Results();

        /**
         * Handler for when the sending of a Spin 1 Phase Complete notification fails.
         * Shows default error message: if it was a comms error, we can try again, by
         * resending the appropriate Spin Phase Complete notification.
         * @param {ServiceExecutionError} error
         */
        let onFailure = error => {
            log.error("Error in Spin 1 Phase Complete server operation");

            this.showCommsErrorDialog(error, () => {
                this.sendSpin1PhaseCompleteNotification();
            });
        };

        let isTwoSpinGame = this._slotConfig.isTwoSpin;
        if (isTwoSpinGame) {
            this._services.sendSpin1PhaseCompleteRequest(onSuccess, onFailure);
        }
        else this._services.sendSingleSpinPhaseCompleteRequest(onSuccess, onFailure);
    };


    /**
     * Selects, and enters, the appropriate state, after Spin 1 Phase Results have been
     * shown (and the server has acknowledged our notification of this).
     * @private
     */
    selectStateAfterSpin1Results() {
        let spin2IsRequired = this._spinModel.getIsSpin2Required();
        let hasBonusWin = this._spinModel.hasBonusWin();
        let hasFreeSpinWin = this._spinModel.hasFreeSpinWin();

        if (spin2IsRequired) {
            if (this._model.isAutoplayInProgress()) {
                this.enterState(SlotGameState.FETCHING_SPIN_2_RESULTS);
            }
            else this.enterState(SlotGameState.IDLE_2);
        }
        else if (hasBonusWin) {
            this.enterState(SlotGameState.SHOW_SCATTER_WIN);
        }
        // We may have a FreeSpin win, but if there is also a Bonus, we must
        // play this first (and have FreeSpin phase after the Bonus phase).
        // This is because, Bonus might also award some FreeSpins.
        else if (hasFreeSpinWin) {
            this.enterState(SlotGameState.FETCHING_FREESPIN_RESULTS);
        }
        else {
            this.enterState(SlotGameState.SUBBET_FINISHED);
        }
    };


    //------------------------------------------------------------------------------
    // Spin 2 idle state
    //------------------------------------------------------------------------------
    // We only enter this state if we are in a 2 spin game. In this state
    // - we start a timer to automatically start spin 2 (its very short in autoplay)
    // - we let the player select which reels to hold from spin 1.
    //------------------------------------------------------------------------------
    /**
     * Enter action for Spin 2 Idle state.
     * @private
     */
    enterSpin2IdleState() {
        let autoStartTimeSecs = this._model.isAutoplayInProgress() ? 2 : 10;
        let autoStartTimeMs = autoStartTimeSecs * 1000;

        this._spin2AutoStartTimer = setTimeout(()=>this.startSpin2(), autoStartTimeMs);

        this._events.once(C_GameEvent.BUTTON_SPIN_PRESSED, this.startSpin2, this);
        
        this._events.emit(C_GameEvent.SHOW_SPIN_INDICATOR, C_SpinPhase.SPIN_2);
        this._events.emit(C_GameEvent.ENABLE_SPIN_BUTTON);
        this._events.emit(C_GameEvent.ENABLE_HOLDS_INPUT);
        this._events.emit(C_GameEvent.SHOW_SPIN2_HOLDS_PATTERN);
        this._events.emit(C_GameEvent.SHOW_SPIN2_IDLE_STATE);
        this._events.emit(C_GameEvent.SHOW_AUTOSPIN_TIMER, autoStartTimeSecs);
    };


    /**
     * Exit action for Spin 2 Idle state.
     * @private
     */
    exitSpin2IdleState() {
        clearTimeout(this._spin2AutoStartTimer);

        this._events.removeListener(C_GameEvent.BUTTON_SPIN_PRESSED, this.startSpin2, this);

        this._events.emit(C_GameEvent.DISABLE_SPIN_BUTTON);
        this._events.emit(C_GameEvent.DISABLE_HOLDS_INPUT);
        this._events.emit(C_GameEvent.HIDE_SPIN2_IDLE_STATE);
        this._events.emit(C_GameEvent.HIDE_AUTOSPIN_TIMER);
    };


    /**
     * @private
     */
    startSpin2()
    {
        this.enterState(SlotGameState.FETCHING_SPIN_2_RESULTS);
    };


    //------------------------------------------------------------------------------
    // Start Spin 2 state
    //------------------------------------------------------------------------------
    // 
    //------------------------------------------------------------------------------
    /**
     * Enter action for the start spin 2 state. This sets the Spin 2 starting animation
     * in progress & requests spin 2 results.
     * @private
     */
    enterFetchSpin2ResultsState() {
        this._events.emit(C_GameEvent.FETCHING_SPIN_2_PHASE_RESULTS);

        /*
        this._events.emit(C_GameEvent.SET_MESSAGE, {
            localizationId : "T_message_goodLuck"
        });
        */
        
        let holdsPatternSelected = this._spinModel.getPlayerHoldsPattern();

        this._services.sendSpin2PhaseResultsRequest(
            holdsPatternSelected,
            results => {
                this._model.processGameResultMsg(results);
                this._spinModel.setSpin2Result(results);
                this.enterState(SlotGameState.SHOW_SPIN_2_RESULTS);
            },
            error => this.handleSpin2ResultsCommsError(error));
    };


    /**
     * Exit action for the Start Spin 2 state.
     * @private
     */
    exitFetchSpin2ResultsState() {
        // Nothing to do, at the moment
    };


    // TODO: Test out this method fully. This part should be fine, but the
    // view action to revert a Spin 2 failure might need a bit more work!!
    /**
     * @private
     * @param {ServiceExecutionError} error 
     */
    handleSpin2ResultsCommsError(error) {
        log.error("Error in Spin 2 Phase Results server operation");

        this._events.once(C_GameEvent.INFINITE_SPIN_ANIMATION_ABORTED, () => {
            this.showCommsErrorDialog(error, () => {
                this.enterState(SlotGameState.FETCHING_SPIN_2_RESULTS);
            });
        });

        // Inform the world that we failed to get Spin 2 results.
        this._events.emit(C_GameEvent.FETCH_SPIN_2_PHASE_RESULTS_FAILED);
        this._events.emit(C_GameEvent.ABORT_INFINITE_SPIN_ANIMATION);
    };


    //------------------------------------------------------------------------------
    // Finish Spin 2 state
    //------------------------------------------------------------------------------
    // In this state, we
    // - get the view to show the "finish spin 2 sequence"
    // - once the view reports that all Spin 2 results are shown, we send a "spin
    //   phase 2 complete request" to the server
    // Once these 2 sequenced actions are complete, we may proceed
    // to the next state
    //------------------------------------------------------------------------------
    /**
     * Enter action for the Finish Spin 2 state.
     * @private
     */
    enterShowSpin2ResultsState() {
        this._events.on(C_GameEvent.SPIN_2_PHASE_RESULTS_SHOWN, this.onSpin2ResultsShown, this);
        
        if (this._model.isAutoplayInProgress() || this._isRestoredSpin2)
        {
            this._isRestoredSpin2 = false;
            this._events.emit(C_GameEvent.SHOW_SPIN_INDICATOR, C_SpinPhase.SPIN_2);
            this._events.emit(C_GameEvent.SHOW_SPIN2_HOLDS_PATTERN);
        }

        this._events.emit(C_GameEvent.SHOW_SPIN_2_PHASE_RESULTS);
    };


    /**
     * Exit action for the Finish Spin 2 state.
     * @private
     */
    exitShowSpin2ResultsState() {
        this._events.emit(C_GameEvent.HIDE_SPIN2_HOLDS_PATTERN);
        this._events.removeListener(C_GameEvent.SPIN_2_PHASE_RESULTS_SHOWN, this.onSpin2ResultsShown, this);
    };


    /**
     * Action invoked when the view informs us that Spin 2 Phase Results have been shown.
     * @private
     */
    onSpin2ResultsShown() {
        log.info("Spin 2 results shown to the player, send Spin2PhaseComplete notification to the server");
        this._events.removeListener(C_GameEvent.SPIN_2_PHASE_RESULTS_SHOWN, this.onSpin2ResultsShown, this);
        this.sendSpin2PhaseCompleteNotification();
    };


    /**
     * Sends the Spin 2 Phase Complete notification to the server.
     * @private
     */
    sendSpin2PhaseCompleteNotification()
    {
        let onSuccess = () => this.selectStateAfterSpin2Results();

        /**
         * Handler for when the sending of a Spin 2 Phase Complete notification fails.
         * Shows default error message: if it was a comms error, we can try again, by
         * resending the appropriate Spin 2 Phase Complete notification.
         * @param {ServiceExecutionError} error
         */
        let onFailure = error => {
            log.error("Error in Spin 2 Phase Complete server operation");

            this.showCommsErrorDialog(error, () => {
                this.sendSpin2PhaseCompleteNotification();
            });
        };

        this._services.sendSpin2PhaseCompleteRequest(onSuccess, onFailure);
    };


    /**
     * Selects, and enters, the appropriate state, after Spin 2 Phase Results have been
     * shown (and the server has ackowledged our notification of this).
     * @private
     */
    selectStateAfterSpin2Results() {
        if (this._spinModel.hasBonusWin()) {
            this.enterState(SlotGameState.SHOW_SCATTER_WIN);
        }
        else
        if (this._spinModel.hasFreeSpinWin()) {
            this.enterState(SlotGameState.FETCHING_FREESPIN_RESULTS);
        }
        else {
            this.enterState(SlotGameState.SUBBET_FINISHED);
        }
    };

    //------------------------------------------------------------------------------
    // Finish Quick Phase results state
    //------------------------------------------------------------------------------
    // In this state, we get the view to show the results to the player
    // Once this has been show
    //------------------------------------------------------------------------------

    /**
     * Dispatches a Game Event which tells the view to show the quick phase results
     * when the quick phase results shown event has been received the a quick phase complete
     * notification will be sent.
     * @private
     */
    enterQuickPhaseResultsState() {

        this._events.on(C_GameEvent.QUICK_PHASE_RESULTS_SHOWN, this.onQuickPhaseResultsShown, this);

        this._events.emit(C_GameEvent.SHOW_QUICK_PHASE_RESULTS);
    }

    /**
     * Exits the quick phase results state. Removes all event listeners.
     * @private
     */
    exitQuickPhaseResultsState(){
        this._events.removeListener(C_GameEvent.QUICK_PHASE_RESULTS_SHOWN, this.onQuickPhaseResultsShown, this);
    }

    /**
     * Invokes the send quick phase complete method 
     */
    onQuickPhaseResultsShown()
    {
        this.sendQuickPhaseCompleteNotification();
    }

     /**
     * Sends the Quick Phase Complete notification to the server.
     * @private
     */
     sendQuickPhaseCompleteNotification()
     {
        let onSuccess = () => this.selectStateAfterQuickPhaseResults();

        /**
         * Handler for when the sending of a Spin 2 Phase Complete notification fails.
         * Shows default error message: if it was a comms error, we can try again, by
         * resending the appropriate Spin 2 Phase Complete notification.
         * @param {ServiceExecutionError} error
         */
        let onFailure = error => {
            log.error("Error in Quick Phase Complete server operation");

            this.showCommsErrorDialog(error, () => {
                this.sendQuickPhaseCompleteNotification();
            });
        };

        this._services.sendQuickPhaseCompleteRequest(onSuccess, onFailure);

     }

      /**
     * Selects (and enters) the appropriate state after Quick Phase Results have been shown.
     * @private
     */
    selectStateAfterQuickPhaseResults() {

        if (this._spinModel.hasBonusWin())
        {
            this.enterState(SlotGameState.SHOW_SCATTER_WIN);
        }
        else
        {
            this.enterState(SlotGameState.SUBBET_FINISHED);
        }

    }



    //------------------------------------------------------------------------------
    // Scatter Win state
    //------------------------------------------------------------------------------
    // After spin results are shown, if there is a bonus to start, we
    // - show the scatter win animation to the player
    // - fetch bonus results from the server
    // Once these 2 async actions are complete, we enter the "transition to bonus"
    // state
    //------------------------------------------------------------------------------
    /**
     * Enter action for the Scatter Win state. Starts a sequence running, where we
     * show the Bonus Win in the reels, and requests Bonus Results (before changing
     * to Bonus state)
     * @private
     */
    enterScatterWinState() {
        /**
         * Tracks whether bonus results are received yet.
         * @private
         * @type {boolean}
         */
        this._bonusResultsReady = false;

        /**
         * Indicates whether the Scatter Win Presentation has already been shown to the player
         * (set to true once we receive the event that indicates that scatter win presentation
         * is completed - it is an asynchronous sequence)
         * @private
         * @type {boolean}
         */
        this._scatterWinShown = false;

        this._events.on(C_GameEvent.SCATTER_WIN_SEQUENCE_SHOWN, this.onScatterWinShown, this);
        this._events.emit(C_GameEvent.SHOW_SCATTER_WIN_SEQUENCE);

        this.fetchBonusPhaseResults();
    };


    /**
     * Exit action for the Scatter Win state.
     * @private
     */
    exitScatterWinState() {
        this._events.removeListener(C_GameEvent.SCATTER_WIN_SEQUENCE_SHOWN, this.onScatterWinShown);
        this._bonusResultsReady = false;
    };


    /**
     * Fetches Bonus Phase Results from the server. If this fails due to a comms error, we have
     * the oppotunity to try it again.
     * @private
     */
    fetchBonusPhaseResults() {
        this._services.sendBonusPhaseResultsRequest(
            results => {
                this._model.processGameResultMsg(results);
                this._bonusModel.setPhaseResults(results);
                this._bonusResultsReady = true;
                this.tryAndStartBonus();
            },
            error => this.handleBonusResultsCommsError(error));
    }


    /**
     * Action invoked in Scatter Win state, when the view layer indicates by event that
     * the scatter win animation has completed.
     * @private
     */
    onScatterWinShown() {
        this._scatterWinShown = true;
        this.tryAndStartBonus();
    };


    /**
     * Attempts to enter Bonus state, if all pre-conditions are met.
     * @private
     */
    tryAndStartBonus() {
        if (this._scatterWinShown && this._bonusResultsReady) {
            this.enterState(SlotGameState.BONUS);
        }
    };


    /**
     * Error handler for Bonus Phase results.
     * @private
     * @param {ServiceExecutionError} error
     */
    handleBonusResultsCommsError(error) {
        log.error("Error in BonusPhaseResults server operation");

        this.showCommsErrorDialog(error, () => {
            log.debug("Re-attempting to fetch Bonus Phase Results");
            this.fetchBonusPhaseResults();
        });
    };


    //------------------------------------------------------------------------------
    // Bonus State
    //------------------------------------------------------------------------------
    // In this state, we defer control over to the relevant BonusController.
    // This is its own state machine. Once the bonus phase is complete, it will
    // pass control back to us here in main controller with an event.
    //------------------------------------------------------------------------------
    /**
     * @private
     */
    enterBonusState() {
        let isRestoredBonus = this._bonusModel.getIsReloadedPhase();
        this._events.on(C_GameEvent.BONUS_VISUALIZATION_COMPLETE, this.onBonusFinished, this);
        this._events.emit(C_GameEvent.SHOW_BONUS_VISUALIZATION, isRestoredBonus);
    };


    /**
     * @private
     */
    exitBonusState() {
        this._events.removeListener(C_GameEvent.BONUS_VISUALIZATION_COMPLETE, this.onBonusFinished, this);
    };


    /**
     * Action invoked, after the Bonus View indicates that it has finished showing Bonus Phase Results.
     * @private
     */
    onBonusFinished() {
        log.info("Bonus Results shown to player, send notification to server");
        this._events.removeListener(C_GameEvent.BONUS_VISUALIZATION_COMPLETE, this.onBonusFinished, this);
        this.sendBonusPhaseCompleteNotification();
    };


    /**
     * Sends the Bonus Phase Complete notification to the server.
     * @private
     */
    sendBonusPhaseCompleteNotification() {
        let onSuccess = () => this.selectStateAfterBonusResults();

        /**
         * Handler for when the sending of a Bonus Phase Complete notification fails.
         * Shows default error message: if it was a comms error, we can try again, by
         * resending the Bonus Phase Complete notification.
         * @param {ServiceExecutionError} error
         */
        let onFailure = error => {
            log.error("Error in Bonus Phase Complete server operation");

            this.showCommsErrorDialog(error, () => {
                this.sendBonusPhaseCompleteNotification();
            })
        }

        this._services.sendBonusPhaseCompleteRequest(onSuccess, onFailure);
    };


    /**
     * Selects (and enters) the appropriate state after Bonus Results have been shown.
     * @private
     */
    selectStateAfterBonusResults() {
        let freeSpinRequired = this._model.getFreeSpinsNumRemaining() > 0;
        if (freeSpinRequired) {
            this.enterState(SlotGameState.FETCHING_FREESPIN_RESULTS);
        }
        else this.enterState(SlotGameState.SUBBET_FINISHED);
    }


    //------------------------------------------------------------------------------
    // FreeSpin state
    //------------------------------------------------------------------------------
    /**
     * @private
     */
    enterFetchFreeSpinResultsState() {
        this._events.emit(C_GameEvent.FETCHING_FREESPIN_PHASE_RESULTS);
        this._events.emit(C_GameEvent.FREESPINS_STARTED);

        this._services.sendFreeSpinPhaseResultsRequest(
            results => {
                this._model.processGameResultMsg(results);
                this._spinModel.setFreeSpinResult(results);
                this.enterState(SlotGameState.FREESPIN_START_NOTIFICATION);
            },
            err => this.handleFreeSpinResultsCommsError(err));
    };


    /**
     * @private
     */
    exitFetchFreeSpinResultsState() {
        // Nothing to do at the moment
    };


    /**
     * Error handler for FreeSpin Phase results.
     * @private
     * @param {ServiceExecutionError} error
     */
    handleFreeSpinResultsCommsError(error) {
        log.error("Error in FreeSpinPhaseResults server operation");

        this._events.once(C_GameEvent.INFINITE_SPIN_ANIMATION_ABORTED, () => {
            this.showCommsErrorDialog(error, () => {
                this.enterState(SlotGameState.FETCHING_FREESPIN_RESULTS);
            });
        });

        // Inform the world that we failed to get FreeSpin results.
        this._events.emit(C_GameEvent.FETCH_FREESPIN_PHASE_RESULTS_FAILED);
        this._events.emit(C_GameEvent.ABORT_INFINITE_SPIN_ANIMATION);
    };


    //------------------------------------------------------------------------------
    // FreeSpin Phase Notification
    //------------------------------------------------------------------------------
    /**
     * @private
     */
    enterFreeSpinNotificationState() {
        this._model.logFreeSpinPhaseStarted();

        this._events.once(C_GameEvent.FREESPIN_START_NOTIFICATION_CLOSED, () => {
            this.enterState(SlotGameState.SHOW_FREESPIN_RESULTS);
        });

        this._events.emit(C_GameEvent.SHOW_SPIN_INDICATOR, C_SpinPhase.FREESPIN);
        this._events.emit(C_GameEvent.SHOW_FREESPIN_START_NOTIFICATION);
    };


    /**
     * @private
     */
    exitFreeSpinNotificationState() {
        this._events.emit(C_GameEvent.HIDE_FREESPIN_START_NOTIFICATION);
    };


    //------------------------------------------------------------------------------
    // Show FreeSpin Results
    //------------------------------------------------------------------------------
    // Show the FreeSpin Phase results sequence, and wait for a notification that
    // its done. We don't handle any important game input during this state.
    //------------------------------------------------------------------------------
    /**
     * @private
     */
    enterShowFreeSpinResultsState() {
        this._events.on(C_GameEvent.FREESPIN_PHASE_RESULTS_SHOWN, this.onFreeSpinResultsShown, this);
        this._events.emit(C_GameEvent.SHOW_FREESPIN_PHASE_RESULTS);
    };


    /**
     * @private
     */
    exitShowFreeSpinResultsState() {
        this._events.emit(C_GameEvent.FREESPINS_ENDED);
        this._events.removeListener(C_GameEvent.FREESPIN_PHASE_RESULTS_SHOWN, this.onFreeSpinResultsShown, this);
    };


    /**
     * Action invoked when the view informs us that FreeSpin phase results have been shown.
     * @private
     */
    onFreeSpinResultsShown() {
        log.info("FreeSpin results have been shown, send PhaseComplete notification to the server");
        this._events.removeListener(C_GameEvent.FREESPIN_PHASE_RESULTS_SHOWN, this.onFreeSpinResultsShown, this);
        this.sendFreeSpinPhaseCompleteNotification();
    };


    /**
     * Sends the FreeSpin Phase Complete notification to the server.
     * @private
     */
    sendFreeSpinPhaseCompleteNotification() {
        let onSuccess = () => {
            this._model.logFreeSpinPhaseFinished();
            this.enterState(SlotGameState.SUBBET_FINISHED);
        };

        /**
         * Handler for when the sending of a FreeSpin Phase Complete notification fails.
         * Shows default error message: if it was a comms error, we can try again, by
         * resending the appropriate FreeSpin Phase Complete notification.
         * @param {ServiceExecutionError} error
         */
        let onFailure = error => {
            log.error("Error in FreeSpin Phase Complete server operation");

            this.showCommsErrorDialog(error, () => {
                this.sendFreeSpinPhaseCompleteNotification();
            });
        };

        this._services.sendFreeSpinPhaseCompleteRequest(onSuccess, onFailure);
    };


    //------------------------------------------------------------------------------
    // Subbet finished state
    //------------------------------------------------------------------------------
    // When all phases of a subbet have finished, GameController must enter the
    // "subbet finished" state. Here, we determine what ongoing action is required
    // - do we need to play off superbet (ie: there will be superbet subbets)
    // - do we need to offer the player to play subbets from winnings, or gamble
    // - should we simply end the current game (bet)
    //------------------------------------------------------------------------------
    /**
     * @private
     */
    enterSubBetFinishedState()
    {
        let autoplayInProgress = this._model.isAutoplayInProgress();

        this._events.emit(C_GameEvent.HIDE_SPIN_INDICATOR);

        // TODO: This may not be the correct check - because the player may have some capacity for changing
        // Superbet bet settings. This is not yet clearly defined.
        if (this._model.getPlayerSuperbet() >= this._model.getTotalBetMin()) {
            log.info(`SubBet complete: still have superbet ${this._model.getPlayerSuperbet()} to play off`);

            if (autoplayInProgress) {
                this._model.setCurrencyTypeToSuperbet();
                this.enterState(SlotGameState.FETCHING_SPIN_1_RESULTS);
            }
            else
            {
                this.enterState(SlotGameState.SUPERBET_IDLE);
            }
        }
        else
        if (false && // TODO: need to check if winnings replay is supported for the current game...
            this._model.getPlayerWinnings() >= this._model.getTotalBetMin()) {
            log.info(`SubBet complete: possible to play winnings subbets`);
        }
        else
        {
            // TODO: add in the proper logic which checks superbet, and winnings
            // play availability. For now, its ok to just move on to "game finished"
            this.enterState(SlotGameState.GAME_FINISHED);
        }
    }


    /**
     * @private
     */
    exitSubBetFinishedState()
    {

    }


    //------------------------------------------------------------------------------
    // Game finished state
    //------------------------------------------------------------------------------
    // When all subbet of a game have finished, GameController must enter the
    // "Game Finished" state. Here, we can check the time that the game lasted for
    // (and linger in this state, if we need to meet any minimum game time rules).
    // We also centralize all checks about what state to enter next.
    //------------------------------------------------------------------------------
    // TODO: This functionality here  (checking minimum game times) seems fine for
    // GameController - but its ROOT game controller functionality (not tied to
    // SlotGameController specifically) - this should be taken into account in the
    // next round of tiidying up.
    /**
     * @private
     */
    enterGameFinishedState()
    {
        this.sendGameCompleteRequest();
    }

    /**
     * Sends the GameComplete request to the server
     * - if the request fails, with recoverable error, calls "handleGameCompleteCommsError"
     *   (from which we can re-enter this method)
     * - if game complete request succeeds, then checks if we need to enforce min game time rules:
     *   if yes, a timeout may be started, before we re-enter idle state (if no, we can just reenter
     *   idle state immediately)
     * @private
     */
    sendGameCompleteRequest()
    {
        // We cannot advance out of "GameFinished" to a new game
        // until we have completed certain rules:
        // 1) Server acknowledged that game is complete
        // 2) Any min game time rule is elapsed.
        
        let onServerAcknowledgeGameComplete = () => {
            // Invoked when we know we have satisified any min-game time requirements
            let returnGameToIdleState = () => {
                this.enterState(SlotGameState.IDLE);
                
                if (this._onGameComplete) {
                    let onGameComplete = this._onGameComplete;
                    this._onGameComplete = null;
                    return onGameComplete();
                }
            }
            
            // Determine if any minimum game time rules are configured.
            // If they are, we may need to insert a pause manually, before
            // allowing the game client to advance to the next game.
            let gameStartTime = this._gameStartTime;
            let gameFinishTime = Date.now();
            let gameTotalTime = gameFinishTime - gameStartTime;

            log.info(`SlotGameController: game lasted for ${gameTotalTime/1000} seconds`);

            if (this._model.isMinimumGameTimerRequired()) {
                let minGameTimeMins = this._model.getMinimumGameTimeMins();
                let minGameTimeMs = minGameTimeMins * 60000;
                let timeBeforeNewGameAllowed = minGameTimeMs - gameTotalTime;

                if (timeBeforeNewGameAllowed > 0)
                {
                    log.info(`SlotGameController: game has only lasted ${gameTotalTime}ms, starting additional countdown of ${timeBeforeNewGameAllowed}ms`);

                    this._gameIdleTimer = setTimeout(() => {
                        returnGameToIdleState();
                    }, timeBeforeNewGameAllowed);
                }
                else returnGameToIdleState();
            }
            else returnGameToIdleState()
        }
        
        // Inform the server that our game is complete.
        this._services.sendGameCompleteRequest(
            (gameCompleteMsg, lmsCustomData) => {
                let continueGameFlow = () =>
                {
                    log.debug('SlotGameController.gameFinishedState: continue game flow');

                    // Tidies up game stats.
                    // NOTE:
                    // This call also handles Autoplay stats ( and deciding if
                    // Autoplay mode should still be engaged ): this does not
                    // need to be explicitly handled in GameController.
                    this._model.processGameCompleteMsg(gameCompleteMsg);
                    this._spinModel.clearHoldsPattern();
                    onServerAcknowledgeGameComplete();
                }

                if (lmsCustomData && this._onLmsCustomData) {
                    this._onLmsCustomData(lmsCustomData, continueGameFlow);
                }
                else continueGameFlow();
            },
            err => this.handleGameCompleteCommsError(err));
    };


    /**
     * Handles a comms error (possibly recoverable error) for the Game Complete server operation
     * @private
     * @param {ServiceExecutionError} error
     */
    handleGameCompleteCommsError(error) {
        log.error("Error in GameComplete request");

        this.showCommsErrorDialog(error, () => this.sendGameCompleteRequest());
    }


    /**
     * @private
     */
    exitGameFinishedState() {
        clearTimeout(this._gameIdleTimer);
    };

    
    //------------------------------------------------------------------------------
    // Gamble state
    //------------------------------------------------------------------------------
    // With AppControl and GameControl broken apart, Gamble - may be tricky!!!
    // It's a special state, which feeds into this concept of the game being idle,
    // or not. However, i guess that really, the game is not in progress here, and
    // normal GUI shouldn't be enabled (we may need to show some special GUI buttons)
    //------------------------------------------------------------------------------
    // TODO: This is initially here to start testing out this portion of logic.
    /**
     * @private
     */
    enterGambleState() {
        this._services.sendGamblePhaseResultsRequest(results => {
            // TODO: implement the gamble state !!
            log.error("no gamble functionality implemented yet!");
        }, this.handleGambleResultsCommsError);
    };


    /**
     * @private
     * @param {ServiceExecutionError} error 
     */
    handleGambleResultsCommsError(error) {
        log.error("Error in Gamble Phase Results server operation");
    };


    //------------------------------------------------------------------------------
    // For Fun advert state
    //------------------------------------------------------------------------------
    // This shows an advert to the player. Once dismissed, we call
    // "selectStateAfterGameFinished", essentially resuming what we were trying to
    // do for the game finishing.
    //------------------------------------------------------------------------------
    enterForFunAdvertState() {

    };


    exitForFunAdvertState() {

    };


    //------------------------------------------------------------------------------
    // Default Fatal Error state
    //------------------------------------------------------------------------------
    // Default Fatal Error state to use when there is an error invoking a service of
    // some kind. We are free to create custom error handlers for different key
    // points in the game, but this state is always available as a fallback.
    //------------------------------------------------------------------------------
    // TODO: It doesnt look like we ever trigger this ?
    /**
     * @private
     */
    enterFatalErrorState()
    {
        let errorCode = ClientErrorCode.GENERIC_ERROR;
     
        // TODO: If this is kept, then the cache commsError object needs documenting
        if (this._commsError && this._commsError.code) {
            errorCode = this._commsError.code;
            log.error(JSON.stringify(this._commsError));
        }

    };
}

export default SlotGameController;