import BaseController from "./BaseController";
import C_GameEvent from "../const/C_GameEvent";
import * as ClientErrorCode from "../const/C_ClientErrorCode";

import * as LogManager from "../logging/LogManager";
import C_SessionType from "../const/C_SessionType";
import { ExitClientUtil } from "../utils/ExitClientUtil";

const log = LogManager.getLogger('GameController');

const AppState =
{
    LOADING_SCREEN : "WmgAppState_LoadingScreen",
    START_SESSION : "WmgAppState_StartSession",
    RESUME_GAME_NOTIFICATION : "WmgAppState_ResumeGameNotification",
    IDLE : "WmgAppState_Idle",
    FREEGAMES_NOTIFICATION : "WmgAppState_FreeRoundsNotification",
    GAME_IN_PROGRESS : "WmgAppState_GameInProgress",
    POST_GAME_NOTIFICATIONS : "WmgAppState_PostGameNotifications",
    BET_TOO_HIGH_DIALOG : "WmgAppState_BetTooHighDialog",
    OUT_OF_CREDIT_DIALOG : "WmgAppState_OutOfCreditDialog",
    GAME_MENU : "WmgAppState_Menu",
    AUTOPLAY_MENU : "WmgAppState_AutoplayMenu",
    BET_MENU : "WmgAppState_BetMenu",
    ADD_CREDIT : "WmgAppState_AddCredit",
    MAX_CREDIT_ADDED_DIALOG : "WmgAppState_MaxCreditAddedDialog",
    SESSION_STATS : "WmgAppState_SessionStats",
    SESSION_INACTIVE : "WmgAppState_SessionInactive",
    FORFUN_ADVERT : "WmgAppState_ForFunAdvert",
    FATAL_ERROR : "WmgAppState_FatalError"
};

/**
 * @typedef WmgAppControllerDependencies
 * @property {ServicesApi} services
 * @property {Model} model
 * @property {GameController} gameController
 * @property {DialogController} dialogController
 * @property {EventEmitter} dispatcher
 * @property {BusinessConfig} businessConfig
 * @property {PlatformApi} platform
 */

/**
 * Wmg specific implementation of App Controller.
 */
class WmgAppController extends BaseController
{
    /**
     * Contructs a new GameController instance.
     * @param {WmgAppControllerDependencies} dependencies
     * The standard dependencies object.
     */
    constructor(dependencies)
    {
        super(dependencies);
        
        this._id = "WmgAppController";

        /**
         * @private
         * @type {Model}
         */
        this._model = dependencies.model;

        /**
         * @private
         * @type {BusinessConfig}
         */
        this._businessConfig = dependencies.businessConfig;
        
        /**
         * @private
         * @type {ServicesApi}
         */
        this._services = dependencies.services;

        /**
         * @private
         * @type {GameController}
         */
        this._gameController = dependencies.gameController;

        /**
         * @private
         * @type {DialogController}
         */
        this._dialogController = dependencies.dialogController;

        /**
         * @private
         * @type {PlatformApi}
         */
        this._platform = dependencies.platform;

        /**
         * @private
         * @type {ExitClientUtil}
         */
        this._exitClientUtil = dependencies.exitClientUtil;

        /**
         * Indicates the string id of any game state that must be restored to. If this flag is non-null,
         * it tells us that a game must be restored, and it tells us the id of the phase (required by the
         * Game Controller instance in use). Clear this flag back to null, to indicate that restoration
         * has been done.
         * @private
         * @tyhpe {string}
         */
        this._restoredGamePhase = null;


        /**
         * Tracks how many games are remaining, until the "ForFun" advert should be shown.
         * This is not enabled for all client.
         * @private
         * @type {number}
         */
        this._numGamesUntilForFunAdvert = 0;

        // inactivity timer should star in all this states 
        this._statesWithSessionTimer = [AppState.ADD_CREDIT, AppState.BET_MENU, AppState.AUTOPLAY_MENU, AppState.IDLE, 
            AppState.GAME_MENU, AppState.BET_TOO_HIGH_DIALOG];


        this._events.addListener(C_GameEvent.SESSION_OPENED, this.startSessionTimers, this);
        this._events.addListener(C_GameEvent.SESSION_CLOSED, this.killSessionTimers, this);
        this._events.addListener(C_GameEvent.BUTTON_STOP_AUTOPLAY_PRESSED, this.stopAutoplay, this);

        this.addState(AppState.LOADING_SCREEN, this.enterLoadingState, this.exitLoadingState);
        this.addState(AppState.START_SESSION, this.enterStartSessionState, this.exitStartSessionState);
        this.addState(AppState.RESUME_GAME_NOTIFICATION, this.enterResumeGameNotificationState, this.exitResumeGameNotificationState);
        this.addState(AppState.IDLE, this.enterIdleState, this.exitIdleState);
        this.addState(AppState.FREEGAMES_NOTIFICATION, this.enterFreeGamesNotification, this.exitFreeGamesNotification);
        this.addState(AppState.BET_TOO_HIGH_DIALOG, this.enterBetTooHighDialogState, this.exitBetTooHighDialogState);
        this.addState(AppState.OUT_OF_CREDIT_DIALOG, this.enterOutOfCreditDialogState, this.exitOutOfCreditDialogState);
        this.addState(AppState.GAME_MENU, this.enterGameMenuState, this.exitGameMenuState);
        this.addState(AppState.AUTOPLAY_MENU, this.enterAutoplayMenuState, this.exitAutoplayMenuState);
        this.addState(AppState.BET_MENU, this.enterBetMenuState, this.exitBetMenuState);
        this.addState(AppState.ADD_CREDIT, this.enterAddCreditState, this.exitAddCreditState);
        this.addState(AppState.MAX_CREDIT_ADDED_DIALOG, this.enterMaxCreditAddedState, this.exitMaxCreditAddedState);
        this.addState(AppState.SESSION_STATS, this.enterSessionStatsState, this.exitSessionStatsState);
        this.addState(AppState.SESSION_INACTIVE, this.enterSessionInactiveState, this.exitSessionInactiveState);
        this.addState(AppState.FORFUN_ADVERT, this.enterForFunAdvertState, this.exitForFunAdvertState);
        this.addState(AppState.FATAL_ERROR, this.enterFatalErrorState);
        this.addState(AppState.GAME_IN_PROGRESS, this.enterGameInProgressState, this.exitGameInProgressState);
        this.addState(AppState.POST_GAME_NOTIFICATIONS, this.enterPostGameNotificationsState, this.exitPostGameNotificationsState);
        
        // There is some logic to suggest that "ForFunAdvert" is actually a WmgAppState, and not a SlotGameState
        // (which is where it has currently been wedged)

        this.enterState(AppState.LOADING_SCREEN);
    };

    /**
     * Method used to intercept all state changes and manage session inactivity timer.
     * It starts only for states included in _statesWithSessionTimer array. If game is in progress timer is killed
     * @param {string} appState
     */
    enterState(appState)
    {
        super.enterState(appState);

        if(appState === AppState.GAME_IN_PROGRESS)
            this.killSessionInactivityTimer();
        else if(this._statesWithSessionTimer.includes(appState))
            this.startSessionInactivityTimer();
    }



    //------------------------------------------------------------------------------
    // Assorted utility functions, and permanent event listeners
    //------------------------------------------------------------------------------
    /**
     * Aborts any autoplay session that is currently in progress.
     * @private
     */
    stopAutoplay() {
        this._model.cancelAutoplay();
    };


    //------------------------------------------------------------------------------
    // Session Timer functionality
    //------------------------------------------------------------------------------
    // It is very useful to centralize the implementation of these timers here in
    // GameController. However, other modules will raise events that GameController
    // must listen to ( because these may trigger the timers to be stopped, or
    // restarted ).
    //------------------------------------------------------------------------------
    /**
     * Starts all required session timers running. If any timer is already running,
     * it will be cancelled / reset as appropriate. Only timers required for the
     * current game deployment (eg: that have been explicitly configured to be
     * required) will be started by this method call.
     * @private
     */
    startSessionTimers() {
        this.log.info(`WmgAppController.startSessionTimers()`);

        this.startSessionKeepAliveTimer();
    };


    /**
     * Stops all running session related timers.
     * @private
     */
    killSessionTimers() {
        this.log.info('WmgAppController.killSessionTimers()');

        this.killSessionInactivityTimer();
        this.killSessionKeepAliveTimer();

        this._events.off(C_GameEvent.COMMS_REQUEST_SENT, this.onCommsRequestSent, this);
    };


    /**
     * Handler invoked whenever a comms request is sent.
     * @private
     * @param {CommsRequestInfo} commsRequestInfo
     */
    onCommsRequestSent(commsRequestInfo) {
        this.log.debug(`WmgAppController.onCommsRequestSent:${JSON.stringify(commsRequestInfo)}`);

        if (commsRequestInfo.isSessionRequest) {
            this.log.debug('WmgAppController.onCommsRequestSent: is session request, reset session inactivity timer');
            this.resetSessionInactivityTimer();
        }
        else
        {
            this.log.debug('WmgAppController.onCommsRequestSent: not a session request, no need to reset session inactivity timer');
        }
    };


    /**
     * Resets, and restarts, the Session Inactivity timer.
     * @private
     */
    resetSessionInactivityTimer() {
        this.log.debug(`WmgAppController.resetSessionInactivityTimer()`);
        this.startSessionInactivityTimer();
    };


    /**
     * Starts the Session Inactivity Timer running. If the timer is already in
     * progress, it will be cancelled before being restarted. This method will
     * only start the timer if it is explicitly configured to be used for the
     * current game deployment.
     * @private
     */
    startSessionInactivityTimer() {
        this.killSessionInactivityTimer();
        
        let timeMins = this._model.getSessionInactivityTimeout();
        if (timeMins > 0) {
            let timeMs = timeMins * 60000;

            log.info(`WmgAppController.startSessionInactivityTimer: started, with timeout of ${timeMins} minutes`);

            /**
             * Reference to the currently active Session Inactivity Timer
             * (if one is running). When not running, this will be null.
             * @private
             */
            this._sessionInactivityTimer = setTimeout(() => {
                log.info('WmgAppController: session inactivity timer has completed.');
                this.enterState(AppState.SESSION_INACTIVE);
            }, timeMs);
        }
        else
        {
            log.warn('WmgAppController.startSessionInactivityTimer: timeout indicating by server is 0 minutes. Not starting timer');
        }
    };


    /**
     * Stops the Session Inactivity Timer ( if it is active )
     * @private
     */
    killSessionInactivityTimer() {
        if (this._sessionInactivityTimer) {
            log.info('WmgAppController.killSessionInactivityTimer()');
            clearTimeout(this._sessionInactivityTimer);
            this._sessionInactivityTimer = null;
        }
        else
        {
            log.info('WmgAppController.killSessionInactivityTimer: timer not running');
        }
    };


    /**
     * Starts the Session Keep Alive timer running. If the timer is already in
     * progress, it will be cancelled before being restarted. This method will
     * only start the timer if it is explicitly configured to be used for the
     * current game deployment.
     * @private
     */
    startSessionKeepAliveTimer() {
        this.killSessionKeepAliveTimer();

        let timeMs = 60000;

        log.info("WmgAppController.startSessionKeepAliveTimer: next keep alive request in 1 minute");

        /**
         * Reference to the currently active Session Inactivity Timer
         * (if one is running). When not running, this will be null.
         * @private
         */
        this._sessionKeepAliveTimer = setInterval(() => {
            log.debug('WmgAppController: sending new KeepAlive request');

            this._services.sendKeepAliveRequest(() => {
                log.debug('WmgAppController: KeepAlive acknowledged by the server')
            },
            // Keep Alive error handling is tricky: its the one place where
            // we cannot meaningfully reset the current state, because it
            // doesn't represent a state transition. For now, we will warn
            // about a comms error on keep alive (the next "real" operation
            // should trigger that comms error), and we will enter fatal error
            // state in the event that its any other kind of error.
            // TODO: "non fatal" error handling still needs definining for the
            // V2 platform: the specification is far too vague about what it
            // means. Currently, the spec seems to say "the client will
            // decide" which defeats the point of the field.
            err => {
                if (err.isConnectionError) {
                    log.warn('WmgAppController: KeepAlive failed due to connection error')
                }
                else
                {
                    this.enterState(AppState.FATAL_ERROR);
                }
            });
        }, timeMs);
    };


    /**
     * Stops the Session Keep Alive timer ( if it is running )
     * @private
     */
    killSessionKeepAliveTimer() {
        if (this._sessionKeepAliveTimer) {
            log.info('WmgAppController.killSessionKeepAliveTimer()');
            clearInterval(this._sessionKeepAliveTimer);
            this._sessionKeepAliveTimer = null;
        }
        else
        {
            log.info('WmgAppController.killSessionKeepAliveTimer: timer not running');
        }
    };


    //------------------------------------------------------------------------------
    // Loading (splash screen) state.
    //------------------------------------------------------------------------------
    // Shows a view to the player while assets are loading. Also offers the player
    // a choice of Session Types (unless no choices are enabled for the client
    // deployment). A Session may be restored or started during the Loading screen.
    //------------------------------------------------------------------------------
    /**
     * Enter action for the Loading (Splash Screen) state.
     * @private
     */
    enterLoadingState() {
        this._events.once(C_GameEvent.LOADING_SCREEN_COMPLETE, this.onLoadingComplete, this);
        
        this._events.emit(C_GameEvent.SHOW_LOADING_SCREEN);
    };


    /**
     * Exit action for the Loading (Splash Screen) state.
     * @private
     */
    exitLoadingState() {
        this._events.removeListener(C_GameEvent.LOADING_SCREEN_COMPLETE, this.onLoadingComplete, this);

        this._events.emit(C_GameEvent.HIDE_LOADING_SCREEN);
    };


    /**
     * @private
     */
    onLoadingComplete()
    {
        let nextState;

        // After loading, we may already know that the game session is restored.
        if (this._model.isSessionReloaded()) {
            this._restoredGamePhase = this._model.getRestoredGamePhase();

            // We can show a simple notification before resuming the game, or
            // we can just skip straight to showing the resume game. This can
            // be configured in BusinssConfig, as required by each licensee.
            let resumeSessionConfig = this._businessConfig.resumeSession;
            if (resumeSessionConfig && resumeSessionConfig.showNotification) {
                nextState = AppState.RESUME_GAME_NOTIFICATION;
            }
            else nextState = AppState.GAME_IN_PROGRESS;
        }
        // A session might already be open, when loading is completed.
        else
        if (this._model.isSessionInProgress()) {
            nextState = AppState.IDLE;
        }
        else
        {
            nextState = AppState.START_SESSION;
        }

        this.enterState(nextState);
    };


    //------------------------------------------------------------------------------
    // Start session
    //------------------------------------------------------------------------------
    // The "LoadingScreen" may have already started a session - or not. If it didn't
    // then we must open a Session within main GameController. This can be done
    // using the CasherView (we delegate the responsibility there), or (in future)
    // from an automated action here,
    //------------------------------------------------------------------------------
    /**
     * Enter action for the Start Session state.
     * @private
     */
    enterStartSessionState() {
        
        let startSessionConfig = this._businessConfig.startSession;
        let useCashier = startSessionConfig && startSessionConfig.useCashier? true : false;
        
        if (useCashier)
        {
            log.debug("WmgAppController.enterStartSession: use the cashier");

            this._events.addListener(C_GameEvent.CASHIER_COMPLETE, this.onSessionOpened, this);
            this._events.emit(C_GameEvent.SHOW_CASHIER);
        }
        else
        {
            log.debug("WmgAppController.enterStartSession: start the session behind the scenes with max credit");
            
            let sessionMode = this._model.getSessionType();

            let openSession = () => {
                this._events.emit(C_GameEvent.SHOW_LOADING_SPINNER);
                this._services.sendSessionInitWithMaxBalanceRequest(
                    sessionMode,
                    sessionInitReply => {
                        this._model.processSessionInitMsg(sessionInitReply);
                        this.onSessionOpened();
                    },
                    err => {
                        this.log.error("WmgAppController: opening session (with max balance) has failed");
                        this._events.emit(C_GameEvent.HIDE_LOADING_SPINNER);
                        this.showCommsErrorDialog(err, () => { return openSession(); } );
                    }
                );
            }

            openSession();
        }
    };


    /**
     * Exit action for the Start Session state.
     * @private
     */
    exitStartSessionState() {
        this._events.removeListener(C_GameEvent.CASHIER_COMPLETE, this.onSessionOpened, this);
        this._events.emit(C_GameEvent.HIDE_CASHIER_VIEW);
        this._events.emit(C_GameEvent.HIDE_LOADING_SPINNER);
    };


    /**
     * Invoked when the Start Session Cashier is closed.
     * @private
     */
    onSessionOpened() {
        if (this._model.getSessionNumFreeGames() > 0 || this._model.getPromoGamesAward()) {
            this.enterState(AppState.FREEGAMES_NOTIFICATION);
        }
        else this.enterState(AppState.IDLE);
    };


    //------------------------------------------------------------------------------
    // Resume Game notification state
    //------------------------------------------------------------------------------
    /**
     * @private
     */
    enterResumeGameNotificationState()
    {
        this.log.info('WmgAppController.ResumeGameNotification.enter()');

        this._dialogController.showResumeGameNotification(() => {
            this.enterState(AppState.GAME_IN_PROGRESS);
        });
    };


    /**
     * @private
     */
    exitResumeGameNotificationState()
    {
        this.log.info('WmgAppController.ResumeGameNotification.exit()');
    };


    //------------------------------------------------------------------------------
    // Session Inactive state
    //------------------------------------------------------------------------------
    /**
     * @private
     */
    enterSessionInactiveState() {
        this.log.info(`WmgAppController.enterSessionInactiveState()`);

        this.killSessionTimers();

        this._events.emit(C_GameEvent.SESSION_INACTIVITY_TIMEOUT);

        let sessionTimeoutMins = this._model.getSessionInactivityTimeout();
        let sessionIsFreeplay = this._model.getSessionType() === C_SessionType.FREE_PLAY;

        let sessionId = null;
        if(this._model.getSessionInfo() != null) {
            sessionId = this._model.getSessionInfo().sessionId;
        }

        
        this._dialogController.showDialogs([{
            type : "undismissableNotification",
            header : { localizationId: "T_dialog_SessionClosed_Title" },
            message : {
                localizationId : sessionId && !sessionIsFreeplay?
                    "T_dialog_SessionClosed_TimeoutWithSessionId_Content" : "T_dialog_SessionClosed_Timeout_Content",
                localizationMods : {
                    "[SESSION_TIMEOUT_MINS]" : sessionTimeoutMins,
                    "[SESSION_ID]": sessionId
                }
            }
        }]);

        // If we think the session is still open, then try and close it.
        if (this._model.isSessionInProgress()) {
            this.log.info('WmgAppController.enterSessionInactiveState: session still open, try and close it with the server');

            this._services.sendSessionEndRequest(
                reply => {
                    this.log.debug('WmgAppController.sessionInactive: session has been closed with the server');
                    this._model.processSessionClosedMsg(reply);
                },
                err => {
                    this.log.error(`WmgAppController.sessionInactiveState: failed to close session with server (${JSON.stringify(err)})`);
                });
        }
    }


    /**
     * @private
     */
    exitSessionInactiveState() {
        // Nothing to do here at the moment
    };


    //------------------------------------------------------------------------------
    // Game Idle state
    //------------------------------------------------------------------------------
    // This is a big (and important) game state.
    // We can transition to various different sub-menus from this state.
    //------------------------------------------------------------------------------
    /**
     * @private
     */
    enterIdleState()
    {
        // TODO: Going to have to commit to SessionWallet v PlayerWallet v Wallet type api..
        //log.info(`Wallet:${this._model.getSessionWallet()},minTotalBet:${this._model.getTotalBetMin()}`);
        log.info(`Wallet:${this._model.getPlayerWallet()},minTotalBet:${this._model.getTotalBetMin()}`);

        this._model.setCurrencyTypeToCredit();
        
        this._events.on(C_GameEvent.BUTTON_SPIN_PRESSED, this.actionOnButtonSpin, this);
        this._events.on(C_GameEvent.BUTTON_TOGGLE_AUTOPLAY_MENU_PRESSED, this.actionOnButtonAutoplay, this);
        this._events.on(C_GameEvent.BUTTON_TOGGLE_BET_MENU_PRESSED, this.actionOnButtonBet, this);
        this._events.on(C_GameEvent.BUTTON_ADD_CREDIT_PRESSED, this.actionOnButtonAddCredit, this);
        this._events.on(C_GameEvent.BUTTON_TOGGLE_GAME_MENU_PRESSED, this.actionOnButtonMenu, this);
        this._events.on(C_GameEvent.BUTTON_CLOSE_SESSION_PRESSED, this.actionOnButtonCloseSession, this);

        this._events.emit(C_GameEvent.GAME_IDLE_STARTED);
        this._events.emit(C_GameEvent.ENABLE_GUI_BUTTONS);
        this._events.emit(C_GameEvent.ENABLE_SPIN_BUTTON);
    }


    /**
     * @private
     */
    exitIdleState()
    {
        this._events.removeListener(C_GameEvent.BUTTON_SPIN_PRESSED, this.actionOnButtonSpin, this);
        this._events.removeListener(C_GameEvent.BUTTON_TOGGLE_AUTOPLAY_MENU_PRESSED, this.actionOnButtonAutoplay, this);
        this._events.removeListener(C_GameEvent.BUTTON_TOGGLE_BET_MENU_PRESSED, this.actionOnButtonBet, this);
        this._events.removeListener(C_GameEvent.BUTTON_ADD_CREDIT_PRESSED, this.actionOnButtonAddCredit, this);
        this._events.removeListener(C_GameEvent.BUTTON_TOGGLE_GAME_MENU_PRESSED, this.actionOnButtonMenu, this);
        this._events.removeListener(C_GameEvent.BUTTON_CLOSE_SESSION_PRESSED, this.actionOnButtonCloseSession, this);

        // kill inactivity timer on exit of idle state
        this.killSessionInactivityTimer();

        this._events.emit(C_GameEvent.GAME_IDLE_ENDED);
        this._events.emit(C_GameEvent.DISABLE_GUI_BUTTONS);
        this._events.emit(C_GameEvent.DISABLE_SPIN_BUTTON);
    }


    /**
     * Action invoke, when the "spin" button is pressed in IDLE state.
     * @private
     */
    actionOnButtonSpin() {
        log.debug('[WmgAppController].actionOnButtonSpin()');

        let isNewGameAffordable = this._model.isNewGameAffordable();
        if (isNewGameAffordable) {
            // TODO: Sort this out !!!!!
            //if (this._model.getTotalBet() > this._model.getSessionWallet()) {
            if (this._model.getTotalBet() > this._model.getPlayerWallet()) {
                this.enterState(AppState.BET_TOO_HIGH_DIALOG);
            }
            else {
                this.enterState(AppState.GAME_IN_PROGRESS);
            }
        }
        else {
            this.enterState(AppState.OUT_OF_CREDIT_DIALOG);
        }
    }


    /**
     * Action invoked, when the "open autoplay menu" button is pressed
     * in IDLE state.
     * @private
     */
    actionOnButtonAutoplay() {
        log.debug('[WmgAppController].actionOnButtonAutoplay()');
        this.enterState(AppState.AUTOPLAY_MENU);
    };


    /**
     * Action invoked when the Bet button is pressed in the GUI.
     * @private
     */
    actionOnButtonBet() {
        log.debug('[WmgAppController].actionOnButtonBet()');
        this.enterState(AppState.BET_MENU);
    };


    /**
     * Action invoked when the Add Credit button is pressed. We can either
     * start the action for adding credit, or not allow it, according to
     * what business rules may be configured for the target licensee.
     * @private
     */
    actionOnButtonAddCredit() {
        let addCreditEnabled = this._businessConfig.addCredit && this._businessConfig.addCredit.isEnabled;

        // Check for rules about whether player may add
        // credit. We can show a notification indicating
        // if they may not add credit.
        if (addCreditEnabled) {
            log.debug('[WmgAppController].actionOnButtonAddCredit()');

            if (this._model.canAddMoreCreditToSession()) {
                this.enterState(AppState.ADD_CREDIT);
            }
            else {
                this.enterState(AppState.MAX_CREDIT_ADDED_DIALOG);
            }
        }
        else
        {
            this.log.warn('[WmgAppController].actionOnButtonAddCredit: addCredit not enabled. Check your BusinessConfig');
        }
    };


    /**
     * Action invoked, when the Open Menu button is pressed in IDLE state.
     * @private
     */
    actionOnButtonMenu() {
        log.debug('[WmgAppController].actionOnButtonMenu()');

        this.enterState(AppState.GAME_MENU);
    };


    /**
     * Action invoked, when the Close Session button is pressed in IDLE state.
     * @private
     */
    actionOnButtonCloseSession() {
        log.debug('[WmgAppController].actionOnButtonCloseSession()');

        let closeSessionButtonConfig = this.getCloseSessionButtonConfig();

        if (closeSessionButtonConfig)
        {
            if (closeSessionButtonConfig.action === "closeSession") {
                log.debug('[WmgAppController].actionOnButtonCloseSession: entering session stats');
                this.enterState(AppState.SESSION_STATS);
            }
            else
            if (closeSessionButtonConfig.action === "exitClient") {
                log.debug('[WmgAppController].actionOnButtonCloseSession: attempting to close client');
                let exitClientAction = closeSessionButtonConfig.customAction || this.getDefaultExitClientAction();
                this._exitClientUtil.exitClient(exitClientAction);
            }
            else
            if (closeSessionButtonConfig.action === "closeSessionThenExitClient") {
                log.debug('[WmgAppController].actionOnButtonCloseSession: closeSession, then exitClient');

                let exitClientAction = closeSessionButtonConfig.customAction || this.getDefaultExitClientAction();

                // we don't need a new state.. we can just exit this one !
                this.exitIdleState();
                this._services.sendSessionEndRequest(() => {
                    log.debug('[WmgAppController].actionOnButtonCloseSession: session closed, attempting to close client');

                    this._exitClientUtil.exitClient(exitClientAction);
                },
                err => {
                    this.log.error("WmgAppController: closing session has failed");
                    this.showCommsErrorDialog(err)
                });
            }
        }
    };


    /**
     * @private
     * @return {CloseSessionButtonConfig}
     */
    getCloseSessionButtonConfig()
    {
        if (this._platform.isMobile()) {
            return this._businessConfig.mobileGui.closeSessionButton;
        }
        else return this._businessConfig.desktopGui.closeSessionButton;
    };


    /**
     * @private
     * @return {ExitClientAction}
     */
    getDefaultExitClientAction() {
        if (this._platform.isMobile()) {
            return this._businessConfig.exitClientActionMobile;
        }
        else return this._businessConfig.exitClientActionDesktop;
    }


    //------------------------------------------------------------------------------
    // Free Games (Promo Games) Notification
    //------------------------------------------------------------------------------
    /**
     * @private
     */
    enterFreeGamesNotification()
    {
        let numPromoGames = this._model.getSessionNumFreeGames();
        let promoGamesAward = this._model.getPromoGamesAward();

        if (numPromoGames > 0 || (promoGamesAward && promoGamesAward.type === "fixedBet"))
        {
            // Show notification to player of a certain number of promo games

            if (promoGamesAward && promoGamesAward.type === "fixedBet") {
                numPromoGames = promoGamesAward.numPromoGames;
                this._model.setPromoGamesSessionNumGamesSelected(numPromoGames);
                this._model.clearPromoGamesAward();
                this.setTotalBetToValue(promoGamesAward.betLevel);
            }

            this.log.debug(`FreeGamesNotification: fixed number of promoGames (${numPromoGames}) available`);
            
            this._dialogController.showDialogs([{
                type : "multiChoice",
                header : {
                    localizationId : "T_dialog_FreeGamesAward_Title"
                },
                message : {
                    localizationId : "T_dialog_FreeGamesAward_Content",
                    localizationMods : {
                        "[NUM_FREE_GAMES]" : numPromoGames
                    }
                },
                options : [{
                    localizationId : "T_button_begin"
                }]
            }],
            () => {
                this._model.setCurrencyTypeToPromoGame();
                this.enterState(AppState.GAME_IN_PROGRESS);
            });
        }
        else
        if (promoGamesAward.type === "playerChoosesBet")
        {
            // Player must choose the details of their promoGames session

            this.log.debug(`FreeGamesNotification: player must choose promoGames option`);

            /** @type {DialogTextButtonConfig[]} */
            let options = [];
            
            // Add an option for each possible FreeRound combo
            promoGamesAward.options.forEach(option => {
                if (this._model.isTotalBetValid(option.betLevel)) {
                    options.push({
                        localizationId : "T_button_freeGamesOption",
                        localizationMods : {
                            "[NUM_FREE_GAMES]" : option.numPromoGames,
                            "[TOTAL_BET]" : option.betLevel
                        },
                        action : () => {
                            this._model.setPromoGamesSessionNumGamesSelected(option.numPromoGames);
                            this.setTotalBetToValue(option.betLevel);
                        }
                    });
                }
            });

            if (options.length > 0) {
                this._dialogController.showDialogs([{
                    type : "multiChoice",
                    header : { localizationId : "T_dialog_FreeGamesSelection_Title" },
                    message : { localizationId : "T_dialog_FreeGamesSelection_Content" },
                    options : options
                }],
                () => {
                    this._model.clearPromoGamesAward();
                    this._model.setCurrencyTypeToPromoGame();
                    this.enterState(AppState.GAME_IN_PROGRESS);
                });
            }
            else
            {
                this.log.error(`couldn't find any valid bet combinations for PromoGames selection!`);

                // TODO: Handle some kind of error dialog here.
            }
        }
    };


    /**
     * @private
     */
    exitFreeGamesNotification()
    {
        // Nothing to do, yet !
    };


    // TODO: Consider "bet is invalid" edge cases
    /**
     * Sets total bet to a given value: individual bet settings will be changed to the highest
     * priority combination which creates the requested total bet value.
     * @private
     * @param {number} totalBetValue 
     */
    setTotalBetToValue(totalBetValue)
    {
        let allPossibleBetCombos = this._model.getAllBetSettingsMatchingTotalBet(totalBetValue);
        let numBetCombos = allPossibleBetCombos.length;
        let selectedCombo = allPossibleBetCombos[numBetCombos - 1];
        this._model.setBetSettings(selectedCombo);
    };


    //------------------------------------------------------------------------------
    // Bet Too High dialog
    //------------------------------------------------------------------------------
    /**
     * @private
     */
    enterBetTooHighDialogState() {
        this._dialogController.showDialogs([{
            type : "multiChoice",
            header : { localizationId: "T_dialog_BetTooHigh_Title" },
            message : { localizationId : "T_dialog_BetTooHigh_Content" },
            options : [{
                localizationId : "T_button_decreaseTotalBet",
                action : () => {
                    this._model.setTotalBetToMin()
                }
            }]
        }],
        () => {
            log.debug('all notifications shown...');
            this.enterState(AppState.IDLE);
        });
    }


    /**
     * @private
     */
    exitBetTooHighDialogState() {
        this._events.emit(C_GameEvent.HIDE_DIALOG_VIEW);
    }


    // NOTE: We are not meant to use this state for SG Digital
    //------------------------------------------------------------------------------
    // Out of credit dialog. This notifies the player that they do not have enough
    // credit to start a new game. If AddCredit operation is enabled for the client,
    // and they have not breached any session rules about amount of credit added,
    // then the dialog will also offer them a chance to add more credit to their
    // game session (ie: to enter the cashdesk, or whatever is appropriate)
    //------------------------------------------------------------------------------
    /**
     * @private
     */
    enterOutOfCreditDialogState() {
        // TODO: Consider delegating this request to DialogController

        let addCreditConfig = this._businessConfig.addCredit;
        let canAddMoreCreditToSession = this._model.canAddMoreCreditToSession();
        let addCreditEnabled = addCreditConfig && addCreditConfig.isEnabled;
        let canAddCredit = addCreditEnabled && canAddMoreCreditToSession;

        if (canAddCredit)
        {
            let playerWantsToAddCredit = false;

            this._dialogController.showDialogs([{
                type : "multiChoice",
                header : { localizationId:"T_dialog_AddCredit_Title" },
                message : { localizationId:"T_dialog_AddCredit_Content" },
                options : [{
                    localizationId : "T_button_addCredit",
                    action : () => {
                        playerWantsToAddCredit = true;
                    }
                },
                {
                    localizationId : "T_button_dismiss"
                }]
            }],
            () => {
                if (playerWantsToAddCredit) {
                    this.enterState(AppState.ADD_CREDIT);
                }
                else this.enterState(AppState.IDLE);
            });
        }
        else
        {
            this._dialogController.showDialogs([{
                type : "dismissableNotification",
                header : { localizationId:"T_dialog_OutOfCredit_Title" },
                message : { localizationId:"T_dialog_OutOfCredit_Content" }
            }], () => {
                this.enterState(AppState.IDLE);
            });
        }
    };


    /**
     * @private
     */
    exitOutOfCreditDialogState() {
        //this._events.emit(C_GameEvent.HIDE_DIALOG_VIEW);
    };
    

    //------------------------------------------------------------------------------
    // Menu state
    //------------------------------------------------------------------------------
    // 
    //------------------------------------------------------------------------------
    /**
     * @private
     */
    enterGameMenuState()
    {
        this._events.on(C_GameEvent.BUTTON_CLOSE_GAME_MENU_PRESSED, this.closeGameMenu, this);
        this._events.on(C_GameEvent.BUTTON_TOGGLE_GAME_MENU_PRESSED, this.closeGameMenu, this);
        this._events.emit(C_GameEvent.SHOW_MENU);
    };


    /**
     * @private
     */
    exitGameMenuState() {
        this._events.off(C_GameEvent.BUTTON_CLOSE_GAME_MENU_PRESSED, this.closeGameMenu, this);
        this._events.off(C_GameEvent.BUTTON_TOGGLE_GAME_MENU_PRESSED, this.closeGameMenu, this);
        this._events.emit(C_GameEvent.HIDE_MENU_VIEW);
    };


    /**
     * @private
     */
    closeGameMenu()
    {
        this.enterState(AppState.IDLE);
    };


    //------------------------------------------------------------------------------
    // Add Credit state
    //------------------------------------------------------------------------------
    // Shows the Cashier menu to the player (set to "add credit" mode).
    // In the future, we could support a single action here (increase available
    // credit)
    //------------------------------------------------------------------------------
    /**
     * @private
     */
    enterAddCreditState() {
        // TODO: Add support for the CashierView - but does that go here ?

        this._events.on(C_GameEvent.CASHIER_COMPLETE, this.onAddCreditCashierComplete, this);
        this._events.emit(C_GameEvent.SHOW_CASHIER, { addCredit:true });

        // In future, we can also support adding the maximum allowed
        // value here (or some equivalent functionality). We could
        // expose a single service that would do this automatically
        // (it sends both balance / add credit actions in sequence).
        // If the action fails, the service can pass back parameters
        // which would indicate this.
        // eg:
        // this._events.emit(C_GameEvent.SET_MESSAGE, {
        //      localizationId : "message_addingCredit"
        // });
        // this._services.addMaxCreditToSession(succeeded, reply => {
        //      this.enterState(C_GameState.IDLE);
        //      this._events.emit(C_GameEvent.SET_MESSAGE. {
        //          localizationId : "message_creditAdded",
        //          localizationMods : {
        //              "[WALLET]" : this._model.getSessionWallet()
        //          }
        //      })
        // })
    };


    /**
     * @private
     */
    exitAddCreditState() {
        this._events.removeListener(C_GameEvent.CASHIER_COMPLETE, this.onAddCreditCashierComplete, this);
        this._events.emit(C_GameEvent.HIDE_CASHIER_VIEW);
    };


    /**
     * Action invoked when the Add Credit Cashier view has completed its job. This can
     * happen when either it has added credit, failed to add credit (not allowed), or
     * the player requested to close the view,
     * @private
     */
    onAddCreditCashierComplete() {
        this.enterState(AppState.IDLE);
    };


    //------------------------------------------------------------------------------
    // Max Credit Added Dialog state
    //------------------------------------------------------------------------------
    // A dialog is shown to the player, indicating that they cannot add any more
    // credit (eg: due to country specific laws on how much can be added). Generally,
    // we should not need this, but if a View implementation doesn't disable the
    // Add Credit button (or something similar), we have this information state to
    // fall back on.
    //------------------------------------------------------------------------------
    /**
     * @private
     */
    enterMaxCreditAddedState() {
        this._dialogController.showDialogs([{
            type : "dismissableNotification",
            header : { localizationId:"T_dialog_MaxCreditAddedToSession_Title" },
            message : { localizationId:"T_dialog_MaxCreditAddedToSession_Content" }
        }],
        () => {
            this.enterState(AppState.IDLE);
        });
    };


    /**
     * @private
     */
    exitMaxCreditAddedState() {
        //this._events.emit(C_GameEvent.HIDE_DIALOG_VIEW);
    };


    //------------------------------------------------------------------------------
    // Autoplay Menu State
    //------------------------------------------------------------------------------
    // Now, the player will open an autoplay menu. Once they close it, we start
    // autoplay automatically (if they have selected a number of games). Otherwise,
    // we can revert to idle state.
    //------------------------------------------------------------------------------
    /**
     * @private
     */
    enterAutoplayMenuState() {
        this._events.on(C_GameEvent.BUTTON_CLOSE_AUTOPLAY_MENU_PRESSED, this.closeAutoplayMenu, this);
        this._events.on(C_GameEvent.BUTTON_TOGGLE_AUTOPLAY_MENU_PRESSED, this.closeAutoplayMenu, this);

        this._events.emit(C_GameEvent.SHOW_AUTOPLAY_MENU);
    };


    /**
     * @private
     */
    exitAutoplayMenuState() {
        this._events.off(C_GameEvent.BUTTON_CLOSE_AUTOPLAY_MENU_PRESSED, this.closeAutoplayMenu, this);
        this._events.off(C_GameEvent.BUTTON_TOGGLE_AUTOPLAY_MENU_PRESSED, this.closeAutoplayMenu, this);

        this._events.emit(C_GameEvent.HIDE_AUTOPLAY_MENU);
    };


    /**
     * @private
     */
    closeAutoplayMenu() {
        if (this._model.getAutoplayNumGamesRemaining() > 0) {
            if (this._model.getPlayerWallet() >= this._model.getTotalBet()) {
                this.enterState(AppState.GAME_IN_PROGRESS);
            }
            else
            {
                this._model.cancelAutoplay();
                this.log.info(`onAutoplayMenuClosed: cannot start autoplay as bet is too high. Aborting`);
                
                if (this._model.isNewGameAffordable()) {
                    this.enterState(AppState.BET_TOO_HIGH_DIALOG);
                }
                else
                {
                    this.enterState(AppState.OUT_OF_CREDIT_DIALOG);
                }
            }
        }
        else {
            this.enterState(AppState.IDLE);
        }
    };


    //------------------------------------------------------------------------------
    // Bet Settings menu
    //------------------------------------------------------------------------------
    /**
     * @private
     */
    enterBetMenuState() {
        // "Close Bet Menu Pressed" comes from the bet menu itself.
        // "Button Bet Pressed" means that any "toggle bet menu" action
        // has been invoked within our GUI
        this._events.on(C_GameEvent.BUTTON_CLOSE_BET_MENU_PRESSED, this.closeBetMenu, this);
        this._events.on(C_GameEvent.BUTTON_TOGGLE_BET_MENU_PRESSED, this.closeBetMenu, this);

        this._events.emit(C_GameEvent.SHOW_BET_MENU);
    };


    /**
     * @private
     */
    exitBetMenuState() {
        this._events.off(C_GameEvent.BUTTON_CLOSE_BET_MENU_PRESSED, this.closeBetMenu, this);
        this._events.off(C_GameEvent.BUTTON_TOGGLE_BET_MENU_PRESSED, this.closeBetMenu, this);

        this._events.emit(C_GameEvent.HIDE_BET_MENU);
    };


    /**
     * @private
     */
    closeBetMenu() {
        this.enterState(AppState.IDLE);
    };


    //------------------------------------------------------------------------------
    // Game in progress
    //------------------------------------------------------------------------------
    /**
     * @private
     */
    enterGameInProgressState() {
        this._events.emit(C_GameEvent.DISABLE_GUI_BUTTONS);
        this._events.emit(C_GameEvent.DISABLE_SPIN_BUTTON);

        // TODO: We could check here, if we need to resume the game or not, as we skip
        // to this state, when we we have a game to restore.
        if (this._restoredGamePhase)
        {
            // Important to clear this field back to null, because we are only doing a
            // simple check here.
            let restoredGamePhase = this._restoredGamePhase;
            this._restoredGamePhase = null;

            this._gameController.resumeGame(
                restoredGamePhase,
                () => this.enterState(AppState.POST_GAME_NOTIFICATIONS),
                () => this.log.error('Game failed'),
                (data,resumeGameFlow) => this.handleLmsCustomData(data, resumeGameFlow));
        }
        else
        {
            this._gameController.startNewGame(
                () => this.enterState(AppState.POST_GAME_NOTIFICATIONS),
                () => this.log.error('Game failed'),
                (data,resumeGameFlow) => this.handleLmsCustomData(data, resumeGameFlow));
        }
    };


    /**
     * @private
     */
    exitGameInProgressState() {

    };


    // TODO: We could REALLY do with improving this method name..
    // TODO: Should we interrupt spins ? This is unclear.. ubnfotunately,
    // i have abstractions that are not gelling (although they should).
    // GameController knows how to send game messages - as it should!
    // But the game messages can include this extra data, which actually
    // needs to be handled at this level. But this level doesn't know how
    // to interrupt the flow, for these custom notifications.. it could get
    // ugly, to be honest, and i am not sure of a good way out yet.
    /**
     * Handles a batch of LMS Custom Data from the server : this is generally a set of notifications
     * to be shown to the player.
     * @private
     * @param {{notifications:ServerToClientNotification[]}} data 
     * @param {()=>void} resumeGameFlow
     */
    handleLmsCustomData(data, resumeGameFlow)
    {
        this.log.info(`WmgAppController.handleLmsCustomData(${JSON.stringify(data)})`);

        // So, this works locally, but for some reason.. the logs from the Sisal testing guys, show nothing
        // beyond the first log statement ("handLmsCustomData")

        if (data && data.notifications && data.notifications.length > 0) {
            this.log.debug(`WmgAppController: got ${data.notifications.length} notifications to show from server`);
            
            this._dialogController.showServerNotifications(data.notifications, () => resumeGameFlow());
        }
        else
        {
            if (!data) {
                this.log.debug(`WmgAppController: lms data object appears to be null`);
            }
            else
            if (!data.notifications) {
                this.log.debug(`WmgAppController: no notifications field present in lmsExtraInfo`);
            }
            else
            if (data.notifications.length === 0) {
                this.log.debug(`WmgAppController: got lmsExtraInfo.notifications, but it was empty`);
            }

            resumeGameFlow();
        }
    };


    //------------------------------------------------------------------------------
    // Post game notifications
    // This could now be a state in its own right: what is happening is very simple
    //------------------------------------------------------------------------------
    /**
     * @private
     */
    enterPostGameNotificationsState() {
        // change state before notify the user with post game notification (e.g autoplay finished)
        // otherwise game is not in idle and session inactivity timer isn't started as it should
        this.enterState(AppState.IDLE);
        // after showing post game notifications, recheck state (e.g restart autoplay)
        this._dialogController.showPostGameNotifications(() => this.selectStateAfterGameFinished());

    };


    /**
     * @private
     */
    exitPostGameNotificationsState() {
    };
    

    //------------------------------------------------------------------------------
    // 
    //------------------------------------------------------------------------------
    /**
     * Selects the next idle state, after a game is finished. This check can come from
     * "game finished", or it can come from the ForFun advert state ( both of which can
     * happen after a game is finished )
     * @private
     */
    selectStateAfterGameFinished() {
        let nextStateId = AppState.IDLE;

        let autoplayInProgress = this._model.isAutoplayInProgress();
        let numAutoplaysRemaining = this._model.getAutoplayNumGamesRemaining();
        let numFreeGamesLeft = this._model.getSessionNumFreeGames();
        
        log.debug(`selectStateAfterGameFinished(autoplayInProgress:${autoplayInProgress},numAutoplayGamesLeft:${numAutoplaysRemaining},numFreeGames:${numFreeGamesLeft})`);
        

        const desktopFreeGameNotificationConfig = this._businessConfig.desktopGui.freeGamesNotificationButton === undefined ? null : this._businessConfig.desktopGui.freeGamesNotificationButton;
        
        const mobileFreeGameNotificationConfig = this._businessConfig.mobileGui.freeGamesNotificationButton === undefined ? null : this._businessConfig.mobileGui.freeGamesNotificationButton;


        const launchSessionStatsScreenEnabled = this._platform.isDesktop() ? (desktopFreeGameNotificationConfig && desktopFreeGameNotificationConfig.launchSessionStatsScreen.isEnabled) : (mobileFreeGameNotificationConfig && mobileFreeGameNotificationConfig.launchSessionStatsScreen.isEnabled);

       
        if (numFreeGamesLeft > 0 || autoplayInProgress || numAutoplaysRemaining > 0) {
            if (numFreeGamesLeft > 0) {
                this._model.setCurrencyTypeToPromoGame();
            }
            nextStateId = AppState.GAME_IN_PROGRESS;
        }

        if (numFreeGamesLeft === 0 && this._model.isNewGameAffordable() === false) {
            nextStateId = AppState.OUT_OF_CREDIT_DIALOG;
        }

        if (numFreeGamesLeft === 0 && this._model.getFreeGamesSessionData() && launchSessionStatsScreenEnabled)
        {
            nextStateId = AppState.SESSION_STATS;
        }

        // TODO: when the game supports winnings replay, this is potentially where we
        // offer the player a chance to select gamble, respin or payout

        this.enterState(nextStateId);
    }


    //------------------------------------------------------------------------------
    // For Fun advert state
    //------------------------------------------------------------------------------
    // This shows an advert to the player. Once dismissed, we call
    // "selectStateAfterGameFinished", essentially resuming what we were trying to
    // do for the game finishing.
    //------------------------------------------------------------------------------
    enterForFunAdvertState() {

    };


    exitForFunAdvertState() {

    };


    //------------------------------------------------------------------------------
    // Session Stats State
    //------------------------------------------------------------------------------
    // 
    //------------------------------------------------------------------------------
    /**
     * @private
     */
    enterSessionStatsState() {
        this._events.emit(C_GameEvent.SHOW_SESSION_STATS);
        this._events.addListener(C_GameEvent.BUTTON_RESTART_SESSION_PRESSED, this.restartSession, this);
    };


    /**
     * @private
     */
    exitSessionStatsState() {
        this._events.removeListener(C_GameEvent.BUTTON_RESTART_SESSION_PRESSED, this.restartSession, this);
    };


    /**
     * When the Session Stats view ends, with a request to start a new game session, we
     * head back to the Start Session state.
     * @private
     */
    restartSession() {
        log.info("Player requested to start a new session, changing game state");
        this.enterState(AppState.START_SESSION);
    };
    

    //------------------------------------------------------------------------------
    // Default Fatal Error state
    //------------------------------------------------------------------------------
    // Default Fatal Error state to use when there is an error invoking a service of
    // some kind. We are free to create custom error handlers for different key
    // points in the game, but this state is always available as a fallback.
    //------------------------------------------------------------------------------
    /**
     * @private
     */
    enterFatalErrorState()
    {
        let errorCode = ClientErrorCode.GENERIC_ERROR;
     
        // TODO: If this is kept, then the cache commsError object needs documenting
        if (this._commsError && this._commsError.code) {
            errorCode = this._commsError.code;
            log.error(JSON.stringify(this._commsError));
        }

    };
}

export default WmgAppController;