import formatCoin from "./formatCoin";
import C_SessionType from "../const/C_SessionType";

/**
 * Standard, configurable implementation of a Currency Formatter.
 * @implements CurrencyFormatter
 */
export class ConfigurableCurrencyFormatter
{
    /**
     * Constructs a new instance of the Configurable Currency Formatter.
     * @param {CurrencyFormat} currencyFormat 
     */
    constructor(currencyFormat)
    {
        /**
         * The currently active currency format.
         * @private
         * @type {CurrencyFormat}
         */
        this._currencyFormat = currencyFormat;
    };


    /**
     * Updates the active Currency Format/
     * @public
     * @param {CurrencyFormat} newFormat 
     */
    setCurrencyFormat(newFormat)
    {
        this._currencyFormat = newFormat;
    };


    /**
     * @public
     * @inheritDoc
     * @param {number} moneyInPoints 
     * @param {GameplayMode} sessionType
     * @return {string}
     */
    formatCoin(moneyInPoints, sessionType)
    {
        let useCurrencyChar = sessionType != C_SessionType.FREE_PLAY;
        
        return formatCoin(moneyInPoints, this._currencyFormat, useCurrencyChar);
    };
}