/**
 * A CurrencyFormat object, specifies all the flags needed to render a value in
 * points as a currency string, using the dynamic currency string rendering
 * functionality.
 * @todo: Support regular points strings as well, with the same formatting
 * object. After all, the format is basically used for a domain, eg: although
 * Euro is a currency, i expect us to have separate money formats for each
 * country that uses Euros (as we may get specialized requests for how to
 * render the text: i am, in particular, thinking of the '.' vs ',' for
 * decimal divider).
 */
interface CurrencyFormat
{
    /**
     * The id of the Money Format object.
     */
    id : string,

    /**
     * Indicates if the pre-decimal part should use thousands formatting.
     * If true, a value of $4250060 would be rendered as $4,250,060 (assuming
     * that thousandsDivider is set to ',')
     */
    useThousands : boolean,

    /**
     * The character to use for any optional thousands division. This is only
     * used if useThousands is specified.
     */
    thousandsDivider : string,

    /**
     * Should we use a decimal division with this currency? Not all world currencies
     * use sub-units (for example: Japanese Yen)
     */
    useDecimal : boolean,

    /**
     * The number of decimal places used in the decimal part of the currency. The
     * vast majority of world curencies use 2 decimal places after any "decimal". A
     * couple of minor currencies use 3 decimal places (and some use none). It is highly
     * unlikely we will ever need to support any country that uses anything other than
     * 0 or 2, but the field is left in for semantic completeness.
     */
    decimalAmount : number,
    
    /**
     * The character to use for the decimal divider (if it is being used). For most
     * cases, this will be a choice between '.' and ','
     */
    decimalDivider : string,

    /**
     * The actual currency id string. Some markets may use the same currencies
     * (even though we have a separate money format object, because we might
     * have slightly different conventions for rendering the currency in each
     * country).
     */
    currencyCode : string,

    /**
     * The char to use for the currency.
     */
    currencyChar : string,

    /**
     * Should the currency char be placed at the start of the currency string?
     * If true, the char will be placed at the beginning of the currency string,
     * eg: $1000. If false, it will be placed at the end (eg: 1000$)
     */
    currencyFirst : boolean;

    /**
     * Indicates if a single space should be used between money and currency. If
     * the currency char is rendered at the beginning of the currency string, then
     * the final result would be "$ 1000". If the currency char is rendered at the
     * end of the currency string, then the final result would be "1000 $".
     */
    currencySpace : boolean;
};