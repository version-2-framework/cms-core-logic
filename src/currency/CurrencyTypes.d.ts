/**
 * A Currency Formatter is anything which can take a currency value (in points), and render
 * it to a formatted string.
 */
interface CurrencyFormatter
{
    /**
     * Converts a currency value (in points) into a string value to be rendered.
     * @param moneyInPoints
     * The value (in points) to convert to a currency string. Internally, WMG games always
     * use points, so all CurrencyFormatter implementations must take this into account.
     * If currncy is Euro, then 1 euro == 100 points (so a value passed of 550, means "Euro
     * 5.50"). When interfacing with some external currency conversion APIs, remember that
     * sometimes their methods may expect values of 1.00 where we would use 100.
     */
    formatCoin(moneyInPoints:number, sessionType:GameplayMode) : string;
}