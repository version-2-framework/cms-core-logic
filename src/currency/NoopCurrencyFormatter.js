/**
 * @implements CurrencyFormatter
 */
export class NoopCurrencyFormatter
{
    /**
     * @public
     * @override
     * @param {number} moneyInPoints 
     * @param {GameplayMode} sessionType
     * @return {string}
     */
    formatCoin(moneyInPoints, sessionType)
    {
        return moneyInPoints? moneyInPoints.toString() : "0";
    }
}