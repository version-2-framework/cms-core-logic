/**
 * A GCM implementation of Currency Formatter: falls back to using GCM methods for the
 * currency to string conversion step. GCM expects values to be in a different range to
 * the ones we use internally (they want 1.00 where we use 100): this class also takes
 * care of this conversion.
 * @implements {CurrencyFormatter}
 */
export class SgdCurrencyFormatter
{
    /**
     * Constructs a new Gcm Currency Formatter.
     * @param {SgdGcmCoreApi} gcm 
     */
    constructor(gcm)
    {
        /**
         * @private
         * @type {SgdGcmCoreApi}
         */
        this._gcm = gcm;
    };


    /**
     * @public
     * @inheritdoc
     * @param {number} moneyInPoints 
     * @param {GameplayMode} sessionType
     * @return {string}
     */
    formatCoin(moneyInPoints, sessionType)
    {
        return this._gcm.formatAmount(moneyInPoints / 100);
    };
}