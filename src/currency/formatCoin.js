//--------------------------------------------------------------------------------------------------
// Functionality for formatting points into a currency string, using a formatting config.
// This is not the most efficient way you can do it, but it is hugely flexible:
// the config object let's you dynamically modify how the output currency string
// should be rendered
// 
//--------------------------------------------------------------------------------------------------
/**
 * Converts a value in coin, into a currency string. This is not the most efficient way we
 * can achieve the conversion, but it is hugely flexible: changing the config object lets
 * you dynamically modify how the output currency string should be rendered, including to
 * change the decimal character, number of spaces in different parts, ordering of any
 * currency symbol, etc.
 * @see {CurrencyFormat}
 * @param {number} moneyInPoints
 * The value (in points) to render as a currency string.
 * @param {CurrencyFormat} format
 * The formatter to use for the currency.
 * @param {boolean} useCurrency
 * Indicate if currency should be added. In freeplay games, we probably
 * want to drop the currency indication (but keep currency formatting).
 * @return {string}
 * The formatted currency string.
 */
function formatCoin(moneyInPoints, format, useCurrency)
{
    let preDecimalPart, postDecimalPart;
    let finalMoneyText = "";

    if (format.useDecimal)
    {
        let decimalAmount = format.decimalAmount;
        let decimalDivider = Math.pow(10, decimalAmount);
        let coinInSubUnits = (moneyInPoints / decimalDivider).toFixed(decimalAmount).split('.');
        preDecimalPart = coinInSubUnits[0];
        postDecimalPart = coinInSubUnits[1];
    }
    else
    {
        preDecimalPart = moneyInPoints.toString();
    }

    if (format.useThousands)
    {
        for (let charIndex = 0; charIndex < preDecimalPart.length; charIndex ++)
        {
            if (charIndex > 0 && charIndex % 3 === 0)
            {
                finalMoneyText = format.thousandsDivider + finalMoneyText;
            }

            finalMoneyText = preDecimalPart[preDecimalPart.length - charIndex - 1] + finalMoneyText;
        }
    }
    else
    {
        finalMoneyText = preDecimalPart;
    }

    if (postDecimalPart)
    {
        finalMoneyText += format.decimalDivider + postDecimalPart;
    }

    if (useCurrency)
    {
        let firstPart = (format.currencyFirst)? format.currencyChar : finalMoneyText;
        let finalPart = (format.currencyFirst)? finalMoneyText : format.currencyChar;
        let middlePart = (format.currencySpace)? ' ' : '';
        finalMoneyText = firstPart + middlePart + finalPart;
    }

    return finalMoneyText;
};

export default (formatCoin);