//--------------------------------------------------------------------------------------------------
// Methods for fetching an appropriate Currency Format
//--------------------------------------------------------------------------------------------------

import C_Currency from "../const/C_Currency";

/**
 * Returns the most appropriate Currency Format to use, when passed a string key
 * (the value that is passed to a V2 game client representing currency). When the
 * key passed is undefined or unrecognised, then a default Currency Format value
 * will be returned.
 * @param {string} urlParamCurrencyKey 
 * The V2 url parameter key representing currency format to use.
 * @return {CurrencyFormat}
 * The Currency Format to use
 */
export function getCurrencyFormatFromUrlParam(urlParamCurrencyKey)
{
    let currencyFormat = C_Currency.POINTS;
    
    switch (urlParamCurrencyKey) {
        case "euro":
            currencyFormat = C_Currency.EURO_ITALY;
            break;

        // TODO: There are added to test the logic. We don't yet use these,
        // and the actual enums are not clearly defined.
        case "pound":
            currencyFormat = C_Currency.POUND_UK;
            break;

        case "yen":
            currencyFormat = C_Currency.JAPANESE_YEN;
            break;
    }

    return currencyFormat;
}