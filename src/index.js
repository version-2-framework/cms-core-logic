import ViewManager from '../../cms-core-view/src/views/ViewManager.js';
import { KeyboardManager } from '../../cms-core-view/src/input/KeyboardManager.js';
import Assets from "../../cms-core-view/src/utils/Assets";
import Model from "./model/Model";
import Layout from "../../cms-core-view/src/utils/Layout";
import Locale from "../../cms-core-view/src/utils/Locale";
import SoundController from '../../cms-core-view/src/utils/SoundController';
import LocalData from "../../cms-core-view/src/utils/LocalData";
import KeyboardView from "../../cms-core-view/src/ui/KeyboardView";
import PrecacheView from "../../cms-core-view/src/views/PrecacheView";
import TextField from "../../cms-core-view/src/ui/TextField";
import Utils from "../../cms-core-view/src/utils/Utils";
import C_Dependency from "../../game/src/C_Dependency";
import * as LogManager from "./logging/LogManager";
import * as LogConfig from "./logging/LogConfig";
import * as C_ClientConfigValue from "./const/C_ClientConfigValues";
import { BuildInfo } from './config/BuildInfo.js';
import { SpinModelUtils } from './model/SpinModelUtils.js';
import { OpenLinkUtil } from './utils/OpenLinkUtil.js';
import { ExitClientUtil } from './utils/ExitClientUtil.js';
import WmgAppController from './controllers/WmgAppController.js';
import SgdAppController from './controllers/SgdAppController.js';
import SlotGameController from './controllers/SlotGameController.js';
import { WmgDialogController, SgdDialogController } from './controllers/DialogController.js';
import { DefaultMessageBarController } from './controllers/MessageBarController.js';
import screenfull from "screenfull";
import C_Currency from './const/C_Currency.js';
import { PlatformJsPlatformApi } from './utils/PlatformUtil.js';
import { SgdCurrencyFormatter } from './currency/SgdCurrencyFormatter.js';
import { ReelPositionUtil } from '../../cms-core-view/src/views/ReelPositionUtil';
import { getCurrencyFormatFromUrlParam } from './currency/getCurrencyFormat.js';

const log = LogManager.getDefaultLogger();
LogConfig.setLoggingToTrace();

const onResizeLog = LogManager.getNoopLoger();

const GAME_ASPECT_RATIO = 16/9;

let urlParams = {};
location.search.substr(1).split("&").forEach(
    function (item)
    {
        let tmp = item.split("=");
        if (tmp[0].length > 0)
        {
            urlParams[tmp[0]] = decodeURIComponent(tmp[1]);
        }
    });

/** @type {ClientConfigValue} */
let clientConfigValue = urlParams.config | 1;

/**
 * A BuildInfo instance, using values that are baked into the build.
 */
let buildInfo = new BuildInfo(BUILD_VERSION, GAME_STATIC_VERSION, BUILD_TYPE, BUILD_TARGET, BUILD_PLATFORM, clientConfigValue);

log.debug(`location = ${JSON.stringify(location)}`);
log.info(`BuildInfo = ${buildInfo}`);
log.info(`core-logic-version:${CORE_LOGIC_VERSION}, core-view-version:${CORE_VIEW_VERSION}`);
log.info(`UrlParams = ${JSON.stringify(urlParams)}`);

let link = document.createElement('link');
link.rel = 'stylesheet';
link.href = `./cms-core-view-static/styles/main.css?version=${buildInfo.getBuildVersion()}`;
document.getElementsByTagName('head')[0].appendChild(link);

let startGameClient = function()
{
    let params =
    {
        config : C_Dependency.config,
        gameFactory : C_Dependency.gameFactory,
    };

    let style = document.createElement("style");
    style.type = 'text/css';


    /**
     * The default fonts set in the core-view library
     */
    const defaultFonts = params.config.defaultFonts;

    /**
     * The fonts set in the game library
     */
    const gameConfig = params.config.fonts;

    let fontsToLoad = {};

    //First load in the default fonts
    for (let fontIdx = 0; fontIdx < defaultFonts.length; fontIdx++)
    {
        let fontData = defaultFonts[fontIdx];
        fontsToLoad[fontData.name] =
        {
            url: `./cms-core-view-static/assets/fonts/${fontData.url}?version=${buildInfo.getBuildVersion()}`,
            format: fontData.format ? fontData.format : "truetype"
        };

        TextField.fontMeasure[fontData.name] =
        {
            text: fontData.measureText || params.config.defaultMeasureText,
            baseline: fontData.measureBaseline || params.config.defaultMeasureBaseline
        };
    }

    //Next load in the fonts set in the game config 
    for (let fontId = 0; fontId < gameConfig.length; fontId++)
    {
        let fontData = gameConfig[fontId];

        fontsToLoad[fontData.name] =
        {
            url: `./game-static/assets/fonts/${fontData.url}?version=${buildInfo.getBuildVersion()}`,
            format: fontData.format ? fontData.format : "truetype"
        };

        TextField.fontMeasure[fontData.name] =
        {
            text: fontData.measureText || params.config.defaultMeasureText,
            baseline: fontData.measureBaseline || params.config.defaultMeasureBaseline
        };
    }


    for (let fontName in fontsToLoad)
    {
        style.innerHTML +=
            "@font-face{" +
            "  font-family: " + fontName + ";" +
            "  src: url('" + fontsToLoad[fontName].url + "')" +
            "       format('" + fontsToLoad[fontName].format + "');" +
            "  font-display: block;" +
            "}";

        let p = document.createElement("p");
        p.innerHTML = "abc123";
        p.style.fontFamily = fontName;
        p.style.position = "fixed";
        p.style.bottom = "100%";
        document.getElementsByTagName('body')[0].appendChild(p);
    }

    document.getElementsByTagName('head')[0].appendChild(style);

    window.onload = function ()
    {
        let dpr = window.devicePixelRatio || 1;

        let dimensions = calculateGameDimensions(window.screen.width, window.screen.height);
        let pixiGameWidth = dimensions.width * dpr;
        let pixiGameHeight = dimensions.height * dpr;
        let platformUtil = new PlatformJsPlatformApi();

        log.info(`game start: dpr = ${dpr}`);
        log.info(`game start: window.screen = (width:${window.screen.width},height:${window.screen.height})`);
        log.info(`game start: calculated dimensions: ${JSON.stringify(dimensions)}`);
        log.info(`game start: pixiGameWidth:${pixiGameWidth}, pixiGameHeight:${pixiGameHeight}`);
        log.info(`game start: isDesktop: ${platformUtil.isDesktop()}, isMobile: ${platformUtil.isMobile()}, isTablet:${platformUtil.isTablet()}, isMobileOrTablet:${platformUtil.isMobileOrTablet()}`);

        let app = new PIXI.Application(pixiGameWidth, pixiGameHeight, {transparent: true, resolution:dpr});
        

        PIXI.app = app;
        
        app.version = buildInfo.getBuildVersion();
        app.buildInfo = buildInfo;
        app.config = params.config;
        app.dispatcher = new PIXI.utils.EventEmitter();
        app.urlParams = urlParams;
        app.platform = platformUtil;

        app.localData = new LocalData();
        app.layout = new Layout(pixiGameWidth, pixiGameHeight);
        app.assets = new Assets();

        let keyboardManager = new KeyboardManager();
        keyboardManager.setEnabled(platformUtil.isDesktop());
        app.keyboardManager = keyboardManager;
        app.keyboardView = new KeyboardView();

        let gameContentDiv = document.getElementById('content');
        window.onresize = onResize;
        gameContentDiv.appendChild(app.view);
        onResize();

        /**
         * Toggles full screen mode for the game client, if at all possible.
         */
        app.toggleFullScreen = () =>
        {
            log.info(`App.toggleFullScreen()`);

            // Unfortunately, screenfull doesn't seem to forward us any meaningfull
            // error message when toggling full screen fails: I would bet that the
            // browser generates one, but screenfull simply returns "fullscreen
            // error" as its error message. Oh well!
            screenfull.toggle(gameContentDiv)
                .then(() => log.info(`App: app is now in ${screenfull.isFullscreen? "fullscreen" : "normal"} state`))
                .catch(err => log.debug(`App: could not toggle fullscreen: ${err}`));
        }

        app.gameFactory = params.gameFactory;
        app.gameFactory.app = app;

        document.title = app.config.titleHTML;

        let preCacher = new PrecacheView(app.stage.width, app.stage.height);
        app.stage.addChildAt(preCacher, 0);

        // TODO: Verify the handling of language configuration between the V1 and V2 platforms
        // TODO: The set of supported languages, should exist in a set of constants: in practise,
        // if we start supporting any language, this language must be available for all game clients

        let language = (() =>
        {
            let defaultLanguage, requestedLanguage, actualLanguage;

            if (BUILD_TARGET === "v1")
            {
                defaultLanguage = "it";
                requestedLanguage = defaultLanguage;
            }
            else
            {
                // For V2, we can support a url parameter. By default, we dont expect to get this value
                // (its not sent to the game client by the server which generates launch url, only a "Country"
                // parameter is provided). So, the language url param may not be there at all. If it is,
                // we can configure language for the game. However - because the server returns a country
                // url parameter - we *can* use this to determine "default language". So if country is
                // specified as italy, then language will be italian (unless another one is specified)

                let country = urlParams.country;
                let countryToLanguageMap =
                {
                    ITA : "it"
                };

                if (country && country in countryToLanguageMap)
                {
                    defaultLanguage = countryToLanguageMap[country];
                    log.info(`game start: country is ${country}, therefore default language is ${defaultLanguage}`);
                }
                else
                {
                    defaultLanguage = "en";
                    log.info(`game start: cannot map country (${country}) to a default language, so falling to english for default`);
                }

                requestedLanguage = urlParams.language;

                
            }

            let availableLanguages = app.config.languages;
            if (requestedLanguage && availableLanguages.indexOf(requestedLanguage) > -1) {
                actualLanguage = requestedLanguage;
                log.info(`game start: language is set to requested value of ${requestedLanguage}`);
            }
            else
            {
                if (requestedLanguage) {
                    log.warn(`game start: requestedLanguage ${requestedLanguage} not supported`);
                    log.warn(`game start: languages available are ${availableLanguages}`);
                    log.warn(`game start: falling back to default of ${defaultLanguage}`);
                }
                else
                {
                    log.info(`game start: no specific language requested on gameClient launch, using default language of ${defaultLanguage}`);
                }

                actualLanguage = defaultLanguage;
            }

            return actualLanguage;
        })();

        // Determine what file to load up for BusinessConfig.
        // TODO: For V2, in future iterations, this can also check what "licensee" info
        // is passed to the game client: it can load up for itself, the specific licensee
        // sub-data. This is potentially very useful, as it means we can have a single
        // client deployment, which works as qa / staging / production, and can simply
        // load the required licensee config: should make continuous updates to the game
        // clients far simpler.
        let businessConfigFileName = 'config/BusinessConfig.json';
        let localizationsCommonFileName = `localization/Localizations_Common_${language}.json`;
        let localizationsLicenseeFileName = `game-static/localization/Localizations_Licensee_${language}.json`;

        if (BUILD_TARGET === "v1")
        {
            localizationsLicenseeFileName = `config/Localizations_Licensee_${language}.json`;
        }
        else
        if (BUILD_TARGET === "v2")
        {
            /** @type {UrlParamsGen2} */
            let v2UrlParams = urlParams;

            // Our "clientConfig" value internally is a number (1/2/3), but of course,
            // the url parameter is a string! For all V2 builds, we check this parameter,
            // as we are determining whether to load the QA, STAGING or PRODUCTION config.
            let clientConfig = parseInt(v2UrlParams.config);
            let businessConfigSuffix;
            if (clientConfig === C_ClientConfigValue.QA)
            {
                businessConfigSuffix = 'QA';
            }
            else
            if (clientConfig === C_ClientConfigValue.STAGING)
            {
                businessConfigSuffix = 'STAGING';
            }
            else
            // By defaut, we assume its a production environment
            // if (clientConfig === C_ClientConfigValue.PRODUCTION)
            {
                businessConfigSuffix = 'PRODUCTION';
            }

            // Set business config to a default fallback - and then next,
            // we see if we can find something more specific that we should
            // be loading up.
            businessConfigFileName = `./config/BusinessConfig.${businessConfigSuffix}.json`;

            if (BUILD_PLATFORM === "wmg")
            {
                let licenseeEnumId;
                
                if (v2UrlParams.licenseeId) {
                    licenseeEnumId = Number.parseInt(v2UrlParams.licenseeId);
                }

                // In case of WMG licensee ids, "0" is supposed to be unassigned (eg: it means "no licensee"),
                // and any value larger represents an specified operator. We start from 1, and go upwards,
                // although we should basically try and load files according to whatever numerical id was
                // passed in.
                if (licenseeEnumId > 0)
                {
                    if (v2UrlParams.subLicenseeId)
                    {
                        let subLicenseeId = v2UrlParams.subLicenseeId;

                        log.info(`game start: loading custom licensee data for licensee ${licenseeEnumId}, subLicensee ${subLicenseeId}`);

                        businessConfigFileName = `./config/${licenseeEnumId}/${subLicenseeId}/BusinessConfig.${businessConfigSuffix}.json`;
                        localizationsLicenseeFileName = `./config/${licenseeEnumId}/${subLicenseeId}/Localizations_Licensee_${language}.json`;
                    }
                    else
                    {
                        log.info(`game start: loading custom licensee data for licensee ${licenseeEnumId}`);

                        // Sub-licensee not used
                        businessConfigFileName = `./config/${licenseeEnumId}/BusinessConfig.${businessConfigSuffix}.json`;
                        localizationsLicenseeFileName = `./config/${licenseeEnumId}/Localizations_Licensee_${language}.json`;
                    }
                }
                else
                {
                    // We don't need to do anything further, as the default locations which our businessConfig file
                    // name / common localizations fileName point to are already correct for default files. However,
                    // some warnings printed to logs are usefull!

                    if (licenseeEnumId === 0)
                    {
                        log.info(`game start: licenseeId is 0, loading default config`);
                    }
                    else
                    if (!licenseeEnumId) {
                        log.warn(`game start: licenseeId not provided through url parameters`);
                    }
                    else
                    {
                        log.warn(`game start: no data configured for licenseeId ${licenseeEnumId}`);
                    }

                    log.info(`game start: loading default businessConfig and localization files for V2 platform`);
                }
            }
            else
            if (BUILD_PLATFORM === "sgdigital")
            {
                // We need to load default SGDigital config (which will already be happening).
                // OR override it based on operator id.

                let sgdOperatorId = v2UrlParams.sgdOperatorId;
                
                // If we got an operator id, attempt to access sub config. This could fail, if we receive
                // an operatorId with no config available : in such a case, the game client will simply
                // fail to load. Logs should tell us which file failed to load, though.
                if (sgdOperatorId)
                {
                    log.info(`game start: loading custom BusinessConfig for SGDigital operator ${sgdOperatorId}`);

                    businessConfigFileName = `config/${sgdOperatorId}/BusinessConfig.${businessConfigSuffix}.json`;
                    localizationsLicenseeFileName = `config/${sgdOperatorId}/Localizations_Licensee_${language}.json`;
                }
                // Otherwise, we were not provided an sgd operator id, so we fall back to default config.
                else
                {
                    log.info(`game start: no sgdigital operator id supplied, load up default sgdigital business configs`);
                }
            }

            log.debug(`game start: loading BusinessConfig from ${businessConfigFileName}`);
            log.debug(`game start: loading Common Localizations from ${localizationsCommonFileName}`);
            log.debug(`game start: loading Licensee Localizations from ${localizationsLicenseeFileName}`);
        }

        // Disable logging, for now, until we have business config loaded.
        LogConfig.setLoggingToWarning();

        // TODO: Pixi's default loader doesn't actually give you a simple way of checking if an asset
        // fails to load.
        // - loader.onComplete fires when the "queueu is finished", but all assets could have failed
        //   to load, and this would still happen
        // - you can listen for an individually queued asset failing to load
        // - you can detect an asset failing to load, but you only get a reference to its abstract
        //   asset loading data. You CAN check its name - this is probably best case scenario.
        // In any case, all of the bootLoader assets are critical: its only really sound assets that
        // we could allow to fail loading, when the game is running.

        let bootLoader = new PIXI.loaders.Loader();
        bootLoader
            .add('atlases', `./game-static/atlases.json?version=${buildInfo.getBuildVersion()}`)
            .add('dynamicAssets', `dynamicAssets.json?version=${buildInfo.getBuildVersion()}`)
            .add('preloader', './game-static/assets/textures/preloader.json')
            .add('businessConfig', `./${businessConfigFileName}?version=${buildInfo.getBuildVersion()}`)
            .add(`locale_common_${language}`, `./game-static/${localizationsCommonFileName}?version=${buildInfo.getBuildVersion()}`)
            .add(`locale_licensee_${language}`, `./${localizationsLicenseeFileName}?version=${buildInfo.getBuildVersion()}`)
            .add(`locale_game_${language}`, `./game-static/assets/localization/Game_${language}.json`);

        // Load country specific art assets: at the moment, this basically means loading in responsible
        // gaming art assets, for Italy (and not when SGDigital). This part should get smarted up later:
        // it would make sense for our BusinessConfig to have some say about what optional asset set gets
        // loaded in here.
        if (BUILD_TARGET === "v2" && (urlParams.country === "ITA" && BUILD_PLATFORM === "wmg") ||
            BUILD_TARGET === "v1")
        {
            bootLoader.add('countryAssets', './cms-core-view-static/assets/textures/ADM_logos_italy.json');
            bootLoader.add('licenseeAssets', './cms-core-view-static/assets/textures/licensee_assets_italy.json');
        }
        
        // TODO: The manner in which languages are loaded needs to be changed
        
        let bootLoaderAssetsFailed = false;

        /**
         * Callback invoked when bootLoader reports that its queue has completed. This doesn't actually
         * mean that all assets loaded - it means that all assets have failed or completed, and there is
         * nothing left in the load queue. We cannot continue the application if any bootLoad asset
         * failed - they are generally critical configuration data.
         */
        let bootComplete = () =>
        {
            if (bootLoaderAssetsFailed)
            {
                // TODO: Its possible that we could look at the nature of the failure, and see if we can
                // try again.

                window.alert("There was a problem loading some assets");

                log.fatal("Some bootloader assets failed to load");   
            }
            else
            {
                log.info(`bootLoader queue complete: all boot assets available`);

                initGameComponentsWhichRequireBootLoaderAssets();
            }
        }

        bootLoader.load(bootComplete);
        bootLoader.onError.add((errMsg, loader, resource) => {
            bootLoaderAssetsFailed = true;
            log.error(`boot asset (${resource.name}) failed to load: ${errMsg}`);
        });
        
        app.bootLoader = bootLoader;

        let initGameComponentsWhichRequireBootLoaderAssets = () =>
        {
            log.info('game start: init game components which require boot loader assets');

            let isSgDigital = BUILD_PLATFORM === "sgdigital";

            /** @type {Model} */
            let model;

            // Manually insert "Licensee Assets" and "Country Assets" into default asset set.
            // TODO: If BoadLoader assets get disposed of, if this involves any call to dispose
            // the texture loaded, then this process will break. So, the way responsible gaming
            // and country assets are handled may need some changing.
            
            if (bootLoader.resources.licenseeAssets) {
                PIXI.loader.resources.licenseeAssets = bootLoader.resources.licenseeAssets;
            }

            if (bootLoader.resources.countryAssets) {
                PIXI.loader.resources.countryAssets = bootLoader.resources.countryAssets;
            }

            let dynamicAssetsJson = bootLoader.resources.dynamicAssets.data;

            /** @type {BusinessConfig} */
            let businessConfig = bootLoader.resources['businessConfig'].data;

            // All of the components instantiated here, require some of the pre-loaded assets to be
            // available. Of special importance, is BusinessConfig, which tells us the set of rules
            // that govern how the game client is integrated into its current deployment.
            app.businessConfig = businessConfig;
            app.spinModelUtils = new SpinModelUtils(app);

            // Configure logging, based off the BusinessConfig setting
            if (businessConfig.enableLogging) {
                LogConfig.setLoggingToTrace();
                LogConfig.disableSpinAnimLogging();
            }

            // For SGDigital, we use a custom Currency Formatter. This basically calls out to
            // a method available on the SGDigital GUI's api. The custom currencyFormatter of
            // course has the standard API, so its use in the code base doesnt change.
            if (isSgDigital) {
                log.info('game start: set CurrencyFormatter to Sgdigital Currency Formatter');
                app.currencyFormatter = new SgdCurrencyFormatter(window.wmg.getGcmInstance());
            }

            app.model = model = new Model(app);
            app.spinPresentationBuilder = params.gameFactory.getSpinPresentationBuilder(app);
            app.spinModel = params.gameFactory.getSpinModel(app);
            app.bonusModel = params.gameFactory.getBonusModel(app);
            app.services = params.gameFactory.getServices(app);
            app.utils = new Utils();
            app.viewManager = new ViewManager();
            app.dragonBones = dragonBones.PixiFactory.factory;
            app.openLinkUtil = new OpenLinkUtil(app);
            app.exitClientUtil = new ExitClientUtil(urlParams);
            app.reelPositionUtil = new ReelPositionUtil();
            
            
            app.locale = new Locale(language, bootLoader);

            // SoundController wants to know up front the set of data assets loaded: this allows
            // it to automatically make inferrences about file type and so on.
            let soundController = new SoundController();
            soundController.preParseDynamicAssets(dynamicAssetsJson);
            app.sound = soundController;

            app.spinSoundController = params.gameFactory.getSpinSoundController(app.sound);

            let getGameController = () => {
                let gameType = "slot";
                if (gameType === "slot") {
                    return new SlotGameController(app);
                }
                else return null;
            }

            // Let's configure some model settings
            model.setCurrencyFormat(getCurrencyFormat());

            // TODO: This is basically the "GUI Integration" stage.
            if (isSgDigital)
            {
                log.info('game start: buildPlatform is sgdigital online. Instantiate SGDigital components');

                app.gcm = window.wmg.getGcmInstance();
                
                app.dialogController = new SgdDialogController(app);
                app.messageBarController = params.gameFactory.getMessageBarController();
                app.gameController = getGameController();
                app.controller = new SgdAppController(app);
            }
            // Standard Wmg online version
            else
            {
                log.info('game start: buildPlatform is wmg online. Instantiate WMG Online components.');

                app.dialogController = new WmgDialogController(app);
                app.messageBarController = params.gameFactory.getMessageBarController();
                app.gameController = getGameController();
                app.controller = new WmgAppController(app);
            }
        }


        function disposeBootLoader()
        {
            // todo - dispose assets from bootloader
        }


        /**
         * Executed when the browser window resizes. Takes care of master resizing, and then tells the Layout
         * class to resize (which is what triggers other resize actions). In this code unit, this function is
         * assigned to a global method on window (and is called elsewhere). It's UGLY, and could do with some
         * refacoring.
         */
        function onResize()
        {
            let scale = 'scale(1)';
            document.body.style.webkitTransform = scale;    // Chrome, Opera, Safari
            document.body.style.msTransform = scale;        // IE 9
            document.body.style.transform = scale;          // General

            let dimensions = calculateGameDimensionsFromAdjustedDimensions(window.innerWidth, window.innerHeight);

            onResizeLog.info(`onResize: calculated dimensions = ${JSON.stringify(dimensions)}`);

            let vpWidth = dimensions.width;
            let vpHeight = dimensions.height;

            onResizeLog.info(`onResize: innerWidth:${window.innerWidth}, innerHeight:${window.innerHeight}`);

            // This "isLandscape" check, is.... incorrect ? They are taking the values
            // from the recalculated game dimensions, and .... not from the actual window size.
            // Because the game is always wider than it is tall, this always determines that the
            // game is in landscape - so any following logic which relates to the game not being
            // in landscape, will never be executed.
            let isLandscape = vpWidth > vpHeight;

            // OK, i can understand scalefactor here: we create the PIXI game with a certain set of
            // dimensions, and presumably, its initial scale is 1: we then need to work out what to
            // scale it to, to match the newer dimensions
            let scaleFactor, newWidth, newHeight;

            // Because the landscape check is wrong, its always in landscape by their judgement
            // I cannot yet tell if this creates a problem.
            // In addition, I guess that it is actually not relevant if we are currently in landscape,
            // its maybe more relevant if our orientation changed from startWidth / startHeight.
            // Maybe not? Maybe the div doesnt change orientation. There is no documentation here to
            // explain how the sizing process was implemented, so this is guesswork.

            // another thing that *seems* wrong - this code here doesn't take DPR into account.
            // START_WIDTH / START_HEIGHT are values scaled by dpr - the values calculated in this
            // method are not. So, if dpr==3 (as it does on various mobile phones), then the scale
            // factor calculated here *appears* to be out by a multiple of 3, which seems weird!!
            // However - it works correctly as it is, which I don't full understand yet. PIXI already
            // knows the dpr (its passed as a parameter when the PIXI.Application is created), so it
            // may well be that this part is actually implemented correctly, for reasons that I do not
            // fully understand yet. but darnit, this code could desparately have done with being
            // documented: the whole mechanism for sizing is arbitrary (in the sense that there are
            // many ways to do it, and many details selected here are arbitrary): it isn't ok for stuff
            // like this to not be explained.
            if (isLandscape)
            {
                scaleFactor = Math.min(vpWidth / pixiGameWidth, vpHeight / pixiGameHeight);
                newWidth = Math.ceil(pixiGameWidth * scaleFactor);
                newHeight = Math.ceil(pixiGameHeight * scaleFactor);
            }
            else
            {
                scaleFactor = vpWidth / pixiGameWidth;
                newWidth = vpWidth;
                newHeight = vpHeight;
            }

            onResizeLog.info(`onResize: resizing to (width:${newWidth}, height:${newHeight}, scaleFactor:${scaleFactor}`);

            app.renderer.view.style.width = `${newWidth}px`;
            app.renderer.view.style.height = `${newHeight}px`;

            // TODO: I don't explicitly understand why this needs to be done. Some explanation needs adding here.
            app.renderer.resize(newWidth, newHeight);
            app.stage.scale.set(scaleFactor);

            // This seems to be.. basically correcting for sub-pixel offsets, which seems like it shouldn't
            // actually work or do anything. Real aspect and target aspect are more or less identical in
            // practise, with the exception that our real aspect might be imperfect (we have to round the pixels
            // in one of our 2 dimensions). This seems.. to correct for that, ie: it pushes x or y by a......
            // sub-pixel value..... which seems like it should do nothing?
            // Again, this desperately needed some documentation to explain why this has been written. I cannot
            // tell if its nonsense, or does something critical.
            // I am not even sure what setting the x/y values of PIXI's stage really does in practise, so that
            // makes it extra-confusing to understand the point of this.
            let aspect = newWidth/newHeight;
            if (aspect > GAME_ASPECT_RATIO)
            {
                app.stage.x = (newWidth - GAME_ASPECT_RATIO * newHeight) * .5;
                app.stage.y = 0;
            }
            else
            {
                app.stage.x = 0;
                app.stage.y = (newHeight - (1/GAME_ASPECT_RATIO) * newWidth) * .5;
            }
            
            app.layout.onResize(newWidth, newHeight);
        }


        /**
         * Calculates game dimensions, based on the available width / height. The values passed
         * here should be window.screen.width / window.screen.height (which do not change on a mobile
         * device, even if orientation changes): this will take orientation into account, eg: it will treat
         * the larger dimension as the desired max game width.
         * @todo: It it really 
         * @param {number} availableWidth 
         * @param {number} availableHeight
         * @return {{ width:number, height:number }}
         */
        function calculateGameDimensions(availableWidth, availableHeight)
        {
            let orientationAdjustedWidth, orientationAdjustedHeight;

            if (availableWidth > availableHeight) {
                orientationAdjustedWidth = availableWidth;
                orientationAdjustedHeight = availableHeight;
            }
            else
            {
                orientationAdjustedWidth = availableHeight;
                orientationAdjustedHeight = availableWidth;
            }
            
            return calculateGameDimensionsFromAdjustedDimensions(orientationAdjustedWidth, orientationAdjustedHeight);
        };

        
        /**
         * Calculates game dimensions, not taking any account of landscape / portrait orientation: it assumes
         * that the width / height values passed in are already adjusted (some devices return them in the opposite
         * way to what you would expect). For desktop, its appropriate to call this exactly as it is.
         * @param {number} availableWidth 
         * @param {number} availableHeight 
         * @return {{ width:number, height:number }}
         */
        function calculateGameDimensionsFromAdjustedDimensions(availableWidth, availableHeight)
        {
            let gameWidth, gameHeight;

            let targetAspectRatio = GAME_ASPECT_RATIO;
            let actualAspectRatio = availableWidth / availableHeight;

            if (actualAspectRatio >= targetAspectRatio)
            {
                gameHeight = availableHeight;
                gameWidth = gameHeight * targetAspectRatio;
            }
            else
            {
                gameWidth = availableWidth;
                gameHeight = gameWidth / targetAspectRatio;
            }

            return { width:gameWidth, height:gameHeight };
        }

        /**
         * Returns the active Currency Format. For V1 builds, this will always be Italy. For V2 builds,
         * it is dynamically assigned on game client launch. Note that for SGDigital builds, CurrencyFormat
         * is basically not used (as currency string formatting is carried out by an SGDigital GUI component)
         * @return {CurrencyFormat}
         * The Currency Format to use
         */
        function getCurrencyFormat()
        {
            let currencyFormat = C_Currency.POINTS;
        
            if (BUILD_TARGET === "v1") {
                currencyFormat = C_Currency.EURO_ITALY;
            }
            else
            if (BUILD_TARGET === "v2") {
                let currencyUrlParam = urlParams.currency;
                return getCurrencyFormatFromUrlParam(currencyUrlParam);
            }
        
            return currencyFormat;
        }
    };
};



// This method must be called at the html layer. For some game client builds and deployments,
// we have some special loading cases (for example: SG Digital requires us to do some complicated
// dynamic library loading, which must be performed before we can start the game client). As the
// html layer knows when this operation is completed, it is responsible for calling this method.
window.wmg = window.wmg || {};
window.wmg.startGameClient = startGameClient;