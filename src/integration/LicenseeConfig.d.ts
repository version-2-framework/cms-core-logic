// TODO: Document or deprecate this feature (it relates to the V2 platform)

interface LicenseeData
{
    /**
     * A print friendly rendering of the Licensee Name (if required) for the Licensee.
     */
    presentationName : string;

    /**
     * Name of the config sub-folder, under which we will find the correct business config
     * files for this licensee.
     */
    folderName : string;
}