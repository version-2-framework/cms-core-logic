import ulog from 'ulog';
import { setSpinViewLogLevel } from './LogManager';

export function setLoggingToTrace() {
    ulog.level = ulog.TRACE;
}

export function setLoggingToWarning() {
    ulog.level = ulog.WARN;
}

export function disableSpinAnimLogging() {
    setSpinViewLogLevel(ulog.WARN);
}