import ulog from 'ulog';

import * as Logs from './Logs';

/**
 * Returns a logger matching a given key.
 * @param {*} key
 * @return {Logger}
 */
export function getLogger(key)
{
    return ulog(key);
};

let noopLogger = ulog("noop");
noopLogger.level = 0;

/**
 * Returns a logger instance that is guaranteed to never print anything to any target output.
 * Useful when you want to keep a large amount of logging code within a code unit - not to
 * necessarily disable the "channel" that the code is being printed through - and to simply
 * redirect that logging to a null output (by switching the logger being used)
 * @returns {Logger}
 */
export function getNoopLogger() {
    return noopLogger;
}

/**
 * Returns the logger to use for the comms (and services) layer.
 * @return {Logger}
 */
export function getCommsLogger() {
    return ulog(Logs.COMMS);
};


/**
 * Returns the logger to use for the Game View layer.
 */
export function getGameViewLogger() {
    return ulog(Logs.GAME_VIEW);
};

/**
 * Sets the GameView logger loglevel.
 * @param {LogLevel} level 
 */
export function setGameViewLogLevel(level) {
    getGameViewLogger().level = level;
}

/**
 * Returns the logger to use for the Spin View layer.
 */
export function getSpinViewLogger() {
    return ulog(Logs.SPIN_VIEW);
};

/**
 * 
 * @param {LogLevel} level
 * The log level, from 0 (disabled) tp 6 (trace)
 */
export function setSpinViewLogLevel(level) {
    getSpinViewLogger().level = level;
};

/**
 * Returns the logger to use for general App diagnostics. This log channel
 * is used to give general info on app configuration, launching, etc.
 */
export function getAppLogger() {
    return ulog(Logs.CONTROL)
}

export function getNoopLoger() {
    let log = ulog("noop");
    log.level = log.NONE;
    return log;
}


/**
 * Returns the default logger instance
 * @return {Logger}
 */
export function getDefaultLogger()
{
    return ulog();
}