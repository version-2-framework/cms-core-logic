interface Logger
{
    fatal(msg : string, ...args : any[]) : void;

    error(msg : string, ...args : any[]) : void;

    warn(msg : string, ...args : any[]) : void;

    info(msg : string, ...args : any[]) : void;

    debug(msg : string, ...args : any[]) : void;

    trace(msg : string, ...args : any[]) : void;
}

type LogLevel = 0 | 1 | 2 | 3 | 4 | 5 | 6;