export const COMMS = 'comms';

export const GAME_VIEW = 'gameView';

export const SPIN_VIEW = 'spinView';

export const CONTROL = 'control';

export const MODEL = 'model';