/**
 * Implementation of an Outcome Selector, which uses a simple choose table in the
 * background to calculate the chance of each specific outcome.
 * 
 * When calling the constructor, you need to pass in an array of Outcome items. Let's consider a simple
 * example: we want to select the values [1,2,3], with a random To create a choose
 * table, which will 
 * 
 * [{ value:1, chance:60 }]
 * 
 * @implements {OutcomeSelector}
 */
export class ChooseTable
{
    /**
     * Constructs a new Choose Table instance, pre-initialized with a set of values.
     * @param {Outcome[]} outcomeTable 
     */
    constructor(outcomeTable)
    {
        /**
         * @private
         * @type {Outcome[]}
         */
        this._outcomeTable = outcomeTable;

        /**
         * @private
         * @type {number}
         */
        this._chooseRange = 0;
    };


    /**
     * @public
     * @param {Rng} rng 
     * @return {*}
     */
    selectOutcome(rng)
    {
        return chooseOutcome(rng, this._outcomeTable);
    };
}


/**
 * Selects an outcome from an outcome table.
 * @param {Rng} rng 
 * @param {Outcome[]} outcomeTable 
 */
export function chooseOutcome(rng, outcomeTable)
{
    let outcome = null;
    let cumulativeChoose = 0;

    let chooseRange = 0;
    outcomeTable.forEach(possibleOutcome => {
        chooseRange += possibleOutcome.chance;
    });

    let randomValue = rng.getRandom(chooseRange);
    
    for (let i = 0; i < outcomeTable.length; i ++) {
        let possibleOutcome = outcomeTable[i];
        cumulativeChoose += possibleOutcome.chance;
        if (randomValue <= cumulativeChoose) {
            outcome = possibleOutcome.value;
            break;
        }
    }

    return outcome;
}