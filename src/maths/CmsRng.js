//--------------------------------------------------------------------------------------------------
// CMS Rng
//--------------------------------------------------------------------------------------------------
// JavaScript implementation of the CMS Random Number Generator.
//--------------------------------------------------------------------------------------------------

/**
 * Default value for range.
 * @todo:
 * Determine what this should be.
 */
const DEFAULT_RANGE = 100;

const MULT_IX = 171;
const MULT_IY = 172;
const MULT_IZ = 170;
	
const MOD_IX = 30269;
const MOD_IY = 30307;
const MOD_IZ = 30323;
	
/**
 * @typedef {Object} RngSeedState
 * @property {number} ix
 * @property {number} iy
 * @property {number} iz
 */

/**
 * An implementation of the Cms Rng, as a class.
 * @implements {Rng}
 */
export class CmsRng
{
	constructor()
	{
		this.setSeed();
	}

	/**
	 * Sets the seed to be used by the RNG.
	 * @public
	 * @param {number} [seed]
	 * The seed value. If '0', or a negative is passed in, the RNG will seed itself
	 * based off the current system time. Otherwise, a positivei number passed as the
	 * argument, will be used for generating the seed.
	 */
	setSeed(seed)
	{
		seed = seed || 0;
		seed = (seed < 0)? 0 : seed;

		if (seed === 0) {
			seed = new Date().valueOf();
		}

		this._ix = seed - 100;
		this._iy = seed - 999;
		this._iz = seed + 3016;
	};

	/**
	 * Sets the RNG seed state, using the seed state object (returned by
	 * "setSeedState"). This method allows us to track the current RNG
	 * seed state, and then reproduce it.
	 * @public
	 * @param {RngSeedState} seedState 
	 */
	setSeedState(seedState)
	{
		this._ix = seedState.ix;
		this._iy = seedState.iy;
		this._iz = seedState.iz;
	};

	/**
	 * Returns the current seed state of the RNG, as the 3 partial components.
	 * @public
	 * @return {RngSeedState}
	 */
	getSeedState()
	{
		return { ix:this._ix, iy:this._iy, iz:this._iz };
	};

	/**
	 * Returns a random integer, up to (but not including) the value specified
	 * in parameter range.
	 * @public
	 * @param {number} range
	 * The range of the random integer to generate. The value returned will have
	 * a maximum value of range - 1. If range value passed in is <= 0, or no range
	 * value is specified, the value returned will be 0.
	 * @return {number}
	 */
	getRandom(range)
	{
		let randomInt;

		range = range || 0;
		if (range <= 0)
		{
			return 0;
		}

		this._ix = (this._ix * MULT_IX) % MOD_IX;
		this._iy = (this._iy * MULT_IY) % MOD_IY;
		this._iz = (this._iz * MULT_IZ) % MOD_IZ;

		randomInt =  (this._ix << 16) / MOD_IX;
		randomInt += (this._iy << 16) / MOD_IY;
		randomInt += (this._iz << 16) / MOD_IZ;

		randomInt &= 0x0000ffff;

		if (range < 0x10000)
		{
			randomInt *= range;
			randomInt >>= 16;
		}

		return randomInt;
	};


	/**
	 * @public
	 * @inheritDoc
	 * @param {number} min 
	 * @param {number} max
	 * @return {number}
	 */
	getRandomBetween(min, max)
	{
		let range = (max - min) + 1;
		return min + this.getRandom(range);
	};


    /**
     * @public
     * @inheritdoc
     * @param {number} minValue 
     * @param {number} maxValue 
     */
    getRandomFloatBetween(minValue, maxValue)
    {
        let randomRange = maxValue - minValue;
        let randomFloat = minValue + (randomRange * this.getRandom(100) / 100);
        return randomFloat;
    };


	/**
	 * Get a random boolean. This is a simple shortcut for 50 / 50 chance.
	 * @public
	 * @return {boolean}
	 */
	getRandomBoolean()
	{
		return (this.getRandom(2) === 0);
	};


    /**
     * @public
     * @inheritdoc
     * @param {number} a 
     * @param {number} b
     * @return {boolean}
     */
    getRandomChance(a, b)
    {
        return this.getRandom(b) < Math.min(a,b);
    };
}