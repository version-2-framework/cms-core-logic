import { CmsRng } from "./CmsRng";

/**
 * A singleton RNG instance. Can be used when you don't want to instantiate one manually.
 * @type {Rng}
 */
export default new CmsRng();