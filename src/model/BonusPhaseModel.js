//--------------------------------------------------------------------------------------------------
// Bonus Model
//--------------------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------------------
import * as Parse from '../utils/ParseUtil';
import * as Field from '../const/C_MessageField';
import {CmsRng} from '../maths/CmsRng';
import { GamePhaseResultModel } from './GamePhaseResultModel';


/**
 * Base class for a Bonus Model used in a game based on Gen 2 mathematics. For Gen 2
 * based mathematics, the Game Client implementation is basically free to do whatever
 * it wants in the Bonus Phase - the Game Engine Server provides only a very limited
 * set of data in its results (including a fixed RNG seed, designed to let us generate
 * our local client results model).
 * 
 * This class is a base for Game Client specific Bonus Model implementations. The
 * extended classes should implement custom parsing (to generate whatever additional
 * local results data that they want).
 * 
 * @todo:
 * This class is currently a bit thin - but i suspect we will need to expand the common
 * functionality added here (it's not yet completely clear how the common functionality
 * will work, we need to work this out over time).
 */
class BonusPhaseModel extends GamePhaseResultModel
{
    /**
     * @param {Dependencies} dependencies
     */
    constructor(dependencies)
    {
        super();

        /**
         * The type of Bonus in play.
         * @private
         */
        this._bonusType = 0;
    };
    

    /**
     * @public
     * @param {Gen2BonusPhaseResultsReply} serverResult
     */
    setPhaseResults(serverResult) {
        super.setPhaseResults(serverResult);
        this._bonusType = 0;
        this._isBigWin = false;
        this._numFreeSpinsWon = serverResult.numFreeSpins;
        this._phaseResult = serverResult.result;
    };


    /**
     * Returns the phase result for the bonus
     * @public
     * @return
     */
    getPhaseResult() {
        return this._phaseResult;
    }

    //--------------------------------------------------------------------------------------------------
    // Public actions, callable by view.
    //--------------------------------------------------------------------------------------------------
    /**
     * @public
     * @return {number}
     */
    getBonusType() {
        return this._bonusType;
    };


    /**
     * Indicates if the whole Bonus Phase should be treated as a Big Win(ning) Bonus.
     * @public
     * @return {boolean}
     */
    getIsBigWin() {
        return this._isBigWin;
    };


    /**
     * Returns the total number of FreeSpins won for the whole phase.
     * @public
     * @return {number}
     */
    getNumFreeSpinsWon() {
        return this._numFreeSpinsWon;
    };
}

export default BonusPhaseModel;