import * as Parse from '../utils/ParseUtil';

/**
 * Base class of Phase Results model for games based on Gen 2 mathematics.
 */
export class GamePhaseResultModel
{
    constructor()
    {
        
    }

    /**
     * 
     * @param {GameResultPacket<GamePhaseResult>} serverResult 
     */
    setPhaseResults(serverResult) {
        this._totalCreditWon = serverResult.result.totalCreditWon;
        this._totalSuperBetWon = serverResult.result.totalSuperBetWon;
        this._numFreeGamesWon = serverResult.result.numFreeGamesWon;
    };

    // TODO: Add this field, or deprecate it 
    /**
     * Indicates if this is a reloaded Phase.
     */
    getIsReloadedPhase() {
        return false; //this._isReloadedPhase;
    };

    /**
     * Returns the total Credit won for the whole phase.
     * @public
     * @return {number}
     */
    getTotalCreditWon() {
        return this._totalCreditWon;
    };

    /**
     * Returns the total SuperBet won for the whole phase.
     * @public
     * @return {number}
     */
    getTotalSuperBetWon() {
        return this._totalSuperBetWon;
    };


    /**
     * Returns the total number of FreeGames won for the whole phase.
     * @public
     * @return {number}
     */
    getNumFreeGamesWon() {
        return this._numFreeGamesWon;
    };
    

    /**
     * Returns the total number of FreeSpins won for the whole phase.
     * @public
     * @return {number}
     */
    getNumFreeSpinsWon() {
        return 0;
    };
}