import * as C_RewardGroup from '../const/C_RewardGroup';
import * as Parse from '../utils/ParseUtil';
import * as ArrayUtils from "../utils/ArrayUtils";
import C_SymbolType from '../const/C_SymbolType';
import {SpinModelUtils} from './SpinModelUtils';

const MessageTypes =
{
    SINGLE_SPIN_PHASE_RESULT : "WMG_GAME_RESULTS_REPLY",
    SPIN_1_PHASE_RESULT : "WMG_SPIN1_RESULTS_REPLY",
    SPIN_2_PHASE_RESULT : "WMG_SPIN2_RESULTS_REPLY"
}

/**
 * Results parser for Gen 1 slot games.
 */
export class Gen1SpinResultParser
{
    /**
     * 
     * @param {{config:SlotConfig, spinModelUtils:SpinModelUtils}} deps 
     */
    constructor(deps)
    {
        /**
         * @private
         */
        this._slotConfig = deps.config;

        /**
         * @private
         */
        this._spinUtils = deps.spinModelUtils;

        // TODO: This would cleary fail, if we had more than 1 bonus symbol... although, on
        // Gen 1 game methamatics, we will not (as there are only 4 gen 1 game maths, and
        // all have exactly 1 bonus symbol id)
        this._bonusSymbol = (() => {
            let bonusSymbol = null;
            Object.values(this._slotConfig.symbols).forEach(symbol => {
                if (symbol.type === C_SymbolType.SCATTER) {
                    bonusSymbol = symbol;
                }
            })
            return bonusSymbol;
        })();
    };


    /**
     * Parses a Gen 1 Server result, which could be any type of Spin phase result - Single Spin,
     * Spin 1 or Spin 2. The method checks the messageType field to determine which it is expected
     * to be, and then invokves the appropriate bespoke functionality.
     * @public
     * @param {Gen1SpinPhaseResultsReply} serverResult
     * @return {SpinPhaseResult}
     */
    parseServerResult(serverResult)
    {
        let totalStake = Parse.asInt(serverResult.BET);
        let startReelPositions = Parse.asIntegerArray(serverResult.SNR);
        let finalReelPositions = Parse.asIntegerArray(serverResult.RNR);
        let finalSymbolIds = this.extractSymbols(finalReelPositions);
        let symbolWins = this.extractSymbolWinsData(serverResult);
        let winningLineIds = Parse.asIntegerArray(serverResult.WLN).filter(value => value >= 1);
        let totalCreditWon = this.calculateTotalCreditWin(serverResult);
        let totalSuperBetWon = 0;
        let numFreeGamesWon = 0;
        let numFreeSpinsWon = 0;
        let progressiveMultiplier = 1;
        let spinType = 1;
        let specialWins = [];
        let hasBonusWinPerHand = Parse.asBooleanArray(serverResult.PBN);
        let stickySymbolsMap = this._spinUtils.generateBlankMap();
        let handsWithBonusWin = [];
        
        {
            let handsWithWin = [];

            hasBonusWinPerHand.forEach((handHasBonusWin, handIndex) => {
                if (handHasBonusWin)
                {
                    let handId = handIndex + 1;

                    // Generate map of where the bonus symbols are
                    let winningReels = [], winningPositions = [];
                    let symbolsOnHand = finalSymbolIds[handIndex];
                    let winningPositionsMap = this._spinUtils.generateBlankSingleHandMap();

                    for (let reelIndex = 0; reelIndex < symbolsOnHand.length; reelIndex ++) {
                        let symbolsOnReel = symbolsOnHand[reelIndex];
                        for (let posIndex = 0; posIndex < symbolsOnReel.length; posIndex ++) {
                            if (symbolsOnReel[posIndex].type == C_SymbolType.SCATTER) {
                                winningReels.push(reelIndex + 1);       // TODO: Remove ?
                                winningPositions.push(posIndex + 1);    // TODO: Remove ?
                                winningPositionsMap[reelIndex][posIndex] = true;
                            }
                        }
                    }

                    handsWithWin.push(handId);
                    symbolWins.push({
                        handId,
                        lineId : 0,
                        rewardGroup : C_RewardGroup.BONUS,
                        rewardType : 1,
                        rewardValue : 0,
                        winningSymbol : this._bonusSymbol,
                        winningReels,
                        winningPositions,
                        winningPositionsMap
                    });

                    handsWithBonusWin.push(handId);
                }
            });
        }

        let hasBonusWin = handsWithBonusWin.length > 0;
        let hasAnyWin = hasBonusWin || symbolWins.length > 0;

        /** @type {SpinResult} */
        let spinResult =
        {
            finalSymbolIds,
            hasAnyWin,
            totalCreditWon,
            totalSuperBetWon,
            numFreeGamesWon,
            numFreeSpinsWon,
            progressiveMultiplier,
            spinType,
            hasBonusWin,
            symbolWins,
            specialWins,
            stickySymbolsMap
        };

        if (serverResult.MTP === MessageTypes.SINGLE_SPIN_PHASE_RESULT) {
            /** @type {SingleSpinPhaseResult} */
            let spinPhaseResult =
            {
                totalCreditWon,
                totalSuperBetWon,
                numFreeGamesWon,
                numFreeSpinsWon,
                hasBonusWin,
                handsWithBonusWin,
                hasBonusWinPerHand,
                rounds : [{
                    totalCreditWon,
                    totalSuperBetWon,
                    numFreeGamesWon,
                    spins : [spinResult]
                }]
            };

            return spinPhaseResult;
        }
        else
        if (serverResult.MTP === MessageTypes.SPIN_1_PHASE_RESULT) {
            /** @type {Gen1Spin1PhaseResultsReply} */
            let spin1ServerResult = serverResult;
            let suggestedAutoholds = this.extractHoldsPattern(spin1ServerResult.AHD);
            let playSpin2 = !hasAnyWin;

            /** @type {Spin1PhaseResult} */
            let spin1PhaseResult = {
                totalCreditWon,
                totalSuperBetWon,
                numFreeGamesWon,
                numFreeSpinsWon,
                hasBonusWin,
                handsWithBonusWin,
                hasBonusWinPerHand,
                rounds : [{
                    totalCreditWon,
                    totalSuperBetWon,
                    numFreeGamesWon,
                    spins : [spinResult]
                }],
                suggestedAutoholds,
                playSpin2
            }

            return spin1PhaseResult;
        }
        else
        if (serverResult.MTP === MessageTypes.SPIN_2_PHASE_RESULT) {
            /** @type {Gen1Spin2PhaseResultsReply} */
            let spin2ServerResult = serverResult;
            let startSymbolIds = this.extractSymbols(startReelPositions);
            let holdsUsed = this.extractHoldsPattern(spin2ServerResult.AHD);

            /** @type {Spin2PhaseResult} */
            let spin2PhaseResult = {
                totalCreditWon,
                totalSuperBetWon,
                numFreeGamesWon,
                numFreeSpinsWon,
                hasBonusWin,
                handsWithBonusWin,
                hasBonusWinPerHand,
                rounds : [{
                    totalCreditWon,
                    totalSuperBetWon,
                    numFreeGamesWon,
                    spins : [spinResult]
                }],
                startSymbolIds,
                holdsUsed
            }

            return spin2PhaseResult;
        }
        else
        {
            console.error(`Unrecognised MTP field from Gen1 Spin Phase Result (got ${serverResult.MTP})`);
        }
    };


    /**
     * Extracts a set of reel symbol parameters, into a Symbols Matrix object.
     * @public
     * @param {number[]} symbolsData 
     * @param {SymbolsMatrix} [symbolsMatrix]
     * An optional SymbolsMatrix instance - if passed in, it will be re-used
     * and returned. Otherwise, a new instance will be created.
     * @return {SymbolsMatrix}
     */
    extractSymbols(symbolsData, symbolsMatrix=null) {
        if (!symbolsMatrix) {
            symbolsMatrix = this.createSymbolsMatrix();
        }

        // Armed with the symbolsData, we can now parse that information into
        // the start and final SymbolsMatrices. However, we DO need a dynamic
        // mechanism to achieve this (some games will return lists of actual
        // symbol ids, others return reel positions. And some have multiple hands..)
        if (this._slotConfig.reelbands) {
            let numHands = this._slotConfig.numHands;
            let numReels = this._slotConfig.numReels;
            for (let handIndex = 0; handIndex < numHands; handIndex ++) {
                let handSymbols = symbolsMatrix[handIndex];
                for (let reelIndex = 0; reelIndex < this._slotConfig.numReels; reelIndex ++) {
                    let reelSymbols = handSymbols[reelIndex];
                    let numSymOnReel = this._slotConfig.numSymsPerReel[reelIndex];
                    let reelBand = this._slotConfig.reelbands[reelIndex];
                    let numSymInReelBand = reelBand.length;
                    let reelPos = symbolsData[(handIndex * numReels) + reelIndex];
                    for (let symIndex = 0; symIndex < numSymOnReel; symIndex ++) {
                        let posIndex = (reelPos + symIndex) % numSymInReelBand;
                        let symbolId = reelBand[posIndex];
                        let symConfig = this._slotConfig.symbols[symbolId];
                        reelSymbols[symIndex] = symConfig;
                    }
                }
            }
        }
        else {
            // If no reelbands, treat the message as a Big Ghoulies message.
            // Here, its structured as 
            // [ R1.Pos1, R2.Pos1, R3.Pos1, R4.Pos1, R5.Pos1,
            //   R1.Pos2, R2.Pos2, R3.Pos2, R4.Pos2, R5.Pos2,
            //   R1.Pos3, R2.Pos3, R3.Pos3, R4.Pos3, R5.Pos3 ]
            // Now, there is no formal definition yet of what "variable num sym per reel" means for this
            // old results model, and there never will be. Therefore, we take the first value from
            // SlotConfig.numSymPerReel, and treat it as the value to use for all reels.
            let totalIndex = 0;
            let numSymPerReel = this._slotConfig.numSymsPerReel[0];
            for (let handIndex = 0; handIndex < this._slotConfig.numHands; handIndex ++) {
                for (let posIndex = 0; posIndex < numSymPerReel; posIndex ++) {    
                    for (let reelIndex = 0; reelIndex < this._slotConfig.numReels; reelIndex ++) {
                        let symAtPos = symbolsData[totalIndex];
                        let symConfig = this._slotConfig.symbols[symAtPos];
                        symbolsMatrix[handIndex][reelIndex][posIndex] = symConfig;
                        totalIndex ++;
                    }
                }
            }
        }

        return symbolsMatrix;
    };


    /**
     * Creates and returns a new symbols array, according to the data which configures
     * what our slot field looks like. The symbols will be randomized for each reel,
     * using a mechanism for random generation that is... appropriate??.. for the game.
     * @public
     * @return {SymbolsMatrix}
     */
    createSymbolsMatrix()
    {
        let symbols = [];

        // Creates a symbols matrix, initialized to a set of starting
        // symbols. For fixed reelbands, we just choose position 0 for
        // each reel. For non-fixed reelbands, we select some symbols
        // at random (ignoring any special symbols).
        for (let handIndex = 0; handIndex < this._slotConfig.numHands; handIndex ++) {
            let handSymbols = [];
            for (let reelIndex = 0; reelIndex < this._slotConfig.numReels; reelIndex ++) {
                let reelSymbols = [];
                let numSymbolsOnReel = this._slotConfig.numSymsPerReel[reelIndex];
                for (let symIndex = 0; symIndex < numSymbolsOnReel; symIndex ++) {
                    if (this._slotConfig.reelbands) {
                        let reelBand = this._slotConfig.reelbands[reelIndex];
                        let reelPos = 0;
                        let reelSymIndex = (reelPos + symIndex) % numSymbolsOnReel
                        let symbolId = reelBand[reelSymIndex];
                        let symConfig = this._slotConfig.symbols[symbolId];
                        reelSymbols.push(symConfig);
                    }
                    else {
                        // TODO:
                        // Determine where the symbol id is selected.
                        // We might want to adopt a special rule here?
                        reelSymbols.push(this._slotConfig.symbols[1]);
                    }
                }
                handSymbols.push(reelSymbols);
            }
            symbols.push(handSymbols);
        }

        return symbols;
    };


    /**
     * Parses a piece of data as a "Hold Reels" holds pattern (which is the only type of holds
     * pattern that we need for V1 platform games)
     * @private
     * @param {*} data 
     * @return {HoldsPattern}
     */
    extractHoldsPattern(data) {
        let numHands = this._slotConfig.numHands;
        let numReelsPerHand = this._slotConfig.numReels;
        return Parse.asHoldReelsPattern(data, numHands, numReelsPerHand);
    };


    /**
     * Extracts WinlineWins from a server result.
     * @private
     * @param {Gen1SpinPhaseResultsReply} serverResult
     * @return {SymbolWin[]}
     */
    extractSymbolWinsData(serverResult)
    {
        let wins = [];
        let winlineIds = Parse.asIntegerArray(serverResult.WLN);
        let winlineNumSym = Parse.asIntegerArray(serverResult.WSL);
        let winlineCreditWin = Parse.asIntegerArray(serverResult.WLV);
        let winlineHand;
        if ("HWL" in serverResult) {
            winlineHand = Parse.asIntegerArray(serverResult.HWL);
        }

        // Unfortunately, even if there are no wins, this field usually
        // contains a single 0 value, eg: [0] means "no win"
        let hasWins = winlineNumSym.length > 0 && winlineNumSym[0] !== 0;

        if (hasWins)
        {
            let finalSymbols = this.extractSymbols(Parse.asIntegerArray(serverResult.RNR));
            let numWins = winlineIds.length;
            for (let i = 0; i < numWins; i ++)
            {
                let lineId = winlineIds[i];
                let rewardGroup = C_RewardGroup.CREDIT;
                let rewardType = 1; // TODO: Change into constant reference!!!
                let rewardValue = winlineCreditWin[i];
                let numWinningSym = winlineNumSym[i];
                let handId = winlineHand? winlineHand[i] : 1;
                let handIndex = handId - 1;
                let winline = this._slotConfig.winlines[lineId];
                let winningSymbol = finalSymbols[handIndex][0][winline.positions[0]];   // pick first symbol on line as winning
                let winningReels = [];
                let winningPositions = [];
                let winningPositionsMap = this._spinUtils.generateBlankSingleHandMap();
                let numWinningReels = 0;
                let indexedWins = {};
                
                for (let reelIndex = 0; reelIndex < numWinningSym; reelIndex ++) {
                    let reelId = reelIndex + 1;
                    let winningPosIndex = winline.positions[reelIndex];
                    let winningPosition = winningPosIndex + 1

                    if (winningReels.indexOf(reelId) === -1) {
                        numWinningReels += 1;
                    }

                    winningReels.push(reelId);

                    // NOTE: winline contains posIndex, we make it into posId
                    // (for now...)
                    winningPositions.push(winningPosition);  

                    winningPositionsMap[reelIndex][winningPosIndex] = true;
                }

                /**
                 * @type {SymbolWin}
                 */
                let symbolWin =
                {
                    lineId,
                    handId,
                    rewardGroup,
                    rewardType,
                    rewardValue,
                    winningSymbol,
                    winningReels,
                    winningPositions,
                    winningPositionsMap
                }

                wins.push(symbolWin);
            }

            this.sortWinlinesFromLowestToLargest(wins);
        }

        return wins;
    };


    /**
     * Sorts an array of Winline Wins, ensuring that it starts with lowest value
     * win, and ends with highest value win. In this older results model, there
     * are only Credit wins - so this is all we have to check for.
     * @private
     * @param {SymbolWin[]} winlineWins 
     */
    sortWinlinesFromLowestToLargest(winlineWins)
    {
        winlineWins.sort((win1, win2) => {
            if (win1.rewardValue != win2.rewardValue) {
                return win1.rewardValue - win2.rewardValue;
            }
            else return win1.winningSymbol.id - win2.winningSymbol.id;
        });
    };


    /**
     * Calculates the Total Credit Won for a Spin Phase Result. The older generation 1
     * results model only supports credit wins, so this is the only calculation that we
     * need to perform. We basically just read the list of Credit Values for any winline
     * wins that exist, and add them up. This works fine, even in the event that we get
     * a "no wins" value in this field (eg: the value in the field is equivalent to [0]).
     * @private
     * @param {Gen1SpinPhaseResultsReply} serverResult
     * @return {number}
     */
    calculateTotalCreditWin(serverResult)
    {
        let allCreditWins = Parse.asIntegerArray(serverResult.WLV);
        let totalCreditWin = 0;
        if (allCreditWins.length > 0) {
            totalCreditWin = allCreditWins.reduce((a,b)=>a+b);
        }
        return totalCreditWin;
    };
}