import { GamePhaseResultModel } from "./GamePhaseResultModel";

class Gen2GamblePhaseModel extends GamePhaseResultModel
{
    /**
     * 
     * @param {Dependencies} dependencies 
     */
    constructor(dependencies)
    {
        super();
    };

    /**
     * @public
     * @param {Gen2GamblePhaseResultsServiceResponse} serverResult 
     */
    setPhaseResults(serverResult)
    {
        super.setPhaseResults(serverResult);
        
        // TODO: start to work on the gamble phase...
    }
}