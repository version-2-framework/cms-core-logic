/**
 * @type {Object.<number, LicenseeData>}
 */
let map =
{
    1 :
    {
        presentationName : "MerkurWin",
        commsName : "merkurwin"
    },

    2 :
    {
        presentationName : "Vincitu",
        commsName : "vincitu"
    },

    3 :
    {
        presentationName : "Betium",
        commsName : "betium"
    },

    4 :
    {
        presentationName : "Sisal",
        commsName : "sisal"
    },

    5 : 
    {
        presentationName : "Giochi24",
        commsName : "giochi24"
    },

    6 :
    {
        presentationName : "Eurobet",
        commsName : "eurobet"
    },

    7 :
    {
        presentationName : "Snai",
        commsName : "snai"
    },

    8 :
    {
        presentationName : "Lottomatica",
        commsName : "lottomatica"
    },

    9 :
    {
        presentationName : "WilliamHill",
        commsName : "williamHill"
    },

    10 :
    {
        presentationName : "Intralot",
        commsName : "intralot"
    },

    11 : 
    {
        presentationName : "Goldbet",
        commsName : "goldbet"
    },

    12 : 
    {
        presentationName : "Greentube",
        commsName : "greentube"
    },

    13 :
    {
        presentationName : "Betpoint",
        commsName : "betpoint"
    },

    14 :
    {
        presentationName : "Hit",
        commsName : "hit"
    },

    15 :
    {
        presentationName : "Bet365",
        commsName : "bet365"
    },

    16 :
    {
        presentationName : "Betflag",
        commsName : "betflag"
    }
};

/**
 * @type {LicenseeData}
 */
const DEFAULT_LICENSEE_DATA =
{
    presentationName : "Default Client",
    commsName : ""
}

/**
 * 
 * @param {number} licenseeId 
 * @return {LicenseeData}
 */
export function getDataFor(licenseeId)
{
    return (licenseeId && licenseeId in map)? map[licenseeId] : DEFAULT_LICENSEE_DATA;
}

/**
 * Returns the comms name to use for the gmae client, based on licensee id.
 * @param {number} licenseeId
 * @return {string}
 */
export function getCommsIdForLicenseeId(licenseeId)
{
    return getDataFor(licenseeId).commsName;
}