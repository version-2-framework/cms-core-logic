import * as ResponseType from "../const/C_ServiceResponseTypeGen2";
import {SpinModelUtils} from './SpinModelUtils';
import * as LogManager from "../logging/LogManager";

const log = LogManager.getCommsLogger();

// This is an example of a Result Parser - we would maintain implementations for the
// older games (we can actually deprecate some configuration variables from SlotConfig
// this way, and simply have 4 different implementations).
// The actual code is probably going to get a little convoluted at first - we need to
// stick with it!
class Gen2SpinResultParser
{
    /**
     * 
     * @param {Dependencies} deps 
     */
    constructor(deps)
    {
        /**
         * @private
         * @type {SlotConfig}
         */
        this._slotConfig = deps.config;

        /**
         * @private
         * @type {SpinModelUtils}
         */
        this._spinUtils = deps.spinModelUtils;

        /**
         * The number of symbols per reel - from the point of view of symbol parsing.
         * @private
         * @type {number[]}
         */
        this._numSymsPerReel = this._slotConfig.numSymsPerReelInData ?
            this._slotConfig.numSymsPerReelInData : this._slotConfig.numSymsPerReel;

        log.debug(`Gen2SpinResultParser: numSymsPerReel = ${this._numSymsPerReel}`);

        // TODO: better name ??
        /**
         * Map which lets us determine reel / position within reel, from a single "symbol
         * position" as the server returns (we get a list of positions per hand, but its
         * not semantically broken down by reel, which we actually need). This works, but
         * is rather a heavy-handed way of dealing with things (or will be in the future,
         * if we have a game with a huge number of symbols!). So maybe a more elegant
         * solution could appear (but maybe not).
         * 
         * The key is the "server returned" symbol position, and the value is a struct,
         * indicating both reel and position within reel.
         * @private
         */
        this._symbolPosLookupTable = (()=>{
            let symbolPosTable = [];
            for (let reelIndex = 0; reelIndex < this._slotConfig.numReels; reelIndex ++) {
                let numSymForReel = this._numSymsPerReel[reelIndex];
                for (let posIndex = 0; posIndex < numSymForReel; posIndex ++) {
                    symbolPosTable.push({
                        reelId : reelIndex + 1,
                        posId : posIndex + 1
                    });
                }
            }
            return symbolPosTable;
        })();
    };

    /**
     * @public
     * @param {Gen2SlotGamePhaseResultsReply} serverResult
     * @return {spinPhaseResult}
     */
    parseServerResult(serverResult)
    {
        let messageType = serverResult.messageType;
        let spinPhaseResult;
        if (messageType === ResponseType.SINGLE_SPIN_PHASE_RESULTS) {
            // @ts-ignore - We checked the MessageType, so this interface up-cast is (supposedly) fine
            return this.parseSingleSpinPhaseResult(serverResult.result);
        }
        else
        if (messageType === ResponseType.SPIN_1_PHASE_RESULTS) {
            // @ts-ignore - We checked the MessageType, so this interface up-cast is (supposedly) fine
            return this.parseSpin1PhaseResult(serverResult.result);
        }
        else
        if (messageType === ResponseType.SPIN_2_PHASE_RESULTS)
        {
            // @ts-ignore - We checked the MessageType, so this interface up-cast is (supposedly) fine
            return this.parseSpin2PhaseResult(serverResult.result);
        }
        else
        if (messageType === ResponseType.FREESPIN_PHASE_RESULTS) {
            // @ts-ignore - We checked the MessageType, so this interface up-cast is (supposedly) fine
            return this.parseFreeSpinPhaseResult(serverResult.result);
        }
        else
        if (messageType === ResponseType.QUICK_PHASE_RESULTS)
        {
            // @ts-ignore - We checked the MessageType, so this interface up-cast is (supposedly) fine
            return this.parseQuickPhaseResult(serverResult.result);
        }
        else
        {
            log.error(`Gen2SpinResultParser: Cannot parse message of type ${messageType}: expected a Spin Phase Result message`);
        }
        return spinPhaseResult;
    };


    /**
     * @public
     * @param {Gen2SingleSpinPhaseResult} serverResult
     * @return {SingleSpinPhaseResult}
     */
    parseSingleSpinPhaseResult(serverResult)
    {
        /** @type {SingleSpinPhaseResult} */
        let spinPhaseResult =
        {
            totalCreditWon : serverResult.totalCreditWon,
            totalSuperBetWon : serverResult.totalSuperBetWon,
            numFreeGamesWon : serverResult.numFreeGamesWon,
            numFreeSpinsWon : serverResult.numFreeSpinsWon,
            handsWithBonusWin : serverResult.handsWithBonusWin,
            hasBonusWin : serverResult.handsWithBonusWin.length > 0,
            hasBonusWinPerHand : this.extractHasBonusWinPerHand(serverResult.handsWithBonusWin),
            rounds : this.extractSpinPhaseRoundResults(serverResult)
        };

        return spinPhaseResult;
    };


    /**
     * @public
     * @param {Gen2Spin1PhaseResult} serverResult 
     * @return {Spin1PhaseResult}
     */
    parseSpin1PhaseResult(serverResult)
    {
        /** @type {Spin1PhaseResult} */
        let spinPhaseResult =
        {
            totalCreditWon : serverResult.totalCreditWon,
            totalSuperBetWon : serverResult.totalSuperBetWon,
            numFreeGamesWon : serverResult.numFreeGamesWon,
            numFreeSpinsWon : serverResult.numFreeSpinsWon,
            playSpin2 : serverResult.playSpin2,
            handsWithBonusWin : serverResult.handsWithBonusWin,
            hasBonusWin : serverResult.handsWithBonusWin.length > 0,
            hasBonusWinPerHand : this.extractHasBonusWinPerHand(serverResult.handsWithBonusWin),
            rounds : this.extractSpinPhaseRoundResults(serverResult),
            suggestedAutoholds : this.extractHoldsPattern(serverResult.autoholdsPattern)
        };

        return spinPhaseResult;
    };




    /**
     * @public
     * @param {Gen2Spin2PhaseResult} serverResult 
     * @return {Spin2PhaseResult}
     */
    parseSpin2PhaseResult(serverResult)
    {
        /** @type {Spin2PhaseResult} */
        let spinPhaseResult =
        {
            totalCreditWon : serverResult.totalCreditWon,
            totalSuperBetWon : serverResult.totalSuperBetWon,
            numFreeGamesWon : serverResult.numFreeGamesWon,
            numFreeSpinsWon : serverResult.numFreeSpinsWon,
            handsWithBonusWin : serverResult.handsWithBonusWin,
            hasBonusWin : serverResult.handsWithBonusWin.length > 0,
            hasBonusWinPerHand : this.extractHasBonusWinPerHand(serverResult.handsWithBonusWin),
            rounds : this.extractSpinPhaseRoundResults(serverResult),
            holdsUsed : this.extractHoldsPattern(serverResult.holdsPatternUsed),
            startSymbolIds : this.extractSymbols(serverResult.startSymbolIds)
        };

        return spinPhaseResult;
    };


    /**
     * @public
     * @param {Gen2FreeSpinPhaseResult} serverResult 
     * @return {FreeSpinPhaseResult}
     */
    parseFreeSpinPhaseResult(serverResult)
    {
        /** @type {FreeSpinPhaseResult} */
        let spinPhaseResult =
        {
            totalCreditWon : serverResult.totalCreditWon,
            totalSuperBetWon : serverResult.totalSuperBetWon,
            numFreeGamesWon : serverResult.numFreeGamesWon,
            freeSpinType : serverResult.freeSpinType,
            rounds : this.extractFreeSpinPhaseRoundResults(serverResult)
        };

        return spinPhaseResult;
    };

    parseQuickPhaseResult(serverResult)
    {
        let quickSpinPhaseResult = 
        {
            totalCreditWon: serverResult.totalCreditWon,
            totalSuperBetWon : serverResult.totalSuperBetWon,
            numFreeGamesWon : serverResult.numFreeGamesWon,
            winAmounts: serverResult.winAmounts,
            handsWithBonusWin : serverResult.handsWithBonusWin,
            hasBonusWin : serverResult.handsWithBonusWin.length > 0,
            bonusPosition: serverResult.bonusPosition,
            hasBonusWinPerHand : this.extractHasBonusWinPerHand(serverResult.handsWithBonusWin),
        }

        return quickSpinPhaseResult;
        
    }


    /**
     * Generates a list of Round Results for any Spin phase.
     * @private
     * @param {Gen2SpinPhaseResult} spinPhaseResult
     * @return {SpinRoundResult[]}
     */
    extractSpinPhaseRoundResults(spinPhaseResult)
    {
        let spins = spinPhaseResult.spins;
        
        /** @type {SpinRoundResult} */
        let spinRoundResultModel =
        {
            totalCreditWon : spinPhaseResult.totalCreditWon,
            totalSuperBetWon : spinPhaseResult.totalSuperBetWon,
            numFreeGamesWon : spinPhaseResult.numFreeGamesWon,
            spins : this.parseSpinResults(spins)
        };

        return [spinRoundResultModel];
    };


    /**
     * Generates a list of Round Results for the FreeSpin phase.
     * @private
     * @param {Gen2FreeSpinPhaseResult} serverResult 
     * @return {SpinRoundResult[]}
     */
    extractFreeSpinPhaseRoundResults(serverResult)
    {
        let rounds = serverResult.rounds;
        let numSpins = serverResult.rounds.length;
        let parsedRoundResults = [];

        rounds.forEach(roundResult => {
            let spins = roundResult.spins;

            /** @type {SpinRoundResult} */
            let parsedRoundResult =
            {
                totalCreditWon : roundResult.totalCreditWon,
                totalSuperBetWon : roundResult.totalSuperBetWon,
                numFreeGamesWon : roundResult.numFreeGamesWon,
                spins : this.parseSpinResults(spins)
            };

            parsedRoundResults.push(parsedRoundResult);
        });

        return parsedRoundResults;
    };


    /**
     * Generates a sorted list of Winning Line ids for a given Spin Result. Any SymbolWin
     * with lineId of 0 will be ignored (and this is not counted as a Winline win). The
     * list returned will be sorted by value (ordered from smallest lineId to largest).
     * @private
     * @param {Gen2SpinResult} spinResult
     * @param {number} [targetRewardType]
     * The target rewardType to search for. If passed in, only lineIds with
     * the target rewardType will be returned.
     * @return {number[]}
     */
    findWinningLineIds(spinResult, targetRewardType=null)
    {
        let filterByRewardType = targetRewardType !== null;
        let winningLineIdsMap = {};
        let winningLineIds = [];

        if (spinResult.symbolWins) {
            spinResult.symbolWins.forEach(symbolWin => {
                if (!filterByRewardType ||
                    filterByRewardType && symbolWin.rewardType === targetRewardType)
                {
                    let lineId = symbolWin.lineId;
                    let lineIdAlreadyFound = lineId in winningLineIdsMap;
                    if (lineId > 0 && !lineIdAlreadyFound) {
                        winningLineIdsMap[lineId] = true;
                        winningLineIds.push(lineId);
                    }
                }
            });
        }

        winningLineIds = winningLineIds.sort((val1,val2) => val1 - val2);
        
        return winningLineIds;
    };


    /**
     * Parses a list of server delivered SpinResults, and returns a list of client SpinResults.
     * @private
     * @param {Gen2SpinResult[]} spinResults 
     * @return {SpinResult[]}
     */
    parseSpinResults(spinResults)
    {
        let parsedSpinResults = [];
        spinResults.forEach(spinResult => {
            parsedSpinResults.push(this.parseSpinResult(spinResult));
        });
        return parsedSpinResults;
    };


    /**
     * Parses a server delivered SpinResult, into the client SpinResult type.
     * @private
     * @param {Gen2SpinResult} serverSpinResult
     * @return {SpinResult}
     */
    parseSpinResult(serverSpinResult)
    {
        let hasBonusWin = false;    // TODO: I think we can actually remove this from SpinResult api

        // TODO: this might get returned by the server...
        let hasAnyWin = serverSpinResult.symbolWins.length > 0 || serverSpinResult.specialWins.length > 0;

        let stickySymbolsMap = this.generateStickySymbolsMap(serverSpinResult.stickySymbolPositions);
        
        /**
         * @type {SpinResult}
         */
        let spinResultModel =
        {
            finalSymbolIds : this.extractSymbols(serverSpinResult.symbolIds),
            totalCreditWon : serverSpinResult.totalCreditWon,
            totalSuperBetWon : serverSpinResult.totalSuperBetWon,
            numFreeGamesWon : serverSpinResult.numFreeGamesWon,
            numFreeSpinsWon : serverSpinResult.numFreeSpinsWon,
            spinType : serverSpinResult.spinType,
            progressiveMultiplier : serverSpinResult.progressiveMultiplier,
            hasAnyWin,
            hasBonusWin,
            symbolWins : this.extractAllSymbolWins(serverSpinResult),
            specialWins : this.extractAllSpecialWins(serverSpinResult),
            stickySymbolsMap
        };
        
        return spinResultModel;
    };


    /**
     * Parses a set of sticky symbol positions (as returned by the Gen2 server) into a multi
     * hand map, representing for each symbol whether it is sticky or not.
     * @param {number[][]} stickySymbolPositions
     * Original Gen2 server map of sticky symbol positions. The outer array is "per hand data",
     * the inner array for each hand, is a list of indexed positions which are sticky.
     * @return {boolean[][][]}
     */
    generateStickySymbolsMap(stickySymbolPositions)
    {
        let stickySymbolsMap = this._spinUtils.generateBlankMap();

        stickySymbolPositions.forEach((handData, handIndex) => {
            handData.forEach(stickyPosIndex => {
                let symPosData = this._symbolPosLookupTable[stickyPosIndex];
                let reelIndex = symPosData.reelId - 1;
                let posIndex = symPosData.posId - 1;
                stickySymbolsMap[handIndex][reelIndex][posIndex] = true;
            });
        });

        return stickySymbolsMap;
    };


    /**
     * @private
     * @param {Gen2SpinResult} spinResult 
     * @return {SymbolWin[]}
     */
    extractAllSymbolWins(spinResult)
    {
        /** @type {SymbolWin[]} */
        let parsedWins = [];

        spinResult.symbolWins.forEach(srcWin => {
            let parsedWin = this.parseSymbolWin(srcWin);
            parsedWins.push(parsedWin);
        });

        return parsedWins;
    };


    /**
     * @private
     * @param {Gen2SymbolWin} symbolWin 
     * @return {SymbolWin}
     */
    parseSymbolWin(symbolWin)
    {
        let winningPositions = [];
        let winningReels = [];
        let winningPositionsMap = this._spinUtils.generateBlankSingleHandMap();
        
        if (symbolWin.winningPositions)
        {
            symbolWin.winningPositions.forEach(winningPos =>
            {
                let symPosData = this._symbolPosLookupTable[winningPos];

                // TODO: Deprecate these 2
                winningPositions.push(symPosData.posId);
                winningReels.push(symPosData.reelId);

                winningPositionsMap[symPosData.reelId - 1][symPosData.posId - 1] = true;
            });
        }

        /** @type {SymbolWin} */
        let parsedSymbolWin =
        {
            rewardGroup : symbolWin.rewardGroup,
            rewardType : symbolWin.rewardType,
            rewardValue : symbolWin.rewardValue,
            handId : symbolWin.handId,
            lineId : symbolWin.lineId,
            winningSymbol : this._slotConfig.symbols[symbolWin.symbolId],
            winningPositions ,
            winningReels,
            winningPositionsMap,
            winGroup : symbolWin.winGroup,
            winMechanic : symbolWin.winMechanic,
            winSize : symbolWin.winSize,
            numWins : symbolWin.numWins,
            specialSymbols : symbolWin.specialSymbols
        };

        return parsedSymbolWin;
    };


    /**
     * @private
     * @param {Gen2SpinResult} spinResult 
     * @return {SpecialWin[]}
     */
    extractAllSpecialWins(spinResult)
    {
        /** @type {SpecialWin[]} */
        let parsedWins = [];

        spinResult.specialWins.forEach(serverSpecialWin => {
            parsedWins.push(this.parseSpecialWin(serverSpecialWin));
        });

        return parsedWins;
    };


    /**
     * @private
     * @param {Gen2SpecialWin} srcWin
     * @return SpecialWin
     */
    parseSpecialWin(srcWin)
    {
        /** @type {SpecialWin} */
        let parsedWin =
        {
            rewardGroup : srcWin.rewardGroup,
            rewardType : srcWin.rewardType,
            rewardValue : srcWin.rewardValue,
            winType : srcWin.winType
        };

        return parsedWin;
    };


    /**
     * Takes a list of Symbol Ids, and returns a broken down matrix of Symbol(Config) objects.
     * @public
     * @param {number[][]} symbolIds 
     * @return {SymbolConfig[][][]}
     */
    extractSymbols(symbolIds)
    {
        let numHands = this._slotConfig.numHands;
        let numReels = this._slotConfig.numReels;
        let numSymsPerReel = this._numSymsPerReel;
        let parsedSymbols = [];

        for (let handIndex = 0; handIndex < numHands; handIndex ++) {
            let sourceHandSymbols = symbolIds[handIndex];
            let parsedHandSymbols = [];
            let symIndexForHand = 0;
            for (let reelIndex = 0; reelIndex < numReels; reelIndex ++) {
                let reelSymbols = [];
                let numSymOnReel = numSymsPerReel[reelIndex];
                for (let symIndex = 0; symIndex < numSymOnReel; symIndex ++) {
                    let symbolId = sourceHandSymbols[symIndexForHand];
                    let symbol = this._slotConfig.symbols[symbolId];
                    reelSymbols.push(symbol);
                    symIndexForHand += 1;
                }
                parsedHandSymbols.push(reelSymbols);
            }
            parsedSymbols.push(parsedHandSymbols);
        }

        return parsedSymbols;
    };


    /**
     * @private
     * @param {number[]} handsWithBonusWin 
     * @return {boolean[]}
     */
    extractHasBonusWinPerHand(handsWithBonusWin) {
        let hasWinPerHand = [];
        let numHands = this._slotConfig.numHands;
        for (let handId = 1; handId <= numHands; handId ++) {
            let hasWin = handsWithBonusWin.indexOf(handId) > -1;
            hasWinPerHand.push(hasWin);
        }
        return hasWinPerHand;
    };


    /**
     * Parses a raw holds pattern into something more useful for the game.
     * @public
     * @param {boolean[][]} rawHoldsPattern
     * @return {HoldsPattern}
     */
    extractHoldsPattern(rawHoldsPattern)
    {
        if (this._slotConfig.holdsMode === "holdReels") {
            return this.extractHoldReelsPattern(rawHoldsPattern);
        }
        else
        if (this._slotConfig.holdsMode === "holdSymbols") {
            return this.extractHoldSymbolsPattern(rawHoldsPattern);
        }
    };


    /**
     * @private
     * @param {boolean[][]} rawHoldsPattern 
     * @return {HoldReelsPattern}
     */
    extractHoldReelsPattern(rawHoldsPattern) {
        return {
            type : "holdReels",
            pattern : rawHoldsPattern
        };
    };


    /**
     * @private
     * @param {boolean[][]} rawHoldsPattern 
     * @return {HoldSymbolsPattern}
     */
    extractHoldSymbolsPattern(rawHoldsPattern) {
        let parsedPattern = this._spinUtils.generateBlankMap();

        this._spinUtils.symbolsIterator((handIndex, reelIndex, symIndex, handPosIndex) => {
            let isHeld = rawHoldsPattern[handIndex][handPosIndex];
            parsedPattern[handIndex][reelIndex][symIndex] = isHeld;
        });

        return {
            type : "holdSymbols",
            pattern : parsedPattern
        };
    }
}

export {Gen2SpinResultParser};