/**
 * @todo:
 * This could definitely become "HistoryViewModel", and it could support information
 * such as "currently open game" fields.
 */
export class HistoryModel
{
    constructor()
    {
        /**
         * A list of all games that we have history for.
         * @private
         * @type {GameHistoryModel[]}
         */
        this._games = [];
    }

    /**
     * Processes a History Response message, filling out the model data.
     * @public
     * @param {Object} msg
     */
    processResults(msg)
    {
        // Clear out any cached games
        this._games.length = 0;

        // We are going to check all fields on the message. We are looking
        // for any that start with GAME eg GAME1, GAME2 etc
        for (let key in msg)
        {
            if (key.startsWith("GAME"))
            {
                let gameNumber = parseInt(key.split("GAME")[1]);
                let gameData = msg[key];

                // TODO:
                // Needs manufacturing ( from our own GameFactory? ),
                // because the implementation is going to change, from
                // game to game.
                let gameHistoryModel = new GameHistoryModel(gameNumber, gameData);
                this._games.push(gameHistoryModel);
            }
        }

        // Finally, sort the games into numerical order.
        this._games.sort((game1, game2) => game1.id > game2.id ? 1  : -1);
    }
}

// TODO:
// We will need an overridden implementation of GameHistoryModel for different games.
// Now that we are looking to standardize how the messaging works, we could increasingly
// rely on a generic implementation ( and only needing custom overrides to support old 
// legacy game clients ). However, its clear that old games are very very different.
// Even the bet settings change. And WMG will not let me make history more generic -
// I tried this with Big Ghoulies, and was basically told to completely revert it to how
// it was before (unfortunately, this meant taking the really simple, elegant text fields
// i had designed, and adding 4x as much information, then tripling its size on screen....)
export class GameHistoryModel
{
    /**
     * Creates a new GameHistoryModel object.
     * @param {number} id 
     * @param {Object} data 
     */
    constructor(id, data)
    {
        this.id = id;
        this.data = data;

        this.parseData(data);
    }

    parseData(data)
    {

    }
}