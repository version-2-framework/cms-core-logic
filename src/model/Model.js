///<reference path='../types/types.d.ts'/>

import C_SessionType from "../const/C_SessionType";
import C_Currency from "../const/C_Currency";
import formatCoin from "../currency/formatCoin";
import formatPercentage from "../utils/formatPercentage";
import C_CommsMode from "../const/C_CommsMode";
import C_GameEvent from "../const/C_GameEvent";
import * as C_CurrencyType from "../const/C_CurrencyType";
import * as Country from "../const/C_Country";
import * as Parse from '../utils/ParseUtil';
import * as StringUtil from '../utils/StringUtil';
import * as C_MessageField from "../const/C_MessageField";
import * as LogManager from "../logging/LogManager";
import { ConfigurableCurrencyFormatter } from "../currency/ConfigurableCurrencyFormatter";
import * as C_Client_Environment_Value from "../const/C_ClientEnvironmentValues";

/**
 * Default value of Session Inactivity Timeout (in minutes).
 * This will be used when we need to set a value, but no value
 * was returned by the server.
 */
const DEFAULT_SESSION_INACTIVITY_TIMEOUT = 20;

const log = LogManager.getLogger('GameModel');

/**
 * @implements {ModelApi}
 */
class Model
{
    /**
     * Creates a new instance of the Model object.
     * @param {Dependencies} dependencies 
     */
    constructor(dependencies)
    {
        /**
         * The country the game client is being played in.
         * @private
         * @type {Country}
         */
        this._country = Country.ITALY;

        /**
         * @private
         * @type {SlotConfig}
         */
        this._slotConfig = dependencies.config;

        /**
         * @private
         * @type {BusinessConfig}
         */
        this._config = dependencies.businessConfig;

        /**
         * Reference to the global Event Emitter
         * @private
         * @type {EventEmitter}
         */
        this._events = dependencies.dispatcher;

        /**
         * Reference to the url params
         * @private
         * @type {UrlParamsGen2}
         */
         this._urlParams =  dependencies.urlParams;       

        /**
         * Caches whatever comms mode is in use by the client.
         * @private
         * @type {string}
         */
        this._commsMode = C_CommsMode.SERVER;

        /**
         * @private
         * @type {String}
         */
        this._gameClientStartDate = generateGameClientStartDateString();

        /**
         * @private
         * @type {String}
         */
        this._gameClientStartTime = generateGameClientStartTimeString();

        /**
         * The CurrencyFormat that is currently in use in the Game Client. This is
         * used to render currency values appropriately, into the form required both
         * with currency currency (eg: Pound, Euro), but also for target country (2
         * countries with the Euro currency, may actually want the currency values
         * rendered in a different way, for example).
         * @private
         * @type {CurrencyFormat}
         */
        this._currencyFormat = C_Currency.EURO_ITALY;

        /**
         * @private
         * @type {CurrencyFormatter}
         */
        this._currencyFormatter = dependencies.currencyFormatter;
        if (!this._currencyFormatter) {
            this._currencyFormatter = new ConfigurableCurrencyFormatter(this._currencyFormat);
            this._usingDefaultCurrencyFormatter = true;
        }
        else
        {
            log.debug('Model: using custom supplied Currency Formatter');
        }

        /**
         * @private
         * @type {string}
         */
        this._playerNickName = "";

        /**
         * Type of any session currently in progress.
         * @private
         * @type {GameplayMode}
         */
        this._sessionType = C_SessionType.FREE_PLAY;

        /**
         * Information about any Session in progress.
         * @private
         * @type {SessionInfo}
         */
        this._regulation = null;

        /**
         * The total amount spent in the current Session. Must be reset when a
         * new session is opened.
         * @private
         * @type {number}
         */
        this._sessionCreditSpent = 0;

        /**
         * The total amount won in the current Session. Must be reset when a
         * new session is opened.
         * @private
         * @type {number}
         */
        this._sessionTotalCreditWon = 0;

        /**
         * Tracks the number of games played in the current Session. Must be reset
         * when a new session is opened.
         * @private
         * @type {number}
         */
        this._sessionNumGamesPlayed = 0;

        /**
         * The last known real value of Session Wallet (returned by the server). We track
         * this value separately to "playerWallet" (which is a simulation value): this
         * allows us to make logic decisions based on the real value (and not the simulation
         * value).
         * @private
         * @type {number}
         */
        this._sessionWallet = 100000;

        /**
         * The last known real value of Session Winnings (returned by the server). We track
         * this value separately to "playerWinnings" (which is a simulation value): this
         * allows us to make logic decisions based on the real value (and not the simulation
         * value). Nonetheless, we reset this value when a game ends.
         * @private
         * @type {number}
         */
        this._sessionWinnings = 0;
        
        /**
         * The last known real value of Session SuperBet (returned by the Server).  We track
         * this value separately to "playerSuperBet" (which is a simulation value): this
         * allows us to make logic decisions based on the real value (and not the simulation
         * value). Nonetheless, we reset this value when a game ends.
         * @private
         * @type {number}
         */
        this._sessionSuperBet = 0;

        /**
         * The last known real value of Session NumFreeGames (returned by the Server). We track
         * this value separately to "playerNumFreeGames" (which is a simulation value): this
         * allows us to make logic decisions based on the real value (and not the simulation
         * value). Nonetheless, we reset this value when a game ends.
         * @private
         * @type {number}
         */
        this._sessionNumPromoGames = 0;

        /**
         * The last known real value of Session NumFreeSpins (returned by the Server). We track
         * this value separately to "playerNumFreeSpins" (which is a simulation value): this
         * allows us to make logic decisions based on the real value (and not the simulation
         * value). Nonetheless, we reset this value when a game ends.
         * @private
         * @type {number}
         */
        this._sessionNumFreeSpins = 0;

        /**
         * The number of minutes that should be used for the Session Inacivity Timer.
         * This value is returned by the Server in Session Init Reply ( but is not 
         * currently returned for a restored Session ).
         * @private
         * @type {number}
         */
        this._sessionInactivityTimeout = DEFAULT_SESSION_INACTIVITY_TIMEOUT;

        /**
         * Indicates if a Session is currently in progress.
         * @private
         * @type {boolean}
         */
        this._isSessionInProgress = false;

        /**
         * Indicates if the current Session is reloaded.
         * @private
         * @type {boolean}
         */
        this._isSessionReloaded = false;

        /**
         * Tracks whether a game is currently in progress. Must only be changed
         * by the methods logGameStarted / logGameFinished.
         * @private
         * @type {boolean}
         */
        this._gameInProgress = false;

        /**
         * Tracks stats about the most recent game in progress. These are simulation values.
         * @private
         */
        this._gameStats =
        {
            /**
             * Tracks the real wallet value, at the start of a game. Used to calculate
             * the running simulation value that is returned by getPlayerWallet(). Must
             * only be changed by the method logGameStarted.
             * @type {number}
             */
            startWallet : 0,

            /**
             * Indicates if the game was started as a FreeGame (PromoGame): if not, then it
             * was a CREDIT started game.
             * @type {boolean}
             */
            isFreeGame : false,

            /**
             * Tracks the Total Bet value for any game currently in progress. Used to
             * calculate the running simulation value that is returned by getPlayerWallet().
             * Must only be changed by the method logGameStarted.
             * @type {number}
             */
            totalBet : 0,

            /**
             * Tracks the total amount won for the game currently in progress. Can be incremented
             * by the method addToPlayerWinnings(). Is reset to by logGameStarted().
             * @type {number}
             */
            winnings : 0
        };

        /**
         * The value of winnings from the last game played.
         * @private
         * @type {number}
         */
        this._lastGameWinnings = 0;

        /**
         * Tracks the id of the phase the game client was restored to.
         * @private
         * @type {string}
         */
        this._restoredGamePhase = null;


         /**
         * Object containing the result after game restoration message is processed. This is used in Quick phase games.
         * @private
         * @type {object}
         */
         this._restoredGameResult = null;

        // TODO: Confirm the mechanism for resetting superbet, because actually, this functionality
        // might need to be tweaked a bit (we can play "sub-games" from our superbet credit, so it
        // gets docked at a different point to other stats)
        /**
         * Tracks the total amount of Superbet for the game currently in progress. Can be incremented
         * by calling the method addToPlaySuperbet(). Is reset by logGameStarted
         * @private
         * @type {number}
         */
        this._superbet = 0;
        
        /**
         * Tracks the current number of Free Spins that the player has remaining.
         * @private
         * @type {number}
         */
        this._numFreeSpins = 0;

        /**
         * Indicates if an Autoplay session is currently in progress.
         * @private
         * @type {boolean}
         */
        this._autoplayInProgress = false;

        /**
         * Indicates the number of games that were set to be played in the current autoplay
         * session. If no session is in progress, this should be set to 0.
         * @private
         * @type {number}
         */
        this._autoplayNumGamesTotal = 0;

        /**
         * The last number of autoplay games that the player selected.
         * @private
         * @type {number}
         */
        this._autoplayLastNumGames = 0;

        /**
         * Total number of games played in the most recent autoplay session.
         * @private
         * @type {number}
         */
        this._autoplayNumGamesPlayed = 0;

        /**
         * Total amount won, in the most recent autoplay session.
         * @private
         * @type {number}
         */
        this._autoplayWinnings = 0;

        /**
         * Total amount spent, in the most recent autoplay session.
         * @private
         * @type {number}
         */
        this._autoplaySpent = 0;

        /**
         * Maximum amount the player can win in an autoplay session.
         * @private
         * @type {number}
         */
        this._autoplayMaxWinnings = Number.POSITIVE_INFINITY;

        /**
         * Maximum amount the player can lose in an autoplay session.
         * @private
         * @type {number}
         */
        this._autoplayMaxLosses = Number.POSITIVE_INFINITY;

        /**
         * @private
         * @type {AutoplaySessionData}
         */
        this._autoplaySessionData = null;

        /**
         * Tracks data about the FreeSpins phase
         * @private
         */
        this._freeSpins =
        {
            /**
             * Indicates whether a FreeSpins session is currently in progress.
             */
            inProgress : false,

            /**
             * Tracks the total won, for the current FreeSpins phase.
             */
            totalWon : 0
        };

        /**
         * Tracks data about Free Games.
         * @private
         */
        this._freeGames =
        {
            /**
             * Is a FreeGames session currently in progress ?
             * @type {boolean}
             */
            inProgress : false,

            /**
             * @type {number}
             */
            numTotal : 0,

            /**
             * Number of FreeGames started (but not necessarily completed) for the session.
             * @type {number}
             */
            numStarted : 0,

            /**
             * Used to store FreeGames winnings, but only updated when a Game Complete message is
             * processed (so this value only updates when a game is completed, and it only ever
             * contains complete game winnings values, according to the values returned by the
             * server).
             * @type {number}
             */
            winningsAtGameEnd : 0,

            /**
             * Used to track FreeGames winnings, in a form that can be presented to the player. Any
             * partial game winnings that are recorded using the recordCreditWin method, will be added
             * to this field. Of course, not all partial winnings may get recorded during the game's
             * life-cycle (eg: the player could invoke a "skip to end" method), so we can also use the
             * "winningsAtGameEnd" field to accurately track winnings from completed games, and update
             * this field to that value whenever a FreeGame is completed.
             * @type {number}
             */
            winningsForVisualization : 0
        };

        /**
         * Tracks data about the most recently completed FreeGames session. This
         * is cleared when the session is completed, and a new game starts.
         * @private
         * @type {FreeGamesSessionData}
         */
        this._freeGamesSessionData = null;

        /**
         * Cached reference to any generic Wmg Promo Games Award that may have been returned
         * in a default way (eg: in a Session Init Reply). This data is available, until the
         * app controller formally accepts it (by calling clearPromoGamesAward)
         * @private
         * @type {WmgPromoGamesAward}
         */
        this._promoGamesAward = null;
        
        // TODO: The whole "bet setting" functionality still requires a little tidying
        // up after the recent config change.
        /**
         * Configuration for available Bet Settings.
         * @private
         */
        this._betConfig = this._slotConfig.betConfig;

        /**
         * The currently active currency type.
         * @private
         * @type {CurrencyType}
         */
        this._currencyType = C_CurrencyType.CREDIT;

        /**
         * The minimum possible value of Bet per Line.
         * @private
         */
        this._betPerLineMin = 1;

        /**
         * The maximum possible value of Bet per Line.
         * @private
         */
        this._betPerLineMax = 1;

        /**
         * The minimum possible value of Num Lines.
         * @private
         */
        this._numLinesMin = 1;

        /**
         * The maximum possible value of Num Lines.
         * @private
         */
        this._numLinesMax = 1;

        /**
         * The minimum possible value of Num Hands.
         * @private
         */
        this._numHandsMin = 1;

        /**
         * The maximum possible value of Num Hands.
         * @private
         */
        this._numHandsMax = 1;

        /**
         * The minimum possible value of Special Wager Multiplier.
         * @private
         * @type {number}
         */
        this._specialWagerMultiplierMin = 1;

        /**
         * The maximum possible value of Special Wager Multiplier.
         * @private
         * @type {number}
         */
        this._specialWagerMultiplierMax = 1;

        // Determine possible ranges for bet settings, and cache them.
        let betConfig = this._betConfig;
        if (betConfig.stakePerHand) {
            let values = betConfig.stakePerHand;
            let betPerHandMin = values[0];
            let betPerHandMax = values[values.length - 1];

            this._betPerLineMin = betPerHandMin.betPerLine;
            this._betPerLineMax = betPerHandMax.betPerLine;

            this._numLinesMin = betPerHandMin.numLines;
            this._numLinesMax = betPerHandMax.numLines;
        }
        else
        {
            if (betConfig.numLines) {
                let numLines = betConfig.numLines;
                if (numLines.isEnumerated) {
                    let values = numLines.values;
                    this._numLinesMin = values[0];
                    this._numLinesMax = values[values.length - 1];
                }
                else
                {
                    this._numLinesMin = numLines.minValue;
                    this._numLinesMax = numLines.maxValue;
                }
            }

            if (betConfig.betPerLine) {
                let betPerLine = betConfig.betPerLine;
                if (betPerLine.isEnumerated) {
                    let values = betPerLine.values;
                    this._betPerLineMin = values[0];
                    this._betPerLineMax = values[values.length - 1];
                }
                else
                {
                    this._betPerLineMin = betPerLine.minValue;
                    this._betPerLineMax = betPerLine.maxValue;
                }
            }
        }

        if (betConfig.numHands) {
            let numHands = betConfig.numHands;
            if (numHands.isEnumerated) {
                let values = numHands.values;
                this._numHandsMin = values[0];
                this._numHandsMax = values[values.length - 1];
            }
            else
            {
                this._numHandsMin = numHands.minValue;
                this._numHandsMax = numHands.maxValue;
            }
        }

        if (betConfig.specialWagerMultiplier) {
            let specialWagerMultiplier = betConfig.specialWagerMultiplier;
            if (specialWagerMultiplier.isEnumerated) {
                let values = specialWagerMultiplier.values;
                this._specialWagerMultiplierMin = values[0];
                this._specialWagerMultiplierMax = values[values.length - 1];
            }
            else
            {
                this._specialWagerMultiplierMin = specialWagerMultiplier.minValue;
                this._specialWagerMultiplierMax = specialWagerMultiplier.maxValue;
            }
        }

        /**
         * The index of "Bet per Hand" option. For some games, Bet per is a bet
         * setting in its own right (effectively, it changes Bet per Line and
         * Num Lines simultaneously, through a preset collection of linked settings).
         * This option is available only when betConfig.enableBetPerHand is true.
         * This field tracks the index of the current setting when we change Bet
         * per Hand setting (because we still directly read from the betConfig
         * fields "betPerLineOptions" and "numLineOptions", using the same shared
         * index).
         * @private
         * @type {number}
         */
        this._betPerHandIndex = 0;

        /**
         * The current value of Bet per Line.
         * @private
         * @type {number}
         */
        this._betPerLine = this._betPerLineMin;

        /**
         * The current value of Num Lines.
         * @private
         * @type {number}
         */
        this._numLines = this._numLinesMin;

        /**
         * The current value of Num Hands.
         * @private
         * @type {number}
         */
        this._numHands = this._numHandsMin;

        /**
         * The current value of the Special Wager Multiplier bet setting.
         * @private
         * @type {number}
         */
        this._specialWagerMultiplier = this._specialWagerMultiplierMin;

        /**
         * The current setting for Bet Multiplier.
         * @private
         * @type {number}
         */
        this._betMultiplier = 1;    // TODO: This wants configuring externally..

        // Initialy bet settings to custom values
        let defaultBet = this._slotConfig.defaultBet;
        if (defaultBet)
        {
            log.info(`Model: trying to set default bet settings to ${JSON.stringify(defaultBet)}`);

            this._numHands = defaultBet.numHands;
            this._numLines = defaultBet.numLines;
            this._betPerLine = defaultBet.stakePerLine;
            this._specialWagerMultiplier = defaultBet.specialWagerMultiplier;
        }
    }

    //------------------------------------------------------------------------------
    // Basic utility methods
    //------------------------------------------------------------------------------
    /**
     * Checks if this is a Mobile device.
     * @public
     * @return {boolean}
     */
    isMobile() {
        return /Mobi/.test(navigator.userAgent);
    }


    /**
     * Checks if this is an Android device.
     * @public
     * @return {boolean}
     */
    isAndroid() {
        return /android/i.test(navigator.userAgent);
    }


    /**
     * Returns the player's nickname.
     * @public
     * @return {string}
     */
    getPlayerNickName() {
        return this._playerNickName;
    };


    /**
     * Sets the Comms Mode for the application.
     * TODO: For a production build, this should probably be disabled ?
     * @public
     * @param {string} commsMode 
     */
    setCommsMode(commsMode) {
        this._commsMode = commsMode;
    }


    /**
     * Returns the active comms mode of the application.
     * @public
     * @return {string}
     */
    getCommsMode() {
        return this._commsMode;
    }

    /**
     * @public
     * @return {number}
     */
    getClientEnvironmentValue()
    {

        let clientEnvironmentValueHasBeenSet = this._urlParams.clientEnv && this._urlParams.clientEnv !== "undefined";

        let clientEnvironmentValue = clientEnvironmentValueHasBeenSet ? this._urlParams.clientEnv : C_Client_Environment_Value.NOT_APPLICABLE;

        return Number(clientEnvironmentValue);
    }


    //------------------------------------------------------------------------------
    // Currency Format functionality
    //------------------------------------------------------------------------------
    /**
     * Returns the currently active currency format.
     * @public
     * @return {CurrencyFormat}
     */
    getCurrencyFormat() {
        return this._currencyFormat;
    };


    /**
     * Updates the active Currency Format (this is equivalent to setting the actual currency
     * in use by the game client)
     * @public
     * @param {CurrencyFormat} value 
     */
    setCurrencyFormat(value) {
        this._currencyFormat = value;

        // TODO: After the SGD Currency Formatting changes, this is a bit ugly!!
        // Should leak about some less leaky abstractions to handle this functionality.
        if (this._usingDefaultCurrencyFormatter) {
            this._currencyFormatter.setCurrencyFormat(this._currencyFormat);
        }

        // TODO: We could dispatch some notifications here, if we ever wanted to update
        // currency on-screen (allowing us to redraw currency values)
    };


    //------------------------------------------------------------------------------
    // Session stuff
    //------------------------------------------------------------------------------
    /**
     * Sets the type of session in progress.
     * @todo: Decide if we want to enhance this functionality, eg: check if session
     * in progress, check against invalid types, etc.
     * @public
     * @param {String} sessionType 
     */
    setSessionType(sessionType) {
        this._sessionType = sessionType;
    };


    /**
     * Returns the currently active Session Type.
     * @public
     * @return {string}
     */
    getSessionType() {
        return this._sessionType;
    }


    /**
     * Indicates if this is a realplay session
     * @public
     * @return {boolean}
     */
    getSessionIsRealPlay() {
        return this._sessionType === C_SessionType.REAL_PLAY;
    }


    /**
     * @public
     */
    getSessionIsFunBonus() {
        return this._sessionType === C_SessionType.FUN_BONUS;
    }


    /**
     * @public
     * @return {SessionInfo}
     */
    getSessionInfo() {
        return this._regulation;
    }

    /**
     * Returns the number of minutes that the Session Inacitivity Timeout should
     * run for.
     * @public
     * @return {number}
     */
    getSessionInactivityTimeout() {
        return this._sessionInactivityTimeout;
    }


    /**
     * Indicates if a game Session is currently in progress.
     * @public
     * @return {boolean}
     */
    isSessionInProgress() {
        return this._isSessionInProgress;
    }


    /**
     * Indicates if any currently open game session is a reloaded game session.
     * @public
     * @return {boolean}
     */
    isSessionReloaded() {
        return this._isSessionReloaded;
    }


    /**
     * Indicates if the game is being played in a For Fun session (freeplay, with
     * local comms).
     * @public
     * @return {boolean}
     */
    isSessionForFun() {
        return false;
    }


    /**
     * Indicates if any currently open game session is a Realplay session.
     * @public
     * @return {boolean}
     */
    isSessionRealplay() {
        return this._sessionType === C_SessionType.REAL_PLAY;
    }


    /**
     * Indicates if any currently open game session is a Freeplay session.
     * @public
     * @return {boolean}
     */
    isSessionFreeplay() {
        return this._sessionType === C_SessionType.FREE_PLAY;
    }


    /**
     * Indicates if any currently open game session is a FunBonus session.
     * @public
     * @return {boolean}
     */
    isSessionFunBonus() {
        return this._sessionType === C_SessionType.FUN_BONUS;
    }


    /**
     * Returns the total amount of credit added to the current session (in points).
     * This is the sum of all transfers, including the initial session transfer.
     * @public
     * @return {number}
     */
    getSessionCreditAdded() {
        if (this._regulation && this._regulation.amountAlreadyImported) {
            return this._regulation.amountAlreadyImported;
        }
        else return 0;
    }


    /**
     * Returns the total amount of credit spent in the current session (in points).
     * This is the sum of all bets made.
     * @public
     * @return {number}
     */
    getSessionCreditSpent() {
        return this._sessionCreditSpent;
    }


    /**
     * Returns the total amount of credit won in the current session (in points)
     * @public
     * @return {number}
     */
    getSessionCreditWon() {
        return this._sessionTotalCreditWon;
    }


    /**
     * Returns the total number of games that have been played in the current session.
     * @public
     * @return {number}
     */
    getSessionNumGamesPlayed() {
        return this._sessionNumGamesPlayed;
    }


    /**
     * Returns the current value of the player's Session Wallet. This is the amount
     * of credit the player has in their session. This field is different to "player
     * wallet". Session Wallet is always the last known value returned from the server.
     * (The server also expects the game client to send the correct value for this
     * field in various requests). PlayerWallet is really the same thing, but we are
     * free to change the value during the game, after winning sequences are shown,
     * etc (because we are not going to send that value to the server)
     * @public
     * @return {number}
     */
    getSessionWallet() {
        return this._sessionWallet;
    }


    /**
     * Returns the current value of the player's Session.SuperBet
     * @public
     * @return {number}
     */
    getSessionSuperBet() {
        return this._sessionSuperBet;
    }


    /**
     * Returns the current value of the player's Session.NumFreeGames
     * @public
     * @return {number}
     */
    getSessionNumFreeGames() {
        return this._sessionNumPromoGames;
    };


    /**
     * Returns the minimum amount of credit which may be transferred to a new game session.
     * @public
     * @return {number}
     */
    getMinSessionTransfer() {
        return this.getTotalBetMin() * 2;
    };


    /**
     * Indicates if it is possible to add more credit to the current game session, based on
     * "max credit in session" rules, ie: whether the player has already added some maximum
     * amount (or hit any other general rules that might exist). Does not take into account
     * affordability (eg: whether the player actually has any account money left to add), or
     * any game client configuration rules (eg: if Add Credit feature is enabled or disabled
     * for a game client deployment).
     * @public
     * @return {boolean}
     */
    canAddMoreCreditToSession() {
        let canAddMoreCredit = true;
        if (this._regulation) {
            if (this._regulation.country === "ITA") {
                /** @type {SessionInfoItaly} */
                let regItaly = this._regulation;
                let maxSessionTransfer = regItaly.regMoneyLimit;
                let amountAlreadyTransferred = regItaly.amountAlreadyImported;
                let minSessionTransfer = this.getMinSessionTransfer();
                if (amountAlreadyTransferred + minSessionTransfer > maxSessionTransfer) {
                    canAddMoreCredit = false;
                }
            }
        }
        return canAddMoreCredit;
    };


    /**
     * Indicates if its valid for the Game Client to perform a Session Restart action
     * (eg: start a new session, after one was manually closed within the game client).
     * This takes into account rules about max session transfers which may exist in
     * different countries: it does not take into account affordability (whether the
     * player has any money left for a new transfer), or any game client configuration
     * rules (eg: if Restart Session feature is enmabled or disabled for a game client
     * deployment).
     * @public
     * @return {boolean}
     */
    canRestartSession() {
        let canRestart = true;

        if (this._regulation) {
            if (this._regulation.country === "ITA") {
                /** @type {SessionInfoItaly} */
                let regItaly = this._regulation;
                let maxSessionTransfer = regItaly.regMoneyLimit;
                let amountAlreadyTransferred = regItaly.amountAlreadyImported;
                if (amountAlreadyTransferred >= maxSessionTransfer) {
                    canRestart = false;
                }
            }
        }

        return canRestart;
    };


    /**
     * Indicates if the minimum game timer feature should be enabled. This is a country
     * specific rule, so its configured through the model (and not through business config).
     * @public
     * @return {boolean}
     */
    isMinimumGameTimerRequired() {
        let minGameTimeMins = this.getMinimumGameTimeMins();
        return minGameTimeMins >= 1;
    };


    /**
     * Returns the minimum time a game should last, in minutes. If no minimum game time
     * restriction exists, then a value less than 1 will be returned.
     * @public
     * @return {number}
     */
    getMinimumGameTimeMins() {
        return -1;
    };


    /**
     * Returns the current value of "Player Wallet". This is the value to display in
     * the "Coin" field for the game. It is a simulation of player's current credit,
     * but it can include any winnings that may have been shown. Player Wallet is a
     * local simulation value: we are free to change its actual value in any way that
     * we need to support the game simulation that is shown to the player. We only
     * need to sync this to the same value as "SessionWallet" when no game is in progress.
     * @public
     * @return {number}
     */
    getPlayerWallet() {
        if (this._gameInProgress) {
            // TODO: we actually do NOT want to add gameTotalWinnings onto this field,
            // under our new way of working: instead, we only update the value of player
            // wallet, once we "pay out". However, i am going to leave this as is for
            // now, because the actual way i go about sequencing this change, will need
            // to be formally approved
            if (this._gameStats.isFreeGame) {
                return this._gameStats.startWallet;
            }
            else
            {
                return this._gameStats.startWallet - this._gameStats.totalBet;
            }
        }
        else {
            return this.getSessionWallet();
        }
    };


    /**
     * @public
     * @inheritDoc
     * @param {number} newValue 
     */
    setUpdatedWallet(newValue)
    {
        if (this._gameInProgress) {
            if (this._gameStats.isFreeGame) {
                this._gameStats.startWallet = newValue;
            }
            else {
                this._gameStats.startWallet = newValue + this._gameStats.totalBet;
            }
        }
        else
        {
            this._sessionWallet = newValue;
        }

        this.notifyWalletChanged();
    };


    /**
     * Returns the current value of "Player Winnings": this is the total amount shown
     * to have been won, during the current game in progress.
     * @public
     * @return {number}
     */
    getPlayerWinnings() {
        return Math.max(0, this._gameInProgress? this._gameStats.winnings : this._sessionWinnings);
    };


    /**
     * Records a Credit Win of a specific amount. If autoplay is in progress, this will
     * automatically be added onto autoplay stats related to winnings.
     * @public
     * @param {number} amountWon 
     */
    recordCreditWin(amountWon) {
        let prevWinnings = this._gameStats.winnings;
        this._gameStats.winnings = prevWinnings + amountWon;
        
        log.debug(`Model.recordCreditWin(was:${prevWinnings},add:${amountWon},now:${this._gameStats.winnings})`);
        
        if (this.isAutoplayInProgress()) {
            this._autoplayWinnings += amountWon;
            this.notifyAutoplayChanged();
        }

        if (this._freeSpins.inProgress) {
            this._freeSpins.totalWon += amountWon;
            this.notifyFreeSpinsChanged();
        }

        if (this._freeGames.inProgress) {
            this._freeGames.winningsForVisualization += amountWon;
            // TODO: Do we need a dedicated "FreeGamesChanged" event dispatched here?
            // Afterall, we are sending the winnings changed event.
        }

        this.notifyWinningsChanged();
    };


    // TODO: We need equivalent methods for the other basic types of win.


    /**
     * @deprecated
     */
    addToPlayerWinnings(amountWon) {
        log.warn("Model.addToPlayerWinnings is deprecated, and will be removed in a later release. Please use recordCreditWin instead");
    }


    /**
     * Returns the current value of "Player Superbet".
     * @public
     * @return {number}
     */
    getPlayerSuperbet() {
        return this._superbet;
    }


    /**
     * @deprecated
     */
    addToPlayerSuperbet(amountWon) {
        log.warn("Model.addToPlayerSuperBet is deprecated, and will be removed in a later release. Please use recordSuperBetWin instead");
    };


    /**
     * Records a win which awards a specific amount of SuperBet. This should be called, when the win has been shown.
     * @public
     * @param {number} amountWon 
     */
    recordSuperBetWin(amountWon) {
        let prevSuperbet = this._superbet;
        this._superbet = prevSuperbet + amountWon;
        
        log.debug(`Model.addToPlayerSuperbet(was:${prevSuperbet},add:${amountWon},now:${this._superbet})`);
        
        this.notifySuperbetChanged();
    };


    /**
     * Returns any cached instance of a generic Wmg Promo Games award. Returns null when there is no
     * Promo Games Award to be consumed.
     * @public
     * @return {WmgPromoGamesAward | null}
     */
    getPromoGamesAward() {
        return this._promoGamesAward;
    };


    /**
     * Clears any cached generic Wmg Promo Games award. Should be called when the Promo Games award has
     * been accepted by the player.
     * @public
     */
    clearPromoGamesAward() {
        this._promoGamesAward = null;
    };


    // TODO: Consider if this should accept the whole session description instead, and should
    // handle the Bet level selection (consider that Model has access to bet settings meta-data:
    // this definitely could be handled here, its a case of where do we want to place the flexibility ?)
    /**
     * Manually sets a promo games session award (sometimes, the server doesnt tell us this data immediately,
     * or the player must be able to make a choice).
     * @public
     * @param {number} numPromoGames
     */
    setPromoGamesSessionNumGamesSelected(numPromoGames) {
        this._sessionNumPromoGames = numPromoGames;
    };


    /**
     * @return {number}
     */
    getPlayerNumPromoGames() {
        // TODO: This may not be the right value to return, this all needs some tidying up
        return this._sessionNumPromoGames;
    };


    /**
     * Records a specific number of Free Spins as having been won. This should be called, when the win has been shown.
     * @public
     * @param {number} numWon 
     */
    recordFreeSpinsWin(numWon) {
        this._numFreeSpins += numWon;
        this.notifyFreeSpinsChanged();
    }


    /**
     * Logs the FreeSpin phase starting : should be called at the start of a FreeSpin phase, exactly
     * once, by GameController.
     * @public
     */
    logFreeSpinPhaseStarted() {
        this._freeSpins.inProgress = true;
        this.notifyFreeSpinsChanged();
    };


    /**
     * Logs the FreeSpin phase finishing : should be called at the end of a FreeSpin phase, exactly
     * once, by GameController.
     * @public
     */
    logFreeSpinPhaseFinished() {
        this._freeSpins.inProgress = false;
        this.notifyFreeSpinsChanged();
    };


    /**
     * Resets all FreeSpin phase stats. Should happen whenever a new SubBet starts (as the SubBet could include
     * a FreeSpin phase).
     * @private
     */
    resetFreeSpinsStats()
    {
        this._freeSpins.inProgress = false;
        this._freeSpins.totalWon = 0;
    };


    /**
     * Decrements the number of FreeSpins remaining by 1, and notifies the world.
     * @public
     */
    logFreeSpinRoundStarted() {
        this._numFreeSpins = Math.max(this._numFreeSpins - 1, 0);
        this.notifyFreeSpinsChanged();
    };


    /**
     * Returns the number of Free Spins the player has remaining.
     * @public
     * @return {number}
     */
    getFreeSpinsNumRemaining() {
        return this._numFreeSpins;
    };


    /**
     * @public
     * @return {number}
     */
    getFreeSpinsWinnings() {
        return this._freeSpins.totalWon;
    };


    /**
     * Returns the total amount won from the last game completed.
     * @public
     * @return {number}
     */
    getLastGameWinnings() {
        return this._lastGameWinnings;
    };


    /**
     * Returns the string id of a game phase to restore to (or null, if nothing
     * to restore).
     * @public
     * @return {string | null}
     */
    getRestoredGamePhase() {
        return this._restoredGamePhase;
    };

    /**
     * Returns the object for the restored game results  (or null, if nothing
     * to restore).
     * @public
     * @return {object | null}
     */
    getRestoredGameResult()
    {
        return this._restoredGameResult;
    }


    /**
     * Logs the beginning of a new subbet.
     * @public
     */
    logSubBetStarted() {
        log.info('GameModel.logSubBetStarted()');

        this.resetFreeSpinsStats();
        
        if (this._currencyType === C_CurrencyType.CREDIT) {
            this.logCreditSubBetStarted();
        }
        else
        if (this._currencyType === C_CurrencyType.SUPER_BET) {
            this.logSuperbetSubBetStarted();
        }
        else
        if (this._currencyType === C_CurrencyType.WINNINGS) {
            this.logWinningsSubBetStarted();
        }
        else
        if (this._currencyType === C_CurrencyType.PROMO_GAME) {
            this.logPromoGameSubBetStarted();
        }
    };


    /**
     * Reverts the last call to "logSubBetStarted". Call when the first service
     * in starting a new SubBet has failed (in a way that is recoverable).
     * @public
     */
    revertSubBetStarted() {
        log.info('GameModel.revertSubBetStarted()');
        
        if (this._currencyType === C_CurrencyType.CREDIT) {
            this.revertCreditSubBetStarted();
        }
        else
        if (this._currencyType === C_CurrencyType.SUPER_BET) {
            this.revertSuperbetSubBetStarted();
        }
        else
        if (this._currencyType === C_CurrencyType.WINNINGS) {
            this.revertWinningsSubBetStarted();
        }
        else
        if (this._currencyType === C_CurrencyType.PROMO_GAME) {
            this.revertPromoGameSubBetStarted();
        }
    };


    /**
     * Logs the start of a new game. This is the mechanism that must be used
     * by GameController when a new game is entered.
     * @param {boolean} isFreeGame
     * @private
     */
    logGameStarted(isFreeGame=false) {
        log.debug(`GameModel.logGameStarted(isFreeGame:${isFreeGame})`);

        this._gameInProgress = true;
        this._gameStats.isFreeGame = isFreeGame;
        this._gameStats.startWallet = this.getSessionWallet();
        this._gameStats.totalBet = this.getTotalBet();
        this._gameStats.winnings = 0;
        this._superbet = 0;
        this._numFreeSpins = 0;
        this._sessionCreditSpent += this.getTotalBet();
        this._sessionNumGamesPlayed += 1;

        // Check for FreeGames Session commencing
        if (isFreeGame) {
            if (this._freeGames.inProgress === false) {
                this._freeGames.inProgress = true;
                this._freeGames.numTotal = this._sessionNumPromoGames;
                this._freeGames.numStarted = 1;
                this._freeGames.winningsForVisualization = 0;
            }
            else
            {
                this._freeGames.numStarted += 1;
            }
        }
        else
        {
            this._freeGames.inProgress = false;
            this._freeGamesSessionData = null;
        }

        // TODO: What about "isFreeGame" and "isAutoplay" ? In principle, this
        // could happen (although the GUI doesn't currently allow it). Some
        // rule or handling code here would be usefull.

        // Check for Autoplay Session commencing
        if (this.getAutoplayNumGamesRemaining() > 0) {
            if (this._autoplayInProgress === false) {
                // Autoplay mode is being started
                this._autoplayInProgress = true;
                this._autoplayNumGamesPlayed = 0;
                this._autoplaySpent = 0;
                this._autoplayWinnings = 0;
                this._autoplaySessionData = null;
                this.notifyAutoplayStarted();
            }

            this._autoplayNumGamesPlayed += 1;
            this._autoplaySpent += this.getTotalBet();
            this.notifyAutoplayChanged();
        }
        else
        {
            this.clearAutoplayFlags();
            this._autoplaySessionData = null;
        }
        
        this.notifyWalletChanged();
        this.notifyWinningsChanged();
        this.notifyGameStarted();
    };


    /**
     * @private
     * @param {boolean} isPromoGame
     */
    revertGameStarted(isPromoGame=false) {
        log.debug(`GameModel.revertGameStarted(isPromoGame:${isPromoGame})`);

        this._gameInProgress = false;
        this._gameStats.startWallet = 0;
        this._sessionCreditSpent -= this._gameStats.totalBet;
        this._sessionNumGamesPlayed -= 1;

        if (this._autoplayInProgress) {
            this._autoplayNumGamesPlayed -= 1;
            this._autoplaySpent -= this._gameStats.totalBet;
            this.notifyAutoplayChanged();
        }

        this.notifyWalletChanged();
        this.notifyWinningsChanged();
    };


    /**
     * @private
     */
    logCreditSubBetStarted() {
        log.debug('GameModel.logCreditSubBetStarted()');

        this.logGameStarted();
    };


    /**
     * @private
     */
    revertCreditSubBetStarted() {
        log.debug('GameModel.revertCreditSubBetStarted()');

        this.revertGameStarted();
    };


    /**
     * @private
     */
    logPromoGameSubBetStarted() {
        log.debug('GameModel.logPromoGameSubBetStarted()');

        this.logGameStarted(true);
    };


    /**
     * @private
     */
    revertPromoGameSubBetStarted() {
        log.debug('GameModel.revertPromoGameSubBetStarted()');

        this.revertGameStarted(true);
    };


    /**
     * @private
     */
    logSuperbetSubBetStarted() {
        log.debug('GameModel.logSuperbetSubBetStarted()');

        this._superbet -= this.getTotalBet();
        this.notifySuperbetChanged();
    };


    /**
     * @private
     */
    revertSuperbetSubBetStarted() {
        log.debug('GameModel.revertSuperbetSubBetStarted()');

        this._superbet += this.getTotalBet();
        this.notifySuperbetChanged();
    };


    /**
     * @private
     */
    logWinningsSubBetStarted() {
        log.debug('GameModel.logWinningsSubBetStarted()');

        this._gameStats.winnings -= this.getTotalBet();
        this.notifyWinningsChanged();
    };


    /**
     * @private
     */
    revertWinningsSubBetStarted() {
        log.debug('GameModel.revertWinningSubBetStarted()');

        this._gameStats.winnings += this.getTotalBet();
        this.notifyWinningsChanged();
    };


    /**
     * Logs the end of a game. This is the mechanism that must be used by
     * GameController when the current game has finished.
     * @public
     * @param {Gen2GameCompleteReply} msg
     */
    processGameCompleteMsg(msg) {
        log.info(`logGameFinished(${JSON.stringify(msg)})`);

        this._gameInProgress = false;
        this._lastGameWinnings = this._gameStats.winnings;
        this._sessionTotalCreditWon += this._gameStats.winnings;
        this._sessionWallet = msg.currentWallet;
        this._sessionWinnings = 0;
        this._sessionSuperBet = 0;
        this._sessionNumFreeSpins = 0;
        this._gameStats.winnings = 0;

        if (this._freeGames.inProgress) {
            this._freeGames.winningsAtGameEnd += this._lastGameWinnings;
            this._freeGames.winningsForVisualization = this._freeGames.winningsAtGameEnd;

            // If the FreeGames session is completed, then clear FreeGames data, and cache
            // some data about the completed FreeGames session.
            if (this._sessionNumPromoGames === 0) {
                this._freeGamesSessionData =
                {
                    winnings : this._freeGames.winningsAtGameEnd,
                    numGamesPlayed : this._freeGames.numTotal
                }

                this.clearFreeGamesData();
            }
        }
        
        // Decide if autoplay mode should be cancelled.
        if (this.isAutoplayInProgress())
        {
            this.notifyAutoplayChanged();

            // TODO: To support SG Digital (and their concept of affordability),
            // the way this logic is implemented - and WHERE it is implemented -
            // may end up having to change (as we directly check if the player
            // can afford a game here..)
            let noAutoplaysLeft = this.getAutoplayNumGamesRemaining() === 0;
            let cannotAffordNewGame = this.shouldCancelAutoplayWhenInsufficientFunds() && (this.getSessionWallet() < this.getTotalBet());
            let exceededMaxWinnings = this.getAutoplayWinnings() >= this.getAutoplayMaxWinnings();
            let exceededMaxLosses = this.getAutoplayLosses() >= this.getAutoplayMaxLosses();

            log.debug(`autoplay: numGamesLeft:${this.getAutoplayNumGamesRemaining()}`);
            log.debug(`autoplay: spent:${this.getAutoplaySpent()}`);
            log.debug(`autoplay: winnings:${this.getAutoplayWinnings()}, maxWinnings:${this.getAutoplayMaxWinnings()}`);
            log.debug(`autoplay: losses:${this.getAutoplayLosses()}, maxLosses:${this.getAutoplayMaxLosses()}`);

            if (noAutoplaysLeft || cannotAffordNewGame || exceededMaxWinnings || exceededMaxLosses)
            {
                log.debug(`processGameComplete: autoplay rules violated, aborting autoplay`);

                // TODO: We could handle this differently, eg: explicitly dispatch
                // a notification saying that autoplay ended due to other reasons.
                this._autoplaySessionData = this.generateAutoplaySessionData();
            
                if (noAutoplaysLeft) {
                    this._autoplaySessionData.completed = true;
                }
                else
                {
                    this._autoplaySessionData.aborted = true;
                    this._autoplaySessionData.abortedDescription = {
                        cannotAffordNewGame,
                        exceededMaxWinnings,
                        exceededMaxLosses
                    };
                }

                this.clearAutoplayFlags();
                this.notifyAutoplayEnded();
            }
        }

        this.notifyWalletChanged();
        this.notifyWinningsChanged();
        this.notifyGameFinished();
    }


    /**
     * Checks if we should cancel autoplay session when not enough funds for a new game. Some integrations
     * require us to not do this: for example, we should only inform the player of insufficient funds, after
     * actually attempting to send a request (with invalid bet settings relative to current wallet). In such
     * a scenario, we may not want to auto-cancel autoplay here, but let it continue (and let the server
     * reject our new game request)
     * @private
     * @return {boolean} True if we *should* cancel.
     */
    shouldCancelAutoplayWhenInsufficientFunds() {
        let shouldCancel = true;

        if (BUILD_PLATFORM === "sgdigital") {
            shouldCancel = false;
        }

        return shouldCancel;
    }


    //------------------------------------------------------------------------------
    // Autoplay functionality
    //------------------------------------------------------------------------------
    /**
     * Sets the number of games to play in an autoplay session.
     * @public
     * @param {number} numGames 
     */
    setAutoplayNumGamesTotal(numGames) {
        log.info(`Model.setAutoplayNumGamesTotal(numGames:${numGames})`);

        this._autoplayNumGamesTotal = numGames;
        this._autoplayLastNumGames = numGames;
        this.notifyAutoplayChanged();
    }


    /**
     * Returns the total number of games that were selected to be played in autoplay mode.
     * @public
     * @return {number}
     */
    getAutoplayNumGamesTotal() {
        return this._autoplayNumGamesTotal;
    }


    /**
     * Sets the number of autoplay games, to the last user selected value. This is a utility
     * method for repeating previous autoplay sessions, without requring the user to do any
     * thing other than agreeing to do this.
     * @public
     */
    setAutoplayNumGamesToLastSelectedValue() {
        log.info(`Model.setAutoplayNumsGamesToLastSelectedValue(lastValue:${this._autoplayLastNumGames})`);

        this._autoplayNumGamesTotal = this._autoplayLastNumGames;
        this.notifyAutoplayChanged();
    };


    /**
     * Returns the number of games that have been played in any current active
     * autoplay session.
     * @public
     * @return {number}
     */
    getAutoplayNumGamesPlayed() {
        return this._autoplayNumGamesPlayed;
    }


    /**
     * Returns the number of games remaining to be played in autoplay mode.
     * @public
     * @return {number}
     */
    getAutoplayNumGamesRemaining() {
        return Math.max(0, this.getAutoplayNumGamesTotal() - this.getAutoplayNumGamesPlayed());
    }


    /**
     * Returns the amount won (in points) in the current autoplay session.
     * @public
     * @return {number}
     */
    getAutoplayWinnings() {
        return this._autoplayWinnings;
    }


    /**
     * Returns the amount spent (in points) in the current autoplay session.
     * @public
     * @return {number}
     */
    getAutoplaySpent() {
        return this._autoplaySpent;
    }


    /**
     * Returns the amount lost (in points) in the current autoplay session. This is
     * defined as the difference between what has been won in the autoplay session,
     * and what has been spent: however, if the player has won more than they have
     * spent, then a value of 0 is returned (as negative losses are not useful to
     * track in this context). This can be understood as follows:
     * - If the player has won 100, but spent 200, then losses are 100
     * - If the player has won 200, but spent 100, then losses are 0
     * @public
     * @return {number}
     */
    getAutoplayLosses() {
        return Math.max(0, this._autoplaySpent - this._autoplayWinnings);
    }


    /**
     * Indicates if an Autoplay Session is currently in progress.
     * @public
     * @return {boolean}
     */
    isAutoplayInProgress() {
        return this._autoplayInProgress;
    }


    /**
     * @deprecated
     */
    addToAutoplayWinnings(amountWon) {
        log.error("Model.addToAtoplayWinnings is deprecated, and will be removed in a later release");
    }


    /**
     * Cancels any autoplay session that is in progress. This should be invoked if the
     * player selects to cancel autoplay mode.
     * @public
     */
    cancelAutoplay() {
        if (this._autoplayInProgress) {
            log.info('Model.cancelAutoplay: autoplay in progress, setting session to aborted');
            
            this._autoplaySessionData = this.generateAutoplaySessionData();
            this._autoplaySessionData.cancelled = true;

            this.clearAutoplayFlags();
            this.notifyAutoplayEnded();

            log.info(`Model.cancelAutoplay: numGamesTotal:${this.getAutoplayNumGamesTotal()}`);
            log.info(`Model.cancelAutoplay: numGamesPlayed:${this.getAutoplayNumGamesPlayed()}`);
            log.info(`Model.cancelAutoplay: numGamesRemaining:${this.getAutoplayNumGamesRemaining()}`);
        }
        // Autoplay not in progress, but is queued
        else
        if (this.getAutoplayNumGamesTotal() > 0)
        {
            log.info(`Model.cancelAutoplay: autoplay not in progress, but was queued with ${this.getAutoplayNumGamesTotal()} games. Aborting queued session`);

            this.clearAutoplayFlags();
            this.notifyAutoplayChanged();
        }
    }


    /**
     * Generates a new Autoplay Session Data object, based on current autoplay stats.
     * The only fields not configured on this object, are reasons for why the autoplay
     * session ended.
     * @private
     * @return {AutoplaySessionData}
     */
    generateAutoplaySessionData() {
        return {
            winnings : this.getAutoplayWinnings(),
            spent : this.getAutoplaySpent(),
            losses : this.getAutoplayLosses(),
            maxWinnings : this.getAutoplayMaxWinnings(),
            maxLosses : this.getAutoplayMaxLosses()
        };
    }


    /**
     * Clears all data associated with an autoplayu session
     * @private
     */
    clearAutoplayFlags() {
        this._autoplayInProgress = false;
        this._autoplayNumGamesTotal = 0;
        this._autoplayNumGamesPlayed = 0;
        this._autoplayWinnings = 0;
    };


    /**
     * Sets the maximum amount that the player may win in a single Autoplay Session.
     * @public
     * @param {number} value 
     */
    setAutoplayMaxWinnings(value) {
        this._autoplayMaxWinnings = value;
        log.debug(`autoplayMaxWinnings set to ${value}`);
    }


    /**
     * Returns the maximum amount that the player may win in an Autoplay Session.
     * @public
     * @return {number}
     */
    getAutoplayMaxWinnings() {
        return this._autoplayMaxWinnings;
    }


    /**
     * Sets the maximum amount that the player may lose in an Autoplay Session.
     * @public
     * @param {number} value 
     */
    setAutoplayMaxLosses(value) {
        this._autoplayMaxLosses = value;
        log.debug(`autoplayMaxLosses set to ${value}`);
    }


    /**
     * Returns the maximum amount that the player may lose in an Autoplay Session.
     * @public
     * @return {number}
     */
    getAutoplayMaxLosses() {
        return this._autoplayMaxLosses;
    }


    /**
     * Returns the auto-matic loss limit for Autoplay. This is for use in the Autoplay
     * Settings Menu, and must be re-calculated whenever other Autoplay settings (eg:
     * total number of autoplay games) are changed. Autoplay Loss Limit is the default
     * Loss Limit value to use, unless the player explicitly overrides it: it is guaranteed
     * to be a value that the player can currently afford.
     * @public
     * @return {number}
     */
    getAutoplayAutoLossLimit() {
        let autoLossLimit = this.getTotalBet() * this.getAutoplayNumGamesTotal();
        let sessionWallet = this.getSessionWallet();
        return autoLossLimit > sessionWallet ? sessionWallet : autoLossLimit;
    };


    /**
     * Returns data about the most recently completed Autoplay Session. This will only
     * return a data object when no game is in progress, and an autoplay session has
     * just completed (it exists to cache this information, for the benefit of post game
     * notifications). So, if this value is non-null when a game completes, it can be
     * taken as an indication that the game just completed was the last in an Autoplay
     * Session.
     * @public
     * @return {AutoplaySessionData}
     */
    getAutoplaySessionData() {
        return this._autoplaySessionData;
    };


    //------------------------------------------------------------------------------
    // FreeGames stuff
    //------------------------------------------------------------------------------
    /**
     * Checks if a FreeGames session is currently in progress.
     * @public
     * @return {boolean}
     */
    getFreeGamesInProgress() {
        return this._freeGames.inProgress;
    };


    /**
     * Returns the current number of FreeGames remaining (as should be shown to the player).
     * Guarantees to never return a value below 0.
     * @public
     * @return {number}
     */
    getFreeGamesNumRemaining() {
        let numRemaining = 0;
        if (this._freeGames.inProgress) {
            numRemaining = Math.max(0, this._freeGames.numTotal - this._freeGames.numStarted);
        }
        return numRemaining;
    };


    /**
     * Returns the current display winnings for any FreeGames that is in progress. (If not
     * FreeGames Session is in progress, this will be a value of 0).
     * @public
     * @return {number}
     */
    getFreeGamesWinnings() {
        let winnings = 0;
        if (this._freeGames.inProgress) {
            // freeGames.winnings should never be below 0, but we guarantee to only show
            // values of 0 or more, in the event we have some bug elsewhere in our model.
            winnings = Math.max(0, this._freeGames.winningsForVisualization);
        }
        return winnings;
    };


    /**
     * Returns data about the most recently completed FreeGames Session. This will only
     * return a data object when no game is in progress, and a FreeGames Session has just
     * completed (it exists to cache this ninformation, for the benefit of post game
     * notifications). So, if this value is non-null when a game completes, it can be taken
     * as an indication that the game just completed was the last in a FreeGames session.
     * @public
     * @return {FreeGamesSessionData | null}
     */
    getFreeGamesSessionData() {
        return this._freeGamesSessionData;
    };


    /**
     * Resets all FreeGames data back to a default "no freegames in progress" state.
     * @private
     */
    clearFreeGamesData() {
        this._freeGames.inProgress = false;
        this._freeGames.numStarted = 0;
        this._freeGames.numTotal = 0;
        this._freeGames.winningsAtGameEnd = 0;
        this._freeGames.winningsForVisualization = 0;
    };


    //------------------------------------------------------------------------------
    //------------------------------------------------------------------------------


    /**
     * Returns the currently active Currency Format object.
     * @public
     * @return {CurrencyFormat}
     */
    getCurrency() {
        return this._currencyFormat;
    };


    /**
     * Formats a currency value (an integer, in points) into a Currency String.
     * This uses the currently active CurrencyFormat to render the string. It
     * will also drop the Currency symbol used when the game is in freeplay mode.
     * @public
     * @param {number} value
     * @return {string}
     */
    formatCurrency(value) {
        return this._currencyFormatter.formatCoin(value, this._sessionType);
    };


    /**
     * Formats a points value (an integer, in points!) into a Points String.
     * NOTE: This is place-hold functionality. I anticipate being requested at some
     * point to change the way that points values (ie: any winning amounts not rendered
     * as currency) are formatted (for different locales / licensees). I am not 100% sure
     * if the actual formatting should be specific in CurrencyFormat object though.
     * @public
     * @param {number} value 
     * @return {string}
     */
    formatPoints(value) {
        return value.toString();
    };


    /**
     * Formats a numerical value, into a percentage string. This is useful for when we need to render
     * Return to Player (RTP) information on-screen, and we want to pick a locale specific way of doing
     * it.
     * @public
     * @param {number} value
     * The actual percentage value. This should be in a real percentage range, eg : "90.5%" will be passed
     * in as a value of 90.5
     * @return {string}
     * The percentage value, formatted to locale specific requirements.
     */
    formatPercentage(value) {
        return formatPercentage(this._country, value);
    };



    /**
     * Checks if a new game can be afforded (using any bet settings).
     * @public
     * @return {boolean}
     */
    isNewGameAffordable() {
        return this.getTotalBetMin() <= this.getSessionWallet();
    };


    //------------------------------------------------------------------------------
    // Bet Settings
    //------------------------------------------------------------------------------
    /**
     * @public
     * @return {SlotBetSettings}
     */
    getCurrentBetSettings() {
        return {
            currencyType : this._currencyType,
            numHands : this.getNumHands(),
            numLines : this.getNumLines(),
            stakePerLine : this.getBetPerLine(),
            specialWagerMultiplier : this.getSpecialWagerMultiplier()
        };
    }


    /**
     * @public
     * @param {number} totalBet
     * @return {boolean}
     */
    isTotalBetValid(totalBet) {
        return this.getAllBetSettingsMatchingTotalBet(totalBet).length > 0;
    }


    /**
     * @private
     * @param {SingleBetSetting} singleBetSetting 
     * @return {number[]}
     */
    getAllValuesForSingleBetSetting(singleBetSetting) {

        let allValues;

        singleBetSetting = singleBetSetting ? singleBetSetting : null;

        if (singleBetSetting)
        {
            if (singleBetSetting.isEnumerated) {
                allValues = singleBetSetting.values.slice();
            }
            else
            if (singleBetSetting.isEnumerated === false)
            {
                allValues = [];
                for (let val = singleBetSetting.minValue; val <= singleBetSetting.maxValue; val ++) {
                    allValues.push(val);
                }
            }
        }
        else 
        {
            allValues = [1,1];
        }

        
        return allValues;
    };


    /**
     * Returns all available values of Stake Per Hand (if this sett)
     * @private
     * @param {StakePerHand[]} stakePerHands
     * @return {number[]}
     */
    getAllValuesForStakePerHand(stakePerHands) {
        let allValues = [];
        stakePerHands.forEach(stakePerHand => {
            allValues.push(stakePerHand.betPerLine * stakePerHand.numLines);
        });
        return allValues;
    }


    /**
     * @public
     * @return {SlotBetSettings[]}
     */
    getAllPossibleBetSettings() {
        /** @type {SlotBetSettings[]} */
        let betSettings = [];

        let specialWagerMultiplierValues = this.getAllValuesForSingleBetSetting(this._betConfig.specialWagerMultiplier);
        let numHandsValues = this.getAllValuesForSingleBetSetting(this._betConfig.numHands);

        numHandsValues.forEach(numHandsVal =>
        {
            specialWagerMultiplierValues.forEach(specialWagerMultiplierVal =>
            {
                if (this._betConfig.stakePerHand) {
                    this._betConfig.stakePerHand.forEach(stakePerHand => {
                        betSettings.push({
                            currencyType : null,
                            numHands : numHandsVal,
                            stakePerLine : stakePerHand.betPerLine,
                            numLines : stakePerHand.numLines,
                            specialWagerMultiplier : specialWagerMultiplierVal
                        });
                    });
                }
                else
                {
                    let numLinesValues = this.getAllValuesForSingleBetSetting(this._betConfig.numLines);
                    let betPerLineValues = this.getAllValuesForSingleBetSetting(this._betConfig.betPerLine);


                    numLinesValues.forEach(numLinesVal => {
                        betPerLineValues.forEach(betPerLineVal => {
                            betSettings.push({
                                currencyType : null,
                                numHands : numHandsVal,
                                stakePerLine : betPerLineVal,
                                numLines : numLinesVal,
                                specialWagerMultiplier : specialWagerMultiplierVal
                            });
                        });
                    });
                }
            });
        });

        return betSettings;
    }


    /**
     * @public
     * @param {number} totalBet
     * @return {SlotBetSettings[]}
     */
    getAllBetSettingsMatchingTotalBet(totalBet) {
        return this.getAllPossibleBetSettings().filter(betSettings => {
            return totalBet === betSettings.numHands * betSettings.numLines * betSettings.stakePerLine * betSettings.specialWagerMultiplier;
        });
    };
    

    /**
     * Calculates a list of all possible values of Total Bet. Duplicates are removed, and the list
     * will be ordered.
     * @public
     * @return {number[]}
     */
    getAllPossibleTotalBetValues()
    {
        let betSettings = [];

        this.getAllValuesForSingleBetSetting(this._betConfig.numHands).forEach(numHandsVal =>
        {
            if (this._betConfig.stakePerHand) {
                this.getAllValuesForStakePerHand(this._betConfig.stakePerHand).forEach(stakePerHandVal => {
                    betSettings.push(stakePerHandVal * numHandsVal);
                });
            }
            else
            {
                let numLinesValues = this.getAllValuesForSingleBetSetting(this._betConfig.numLines);
                let betPerLineValues = this.getAllValuesForSingleBetSetting(this._betConfig.betPerLine);

                numLinesValues.forEach(numLinesVal => {
                    betPerLineValues.forEach(betPerLineVal => {
                        betSettings.push(numLinesVal * betPerLineVal * numHandsVal);
                    });
                });
            }
        });

        return betSettings
            .filter((item, index) => betSettings.indexOf(item) === index)
            .sort((a,b) => a - b);
    }


    /**
     * Sets current bet settings (excluding CurrencyType) based on a bet settings object.
     * At the moment, the method performs no validation of the value passed in (this may
     * change in a future revision).
     * @public
     * @param {SlotBetSettings} betSettings 
     */
    setBetSettings(betSettings) {
        // TODO: This does not currently validate the values set, and perhaps it should?
        // The method api deliberately doesnt mention this at the moment.

        this._numHands = betSettings.numHands;
        this._numLines = betSettings.numLines;
        this._betPerLine = betSettings.stakePerLine;
        this._specialWagerMultiplier = betSettings.specialWagerMultiplier;
        this.notifyBetChanged();

        log.info(`Model.setBetSettings: updated to {numHands:${this._numHands},numLines:${this._numLines},betPerLine:${this._betPerLine}}`);
    }


    /**
     * Returns the current settings for Total Bet (in points)
     * @public
     * @return {number}
     */
    getTotalBet() {
        return  this.getBetPerHand() *
                this.getNumHands() *
                this.getSpecialWagerMultiplier() *
                this.getBetMultiplier();
    }


    /**
     * Returns the minimum allowed value for Total Bet (in points)
     * @public
     * @return {number}
     */
    getTotalBetMin() {
        return  this.getBetPerHandMin() *
                this.getNumHandsMin() *
                this.getSpecialWagerMultiplierMin();
    };


    /**
     * Returns the maxmimum allowed value for Total Bet (in points)
     * @public
     * @return {number}
     */
    getTotalBetMax() {
        return  this.getBetPerHandMax() *
                this.getNumHandsMax() *
                this.getSpecialWagerMultiplierMax();
    };


    /**
     * Selects the lowest possible bet settings.
     * @public
     * @todo:
     * Currently ignores "Bet Multiplier"
     */
    setTotalBetToMin() {
        if (this._betConfig.stakePerHand) {
            this._betPerHandIndex = 0;
        }
        this._betPerLine = this.getBetPerLineMin();
        this._numLines = this.getNumLinesMin();
        this._numHands = this.getNumHandsMin();
        this._specialWagerMultiplier = this.getSpecialWagerMultiplierMin();
        this.notifyBetChanged();
    }


    /**
     * Selects the highest possible bet settings.
     * @public
     * @todo:
     * Currently ignores "Bet Multiplier"
     */
    setTotalBetToMax() {
        if (this._betConfig.stakePerHand) {
            this._betPerHandIndex = this._betConfig.stakePerHand.length - 1;
        }
        this._betPerLine = this.getBetPerLineMax();
        this._numLines = this.getNumLinesMax();
        this._numHands = this.getNumHandsMax();
        this._specialWagerMultiplier = this.getSpecialWagerMultiplierMax();
        this.notifyBetChanged();
    }


    /**
     * Returns the number of lines currently active in the game.
     * @public
     * @return {number}
     */
    getNumLines() {
        return this._numLines;
    };


    /**
     * @public
     * @param {number} value 
     */
    setNumLines(value) {
        this._numLines = value;
        this.notifyBetChanged();
    };


    /**
     * Returns the minimum number of lines available in the game.
     * @public
     * @return {number}
     */
    getNumLinesMin() {
        return this._numLinesMin;
    };


    /**
     * Returns the maximum number of lines available in the game.
     * @public
     * @return {number}
     */
    getNumLinesMax() {
        return this._numLinesMax;
    };


    /**
     * Cycles the value of Number of Active Winlines, through all available options. No
     * affordability checks are made. Defaults to increasing Bet per Line, unless optional
     * parameter increase is explicitly set to false. If BetConfig.enableBetPerHand is true,
     * then this method will call through to cycleBetPerHand instead.
     * @public
     * @param {boolean} [increase=true]
     * Indicates if Num Lines should be cycled upwards (when true), or downwards (when false).
     * Defaults to true (upwards).
     */
    cycleNumLines(increase=true) {
        if (this._betConfig.stakePerHand) {
            this.cycleBetPerHand(increase);
            return;
        }

        let numLinesConfig = this._betConfig.numLines;
        if (numLinesConfig) {
            this._numLines = this.cycleBetSetting(increase, numLinesConfig, this._numLines);
            this.notifyBetChanged();
        }
    };


    /**
     * Returns the current value of Stake per Line ( in points ).
     * @public
     * @return {number}
     */
    getBetPerLine() {
        return this._betPerLine;
    };


    /**
     * @param
     * @param {number} value 
     */
    setBetPerLine(value) {
        
        this._betPerLine = value;
        this.notifyBetChanged();
    }


    /**
     * Returns the minimum possible value of Bet Per Line for this game client.
     * @public
     * @return {number}
     */
    getBetPerLineMin() {
        return this._betPerLineMin;
    }


    /**
     * Returns the maximum possible value of Bet Per Line for this game client.
     * @public
     * @return {number}
     */
    getBetPerLineMax() {
        return this._betPerLineMax;
    }


    /**
     * Cycles the value of Bet per Line, through all available options. No affordability
     * checks are made. Defaults to increasing Bet per Line, unless optional parameter
     * increase is explicitly set to false. If BetConfig.enableBetPerHand is true, then
     * this method will call through to cycleBetPerHand instead.
     * @public
     * @param {boolean} [increase=true]
     * Indicates if Num Lines should be cycled upwards (when true), or downwards (when false).
     * Defaults to true (upwards).
     */
    cycleBetPerLine(increase=true) {
        if (this._betConfig.stakePerHand) {
            this.cycleBetPerHand(true);
            return;
        }

        let betPerLineConfig = this._betConfig.betPerLine;
        if (betPerLineConfig) {
            this._betPerLine = this.cycleBetSetting(increase, betPerLineConfig, this._betPerLine);
            this.notifyBetChanged();
        }
    }


    /**
     * Returns the current number of active hands in the game.
     * For any game that doesn't support multiple hands, this will
     * return 1.
     * @public
     * @return {number}
     */
    getNumHands() {
        return this._numHands;
    }


    /**
     * @public
     * @param {number} value 
     */
    setNumHands(value) {
        this._numHands = value;
        this.notifyBetChanged();
    }


    /**
     * Returns the minimum number of hands available in the Game Client.
     * @public
     * @return {number}
     */
    getNumHandsMin() {
        return this._numHandsMin;
    }


    /**
     * Returns the maximum number of hands available in the Game Client.
     * @public
     * @return {number}
     */
    getNumHandsMax() {
        return this._numHandsMax;
    }


    /**
     * Cycles the value of Number of Active Hands, through all available options. No
     * affordability checks are made. Defaults to increasing the Number of Active Hands,
     * unless the optional parameter increase is explicitly set to false.
     * @public
     * @param {boolean} [increase=true]
     * Indicates if Num Hands should be cycled upwards (when true), or downwards (when false).
     * Defaults to true (upwards).
     */
    cycleNumHands(increase) {
        let numHandsOptions = this._betConfig.numHands;
        if (numHandsOptions) {
            this._numHands = this.cycleBetSetting(increase, numHandsOptions, this._numHands);
            this.notifyBetChanged();
        }
    };


    /**
     * @public
     * @inheritDoc
     * @returns {number}
     */
    getSpecialWagerMultiplier() {
        return this._specialWagerMultiplier;
    };


    /**
     * @public
     * @inheritDoc
     * @returns {number}
     */
    getSpecialWagerMultiplierMin() {
        return this._specialWagerMultiplierMin;
    };


    /**
     * @public
     * @inheritDoc
     * @returns {number}
     */
    getSpecialWagerMultiplierMax() {
        return this._specialWagerMultiplierMax;
    };


    /**
     * Sets the current value of the Special Wager Multiplier bet setting.
     * @public
     * @param {number} value
     * The new value to set
     */
    setSpecialWagerMultiplier(value) {
        this._specialWagerMultiplier = value;
        this.notifyBetChanged();
    };


    /**
     * @private
     * @param {boolean} increase 
     * @param {RangedBetSetting | EnumeratedBetSetting} betSetting 
     * @param {number} currValue 
     * @return {number}
     */
    cycleBetSetting(increase, betSetting, currValue) {
        let newValue = currValue;

        if (betSetting.isEnumerated) {
            let enumValues = betSetting.values;
            let currValueIndex = enumValues.indexOf(currValue);
            let newValueIndex = 0;
            if (currValueIndex > -1) {
                if (increase) {
                    if (currValueIndex < enumValues.length) {
                        newValueIndex = currValueIndex + 1;
                    }
                    else newValueIndex = 0;
                }
                else
                {
                    if (currValueIndex > 0) {
                        newValueIndex = currValueIndex - 1;
                    }
                    else newValueIndex = enumValues.length - 1;
                }
            }
            newValue = enumValues[newValueIndex];
        }
        else
        {
            if (increase) {
                if (currValue < betSetting.maxValue) {
                    newValue += 1;
                }
                else newValue = betSetting.minValue;
            }
            else
            {
                if (currValue > betSetting.minValue) {
                    newValue -= 1;
                }
                else newValue = betSetting.maxValue;
            }
        }

        return newValue;
    }


    /**
     * Returns the current value of Bet per Hand. On any game where there is
     * only 1 active hand, this will return the same value as "totalBet".
     * @public
     * @return {number}
     */
    getBetPerHand() {
        return this.getNumLines() * this.getBetPerLine();
    }


    /**
     * Returns the minimum available value of Bet per Hand for the current game client.
     * @public
     * @return {number}
     */
    getBetPerHandMin() {
        return this.getNumLinesMin() * this.getBetPerLineMin();
    }


    /**
     * Returns the maximum available value of Bet per Hand for the current game client.
     * @public
     * @return {number}
     */
    getBetPerHandMax() {
        return this.getNumLinesMax() * this.getBetPerLineMax();
    }


    /**
     * Cycles the value of Bet per Hand, through all available options. No affordability
     * checks are made. Defaults to increasing Bet per Hand, unless optional parameter
     * increase is explicitly set to false. If BetConfig.enableBetPerHand setting is false,
     * then this function will return with no side effect.
     * @public
     * @param {boolean} [increase=true]
     * Indicates if the value should be cycle up, or down.
     */
    cycleBetPerHand(increase) {
        let betPerHand = this._betConfig.stakePerHand;
        let settingIsAvailable = betPerHand? true : false;
        if (!settingIsAvailable) {
            return;
        }

        let numBetPerLineOptions = betPerHand.length;
        let betPerHandIndex = this._betPerHandIndex;

        if (increase) {
            if (betPerHandIndex < numBetPerLineOptions - 1) {
                betPerHandIndex ++;
            }
            else {
                betPerHandIndex = 0;
            }
        }
        else {
            if (betPerHandIndex > 0) {
                betPerHandIndex --;
            }
            else {
                betPerHandIndex = numBetPerLineOptions - 1;
            }
        }

        // Update real value of Bet Per Winline.
        let betPerHandVal = betPerHand[betPerHandIndex];
        this._betPerLine = betPerHandVal.betPerLine;
        this._numLines = betPerHandVal.numLines;
        this._betPerHandIndex = betPerHandIndex;

        this.notifyBetChanged();
    }


    /**
     * Returns the current setting for Bet Multiplier. If the BetMultiplier setting is
     * disabled, then this method will always return 1.
     * @public
     * @return {number}
     */
    getBetMultiplier() {
        return this._betMultiplier;
    }


    /**
     * Returns the minimum available value of Bet Multiplier for the current game client.
     * @public
     * @return {number}
     */
    getBetMultiplierMin() {
        return 1;
    }


    /**
     * Returns the maximum available value of Bet Multiplier for the current game client.
     * @public
     * @return {number}
     */
    getBetMultiplierMax() {
        return 1;
    }


    /**
     * Sets the active CurrencyType to Credit.
     * @public
     */
    setCurrencyTypeToCredit() {
        this.setCurrencyTypeTo(C_CurrencyType.CREDIT);
    };


    /**
     * Sets the active CurrencyType to Superbet.
     * @public
     */
    setCurrencyTypeToSuperbet() {
        this.setCurrencyTypeTo(C_CurrencyType.SUPER_BET);
    };


    /**
     * Sets the active CurrencyType to Winnings.
     * @pubic
     */
    setCurrencyTypeToWinnings() {
        this.setCurrencyTypeTo(C_CurrencyType.WINNINGS);
    };


    /**
     * Sets the active CurrencyType to PromoGame.
     * @public
     */
    setCurrencyTypeToPromoGame() {
        this.setCurrencyTypeTo(C_CurrencyType.PROMO_GAME);
    };


    /**
     * Sets the Bet.CurrencyType value to a specific CurrencyType, and dispatches
     * an event notification only if the new value is different to the old one.
     * @public
     * @param {CurrencyType} newCurrencyType 
     */
    setCurrencyTypeTo(newCurrencyType) {
        let oldCurrencyType = this._currencyType;
        this._currencyType = newCurrencyType;
        if (oldCurrencyType !== newCurrencyType) {
            log.info(`Model: currencyType changed from ${oldCurrencyType} to ${newCurrencyType}`);
            this._events.emit(C_GameEvent.CURRENCY_TYPE_CHANGED);
        }
    };


    /**
     * Returns the active currency type.
     * @public
     * @return {CurrencyType}
     */
    getActiveCurrencyType() {
        return this._currencyType;
    };


    // TODO: We can either deprecate this one (its no longer relevant),
    // or adjust how the field is configured and used.
    /**
     * Returns the theoratical maximum credit that can be transfered during the player's session.
     * For example, in italy, the player is only supposed to add 1000 euro to their session. This
     * field returns either POSITIVE_INFINITY if no limit exists, or the actual value of the limit
     * (if we know it).
     * @public
     * @return {number}
     */
    getSessionMaxTransfer() {
        let limit = Number.POSITIVE_INFINITY;
        if (this._regulation) {
            if (this._regulation.country === "ITA") {
                /** @type {SessionInfoItaly} */
                let reg = this._regulation;
                limit = reg.regMoneyLimit;
            }
        }
        return limit;
    }


    //------------------------------------------------------------------------------
    // Methods to raise events
    //------------------------------------------------------------------------------
    /**
     * Dispatches the event notification, that a new game has started.
     * @private
     */
    notifyGameStarted() {
        this._events.emit(C_GameEvent.GAME_STARTED);
    }


    /**
     * Dispatches the event notification, that the game in progress has finished.
     * @private
     */
    notifyGameFinished() {
        this._events.emit(C_GameEvent.GAME_FINISHED);
    }


    /**
     * Dispatches the event notification, that any Bet Setting has changed.
     * @private
     */
    notifyBetChanged() {
        this._events.emit(C_GameEvent.BET_SETTINGS_CHANGED);
    }


    /**
     * Dispatches the event notification that Autoplay mode has started.
     * @private 
     */
    notifyAutoplayStarted() {
        this._events.emit(C_GameEvent.AUTOPLAY_STARTED);
    }


    /**
     * Dispatches the event notification that Autoplay mode has ended.
     * @private
     */
    notifyAutoplayEnded() {
        this._events.emit(C_GameEvent.AUTOPLAY_ENDED);
    }


    /**
     * Dispatches the event notification that anything related to Autoplay has
     * changed.
     * @private
     */
    notifyAutoplayChanged() {
        this._events.emit(C_GameEvent.AUTOPLAY_CHANGED);
    }


    /**
     * Dispatches the event notification that anything related to Player Wallet
     * (PlayerWallet, playerPoints, playerWinnings) has changed.
     * @private
     */
    notifyWalletChanged() {
        this._events.emit(C_GameEvent.WALLET_CHANGED);
    }


    /**
     * Dispatches the event notification, that anything related to Player Winnings
     * has changed.
     * @private
     */
    notifyWinningsChanged() {
        this._events.emit(C_GameEvent.WINNINGS_CHANGED);
    }


    /**
     * Dispatches the event notification, that anything related to Freespins has changed.
     * @private
     */
    notifyFreeSpinsChanged() {
        this._events.emit(C_GameEvent.FREESPINS_CHANGED);
    }


    /**
     * Dispatches the event notification that anything related to Player Superbet
     * has changed.
     * @private
     */
    notifySuperbetChanged() {
        this._events.emit(C_GameEvent.SUPERBET_CHANGED);
    }


    /**
     * Dispatches the event notification that a session has been opened.
     * @private
     */
    notifySessionOpened() {
        this._events.emit(C_GameEvent.SESSION_OPENED);
    }


    /**
     * Dispathes an event notification, informing everyone that session info has changed
     * (session may have closed / opened / been updated with a new ticket)
     * @private
     */
    notifySessionChanged() {
        this._events.emit(C_GameEvent.SESSION_CHANGED);
    }


    /**
     * Dispatches an event notification, informing everyone that the Session has been closed.
     * @private
     */
    notifySessionClosed() {
        this._events.emit(C_GameEvent.SESSION_CLOSED);
    }


    //------------------------------------------------------------------------------
    // Message processing functions
    //------------------------------------------------------------------------------
    // These are not part of the public API for consumption by the view layer, but
    // they are available publically for other parties within the business layer.
    // For this reason, they are marked "protected" (may be used by business layer,
    // not by View layer)
    //------------------------------------------------------------------------------
    /**
     * Processes Download Statistics data, returned by the server.
     * @param {DownloadStatisticsData} msg 
     */
    processDownloadStatisticsMsg(msg) {
        this._playerNickName = msg.playerNickName;
    };


    /**
     * Processes a Session Init Reply, extracting data from it about the session.
     * This method is marked as protected : it is for use only by the Business Logic
     * layer (not by the view layer).
     * @protected
     * @param {Gen2SessionInitReply} msg
     */
    processSessionInitMsg(msg) {
        log.debug(`Model.processSessionInitReply(${JSON.stringify(msg)})`);

        this._isSessionInProgress = true;
        this._isSessionReloaded = false;
        this._regulation = msg.regulation;
        
        // Sync real session values.
        this._sessionWallet = msg.currentWallet;
        this._sessionWinnings = msg.winningsAmount;
        this._sessionSuperBet = msg.superbetAmount;
        this._sessionNumFreeSpins = msg.numFreeSpins;

        this._sessionInactivityTimeout = msg.timeout;

        if (msg.promoGamesData) {
            this._promoGamesAward = msg.promoGamesData;
        }
        
        this.notifySessionOpened();
        this.notifySessionChanged();
    };


    /**
     * Processes an Add Credit Reply from the server, extract data from it regarding
     * the Session and the new Ticket.
     * This method is marked as protected : it is for use only by the Business Logic
     * layer (not by the view layer).
     * @protected
     * @param {Gen2AddCreditReply} msg 
     */
    processAddCreditMsg(msg) {
        log.info(`processAddCreditMsg(${JSON.stringify(msg)})`);
        this._regulation = msg.regulation;
        this._sessionWallet = msg.currentWallet;
        this.notifySessionChanged();
    };


    /**
     * Processes a Session Closed Reply from the server.
     * This method is marked as protected : it is for use only by the Business Logic
     * layer (not by the view layer).
     * @protected
     * @param {Object} msg 
     */
    processSessionClosedMsg(msg) {
        // Yes, we don't really care about the data in this response at present.
        // When a new Session is opened, we will clear various statistics - for now,
        // they are all left at the same state, so that we can relay information
        // about the session just closed back to the player. The Session Closed Reply
        // contains no other information of real use to the Game Client,.
        this._isSessionInProgress = false;

        this.notifySessionClosed();
        this.notifySessionChanged();
    };


    /**
     * Syncs the model to data contained in a Game Phase Results reply.
     * @protected
     * @param {GameResultPacket} msg 
     */
    processGameResultMsg(msg) {
        this._sessionWallet = msg.currentWallet;
        this._sessionWinnings = msg.winningsAmount;
        this._sessionSuperBet = msg.superbetAmount;
        this._sessionNumPromoGames = msg.numPromoGames;
        this._sessionNumFreeSpins = msg.numFreeSpins;

        this._events.emit(C_GameEvent.GAME_RESULT_PROCESSED);

        // TODO : "We could use a single event here.
        // Russell's note (12.03.2021): not sure what I originally meant by this:
        // I don't think that the event dispatch is currently important at this
        // moment in any case (the logGameStarted / logGameEnded events are nore
        // significant): in particular, because we are only changing "session"
        // values (eg: values according to the server: we almost never use these
        // in any part of the GUI, or show them to the player), no other part of
        // the game client needs to know about this.

        // this.notifyWalletChanged();
        // this.notifyWinningsChanged();
        // this.notifyFreeSpinsChanged();
        // this.notifyFreeGamesChanged();

        // TODO: This is rather a long looking log statement..
        log.info(`Model.processGameResultMsg(wallet:${this._sessionWallet},winnings:${this._sessionWinnings},superbet:${this._sessionSuperBet},NumFreeGames:${this._sessionNumPromoGames})`);
    };


    /**
     * This method is marked as protected : it is for use only by the Business Logic
     * layer (not by the view layer).
     * @protected
     * @param {RestoredGameResultPacket<GamePhaseResult>} msg 
     */
    processGameRestorationMsg(msg) {
        log.debug(`Model.processGameRestorationMsg(${JSON.stringify(msg)})`);

        this._isSessionInProgress = true;
        this._isSessionReloaded = true;

        // TODO: There is no hint that we have "regulation" data on the reply
        // yet, but it really does need to be there
        this._regulation = msg.regulation;

        // Regarding the following stats, we aren't currently returned any
        // information about them on game restoration: so we reset them to
        // a default value. This isn't correct, but until the game restoration
        // operation gets updated server side, there is nothing we can do
        // regarding this.
        // TODO: We can handle this info based (partly) on the regulation object
        // that we get back (although i don't yet know the api of the message)
        this._sessionCreditAdded = 0;
        this._sessionCreditSpent = 0;
        this._sessionTotalCreditWon = 0;
        this._sessionNumGamesPlayed = 0;
        this._sessionInactivityTimeout = DEFAULT_SESSION_INACTIVITY_TIMEOUT;    // TODO: This should come from the services data returned.

        // When restoring a game to a specific phase, we have to treat it the
        // same as if we were playing the phase for real. This means
        // - we update "session" win stats to their final values
        // - we update "player" win stats to their values before the phase started
        // For the Gen1 server platform, we can then let the Bonus phase (if that
        // is what we are restoring) handle the functionality of "increase winnings
        // to some midway point".

        // Update Session Win stats to their final values (eg: assume the phase result
        // has already been shown).
        this._sessionWallet = msg.currentWallet;
        this._sessionWinnings = msg.winningsAmount;
        this._sessionSuperBet = msg.superbetAmount;
        this._sessionNumPromoGames = msg.numPromoGames;
        this._sessionNumFreeSpins = msg.numFreeSpins;

        // Restore general game state
        
        // this._currencyType = msg.betSettings.currencyType; // TODO: Needs speccing correctly
        this._restoredGamePhase = msg.restoreGamePhase;
        this._restoredGameResult = msg.result;
        this._numHands = msg.betSettings.numHands;
        this._betPerLine = msg.betSettings.stakePerLine;
        this._numLines = msg.betSettings.numLines;
        this._specialWagerMultiplier = msg.betSettings.specialWagerMultiplier;
        this._gameInProgress = true;
        this._gameStats.totalBet = this.getTotalBet();
        this._gameStats.startWallet = msg.currentWallet + this._gameStats.totalBet; // value will already be reduced..



        // Update game win stats to their values before the phase has started at all.
        // This means "final win values" minus anything won in the actual phas result.
        // For the Gen2 platform, we always restart a phase right at the beginning. For
        // Gen 1 on bonus, we often restore to a midway point, but we will let the bonus
        // specific code handle adding the extra winnings (ie: it will work out how
        // much winnings from the bonus has already been shown, and add this manually)
        this._gameStats.winnings = msg.winningsAmount - msg.result.totalCreditWon;
        this._superbet = msg.superbetAmount - msg.result.totalSuperBetWon;

        // TODO: Do we need to do anything to restore "freeGames" / "promoGames" info ?
        
        this._numFreeSpins = msg.result.numFreeSpinsWon? msg.result.numFreeSpinsWon : msg.numFreeSpins; 


        log.debug(`Model.processGameRestorationMsg: sessionWallet = ${this._sessionWallet}`);
        log.debug(`Model.processGameRestorationMsg: sessionWinnings = ${this._sessionWinnings}`);
        log.debug(`Model.processGameRestorationMsg: sessionSuperBet = ${this._sessionSuperBet}`);
        log.debug(`Model.processGameRestorationMsg: sessionNumPromoGames = ${this._sessionNumPromoGames}`);
        log.debug(`Model.processGameRestorationMsg: sessionNumFreeSpins = ${this._sessionNumFreeSpins}`);
        log.debug(`Model.processGameRestorationMsg: restoredGamePhase = ${this._restoredGamePhase}`);
        log.debug(`Model.processGameRestorationMsg: restoredGameResult = ${JSON.stringify(this._restoredGameResult)}`);
        log.debug(`Model.processGameRestorationMsg: betNumHands = ${this._numHands}`);
        log.debug(`Model.processGameRestorationMsg: betPerLine = ${this._betPerLine}`);
        log.debug(`Model.processGameRestorationMsg: betNumLines = ${this._numLines}`);
        log.debug(`Model.processGameRestorationMsg: betSpecialWagerMultiplier = ${this._specialWagerMultiplier}`);
        log.debug(`Model.processGameRestorationMsg: totalBet = ${this.getTotalBet()}`);
        log.debug(`Model.processGameRestorationMsg: gameInProgress = ${this._gameInProgress}`);
        log.debug(`Model.processGameRestorationMsg: gameStats = ${JSON.stringify(this._gameStats)}`);
        log.debug(`Model.processGameRestorationMsg: gameStats = ${JSON.stringify(this._gameStats)}`);

        

        this.notifySessionOpened();
        this.notifySessionChanged();
    };


    /**
     * Processes a Fun Bonus Activation reply received from the server.
     * This method is marked as protected : it is for use only by the Business Logic
     * layer (not by the view layer).
     * @protected
     * @param {Object} msg 
     */
    processFunBonusActivationMsg(msg) {
        // TODO: implement when the specification is clear!
    };
}

export default Model;

function generateGameClientStartDateString()
{
    var startDate = new Date();
    var result = "";
    result += StringUtil.zeroPadString(startDate.getUTCHours().toString(), 2);
    result += StringUtil.zeroPadString(startDate.getUTCMinutes().toString(), 2);
    result += StringUtil.zeroPadString(startDate.getUTCSeconds().toString(), 2);
    return result;
}

function generateGameClientStartTimeString()
{   
    var startDate = new Date();
    var date = startDate.getDate().toString();
    var month = Number(startDate.getMonth() + 1).toString();
    var result;

    if (date.length == 1) date = "0" + date;
    if (month.length == 1) month = "0" + month;

    result = startDate.getFullYear().toString() + month + date;

    return result;
}