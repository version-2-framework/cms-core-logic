// TODO: Pick a better name ? One that implies "mutability"
// TODO: add setters ?
/**
 * Mutable implementation of Bet Settings for a Slot game.
 */
export class MutableSlotBetSettings
{
    /**
     * 
     * @param {number} numHands 
     * @param {number} numLines 
     * @param {number} betPerLine 
     */
    constructor(numHands, numLines, betPerLine)
    {
        this._numHands = numHands;
        this._numLines = numLines;
        this._betPerLine = betPerLine;
    }

    getTotalBet() {
        return this.getStakePerHand() * this.getNumHands();
    }


    getNumHands() {
        return this._numHands;
    }


    getNumLines() {
        return this._numLines;
    }


    getBetPerLine() {
        return this._betPerLine;
    }


    getStakePerHand() {
        return this._betPerLine * this._numLines;
    }


    /**
     * @inheritDoc
     */
    toString() {
        return `{numHands:${this.getNumHands()},numLines:${this.getNumLines()},betBerLine:${this.getBetPerLine()}}}}`;
    }
}


/**
 * 
 * @param {SlotBetSettings} betSettings 
 */
export function calculateTotalBet(betSettings)
{
    return betSettings.numHands * betSettings.numLines * betSettings.stakePerLine;
}