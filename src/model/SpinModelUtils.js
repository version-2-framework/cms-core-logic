import Rng from '../maths/RngSingleton';
import * as C_RewardGroup from "../const/C_RewardGroup";

export class SpinModelUtils
{
    /**
     * @param {{ config:SlotConfig }} dependencies 
     */
    constructor(dependencies)
    {
        /**
         * @private
         * @type {SlotConfig}
         */
        this._slotConfig = dependencies.config;

        /**
         * The number of symbols per reel - from the point of view of symbol parsing.
         * @private
         * @type {number[]}
         */
        this._numSymsPerReel = this._slotConfig.numSymsPerReelInData ?
            this._slotConfig.numSymsPerReelInData : this._slotConfig.numSymsPerReel;
    };
    

    /**
     * Generates an arbitrary set of Symbols, which conform to the rules specified in
     * parameter slotConfig. This is useful when first loading a GameClient (and we need
     * some symbols to show, regardless of what they are).
     * @public
     * @return {SymbolsMatrix}
     */
    generateRandomSymbolsMatrix()
    {
        let slotConfig = this._slotConfig;

        /** @type {SymbolsMatrix} */
        let symbolsMatrix = [];

        let symbols = Object.values(slotConfig.symbols);

        for (let handIndex = 0; handIndex < slotConfig.numHands; handIndex ++) {
            let symbolsForHand = [];
            for (let reelIndex = 0; reelIndex < slotConfig.numReels; reelIndex ++) {
                let symbolsForReel = [];
                let numSymToGenerate = this._numSymsPerReel[reelIndex];

                if (slotConfig.reelbands) {
                    let startPos = 0;
                    let reelband = slotConfig.reelbands[reelIndex];
                    let numSymInReelband = reelband.length;
                    for (let symIndex = 0; symIndex < numSymToGenerate; symIndex ++) {
                        let posInReelband = (startPos + symIndex) % numSymInReelband;
                        let symbolId = reelband[posInReelband];
                        let symbolConfig = symbols[symbolId];
                        symbolsForReel.push(symbolConfig);
                    }
                }
                else
                {
                    for (let symIndex = 0; symIndex < numSymToGenerate; symIndex ++) {
                        let randomSymbolIndex = Rng.getRandom(symbols.length);
                        let randomSymbol = symbols[randomSymbolIndex];
                        symbolsForReel.push(randomSymbol);
                    }
                }

                symbolsForHand.push(symbolsForReel);
            }

            symbolsMatrix.push(symbolsForHand);
        }

        return symbolsMatrix;
    };


    // TODO: This method needs to function correctly with respect to the recent changes
    // that have been done for Gen1 / Gen2 support. HoldsPattern is now better defined,
    // and this needs to support it. A SlotConfig setting is going to be required for
    // that.
    /**
     * Generates an arbitrary, empty Holds Pattern, whose size will confirm to the rules
     * specified in parameter slotConfig. The pattern will represent "nothing held".
     * @public
     * @return {HoldsPattern}
     */
    generateEmptyHoldsPattern()
    {
        let slotConfig = this._slotConfig;
        let type = slotConfig.holdsMode, pattern;

        if (slotConfig.holdsMode === "holdReels") {
            pattern = [];

            for (let reelIndex = 0; reelIndex < slotConfig.numReels; reelIndex ++) {
                pattern.push(false);
            }
        }
        else
        if (slotConfig.holdsMode === "holdSymbols") {
            pattern = [];

            for (let reelIndex = 0; reelIndex < slotConfig.numReels; reelIndex ++) {
                let numSymOnReel = this._numSymsPerReel[reelIndex];
                let reelHolds = [];
                for (let posIndex = 0; posIndex < numSymOnReel; posIndex ++) {
                    reelHolds.push(false);
                }

                pattern.push(reelHolds);
            }
        }
        
        return { type, pattern };
    };


    /**
     * Generates a blank map for a single hand.
     * @public
     * @return {boolean[][]}
     */
    generateBlankSingleHandMap()
    {
        let slotConfig = this._slotConfig;
        let numReels = slotConfig.numReels;
        let numSymPerReel = this._numSymsPerReel;

        let handData = [];
        for (let reelIndex = 0; reelIndex < numReels; reelIndex ++) {
            let reelData = [];
            let numPos = numSymPerReel[reelIndex];
            for (let posIndex = 0; posIndex < numPos; posIndex ++) {
                reelData.push(false);
            }
            handData.push(reelData);
        }

        return handData;
    };

    
    /**
     * Generates a blank boolean map, that is the correct size for the game.
     * @public
     * @return {boolean[][][]}
     */
    generateBlankMap() {
        let slotConfig = this._slotConfig;
        let numHands = slotConfig.numHands;
        
        let map = [];
        for (let handIndex = 0; handIndex < numHands; handIndex ++) {
            map.push(this.generateBlankSingleHandMap());
        }

        return map;
    };


    /**
     * Compares 2 boolean reel maps, and returns a new boolean reel map. A position in
     * the new map will be true if
     * - the value at that position has changed
     * - the value was not previously true
     * @param {boolean[][][]} map1 
     * @param {boolean[][][]} map2
     * @return {boolean[][][]}
     */
    findAddedPositions(map1, map2) {
        return this.compareMaps(map1, map2, (map1Val, map2Val) => {
            return (map2Val && !map1Val);
        });
    };


    /**
     * @public
     * @param {boolean[][][]} map1 
     * @param {boolean[][][]} map2
     * @param {(map1Val : boolean, map2Val: boolean) => boolean} includeInOutput
     * @return {boolean[][][]}
     */
    compareMaps(map1, map2, includeInOutput) {
        let diffMap = this.generateBlankMap();

        this.symbolsIterator((handIndex, reelIndex, posIndex) => {
            let map1Value = map1[handIndex][reelIndex][posIndex];
            let map2Value = map2[handIndex][reelIndex][posIndex];
            let include = includeInOutput(map1Value, map2Value);
            diffMap[handIndex][reelIndex][posIndex] = include;
        });

        return diffMap;
    };


    /**
     * For a SymbolsMatrix, generates a boolean map (with the same dimensions as the
     * input SymbolsMatrix), which indicates which symbols have a given id.
     * @param {number} targetSymbolId 
     * @param {SymbolsMatrix} symbolsMatrix 
     * @return {boolean[][][]}
     */
    getMapOfSymbolsMatchingId(targetSymbolId, symbolsMatrix) {
        return this.getMapOfSymbolsMatching(symbolsMatrix, sym => sym.id === targetSymbolId);
    };


    /**
     * @public
     * @param {SymbolsMatrix} symbolsMatrix 
     * @param {(symbol : SymbolConfig) => boolean} isMatching
     * @return {boolean[][][]}
     */
    getMapOfSymbolsMatching(symbolsMatrix, isMatching)
    {
        let map = this.generateBlankMap();

        this.symbolsIterator((handIndex, reelIndex, posIndex) => {
            let symbol = symbolsMatrix[handIndex][reelIndex][posIndex];
            let matches = isMatching(symbol);
            map[handIndex][reelIndex][posIndex] = matches;
        });

        return map;
    };


    /**
     * @callback SymbolsIteratorCallback
     * @param {number} handIndex
     * The index of the hand that is currently being processed.
     * @param {number} reelIndex
     * The index of the reel (relative to current hand) that is currently being processed.
     * @param {number} symIndex
     * The index of the symbol (relative to the current reel) that is currently being processed.
     * EG: if Reel N has 4 symbols, this will have a range of 0 to 3 when Reel N is being processed.
     * @param {number} handPosIndex
     * The overall index of the symbol that is currently being processed, relative to the current
     * hand. EG: if the current hand has 15 symbols, this will have a range of 0 to 14.
     */


    /**
     * Invokes a special iterator, which will traverse all symbols within the standard
     * symbols matrix for the game. A callback is executed, which is passed the indices
     * of hand, reel and position within the reel.
     * @public
     * @param {SymbolsIteratorCallback} callback
     * A callback which will be executed for all possible positions in a symbol matrix.
     */
    symbolsIterator(callback)
    {
        let slotConfig = this._slotConfig;
        let numHands = slotConfig.numHands;
        let numReels = slotConfig.numReels;
        let numSymPerReel = this._numSymsPerReel;

        for (let handIndex = 0; handIndex < numHands; handIndex ++) {
            let handPosIndex = 0;
            for (let reelIndex = 0; reelIndex < numReels; reelIndex ++) {
                let numSymbolsOnReel = numSymPerReel[reelIndex];
                for (let symIndex = 0; symIndex < numSymbolsOnReel; symIndex ++) {
                    callback(handIndex, reelIndex, symIndex, handPosIndex);
                    handPosIndex += 1;
                }
            }
        }
    };


    /**
     * Generates a map of all symbols from a spin result, which are involved in a Symbol
     * Win that with Reward Group of CREDIT.
     * @public
     * @param {SpinResult} spinResult 
     * @return {boolean[][][]}
     */
    getMapOfSymbolsInvolvedInCreditWin(spinResult) {
        return this.getMapOfSymbolsInvolvedInWinFor(spinResult, C_RewardGroup.CREDIT);
    };


    /**
     * Returns a map of all symbols from a spin result, which are involved in any kind of
     * Symbol Win.
     * @public
     * @param {SpinResult} spinResult 
     * @return {boolean[][][]}
     */
    getMapOfSymbolsInvolvedInAnyWin(spinResult) {
        return this.getMapOfSymbolsInvolvedInWinFor(spinResult);
    };


    /**
     * Generates a map of all symbols from a SpinResult, which are involved in a Symbol
     * Win which has a specific reward group.
     * @param {SpinResult} spinResult 
     * @param {number|null} targetRewardGroup 
     * The target reward group. If this parameter is not specified, then the map returned
     * will not be reward group specified, but will allow through any winning symbols found.
     * @return {boolean[][][]}
     */
    getMapOfSymbolsInvolvedInWinFor(spinResult, targetRewardGroup=null)
    {
        let map = this.generateBlankMap();

        spinResult.symbolWins.forEach(symbolWin => {
            if (symbolWin.rewardGroup === targetRewardGroup || targetRewardGroup === null) {
                let handIndex = symbolWin.handId - 1;
                let numWinPos = symbolWin.winningReels.length;
                for (let i = 0; i < numWinPos; i ++) {
                    let reelIndex = symbolWin.winningReels[i] - 1;
                    let posIndex = symbolWin.winningPositions[i] - 1;

                    map[handIndex][reelIndex][posIndex] = true;
                }
            }
        });

        return map;
    };


    /**
     * Returns a list of all winlines ids that have wins with RewardGroup CREDIT. The list is
     * sub-divided per hand.
     * @public
     * @param {SpinResult} spinResult 
     * @return {number[][]}
     */
    getWinlinesIdsInvolvedInCreditWin(spinResult) {
        return this.getWinlineIdsInvolvedInWinFor(spinResult, C_RewardGroup.CREDIT);
    };


    /**
     * Returns a list of all winlines ids that have wins of a specific RewardGroup. The list is
     * sub-divided per hand.
     * @public
     * @param {SpinResult} spinResult
     * @param {number} rewardGroup
     * @return {number[][]}
     */
    getWinlineIdsInvolvedInWinFor(spinResult, rewardGroup) {
        let winlineIds = [];

        for (let handIndex = 0; handIndex < this._slotConfig.numHands; handIndex ++) {
            winlineIds.push([]);
        };

        spinResult.symbolWins.forEach(symbolWin => {
            if (symbolWin.lineId > 0 && symbolWin.rewardGroup === rewardGroup) {
                let handIndex = symbolWin.handId - 1;
                if (winlineIds[handIndex].indexOf(symbolWin.lineId) === -1) {
                    winlineIds[handIndex].push(symbolWin.lineId);
                }
            }
        });

        winlineIds.forEach(hand => {
            hand.sort();
        });

        return winlineIds;
    };


    /**
     * Fetches the total amount won for a specific reward group, from a Spin Result.
     * @public
     * @param {SpinResult} spinResult 
     * @param {number} rewardGroup
     * @return {number}
     */
    getRewardGroupWinningsFromSpinResult(spinResult, rewardGroup) {
        let totalWon = 0;

        spinResult.symbolWins.forEach(symbolWin => {
            if (symbolWin.rewardGroup === rewardGroup) {
                totalWon += symbolWin.rewardValue;
            }
        });

        spinResult.specialWins.forEach(specialWin => {
            if (specialWin.rewardGroup === rewardGroup) {
                totalWon += specialWin.rewardValue;
            }
        });

        return totalWon;
    };
}