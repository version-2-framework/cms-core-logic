import { getAppLogger } from "../logging/LogManager";
import { GamePhaseResultModel as GamePhaseResultModel } from "./GamePhaseResultModel";

const log = getAppLogger();

/**
 * Base implementation for Gen 2 Spin Model class.
 * 
 * This should implement an interface which is common to the Gen 1 model - which means
 * a bit of code duplication, but given the differences between older and new standard
 * maths (and the fact that we also have common data for Gen 2, between all phase results),
 * i think i prefer that approach.
 * 
 * @implements {MutableSpinPhaseModel}
 */
export default class SpinPhaseModel extends GamePhaseResultModel
{
    /**
     * @param {Dependencies} dependencies 
     */
    constructor(dependencies)
    {
        super();

        /**
         * @private
         */
        this._slotConfig = dependencies.config;

        /**
         * @private
         */
        this._spinUtils = dependencies.spinModelUtils;

        /**
         * @private
         */
        this._presentationBuilder = dependencies.spinPresentationBuilder;

        /**
         * Current active holds pattern for the player.
         * @private
         */
        this._playerHoldsPattern = this._spinUtils.generateEmptyHoldsPattern();

        /**
         * Symbols shown at the start of the Spin Phase that we currently have results for.
         * @private
         * @type {SymbolsMatrix}
         */
        this._startSymbolIds = null;

        /**
         * Symbols shown at the end of the Spin Phase that we currently have results for.
         * These will be usually be the Symbols from the final Spin of the Spin Phase, when
         * its a multi-spin phase.
         * @private
         * @type {SymbolsMatrix}
         */
        this._finalSymbolIds = this._spinUtils.generateRandomSymbolsMatrix();

        /**
         * @private
         * @type {SpinPhaseResult | FreeSpinPhaseResult}
         */
        this._spinPhaseResults = null;

        /**
         * @private
         * @type {boolean}
         */
        this._playSpin2 = false;

        /**
         * @private
         * @type {boolean}
         */
        this._hasBonusWin = false;

        /**
         * @private
         * @type {boolean}
         */
        this._hasFreeSpinWin = false;

        /**
         * @private
         * @type {"singleSpin" | "spin1" | "spin2" | "freeSpin" | "quickPhase"}
         */
        this._phaseType = null;
    };


    /**
     * @public
     * @inheritDoc
     */
    clearCachedData()
    {
        this._phaseType = null;
        this._hasFreeSpinWin = false;
        this._hasBonusWin = false;
        this._playSpin2 = false;
        this._spinPhaseResults = null;
        this._startSymbolIds = null;
        this._playerHoldsPattern = this._spinUtils.generateEmptyHoldsPattern();
    };


    /**
     * @public
     * @inheritDoc
     */
    clearHoldsPattern()
    {
        this._playerHoldsPattern = this._spinUtils.generateEmptyHoldsPattern();
    };


    // TODO: We only have to do this, because the internal data structure doesn't
    // yet include the "spin2StartSymbolIds" or "finalSymbolIds" fields, which it
    // should
    /**
     * @private
     * @param {{rounds:SpinRoundResult[]}} result
     * @return {SymbolsMatrix}
     */
    getFinalSymbolIdsFromResult(result)
    {
        let numRounds = result.rounds.length;
        let finalRound = result.rounds[numRounds-1];
        let numSpins = finalRound.spins.length;
        let finalSpin = finalRound.spins[numSpins-1];
        let finalSymbolIds = finalSpin.finalSymbolIds;
        return finalSymbolIds;
    };


    /**
     * @public
     * @param {GameResultPacket<SingleSpinPhaseResult>} resultPacket 
     */
    setSingleSpinResult(resultPacket)
    {
        this.setPhaseResults(resultPacket);

        this._phaseType = "singleSpin";
        this._spinPhaseResults = resultPacket.result;
        this._playerHoldsPattern = null;
        this._playSpin2 = false;
        this._hasBonusWin = resultPacket.result.hasBonusWin;
        this._hasBonusWinPerHand = resultPacket.result.hasBonusWinPerHand;
        this._hasFreeSpinWin = resultPacket.result.numFreeSpinsWon > 0;
        this._spinPresentation = this._presentationBuilder.buildPresentation(resultPacket.result, resultPacket.betSettings);
        this._finalSymbolIds = this.getFinalSymbolIdsFromResult(resultPacket.result);
    };


    /**
     * @public
     * @param {GameResultPacket<Spin1PhaseResult>} resultPacket
     */
    setSpin1Result(resultPacket)
    {
        this.setPhaseResults(resultPacket);

        let spin1PhaseResult = resultPacket.result;

        this._phaseType = "spin1";
        this._spinPhaseResults = spin1PhaseResult;
        this._playerHoldsPattern = spin1PhaseResult.suggestedAutoholds;
        this._playSpin2 = resultPacket.result.playSpin2;
        this._hasBonusWin = resultPacket.result.hasBonusWin;
        this._hasBonusWinPerHand = resultPacket.result.hasBonusWinPerHand;
        this._hasFreeSpinWin = resultPacket.result.numFreeSpinsWon > 0;
        this._spinPresentation = this._presentationBuilder.buildPresentation(resultPacket.result, resultPacket.betSettings);
        this._finalSymbolIds = this.getFinalSymbolIdsFromResult(resultPacket.result);
    };


    /**
     * @public
     * @param {GameResultPacket<Spin2PhaseResult>} resultPacket
     */
    setSpin2Result(resultPacket)
    {
        this.setPhaseResults(resultPacket);

        let spin2PhaseResult = resultPacket.result;

        this._phaseType = "spin2";
        this._spinPhaseResults = spin2PhaseResult;
        this._playerHoldsPattern = spin2PhaseResult.holdsUsed;
        this._playSpin2 = false;
        this._hasBonusWin = resultPacket.result.hasBonusWin;
        this._hasBonusWinPerHand = resultPacket.result.hasBonusWinPerHand;
        this._hasFreeSpinWin = resultPacket.result.numFreeSpinsWon > 0;
        this._spinPresentation = this._presentationBuilder.buildPresentation(resultPacket.result, resultPacket.betSettings);
        this._finalSymbolIds = this.getFinalSymbolIdsFromResult(resultPacket.result);
    };


    /**
     * @public
     * @param {GameResultPacket<FreeSpinPhaseResult>} resultPacket 
     */
    setFreeSpinResult(resultPacket)
    {
        this.setPhaseResults(resultPacket);

        this._phaseType = "freeSpin";
        this._spinPhaseResults = resultPacket.result;
        this._playerHoldsPattern = null;
        this._playSpin2 = false;
        this._hasBonusWin = false;
        this._hasBonusWinPerHand = null;
        this._hasFreeSpinWin = false;
        this._spinPresentation = this._presentationBuilder.buildPresentation(resultPacket.result, resultPacket.betSettings);
        this._finalSymbolIds = this.getFinalSymbolIdsFromResult(resultPacket.result);
    };

    /**
     * 
     * @param {GameResultPacket<QuickPhaseResult>} resultPacket 
     */
    setQuickPhaseResult(resultPacket) 
    {
        this.setPhaseResults(resultPacket);

        this._phaseType = "quickPhase";
        this._spinPhaseResults = resultPacket.result;

        this._playerHoldsPattern = null;
        this._playSpin2 = false;
        this._hasBonusWin = resultPacket.result.hasBonusWin;
        this._hasBonusWinPerHand = resultPacket.result.hasBonusWinPerHand;
        this._hasFreeSpinWin = false;
    }

    /**
     * @public
     * @param {SymbolsMatrix} symbolIds 
     */
    setFinalSymbolIdsAfterBonus(symbolIds)
    {
        this._finalSymbolIds = symbolIds;
    };


    
    
    
    //--------------------------------------------------------------------------------------------------
    // Getters
    //--------------------------------------------------------------------------------------------------
    /**
     * Indicates if the current spin phase is a Single Spin phase.
     * @public
     * @return {boolean}
     */
    getIsSingleSpinPhase() {
        return this._phaseType === "singleSpin";
    };


    /**
     * Indicates if the current spin phase is a Spin 1 phase.
     * @public
     * @return {boolean}
     */
    getIsSpin1Phase() {
        return this._phaseType === "spin1";
    };


    /**
     * Indicates if the current spin phase is a Spin 2 phase.
     * @public
     * @return {boolean}
     */
    getIsSpin2Phase() {
        return this._phaseType === "spin2";
    };
    

    /**
     * Indicates if the current spin phase is a FreeSpin phase.
     * @public
     * @return {boolean}
     */
    getIsFreeSpinPhase() {
        return this._phaseType === "freeSpin";
    };


    /**
     * Returns the current Spin Result Model to display.
     * @public
     * @return {SpinPhaseResult}
     */
    getPhaseResults() {
        return this._spinPhaseResults;
    };


    /**
     * Returns the Symbol Ids to show, at the start of the current Spin Phase.
     * @public
     * @return {SymbolsMatrix}
     */
    getStartSymbolIds() {
        return this._startSymbolIds;
    };


    /**
     * Returns the Symbol Ids to show, at the end of the current Spin Phase.
     * @public
     * @return {SymbolsMatrix}
     */
    getFinalSymbolIds() {
        return this._finalSymbolIds;
    };


    /**
     * Returns the current value of player holds pattern. When Spin 1 results are
     * returned, this will be initialized to the same value as the auto-holds pattern
     * returned by the server. The player may change the value of holds used: the
     * value of this field will therefore be changed (and the new value will be sent
     * to the server when we request Spin 2 results). The player holds pattern is also
     * returned in Spin 2 results (alongside autoholds pattern), so in the event of
     * game restore, we can show the actual holds pattern applied for second spin.
     * 
     * Because we use the same holds pattern applied across all hands (for a multi
     * hand game), this pattern is only as long as the number of reelbands that have
     * per hand.
     * @public
     * @return {HoldsPattern}
     */
    getPlayerHoldsPattern() {
        return this._playerHoldsPattern;
    };


    /**
     * @public
     * @inheritDoc
     * @param {number} handIndex 
     * @param {number} reelIndex 
     * @param {boolean} shouldBeHeld 
     */
    setReelHoldState(handIndex, reelIndex, shouldBeHeld) {
        if (this._playerHoldsPattern.type === "holdReels") {
            // TODO: This assumes the pattern must be the same on all hands
            this._playerHoldsPattern.pattern.forEach(handPattern => {
                handPattern[reelIndex] = shouldBeHeld;
            });
        }
        // TODO: Does not yet take into account "perSymbol" patterns
    };


    /**
     * @public
     * @inheritDoc
     * @param {number} handIndex 
     * @param {number} reelIndex 
     */
    toggleReelHoldState(handIndex, reelIndex) {
        if (this._playerHoldsPattern.type === "holdReels") {
            // TODO: This assumes the pattern must be the same on all hands
            this._playerHoldsPattern.pattern.forEach(handPattern => {
                let oldState = handPattern[reelIndex];
                let newState = !oldState;
                handPattern[reelIndex] = newState;
            });
        }
        // TODO: Does not yet take into account "perSymbol" patterns
    };


    /**
     * Indicates if a Bonus Win exists in these results.
     * @public
     * @return {boolean}
     */
    hasBonusWin() {
        return this._hasBonusWin;
    };


    /**
     * @public
     * @return {boolean[]}
     */
    hasBonusWinPerHand() {
        return this._hasBonusWinPerHand;
    };


    /**
     * Indicates if any FreeSpin win was present in the last result.
     * @public
     * @return {boolean}
     */
    hasFreeSpinWin() {
        return this._hasFreeSpinWin;
    };


    /**
     * Indicates if a second Spin is required.
     * @public
     * @return {boolean}
     */
    getIsSpin2Required() {
        return this._playSpin2;
    };


    /**
     * @public
     * @return {SpinPhaseResultPresentation}
     */
    getResultsPresentation() {
        return this._spinPresentation;
    };


    /**
     * @public
     * @inheritDoc
     * @param {number} roundIndex
     * @return {SpinRoundResult}
     */
    getRoundResult(roundIndex) {
        /** @type {SpinRoundResult} */
        let round;

        if (this._spinPhaseResults) {
            let allRounds = this._spinPhaseResults.rounds;
            if (allRounds) {
                let numRounds = allRounds.length;
                if (roundIndex < numRounds) {
                    round = allRounds[roundIndex];
                }
                else round = allRounds[numRounds - 1];
            }
        }

        return round;
    };


    /**
     * @public
     * @inheritDoc
     * @param {number} roundIndex
     * @param {number} spinIndex
     * @return {SpinResult}
     */
    getSpinResult(roundIndex, spinIndex) {
        /** @type {SpinResult} */
        let spin;

        let currRound = this.getRoundResult(roundIndex)
        if (currRound) {
            let allSpins = currRound.spins;
            if (allSpins) {
                let numSpins = allSpins.length;
                if (spinIndex < numSpins) {
                    spin = allSpins[spinIndex];
                }
                else spin = allSpins[numSpins - 1];
            }
        }

        return spin;
    };


    /**
     * @public
     * @inheritDoc
     * @param {number} roundIndex
     * @return {SpinRoundResultPresentation}
     */
    getRoundPresentation(roundIndex) {
        /** @type {SpinRoundResultPresentation} */
        let round;

        if (this._spinPresentation) {
            let allRounds = this._spinPresentation.rounds;
            if (allRounds) {
                let numRounds = allRounds.length;
                if (roundIndex < numRounds) {
                    round = allRounds[roundIndex];
                }
                else
                {
                    round = allRounds[numRounds - 1];
                }
            }
        }

        return round;
    };

    
    /**
     * @public
     * @inheritDoc
     * @param {number} roundIndex 
     * @param {number} spinIndex 
     * @return {SpinResultPresentation}
     */
    getSpinPresentation(roundIndex, spinIndex)
    {
         /** @type {SpinResultPresentation} */
        let spin;

        let currRound = this.getRoundPresentation(roundIndex);
        if (currRound)
        {
            let allSpins = currRound.spins;
            if (allSpins)
            {
                let numSpins = allSpins.length;
        
                if (spinIndex < numSpins) {
                    spin = allSpins[spinIndex];
                }
                else spin = allSpins[numSpins - 1];
            }
        }

        return spin;
    };
};