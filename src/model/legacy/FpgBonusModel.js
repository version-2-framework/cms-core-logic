import C_FpgMessageField from "../../const/legacy/C_FpgMessageField";
import C_FpgBonusWintype from "../../const/legacy/C_FpgBonusWintype";
import * as Parse from "../../utils/ParseUtil";
import * as C_MessageField from "../../const/C_MessageField";
import * as ResultType from "../../const/C_ServiceResponseTypeGen1";
import { CmsRng } from "../../maths/CmsRng";

const MAX_NUM_SCATTERS = 5;

class FpgBonusModel
{
    /**
     * 
     * @param {Dependencies} dependencies 
     */
    constructor(dependencies)
    {
        /**
         * @private
         * @type {boolean}
         */
        this._isReloaded = false;

        /**
         * @private
         * @type {number[]}
         */
        this._scatterPositions = null;

        /**
         * @private
         * @type {number}
         */
        this._prizeValueLow = 0;

        /**
         * @private
         * @type {number}
         */
        this._prizeValueMedium = 0;

        /**
         * @private
         * @type {number}
         */
        this._prizeValueHigh = 0;

        /**
         * @private
         * @type {number}
         */
        this._prizeValueBigWin = 0;

        /**
         * The id of the scatter selected.
         * @todo:
         * Is it scatter or index ??
         * @private
         * @type {number}
         */
        this._scatterSelected = 0;

        /**
         * @private
         * @type {string}
         */
        this._wintype = "";

        /**
         * @private
         * @type {number}
         */
        this._totalWinnings = 0;

        /**
         * @private
         * @type {boolean}
         */
        this._resultsAreAvailable = false;
    };


    /**
     * Sets the Bonus Results message to be used.
     * @param {Object} msg 
     */
    setBonusResults(msg) {
        let msgType = Parse.extractString(C_MessageField.MESSAGE_TYPE, msg);
        if (msgType === ResultType.BONUS_2_SPIN_PHASE_RESULTS) {
            this.processBonusResultsReply(msg);
        }
        else if (msgType === ResultType.BONUS_2_SPIN_SINGLE_SELECTION) {
            this.processBonusSelectionReply(msg);
        }
    };


    /**
     * Processes a Bonus Results Reply. As noted, for FPG this actually doesnt contain
     * the results of the bonus phase (!!). Only the general configuration data.
     * @internal
     * @param {Object} msg 
     */
    processBonusResultsReply(msg) {
        let isReloadMsg = Parse.extractBoolean(C_MessageField.IS_GAME_RELOAD_MSG, msg);
        this.extractBonusConfigData(msg);
        this._scatterSelected = 0;
        this._wintype = "";
        this._totalWinnings = 0;
        this._isReloaded = isReloadMsg;
        this._resultsAreAvailable = false;
    };


    /**
     * For Fowl Play Gold, we do not know the results of the Bonus Phase, until
     * we get the Bonus Selection Reply ( bizarrely!! ). Fowl Play Gold was the
     * first of the WMG online games developed, and as such, the protocols used
     * to develop its mechanics were quite badly thought out in some areas. It's
     * never been clear why the request which was explicitly called "Bonus Results"
     * actually didn't contain any results at all: certainly, this is bizarre
     * naming.
     * @internal
     * @param {Object} msg 
     */
    processBonusSelectionReply(msg) {
        let isReloadMsg = Parse.extractBoolean(C_MessageField.IS_GAME_RELOAD_MSG, msg);
        
        if (isReloadMsg) {
            this.extractBonusConfigData(msg);
            this._isReloaded = isReloadMsg;
            this._scatterSelected = Parse.extractNumber(C_MessageField.BONUS_LAST_SELECTION, msg);
        }

        this._wintype = Parse.extractString(C_MessageField.BONUS_WIN_TYPE, msg);
        
        switch (this._wintype)
        {
            case C_FpgBonusWintype.LOW:
                this._totalWinnings = this._prizeValueLow;
                break;
            case C_FpgBonusWintype.MEDIUM:
                this._totalWinnings = this._prizeValueMedium;
                break;
            case C_FpgBonusWintype.HIGH:
                this._totalWinnings = this._prizeValueHigh;
                break;
            case C_FpgBonusWintype.BIG_WIN:
                this._totalWinnings = this._prizeValueBigWin;
                break;
        }
        
        this._resultsAreAvailable = true;
    };


    /**
     * Extracts common data relating to Bonus Phase ( the data that is available from both
     * a Bonus Results Reply, and a Bonus Selection Reply ).
     * @private
     * @param {Object} msg 
     */
    extractBonusConfigData(msg) {
        this._scatterPositions = Parse.extractNumberArray(C_MessageField.BONUS_SCATTER_POSITIONS, msg);
        this._prizeValueLow = Parse.extractNumber(C_FpgMessageField.BONUS_PRIZE_LOW, msg);
        this._prizeValueMedium = Parse.extractNumber(C_FpgMessageField.BONUS_PRIZE_MEDIUM, msg);
        this._prizeValueHigh = Parse.extractNumber(C_FpgMessageField.BONUS_PRIZE_HIGH, msg);
        this._prizeValueBigWin = Parse.extractNumber(C_FpgMessageField.BONUS_PRIZE_BIG_WIN, msg);
    };


    //--------------------------------------------------------------------------------------------------
    //--------------------------------------------------------------------------------------------------
    /**
     * Sets the id of the "scatter" that the player selected in the bonus.
     * @public
     * @param {number} scatterId
     * The id of the scatter to select ( should be in range 1 to 5 ).
     */
    makeSelection(scatterId) {
        this._scatterSelected = scatterId;
    };


    /**
     * Returns a random selection id. This is ensured either to be valid ( from the data available ),
     * or 0 ( in the event that this is called, and no data is available ).
     * @public
     * @return {number}
     */
    getRandomSelectionId() {
        let randomScatterId = 0;
        if (this._scatterPositions !== null) {
            let randomIndex = new CmsRng().getRandom(MAX_NUM_SCATTERS);
            for (let i = 0; i < MAX_NUM_SCATTERS; i ++) {
                let index = (randomIndex + i) % MAX_NUM_SCATTERS;
                let positionHasScatter = this._scatterPositions[index];
                if (index < this._scatterPositions.length && positionHasScatter) {
                    randomScatterId = index + 1;
                    break;
                }
            }

            // Fallback, in case the "random" routine didn't find
            // a valid scatterId: return the first acceptable one
            // that exists.
            if (randomScatterId === 0) {
                for (let i = 0; i < this._scatterPositions.length; i ++) {
                    if (this._scatterPositions[i]) {
                        randomScatterId = i + 1;
                        break;
                    }
                }
            }
        }
        return randomScatterId;
    };


    //--------------------------------------------------------------------------------------------------
    // Getters
    //--------------------------------------------------------------------------------------------------
    /**
     * Indicates if "wintype" results data is available.
     * @public
     * @return {boolean}
     */
    areResultsAvailable() {
        return this._resultsAreAvailable;
    };


    /**
     * Returns total winnings for the Bonus Phase. This information is only
     * available after "processSelectionReply" has been called.
     * @public
     * @return {number}
     */
    getTotalWinnings() {
        return this._totalWinnings;
    };


    /**
     * Returns the wintype Enum for the bonus phase. This can be checked
     * against the constant values in const/legacy/C_FpgBonusWintype.
     * @public
     * @return {string}
     */
    getWintype() {
        return this._wintype;
    };


    /**
     * Returns the value associated with the Low prize.
     * @public
     * @return {number}
     */
    getPrizeValueLow() {
        return this._prizeValueLow;
    };


    /**
     * Returns the value associated with the Medium prize.
     * @public
     * @return {number}
     */
    getPrizeValueMedium() {
        return this._prizeValueMedium;
    };


    /**
     * Returns the value associated with the High prize.
     * @public
     * @return {number}
     */
    getPrizeValueHigh() {
        return this._prizeValueHigh
    };

    
    /**
     * Returns the value associated with the Big Win prize.
     * @public
     * @return {number}
     */
    getPrizeValueBigWin() {
        return this._prizeValueBigWin;
    };


    /**
     * Returns an array indicating scatter positions.
     * @public
     * @return {number[]}
     */
    getScatterPositions() {
        return this._scatterPositions;
    };


    /**
     * Returns the ID of the scatter that was selected
     * ( ie: the value is in the range 1 to 5 inclusive. Its
     * NOT index based ).
     * @public
     * @return {number}
     */
    getScatterSelected() {
        return this._scatterSelected;
    };
}

export default FpgBonusModel;