import * as C_RewardGroup from "../const/C_RewardGroup";
import { getAppLogger } from "../logging/LogManager";
import { CmsRng } from "../maths/CmsRng";
import * as ArrayUtils from "../utils/ArrayUtils";

/**
 * Defines the standard order that different reward groups should be shown in, in the
 * Spin Phase Result presentation that is output by this module.
 */
const STANDARD_REWARD_GROUP_ORDER = [
    C_RewardGroup.MULTIPLIER,
    C_RewardGroup.CREDIT,
    C_RewardGroup.SUPERBET,
    C_RewardGroup.FREESPIN,
    C_RewardGroup.BONUS
];

/**
 * @type {OrderedSymbolWinPresentationConfig}
 */
const DEFAULT_IDLE_WIN_CONFIG =
{
    type : "showIndividually",
    orderBy : "winValue",
    countUpValue : true
};

const log = getAppLogger();

/**
 * Default, configurable implementation of a Spin Phase Presentation Builder. This cannot
 * cover all wacky future use cases in highly specific games, but it CAN come awfully
 * close. What is more, in principle this class could be extended, to override certain
 * specific bits of functionality, if required.
 * @implements {SpinPhasePresentationBuilder}
 */
export class DefaultSpinPhasePresentationBuilder
{
    /**
     * @param {Dependencies} dependencies 
     */
    constructor(dependencies)
    {
        /**
         * @protected
         */
        this._slotConfig = dependencies.config;

        /**
         * @protected
         * @type {Rng}
         */
        this._rng = new CmsRng();

        /**
         * @private
         */
        this._spinUtils = dependencies.spinModelUtils;

        /**
         * @private
         * @type {SpinPhasePresentationConfig}
         */
        this._presentationConfig = dependencies.config.spinWinPresentationConfig;
    };


    /**
     * @public
     * @inheritDoc
     * @param {SpinPhaseResult} spinPhaseResult
     * @param {SlotBetSettings} betSettings
     * @return {SpinPhaseResultPresentation}
     */
    buildPresentation(spinPhaseResult, betSettings)
    {
        let totalStake = betSettings.numHands * betSettings.numLines * betSettings.stakePerLine;

        /** @type {SpinRoundResultPresentation[]} */
        let roundPresentations = [];
        spinPhaseResult.rounds.forEach(round => {
            roundPresentations.push(this.buildSpinRoundResultPresentation(round, totalStake));
        });

        /**
         * @type {SpinPhaseResultPresentation}
         */
        let phasePresentation =
        {
            rounds : roundPresentations
        };

        return phasePresentation;
    };


    /**
     * Builds a presentation for a Spin Round
     * @private
     * @param {SpinRoundResult} spinRoundResult
     * @param {number} totalStake
     * @return {SpinRoundResultPresentation}
     */
    buildSpinRoundResultPresentation(spinRoundResult, totalStake)
    {
        /** @type {SpinResultPresentation[]} */
        let spinPresentations = [];
        spinRoundResult.spins.forEach(spin => {
            spinPresentations.push(this.buildSpinResultPresentation(spin, totalStake));
        });
        
        /**
         * @type {SpinRoundResultPresentation}
         */
        let spinRoundPresentation =
        {
            spins : spinPresentations
        };
        
        return spinRoundPresentation;
    }


    /**
     * Builds the presentation for a single spin result.
     * @private
     * @param {SpinResult} spinResult 
     * @param {number} totalStake
     * @return {SpinResultPresentation}
     */
    buildSpinResultPresentation(spinResult, totalStake)
    {
        let slotWins = this.buildWinPresentationSequence(spinResult, totalStake);
        let teaseReels = this.getTeaseReelsData(spinResult);

        let winMap = this._spinUtils.getMapOfSymbolsInvolvedInAnyWin(spinResult);

        /** @type {SymbolWinPresentation[]} */
        let idleWins = null;

        // When we have a grouped symbol win presentation, we need to generate "idle win" presentations
        // separately. Otherwise, idle win presentation can simply be extracted by re-using the existing
        // symbol win presentations.
        if (this._presentationConfig.symbolWin.type === "grouped") {
            idleWins = [];
            let idleWinConfig = this._presentationConfig.symbolWin.idleWinConfig || DEFAULT_IDLE_WIN_CONFIG;
            STANDARD_REWARD_GROUP_ORDER.forEach(rewardGroup => {
                let winsForRewardGroup = this.getIndividualSymbolWinPresentations(spinResult, rewardGroup, idleWinConfig);
                idleWins.push(...winsForRewardGroup);
            });
        }
        else
        if (this._presentationConfig.symbolWin.type === "showIndividually") {
            // If we are using individual symbol win presentations, we can basically
            // just grab the exact symbol win presentations already generated, and
            // show them in the order returned.
            idleWins = slotWins.filter(slotWin => {
                return slotWin.type === "symbolWin";
            });
        }

        // TODO: It isn't really necessary to do this search again, but for now, this is the simplest
        // way of checking if we need to show TotalWin if player "skips win sequences": simply check
        // if there is a TotalWin for CREDIT in the sequence of events to show!! Semanatically accurate,
        // but can be returned from "buildWinPresentation" as a piece of meta-data.
        let showTotalWinIfSkip = false;
        slotWins.forEach(slotWin => {
            if (slotWin.type === "totalWin" && slotWin.rewardGroup === C_RewardGroup.CREDIT) {
                showTotalWinIfSkip = true;
            }
        });

        /**
         * @type {SpinResultPresentation}
         */
        let spinResultPresentation =
        {
            slotWins,
            idleWins,
            teaseReels,
            winMap,
            spinResult,
            showTotalWinIfSkip
        };

        return spinResultPresentation;
    };


    // We could also dump the concept of a rewardGroup presentation, and just have a sequence
    // of "win events" that need to be shown.
    /**
     * @private
     * @param {SpinResult} spinResult
     * @param {number} totalStake
     * @return {SlotWinPresentation[]}
     */
    buildWinPresentationSequence(spinResult, totalStake)
    {
        /** @type {SlotWinPresentation[]} */
        let winEvents = [];

        // We will insert any Big Win presentation first
        let bigWinPresentation = this.getBigWinPresentation(spinResult, totalStake);
        if (bigWinPresentation) {
            log.info('BigWin: adding a BigWin presentation');
            winEvents.push(bigWinPresentation);
        }
        else log.info('BigWin: no BigWin presentation added');

        // Then, we will show an ordered sequence of win presentations, based on
        // standard RewardGroup ordering
        STANDARD_REWARD_GROUP_ORDER.forEach(rewardGroup => {
            let numWinEventsForRewardGroup = 0;

            let symbolWinPresentations = this.getSymbolWinPresentations(spinResult, rewardGroup);
            numWinEventsForRewardGroup += symbolWinPresentations.length;
            winEvents.push(...symbolWinPresentations);

            // all special wins second
            spinResult.specialWins.forEach(specialWin => {
                if (specialWin.rewardGroup === rewardGroup) {
                    winEvents.push({ type:"specialWin", win:specialWin });
                    numWinEventsForRewardGroup += 1;
                }
            });

            // Pick any Total Win for the reward group. It's possible that no TotalWin
            // sequence will be added, depending on how the win presentation is configured.
            let totalWinPresentation =
                this.getTotalWinPresentationForRewardGroup(spinResult, rewardGroup, numWinEventsForRewardGroup);
            
            if (totalWinPresentation) {
                winEvents.push(totalWinPresentation);
            }
        })

        return winEvents;
    };


    /**
     * Generates a Big Win Presentation to show for a Spin Result (or returns null, if no
     * Big Win presentation should be shown for the given spin result). Internally, this
     * uses rules specified in the Spin Phase Presentation Config to determine what can
     * constitute a Big Win.
     * @param {SpinResult} spinResult 
     * @param {number} totalStake
     * @return {SlotWinPresentation | null}
     * The presentation to show for Big Win, or null if no presentation is to be shown.
     */
    getBigWinPresentation(spinResult, totalStake)
    {
        let bigWinConfig = this._presentationConfig.bigWin;
        if (!bigWinConfig) {
            return null;
        }

        /** @type {BigWinPresentation} */
        let bigWinPresentation = null;

        if (bigWinConfig.type === "multiplier")
        {
            let bigWinThreshold = bigWinConfig.multiplier * totalStake;
            if (spinResult.totalCreditWon >= bigWinThreshold)
            {
                bigWinPresentation =
                {
                    type : "bigWin",
                    rewardGroup : C_RewardGroup.CREDIT,
                    rewardValue : spinResult.totalCreditWon,
                    highlightSymbols : this._spinUtils.getMapOfSymbolsInvolvedInCreditWin(spinResult),
                    highlightWinlineIds : this._spinUtils.getWinlinesIdsInvolvedInCreditWin(spinResult)
                };
            }
        }

        return bigWinPresentation;
    };


    /**
     * Returns a sequence of Symbol Win presentations for a Spin Result and a given reward group.
     * The ordering of symbol wins can be customized using this method.
     * @protected
     * @param {SpinResult} spinResult 
     * @param {number} rewardGroup 
     * @return {SymbolWinPresentation[]}
     */
    getSymbolWinPresentations(spinResult, rewardGroup)
    {
        /** @type {SymbolWinPresentation[]} */
        let presentations = null;
        let symbolWinConfig = this._presentationConfig.symbolWin;
        let symbolWinsForRewardGroup = spinResult.symbolWins.filter(win => win.rewardGroup === rewardGroup);

        // With grouping, we basically show all matching symbol wins in one lump.
        if (symbolWinConfig.type === "grouped")
        {
            presentations = [];

            if (symbolWinsForRewardGroup.length > 0)
            {
                let rewardValue = 0;

                symbolWinsForRewardGroup.forEach(symbolWin => {
                    rewardValue += symbolWin.rewardValue;
                });

                let winningPositionsMap = this._spinUtils.getMapOfSymbolsInvolvedInWinFor(spinResult, rewardGroup)[0];

                // TODO: This currently only deals with single hand grouped wins, we will
                // need to look at changing this for multi-hand games.
                presentations.push({
                    type : "symbolWin",
                    win : {
                        lineId : 0,
                        handId : 1,
                        rewardGroup,
                        rewardType : 0,
                        rewardValue,
                        winningSymbol : null,
                        winningPositions : null,    // TODO: I suspect there are issues with the definitions of this ?
                        winningReels : null,        // TODO: I suspect there are issues with the definitions of this ?
                        winningPositionsMap
                    }
                });
            }
        }
        // Otherwise, we will show wins individually, with a specific order.
        else
        if (symbolWinConfig.type === "showIndividually")
        {
            presentations = this.getIndividualSymbolWinPresentations(spinResult, rewardGroup, symbolWinConfig);
        }

        return presentations;
    }


    /**
     * Generates standard ordered symbol win presentations.
     * @private
     * @param {SpinResult} spinResult 
     * @param {number} rewardGroup 
     * @param {OrderedSymbolWinPresentationConfig} presentationConfig
     * Configuration which specifies ordering of symbol win presentations.
     * @return {SymbolWinPresentation[]}
     */
    getIndividualSymbolWinPresentations(spinResult, rewardGroup, presentationConfig)
    {
        let symbolWinsForRewardGroup = spinResult.symbolWins.filter(win => win.rewardGroup === rewardGroup);

        /** @type {SymbolWinPresentation[]} */
        let presentations = [];

        if (presentationConfig.orderBy === "winlineId")
        {
            let countUpWinlineIds = presentationConfig.countUpWinlineIds;
            let countUpValue = presentationConfig.countUpValue;

            symbolWinsForRewardGroup.sort((win1, win2) => {
                if (win1.lineId != win2.lineId) {
                    // Correct
                    return (countUpWinlineIds? 1 : -1) * (win1.lineId - win2.lineId);
                }
                // Fallback where winlineIds are the same
                // If counting up on winValue, then
                // - show lower rewardValues first
                // - show higher numbers of symbols first
                // - show higher values of symbol id first
                else
                {
                    if (win1.rewardValue !== win2.rewardValue) {
                        // Correct
                        return (countUpValue? 1 : -1) * (win1.rewardValue - win2.rewardValue);
                    }
                    else
                    if (win1.winningPositions.length != win2.winningPositions.length)
                    {
                        // Correct
                        return (countUpValue? -1 : 1) * (win1.winningPositions.length - win2.winningPositions.length);
                    }
                    else
                    {
                        return (countUpValue? 1 : -1) * (win1.winningSymbol.id - win2.winningSymbol.id);
                    }
                }
            });
        }
        else
        if (presentationConfig.orderBy === "symbolId")
        {
            let countUpSymbolIds = presentationConfig.countUpSymbolIds;
            let countUpValue = presentationConfig.countUpValue;

            symbolWinsForRewardGroup.sort((win1, win2) => {
                if (win1.winningSymbol.id != win2.winningSymbol.id) {
                    // Correct
                    return (countUpSymbolIds? 1 : -1) * (win1.winningSymbol.id - win2.winningSymbol.id);
                }
                // Fallback where symbolIds are the same
                // If counting up on winValue, then
                // - show lower reward value first
                // - show higher number of symbols first
                else
                {
                    if (win1.rewardValue !== win2.rewardValue) {
                        // Correct
                        return (countUpValue? 1 : -1) * (win1.rewardValue - win2.rewardValue);
                    }
                    else
                    {
                        // Correct
                        return (countUpValue? -1 : 1) * (win1.winningPositions.length - win2.winningPositions.length);
                    }
                }
            });
        }
        else
        if (presentationConfig.orderBy === "winValue")
        {
            let countUpValue = presentationConfig.countUpValue;
            symbolWinsForRewardGroup.sort((win1, win2) => {
                if (win1.rewardValue !== win2.rewardValue) {
                    // Correct
                    return (countUpValue? 1 : -1) * (win1.rewardValue - win2.rewardValue);
                }
                // Fallback where rewardvalues are the same
                // If counting up on winValue
                // - show higher numbers of symbols first
                // - show higher
                else
                {
                    if (win1.winningPositions.length != win2.winningPositions.length)
                    {
                        // Correct
                        return (countUpValue? -1 : 1) * (win1.winningPositions.length - win2.winningPositions.length);
                    }
                    else // fallback to symbol id comparison
                    {
                        // Correct
                        return (countUpValue? 1 : -1) * (win1.winningSymbol.id - win2.winningSymbol.id);
                    }
                }
            });
        }

        symbolWinsForRewardGroup.forEach(symbolWin => {
            presentations.push({ type:"symbolWin", win:symbolWin });
        });

        return presentations;
    };


    /**
     * Generates the appropriate Total Win sequence to use for a specific reward group. If no total
     * win sequence is to be added for the reward group, then a value of null will be returned:
     * otherwise, an appropriate SlotWinPresentation will be returned.
     * @param {SpinResult} spinResult 
     * @param {number} rewardGroup
     * @param {number} numWinEvents
     * The number of win events that have already been generated for this sequence. The reason this
     * is passed in as a parameter, instead of being determined by this method: even if the source
     * result from the server had multiple win events, we may have grouped some of them together
     * (eg: consider a multi-hand game, where we get multiple symbolWins across different hands, and
     * all on the same winline: we may have pre-grouped this as a single winEvent). So, this value
     * represents "output" winEvents, and *not* input winEvents.
     * @return {SlotWinPresentation | null}
     */
    getTotalWinPresentationForRewardGroup(spinResult, rewardGroup, numWinEvents)
    {
        // TODO: In future, this can support different config for different reward
        // groups. For now, it's absolutely fine for us to use a single, default
        // config.

        let totalWinConfig = this._presentationConfig.totalWin;

        if (totalWinConfig.type === "none") {
            return null;
        }
        else
        if (totalWinConfig.type === "default")
        {
            if (numWinEvents > 1 || (numWinEvents === 1 && totalWinConfig.useWhenSingleWin))
            {
                let totalWon = this._spinUtils.getRewardGroupWinningsFromSpinResult(spinResult, rewardGroup);

                // We don't have TotalWon when there is no winnings.
                // TODO: This actually needs to take RewardGroup rules into account at some point !!
                if (totalWon > 0)
                {
                    return { type:"totalWin", rewardGroup, totalWon };
                }
                else return null;
            }
            else return null;
        }
    };


    /**
     * @private
     * @param {SpinResult} spinResult 
     * @return {Object | null}
     */
    getPreSpinStopSequence(spinResult)
    {
        // We can filter out special wins, as required. Here we take out all
        // multipliers, but in other cases, we could grab only those multipliers
        // that also had a specific rewardType (probably we wouldn't do this)
        let preSpinStopSpecialWins = spinResult.specialWins.filter(specialWin => {
            return specialWin.rewardGroup === C_RewardGroup.MULTIPLIER;
        });
    }
    

    /**
     * Returns the order that Reward Groups should be handled in, after the reels have
     * stopped spinning.
     * @private
     * @return {number[]}
     */
    getRewardGroupOrder() {
        return STANDARD_REWARD_GROUP_ORDER;
    }


    // TODO: This method probably wants to accept more parameters, but this can be handled later.
    // The current spin result that a presentation is being built for, is pretty much the sensible
    // bare minimum that should be used.
    /**
     * Generates tease reels data for a Spin Result Presentation. Override this methods in customized
     * sub-classes, in order to implement custom functionality (there is probably not a single, sensible
     * way to design this by default, so its better to do it bespoke for any game that requires it).
     * @protected
     * @param {SpinResult} spinResult
     * The raw spin result that a presentation is being built for. This should provide sufficient information
     * to you, to determine how to tease the reels.
     * @return {boolean[]}
     * A set of tease reels - this MUST be the same length as the number of reels-per-hand in the game.
     * false indicates "don't tease the reel", true indicates "do tease this reel".
     */
    getTeaseReelsData(spinResult) {
        return ArrayUtils.createArray(this._slotConfig.numReels, false);
    }
};