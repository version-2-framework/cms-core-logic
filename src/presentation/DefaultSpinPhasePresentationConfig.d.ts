/**
 * Configuration settings for the default Spin Phase Presentation builder.
 */
interface SpinPhasePresentationConfig
{
    /**
     * Configures any Big Win sequence that may be inserted in to the Win Presentation.
     */
    bigWin : BigWinPresentationConfig;

    /**
     * Configures Symbol Win presentations.
     */
    symbolWin : SymbolWinPresentationConfig;

    /**
     * Configures the standard Total Win sequence to use for Credit (and other RewardGroups,
     * if no custom configuration is provided).
     */
    totalWin : TotalWinPresentationConfig;
}

//--------------------------------------------------------------------------------------------------
// Configuration for Big Win rules
//--------------------------------------------------------------------------------------------------
type BigWinPresentationConfig =
    NoBigWinPresentationConfig |
    ThresholdBigWinPresentationConfig;

/**
 * Indicates that no Big Win presentation at all should be used.
 */
interface NoBigWinPresentationConfig
{
    type : "none";
}

/**
 * Indicates that a Big Win animation will be inserted, if Total Credit Won breaches a certain
 * multiple of Total Stake. This is the most common mechanism that would be desired for a Big
 * Win.
 */
interface ThresholdBigWinPresentationConfig
{
    type : "multiplier";

    /**
     * The multiple of TotalStake that credit win must breach, in order to insert a Big Win
     */
    multiplier : number;
}

//--------------------------------------------------------------------------------------------------
// Configuration for Symbol Win presentation
//--------------------------------------------------------------------------------------------------
/**
 * Configures ordering and priority for the Symbol Win presentations. Symbol Wins are primarily
 * grouped according to Reward Group (this is a non-changeable mechanic presently, as there is
 * a good reason to always want to do it in a certain way). However, it makes sense to want to
 * tweak the order that symbol wins WITHIN specific reward groups are shown, eg: to count up
 * based on winline id, or to start with lowest value win and count up to highest, etc.
 */
type SymbolWinPresentationConfig =
    GroupedSymbolWinPresentationConfig |
    OrderedSymbolWinPresentationConfig;

/**
 * Configuration for a grouped Symbol Win presentation. All credit wins will be shown in a
 * single group. In Grouped mode, we show a single presentation of all symbol wins (for each
 * reward group) during the "quick win" presentation. Individual symbol wins are still shown
 * during the idle win presentation.
 */
interface GroupedSymbolWinPresentationConfig
{
    /**
     * Indicates that Symbol Wins will be shown grouped.
     */
    type : "grouped";

    /**
     * Optional configuration for how symbols are shown during the "idle win" portion.
     */
    idleWinConfig ? : OrderedSymbolWinPresentationConfig;
}

type OrderedSymbolWinPresentationConfig =
    SymbolIdOrderedSymbolWinPresentationConfig |
    WinlineOrderedSymbolWinPresentationConfig |
    ValueOrderedSymbolWinPresentationConfig;



/**
 * Indicates that Symbol Wins should be ordered first according to their Symbol Id. We can
 * specify whether we want to count up (start with lower value symol ids), or the opposute.
 * When 2 wins have the same winning symbol id, this mode implements a secondary fallback:
 * - first uses number of symbols in the win (eg: a win with 3 symbols will be shown before
 *   a win with 4 symbols)
 * - If numWinningSymbols is also equal, then falls back to symbol id (first show lower value
 *   symbol ids, then higher value symbol ids: this is always "count up")
 */
interface SymbolIdOrderedSymbolWinPresentationConfig
{
    /**
     * Indicates that Symbol Win presentations during quick-win presentation, will be shown
     * as individual wins, with a configurable ordering.
     */
    type : "showIndividually";

    /**
     * Indicates that individual Symbol Wins will be ordered primarily according to the id of
     * the winning symbol.
     */
    orderBy : "symbolId";

    /**
     * Indicates if we should "count up" (eg: start at lower value symbol ids, and move to
     * higher value symbol ids), or do the reverse (start with highest value symbol ids, and
     * count down).
     */
    countUpSymbolIds : boolean;

    /**
     * If 2 symbol wins share the same winning symbolId, then this specifies secondary ordering.
     * If true
     * - wins with lower rewardValues will be shown first
     * - if rewardValue is the same, wins with higher numbers of symbols will be prioritized
     *   (if 2 wins have the same value, we assume that the one with more symbols is the "lower
     *   priority" win, as its symbol must be worth less).
     * If set to false, the opposite secondary ordering is applied.
     */
    countUpValue : boolean;
}

/**
 * Indicates that Symbol Wins should be ordered first according to their Winline Id (where
 * appropriate). If any Symbol Win does not belong to a winline, then a secondary fall back
 * mechanic can be specicified (TODO: What mechanic!). Ordinarily, we do not expect to have
 * more than 1 win on a single winline (although this can happen on multi-hand games, in which
 * case we use a special form of Symbol Win Presentation)
 */
interface WinlineOrderedSymbolWinPresentationConfig
{
    /**
     * Indicates that Symbol Win presentations during quick-win presentation, will be shown
     * as individual wins, with a configurable ordering.
     */
    type : "showIndividually";

    /**
     * Indicates that individual Symbol Wins will be ordered primarily according to the id
     * of any winning winline that the win is on.
     */
    orderBy : "winlineId";

    /**
     * Indicates if winline wins should be counted up (eg: starting from winline 1 upwards)
     * or down (eg: starting at the highest value of winline id found, and counting down
     * towards 1).
     */
    countUpWinlineIds : boolean;

    /**
     * If winlineId is not available for ordering the win, or 2 wins share the same winlineId,
     * this specifies secondary ordering.
     * If true
     * - withs with lower rewardValues will be shown first
     * - if rewardValue is the same, wins with higher numbers of symbols will be prioritized
     *   (if 2 wins have the same value, we assume that the one with more symbols is the "lower
     *   priority" win, as its symbol must be worth less).
     * - if numWinningSymbols is the same, we can prioritize lower value symbolIds.
     * If set to false, the opposite secondary ordering is applied.
     */
    countUpValue : boolean;
}

/**
 * Indicates that Symbol Wins should be ordered first according to their value (within the
 * reward group). We can specify whether we want to count up (start with lowest value wins,
 * and count up to highest), or count down (start with highest value wins, and count down
 * to lowest). When 2 wins have the same reward values, this mode implements a secondary
 * fallback:
 * - first uses number of symbols in the win (eg: a win with 4 symbols will be shown before
 *   a win with 3 symbols)
 * - If numWinningSymbols is also equal, then falls back to symbol id (first show lower value
 *   symbol ids, then higher value symbol ids: this is always "count up")
 */
interface ValueOrderedSymbolWinPresentationConfig
{
    /**
     * Indicates that Symbol Win presentations during quick-win presentation, will be shown
     * as individual wins, with a configurable ordering.
     */
    type : "showIndividually";

    /**
     * Indicates that the individual Symbol Wins will be ordered primarily according to the
     * value of the win.
     */
    orderBy : "winValue";

    /**
     * Indicates if we should "count upwards", eg: start with the lowest value wins, and count
     * up to the highest value wins.
     */
    countUpValue : boolean;
}

//--------------------------------------------------------------------------------------------------
// Configuration for Total Win Presentation
//--------------------------------------------------------------------------------------------------
type TotalWinPresentationConfig =
    NoTotalWinPresentationConfig |
    DefaultTotalWinPresentationConfig;

/**
 * Indicates that no Total Win sequence should be shown for a specific reward group.
 */
interface NoTotalWinPresentationConfig
{
    type : "none";
}

/**
 * Default configuration for the total winnings sequence.
 */
interface DefaultTotalWinPresentationConfig
{
    type : "default";

    /**
     * Indicates the the total winnings sequence should be shown when only a single win (symbol
     * or special) appears for the reward group. When not set to true, the Total Winnings animation
     * will be omitted for cases where only a single win is present in the results (as any winnings
     * amount shown with that win already is the total winnings). Setting this flag to false would
     * be the default expected behaviour.
     */
    useWhenSingleWin : boolean;
}

