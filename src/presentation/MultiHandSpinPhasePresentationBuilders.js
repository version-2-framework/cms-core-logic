import { DefaultSpinPhasePresentationBuilder } from "./DefaultSpinPhasePresentationBuilder";
import * as ArrayUtils from "../../../cms-core-logic/src/utils/ArrayUtils";
import * as C_RewardGroup from "../const/C_RewardGroup";
import { getAppLogger } from "../logging/LogManager";

const log = getAppLogger();

/**
 * This implementation is for a game that uses Winline Wins (eg: Four Fowl Play, Four Haunted):
 * it will group winline wins together, and bonus wins together (anything else, we do not
 * have a sensible rule for, but we also do not expect this for those games). This implementation
 * also will break down if you have more than 1 on any hand, with a given winlineId (although it
 * does its best to make sure that all wins are shown). If your data input is standard for the
 * CMS Four Hand Maths engines, however, then it will do just fine, and give you the output that
 * you want.
 */
export class WinlineGroupingMultiHandSpinPhasePresentationBuilder
    extends DefaultSpinPhasePresentationBuilder
{
    constructor(dependencies)
    {
        super(dependencies);

        /**
         * @private
         * @type {number}
         */
        this._numHands = this._slotConfig.numHands;

        /**
         * @private
         * @type {number[]}
         */
        this._winlineIds = Object.keys(this._slotConfig.winlines).map(s => parseInt(s));
    }

    /**
     * This overridden implementation generates MultiHand symbol win presentations, using standard logic.
     * @protected
     * @override
     * @inheritDoc
     * @param {SpinResult} spinResult 
     * @param {number} rewardGroup 
     * @return {MultiHandSymbolWinPresentation[]}
     */
    getSymbolWinPresentations(spinResult, rewardGroup)
    {
        // TODO: There is no reason that this should not be able to support config options
        // used on the Default Spin Phase Presentation Builder for Symbol Wins
        // - we could group wins, or have single wins
        // - we should be able to customize ordering (ascending / descending winline ids,
        //   winning symbol id, win value) - this should all be a possibility

        /** @type {MultiHandSymbolWinPresentation[]} */
        let symbolWinPresentations = [];

        /** @type {{[lineId:number]:MultiHandSymbolWinPresentation}} */
        let winsForLines = {};

        /** @type {MultiHandSymbolWinPresentation} */
        let currLineZeroPresentation = null;

        let symbolWinsForRewardGroup = spinResult.symbolWins.filter(
            symbolWin => symbolWin.rewardGroup === rewardGroup);

        
        symbolWinsForRewardGroup.forEach(symbolWin => {
            let lineId = symbolWin.lineId;
            let handIndex = symbolWin.handId - 1;

            if (lineId > 0) {
                /** @type {MultiHandSymbolWinPresentation} */
                let winForLine;
                if (lineId in winsForLines) {
                    winForLine = winsForLines[lineId];
                }
                else
                {
                    winForLine = this.getBlankMultiHandSymbolWinPresentation();
                    winForLine.multiHandMetaData.lineId = lineId;
                    winsForLines[lineId] = winForLine;
                    symbolWinPresentations.push(winForLine);
                }

                // If the symbol win presentation (eg: one that was cached) ALREADY has
                // a win for that hand, we will append this new symbolWin onto a new
                // multi-hand-win-presentation. This at least ensures that all symbolWins
                // will be shown. Unfortunately, there isn't a sensible one-size-fits-all
                // rule to deal with this. However, when your game maths works like Four
                // Haunted or Four Fowl Play, this case will not happen.

                if (winForLine.winPerHand[handIndex] !== null) {
                    log.debug(`WinlineGroupingMultiHandPresentationBuilder: hand[${handIndex}] slot already taken for line ${lineId}`);
                    
                    let newWin = this.getBlankMultiHandSymbolWinPresentation();
                    newWin.winPerHand[handIndex] = symbolWin;
                    //newWin.multiHandMetaData.lineId = lineId;

                    if (handIndex === 0) {
                        newWin.win = symbolWin;
                    }

                    symbolWinPresentations.push(newWin);
                }
                else
                {
                    winForLine.winPerHand[handIndex] = symbolWin;

                    if (handIndex === 0) {
                        winForLine.win = symbolWin;
                    }
                }
            }
            // If lineId is 0, we try and push onto the most recent, cached "line zero" presentation.
            // If the hand of the new win is free on that presentation, we can continue stashing.
            // If not, we will start a new lineZero presentation, and use that instead. This is
            // convoluted: for cases of games like Four Fowl Play, we expect only 1 bonus win per
            // hand, so this will basically work in a very generic way... but not for all future
            // games. We cannot be that smart!
            else
            {
                if (currLineZeroPresentation === null ||
                    currLineZeroPresentation.winPerHand[handIndex] !== null)
                {
                    currLineZeroPresentation = this.getBlankMultiHandSymbolWinPresentation();
                    currLineZeroPresentation.multiHandMetaData.lineId = 0;

                    if (handIndex === 0) {
                        currLineZeroPresentation.win = symbolWin;
                    }

                    symbolWinPresentations.push(currLineZeroPresentation);
                }

                currLineZeroPresentation.winPerHand[handIndex] = symbolWin;
            }
        });

        // Attempt to sort, so that we get asceding winline order
        // TODO: There is no reason that this cannot support the alternate sorting methods
        // implemented on other games
        symbolWinPresentations.sort((win1, win2) => {
            return win1.multiHandMetaData.lineId - win2.multiHandMetaData.lineId;
        });

        symbolWinPresentations.forEach(winPresentation => this.finalizePresentation(winPresentation));

        

        return symbolWinPresentations;
    };


    /**
     * Takes a Multi Hand Symbol Win Presentation - assumed to have the final list of per-hand
     * wins attached - and sums up the total winnings correctly.
     * @private
     * @param {MultiHandSymbolWinPresentation} presentation 
     */
    finalizePresentation(presentation)
    {
        presentation.winPerHand.forEach(symbolWin => {
            if (symbolWin) {
                if (symbolWin.rewardGroup === C_RewardGroup.CREDIT) {
                    presentation.totalCreditWon += symbolWin.rewardValue;
                }
                else
                if (symbolWin.rewardGroup === C_RewardGroup.SUPERBET) {
                    presentation.totalSuperBetWon += symbolWin.rewardValue;
                }
                else
                if (symbolWin.rewardGroup === C_RewardGroup.PROMO_GAME) {
                    presentation.numFreeGamesWon += symbolWin.rewardValue;
                }
            }
        });
    };


    /**
     * @private
     * @return {MultiHandSymbolWinPresentation}
     */
    getBlankMultiHandSymbolWinPresentation()
    {
        return {
            type : "symbolWin",
            totalCreditWon : 0,
            totalSuperBetWon : 0,
            numFreeGamesWon : 0,
            win : null,
            winPerHand : ArrayUtils.createArray(this._numHands, null),
            multiHandMetaData : { lineId:0 }
        };
    };
}