// TODO: This could also be an intersection type
/**
 * Defines the Bet Settings available for a Game Client.
 */
interface BetSettingsConfig
{
    /**
     * Defines the way that the player may select the Num Hands bet setting.
     */
    numHands : SingleBetSetting;

    /**
     * Stake per hand bet settings. If this object is defined, then the Game Client will
     * assume that the player can directly modify the value of Stake per Hand: a single
     * Stake per Hand setting is a fixed pair of values (Stake per Line and Num Lines).
     * When the Stake per Hand Bet Setting is enabled, the player may not change Num Lines
     * or Stake per Line independendly, but instead must select one of these fixed values.
     * 
     * The value of this field is an ordered series of StakePerHand values.
     * 
     * When this setting is configured and present, and setting added for the numLines and
     * betPerLine fields will therefore be ignored.
     */
    stakePerHand ? : StakePerHand[];

    /**
     * Num Lines bet setting. This field defines the possible settings for the Num Lines
     * value. The allowed values for Num Lines can either be a range of possible values, or
     * an enumerated set. This field must be set, but its possible to limit numLines to a
     * single valid value by configuring the field appropriately (eg: configured an enumerated
     * set of values, with only 1 value). In this case, the player will not be permitted to
     * change the value of Num Lines (but will be informed of the single, active value).
     */
    numLines ? : SingleBetSetting;

    /**
     * Stake per Line bet setting. This field defines the possible settings for the Stake per
     * Line value. The allowed values for Stake per Line can either be a range of possible values,
     * or an enumerated set. This field must be set, but its possible to limit numLines to a
     * single valid value by configuring the field appropriately (eg: configured an enumerated
     * set of values, with only 1 value). In this case, the player will not be permitted to
     * change the value of Stake per Line (but will be informed of the single, active value).
     */
    betPerLine ? : SingleBetSetting;

    /**
     * Special Wager Multiplier bet settings. Not all games support this (if they don't, assume
     * a fixed value of 1 for the setting).
     */
    specialWagerMultiplier ? : SpecialWagerBetSetting;

    /**
     * Removes the title text above the bet selector. For some reason the translation key for 
     * the bet selector title cannot be overwritten. 
     */
    removeBetSelectorTitleText ? : boolean;
}

/**
 * A fixed value for Stake per Hand.
 */
interface StakePerHand
{
    /**
     * Number of active winlines.
     */
    numLines : number;

    /**
     * Stake (in "points" per active winline)
     */
    betPerLine : number;
}

type SingleBetSetting = RangedBetSetting | EnumeratedBetSetting;

type SpecialWagerBetSetting = RangedBetSetting | SpecialWagerEnumeratedBetSetting;

/**
 * A Bet Setting, where a range of values is allowed. The range of valid values includes the
 * values specified for min and max, as well as every whole integer in-between.
 */
interface RangedBetSetting
{
    /**
     * Indicaes that this is NOT an enumerated setting (eg: is a ranged setting).
     */
    isEnumerated : false,

    /**
     * The minimum value that the Bet Setting may be.
     */
    minValue : number;

    /**
     * The maximum value that the Bet Setting may be.
     */
    maxValue : number;
}

/**
 * A Bet Setting, where an enumerated set of values is allowed.
 */
interface EnumeratedBetSetting
{
    /**
     * Indicates that this is an enumerated setting.
     */
    isEnumerated : true

    /**
     * The set of values that this Bet Setting may be.
     */
    values : number[]
}

/**
 * Special Wager Multiplier is - strictly speaking - a bet setting like any other, and can
 * be ranged or enumerated. In certain cases though, when it has exactly 2 possible values,
 * we might want to indicate it as a binary setting ("off" means that SpecialWagerMultiplier
 * is set to 1x, "on" means it is set to a single fixed value larger than 1). When indicating
 * that it is binary, really this is metadata for GUI which access bet settings, about how
 * we should think of the bet setting.
 */
interface SpecialWagerEnumeratedBetSetting extends EnumeratedBetSetting
{
    isBinarySetting : true;

    values : [number, number];
}