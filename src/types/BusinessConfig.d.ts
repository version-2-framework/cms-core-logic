//--------------------------------------------------------------------------------------------------
// 
//--------------------------------------------------------------------------------------------------
/**
 * Indicates a Session Type
 */
type GameplayMode = "free" | "real";

//--------------------------------------------------------------------------------------------------
// Root level feature configuration
//--------------------------------------------------------------------------------------------------
/**
 * The most basic form of configuration that can exist for a feature.
 */
interface SimpleFeatureConfig
{
    /**
     * Indicates if this feature is enabled or not.
     */
    isEnabled : boolean;
}

/**
 * The most basic form of configuration that can exist for a button.
 */
interface SimpleButtonConfig extends SimpleFeatureConfig
{

}

interface TextBoxConfig extends SimpleFeatureConfig 
{

}

//--------------------------------------------------------------------------------------------------
// External link functionality
//--------------------------------------------------------------------------------------------------
/**
 * Opens a link in a popup (on desktop) or external tab (on mobile). The link opened is a hard-coded
 * value.
 */
interface OpenUrlLinkAction
{
    type : "url";

    /**
     * The url that needs to be opened.
     */
    url : string;
}

/**
 * Opens a link, whose url is specified by a url parameter.
 */
interface OpenUrlParamLinkAction
{
    type : "urlParam";

    /**
     * The url parameter containing a link to open.
     */
    urlParam : string;
}

type OpenLinkAction = OpenUrlLinkAction | OpenUrlParamLinkAction;

//--------------------------------------------------------------------------------------------------
// Exit client functionality
//--------------------------------------------------------------------------------------------------
/**
 * Config for an Exit Client Action, which redirects the browser to a hard-coded url.
 */
interface RedirectExitClientAction
{
    type : "redirect",

    /**
     * The url that the redirect should be to.
     */
    url : string;
}

/**
 * Config for an Exit Client Action which redirects the browser to a specific url: the url is
 * supplied by a url parameter. When the url parameter is absent, we should not enable any button
 * that requires this action.
 */
interface RedirectToUrlParamExitClientAction
{
    type : "redirectToUrlInParam";

    /**
     * The name of the Url Parameter, that contains the URL to which we must redirect.
     */
    urlParam : string;
}

/**
 * Config for an Exit Client Action, which sends a command to a parent iframe, telling that iframe
 * to shut the window that we are in. This command must be supported by the licensee: the browser
 * provides a generic mechanism for sending a request message to a parent iframe, but the actual
 * mechanism for closing the window we are in (which could be some kind of custom popup container,
 * but contained within an html page) must be a custom implementation provided by the website
 * maintainers.
 * 
 * This uses "Window.postMessage", as provided by the browser environment. This form of Exit Client
 * Action is generally only appropriate for desktop mode (because on mobile, we are unlikely to have
 * our game client open with such a modal popup).
 * 
 * We can pass a custom message string,w hich is specified in the field "postMessageCommand". This
 * message string is effectively the custom event that the licensee website must be listening for.
 * We can also customize the "target" of the message, using the field "postMessageTargets": a simple
 * example would have us sending our custom message to "window.parent", but on some licensee websites,
 * we may need to send the message up through more than 1 iframe layer, and possibly even to a specific
 * child object of the parent iframe. The target is basically the object upon which we will actually
 * call "postMessage".
 */
interface PostMessageExitClientAction
{
    type : "postMessage";

    /**
     * The command string to pass to postMessage. This is basically the custom event id that the
     * licensee website must be listening out for.
     */
    postMessageCommand : string;

    /**
     * A series of targets to post the message to. We expect this to be dot delineated, eg:
     * "parent.parent" would invoke "window.parent.parent.postMessage(postMessageCommand)".
     * You should not add "window" to the beginning of this string.
     * 
     * It is also possible to specify a child object of the parent iframe (although this would be
     * an unusual way to configure it): for example, by specifying "parent.childObject" (this will
     * cause the call to be "window.parent.childObject.postMessage(postMessageCommand)".
     */
    postMessageTargets : string;
}

/**
 * Config for an Exit Client Action, which attempts to close the current browser tab, by invoking
 * "window.close". This will only work if the current tab has been opened programatically, according
 * to mozilla docs: https://developer.mozilla.org/en-US/docs/Web/API/Window/close.
 * 
 * Therefore, this is not appropriate for all licensee websites, but if the licensee confirms to us
 * that the game client is running in a browser frame that was opened this way, then this configuration
 * should work in most modern browsers.
 */
interface CloseCurrentTabExitClientAction
{
    type : "closeCurrentTab";
}

/**
 * Configuration for an Exit Client button. This config defines HOW any exit client button
 * could work: WHERE Exit Client buttons are included in the game client is configured separately.
 * Please note, it is common to include separate configurations for both mobile and desktop (or to
 * only provide an Exit Client button in one of these cases)
 * 
 * Serveral mechanisms for Exiting a game client can currently be offered:
 * - a url can be specified. In this case, invoking the Exit Client button will attempt to redirect
 *   directly to this url.
 * - a url parameter can be specified: 
 * - an action can be invoked directly.
 * 
 * In theory, the game client can offer the Exit Client feature from a number of places (the
 * most common is to include it in the session stats view, but we have also had requests to make
 * it available in the error dialog popup).
 */
type ExitClientAction =
    RedirectExitClientAction |
    RedirectToUrlParamExitClientAction |
    PostMessageExitClientAction |
    CloseCurrentTabExitClientAction;

/**
 * Configuration for an Exit Client button. This is normally used in the Session Stats view,
 * but in theory we can add this standard button config in other places.
 */
interface ExitClientButtonConfig extends SimpleButtonConfig
{
    /**
     * A custom exit client action, that will be used on this exit client button. When not specified,
     * the default action for the view type (desktop or mobile) will be invoked.
     */
    customAction ? : ExitClientAction;
}

//--------------------------------------------------------------------------------------------------
// Abstract feature configs
//--------------------------------------------------------------------------------------------------
/**
 * Configuration for Resume Session functionality.
 */
interface ResumeSessionConfig
{
    /**
     * If set to true, a notification that a session is being resumed will be shown to the
     * player after the loading screen. If set to false, then no notification will be used,
     * and the game will skip straight to starting the resumed game.
     */
    showNotification : boolean;
}

/**
 * Configuration for how a Session is started in the Game Client. This applies to V2 only.
 */
interface StartSessionConfig
{
    /**
     * If true, the Session should be opened using the Cashier. If false, no Cashier will be
     * used for starting the session: instead, the Game Client will attempt to open the session
     * behgin the scenes in a single step, and to bring the maximum possible credit into the
     * session (eg: the maximum amount the player has available, as long as it does not exceed
     * any "max credit in session" rules that may apply to the legal jurisdiction in which the
     * Game Client is running).
     */
    useCashier : boolean;
}

/**
 * Configuration for how a Game Session is started on the V1 platform.
 * Starting a session without cashier on V1, is : confusing !! In theory this can be done
 * in a simple way, but for certain platforms (the V1 integration for SKS) we have ended
 * up with some confusing, non-standard, undocumented (at least as far as CMS Gaming UK
 * are concerned) functionality.
 */
interface V1StartSessionWithoutCashierConfig extends StartSessionConfig
{
    useCashier : true;

    /**
     * Indicates if balance should be checked first. If true, this operation is very simple:
     * it genuinely is the same as using the cashier. If false, we have a special behaviour,
     * where we must set a specific requested amount in Realplay and Freeplay modes. The only
     * current example of this is SKS integration, and because this functionality is undocumented
     * (for the purposes of CMS Gaming, it may be documented intenrally by WMG Italy), we add
     * these extra config settings
     */
    checkBalance : boolean;

    balanceValueToRequestRealplay : number;
    balanceValueToRequestFreeplay : number;
}

/**
 * Configuration for the Add Credit feature. It is possible to specify both whether Add Credit
 * is allowed for the Game Client, and if it is, whether we use the Cashier or not.
 */
interface AddCreditConfig extends SimpleFeatureConfig
{
    /**
     * If true, Add Credit will be performed using the Cashier. If false, then the Add Credit
     * operation will be performed without the Cashier: the Game Client will attempt to bring in
     * the maximum amount of credit possible in the operation (eg: the maximum amount that the
     * player has available, as long as it would not exceed Session Transfer limits for the legal
     * jurisdiction in which the Game Client is being played).
     */
    useCashier : boolean;

    // TODO: THere are likely other places that "AddCredit" feature can be configured
}

//--------------------------------------------------------------------------------------------------
// View based configurations
//--------------------------------------------------------------------------------------------------
/**
 * Configuration for the For Fun advert. This is an advert that is shown between games, on a ForFun
 * ("Freeplay only") Game Client deployment.
 */
interface ForFunAdvertConfig extends SimpleFeatureConfig
{
    /**
     * Indicates how many games should be pass between each advert, before showing the For Fun
     * Advert. If not specified, then a default value will be used.
     */
    numGamesBetweenAdvert ? : number;

    /**
     * An external url, that the ForFun advert can link to. This will generally be used to prompy
     * the player to click the link, and to play for real.
     */
    link ? : OpenLinkAction;
}

/**
 * Configuration for the pair of Social Media buttons that can be shown on the Desktop GUI.
 */
interface SocialMediaButtonsConfig extends SimpleFeatureConfig
{
    /**
     * Url to use for a "share on facebook" button. This is the url that will be shown to the
     * player's friends, if they click the facebook button. If the url is absent, the facebook
     * button should not be shown.
     */
    url_facebook ? : string;

    /**
     * Url to use for a "share on twitter" button. This is the url that will be shown to the
     * player's friends, if they click the twitter button. If the url is absent, the twitter
     * button should not be shown.
     */
    url_twitter ? : string;
}

/**
 * Configuration for the Close Session button in the main Gui.
 */
type CloseSessionButtonConfig =
    DefaultCloseSessionButtonConfig |
    ExitClientCloseSessionButtonConfig |
    CloseSessionThenExitClientCloseSessionButtonConfig;

/**
 * Configures a Close Session Button for the GUI, which will send the Close Session request to the
 * Game Engine Server, and then show the Session Stats view.
 */
interface DefaultCloseSessionButtonConfig extends SimpleButtonConfig
{
    /**
     * The type of action that should be invoked when the button is pressed.
     */
    action : "closeSession";
}

/**
 * Config for Close Session Button, that indicates that the client should be exited / closed.
 */
interface ExitClientCloseSessionButtonConfig extends SimpleButtonConfig
{
    /**
     * The type of action that should be invoked when the button is pressed.
     */
    action : "exitClient";

    /**
     * An optional, customized configuration for exiting the game client when the Close Session
     * button's action is configured as exitClient. When not specified, the default exit client
     * action for the view type (desktop or mobile) will be invoked.
     */
    customAction ? : ExitClientAction;
}

/**
 * Config for Close Session Button, that indicates that the session should be closed (using the close
 * session service), and THEN the client should be closed / exited.
 */
interface CloseSessionThenExitClientCloseSessionButtonConfig extends SimpleButtonConfig
{
    action : "closeSessionThenExitClient";

    /**
     * An optional, customized configuration for exiting the game client when the Close Session
     * button's action is configured as closeSessionThenExitClient. When not specified, the default
     * exit client action for the view type (desktop or mobile) will be invoked.
     */
    customAction ? : ExitClientAction;
}

/**
 * Config for a single Responsible Gaming Link.
 */
interface ResponsibleGamingLinkConfig
{
    /**
     * The id associated with the responsible gaming link. This is used only for debug logging: the
     * actual value of the field has no real effect on how the link works (but sometimes its useful
     * for us to have a readable name associated with the link, when we need to debug to the console
     * or with our other debug tools).
     */
    id : string;

    /**
     * Id of the texture that should be shown for the button.
     */
    buttonTexture : string;

    /**
     * An optional Open Link action that should be invoked when the responsible gaming button is
     * pressed. If no action is specified, then no action should be invoked, and the responsible
     * gaming "link" is simply a graphic present in the set of responsible gaming icons.
     */
    action ? : OpenLinkAction;
}

/**
 * Config for the Url Selection View.
 */
interface UrlSelectionViewConfig
{
    /**
     * Indicates if the Url selection view is enabled. This view is normally shown for debug
     * and testing builds (and never in production), but this option exists to allow us to
     * explicitly override its inclusion.
     */
    isEnabled : boolean;

    /**
     * A list of custom server endpoints to enable in the url selection view. If this field is absent,
     * then the Game Client will fall back to using a set of default server endpoints for either V1 or
     * V2 platform (which are hard-coded). If supplied
     */
    customTestUrls ? : TestServerConfig[];
}

/**
 * Configuration in order to enable the quick load feature. If enabled the player will be taken straight to 
 * the game in idle state without having to click a button.
 */
interface QuickLoadConfig
{
   isEnabled: boolean;
}

/**
 * Config for the Splash Screen View.
 */
interface SplashScreenViewConfig
{
    /**
     * Indicates if user interaction is configured for the Splash Screen. If set to false, the user
     * won't have to press anything here - eg: won't have to explicitly select session type (obviously
     * this cannot be supported for cases where there are more than 1 session type, although if set to
     * true, the Splash Screen should issue a warning, or pick a default session type). By default,
     * this would be true (we expose this option only as an override)
     */
    allowUserInteraction? : boolean;
}

/**
 * Config for the Cashier View. The Cashier can be used either when starting a session, or adding
 * credit to an existing session. There are separate feature configurations for both starting a
 * session and adding credit, which may specify whether the Cashier is used for them. The Cashier
 * config simply configures functionality available within the cas
 */
interface CashierViewConfig
{
    /**
     * Optional config, specifying if an Exit Client button should be included in the Cashier
     * (in the top right corner). This was originally requested by BetFlag asked for it, and is
     * highly unlikely to ever be used in any other circumstance.
     * 
     * If this field is not specified, then the Exit Client button should not be included.
     */
    exitClientButton ? :
    {
        /**
         * Indicates if the Exit Client button should be enabled.
         */
        isEnabled : boolean;

        /**
         * A custom exit client action for desktop (if not specified, default exit client action
         * will be invoked)
         */
        customActionDesktop ? : ExitClientAction;

        /**
         * A custom exit client action for desktop (if not specified, default exit client action
         * will be invoked)
         */
        customActionMobile ? : ExitClientAction;
    }
}

/**
 * Features that are enabled for the Default Session Stats view.
 */
interface SessionStatsViewConfig
{
    // TODO: Future expansion for these 2 buttons, could allow us to use a custom config
    // just for this feature. I will not develop this for now, because it hasn't been
    // requested, but the use of a dedicated config object for each button would allow
    // us to append this additional data, and to implement such a feature at a later stage,
    // without having to perform major modifications to the game clients.

    /**
     * Configuration for an "Exit Game Client" button that can appear in the footer
     * on the desktop version of the game client. The button is optional (this config
     * object allows us to explicitly enable / disable it).
     */
    exitClientButtonDesktop ? : ExitClientButtonConfig;

    /**
     * Configuration for an "Exit Game Client" button that can appear in the footer
     * on the mobile version of the game client. The button is optional (this config
     * object allows us to explicitly enable / disable it).
     */
    exitClientButtonMobile ? : ExitClientButtonConfig;

    /**
     * Configuration for a "Restart Session" button that can appear in the footer
     * on the desktop version of the game client. The button is optional (this config
     * object allows us to explicitly enable / disable it).
     * 
     * This button will only be available when a session is closed, if the player
     * has not breached any session credit related rules (eg: if the player already
     * transferred max credit to the just closed session, then the button will not
     * be available, even if it is enabled for the client)
     */
    restartSessionButtonDesktop ? : SimpleButtonConfig;

    /**
     * Configuration for a "Restart Session" button that can appear in the footer
     * on the mobile version of the game client. The button is optional (this config
     * object allows us to explicitly enable / disable it).
     * 
     * This button will only be available when a session is closed, if the player
     * has not breached any session credit related rules (eg: if the player already
     * transferred max credit to the just closed session, then the button will not
     * be available, even if it is enabled for the client)
     */
    restartSessionButtonMobile ? : SimpleButtonConfig;
}

/**
 * Specifies a component to be drawn in one of the "info slots" on desktop GUI. There are two
 * info slots currently, one to the left / and one to the right of the game logo.
 */
type DesktopGuiInfoSlotConfig =
    EmptyDesktopGuiInfoSlotConfig | 
    ResponsibleGamingLinksInfoSlotConfig |
    SessionInfoSlotConfig;

/**
 * Specifies an empty info slot - nothing shown.
 */
interface EmptyDesktopGuiInfoSlotConfig
{
    type : "none";
}

/**
 * Configures a Reponsible Gaming Links component in the info slot.
 */
interface ResponsibleGamingLinksInfoSlotConfig
{
    type : "responsibleGamingLinks";
}

/**
 * Configures the Session Info "Info Slot" component.
 */
interface SessionInfoSlotConfig
{
    type : "sessionInfo";

    /**
     * An optional Open Link action. If this setting is enabled, then when the Session Info
     * view is pressed, the external link will be opened. If this option is set, then it serves
     * as the default mechanism that will be used in all possible cases (mobile, desktop, testing,
     * production). It can be overriden for these cases by specifying more explicit options.
     */
    openLink ? : OpenLinkAction;
}

/**
 * Features available for the Desktop Gui.
 */
interface DesktopGuiConfig
{
    /**
     * Configuration for the Top Left info slot on the GUI (shown in the top row of the desktop GUI,
     * and sandwiched between the Open Menu button on the left, and the Game Logo in the middle).
     * This config can specify that nothing is to be shown here, or it can specify the component to
     * be used (and any other additional data), for example:
     * - responsible gaming links
     */
    topLeftInfoSlot : DesktopGuiInfoSlotConfig;

    /**
     * Configuration for the Top Left info slot on the GUI (shown in the top row of the desktop GUI,
     * and sandwiched between the Open Menu button on the left, and the Game Logo in the middle).
     * This config can specify that nothing is to be shown here, or it can specify the component to
     * be used (and any other additional data), for example:
     * - responsible gaming links
     */
    topRightInfoSlot : DesktopGuiInfoSlotConfig;
    
    /**
     * Configuration for the Open Menu button, in the top left of the screen.
     */
    openMenuButton : SimpleButtonConfig;

    /**
     * Configuration for the Close Session button, in the top right of the screen.
     */
    closeSessionButton : CloseSessionButtonConfig;

    /**
     * Configuration for the Add Credit button.
     */
    addCreditButton : SimpleButtonConfig;

    /**
     * Configures the Full screen button.
     */
    fullScreenButton : SimpleButtonConfig;

    /**
     * Configuration for the Toggle Sound Button.
     */
    toggleSoundButton : SimpleButtonConfig;

    /**
     * Configuration for the Social Media Buttons on the right of the screen.
     */
    socialMediaButtons : SocialMediaButtonsConfig;


    /**
     * Configuration for the auto play menu.
     */
    autoPlayMenuConfig: AutoplayMenuConfig; 

    /**
     * Configuration for the menu view tabs.
     */
    menuViewTabsConfig: MenuViewTabConfig;

    /**
     * Configuration for the free games notification button
     */
    freeGamesNotificationButton:freeGamesNotificationButtonConfig
}

/**
 * Features available for the Mobile Gui.
 */
interface MobileGuiConfig
{
    /**
     * Configuration for the Open Menu button.
     */
    openMenuButton : SimpleButtonConfig;

    /**
     * Configuration for the Close Session button.
     */
    closeSessionButton : CloseSessionButtonConfig;

    /**
     * Configuration for the auto play menu configuration.
     */
    autoPlayMenuConfig: AutoplayMenuConfig; 

    /**
     * Configuration for the menu view tabs.
     */
    menuViewTabsConfig: MenuViewTabConfig;

    /**
     * Configuration for the free games notification button
     */
    freeGamesNotificationButton:freeGamesNotificationButtonConfig;
}

/**
 * Configurable elements on the autoplay menu
 */
interface AutoplayMenuConfig
{

    /**
     * Configuration for the custom limit loss text box
     */
    lossLimitTextBoxes: SimpleFeatureConfig;

    /**
     * Configuration for the winnings limit loss text box
     */
    winningsLimitTextBox: SimpleFeatureConfig;
}

/**
 * This configuration allows the individual tabs to be either enabled or disabled
 */
interface MenuViewTabConfig
{
    /**
     * Setting to enable/disable the rules tab
     */
    rulesTab: SimpleFeatureConfig;

    /**
     * Setting to enable/disable the history tab
     */
    historyTab: SimpleFeatureConfig;

    /**
     * Setting to enable/disable the settings tab
     */
    settingsTab: SimpleFeatureConfig;

}

/**
 * This configuration enables specific behaviours on the free games notification when the player presses the ok button 
 */
interface freeGamesNotificationButtonConfig
{
    /**
     * When enabled the session stats screen will be shown after the ok button is pressed.
     */
    launchSessionStatsScreen: SimpleFeatureConfig;
}

//--------------------------------------------------------------------------------------------------
// Main Business Config object
//--------------------------------------------------------------------------------------------------
/**
 * Definition for the Business Config object. Each client deployment, will have a
 * Business Config instance associated with it. This allows us to customize features
 * of the client behaviour, specifically for certain websites.
 */
interface BusinessConfig
{
    /**
     * Indicates if Game Client logging should be enabled or not. When disabled, the
     * Game Client will still generate logs, but they will be minimal.
     */
    enableLogging : boolean;

    /**
     * The string name of the client deployment that this Business Config instance
     * specifies. Typically, the game client will be deployed to a game site, and
     * this field would give us a name for the licensee (but other specialized
     * deployments are possible).
     */
    clientDeploymentId : string;

    /**
     * A string that is prefixed to the gameClientId, when sending requests to
     * the backend server. For example, where gameClientId is "fowlplaygold", to
     * identify the specific deployment of fowl play gold (eg: on the licensee web
     * site "betium"), the clientIdPrefix is "betium", so we send the string
     * "betiumfowlplaygold" in a dedicated field in every request sent to the server.
     */
    clientIdPrefix : string;

    // TODO: We now have dedicated separate configs for V1 / V2, so this field can be
    // deprecated in favourmof simply using clientIdPrefix. It is only Merkur that uses
    // it (because merkur has different prefix between V1 / V2 platforms)
    /**
     * A string that will be prefixed to the gameClientId, when sending requests to
     * the backend server, for the V1 platform. This string is optional: if specified,
     * it serves as an override for the vlaue of clientIdPrefix: if absent, then the
     * value of clientIdPrefix will be used instead.
     */
    clientIdPrefixV1 ? : string;
    
    /**
     * The Url that will be used as the target endpoint in production, for all backend
     * service requests.
     */
    productionServerUrl : string;

    /**
     * Configuration for Resume Seession Functionality. This is an optional config: if
     * not specified, appropriate defaults will be used.
     */
    resumeSession ? : ResumeSessionConfig;

    /**
     * Configuration for the Start Session functionality. This is an optional config
     * object: some integrations (eg: SGDigital, which uses a dedicated AppController)
     * do not have any configurability for this functionality, so its pointless to
     * specify it in all cases. If an AppController which does support variability in
     * how a Session is started is being used, then if this field is not present, the
     * App Controller should simply use default behaviour (start the Session quietly,
     * using max available credit).
     */
    startSession ? : StartSessionConfig;

    /**
     * General configuration, to indicate if the Add Credit operation should be
     * available. A button may be configured for the GUI in certain places, but the
     * game client will also offer the player the chance to add credit when they are
     * out of credit (for example, in a dialog shown to the player). If this field is
     * absent, then "Add Credit" should be assumed to not be enabled at all for the
     * game client.
     */
    addCredit ? : AddCreditConfig;

    /**
     * A default Exit client action to be used for Mobile games. This is optional
     * (not all games will use this feature). If any view has an Exit Client button
     * configured, this is the default action that should be used (unless the view
     * gets its own custom configuration).
     */
    exitClientActionMobile ? : ExitClientAction;

    /**
     * A default Exit client action to be used for Desktop games. This is optional
     * (not all games will use this feature). If any view has an Exit Client button
     * configured, this is the default action that should be used (unless the view
     * gets its own custom configuration)
     */
    exitClientActionDesktop ? : ExitClientAction;

    /**
     * Configuration options for the Url Selection View.
     */
    urlSelectionView : UrlSelectionViewConfig;

    /**
     * Configuration options for the quick load feature
     */
    quickLoad: QuickLoadConfig;

    /**
     * Configuration for the splash screen view.
     */
    splashScreenView : SplashScreenViewConfig;

    /**
     * Configuration for the Cashier View.
     */
    cashierView : CashierViewConfig;

    /**
     * Configuration features for the Session Stats view.
     */
    sessionStatsView : SessionStatsViewConfig;
    
    /**
     * Configuration for a For Fun advert. This advert will only be shown on a ForFun game client
     * (its not 100% clear what this will mean, moving forward).
     */
    forfunAdvert : ForFunAdvertConfig;

    /**
     * Configuration for the main Desktop Gui.
     */
    desktopGui : DesktopGuiConfig;

    /**
     * Confoguration for the main Mobile Gui.
     */
    mobileGui : MobileGuiConfig;

    /**
     * Default action for an External History button. All new game clients will use the external
     * history functionality. We do not currently allow differentation between desktop and mobile,
     * although it cannot be ruled out that this will be changed in a future modification.
     */
    externalHistoryAction : OpenLinkAction;
    
    /**
     * A custom set of autoplay presets (whole numbers of autoplays which the player may select
     * for a new autoplay session). When not specified, a default set of values will be used.
     */
    autoplayPresets ? : number[];

    /**
     * The standard set of responsible gaming links to show.
     */
    responsibleGamingLinks : ResponsibleGamingLinkConfig[];

    /**
     * A list of all Gameplay Modes explicitly enabled for this client. This is only used now for
     * V1 builds (V2 always accepts Session Mode information through a url parameter).
     */
    enabledGameModes ? : GameplayMode[];

    /**
     * Indicates the string id of a url parameter, which contains information on what "game modes" are
     * allowed. This field is used for V1 platform only. Game mode in this case, means the type of session
     * that may be launched (eg: freeplay, realplay, etc).
     * 
     * If this field is configured in BusinessConfig, then the V1 game client should check for the presence
     * of a url parameter with the same key as the value of this field. The v1 platform passes a single
     * numerical enum to indicate the combination of game modes to be enabled (either a single game mode, or
     * several, can be specified). Not all V1 game client deployments will use this field (or the mechanism
     * of configurating game modes via url parameter), so this field is optional.
     */
    gameModeUrlParameter ? : string;
}