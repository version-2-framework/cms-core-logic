//--------------------------------------------------------------------------------------------------
// Comms Types
//--------------------------------------------------------------------------------------------------
/**
 * An object which describes additional Xml Http request headers which may be sent.
 */
interface CustomXmlHttpHeaders
{
    Authorization ? : string;

    Accept ? : string;
}

/**
 * Invoked when a Comms action succeeds.
 */
interface CommsSuccessHandler<R>
{
    /**
     * @param response
     * The response object, ready to be parsed and used.
     */
    (response : R) : void;
}

/**
 * Invoked when there is some kind of failure for a Comms action.
 */
interface CommsFailureHandler
{
    /**
     * @param clientErrorCode
     * An error code which gives us a hint about why the comms action failed.
     */
    (error : ServiceExecutionError) : void;
}

/**
 * Data object to describe an error that occurred in a comms operation.
 */
interface ServiceExecutionError
{
    /**
     * The Error code which should be displayed by the Game Client.
     */
    code : string;

    /**
     * Text description of the error. This is not what we show on-screen to the player
     * (which must be an appropriate localized piece of text). Instead, it is a debug
     * description that we can print to game client logs.
     */
    description : string;

    /**
     * The server indicates if this was a "fatal" error (eg: we expect the client to
     * shut down, because the server considers the session un-recoverable at this time).
     */
    isFatal : boolean;

    /**
     * Flag to indicate if the service failed due to a basic connection error: in this
     * case, it may be possible to re-attempt the operation (as opposed to the case where
     * the server rejected the request).
     */
    isConnectionError ? : boolean;
}

/**
 * Public API for the Comms module.
 */
interface CommsApi
{
    /**
     * Sends a request to the intended target.
     * @param path
     * The url that the request will be sent to.
     * @param requestData
     * The data to send in the request.
     * @param customHeaders
     * A set of additional headers to be added to the request object.
     * @param onSuccess
     * An action to be called once the request has succeeded. The definition of succes,
     * is:
     * - no comms error
     * - no server error returned in the message.
     * @param onFailure 
     */
    sendRequest(
        path:string,
        requestData:Object,
        customHeaders:CustomXmlHttpHeaders,
        onSuccess:CommsSuccessHandler,
        onFailure:CommsFailureHandler);
}

/**
 * An interface returned for any comms request: by using this, we can abort a comms action.
 * This is only for use in rare cases.
 */
interface CommsAction
{
    /**
     * Aborts the Comms Action, WITHOUT firing any error messages.
     * This is a very specialized operation: use with care!
     */
    abort : () => void;
}

/**
 * Info about a Comms Request, passed with the COMMS_REQUEST_SENT game event
 */
interface CommsRequestInfo
{
    messageType : string;

    isSessionRequest : boolean;
}