//import { OpenLinkUtil } from "../utils/OpenLinkUtil";
//import { ExitClientUtil } from "../utils/ExitClientUtil";

/**
 * Dependencies, as required by the Business Logic layer of the application.
 */
interface Dependencies
{
    buildInfo : BuildInfoApi;

    /**
     * An object representing url parameters.
     */
    urlParams : UrlParams;

    /**
     * Main Model instance.
     */
    model : Model;

    /**
     * The game specific configuration. This business code is only interested
     * in a small sub-set of the data on this object (so the interface only defines
     * a sub-set of it)
     */
    config : SlotConfig;

    /**
     * Used to open external links.
     */
    openLinkUtil : OpenLinkUtil;

    /**
     * Used for exiting the game client.
     */
    exitClientUtil : ExitClientUtil;

    /**
     * Business configuration object.
     */
    businessConfig : BusinessConfig;

    /**
     * Provides responses to requests ( when the Game Client is running in local mode )
     */
    localResponseProvider : LocalResponseProvider;

    /**
     * Services object. All requests sent to the server will be routed through this.
     */
    services : ServicesApi;

    /**
     * EventEmitter instance.
     */
    dispatcher : EventEmitter;

    /**
     * Spin Model instance for the current Game Client.
     */
    spinModel : SpinPhaseModel;

    /**
     * Utilities that relate to spin model data manipulation.
     */
    spinModelUtils : import("../model/SpinModelUtils").SpinModelUtils;

    /**
     * Used to build presentations for the Spin Phase.
     */
    spinPresentationBuilder : SpinPhasePresentationBuilder;

    /**
     * Bonus Model instance for the current Game Client.
     */
    bonusModel : BonusPhaseModel;
}