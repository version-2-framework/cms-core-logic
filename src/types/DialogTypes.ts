/**
 * A DialogManager is a component which provides a simple API for showing and hiding dialogs.
 * A DialogManager implmentation knows very little about the content of each dialog that you
 * ask it to show (other than the fact that each dialog can have a different basic type, which
 * must be rendered differently): it is not involved in any decisions about what sequences of
 * dialogs to show (this is handled by a DialogController). DialogManager is purely the lower
 * level api.
 * 
 * NOTE (Russell Wakely, 29.05.2020)
 * I originally considered calling this "DialogApi" - but the names of the various implementations -
 * eg: "WmgDialogApi", "SgDialogApi" - implied to me that each one would actually have a different
 * API (which is not true). Its job is to "show and hide dialogs" but I couldn't quite come up with
 * a good name, so DialogManager is what its called.
 */
interface DialogManager
{
    /**
     * Shows a sequence of dialogs to the player, and executes an optional action when all dialogs
     * have been dismissed. If the list of dialogs passed is empty, the method will automatically
     * skip ahead to calling any postDialogAction (in this way, we can dynamically build a list of
     * dialogs, and we can be lazy - if the list of dialogs was empty, we do not have to check if
     * we need to call this method, the method will basically do that for us).
     * @public
     * @param dialogConfigs
     * The ordered sequence of dialogs that you want to be shown. 
     * @param [postDialogAction]
     * An optional "after" action: this will be invoked after the last dialog in the sequence has
     * been dismissed. If the list of dialogs passed was empty, this method would be invoked immediately
     * on the call to showDialogs. Although this parameter is optional, the only intended use case
     * where we would not pass it, is if we are showing a fatal error dialog (no futher action is
     * expected within the game client).
     */
    showDialogs(
        dialogConfigs:DialogViewModel[],
        postDialogAction?:()=>void);

    // TODO: Not 100% yet that this is the appropriate level for this code to exist at,
    // but it will do for now, while we work out how it works..
    showServerNotifications(
        notifications : ServerToClientNotification[],
        postDialogAction? : () => void
    );
}

/**
 * A DialogViewModel defines how a single Dialog is configured (text, type, buttons, etc). The
 * type field specifies which sub-type of Dialog that the DialogViewModel specifies. Each type
 * can have additional properties, specific to that type.
 */
type DialogViewModel =
    UndmissableNotificationDialogViewModel |
    DismissableNotificationDialogViewModel |
    FatalErrorDialogViewModel |
    ConnectionErrorDialogViewModel |
    MultiChoiceDialogViewModel;

/**
 * Common interface of all Dialog View Models.
 */
interface AbstractDialogViewModel
{
    /**
     * Configures text that will be shown in the header. Not all Dialog View implementations
     * will support this functionality (for example, some Dialog Views integrated from an
     * external GUI, may show a preset message, and not allow us to set our own).
     * 
     * For dialog view implementations which DO support a header, the header field is still
     * optional (if not specified, the Dialog View will fall back to showing)
     * @todo: define exactly what the dialog view should fall back to showing in place of
     * the custom header
     */
    header ? : TextConfig;

    /**
     * A string that will be used for header directly : if present, will override any "message"
     * config that is present.
     */
    headerString ? : string;

    /**
     * Configures localizable text that will be shown for the main message. We expect all Dialog
     * View implementations to support this functionality.
     */
    message ? : TextConfig;

    /**
     * A string that will be used for message directly : if present, will override any "message"
     * config that is present.
     */
    messageString ? : string;
}

/**
 * A simple notification dialog view: will show an OK button at the button (its a message
 * that we expect the player to dismiss, and for no further action to be taken). The text
 * on the OK button is not configurable. As the name suggests, this notification can be
 * dismissed !
 */
interface DismissableNotificationDialogViewModel extends AbstractDialogViewModel
{
    type : "dismissableNotification";
}

/**
 * A notification, which *ideally* cannot be dismissed. Iif we are using a 3rd party GUI
 * to show notifications, then it's possible that the 3rd part GUI will not support the
 * concept of notifications which cannot be dismissed: however, if it's possible, then
 * this notification type is the one that should be used.
 */
interface UndmissableNotificationDialogViewModel extends AbstractDialogViewModel
{
    type : "undismissableNotification";
}

/**
 * Configuration for a dialog which indicates a fatal error of some kind.
 */
interface FatalErrorDialogViewModel extends AbstractDialogViewModel
{
    type : "fatalError";

    /**
     * The error code to display to the user.
     */
    errorCode : string;
}

/**
 * Configuration for a dialog, which indicates a connection error. A single button is shown -
 * allowing the player to dismiss the dialog (this generally carries the semantic meaning of
 * "try again" - and is normally only shown when a retry action is available).
 */
interface ConnectionErrorDialogViewModel extends AbstractDialogViewModel
{
    type : "connectionError";

    /**
     * Optional text message to show on a footer button. For
     */
    buttonText ? : DialogTextButtonConfig;
}

/**
 * Configuration for a Dialog which presents the player which some choices to make.
 */
interface MultiChoiceDialogViewModel extends AbstractDialogViewModel
{
    /**
     * Indicates that this is a Multiple Choice dialog (the player is being prompted to select
     * from some possible options).
     */
    type : "multiChoice";

    /**
     * A series of text button configs to show at the bottom of the dialog.
     */
    options : DialogTextButtonConfig[];
}

/**
 * Configuration for a text button in a Dialog.
 */
interface DialogTextButtonConfig extends TextConfig
{
    /**
     * For a Dialog Text Button, the action is optional (if no custom action is set,
     * default behaviour is that the dialog is simply dismissed).
     */
    action ? : () => void;
}

interface DialogController
{
    /**
     * Shows a single Dialog, telling the player that a game needs to be resumed.
     * @param onDismissed
     * The action invoked when the Resume Game notification Dialog is dismissed.
     */
    showResumeGameNotification(onDismissed:()=>void) : void;

    /**
     * Shows a series of Dialogs, and executes an action when they are completed.
     * @param dialogConfigs
     * An ordered series of dialog configurations. DialogController will show these in the exact
     * order that they are provided. It's valid for there to be only a single item in the list.
     * @param postDialogAction 
     * An action called, when all dialogs have been dismissed.
     */
    showDialogs(dialogConfigs:DialogViewModel[], postDialogAction?:()=>void) : void;

    // TODO: Again, is this really a "DialogController" method - or should it be solely
    // on the "WmgDialogController" implementation ?
    showServerNotifications(notifications:ServerToClientNotification[], postDialogAction:()=>void);

    /**
     * Shows any appropriate "post game" notifications to the player, and executes a callback
     * once these have all been dismissed.
     * @param onComplete
     */
    showPostGameNotifications(onComplete:()=>void) : void;

    /**
     * Shows the most appropriate error dialog to the player, for when a service execution error
     * has occurred. Some service execution errors are recoverable: the error contains data to
     * indicate this.
     * Different implementations of Dialog Controller, may pick different custom mappings for this
     * case.
     * @param error
     * @param actionOnRetry
     * A callback that will be invoked, if the error dialog is dimissed
     */
    showServiceExecutionErrorDialog(error:ServiceExecutionError, actionOnRetry?:()=>void) : void;
}