/**
 * An Encryptor is anything which implements an encrypt / decrypt method.
 * Used during server comms.
 */
interface Encryptor
{
    /**
     * Encrypts a string (ready to be sent).
     * @public
     * @param {string} message
     * The message to encrypt.
     * @return {string}
     * Encrypted message.
     */
    encrypt(message : string) : string;

    /**
     * Decrypts a string (ready to be read from).
     * @public
     * @param {string} cipher
     * The message to decrypt.
     * @return {string}
     * The decrypted message.
     */
    decrypt(cipher : string) : string;
}