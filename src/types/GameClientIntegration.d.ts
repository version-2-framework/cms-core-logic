/**
 * Different types of Game Client integration available.
 */
type GameClientIntegrationMode = "wmg" | "sgdigital";