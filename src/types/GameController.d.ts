/**
 * Basic API of a GameController, that will be used by an AppController. The general contract of a
 * GameController instance, is that it is able to fetch its own data during its life-cycle (including
 * data set on model)
 * @todo: Confirm if we want to pass in BetSettings, or leave that as it is.
 */
interface GameController
{
    /**
     * Starts a brand new game.
     * @param onComplete
     * Callback that will be executed when the game has successfully completed. Data about
     * the completed game will be passed to the callback.
     * @param onError 
     * Callback that will be executed when the game has failed at any intermediate step. The
     * game may be in a paused state (this will be indicated by a data object passed to the
     * callback).
     * @param onLmsCustomData
     * Callback executed when the LMS (Licensee Management Server) returns some custom data
     * to the game client. This is only ever expected to happen on Game Start Replies (whatever
     * exact reply that is), or a Game Complete Reply. Not all integrations will be interested
     * in this data (in fact, most integrations will not return it). SGDigital is an example
     * of an integration which does (we basically just have to forward the data onto SG Digital's
     * GUI - our game client is not otherwise concerned with it).
     */
    startNewGame(
        onComplete : GameCompleteCallback,
        onError : GameErrorCallback,
        onLmsCustomData ? : GameLmsDataReceivedCallback<*>
    ) : void;

    /**
     * Resumes an existing game, based on game data returned by the server. The GameController
     * will assume that restored game phase results data is already placed in the correct
     * phase model instance, for quick access: you only need to provide the id of the game]
     * phase that you want it to restore to, to skip to a certain point.
     * @param restoredGamePhaseId
     * The string id of the game phase to restore to.
     * @param onComplete 
     * Callback that will be executed when the game has successfully completed. Data about
     * the completed game will be passed to the callback.
     * @param onError 
     * Callback that will be executed when the game has failed at any intermediate step. The
     * game may be in a paused state (this will be indicated by a data object passed to the
     * callback).
     * @param onLmsCustomData
     * Callback executed when the LMS (Licensee Management Server) returns some custom data
     * to the game client. This is only ever expected to happen on Game Start Replies (whatever
     * exact reply that is), or a Game Complete Reply. Not all integrations will be interested
     * in this data (in fact, most integrations will not return it). SGDigital is an example
     * of an integration which does (we basically just have to forward the data onto SG Digital's
     * GUI - our game client is not otherwise concerned with it).
     * @todo: Its not clear if the onLmsCustomData callback is relevant in the case of a restored
     * game session.
     */
    resumeGame(
        restoreGamePhaseId : string,
        onComplete : GameCompleteCallback,
        onError : GameErrorCallback,
        onLmsCustomData ? : GameLmsDataReceivedCallback<*>
    ) : void;

    // TODO: These are only proposed methods. They exist to illustrate that we may need
    // to interrupt or abort a game that is in progress in certain cases
    // Why would we need to abort ??
    abortGame() : void;

    // TODO: Not sure that we need this, actually
    /**
     * Pauses a game in progress.
     */
    pauseGame() : void;

    // TODO: Not sure that we need this, actually
    /**
     * Unpauses a paused game.
     */
    unpauseGame() : void;

    isGameInProgress() : boolean;

    isGamePaused() : boolean;

    isGameAborted() : boolean;
}

/**
 * Data describing a game that just completed.
 */
interface GameCompleteData
{
    /**
     * Indicates if the game was played in autoplay mode.
     */
    //wasAutoplayGame : boolean;

    /**
     * Final winnings from the game that was paid out to the player.
     */
    totalWinnings : number;
}

/**
 * Callback executed when a game is fully complete (all phases have been played, and
 * the Game Complete Service has been successfully executed on the back-end).
 */
type GameCompleteCallback = (data : GameCompleteData) => void;

interface GameErrorData
{

}

type GameErrorCallback = (data : GameErrorData) => void;

/**
 * Callback type, for when LMS (Licensee Management Server) custom data is received.
 * This custom data can be returned on select backend services (eg: the first request
 * of a new Credit or PromoGame sub-bet, or a Game Complete Reply). Typically, the
 * game client is not interested in the contents of this data, other than to forward
 * it on to that licensee's GUI (this was the solution adopted for SG Digital).
 */
type GameLmsDataReceivedCallback<R> = (data : R, resumeGameFlow : ()=>void) => void;