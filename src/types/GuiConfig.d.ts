/**
 * Configuration for a text field.
 */
interface TextConfig
{
    /**
     * The localization id to look up for the text.
     */
    localizationId : string;

    /**
     * An optional set of key / value pairs, representing modifications
     * to apply to the text loaded from localizationId.
     */
    localizationMods ? : Object;
}

/**
 * Configuration for a button.
 */
interface ButtonConfig
{
    /**
     * The action to execute when the button is pressed. This is guaranteed
     * to already have context bound to it.
     */
    action : () => void;
}

/**
 * Configuration for a button with a label.
 */
interface TextButtonConfig extends ButtonConfig, TextConfig
{
    
}

/**
 * Configuration for a loading screen display label 
 * 
 */
interface LoadingScreenSessionLabelConfig extends ButtonConfig, TextConfig
{

}

/**
 * Config for a request to show a specific message on the Message Bar.
 * @todo:
 * Decide what additional parameters are required.
 */
interface MessageBarConfig extends TextConfig
{

}

interface GuiComponentPosition
{
    /**
     * The x position (as a percentage of screen width) of the component.
     */
    x : number;
    
    /**
     * The y position (as a percentage of screen height) of the component.
     */
    y : number;
 
    /**
     * Target height of the component, as a percentage of screen height.
     */
    height : number;
}

interface GuiButtonPosition extends GuiComponentPosition
{
    /**
     * Configure the orientation of tooltips for this button.
     */
    tooltipOrientation : TooltipOrientation;

    /**
     * Configure the position of tooltips for this button.
     */
    tooltipPosition : TooltipPosition;
}

/**
 * Configures a set of sounds for the GUI.
 */
interface GuiSoundsConfig
{
    /**
     * The id of the sound to play when a standard Gui button is pressed. The same sound will be
     * played for all of the following buttons: OPEN_MENU, CLOSE_SESSION, ADD_CREDIT, TOGGLE_FULL_SCREEN.
     * If this field is null, then no sound will be played when these buttons are pressed. This same
     * sound will also be used in other contexts (its basically a generic GUI sound), eg: in standard
     * menus, when buttons are pressed.
     */
    guiButtonSound : string | null;

    /**
     * The volume that the generic gui button sound will be played at (from 0 to 1 inclusive).
     */
    guiButtonVolume : number;

    /**
     * The id of the sound to play when the Spin button is pressed. If this field is null, then no sound
     * will be played.
     */
    spinButtonSound : string | null;

    /**
     * The volume that the Spin button sound will be played at (from 0 to 1 inclusive).
     */
    spinButtonVolume : number;

    /**
     * The id of the sound to play when the Bet button is pressed. If this field is null, then no sound
     * will be played.
     */
    betButtonSound : string | null;

    /**
     * The volume that the Bet button sound will be played at (from 0 to 1 inclusive).
     */
    betButtonVolume : number;

    /**
     * The id of the sound to play when the Autoplay button is pressed. If this field is null, then no
     * sound wil be played.
     */
    autoplayButtonSound : string | null;

    /**
     * The volume that the Autoplay button sound will be played at (from 0 to 1 inclusive).
     */
    autoplayButtonVolume : number;
}

/**
 * Layout config for a GUI. This defines layout properties which may be customized for individual
 * games. One instance of this config will be provided for each different GUI target that is supported
 * (eg: standard WMG desktop / WMG mobile layouts, and any other layouts required when integrating
 * with other guis)
 */
interface GuiLayoutConfig
{
    // TODO: Wrap up game logo into a standar GuiComponentPosition ?
    // These properties are not actually being used at the moment - should they ?
    /**
     * Desired height of the game logo, as a percentage of screen height. The game logo will be scaled
     * proportionately, so that its vertical height matches this requested height, unless:
     * - the new width of the game logo would be wider than the safe area reserved for game logo (and
     *   there are side components being shown) - in which case, game logo will be scaled to fit inside
     *   of the safe area.
     */
    gameLogoHeight : number;

    /**
     * Target horizontal center position of the game logo. This value is as a percentage of screen width:
     * a value of 50, places the horizontal center of the game logo at 50% of the screen area.
     */
    gameLogoCenterPosX : number;

    /**
     * Target vertical center position of the game logo. This value is as a percentage of screen height:
     * a value of 5 would places the vertical center of the game logo, at 5% from the top of the screen.
     */
    gameLogoCenterPosY : number;

    /**
     * Defines configurable layout for the message bar component.
     */
    messageBar :
    {
        /**
         * Height of the message bar, as a percentage of screen height.
         */
        height : number;

        /**
         * Width of the message bar, as a percentage of screen width.
         */
        width : number;

        /**
         * Offset from horizontal screen center (as a percentage of screen width)
         */
        centerOffset : number;

        /**
         * Offset from vertical screen bottom (as a percentage of screen height)
         */
        bottomOffset : number;
    }

    /**
     * Defines configurable layout
     */
    uiStats :
    {
        /**
         * The width of the Ui Stats bar, as a percentage of screen width. This should normally
         * be set to 100%.
         */
        width : number;

        /**
         * height of the Ui Stats bar, as a percentage of screen height.
         */
        height : number

        /**
         * Offset from horizontal screen center (as a percentage of screen width)
         */
        centerOffset : number;

        /**
         * Offset from vertical screen bottom (as a percentage of screen height)
         */
        bottomOffset : number;
    }

    // TODO: With these propertires, this is actually a "SlotGuiLayout"
    /**
     * Defines positions for the reels.
     */
    reelsPos : GuiComponentPosition;
    
    /**
     * Defines the position of the Spin button on screen.
     */
    spinButtonPos : GuiButtonPosition;

    /**
     * Defines the position of the Bet button on screen.
     */
    betButtonPos : GuiButtonPosition;

    /**
     * Defines the position of the Autoplay button on screen.
     */
    autoplayButtonPos : GuiButtonPosition;
}

/**
 * Configuration options for text on the loading (splash) screen.
 */
interface GuiLoadingScreenTextStyle
{
    /**
     * Configures the fill colours for loading screen text.
     */
    fill : string[] | number[];

    /**
     * Configures the fontWeight of the loading screen text.
     */
    fontWeight : "normal" | "bold" | "bolder" | "lighter";

    /**
     * Configures stroke size for loading screen text. This is a proportional value, representing
     * a percentage of screen-height, and it is in the range of 1 to 100. EG: a value of 1 indicates
     * that the stroke should have a thickness equivalent to 1% of screen-height.
     */
    strokeSize: number;

    /**
     * Configures stroke colour for loading screen text.
     */
    strokeColor: number | string;
}

/**
 * Style configuration for the standard Gui Game Menu View (which is dynamically rendered)
 */
interface GuiGameMenuStyle
{
    /**
     * Colour of text used in the menu header
     */
    headerTextColour : number | string;
    
    /**
     * Standard text colour: used for normal paragraph text in the rules, paytable
     * values text, titles placed next to settings in the settings tab, and any other
     * general messages (eg: the "open external history" message)
     */
    textColour : number | string;

    /**
     * Text colour used for any title text in the rules page.
     */
    titleTextColour : number | string;

    /**
     * Text colour used for "secondary" text messages, eg: the points message in the
     * paytable, any sub-messages added to the rules section.
     */
    secondaryTextColour : number | string;

    /**
     * The colour of the main background of the Game Menu
     */
    bgColour : number | string;

    /**
     * The colour used on the header and footer bars of the Game Menu
     */
    headerFooterColour : number | string;

    /**
     * The colour to use for buttons in the menu (eg: settings buttons)
     */
    btnColour : number | string;

    /**
     * The colour to use for "highlight" state on a menu button.
     */
    btnColourHighlight : number | string;

    /**
     * Colour for the background of a toggle button area.
     */
    toggleBtnBgColour : number | string;

    /**
     * Colour used on text on a button.
     */
    btnTextColour : number | string;
    
    /**
     * Colour of text used on a toggle button setting.
     */
    toggleBtnTextColour : number | string
    ;
    /**
     * Colour used on winline blocks involved in a win. This same colour will
     * also be applied to the little winline numbers text (so the text and win
     * positions will match visually)
     */
    winlineWinningColour : number | string;

    /**
     * Colour used on winline blocks that are not involved in a win.
     * a winline win)
     */
    winlineStandardColour : number | string;
    
    /**
     * The colour of the scrollbar's bg component
     */
    scrollbarBgColour : number | string;

    /**
     * The colour of the scrollbar's button component
     */
    scrollbarBtnColour : number | string;
}

/**
 * Style configuration for the standard Gui Dialog View (which is dynamically rendered)
 */
interface GuiDialogStyle
{
    /**
     * The colour of the main background of the Dialog View.
     */
    bgColour : number | string;

    /**
     * Colour of the footer button background.
     */
    btnColour : number | string;

    /**
     * The height of the dialog view, as a percentage of screen height.
     */
    dialogHeight : number;

    /**
     * The ratio of width to height of the dialog view.
     */
    dialogAspect : number;

    /**
     * Height of text for the dialog, as a percentage of screen height.
     */
    textHeight : number;

    /**
     * Height of the header section, as a percentage of screen height.
     */
    headerHeight : number;

    /**
     * Height of the footer section, as a percentage of screen height.
     */
    footerHeight : number;

    /**
     * Height of a button in the footer section, as a percentage of screen height.
     */
    btnHeight : number;

    /**
     * Name of the font to use for all text.
     */
    font : string;

    /**
     * Colour of the header text.
     */
    headerTxtColour: number | string;

    /**
     * Colour of the body text.
     */
    bodyTxtColour: number | string;

    /**
     * Colour of the button text.
     */
    buttonTxtColour : number | string;
}

interface GuiUiStatsStyle
{
    /**
     * Colour for the clock text
     */
    clockColor : number | string;

    /**
     * Colour of title text for any stat.
     */
    titleColor : number | string;

    /**
     * Colour of value text for any stat. This field is optional : if not set, "titleColor" shall be
     * applied uniformly for both title and value. In this mode, GUI implementations can fall back to
     * using a single text field.
     */
    valueColor ? : number | string;
}

interface GuiUiStatsBgStyle
{
    /**
     * Thickness of the stroke on the background of the Ui Stats bar (as a percentage of screen height).
     */
    lineThickness : number;

    /**
     * Colour for any stroke on the background of the Ui Stats bar.
     */
    lineColor : string | number;

    /**
     * Alpha level for any stroke on the background of the Ui Stats bar.
     */
    lineAlpha : number;

    /**
     * Colour of the background of the Ui Stats bar.
     */
    bgColor : string | number;

    /**
     * Alpha level for the backrgound of the Ui Stats bar.
     */
    bgAlpha : number;

    // TODO: These want moving out
    w : number;
    
    h : number;
    
    angle : number;
    
    centerOffset : number;

    bottomOffset : number;
}

/**
 * Configuration options for Message Bar style.
 */
interface GuiMessageBarStyle
{
    /**
     * The id of the font family which will be applied to the message bar text.
     */
    font : string;

    /**
     * Colour of the font on the message bar.
     */
    fontColor : string | number;

    /**
     * Weighting to use on the font.
     * @default "normal";
     */
    fontWeight : "normal" | "bold";

    /**
     * Indicates if a background should be shown for the message bar. If set to true,
     * then the background will be set to the dimensions of the message bar (and the
     * text will be scaled down by a small aditional amount, to fit inside the back
     * ground).
     */
    useBackground : boolean;

    /**
     * Indicates if a stroke should be applied to the text.
     */
    useStroke : boolean;

    /**
     * Colour of the stroke on the text.
     */
    strokeColor : number | string;

    /**
     * Target stroke size, as a percentage of screen height.
     */
    strokeSize : number;

    /**
     * Sets the miter limit for the message bar text's stroke.
     */
    miterLimit : number,

    /**
     * Colour for an optional background element.
     */
    bgColor : string | number;

    /**
     * Alpha level for the optional background element.
     */
    bgAlpha : number;

    /**
     * @todo: Not sure what this is for yet, the stroke on the background ?
     */
    lineThickness : number;

    /**
     * @todo: Not sure what this is for, i guess its stroke on the background ?
     */
    lineColour : number;

    /**
     * @todo: Not sure what this is for, i guess its stroke on the background ?
     */
    angle : number;

    /**
     * An additional scaling factor, useful for bringing the text into the correct size
     * for a single line. The MessageBar has a size, which is configured independently
     * in its layout. The MessageBar then creates its text at a size relative to the
     * messageBar (if a background is used, it shrinks the text a bit, to fit within the
     * background).
     * 
     * TextScaleFactor allows us to manually tweak the text scaling, relative to the
     * background area. A value of 1 is "unity" - the text occupies the full area of the
     * Message Bar. If we want it to actually occupy 50% of the messagebar, set a value
     * of 0.50. This field also serves another usefull purpose: fonts which size in a
     * way that is unexpected (for example, they are always a bit too large) can be
     * adjusted manually with this field.
     */
    textScaleFactor : number;
}