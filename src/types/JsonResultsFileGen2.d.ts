// TODO: This can probably be deprecated, if there is never a plan to load a json results file.

interface Gen2JsonResultsFile
{
    type : "singlespin" | "twospin";
}

interface Gen2SingleSpinJsonResultsFile extends Gen2JsonResultsFile
{
    /**
     * @inheritDoc
     */
    type : "singlespin";

    /**
     * Sequence of single spin sub bet results.
     */
    results : SingleSpinSubBetResult[];
}

interface Gen2TwoSpinJsonResultsFile extends Gen2JsonResultsFile
{
    /**
     * @inheritDoc
     */
    type : "twospin";

    /**
     * Sequence of two spin sub bet results
     */
    results : TwoSpinSubBetResult[];
}

interface SingleSpinSubBetResult
{
    spin : Gen2SingleSpinPhaseResult;

    bonus ? : Gen2BonusPhaseResult;

    freespin ? : Gen2FreeSpinPhaseResult;
}

interface TwoSpinSubBetResult
{
    spin1 : Gen2Spin1PhaseResult;
    
    spin2 ? : Gen2Spin2PhaseResult;

    bonus ? : Gen2BonusPhaseResult;

    freespin ? : Gen2FreeSpinPhaseResult;
}