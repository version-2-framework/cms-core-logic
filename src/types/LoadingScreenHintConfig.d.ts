interface LoadingScreenHintConfig
{
    /**
     * Positioning for the image.
     */
    img : GuiComponentPosition;
    
    /**
     * Defines the positioning and style of the text.
     */
    text :
    {
        /**
         * X position of the text, as a proportion of screen width (in the range 0 to 100)
         */
        x : number;

        /**
         * Y position of the text, as a proportion of screen height (in the range 0 to 100)
         */
        y : number;
        
        /**
         * Size of the text, as a proportion of screen height (in the range 0 to 100)
         */
        size:number;

        /**
         * Wrap width of the text, as a proportion of screen width (in the range 0 to 100)
         */
        wrapWidth:number;
        
        /**
         * Colours used for the text.
         */
        colors : number[] | string[];
        
        /**
         * Optional data, describing any stroke to apply.
         */
        strokeData ? :
        {
            /**
             * Colour to apply for the stroke.
             */
            strokeColor:number | string;
            
            /**
             * Size of the stroke, as a proportion of screen height (in the range 0 to 100)
             */
            strokeSize:number
        };
    }
        
    /**
     * Indicatges if a drop shadow should be applied.
     */
    dropShadow: boolean;
        
    /**
     * Alpha value of any drop-shadow component (a floating point number, in the range 0 to 1)
     */
    dropShadowAlpha: number;

    /**
     * Colour of the drop shadow (if not set, defaults to 0x000000)
     */
    dropShadowColor ? : number | string;
}