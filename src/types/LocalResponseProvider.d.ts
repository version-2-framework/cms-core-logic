interface LocalResponseProvider
{
    /**
     * Returns a locally generated response, for a given request. The request
     * is accepted as an object (the form that the request is generated in,
     * before it is turned to encrypted string and sent to the Game Engine
     * Server). The response is returned as a JSON string (the form we expect
     * to receive from the server, after decryption). This allows us to double
     * test the process for parsing the results objects locally.
     * (so it can )
     * @public
     * @param {string} requestType
     * The type of request to process. 
     * @param {Object} request
     * The request data object.
     * @return {Object}
     */
    getResponseFor(requestType, request);
}