/**
 * A LocaleApi implementation, provides tools to get translated strings.
 */
interface LocaleApi
{
    /**
     * Get the translation for a string with the specified identifier. If no translation is
     * available under the localizationId passed, then the value returned will be the localization
     * id.
     * @inheritDoc
     * @param localizationId
     * The string key under which the required localization is stored
     * @param [localizationSubstitutions]
     * An optional set of keys to be substituted in the output localization. For example, if
     * the raw text stored under localization id is "Credit remaining : [CREDIT]", the [CREDIT] key
     * can be substituted using this parameter (eg: by passing {"[CREDIT]":"$1200.00"}). This allows
     * for dynamic substitution of values in different languages, including cases where word order
     * may change (or where the key to substitute may not be present in all available localizations).
     * @returns
     * The localized value
     */
    getString(localizationId:string, localizationSubstitutions?:{[key:string]:string}) : string;
}