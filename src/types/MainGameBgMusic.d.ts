interface MusicConfig
{
    mainGameBgMusic ? : MainGameBgMusicConfig;
}

/**
 * Intersection type, used for specifying how main game background music should work.
 */
type MainGameBgMusicConfig =
    NoMainGameBgMusicConfig |
    LoopingMainGameBgMusicConfig;

/**
 * Indicates that there should be no main game background music.
 */
interface NoMainGameBgMusicConfig
{
    type : "none";
};

/**
 * Specifies that there should be a single looping tune for the main game background music.
 */
interface LoopingMainGameBgMusicConfig
{
    type : "looping";

    /**
     * The id of the music asset to play.
     */
    assetName : string;

    /**
     * The volume at which the music should be played.
     */
    volume : number;

    /**
     * Indicates if the music should be suspended during a Slot Big win presentation. When set to
     * true, the BigWin animation being shown in the slot, will suspend background music.
     */
    suspendDuringSlotBigWin ? : boolean;

    /**
     * Indicates if the music should be suspended during a Slot Total Win presentation. When set to
     * true, the TotalWin animation being shown in the slot, will suspend background music.
     */
    suspendDuringSlotTotalWin ? : boolean;
    
    /**
     * Indicates if the Bg Music should be automatically restored at the end of the BigWin animation.
     */
    restoreAfterSlotBigWin ? : boolean;

    /**
     * Indicates if the Bg Music should be automatically restored at the end of the TotalWin animation.
     */
    restoreAfterTotalWinComplete ? : boolean;
};