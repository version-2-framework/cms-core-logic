type ModelApi = Model;

interface Model
{
    /**
     * Checks if this is a Mobile device.
     */
    isMobile() : boolean;
    
    /**
     * Checks if this is an Android device.
     */
    isAndroid() : boolean;
    
    /**
     * Sets the Comms Mode for the application.
     */
    setCommsMode(commsMode : string);

    /**
     * Returns the active comms mode of the application.
     */
    getCommsMode() : string;

    /**
     * Returns the value for the Client Environment Variable. This is set via the url parameter 'clientEnv'.
     */
    getClientEnvironmentValue() : number;

    /**
     * Returns the currently active currency format.
     */
    getCurrencyFormat() : CurrencyFormat;

    /**
     * Updates the active Currency Format (this is equivalent to setting the actual currency
     * in use by the game client)
     */
    setCurrencyFormat(value : CurrencyFormat) : void;

    /**
     * Returns the player's nickname.
     */
    getPlayerNickName() : string;

    /**
     * Sets the session type to use.
     * @param mode
     */
    setSessionType(mode : GameplayMode) : void;
    
    /**
     * Returns the currently active Session Type.
     */
    getSessionType() : GameplayMode;

    /**
     * Indicates if this is a realplay session
     */
    getSessionIsRealPlay() : boolean;

    /**
     * Indicates if this is a funbonus session.
     */
    getSessionIsFunBonus() : boolean;
    
    /**
     * Returns Info about the Session in progress.
     */
    getSessionInfo() : SessionInfo;
    
    /**
     * Returns the number of minutes that the Session Inacitivity Timeout should
     * run for.
     */
    getSessionInactivityTimeout() : number;

    /**
     * Indicates if a game Session is currently in progress.
     */
    isSessionInProgress() : boolean;
    
    /**
     * Indicates if any currently open game session is a reloaded game session.
     */
    isSessionReloaded() : boolean;

    /**
     * Indicates if any currently open game session is a Realplay session.
     */
    isSessionRealplay() : boolean;

    /**
     * Indicates if any currently open game session is a Freeplay session.
     */
    isSessionFreeplay() : boolean;

    /**
     * Indicates if any currently open game session is a FunBonus session.
     */
    isSessionFunBonus() : boolean;

    /**
     * Returns the total amount of credit added to the current session (in points).
     * This is the sum of all transfers, including the initial session transfer.
     */
    getSessionCreditAdded() : number;

    /**
     * Returns the total amount of credit spent in the current session (in points).
     * This is the sum of all bets made.
     */
    getSessionCreditSpent() : number;

    /**
     * Returns the total amount of credit won in the current session (in points)
     */
    getSessionCreditWon() : number;

    /**
     * Returns the total number of games that have been played in the current session.
     */
    getSessionNumGamesPlayed() : number;

    /**
     * Returns the current value of the player's Session Wallet. This is the amount
     * of credit the player has in their session. This field is different to "player
     * wallet". Session Wallet is always the last known value returned from the server.
     * (The server also expects the game client to send the correct value for this
     * field in various requests). PlayerWallet is really the same thing, but we are
     * free to change the value during the game, after winning sequences are shown,
     * etc (because we are not going to send that value to the server)
     */
    getSessionWallet() : number;

    /**
     * Returns the current value of the player's Session.SuperBet
     */
    getSessionSuperBet() : number;

    /**
     * Returns the current value of the player's Session.NumFreeGames
     */
    getSessionNumFreeGames() : number;

    /**
     * Returns the minimum amount of credit which may be transferred to a new game session.
     */
    getMinSessionTransfer() : number;

    /**
     * Indicates if it is possible to add more credit to the current game session, based on
     * "max credit in session" rules, ie: whether the player has already added some maximum
     * amount (or hit any other general rules that might exist). Does not take into account
     * affordability (eg: whether the player actually has any account money left to add), or
     * any game client configuration rules (eg: if Add Credit feature is enabled or disabled
     * for a game client deployment).
     */
    canAddMoreCreditToSession() : boolean;

    /**
     * Indicates if its valid for the Game Client to perform a Session Restart action
     * (eg: start a new session, after one was manually closed within the game client).
     * This takes into account rules about max session transfers which may exist in
     * different countries: it does not take into account affordability (whether the
     * player has any money left for a new transfer), or any game client configuration
     * rules (eg: if Restart Session feature is enmabled or disabled for a game client
     * deployment).
     */
    canRestartSession() : boolean;

    /**
     * Indicates if the minimum game timer feature should be enabled. This is a country
     * specific rule, so its configured through the model (and not through business config).
     */
    isMinimumGameTimerRequired() : boolean;

    /**
     * Returns the minimum time a game should last, in minutes. If no minimum game time restriction
     * exists, then a value less than 1 will be returned.
     */
    getMinimumGameTimeMins() : number;

    /**
     * Returns any cached instance of a generic Wmg Promo Games award. Returns null when there is no
     * Promo Games Award to be consumed.
     */
    getPromoGamesAward() : WmgPromoGamesAward | null;

    /**
     * Clears any cached generic Wmg Promo Games award. Should be called when the Promo Games award has
     * been accepted by the player.
     */
    clearPromoGamesAward() : void;

    // TODO: Consider if this should accept the whole session description instead, and should
    // handle the Bet level selection (consider that Model has access to bet settings meta-data:
    // this definitely could be handled here, its a case of where do we want to place the flexibility ?)
    /**
     * Manually sets a promo games session award (sometimes, the server doesnt tell us this data
     * immediately, or the player must be able to make a choice).
     */
    setPromoGamesSessionNumGamesSelected(numPromoGames : number) : void;
    
    /**
     * Returns the current value of "Player Wallet". This is the value to display in
     * the "Coin" field for the game. It is a simulation of player's current credit,
     * but it can include any winnings that may have been shown. Player Wallet is a
     * local simulation value: we are free to change its actual value in any way that
     * we need to support the game simulation that is shown to the player. We only
     * need to sync this to the same value as "SessionWallet" when no game is in progress.
     */
    getPlayerWallet() : number;

    /**
     * Updates the current value of wallet / playerWallet, to a new value which may have been returned by an
     * external operation. Some licensee integrations use external GUIs, and for some of these, there
     * may be an external "add credit" operation, which also might nudges our GUI to update itself at
     * an arbitrary point (eg: during a game). Therefore, we can forward this data on to our model, so
     * that the player sees the expected updated value on-screen.
     * 
     * This method can be expected to behave slightly differently in 2 different scenarios (when a game is in
     * progress, and when no game is in progress), and also takes "FreeRounds" into account: in general, it
     * tries to update simulation values first (playerWallet, if a game is in progress), and we rely on the
     * next piece of updated data from our backend
     * 
     * @param newValue 
     */
    setUpdatedWallet(newValue : number) : void;

    /**
     * Returns the current value of "Player Winnings": this is the total amount shown
     * to have been won, during the current game in progress.
     */
    getPlayerWinnings() : number;

    /**
     * Returns the current value of "Player Superbet".
     */
    getPlayerSuperbet() : number;

    /**
     * Returns the total number of promo-games the player currently has (the simulation value)
     */
    getPlayerNumPromoGames() : number;

    /**
     * Returns the number of Free Spins the player has remaining.
     */
    getFreeSpinsNumRemaining() : number;

    /**
     * Returns the total amount won for any FreeSpins session in progress for the current SubBet. Will always be
     * be reset to 0, when a new SubBet starts. This is simulation data : when a FreeSpin phase is in progress, this
     * value will be updated in correspondence with any credit wins shown on screen. It will only reresent final win
     * from the FreeSpin phase, once the FreeSpin phase has completed and all winnings are shown.
     */
    getFreeSpinsWinnings() : number;

    /**
     * Returns the total amount won from the last game completed.
     */
    getLastGameWinnings() : number;

    /**
     * Returns the string id of a game phase to restore to (or null, if nothing
     * to restore).
     */
    getRestoredGamePhase() : string;


    /**
     * Returns the object for the restored game results  (or null, if nothing
     * to restore).
     */
     getRestoredGameResult() : object;

    /**
     * Records a Credit Win of a specific amount. If autoplay is in progress, this will
     * automatically be added onto autoplay stats related to winnings.
     */
    recordCreditWin(amountWon : number);

    /**
     * Records a win which awards a specific amount of SuperBet. This should be called, when the win has been shown.
     */
    recordSuperBetWin(amountWon : number);

    /**
     * Records a specific number of Free Spins as having been won. This should be called, when the win has been shown.
     */
    recordFreeSpinsWin(numWon : number);

    /**
     * Logs the FreeSpin phase starting : should be called at the start of a FreeSpin phase, exactly
     * once, by GameController.
     */
    logFreeSpinPhaseStarted();

    /**
     * Logs the FreeSpin phase finishing : should be called at the end of a FreeSpin phase, exactly
     * once, by GameController.
     */
    logFreeSpinPhaseFinished();

    /**
     * Decrements the number of FreeSpins remaining by 1, and notifies the world.
     */
    logFreeSpinRoundStarted();
    
    /**
     * Sets the number of games to play in an autoplay session.
     */
    setAutoplayNumGamesTotal(numGames : number) : void;

    /**
     * Returns the total number of games that were selected to be played in autoplay mode.
     */
    getAutoplayNumGamesTotal() : number;

    /**
     * Sets the number of autoplay games, to the last user selected value. This is a utility
     * method for repeating previous autoplay sessions, without requring the user to do any
     * thing other than agreeing to do this.
     */
    setAutoplayNumGamesToLastSelectedValue() : void;
    
    /**
     * Returns the number of games that have been played in any current active
     * autoplay session.
     */
    getAutoplayNumGamesPlayed() : number;
    
    /**
     * Returns the number of games remaining to be played in autoplay mode.
     */
    getAutoplayNumGamesRemaining() : number;

    /**
     * Returns the amount won (in points) in the current autoplay session.
     */
    getAutoplayWinnings() : number;

    /**
     * Returns the amount spent (in points) in the current autoplay session.
     */
    getAutoplaySpent() : number;
    
    /**
     * Returns the amount lost (in points) in the current autoplay session. This is
     * defined as the difference between what has been won in the autoplay session,
     * and what has been spent: however, if the player has won more than they have
     * spent, then a value of 0 is returned (as negative losses are not useful to
     * track in this context). This can be understood as follows:
     * - If the player has won 100, but spent 200, then losses are 100
     * - If the player has won 200, but spent 100, then losses are 0
     */
    getAutoplayLosses() : number;
    
    /**
     * Indicates if an Autoplay Session is currently in progress.
     */
    isAutoplayInProgress() : boolean;

    /**
     * Updates the value of winnings for an Autoplay Session.
     */
    addToAutoplayWinnings(amountWon : number) : void;
    
    /**
     * Cancels any autoplay session that is in progress. This should be invoked if the
     * player selects to cancel autoplay mode.
     */
    cancelAutoplay() : void;

    /**
     * Sets the maximum amount that the player may win in a single Autoplay Session.
     */
    setAutoplayMaxWinnings(value : number);

    /**
     * Returns the maximum amount that the player may win in an Autoplay Session.
     */
    getAutoplayMaxWinnings() : number;

    /**
     * Sets the maximum amount that the player may lose in an Autoplay Session.
     */
    setAutoplayMaxLosses(value : number);
    
    /**
     * Returns the maximum amount that the player may lose in an Autoplay Session.
     */
    getAutoplayMaxLosses() : number;

    /**
     * Returns the auto-matic loss limit for Autoplay. This is for use in the Autoplay
     * Settings Menu, and must be re-calculated whenever other Autoplay settings (eg:
     * total number of autoplay games) are changed. Autoplay Loss Limit is the default
     * Loss Limit value to use, unless the player explicitly overrides it: it is guaranteed
     * to be a value that the player can currently afford.
     */
    getAutoplayAutoLossLimit() : number;
    
    /**
     * Returns data about the most recently completed Autoplay Session. This will only
     * return a data object when no game is in progress, and an autoplay session has
     * just completed (it exists to cache this information, for the benefit of post game
     * notifications). So, if this value is non-null when a game completes, it can be
     * taken as an indication that the game just completed was the last in an Autoplay
     * Session.
     */
    getAutoplaySessionData () : AutoplaySessionData | null;

    /**
     * Checks if a FreeGames session is currently in progress.
     */
    getFreeGamesInProgress() : boolean;

    /**
     * Returns the current number of FreeGames remaining (as should be shown to the player).
     * Guarantees to never return a value below 0.
     */
    getFreeGamesNumRemaining() : number;

    /**
     * Returns the current display winnings for any FreeGames that is in progress. (If not
     * FreeGames Session is in progress, this will be a value of 0).
     */
    getFreeGamesWinnings() : number;

    /**
     * Returns data about the most recently completed FreeGames Session. This will only
     * return a data object when no game is in progress, and a FreeGames Session has just
     * completed (it exists to cache this ninformation, for the benefit of post game
     * notifications). So, if this value is non-null when a game completes, it can be taken
     * as an indication that the game just completed was the last in a FreeGames session.
     */
    getFreeGamesSessionData() : FreeGamesSessionData | null;
    
    /**
     * Returns the currently active Currency Format object.
     */
    getCurrency() : CurrencyFormat;
    
    /**
     * Formats a currency value (an integer, in points) into a Currency String.
     * This uses the currently active CurrencyFormat to render the string. It
     * will also drop the Currency symbol used when the game is in freeplay mode.
     */
    formatCurrency(value : number) : string;
    
    /**
     * Formats a points value (an integer, in points!) into a Points String.
     * NOTE: This is place-hold functionality. I anticipate being requested at some
     * point to change the way that points values (ie: any winning amounts not rendered
     * as currency) are formatted (for different locales / licensees). I am not 100% sure
     * if the actual formatting should be specific in CurrencyFormat object though.
     */
    formatPoints(value : number) : string;

    /**
     * Formats a numerical value, into a percentage string. This is useful for when we need to render
     * Return to Player (RTP) information on-screen, and we want to pick a locale specific way of doing
     * it.
     * @param value
     * The actual percentage value. This should be in a real percentage range, eg : "90.5%" will be passed
     * in as a value of 90.5
     * @return
     * The percentage value, formatted to locale specific requirements.
     */
    formatPercentage(value : number) : string;
    
    /**
     * Checks if a new game can be afforded (using any bet settings).
     */
    isNewGameAffordable() : boolean;

    /**
     * Returns the current bet settings as an object.
     */
    getCurrentBetSettings() : SlotBetSettings;

    /**
     * Checks if a given value of totalBet is valid.
     * @param totalBet 
     */
    isTotalBetValid(totalBet:number) : boolean;

    // TODO: Maybe we need a separate type, where CurrencyType is not necessarily included
    /**
     * Returns an ordered list of all possible Bet Settings. The list will be ordered by "priority"
     * of Bet Setting. CurrencyType will not be set (this covers only basic bet settings)
     * @return {SlotBetSettings[]}
     */
    getAllPossibleBetSettings() : SlotBetSettings[];

    /**
     * Decomposes a Total Bet value into a list of possible Bet Settings which would generate it.
     * The list will be ordered in what is normally considered "priority", but this doesn't mean
     * much (if a Maths Engine has multiple ways it can implement a single value of Total Bet, there
     * is no guarantee that the "priority" order will match order of RTP). If the Total Bet cannot
     * be decomposed into any values of bet settings, then the list will be empty.
     * @param totalBet
     * @return
     */
    getAllBetSettingsMatchingTotalBet(totalBet:number) : SlotBetSettings[];

    /**
     * Calculates a list of all possible values of Total Bet. Duplicates are removed, and the list
     * will be ordered.
     */
    getAllPossibleTotalBetValues() : number[];

    /**
     * Sets current bet settings (excluding CurrencyType) based on a bet settings object.
     * At the moment, the method performs no validation of the value passed in (this may
     * change in a future revision).
     * @param betSettings 
     */
    setBetSettings(betSettings : SlotBetSettings) : void;

    /**
     * Returns the current settings for Total Bet (in points)
     */
    getTotalBet() : number;
    
    /**
     * Returns the minimum allowed value for Total Bet (in points)
     */
    getTotalBetMin() : number;
    
    /**
     * Returns the maxmimum allowed value for Total Bet (in points)
     */
    getTotalBetMax() : number;
    
    /**
     * Selects the lowest possible bet settings.
     */
    setTotalBetToMin() : void;
    
    /**
     * Selects the highest possible bet settings.
     */
    setTotalBetToMax() : void;

    /**
     * Returns the number of lines currently active in the game.
     */
    getNumLines() : number;
    
    /**
     * Returns the minimum number of lines available in the game.
     */
    getNumLinesMin() : number;

    /**
     * Returns the maximum number of lines available in the game.
     */
    getNumLinesMax() : number;
    
    /**
     * Cycles the value of Number of Active Winlines, through all available options. No
     * affordability checks are made. Defaults to increasing Bet per Line, unless optional
     * parameter increase is explicitly set to false. If BetConfig.enableBetPerHand is true,
     * then this method will call through to cycleBetPerHand instead.
     * @param increase
     * Indicates if Num Lines should be cycled upwards (when true), or downwards (when false).
     * Defaults to true (upwards).
     */
    cycleNumLines(increase?:boolean=true);


    /**
     * Returns the current value of Stake per Line ( in points ).
     */
    getBetPerLine() : number;

    /**
     * Returns the minimum possible value of Bet Per Line for this game client.
     */
    getBetPerLineMin() : number;

    /**
     * Returns the maximum possible value of Bet Per Line for this game client.
     */
    getBetPerLineMax() : number;

    /**
     * Cycles the value of Bet per Line, through all available options. No affordability
     * checks are made. Defaults to increasing Bet per Line, unless optional parameter
     * increase is explicitly set to false. If BetConfig.enableBetPerHand is true, then
     * this method will call through to cycleBetPerHand instead.
     * @param {boolean} [increase=true]
     * Indicates if Num Lines should be cycled upwards (when true), or downwards (when false).
     * Defaults to true (upwards).
     */
    cycleBetPerLine(increase?:boolean=true) : void;

    /**
     * Returns the current number of active hands in the game.
     * For any game that doesn't support multiple hands, this will
     * return 1.
     */
    getNumHands() : number;
    
    /**
     * Returns the minimum number of hands available in the Game Client.
     */
    getNumHandsMin() : number;

    /**
     * Returns the maximum number of hands available in the Game Client.
     */
    getNumHandsMax() : number;

    /**
     * Cycles the value of Number of Active Hands, through all available options. No
     * affordability checks are made. Defaults to increasing the Number of Active Hands,
     * unless the optional parameter increase is explicitly set to false.
     * @param {boolean} [increase=true]
     * Indicates if Num Hands should be cycled upwards (when true), or downwards (when false).
     * Defaults to true (upwards).
     */
    cycleNumHands(increase) : void;

    /**
     * Returns the current value of Bet per Hand. On any game where there is
     * only 1 active hand, this will return the same value as "totalBet".
     */
    getBetPerHand() : number;

    /**
     * Returns the minimum available value of Bet per Hand for the current game client.
     */
    getBetPerHandMin() : number;

    /**
     * Returns the maximum available value of Bet per Hand for the current game client.
     */
    getBetPerHandMax() : number;

    /**
     * Cycles the value of Bet per Hand, through all available options. No affordability
     * checks are made. Defaults to increasing Bet per Hand, unless optional parameter
     * increase is explicitly set to false. If BetConfig.enableBetPerHand setting is false,
     * then this function will return with no side effect.
     * @param {boolean} [increase=true]
     * Indicates if the value should be cycle up, or down.
     */
    cycleBetPerHand(increase) : void;

    /**
     * Returns the current value of Special Wager Multiplier - a bet setting used by certain
     * game mathematics, allowing the player to unlock hidden features, in exchange for
     * multiplying total bet by a certain amount.
     */
    getSpecialWagerMultiplier() : number;

    /**
     * Returns the minimum possible value of Special Wager Multiplier.
     */
    getSpecialWagerMultiplierMin() : number;

    /**
     * Returns the maximum possible value of Special Wager Multiplier.
     */
    getSpecialWagerMultiplierMax() : number;

    /**
     * Returns the current setting for Bet Multiplier. If the BetMultiplier setting is
     * disabled, then this method will always return 1.
     */
    getBetMultiplier() : number;

    /**
     * Returns the minimum available value of Bet Multiplier for the current game client.
     */
    getBetMultiplierMin() : number;

    /**
     * Returns the maximum available value of Bet Multiplier for the current game client.
     */
    getBetMultiplierMax() : number;

    /**
     * Sets the active CurrencyType to Credit.
     */
    setCurrencyTypeToCredit();

    /**
     * Sets the active CurrencyType to Superbet.
     */
    setCurrencyTypeToSuperbet();

    /**
     * Sets the active CurrencyType to Winnings.
     */
    setCurrencyTypeToWinnings();

    /**
     * Sets the active CurrencyType to PromoGame.
     */
    setCurrencyTypeToPromoGame();

    /**
     * Sets the Bet.CurrencyType value to a specific CurrencyType, and dispatches
     * an event notification only if the new value is different to the old one.
     */
    setCurrencyTypeTo(newCurrencyType : CurrencyType);

    /**
     * Returns the active currency type.
     */
    getActiveCurrencyType() : CurrencyType;

    /**
     * Returns the theoratical maximum credit that can be transfered during the player's session.
     * For example, in italy, the player is only supposed to add 1000 euro to their session. This
     * field returns either POSITIVE_INFINITY if no limit exists, or the actual value of the limit
     * (if we know it).
     */
    getSessionMaxTransfer() : number;

    /**
     * Logs the start of a new SubBet. This is the mechanism that must be used
     * by GameController when a new SubBet is entered. Internally, this method automatically
     * keeps track of what kind of subbet it will be.
     */
    logSubBetStarted() : void;

    /**
     * Reverts the last call to "logSubBetStarted". Call when the first service
     * in starting a new SubBet has failed (in a way that is recoverable).
     */
    revertSubBetStarted () : void;

    /**
     * Processes Download Statistics data, returned by the server.
     * @param msg
     */
    processDownloadStatisticsMsg(msg : DownloadStatisticsData) : void;

    /**
     * Processes a Game Result Packet, treating as a restoration message.
     */
    processGameRestorationMsg(msg : GameResultPacket<GamePhaseResult>) : void;

    processSessionInitMsg(msg : Gen2SessionInitReply) : void;

    processAddCreditMsg(msg : Gen2AddCreditReply) : void;
    
    processGameResultMsg(msg : GameResultPacket<?>) : void;

    /**
     * Logs the end of a game. This is the mechanism that must be used by
     * GameController when the current game has finished.
     * @param msg
     * The Game Complete data packet.
     */
    processGameCompleteMsg(msg : Gen2GameCompleteReply) : void

    processSessionClosedMsg(msg : Gen2SessionEndReply) : void;
}

// TODO: These are the new, agnostic concepts for game state changes..
interface NewSessionData
{

}

interface FreeGamesSessionData
{
    /**
     * Total number of FreeGames played in the FreeGames session.
     */
    numGamesPlayed : number;

    /**
     * Total amount won in the FreeGames session.
     */
    winnings : number;
}

interface AutoplaySessionData
{
    /**
     * The total amount won.
     */
    winnings : number;

    /**
     * The total amount spent.
     */
    spent : number;

    /**
     * The final value of losses for the session.
     */
    losses : number;

    /**
     * The value of max winnings that was defined for this session.
     */
    maxWinnings : number;

    /**
     * The value of max losses that was defined for this session.
     */
    maxLosses : number;

    /**
     * Indicates if this autoplay session has "completed" successfully, eg: if all
     * games that the player asked to play got played.
     */
    completed ? : boolean;

    /**
     * Indicates if this autoplay session was automatically aborted for any reason
     * (eg: player breached loss or winnings limits)
     */
    aborted ? : boolean;

    /**
     * A list of data flags describing why autoplay may have been aborted. Its possible
     * that more than one rule was violated, so all violated rules are set here.
     */
    abortedDescription ? :
    {
        cannotAffordNewGame : boolean;
        exceededMaxLosses : boolean;
        exceededMaxWinnings : boolean;
    }

    /**
     * Indicates if this autoplay session was prematurely cancelled by the player.
     */
    cancelled ? : boolean;
}