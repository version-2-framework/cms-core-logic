/**
 * Interface 
 */
interface PlatformApi
{
    /**
     * Checks if the game is likely to be running on a desktop device.
     */
    isDesktop() : boolean;

    /**
     * Checks if the game is likely to be running on a small mobile device (eg: a phone)
     */
    isMobile() : boolean;

    /**
     * Checks if the game is likely to be running on a tablet device
     */
    isTablet() : boolean;

    /**
     * Checks if the game is likely to be running on a mobile or tablet device.
     */
    isMobileOrTablet() : boolean;
}