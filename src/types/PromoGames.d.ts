/**
 * Union type representing any possible way of awarding a PromoGames session.
 * This is the standard WMG Mechanic (certain licensee or platform integrations may involve some
 * custom mechanics or data, in which case, this will not be used)
 */
type WmgPromoGamesAward = WmgPromoGamesFixedBetLevelAward | WmgPromoGamesSelectableBetLevelAward;

/**
 * Description of a promo games session (betLevel and number of PromoGames)
 */
interface WmgPromoGameSessionDescription
{
    /**
     * The value of Total Bet selected. It's generally up to the game client to pick the appropriate
     * way of interpreting this into bet settings.
     */
    betLevel : number;

    /**
     * The number of promo games awarded for the session.
     */
    numPromoGames : number;
}

/**
 * Represents a new PromoGames session awarded with Fixed Bet Level.
 */
interface WmgPromoGamesFixedBetLevelAward extends WmgPromoGameSessionDescription
{
    type : "fixedBet";
}

/**
 * Represents a new PromoGames session awarded, with selectable Bet Level (and num games)
 */
interface WmgPromoGamesSelectableBetLevelAward
{
    type : "playerChoosesBet";

    /**
     * All of the options from which the player may select.
     */
    options : WmgPromoGameSessionDescription[];
}