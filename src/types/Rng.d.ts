//--------------------------------------------------------------------------------------------------
// Types related to Random Number Generator.
//--------------------------------------------------------------------------------------------------
/**
 * Common interface for a Random Number Generator.
 */
interface Rng
{
    /**
	 * Sets the seed to be used by the RNG.
	 * @param {number} [seed]
	 * The seed value. If '0', or a negative is passed in, the RNG will seed itself
	 * based off the current system time. Otherwise, a positivei number passed as the
	 * argument, will be used for generating the seed.
	 */
    setSeed(seed : number) : void;
    
    /**
	 * Returns a random integer, up to (but not including) the value specified
	 * in parameter range.
	 * @param range
	 * The range of the random integer to generate. The value returned will have
	 * a maximum value of range - 1. If range value passed in is <= 0, or no range
	 * value is specified, the value returned will be 0.
	 * @return
	 */
    getRandom(range : number) : number;

    /**
     * Returns a random integer, in a specified range. In this case, "min" and "max" are true
     * inclusive minimum and maxiumums.
     * @param min
     * @param max 
     */
    getRandomBetween(min : number, max : number) : number;

    /**
     * Returns a random floating point number, between 2 given values. In this case, "min" and "max"
     * are true inclusive minimum and maxiumums.
     * @param min 
     * @param max 
     */
    getRandomFloatBetween(min:number, max:number) : number;
	
	/**
	 * Get a random boolean. This is a simple shortcut for 50 / 50 chance.
	 */
	getRandomBoolean() : boolean;

    /**
     * Generates a random chance value. EG: getRandomChance(1,100) means "1 in 100"
     * @param a
     * @param b 
     */
    getRandomChance(a:number, b:number) : boolean;

    
}

/**
 * Defines the chance of selecting a specific value: usually used by Outcome Selector implementations.
 */
interface Outcome
{
    /**
     * The value that should be selected for this outcome.
     */
    value : *;

    /**
     * The chance of selecting this outcome.
     */
    chance : number;
}

/**
 * A concrete instance of something which can select an outcome of some kind.
 */
interface OutcomeSelector<R>
{
    /**
     * Selects an outcome.
     * @param rng
     * The rng that the Outcome Selector should use.
     * @return
     * The selected outcome.
     */
    selectOutcome(rng : Rng) : R;
}