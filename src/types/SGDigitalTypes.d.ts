// The "balances map" is my name for a term that is... not really explained in the SG Digital
// documentation (that i have seen so far). The JsDoc doesn't explain what data i expect in
// the object, other than CASH. The PDF adds a "BONUS" field. I am still vague about what each
// one means, or if this applies at all to our game.
interface SgdBalancesMap
{
    CASH : { amount : number },
    BONUS : { amount : number }
}

type SgdEnvironment = "dev" | "stage" | "production";

type SgdPlayMode = "real" | "demo";

interface SgdOgsParams
{
    /**
     * This is SG Digital's own identifier for our game client. The actual value appears to be
     * numerical (for example: 2370001 seems to be the initial ogsgameid for Fowl Play Centurion).
     * The field is specified as a string, as I don't have any explicit guarantee that its actually
     * a number.
     */
    ogsgameid : string;

    /**
     * I assume this is a string - i haven't found anything in SG Digital docs yet to say it is
     */
    currency : string;

    /**
     * Not yet clear what exactly "gameId" means in this context. Again, i assume this is a string
     * (u haven't yet found any documentation)
     */
    gameid : string;

    /**
     * The language in use. Not yet clear if this is a string, or what the values are likely to be.
     * On their testing page, i got the value of "en_us".
     */
    lang : string;

    /**
     * Seems to denote basic mode of play "real" (realplay) / demo (eg: freeplay)
     */
    mode : SgdPlayMode;

    /**
     * Operator id appears to be numerical (I have seen sample values of 5)
     */
    operatorid : any;

    /**
     * I assume this is like our own session id.
     */
    sessionid : string;

    /**
     * I think this is the correct interpretation of these values.
     */
    device  : "mobile" | "desktop";

    /**
     * Game Lobby url. This parameter seems to be optional (operators will not always send it).
     */
    lobbyurl ? : string;

    /**
     * Deposit url. The parameter seems to be optional (operators will not always send it).
     * NOTE: I couldn't actually find proper documentation on this parameter as a member of OGS
     * params. Its mentioned in the GCM Checklist, but they list its value as "depositurl" (all
     * lower case). However, they also list lobbyUrl as "lobbyurl" (all lower case), and this is
     * definitely not the value that the GCM seems to give to the game client: i assume that the
     * checklist is wrong, therefore. (This seemed correct as of 10.07.2020)
     */
    depositurl ? : string;
}

/**
 * Configuration options which the game is allowed to specify to GCM. GCM requests an instance
 * of this object from us, via a call to GameWrapper.getConfig().
 */
interface SgdGcmGameConfig
{
    gameName : string;

    /**
     * Indicates if the game should use its own loading screen, or fall back to using the Common
     * UI loading screen. true === "use our own loading screen", false === "use the SG Digital
     * Common UI loading screen".
     */
    gameLoadingScreen :  boolean;

    /**
     * cui === "Common UI". This appears to suggest which side that certain components of the
     * common ui appear on. Through testing with their GUI, I haven't been able to find any instance
     * of their desktop GUI which responds to this call. The mobile GUI seems to partly reposition
     * some of its top-bar drop-down according to this value.
     */
    cuiPosition : "left" | "right" | "centre";
}

interface SgdGcmGameWrapper
{
    /**
     * Called by GCM, when GCM is ready.
     * @param gcm
     */
    gcmReady (gcm : SgdGcmCoreApi) : void;

    /**
     * Called by GCM, passing along main game parameters. It is at this point, that we should
     * begin the loading process.
     * @public
     * @param {SgdOgsParams} ogsParams
    */
    configReady (ogsParams: SgdOgsParams) : void;

    /**
     * The player may add credit to their game session, using SGDigital's wrapping gui. This Add Credit
     * operation is a call which goes straight to their back-end (nothing to do with ours). The updated
     * balance will be available to our back-end on the next call that it makes to their back-end.
     * 
     * When this happens, their api will call GameWrapper.balanceHasChanged, to inform our game client
     * that the player's session balance has been updated.
     * 
     * According to John Adamu
     * - this can only be called when no game is in progress (eg: between "gameAnimationStart" &
     * "gameAnimationComplete", it's disabled).
     * - he did mention that the player can also invoke a similar action from an external page, but
     *   it seems that this information will not be forwarded back to our game client (because it is
     *   in a completely separate context).
     * 
     * Note, that "no game in progress" is simply the SGDigital / GCM way of understanding no game in
     * progress: it can stil happen at any arbitrary time / state of our game client (in a menu, when
     * a notification is being shown, in FreeRounds idle, etc).
     * 
     * Even if this call is made to our game client, our back-end should become aware of the updated
     * balance on the next operation it performs in conjunction with the SGDigital back-end. 
     * 
     * The balances map passed in has 2 fields, and apparently we must add them together, in order to
     * obtain the actual new value of balance.
     * @param balanceData
     */
    balancesHasChanged (balanceData:SgdBalancesMap) : void;
    
    /**
     * The external game loading step is finished: it now notifies us, through this method,
     * that we need to show the game.
     */
    gameRevealed () : void;

    /**
     * Action invoked by SGDigital's middle-ware, whenever its popup notification (referred to in their
     * code as "error notification", although it's not exclusively used for errors) is dismissed. The
     * sole parameter passed to this method, is the numerical index of any option that was selected for
     * a multi-choice dialog (allowing us to understand which choice the player selected when the popup
     * was dimissed).
     * @param errorParamIndex
     */
    resume (errorParamIndex:number) : void;

    /**
     * Reload the whole game window. As for "redirect", it isn't clear why this method needs to exist.
     * SGDigital's documentation and examples simply suggest that we need to call window.reload (the
     * standard html browser method that is available) when this method is called. Why their code base
     * cannot call this itself (or why they think it's useful to us to know its happening) isn't ever
     * explained in their documentation.
     */
    reload () : void;

    /**
     * A request from SGDigital's middle-ware layer, that we should redirect to a url. It isn't really
     * clear why this method needs to exist: they are telling us to redirect, and their documents suggest
     * that we simply need to execute the redirection (why they cannot do this themselves, or why they
     * think that informing us about the redirect directly is helpful, isn't ever clarified).
     * @param targetUrl
     * The new url that we are being told to redirect to.
     */
    redirect(targetUrl:string) : void;
            
    /**
     * Invoked by GCM when one of the standard "SGD Game Options" has its value changed via their external
     * menus. We have to register options manually with their Game Client Middleware. It isn't explicitly
     * clear from their documentation whether this method is ONLY expected to be called when a registered
     * option is changed (as opposed to an option that we didn't register), but presumably this is the case
     * (eg: presumably an un-registered option simply isn't available to the player to change).
     * @param optionType
     * The option type that has had its value changed.
     * @param optionValue
     * Indicates if this option is now enabled or disabled.
     */
    optionHasChanged(optionType:SgdGcmOptionType, newValue:boolean) : void;

    /**
     * Gcm can request game configuration options from the GameWrapper.
     */
    getConfig ? : (ogsParams:SgdOgsParams) => SgdGcmGameConfig;

    /**
     * Called by GCM, whenever there are.... some FreeRounds info to update (honestly I am vague
     * on this. The main documentation doesn't even mention that this method needs to be added, I
     * am only aware of it from reading the spreadsheet checklist, and that doesn't exactly go into
     * detail about precisely what purpose this method serves).
     * @param freeRoundsInfo 
     */
    handleFreeRoundsUpdate(freeRoundsInfo : SgdFreeRoundCampaign) : void;
}

/**
 * API to the GCM itself. This is the mechanism we are required to use, to pass information
 * back to GCM and its own gui layer.
 */
interface SgdGcmCoreApi
{
    /**
     * Initializes the Gcm instance.
     * @param gameWrapper
     * @param gcmUrl 
     * @param gameUrl 
     */
    init (gameWrapper:SgdGcmGameWrapper, gcmUrl:string, gameUrl:string) : void;

    /**
     * 
     * @param percentage
     * A value to indicate percentage - the GCM documentation indicates that this should be in the
     * range of 0 to 100, and apparently must be an integer (not a floating point number) - although
     * this last detail seems a little odd.
     */
    loadProgressUpdate(percentage : number) : void;

    // TODO: Find out what this is supposed to represent
    /**
     * Registers a toggle-able boolean option value. These are supposed to represent buttons which
     * may appear along the top bar of  the CommonUI (althgouh not all CommonUI implementations will
     * guarantee to actually implement each possible button).
     * @param option
     * The option to register.
     * @return
     * Current state of that option.
     */
    regOption(option : SgdGcmOptionType) : boolean;

    /**
     * Updates the balance value shown on the GCM Gui.
     * @param balance
     * The new value of Wallet. GCM's javascript documentation has nothing to say about what range
     * this value should be in: it only says that it should be a number. Presumably, where we use
     * points (with an example value of 100), they use currency (with an example value of 1.00)
     */
    balancesUpdate(balance : SgdBalancesMap) : void;

    /**
     * Updates the Total Stake value value shown on the GCM Gui.
     * @param stake
     * The new value of Total Stake. GCM's javascript documentation has nothing to say about what
     * range this value should be in: it only says that it should be a number. Presumably, where we
     * use points (with an example value of 100), they use currency (with an example value of 1.00)
     */
    stakeUpdate(totalStake : number) : void;

    /**
     * Updates the "curreny winnings" value shown on the GCM Gui.
     * @param amountPaid 
     * The new value of current winnings. GCM's javascript documentation has nothing to say about
     * what range this value should be in: it only says that it should be a number. Presumably, where
     * we use points (with an example value of 100), they use currency (with an example value of 1.00).
     */
    paidUpdate(amountPaid : number) : void;

    /**
     * The GameWrapper should call this method, once the game is ready (all assets are loaded, all launch
     * configuration has been completed). We then need to wait for a notification from GCM (GCM will call
     * GameWrapper.gameRevealetd())
     */
    gameReady() : void;

    /**
     * To be called by the Game, when a new game starts.
     * @todo Confirm in these docs, exactly when we are supposed to call it. EG: before the game starts,
     * after the stake is placed... its not clear to me yet, from their docs, which of it this is
     */
    gameAnimationStart() : void;

    /**
     * Informs the GCM that the game simulation has been completed. This should be called after a Game
     * Complete call. The GCM will potentially show some post-game notifications here: for this reason,
     * a callback parameter exists (the callback will be invoked when the GCM layer says its ok for our
     * game to resume).
     * @param resumeGameCallback
     * A callback which will be called by GCM when its safe to resume the game.
     */
    gameAnimationComplete(resumeGameCallback:()=>void) : void

    /**
     * GCM's handle error method displays a popup to the player - although general notifications are valid use
     * cases (suggesting that the method name isn't that great, or that the use of it evolved over time).
     * @param errorCategory 
     * @param errorSeverity
     */
    handleError(
        errorCategory:SgdErrorCategory,
        errorSeverity:SgdErrorSeverity,
        errorCode:string,
        errorMessage:string,
        errorParams?:SgdErrorParams) : void;


    /**
     * Informs GCM that the status of one of its "options" has been toggled. This can be called directly
     * by the GameClient. For example: we may present an AutoplayMenu button in the GameClient GUI (this
     * button is also available in the CommonUI). If we open or close the AutoplayMenu through the Game
     * GUI, we can call this method to inform GCM of the status change.
     * @param optionType 
     * @param changedBy
     * @param newValue 
     */
    optionHasChanged(
        optionType:SgdGcmOptionType,
        changedBy: "COMMONUI" | "GAME",
        newValue:boolean) : void;


    /**
     * 
     * "The game should call in this function after building a promoIngo object containing the available
     * promotions data". In our case, i understand this to mean: we get the promoGames data from our own
     * backend, we have to tell GCM what we know.
     * @param promoInfo 
     */
    setPromoInfo(promoInfo : SgdPromoInfo) : void;

    getPromoInfo() : SgdPromoInfo; // thats the wrong return type... or, is it ??

    /**
     * Once GCM informs the game client that a FreeRounds selection has been made, we need to call this
     * method (apparently). Not sure why - the original jsdoc supplied by SGDigital implies that the
     * method only really exists for the use of SGD layer libraries (there is no hint that the game client
     * should ever call this). But, it seems to work..
     */
    resume() : void;

    /**
     * GCM's currency formatting method. Takes a number, returns a printable string.
     * @param amount
     * The amount to render as a string. GCM api documentation has nothing whatsoever to say about what
     * range this value should be in, but from other docs it seems probable that this expects a value
     * of 1.00, where we would use points (eg: a value of 100).
     */
    formatAmount(amount : number) : string;
}

interface SgdPromoInfo
{
    PROMOTIONS : SgdFreeRoundOffer
}

interface SgdFreeRoundOffer
{
    /**
     * This .. "might".. refer to any normal Credit game (the documentation doesn't actually
     * explain it, and the json schema... doesn't actually give any information about what
     * each field means)
     */
    GAMEPLAY : "pending" | "complete";

    FREEROUNDS : SgdFreeRoundCampaign[];
};

// TODO: Look for a better name
interface SgdFreeRoundCampaign
{
    CAMPAIGNID : string;

    CAMPAIGNVALUE : number;

    CAMPAIGNVALUE_FMT : string; // I think its a string - their schema is awful

    ACTIVATIONID : string;

    ENDDATE : string;

    TOTALWIN : number;

    TOTALWIN_FMT : string;

    REJECTABLE : boolean;

    // This obviosuly MUST have more values than "inprogress" to be of any used - but their
    // schema simply doesn't list any. It's a really shit excuse for a schema.
    // "notstarted" is a value that got passed by GCM when it called this.
    // Additionally, "completed" seems to be available here
    STATUS : "notstarted" | "inprogress" | "completed";

    /**
     * Contains 1 or more FreeRound descriptions. When returned in "handleFreeRoundsUpdate",
     * and the campaign is the selected campaign, this will have a length of 1.
     */
    OPTIONS : SgdFreeRoundOption[];
}

interface SgdFreeRoundOption
{
    BETLEVEL : number;

    TOTALROUNDS : number;

    REMAININGROUNDS : number;

    // Definitely don't know what this represents.
    FEATURE : string;
}

type SgdGcmOptionType = "MUTE" | "TURBO" | "ABOUT" | "HELP" | "PAYTABLE" | "GAME_PREFERENCES";
// In "GameInfo" i found a whole bunch of extra option types that i had not see previous documentation on

// The other options are
// - GAME_FEATURE
// - AMBIENCE_SOUND
// - QUALITY
// - CLOTH_COLOR

interface SgdGcmOptionStatus
{
    enabled : boolean;

    value : boolean;
}

interface SgdGcmOptionMap
{
    MUTE : SgdGcmOptionStatus;
    TURBO : SgdGcmOptionStatus;
    ABOUT : SgdGcmOptionStatus;
    HELP : SgdGcmOptionStatus;
    PAYTABLE : SgdGcmOptionStatus;
    GAME_PREFERENCES: SgdGcmOptionStatus;
}

/**
 * Error Categories which the Game Client can specify to GCM, when calling "handleError" (which shows
 * a dialog popup of some kind). As can be seen, some of these categories are not actually Errors - 
 * presumably, Gcm's "handleError" method was originally intended purely for showing error messages,
 * and then evolved into a general purpose mechanism for showing notifications. They kept the (rather
 * confusing) legacy name for the method ("handleError"), and kept the term "errorCategory" for the
 * parameter.
 */
type SgdErrorCategory = "CRITICAL" | "INSUFFICIENT_FUNDS" | "LOGIN_ERROR" |
    "RECOVERABLE_ERROR" | "NON_RECOVERABLE_ERROR" | "CONNECTION_ERROR" |
    "MULTI_CHOICE_DIALOG" | "OTHER_GAME_IN_PROGRESS" | "REALITY_CHECK";

type SgdErrorSeverity = "WARNING" | "INFO" | "ERROR";

type SgdErrorParams = SgdMultiChoiceDialogErrorParams;

/**
 * Parameters for the Mulith Choice Dialog Error popup.
 */
interface SgdMultiChoiceDialogErrorParams
{
    /**
     * A series of string values to be shown on option buttons.
     */
    options : string[];
}

type SgdDialogViewModel = SgdMultiChoiceDialogViewModel;

interface SgdAbstractDialogViewModel
{
    message : TextConfig;
}

interface SgdMultiChoiceDialogViewModel extends SgdAbstractDialogViewModel
{
    type : "multiChoice",

    /**
     * A series of text button configs to show at the bottom of the dialog.
     */
    options : TextButtonConfig[];
}

/**
 * Custom LMS (Licensee Management Server) data returned for SGDigital: this is returned
 * only on the first Game Phase Results Reply of a new bet (either CREDIT or PROMO_GAME).
 * For SGDigital, this may only be returned sometimes, and if it does, it may contain some
 * updated promo info data.
 */
/*
interface SgdLmsCustomData
{
    promoInfo ? : SgdFreeRoundOffer;
}
*/