type ServerToClientNotification =
    ServerToClientSimpleNotification;

interface ServerToClientSimpleNotification
{
    type : "notification";

    title : string;

    body : string;

    actionOnDismiss : "closePopup" | "closeClient";
}