// Aaaaaaarggggggggg.... ok, this is plain wrong !
// The fields in question are never arrays (it seems), only ever strings -
// which is part of the reason to use defensive extraction methods. Maybe a
// json reviver method would be more appropriate - though you'd still need
// one in all contexts, of course.

/**
 * This file defines common Server Results Models - as we expect them to be between
 * both the Gen1 model, and the Gen2 model.
 */


 //--------------------------------------------------------------------------------------------------
// Server Request Model
//--------------------------------------------------------------------------------------------------
// Common definitions for requests sent to the Game Engine Server. The request definitions within
// this file relate solely to common & session related operations (anything that is game maths
// specific, doesn't belong here).
//--------------------------------------------------------------------------------------------------
/**
 * Common base of all Gen1 Server Requests.
 */
interface Gen1ServiceRequest
{
    /**
     * Message Type of the request. Because all requests are sent to a single end-point
     * on the server, the Message Type value is critical for the server to correctly
     * identify the nature of the request it has received.
     */
    MTP : string;

    /**
     * Message Id
     */
    MID : string;

    /**
     * Wmg Session Id.
     */
    WID : string;
}

/**
 * Common base interface for every message received from the Game Engine Server.
 */
interface Gen1ServiceReply
{
    /**
     * Message ID (like a numberplate for this message operation)
     */
    MID : string;

    /**
     * The Message Type (the id of the Service being executed)
     */
    MTP : string;

    ERR ? : string;

    /**
     * If there is some kind of server error, we expect this field to exist (NOTE: its not 100% clear
     * if the Gen1 server always returns this data).
     */
    ERROR_KEY_CODE ? : string;
}


//--------------------------------------------------------------------------------------------------
// Common Operations
//--------------------------------------------------------------------------------------------------
/**
 * Download statistics request. The client is supposed to send a bunch of useful
 * information, describing the system the player is on. This message was designed
 * by WMG Italy, and unfortunately, most of the information requested simply cannot
 * be provided by code running in a browser. Therefore, we need to generate dummy
 * values (in order for the server to not reject the request operation).
 */
interface Gen1DownloadStatisticsRequest extends Gen1ServiceRequest
{
    /**
     * @inheritDoc
     */
    MTP : "WMG_DOWNLOAD_STATISTICS";

    /**
     * Wmg Game Id
     */
    WGD : string;

    /**
     * Wmg Game Version
     */
    WGV : string;

    /**
     * Browser name
     */
    BRN : string;

    /**
     * Browser version
     */
    BRV : string;

    /**
     * IP Address
     */
    IPA : string;

    /**
     * IP Version
     */
    IPV : string;

    /**
     * Operating System.
     */
    OS : string;

    /**
     * Operating System version.
     */
    OSV : string;

    /**
     * port used (we cannot fetch this at all)
     */
    PRT: number;

    /**
     * Day of request being generated. We normally send UTC day, zero padded to 2 chars
     */
    DAY : string;

    /**
     * Month : we normally send UTC month, zero padded to 2 chars
     */
    MTH : string;

    /**
     * Year : we normally send UTC full year
     */
    YEAR : string;

    /**
     * Hour : we normally send UTC hour, zero padded to 2 chars
     */
    HOUR : string;

    /**
     * Minute : we normally send UTC minute, zero padded to 2 chars
     */
    MNT : string;

    /**
     * Second : we normally send UTC second, zero padded t 2 chars
     */
    SEC : string;
}

/**
 * The reply returned by the server for the Download Statistics operation.
 */
interface Gen1DownloadStatisticsReply
{
    /**
     * @inheritDoc
     */
    MTP : "WMG_DOWNLOAD_STATISTICS_REPLY";

    /**
     * Player's nickname.
     * NOTE: we have never yet used this within the Game Clients (and probably never will)
     */
    NNM : string;
}

/**
 * The request sent by the client, to check if a session is open (and if we should restore
 * to a specific game state).
 */
interface Gen1CheckPendingRequest extends Gen1ServiceRequest
{
    /**
     * @inheritDoc
     */
    MTP : "WMG_CHECK_PENDING_REQUEST";

    /**
     * Wmg Game Id
     */
    WGD : string;

    /**
     * Wmg Game Version
     */
    WGV : string;

    /**
     * Session Id.
     */
    SID : string;

    /**
     * Is this a Freeplay Session ?
     */
    FPL : false;
}

/**
 * Reply returned by the server to the Check Pending operation, when there is no
 * game session to restore (if there is a game to restore, the Gen1 Game Engine Server
 * returns the game message to restore to. Unfortunately, the exact list of possible
 * game messages returned, varies between the different Gen 1 maths engines).
 */
interface Gen1CheckPendingReply extends Gen1ServiceReply
{
    /**
     * @inheritDoc
     */
    MTP : "WMG_CHECK_PENDING_REPLY";

    WGD : string;
}

/**
 * Request sent by the server to check the player's current balance.
 */
interface Gen1CheckBalanceRequest extends Gen1ServiceRequest
{
    /**
     * @inheritDoc
     */
    MTP : "WMG_BALANCE_REQUEST";

    /**
     * Is this a Freeplay Session ?
     */
    FPL : boolean;

    /**
     * Active currency key.
     */
    CUR : String;

    /**
     * Date at the moment that the Game Client was launched.
     */
    DTE : String;

    /**
     * Indicates if the player has selected FunBonus session mode.
     */
    SBF ? : boolean;
}

/**
 * Reply to the Check Balance operation.
 */
interface Gen1CheckBalanceReply extends Gen1ServiceReply
{
    /**
     * @inheritDoc
     */
    MTP : "WMG_BALANCE_REPLY";

    /**
     * Wmg Session Id.
     */
    WID : string;

    /**
     * Id of the game session.
     */
    SID : string;

    /**
     * Session Balance (an integer in points).
     */
    SBL : number;

    /**
     * Transfer Balance.
     */
    TBL : number;

    /**
     * Indicates if this is a Freeplay session.
     */
    FPL : boolean;
}

interface Gen1SessionInitRequest extends Gen1ServiceRequest
{
    /**
     * @inheritDoc
     */
    MTP : "WMG400";

    /**
     * Indicates if this is a Freeplay session.
     */
    FPL : boolean;


    WGD : String;


    WGV : String;
    
    /**
     * The amount of currency (in points) that the player has requested
     * to add to their new game session.
     */
    RAM : number;
}

/**
 * Reply to the Session Init operation.
 */
interface Gen1SessionInitReply extends Gen1ServiceReply
{
    /**
     * @inheritDoc
     */
    MTP : "WMG400R";

    /**
     * Id of the Game Session.
     */
    SID : string;

    /**
     * Id of the Game Ticket.
     */
    PID : string;

    /**
     * Credit balance for the new Session (an integer, in points).
     */
    SBL : number;

    /**
     * Indicates if this is a Freeplay session.
     */
    FPL : boolean;

    /**
     * The number of minutes that the Game Client should use for Session Inactivity timer.
     */
    TMT : number;

    /**
     * Indicates if a Session is active or not.
     * @todo This field is not clear to me, from the available documentation.
     */
    ACS : boolean;

    /**
     * Indicates if a Ticket is active or not.
     * @todo: This field is not clear to me, from the available documentation.
     */
    ACT : boolean;
}

/**
 * Request to add credit to an open game session.
 */
interface Gen1AddCreditRequest extends Gen1ServiceRequest
{
    /**
     * @inheritDoc
     */
    MTP : "WMG420";

    /**
     * Session Id.
     */
    SID : String;

    /**
     * Ticket ID
     */
    PID : String;

    /**
     * Ticket progressive.
     */
    PPR : number;

    /**
     * Is this a Freeplay Session ?
     */
    FPL : boolean;

    /**
     * Is this a reloaded game Session ?
     */
    RLD : boolean;

    /**
     * Current value of Session.Wallet
     */
    WLT : number;

    /**
     * Amount of credit to add to the Session.
     */
    PAM : number;

    WGD : String;
    WGV : String;

    /**
     * Start date of the Game Client.
     */
    STD: String;

    /**
     * Start time of the Game Client.
     */
    STM : String;
}

/**
 * Reply to the Add Credit to Session ("Buy Ticket") Server operation.
 */
interface Gen1AddCreditReply extends Gen1ServiceReply
{
    /**
     * @inheritDoc
     */
    MTP : "WMG420R";

    /**
     * Id of the Game Session.
     */
    SID : string;

    /**
     * Id of the Game Ticket.
     */
    PID : string;

    /**
     * Progressive value of Game Ticket.
     */
    PPR : number;

    /**
     * Indicates if this is a Freeplay session.
     */
    FPL : boolean;

    /**
     * New value of Credit.
     */
    WLT : number;

    /**
     * The amount of credit added in this operation.
     */
    PAM : number;
}


/**
 * Keep Alive request
 */
interface Gen1KeepAliveRequest extends Gen1ServiceRequest
{
    /**
     * @inheritDoc
     */
    MTP : "WMG_GAME_KEEP_ALIVE_REQUEST";

    /**
     * Session id.
     */
    SID : string;

    /**
     * Ticket id.
     */
    PID : string;

    /**
     * Ticket progressive.
     */
    PPR : number;

    /**
     * Indicates if this is a freeplay session.
     */
    FPL : boolean;
}

/**
 * Reply to the Keep Session Alive operation.
 */
interface Gen1KeepAliveReply extends Gen1ServiceReply
{
    /**
     * @inheritDoc
     */
    MTP : "WMG_GAME_KEEP_ALIVE_REPLY";

    /**
     * ID of the Game Session.
     */
    SID : string;

    /**
     * ID of the Game Session Ticket.
     */
    PID : string;

    /**
     * Indicates if this is a Freeplay session.
     */
    FPL : boolean;
}

/**
 * Request to end an open game session.
 */
interface Gen1SessionEndRequest extends Gen1ServiceRequest
{
    /**
     * @inheritDoc
     */
    MTP : "WMG_SESSION_END_500_REQUEST";

    /**
     * Session Id.
     */
    SID : String;

    /**
     * Ticket ID
     */
    PID : String;

    /**
     * Ticket progressive.
     */
    PPR : number;

    /**
     * Is this a Freeplay Session ?
     */
    FPL : boolean;

    /**
     * Current credit.
     */
    WLT : number;

    /**
     * Current points.
     */
    PNT : number;

    /**
     * Current value of Total Stake
     * @todo: Confirm if we really need this (we REALLY shouldn't)
     */
    BET : number;

    /**
     * Current value of Num Lines
     * @todo: Confirm if we really need this (we REALLY shouldn't)
     */
    LNS : number;

    DAY : String;
    MTH : String;
    YEAR : String;
    HOUR : String;
    MNT : String;
    SEC : String;
}

/**
 * Reply to the End Session Server operation.
 */
interface Gen1SessionEndReply extends Gen1ServiceReply
{
    /**
     * @inheritDoc
     */
    MTP : "WMG_SESSION_END_500R";

    /**
     * Wmg Session ID.
     */
    WID : string;

    /**
     * ID of the Game Session.
     */
    SID : string;

    /**
     * Was the closed Session, a Freeplay session ?
     */
    FPL : boolean;
}

/**
 * Request to activate a fun bonus session.
 */
interface Gen1FunBonusActivationRequest extends Gen1ServiceRequest
{
    /**
     * @inheritDoc
     */
    MTP : "WMG_FUN_BONUS_ACTIVATION_REQUEST";
}

/**
 * Reply to the Fun Bonus activation request.
 */
interface Gen1FunBonusActivationReply extends Gen1ServiceReply
{
    /**
     * @inheritDoc
     */
    MTP : "WMG_FUN_BONUS_ACTIVATION_REPLY";
}

interface GameHistoryServiceRequest extends Gen1ServiceRequest
{

}




// TODO : I think GameHistory actually needs to be in "Gen1" / "Gen2" territory
interface GameHistoryServiceResponse extends Gen1ServiceReply
{

}


//--------------------------------------------------------------------------------------------------
// Slot Game operations
//--------------------------------------------------------------------------------------------------

/**
 * Common base interface for all Gen1 Service Requests which relate to playing a Game.
 */
interface Gen1GameServiceRequest extends Gen1ServiceRequest
{
    /**
     * Session Id.
     */
    SID : String;

    /**
     * Ticket ID
     */
    PID : String;

    /**
     * Ticket progressive.
     */
    PPR : number;

    /**
     * Is this a Freeplay Session ?
     */
    FPL : boolean;

    /**
     * Is this a reloaded game Session ?
     */
    RLD : boolean;
}

// TODO: Add common data here ?
interface Gen1GameServiceReply extends Gen1ServiceReply
{
    /**
     * Session Id.
     */
    SID : string;

    /**
     * Ticket ID
     */
    PID : string;

    /**
     * Ticket progressive.
     */
    PPR : number;

    /**
     * Is this a Freeplay Session ?
     */
    FPL : boolean;

    /**
     * Is this a reloaded game Session ?
     */
    RLD : boolean;

    WLT : number;

    PNT : number;
}

/**
 * Common base interface for all Gen1 Service Request for Phase Results.
 */
interface Gen1GamePhaseResultsRequest extends Gen1GameServiceRequest
{
    /**
     * Current value of Session.Wallet
     */
    WLT : number;

    /**
     * Current value of Session.Points
     */
    PNT : number;

    /**
     * Total Stake the Game is being played at.
     */
    BET : number;

    /**
     * Number of Lines that the Game is being played at.
     */
    LNS : number;

    /**
     * Number of Hands that the Game is being played at.
     */
    NHD : number;

    /**
     * The Phase of the Game.
     */
    PHS : number;
}

/**
 * Common part of a Game Phase Results reply.
 */
// TODO: Inherit from GameServiceReply ??
interface Gen1GamePhaseResultsReply extends Gen1GameServiceReply
{
    /**
     * Bet.TotalStake value in use.
     */
    BET : number;

    /**
     * Bet.NumLines value in use.
     */
    LNS : number;

    /**
     * Bet.NumHands value in use (only available on Gen 1 multihand games)
     */
    NHD ? : number;

    /**
     * Indicates if this is a Game Reload message.
     */
    RLD : boolean;
}

/**
 * Common part of a request for Spin results
 */
interface Gen1SpinPhaseResultsRequest extends Gen1GamePhaseResultsRequest
{
    /**
     * Information relating to Start Symbols. In some Gen 1 Game Models, this is a set
     * of Reel Positions. In others, its a set of Reel Symbols. This information
     * describes what was shown at the end of any previous Spin Phase.
     * 
     * This data has to be encoded as a string. It SHOULD be an array of numbers, eg:
     * [5,10,2,1,6]: instead we must send "5,10,2,1,6".
     */
    RNR : string;
}

/**
 * Common part of a Spin Result reply.
 */
interface Gen1SpinPhaseResultsReply extends Gen1GamePhaseResultsReply
{
    /**
     * Reel positions at the start of the spin. This is a string, but represents
     * a list of numbers (number[])
     */
    SNR : string; //number[];

    /**
     * Reel positions at the end of the spin.
     */
    RNR : string; //number[];

    /**
     * Indicates if a Bonus Win is triggered. For single-hand games, a single boolean value
     * is expected here. On multi-hand games, this field will contain an array of booleans
     * (each index maps to a hand, eg: [false,true,true,false] indicates bonus wins on hands
     * 2 & 3). As is common for the Gen1 Platform, this information appears to be returned
     * as a string (and not a boolean array): however, no documentation actually exists to
     * officially explain this point.
     */
    PBN : string; //boolean[];

    /**
     * The Id of each winning Winline.
     */
    WLN : string; //number[];

    /**
     * The credit win value, for each winning Winline.
     */
    WLV : string; //number[];

    /**
     * The number of winning symbols, for each winning Winline.
     */
    WSL : string; //number[];

    /**
     * The Hand Id of each winning Winline. This is only available on a multi-hand game.
     */
    HWL ? : string; //number[];
}

/**
 * Common part of a Spin Complete request.
 */
interface Gen1SpinPhaseCompleteRequest extends Gen1GameServiceRequest
{

}

/**
 * Common part of a Spin Complete reply.
 */
interface Gen1SpinPhaseCompleteReply extends Gen1ServiceReply
{

}

/**
 * Request to the server, for Single Spin results.
 */
interface Gen1SingleSpinPhaseResultsRequest extends Gen1SpinPhaseResultsRequest
{
    /**
     * @inheritDoc
     */
    MTP : "WMG_GAME_START_REQUEST";

    /**
     * @inheritDoc
     */
    PHS : 1;
}

/**
 * Response, with Single Spin Phase results.
 */
interface Gen1SingleSpinPhaseResultsReply extends Gen1SpinPhaseResultsReply
{
    /**
     * @inheritDoc
     */
    MTP : "WMG_GAME_RESULTS_REPLY";
}

/**
 * Message to the server, to inform it that Single Spin Phase results have been shown.
 */
interface Gen1SingleSpinPhaseCompleteRequest extends Gen1SpinPhaseCompleteRequest
{
    /**
     * @inheritDoc
     */
    MTP : "WMG_SPIN_PHASE_COMPLETE_REQ";
}

/**
 * Response from the server, indicating that it has accepted Single Spin phase as completed.
 */
interface Gen1SingleSpinPhaseCompleteReply extends Gen1SpinPhaseCompleteReply
{
    /**
     * @inheritDoc
     */
    MTP : "WMG_SPIN_DATA_SAVED";
}

/**
 * Request for Spin 1 Phase Results
 */
interface Gen1Spin1PhaseResultsRequest extends Gen1SpinPhaseResultsRequest
{
    /**
     * @inheritDoc
     */
    MTP : "WMG_GAME_START1_REQUEST";
}

/**
 * Response with Spin 1 Phase results.
 */
interface Gen1Spin1PhaseResultsReply extends Gen1SpinPhaseResultsReply
{
    /**
     * @inheritDoc
     */
    MTP : "WMG_SPIN1_RESULTS_REPLY";

    /**
     * The suggested autoholds pattern to apply for spin 2. This is a string, but it represents
     * an array of strings. Each value will be "H" for held, and "NH" for not held. The Gen1
     * platform has only every supported holding per reel. So, a value of "H,NH,NH,H,H" means
     * "Hold reels 1,4,5, don't hold reels 2 & 3". On multi-hand games for Gen1, the convention
     * is to send the Holds Pattern for each hand as part of this list: however, we don't ever
     * allow the player to change holds independently for each hand.
     */
    AHD : string;
}

/**
 * Message to the server, to inform it that Spin 1 Phase results have been shown.
 */
interface Gen1Spin1PhaseCompleteRequest extends Gen1SpinPhaseCompleteRequest
{
    /**
     * @inheritDoc
     */
    MTP : "WMG_SPIN1_PHASE_COMPLETE";
}

/**
 * Response from the server, indicating that it has accepted Spin 1 phase as completed.
 */
interface Gen1Spin1PhaseCompleteReply extends Gen1SpinPhaseCompleteReply
{
    /**
     * @inheritDoc
     */
    MTP : "WMG_SPIN1_DATA_SAVED";
}

/**
 * Request for Spin 2 Phase results.
 */
interface Gen1Spin2PhaseResultsRequest extends Gen1SpinPhaseResultsRequest
{
    /**
     * @inheritDoc
     */
    MTP : "WMG_GAME_START2_REQUEST";

    /**
     * The holds pattern selected by the player. It seems that this is supposed to be encoded
     * directly as a string (the Gen1 platform "doesn't support proper JSON"). It may well be
     * that we can actually send an array of strings, but there is a serious lack of proper
     * documentation for the Gen 1 platform, so we should stick with what works.
     * EG: if our real holds pattern for a hand is [true,false,true,true,false] we should send
     * "H,NH,H,H,NH"
     */
    HLD : string;

    /**
     * @inheritDoc
     */
    PHS : 2;
}

/**
 * Response from the server, containing Spin 2 Phase results.
 */
interface Gen1Spin2PhaseResultsReply extends Gen1SpinPhaseResultsReply
{
    /**
     * @inheritDoc
     */
    MTP : "WMG_SPIN2_RESULTS_REPLY";

    /**
     * The actual holds pattern applied on spin 2. Don't be fooled by the fact that the
     * acronym is "AutoHolDs" - that isn't what this is (its the actual pattern the player
     * selected and applied)
     * This is a string, but it represents an array of strings. Each value will be "H" for held,
     * and "NH" for not held. The Gen1 platform has only every supported holding per reel. So, a
     * value of "H,NH,NH,H,H" means "Hold reels 1,4,5, don't hold reels 2 & 3". On multi-hand games
     * for Gen1, the convention is to send the Holds Pattern for each hand as part of this list:
     * however, we don't ever allow the player to change holds independently for each hand.
     */
    AHD : string;
}

/**
 * Message to the server, to inform it that Spin 2 Phase results have been shown.
 */
interface Gen1Spin2PhaseCompleteRequest extends Gen1SpinPhaseCompleteRequest
{
    /**
     * @inheritDoc
     */
    MTP : "WMG_SPIN2_PHASE_COMPLETE";    
}

/**
 * Reponse from the server, indicating that is has accepted Spin 2 phase as completed.
 */
interface Gen1Spin2PhaseCompleteReply extends Gen1SpinPhaseCompleteReply
{
    /**
     * @inheritDoc
     */
    MTP : "WMG_SPIN2_DATA_SAVED";
}

//--------------------------------------------------------------------------------------------------
// Part of Bonus Messaging that is common to all games (the exacty Bonus Messaging, and its
// associated meaning, diverges between the different maths engines and game client integrations).
//--------------------------------------------------------------------------------------------------
// TODO: We might need to distinguish between "2 spin" games here ?
// TODO: for sure, these messages are NOT the same between different
// lgeacy games. We need some different implementations.
interface Gen1BonusPhaseResultsRequest extends Gen1GamePhaseResultsRequest
{
    /**
     * @inheritDoc
     */
    PHS : 3;
}

interface Gen1BonusOneSpinPhaseResultsRequest extends Gen1BonusPhaseResultsRequest
{
    /**
     * @inheritDoc
     */
    MTP : "WMG_SINGLE_SPIN_BONUS_GET_RESULTS_REQUEST";
}

interface Gen1BonusTwoSpinPhaseResultsRequest extends Gen1BonusPhaseResultsRequest
{
    /**
     * @inheritDoc
     */
    MTP : "WMG_BONUS_GET_RESULTS_REQUEST";
}

interface Gen1BonusPhaseResultsReply extends Gen1GamePhaseResultsReply
{

}

/**
 * Common part of Bonus Phase Results for a One Spin Slot game.
 */
interface Gen1BonusOneSpinPhaseResultsReply extends Gen1BonusPhaseResultsReply
{
    /**
     * @inheritDoc
     */
    MTP : "WMG_SINGLE_SPIN_BONUS_GET_RESULTS_REPLY";
}

/**
 * Common part of Bonus Phase Results for a Two Spin Slot game.
 */
interface Gen1BonusTwoSpinPhaseResultsReply extends Gen1BonusPhaseResultsReply
{
    /**
     * @inheritDoc
     */
    MTP : "WMG_BONUS_GET_RESULTS_REPLY";
}

/**
 * Common part of a Bonus Single Selection request. This is a request to the server, to make
 * a single selection of some kind in the Bonus Phase.
 */
interface Gen1BonusSingleSelectionRequest extends Gen1GameServiceRequest
{
    /**
     * The numerical id of the game phase.
     */
    PHS : 3;
}

/**
 * Common part of a Bonus Single Selection reply.
 */
interface Gen1BonusSingleSelectionReply extends Gen1ServiceReply
{

}

/**
 * Common base for Bonus Multi Selection Requests. This is a request to the server, to make
 * a series of 1 or more selections in the Bonus Phase.
 */
interface Gen1BonusMultiSelectionRequest extends Gen1GameServiceRequest
{
    /**
     * The numerical id of the game phase.
     */
    "PHS" : number;
}

/**
 * Common part of a Bonus Multi Selection reply.
 */
interface Gen1BonusMultiSelectionReply extends Gen1ServiceReply
{

}

/**
 * Bonus single selection request for a One Spin Slot game. This is a request to the server, to make
 * a series of 1 or more selections in the Bonus Phase.
 */
interface Gen1BonusOneSpinSingleSelectionRequest extends Gen1BonusSingleSelectionRequest
{
    /**
     * @inheritDoc
     */
    MTP : "WMG_SINGLE_SPIN_PLAY_BONUS_SELECTION_REQ";
}

/**
 * Bonus selection reply for a One Spin Slot Game.
 */
interface Gen1BonusOneSpinSingleSelectionReply extends Gen1BonusSingleSelectionReply
{
    MTP : "WMG_SINGLE_SPIN_PLAY_BONUS_SELECTION_REPLY";
}

/**
 * Bonus Single Selection request for a Two Spin Slot game. This is a request to the server, to make
 * a series of 1 or more selections in the Bonus Phase.
 */
interface Gen1BonusTwoSpinSingleSelectionRequest extends Gen1BonusSingleSelectionRequest
{
    /**
     * @inheritDoc
     */
    MTP : "WMG_PLAY_BONUS_SELECTION_REQ";
}

/**
 * Bonus Single Selection reply for a Two Spin Slot game.
 */
interface Gen1BonusTwoSpinSingleSelectionReply extends Gen1BonusSingleSelectionReply
{
    /**
     * @inheritDoc
     */
    MTP : "WMG_PLAY_BONUS_SELECTION_REPLY";
}

/**
 * Bonus Multi Selection request for a Two Spin slot game.
 */
interface Gen1BonusTwoSpinMultiSelectionRequest extends Gen1BonusMultiSelectionRequest
{
    /**
     * @inheritDoc
     */
    MTP : "WMG_PLAY_BONUS_MULTIPLE_SELECTION_REQ";
}

/**
 * Bonus Multi selection reply for a Two Spin slot game.
 */
interface Gen1BonusTwoSpinMultiSelectionReply extends Gen1BonusMultiSelectionReply
{
    /**
     * @inheritDoc
     */
    MTP : "WMG_PLAY_BONUS_MULTIPLE_SELECTION_REPLY";
}

/**
 * Common part of a Bonus Phase Complete request.
 */
interface Gen1BonusPhaseCompleteRequest extends Gen1GameServiceRequest
{
    /**
     * The numerical id of the game phase.
     */
    "PHS" : 3;
}

/**
 * Common part of a Bonus Phase Complete reply.
 */
interface Gen1BonusPhaseCompleteReply extends Gen1ServiceReply
{

}

/**
 * Bonus Phase complete request for a Single Spin slot game. This informs the server that Bonus Phase
 * Results have been shown by the game client.
 */
interface Gen1BonusOneSpinPhaseCompleteRequest extends Gen1BonusPhaseCompleteRequest
{
    /**
     * @inheritDoc
     */
    MTP : "WMG_BONUS_PHASE_COMPLETE_REQ";
}

/**
 * Bonus Phase Complete Reply for a Single Spin slot game. This is the server acknowledging that Bonus
 * Phase results have been shown by the game client.
 */
interface Gen1BonusOneSpinPhaseCompleteReply extends Gen1BonusPhaseCompleteReply
{
    /**
     * @inheritDoc
     */
    MTP : "WMG_BONUS_PHASE_COMPLETE_REPLY";
}

/**
 * Bonus Phase Complete request for a Two Spin slot game. This is the game client informating the
 * server that Bonus Phase results have been shown by the game client.
 */
interface Gen1BonusTwoSpinPhaseCompleteRequest extends Gen1BonusPhaseCompleteRequest
{
    /**
     * @inheritDoc
     */
    MTP : "WMG_BONUS_PHASE_COMPLETE";
}

/**
 * Bonus Phase Complete Reply for a Two Spin slot game. This is the server acknowleding that Bonus
 * Phase results have been shown by the Game Client.
 */
interface Gen1BonusTwoSpinPhaseCompleteReply extends Gen1BonusPhaseCompleteReply
{
    /**
     * @inheritDoc
     */
    MTP : "WMG_BONUS_PHASE_DATA_SAVED";
}

//--------------------------------------------------------------------------------------------------
// Gen 1 Haunted House
//--------------------------------------------------------------------------------------------------
/**
 * Haunted house implementation of the Bonus Phase Results request.
 */
type Gen1HhBonusPhaseResultsRequest = Gen1BonusTwoSpinPhaseResultsRequest;

/**
 * Haunted House implementaiton of the Bonus Phase Results reply.
 */
type Gen1HhBonusPhaseResultsReply = Gen1BonusTwoSpinPhaseResultsReply;

/**
 * Haunted House implementation of the Bonus Multi selection request.
 */
interface Gen1HhBonusSingleSelectionRequest extends Gen1BonusTwoSpinSingleSelectionRequest
{
    /**
     * The list of Round Result Types. This should be of type number[], but its rendered
     * as a string (because this is the Gen1 platform).
     */
    CKP : string;

    /**
     * The most recent selection made.
     */
    CKD : number;

    /**
     * All selections already made. This should be of type number[], but its rendered as
     * a string (because this is the Gen1 platform).
     */
    NCK : string;
}

/**
 * Haunted House implementation of the Bonus Single Selection reply.
 */
type GenHhBonusSingleSelectionReply = Gen1BonusTwoSpinSingleSelectionReply;

/**
 * Haunted House implementation of the Bonus Multi Selection Request.
 */
interface Gen1HhBonusMultiSelectionRequest extends Gen1BonusTwoSpinMultiSelectionRequest
{
    /**
     * The list of Round Result Types. This should be of type number[], but its rendered
     * as a string (because this is the Gen1 platform).
     */
    CKP : string;

    /**
     * The most recent selection(s) made, encoded as a string (not, of course, as an array
     * of numbers, because this is the V1 platform)
     */
    CKD : string;

    /**
     * All selections already made. This should be of type number[], but its rendered as
     * a string (because this is the Gen1 platform).
     */
    NCK : string;
}

/**
 * Haunted House implementation of the Bonus Multi selection reply.
 */
type Gen1HhBonusMultiSelectionReply = Gen1BonusTwoSpinMultiSelectionReply;

/**
 * Haunted House implementation of the Bonus Phase Complete Request.
 */
type Gen1HhBonusPhaseCompleteRequest = Gen1BonusPhaseCompleteRequest;

/**
 * Haunted House implementation of the Bonus Phase Complete Reply.
 */
type Gen1HhBonusPhaseCompleteReply = Gen1BonusPhaseCompleteReply;

//--------------------------------------------------------------------------------------------------
// Gen 1 Generic haunted House
//--------------------------------------------------------------------------------------------------
/**
 * Generic haunted House implementation of the Bonus Phase Results request.
 */
type Gen1HhGenericBonusPhaseResultsRequest = Gen1HhBonusPhaseResultsRequest;

/**
 * Generic haunted House implementation of the Bonus Phase Results Reply. As for other V1 generic
 * game maths implementations, it returns a fairly abstract set of data, and an rng seed. In this
 * case, it returns simplified prize tables (representing the original Haunted House prize tables)
 * So, if we want a V2 game client using haunted house maths, we can exactly reproduce its behaviour
 * for a V1 build also.
 */
interface Gen1HhGenericBonusPhaseResultsReply extends Gen1BonusTwoSpinPhaseResultsReply
{
    /**
     * Number of scatters which triggered the Bonus Phase.
     */
    PZB : number;

    /**
     * Stringified prize table, in this form: [low prize, mid prize, big prize]
     * Should be an array, but it is a string. eg:
     * "10,2,3" translates to [10,2,3]
     */
    PZS : string;

    /**
     * Stringified number of each prize awarded ([num low win, num mid win, num big win]).
     * Should be an array, but it is a string. eg:
     * "10,2,3" translates to [10,2,3]
     * The number of elements in this field (once parsed) should be the same as the number
     * of elements in PZS. Each index in this, corresponds to the same index in PZS.
     */
    CKP : string;

    /**
     * Total amount won.
     */
    PZG : number;

    /**
     * Rng seed.
     */
    PZD : number;
}

/**
 * Generic Haunted House implementation of the Bonus Phase Complete request.
 */
type Gen1HhGenericBonusPhaseCompleteRequest = Gen1HhBonusPhaseCompleteRequest;

/**
 * Generic Haunted House implementation of the Bonus Phase Compelte reply.
 */
type Gen1HhGenericBonusPhaseCompleteReply = Gen1HhBonusPhaseCompleteReply;

/**
 * Generic Haunted House implementation of the Bonus Multi Selection request. (We won't use
 * a single selection request for this case)
 */
type Gen1HhGenericBonusMultiSelectionRequest = Gen1HhBonusMultiSelectionRequest;

//--------------------------------------------------------------------------------------------------
// Big Ghoulies bonus messages
//--------------------------------------------------------------------------------------------------
/**
 * Request for Big Ghoulies to the server, asking for Bonus Phase Results.
 */
type Gen1BgBonusPhaseResultsRequest = Gen1BonusOneSpinPhaseResultsRequest;

/**
 * Bonus Phase Results reply for Big Ghoulies.
 */
interface Gen1BgBonusPhaseResultsReply extends Gen1BonusOneSpinPhaseResultsReply
{
    /**
     * The total amount won from the Bonus Phase.
     */
    WIN : number;

    /**
     * The list of previous selections already made. This should be of type number[],
     * but its rendered as a string (because this is the Gen1 platform).
     */
    NCK : string;

    /**
     * The list of Round Result Types. This should be of type number[], but its rendered
     * as a string (because this is the Gen1 platform).
     */
    CKP : string;

    /**
     * Most recent set of symbols data, encoded as a string.
     */
    RNR : string;
}

/**
 * Request for Big Ghoulies to the server, informing it that Bonus Phase Results have been shown.
 */
type Gen1BgBonusPhaseCompleteRequest = Gen1BonusOneSpinPhaseCompleteRequest;

/**
 * Acknowledgement by the server, that a Bg Bonus Phase Result has been shown by the game client.
 */
type Gen1BgBonusPhaseCompleteReply = Gen1BonusOneSpinPhaseCompleteReply;

/**
 * Bonus Selection request for any game using Big Ghoulies mathematics.
 */
interface Gen1BgBonusSelectionRequest extends Gen1BonusSingleSelectionRequest // TODO: Extends wrong type ?
{
    /**
     * The list of Round Result Types. This should be of type number[], but its rendered
     * as a string (because this is the Gen1 platform).
     */
    CKP : string;

    /**
     * The most recent selection made.
     */
    CKD : number;

    /**
     * All selections already made. This should be of type number[], but its rendered as
     * a string (because this is the Gen1 platform).
     */
    NCK : string;
}

/**
 * Bonus Multi-Selection request for any game using Big Ghoulies mathematics.
 */
interface Gen1BgBonusMultiSelectionRequest extends Gen1BonusMultiSelectionRequest
{
    /**
     * The list of Round Results (returned by the server previously..)
     */
    CKP : string;

    /**
     * The most recent selections made
     */
    CKD : string;

    /**
     * All selections already made. This should be of type number[], but its rendered as
     * a string (because this is the Gen1 platform).
     */
    NCK : string;
}


//--------------------------------------------------------------------------------------------------
// Gen 1 Generic Big Ghoulies
//--------------------------------------------------------------------------------------------------
/**
 * A Bonus Phase Result, as generated by the Big Ghoulies maths engine (but in a generic form,
 * which removes certain Big Ghoulies data, and adds an Rng Seed field). This is to be used by
 * V1 games based on Big Ghoulies, which are performing client side parsing of the bonus phase
 * results returned by the server (we expect to send a single MultiSelection request for games
 * using this Bonus mechanism: the multi-selection request will be sent when all selections are
 * played, and so the game client can show the parsed results in any form it likes).
 */
interface Gen1BgGenericBonusPhaseResultsReply extends Gen1BonusOneSpinPhaseResultsReply
{
    /**
     * The total amount won from the Bonus Phase.
     */
    WIN : number;

    /**
     * The list of previous selections already made. This should be of type number[],
     * but its rendered as a string (because this is the Gen1 platform).
     */
    NCK : string;

    /**
     * The list of Round Result Types. This should be of type number[], but its rendered
     * as a string (because this is the Gen1 platform).
     */
    CKP : string;

    /**
     * The list of Round Result Values. For this type of Phase Result reply, the values will
     * all be credit win values. This should be of type number[], but its rendered as a string
     * (because this is the Gen1 platform).
     */
    CKD : string;

    /**
     * Number of scatters which triggered the Bonus Phase.
     */
    PZB : number;

    /**
     * Stringified list of individual prizes.
     */
    PZS : string;

    /**
     * Total Multiplier awarded.
     */
    PZD : number;

    /**
     * Rng seed.
     */
    PZG : number;
}

//--------------------------------------------------------------------------------------------------
// Fowl Play Gold bonus messages
//--------------------------------------------------------------------------------------------------
interface Gen1FpgBonusPhaseResultsRequest extends Gen1BonusTwoSpinPhaseResultsRequest
{

}

/**
 * Bonus Phase Complete request for any game using Legacy Fowl Play Gold maths and integration.
 */
interface Gen1FpgBonusPhaseCompleteRequest extends Gen1BonusTwoSpinPhaseCompleteRequest {};

/**
 * Bonus Phase Complete reply for any game using Legacy Fowl Play Gold maths and integration.
 */
interface Gen1FpgBonusPhaseCompleteReply extends Gen1BonusTwoSpinPhaseCompleteReply {};

//--------------------------------------------------------------------------------------------------
// Haunted House bonus messages
//--------------------------------------------------------------------------------------------------
/**
 * Bonus Phase Complete request for any game using Legacy Haunted House maths and integration.
 */
interface Gen1HhBonusPhaseCompleteRequest extends Gen1BonusTwoSpinPhaseCompleteRequest {};

/**
 * Bonus Phase Compelte reply for any game using Legacy Haunted House maths and integration.
 */
interface Gen1HhBonusPhaseCompleteReply extends Gen1BonusTwoSpinPhaseCompleteReply {};

//--------------------------------------------------------------------------------------------------
// Four Fowl Play bonus messages
//--------------------------------------------------------------------------------------------------
/**
 * Bonus Phase Complete request for any game using Legacy Four Fowl Play maths and integration.
 */
interface Gen1FfpBonusPhaseCompleteRequest extends Gen1BonusTwoSpinPhaseCompleteRequest {};

/**
 * Bonus Phase Complete reply for any game using Legacy Four Fowl Play maths and integration.
 */
interface Gen1FfpBonusPhaseCompleteReply extends Gen1BonusTwoSpinPhaseCompleteReply {};