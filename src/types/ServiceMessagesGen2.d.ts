//--------------------------------------------------------------------------------------------------
// Basic Gen2 server results model
//--------------------------------------------------------------------------------------------------
/**
 * Server results model for Generation 2 games. This defines what our raw results from the
 * server look like (so must be changed, if we ever need to match changes to the server /
 * maths results protocol). It's intended as an aid for parsing these results.
 * 
 * The interfaces within this module use the actual "short-key" values returned by the Game
 * Engine Server, but with documentation. The generation 2 results model will likely use
 * these values (but they should be much clearer) - so, Typescript interfaces and documentation
 * should improve our ability to parse these messages substantially.
 */

/**
 * Describes a single discrete step within a game, that can award some winnings
 * which we might want to accumulate.
 */
interface Gen2WinningResult
{
    /**
     * The total value of Credit won on this result.
     */
    totalCreditWon : number;

    /**
     * The total value of Superbet won on this result.
     */
    totalSuperBetWon : number;

    /**
     * The number of FreeGames won on this result.
     */
    numFreeGamesWon : number;
}

interface Gen2GamePhaseResult extends Gen2WinningResult
{
    /**
     * Basic ResultType of the phase.
     */
    resultType : number;

    /**
     * Id of the game phase.
     */
    phaseId : number;
}

interface Gen2SlotGamePhaseResult extends Gen2GamePhaseResult
{

}

/**
 * A win is a single discrete
 */
interface Gen2SlotWin
{
    /**
     * Returns the "group" of the reward of this Win. Examples of Reward Types
     * 1 == credit win
     * 2 == superbet win
     * 3 == freegames win
     * 4 == freespins win
     * 5 == bonus win
     * and we can expand this list with a standard enum whenever we think of
     * a new concept.
     */
    rewardGroup : number;

    /**
     * An additional "type" parameter for the win. This can tell us if the win is additive,
     * multiplicative, etc.
     */
    rewardType : number;

    /**
     * The value of this win's reward. EG: if RewardType was Credit, then a Reward Value
     * of 1500 means "15 euros won". If RewardType was "FreeSpins" then a Reward Value
     * of 10, means "10 FreeSpins won"
     */
    rewardValue : number;
}

/**
 * A Special Win is any discrete win returned in the Slot Phase, which is not represented
 * by a collection of Symbols. The Special Win mechanic can be used to return additional
 * win meta-data.
 * 
 * Consider the following examples
 * - player is given a series of Symbol Wins, and a Special Win which returns a multiplier
 * - player is given an additional random credit or points win as Special Win
 * 
 * In a Game Client, we might visualize Special Wins, using a character shown at the side
 * of the reels. After some spins, the character might perform an animation, visualizing
 * awarding the Special Win to the player.
 */
interface Gen2SymbolWin extends Gen2SlotWin
{
    /**
     * The id of the hand that this Symbol Win is shown on.
     */
    handId : number;

    /**
     * The id of the line that this symbol win is shown on. Its valid for lineId to be 0
     * (the win is described with a set of symbols, but doesnt belong to a winline).
     */
    lineId : number;
    
    /**
     * The id of the primary winning Symbol for this Symbol Win.
     */
    symbolId : number;
    
    /**
     * The list of positions (within the winning hand) that describe this win. These will
     * be in an absolute position format. eg: for Fowl Play Gold, the overall set of
     * symbol positions for a hand can be described as follows
     * 1   4   7   10  13
     * 2   5   8   11  14
     * 3   6   9   12  15
     * So, a set of winning positions as follows - [2,4,5,6,7,11,13,15] - would describe
     * a single SymbolWin that looks as follows (X == winning symbol)
     * -   X   X   -   X
     * X   X   -   X   -
     * -   X   -   -   X
     */
    winningPositions : number[];

    /**
     * Mechanic of the win (eg: winlines, scatter, all ways). The constants will be fixed,
     * but are not strongly defined yet
     */
    winMechanic : number;

    /**
     * Indicates the id of any logical "group of wins" that this symbol win belongs to.
     * The meaning is game specific.
     */
    winGroup : number;

    /**
     * The "size" of the win, changes according to win mechanic, but usually has the general
     * meaning of "number of consecutive symbols". EG: if this was a winline win, with 5 symbols
     * in a row left to right, then "winSize" would be 5. WinningPositions cannot indicate this,
     * because the symbol win might represent more than 1 win (eg: in the case of AllWaysWins)
     */
    winSize : number;

    /**
     * Number of wins indicated by this SymbolWin. Some winMechanics (eg: AllWaysWin) will indicate
     * more than 1 win - this number indicates the value. For a regular winline win, you would
     * normally expect this to hold a value of 1 (unless the maths is using some special mechanic)
     */
    numWins : number;
    
    /**
     * A list of special symbols to highlight for the win
     */
    specialSymbols : Gen2SpecialSymbol[];
}

interface Gen2SpecialSymbol
{
    position : number;
    type : number;
    value : number;
}

/**
 * A Special Win is any discrete win returned in the Slot Phase, which is not represented
 * by a collection of Symbols. The Special Win mechanic can be used to return additional
 * win meta-data.
 * 
 * Consider the following examples
 * - player is given a series of Symbol Wins, and a Special Win which returns a multiplier
 * - player is given an additional random credit or points win as Special Win
 * 
 * In a Game Client, we might visualize Special Wins, using a character shown at the side
 * of the reels. After some spins, the character might perform an animation, visualizing
 * awarding the Special Win to the player.
 */
interface Gen2SpecialWin extends Gen2SlotWin
{
    /**
     * Returns the enumerated type of this Special Win.
     */
    winType : number;
}

/**
 * A Spin Result describes a discrete spin and set of results. This is NOT a Phase Result -
 * a Spin Phase result may be composed of several spins.
 */
interface Gen2SpinResult extends Gen2WinningResult
{
    /**
     * The number of FreeSpins won on this Spin
     */
    numFreeSpinsWon : number;

    /**
     * The progressive multiplier attached to this Spin
     */
    progressiveMultiplier : number;

    /**
     * SpinType for this spin (eg: normal, sticky respin, transform respin)
     */
    spinType : number;

    /**
     * All Symbol Ids shown at the end of this spin. The outer array represents hands,
     * and the inner array represents sticky positions per hand.
     */
    symbolIds : number[][];

    /**
     * A set of symbol positions to hold as sticky on to the next spin (within
     * the same spin phase only). The outer array represents hands, and the inner
     * array represents sticky positions per hand.
     */
    stickySymbolPositions : number[][];

    /**
     * All Symbol Wins awarded on this Spin.
     */
    symbolWins : Gen2SymbolWin[];

    /**
     * Special Wins are wins, but not awarded in the symbols. Imagine an FPG game,
     * where the Chicken is at the side of the screen, and occasionally lays an egg
     * giving the player a 5 x multiplier (multiplying every win from the symbols).
     * We would use Special Wins to describe this.
     */
    specialWins : Gen2SpecialWin[];
}

/**
 * Describes the results of a round in the Free Spin phase.
 */
interface Gen2FreeSpinRoundResult extends Gen2WinningResult
{
    /**
     * Returns the results of all individual Spins.
     */
    spins : Gen2SpinResult[];
}

/**
 * Basic interface for results from a Spin Phase.  
 */
interface Gen2SpinPhaseResult extends Gen2SlotGamePhaseResult
{
	/**
	 * Returns all Spin Results.
	 */
    spins : Gen2SpinResult[];
    
    /**
     * The number of FreeSpins awarded for this Spin Phase Result.
     */
    numFreeSpinsWon : number;

    /**
     * The ids of all hands which have a bonus win.
     */
    handsWithBonusWin : number[];
}

interface Gen2QuickPhaseResult extends Gen2SpinPhaseResult
{

}

interface Gen2SingleSpinPhaseResult extends Gen2SpinPhaseResult
{

}

interface Gen2Spin1PhaseResult extends Gen2SpinPhaseResult
{
	/**
	 * Indicates if a Spin 2 phase is required.
	 */
	playSpin2 : boolean;

	/**
	 * If a Spin 2 is required, these are the start symbol ids for Spin 2.
     * The outer array represents hands, and the inner array represents
     * symbols per hand.
	 */
	spin2StartSymbolIds : number[][];

	/**
	 * Returns the autoholds pattern suggested for Spin 2. The outer array
     * represents hands, and the inner array represents holds per hand.
	 */
	autoholdsPattern : boolean[][];
}

interface Gen2Spin2PhaseResult extends Gen2SpinPhaseResult
{
    /**
     * The Symbol ids shown at the start of Spin 2. The outer array represents hands, and
     * the inner array represents symbols per hand.
     */
    startSymbolIds : number[][];
    
    /**
     * The holds pattern applied for Spin 2. The outer array represents hands, and the inner
     * array represents symbols per hand.
     */
    holdsPatternUsed : boolean[][];

    /**
     * Indicates if the player used the suggested autoholds pattern that was returned with
     * the Spin 1 Phase Result, or selected a custom holds pattern for Spin 2.
     */
    playerUsedAutoholdsPattern : boolean;
}

/**
 * Describes the results of a FreeSpin bonus phase.
 */
interface Gen2FreeSpinPhaseResult extends Gen2SlotGamePhaseResult
{
    /**
     * An integer representing the type of Free Spin phase that this was, for a game maths
     * engine that is designed to support more than 1 Free Spin phase. The meaning of this value is
     * specific to the game maths engine in question, but this will usually be used to describe
     * Free Spin Phase Results with a fundamentally different method of result calculation, and that
     * should be presented to the player in a manner which visually distinguishes them from each other.
     */
    freeSpinType : number;

    /**
     * Returns the ordreed list of all Spin Rounds attached to this Free Spin phase.
     */
    rounds : Gen2FreeSpinRoundResult[];
}

/**
 * Describes a specific Prize type within the game.
 */
interface Gen2BonusPrizeDescriptor
{
    /**
     * The numerical ID of this prize (this is like an Enum value)
     */
    prizeId : number;

    /**
     * Reward Group for this prize (standard Reward Group enum)
     */
    rewardGroup : number;

    /**
     * Reward type of this prize
     */
    rewardType : number;

    /**
     * Reward value of this prize
     */
    rewardValue : number;

    /**
     * The number of times that this prize is rewarded
     */
    numAwarded : number;
}

/**
 * Interface that describes a Bonus Round result. The GameClient is free to use
 * rounds, to group their data, or to just completely ignore this feature (and
 * create a visualization of the bonus based solely on the total winnings fields).
 */
interface Gen2BonusRoundResult extends Gen2WinningResult
{
    /**
     * The number of scatter symbols which triggered this round.
     */
    numScatters : number;

    /**
     * Prize table for this round.
     */
    prizeTable : Gen2BonusPrizeDescriptor[];
}

/**
 * Interface that describes the results of a Bonus Phase.
 */
interface Gen2BonusPhaseResult extends Gen2SlotGamePhaseResult
{
    /**
     * Returns the type of bonus phase that this was.
     */
    bonusType : number;

    /**
     * Returns a fixed RNG seed for this Bonus Phase: this can be used by game clients,
     * in order to persistently generate a parsed results simulation of these results,
     * using randomness.
     */
    rngSeed : number;

    /**
     * Returns the number of Free Spins that were awarded in the Bonus Phase.
     */
    numFreeSpinsWon : number;

    /**
     * All rounds data for this Bonus Phase.
     */
    roundResults : Gen2BonusRoundResult[];
}

/**
 * Interface that describes the results of a Bonus Phase.
 */
interface Gen2QuickBonusPhaseResult extends Gen2BonusPhaseResult
{
    bonusSymbolPositions: number[]
}


/**
 * Interface that describes the results of a Gamble Phase.
 */
interface Gen2GamblePhaseResult extends Gen2SlotGamePhaseResult
{
	/**
	 * Returns the gamble type that was selected.
	 */
	gambleType : number;

	/**
	 * Returns the total winnings bet for this gamble.
	 */
	totalWinningsBet : number;

	/**
	 * Indicates if this is a win!
	 */
    isWin : boolean;
    
    /**
     * The multiplier value the gamble was being played for. (if "isWin" is true,
     * then this is also the multiplier value won).
     */
    gambleMultiplier : number;
}

//--------------------------------------------------------------------------------------------------
// Results as returned from the server
//--------------------------------------------------------------------------------------------------
/**
 * Basic interface for all messages sent to the Server, in the V2 platform.
 */
interface Gen2ServiceRequest
{
    /**
     * Request id, generated uniquely by the game client for each new request
     */
    id : string;

    /**
     * The type of message being sent. Because all requests are sent to a single end-point
     * on the server, the Message Type value is critical for the server to correctly identify
     * the nature of the request it has received.
     */
    messageType : string;

    /**
     * Web session id.
     */
    wmgWebSessionId : string;

    gameplayMode : GameplayMode, //"free" | "real";

    wmgGame : WmgGameDescription;
}

/**
 * Basic interface for all messages sent by the server in the V2 Platform.
 */
interface Gen2ServiceReply
{
    /**
     * String value, indicating the type of the response message.
     */
    messageType : string;

    /**
     * Id of the Web Session in progress.
     */
    wmgWebSessionId : string;

    /**
     * Defines any errors that accompany this message.
     */
    error ? : ServiceExecutionError;
}

interface WmgGameDescription
{
    /**
     * Integer value of game id: this is reported as a url parameter
     */
    gameId : number;

    /**
     * Integer value of licensee id being used: this is reported as a url parameter
     */
    licenseeId : number;

    gameLabel : string;
}

// TODO: All of these are not Gen2 specific, but want moving to a common moduile


/**
 * The country the game client is being played in.
 */
type Country = "" | "ITA";

/**
 * Currency mode in use in the game client.
 */
type Currency = "" | "euro";

/**
 * Common interface for a Session Info object.
 */
type SessionInfo = DefaultSessionInfo | SessionInfoItaly;

interface BaseSessionInfo
{
    /**
     * The country of the Session Info. This field basically links the instance to
     * a concrete sub-type. Set a value of "NONE" to use default configuration (this
     * is likely to be what we do for freeplay or demo type builds).
     */
    country : Country;
}

interface DefaultSessionInfo
{
    country : "";
}

/**
 * Session Info used for Italy.
 */
interface SessionInfoItaly extends BaseSessionInfo
{
    /**
     * @inheritDoc
     */
    country : "ITA";

    aamsSessionId : string;

    aamsTicketId : string;

    ticketProgr : number;

    regMoneyLimit : number;

    amountAlreadyImported : number;
}

/**
 * The type of Currency from which a subbet may be played.
 */
type CurrencyType = "CREDIT" | "SUPER_BET" | "WINNINGS" | "PROMO_GAME";

/**
 * Common base for BetSettings for a single SubBet.
 */
interface BetSettings
{
    currencyType : CurrencyType;
}

/**
 * Bet Settings for a single SubBet for a slot game.
 */
interface SlotBetSettings extends BetSettings
{
    stakePerLine : number;

    numLines : number;

    numHands : number;

    specialWagerMultiplier : number;
}

//--------------------------------------------------------------------------------------------------
// 
//--------------------------------------------------------------------------------------------------
interface Gen2DownloadStatisticsRequest extends Gen2ServiceRequest
{
    /**
     * @inheritDoc
     */
    messageType : "WMG_DOWNLOAD_STATISTICS_REQUEST";

    gameClientVersion : string;

    userAgent : string;

    userAgentVersion : string;
}

interface Gen2DownloadStatisticsReply extends Gen2ServiceReply
{
    /**
     * @inheritDoc
     */
    messageType : "WMG_DOWNLOAD_STATISTICS_REPLY";

    /**
     * Selected gameplayMode for the game client.
     */
    gameplayMode : GameplayMode;

    /**
     * Nickname of the player.
     */
    userId : string;
}

/**
 * Request to check if any game session is in progress (and should be restored)
 */
interface Gen2CheckPendingRequest extends Gen2ServiceRequest
{
    /**
     * @inheritDoc
     */
    messageType : "WMG_CHECK_PENDING_REQUEST";

    /**
     * @inheritDoc
     */
    gameplayMode : GameplayMode;
}

/**
 * Response to the Check Pending Service.
 */
interface Gen2CheckPendingReply extends Gen2ServiceReply
{
    /**
     * @inheritDoc
     */
    messageType : "WMG_CHECK_PENDING_REPLY";

    // TODO: The information for the case where a game is to be restored,
    // is not yet defined in the documentation.
}

/**
 * Request to check the player's current useable balance.
 */
interface Gen2CheckBalanceRequest extends Gen2ServiceRequest
{
    /**
     * @inheritDoc
     */
    messageType : "WMG_BALANCE_REQUEST";
}

/**
 * Response to the Check Pending 
 */
interface Gen2CheckBalanceReply extends Gen2ServiceReply
{
    /**
     * @inheritDoc
     */
    messageType : "WMG_BALANCE_REPLY";

    /**
     * The amount of money available to the player for transferring to a session.
     */
    accountBalance : number;
}

interface Gen2SessionInitRequest extends Gen2ServiceRequest
{
    /**
     * @inheritDoc
     */
    messageType : "WMG_SESSION_INIT_REQUEST";

    /**
     * The amount of money that the player whishes to add to their session.
     */
    transfMoney : number;
}

/**
 * Response to the Session Init operation.
 */
interface Gen2SessionInitReply extends Gen2ServiceReply
{
    /**
     * @inheritDoc
     */
    messageType : "WMG_SESSION_INIT_REPLY";

    /**
     * Indicates the type of new session that has been opened.
     */
    gameplayMode : GameplayMode;

    /**
     * Info about the new session. The type of obejct here changes according to the
     * country that the game is deployed in.
     */
    regulation : SessionInfo;

    /**
     * The number of minutes of Session Inactivity Timeout that should be used.
     */
    timeout : number;

    /**
     * The current value of Wallet.
     */
    currentWallet : number;

    /**
     * The current value of Winnings.
     */
    winningsAmount : number;

    /**
     * The current value of Superbet.
     */
    superbetAmount : number;

    /**
     * The current value of Num FreeSpins.
     */
    numFreeSpins : number;
    
    /**
     * Current value of Wmg Round Ref.
     */
    wmgRoundRef : number;

    /**
     * Indicates if there is a PromoGames award from this Session Init Reply.
     */
    promoGamesAwarded : boolean;

    /**
     * If promoGamesAwarded is true, then this field will contain information about the award.
     */
    promoGamesData ? : WmgPromoGamesAward;
}

interface Gen2AddCreditRequest extends Gen2ServiceRequest
{
    /**
     * @inheritDoc
     */
    messageType : "WMG_ADD_CREDIT_REQUEST";

    regulation : SessionInfo;

    /**
     * The current value of Session.Wallet
     */
    currentWallet : number;

    /**
     * The amount of additional money that the player whishes to transfer to the session.
     */
    transfMoney : number;
}

interface Gen2AddCreditReply extends Gen2ServiceReply
{
    /**
     * @inheritDoc
     */
    messageType : "WMG_ADD_CREDIT_REPLY";

    regulation : SessionInfo;

    /**
     * The new value of wallet, after credit has been added to the Session.
     */
    currentWallet : number;
}

interface Gen2GameCompleteRequest extends Gen2ServiceRequest
{
    /**
     * @inheritDoc
     */
    messageType : "WMG_GAME_COMPLETE_REQUEST";

    wmgRoundRef : number | string;

    regulation : SessionInfo;
}


interface Gen2GameCompleteReply extends Gen2ServiceReply
{
    /**
     * @inheritDoc
     */
    messageType : "WMG_GAME_COMPLETE_REPLY";

    /**
     * New value of wallet, after the game has been completed.
     */
    currentWallet : number;

    /**
     * Additional data properties forwarded by the Licensee Management Server. Integrations
     * with 3rd party platforms may occasionally need us to forward some custom data to the
     * game client (which will normally be solely for the consumption of a 3rd party GUI
     * that is integrated with our game). This field can be used generically to house this
     * data. SG Digital was the first use case for this (there is custom FreeRounds data
     * forwarded when a new PromoGame starts, which is solely for the use of SG Digital's
     * GUI, to show certain special notifications).
     */
    lmsExtraInfo ? : Object;
}

interface Gen2KeepAliveRequest extends Gen2ServiceRequest
{
    /**
     * @inheritDoc
     */
    messageType : "WMG_KEEP_ALIVE_REQUEST";
}

interface Gen2KeepAliveReply extends Gen2ServiceReply
{
    /**
     * @inheritDoc
     */
    messageType : "WMG_KEEP_ALIVE_REPLY";
}

interface Gen2SessionEndRequest extends Gen2ServiceRequest
{
    /**
     * @inheritDoc
     */
    messageType : "WMG_SESSION_END_REQUEST";

    regulation ? : SessionInfo;
}

interface Gen2SessionEndReply extends Gen2ServiceReply
{
    /**
     * @inheritDoc
     */
    messageType : "WMG_SESSION_END_REPLY";

    /**
     * Session Info: only guaranteed to be delivered, when gameplay mode is "real"
     */
    regulation ? : SessionInfo;

    /**
     * Final wallet value for the session. This is the amount the player gets back to their account.
     */
    currentWallet : number;
}

//--------------------------------------------------------------------------------------------------
// Game Phase Results Services
//--------------------------------------------------------------------------------------------------
/**
 * Common part of a request for Game Phase results.
 */
interface Gen2GamePhaseResultsRequest extends Gen2ServiceRequest
{
    currentWallet : number;

    winningsAmount : number;

    superbetAmount : number;

    numFreeSpins : number;

    numPromoGames : number;

    wmgRoundRef : number | string;

    /**
     * Should only be appended for Realplay games
     */
    //regulation ? : SessionInfo;
    regulation : SessionInfo;
}

/**
 * Common part of a reply containing Game Phase results.
 * @param Res
 * The type of Phase Result returned.
 */
interface Gen2GamePhaseResultsReply<Res>
    extends Gen2ServiceReply
{
    /**
     * Info about the game session in progress. We only expect this to be available
     * for a Game Phase Results reply, when it is returned as a response to a Check
     * Pending Request (ie: when the message is intended for restoring a game session).
     */
    regulation ? : SessionInfo;

    currentWallet : number;

    winningsAmount : number;

    superbetAmount : number;

    numFreeSpins : number;

    numPromoGames : number;

    wmgRoundRef : number | string;

    result : Res;
}

/**
 * Common part of a request for Slot Game Phase results.
 */
interface Gen2SlotGamePhaseResultsRequest extends Gen2GamePhaseResultsRequest
{
    betSettings : SlotBetSettings;
}

/**
 * Common part of a reply containing Slot Game Phase Results.
 */
interface Gen2SlotGamePhaseResultsReply<Res>
    extends Gen2GamePhaseResultsReply<Res>
{
    betSettings : SlotBetSettings;
}

/**
 * A request for Quick Spin Phase results
 */
interface Gen2QuickSpinPhaseResultsRequest extends Gen2SlotGamePhaseResultsRequest
{
    /**
     * @inheritDoc
     */
    messageType: "WMG_QUICK_PHASE_RESULTS_REQUEST";
}
/**
 * A request for Single Spin Phase results.
 */
interface Gen2SingleSpinPhaseResultsRequest extends Gen2SlotGamePhaseResultsRequest
{
    /**
     * @inheritDoc
     */
    messageType : "WMG_SINGLE_SPIN_PHASE_RESULTS_REQUEST";
}

interface Gen2QuickPhaseResultsReply extends Gen2SlotGamePhaseResultsReply<Gen2QuickPhaseResult>
{
    /**
     * Bonus Ref (entity id) for any Bonus Phase that will follow the Single Spin Phase of the
     * sub-game. If no BonusPhase is to be triggered, the server will return a value of -1.
     */
    wmgBonusRef : number | string;

     /**
     * Ref (entity id) for this Single Spin Phase.
     */
     wmgSpinRef : number | string;
}

/**
 * A reply containing Single Spin Phase Results.
 */
interface Gen2SingleSpinPhaseResultsReply
    extends Gen2SlotGamePhaseResultsReply<Gen2SingleSpinPhaseResult>
{
    /**
     * @inheritDoc
     */
    messageType : "WMG_SINGLE_SPIN_PHASE_RESULTS_REPLY";

    /**
     * Ref (entity id) for this Single Spin Phase.
     */
    wmgSpinRef : number | string;

    /**
     * Bonus Ref (entity id) for any Bonus Phase that will follow the Single Spin Phase of the
     * sub-game. If no BonusPhase is to be triggered, the server will return a value of -1.
     */
    wmgBonusRef : number | string;

    /**
     * FreeSpins Ref (entity id) for any FreeSpins Phase that will happen in this sub-game. If
     * no FreeSpin phase is to be triggered, the server will return a value of 01.
     */
    wmgFreespinRef : number | string;

    /**
     * The result for the Single Spin phase.
     */
    result : Gen2SingleSpinPhaseResult;

    /**
     * Additional data properties forwarded by the Licensee Management Server. Integrations
     * with 3rd party platforms may occasionally need us to forward some custom data to the
     * game client (which will normally be solely for the consumption of a 3rd party GUI
     * that is integrated with our game). This field can be used generically to house this
     * data. SG Digital was the first use case for this (there is custom FreeRounds data
     * forwarded when a new PromoGame starts, which is solely for the use of SG Digital's
     * GUI, to show certain special notifications).
     */
    lmsExtraInfo ? : {};
}

/**
 * A request for Spin 1 Phase results.
 */
interface Gen2Spin1PhaseResultsRequest extends Gen2SlotGamePhaseResultsRequest
{
    /**
     * @inheritDoc
     */
    messageType : "WMG_SPIN_1_PHASE_RESULTS_REQUEST";
}

/**
 * A reply containing Spin 1 Phase results.
 */
interface Gen2Spin1PhaseResultsReply
    extends Gen2SlotGamePhaseResultsReply<Gen2Spin1PhaseResult>
{
    /**
     * @inheritDoc
     */
    messageType : "WMG_SPIN_1_PHASE_RESULTS_REPLY";

    /**
     * Ref (entity id) for this Spin 1 Phase.
     */
    wmgSpinRef : number | string;

    /**
     * Bonus Ref (entity id) for any Bonus Phase that will immediately follow the Spin 1 Phase
     * of the sub-game. If no bonus phase is to be triggered after Spin 1 (or if a Spin 2 is
     * going to happen), then the server will return a value of -1. The field will only have
     * a different value when a Bonus Phase is coming after Spin 1 (and no Spin 2 is going to
     * happen).
     */
    wmgBonusRef : number | string;
 
    /**
     * FreeSpins Ref (entity id) for any FreeSpin Phase that will immediately follow the Spin 1
     * Phase of the sub-game. If no FreeSpin phase is to be triggered after Spin 1 (or if a Spin
     * 2 is going to happen), then the server will return a value of -1. The field will only have
     * a different value when a FreeSpin Phase is coming after Spin 1 (and no Spin 2 is going to
     * happen). Please note, that if a Bonus Phase is triggered from Spin 1, that this could come
     * before the FreeSpin phase (and that the FreeSpin phase could be triggered from the Bonus)
     */
    wmgFreespinRef : number | string;

    result : Gen2Spin1PhaseResult;

    /**
     * Additional data properties forwarded by the Licensee Management Server. Integrations
     * with 3rd party platforms may occasionally need us to forward some custom data to the
     * game client (which will normally be solely for the consumption of a 3rd party GUI
     * that is integrated with our game). This field can be used generically to house this
     * data. SG Digital was the first use case for this (there is custom FreeRounds data
     * forwarded when a new PromoGame starts, which is solely for the use of SG Digital's
     * GUI, to show certain special notifications).
     */
    lmsExtraInfo ? : {};
}

/**
 * A request for Spin 2 Phase results.
 */
interface Gen2Spin2PhaseResultsRequest extends Gen2SlotGamePhaseResultsRequest
{
    /**
     * @inheritDoc
     */
    messageType : "WMG_SPIN_2_PHASE_RESULTS_REQUEST";

    startSymbolIds : number[][];

    selectedHolds : boolean[][];
}

/**
 * A reply containing Spin 2 Phase results.
 */
interface Gen2Spin2PhaseResultsReply
    extends Gen2SlotGamePhaseResultsReply<Gen2Spin2PhaseResult>
{
    /**
     * @inheritDoc
     */
    messageType : "WMG_SPIN_2_PHASE_RESULTS_REPLY";

    /**
     * Ref (entity id) for this Spin 1 Phase.
     */
    wmgSpinRef : number | string;

    /**
     * Bonus Ref (entity id) for any Bonus Phase that will immediately follow the Spin 2 Phase
     * of the sub-game. If no bonus phase is to be triggered after Spin 2, then the server will
     * return a value of -1.
     */
    wmgBonusRef : number | string;
  
    /**
     * FreeSpins Ref (entity id) for any FreeSpin Phase that will immediately follow the Spin 2
     * Phase of the sub-game. If no FreeSpin phase is to be triggered after Spin 2, then the server
     * will return a value of -1. Please note, that if a Bonus Phase is triggered from Spin 2, that
     * this could come before the FreeSpin phase (and that the FreeSpin phase could be triggered
     * from the Bonus)
     */
    wmgFreespinRef : number | string;

    result : Gen2Spin2PhaseResult;
}

/**
 * A request for Bonus Phase results.
 */
interface Gen2BonusPhaseResultsRequest extends Gen2SlotGamePhaseResultsRequest
{
    /**
     * @inheritDoc
     */
    messageType : "WMG_BONUS_PHASE_RESULTS_REQUEST";

    /**
     * Bonus Ref (entity id) for the Bonus Phase that is being triggered.
     */
    wmgBonusRef : number | string;
}

/**
 * A reply containing Bonus Phase results.
 */
interface Gen2BonusPhaseResultsReply
    extends Gen2SlotGamePhaseResultsReply<Gen2BonusPhaseResult>
{
    /**
     * @inheritDoc
     */
    messageType : "WMG_BONUS_PHASE_RESULTS_REPLY";

    /**
     * Bonus Ref (entity id) for *this* Bonus Phase.
     */
    wmgBonusRef : number | string;

    result : Gen2BonusPhaseResult;
}

/**
 * A reply containing Bonus Phase results.
 */
interface Gen2QuickBonusPhaseResultsReply
    extends Gen2SlotGamePhaseResultsReply<Gen2BonusPhaseResult>
{
    /**
     * @inheritDoc
     */
    messageType : "WMG_BONUS_PHASE_RESULTS_REPLY";

    /**
     * Bonus Ref (entity id) for *this* Bonus Phase.
     */
    wmgBonusRef : number | string;

    result : Gen2QuickBonusPhaseResult;
}

/**
 * A request for FreeSpin phase results.
 */
interface Gen2FreeSpinPhaseResultsRequest extends Gen2SlotGamePhaseResultsRequest
{
    /**
     * @inheritDoc
     */
    messageType : "WMG_FREESPIN_PHASE_RESULTS_REQUEST";

    /**
     * FreeSpin Ref (entity id) for the FreeSpin phase that is being triggered.
     */
    wmgFreespinRef : number | string;
}

/**
 * A reply containing FreeSpin phase results.
 */
interface Gen2FreeSpinPhaseResultsReply
    extends Gen2SlotGamePhaseResultsReply<Gen2FreeSpinPhaseResult>
{
    /**
     * @inheritDoc
     */
    messageType : "WMG_FREESPIN_PHASE_RESULTS_REPLY";

    /**
     * FreeSpin Ref (entity id) for *this* FreeSpin phase.
     */
     wmgFreespinRef : number | string;

    /**
     * Phase result object
     */
    result : Gen2FreeSpinPhaseResult;
}

//--------------------------------------------------------------------------------------------------
// Game Phase complete Services
//--------------------------------------------------------------------------------------------------
/**
 * Common part of a request to infom the server that a Game Phase result has been shown by
 * the game client.
 */
interface Gen2GamePhaseCompleteRequest extends Gen2ServiceRequest
{
    wmgRoundRef : number | string;

    regulation : SessionInfo;
}

/**
 * Common part of a reply from the server, acknowledging that a Game Phase Result has been
 * shown by the game client.
 */
interface Gen2GamePhaseCompleteReply extends Gen2ServiceReply {};

/**
 * Common part of a request to inform the server that a Slot Game Phase result has been
 * shown by the game client.
 */
interface Gen2SlotGamePhaseCompleteReqeust extends Gen2GamePhaseCompleteRequest
{
    betSettings : SlotBetSettings;
}

/**
 * Common part of a reply from the server, acknowledging that a Slot Game Phase Result has
 * been shown by the game client.
 */
interface Gen2SlotGamePhaseCompleteReply extends Gen2GamePhaseCompleteReply {};

/**
 * A request to inform the server that a Single Spin Phase Result has been shown by the
 * game client.
 */
interface Gen2SingleSpinPhaseCompleteRequest extends Gen2SlotGamePhaseCompleteReqeust
{
    /**
     * @inheritDoc
     */
    messageType : "WMG_SINGLE_SPIN_PHASE_COMPLETE_REQUEST";

    wmgSpinRef : number | string;
}

/**
 * A reply from the server, acknowleding that a Single Spin Phase Result has been shown
 * by the game client.
 */
interface Gen2SingleSpinPhaseCompleteReply extends Gen2SlotGamePhaseCompleteReply
{
    /**
     * @inheritDoc
     */
    messageType : "WMG_SINGLE_SPIN_PHASE_COMPLETE_REPLY";
}

/**
 * A request to inform the server that a Spin 1 Phase Result has been shown by the
 * game client.
 */
interface Gen2Spin1PhaseCompleteRequest extends Gen2SlotGamePhaseCompleteReqeust
{
    /**
     * @inheritDoc
     */
    messageType : "WMG_SPIN_1_PHASE_COMPLETE_REQUEST";

    wmgSpinRef : number | string;
}

/**
 * A reply from the server, acknowleding that a Spin 1 Phase Result has been shown
 * by the game client.
 */
interface Gen2Spin1PhaseCompleteReply extends Gen2SlotGamePhaseCompleteReply
{
    /**
     * @inheritDoc
     */
    messageType : "WMG_SPIN_1_PHASE_COMPLETE_REPLY";
}

/**
 * A request to inform the server that a Spin 2 Phase Result has been shown by the
 * game client.
 */
interface Gen2Spin2PhaseCompleteRequest extends Gen2SlotGamePhaseCompleteReqeust
{
    /**
     * @inheritDoc
     */
    messageType : "WMG_SPIN_2_PHASE_COMPLETE_REQUEST";

    wmgSpinRef : number | string;
}

/**
 * A reply from the server, acknowleding that a Spin 2 Phase Result has been shown
 * by the game client.
 */
interface Gen2Spin2PhaseCompleteReply extends Gen2SlotGamePhaseCompleteReply
{
    /**
     * @inheritDoc
     */
    messageType : "WMG_SPIN_2_PHASE_COMPLETE_REPLY";
}

/**
 * A request to inform the server that a Bonus Phase Result has been shown by the
 * game client.
 */
interface Gen2BonusPhaseCompleteRequest extends Gen2SlotGamePhaseCompleteReqeust
{
    /**
     * @inheritDoc
     */
    messageType : "WMG_BONUS_PHASE_COMPLETE_REQUEST";

    wmgBonusRef : number | string;
}

/**
 * A reply from the server, acknowleding that a Bonus Phase Result has been shown
 * by the game client.
 */
interface Gen2BonusPhaseCompleteReply extends Gen2SlotGamePhaseCompleteReply
{
    /**
     * @inheritDoc
     */
    messageType : "WMG_BONUS_PHASE_COMPLETE_REPLY";
}

/**
 * A request to inform the server that a FreeSpin Phase Result has been shown by the
 * game client.
 */
interface Gen2FreeSpinPhaseCompleteRequest extends Gen2SlotGamePhaseCompleteReqeust
{
    /**
     * @inheritDoc
     */
    messageType : "WMG_FREESPIN_PHASE_COMPLETE_REQUEST";

    wmgFreespinRef : number | string;
}

/**
 * A reply from the server, acknowleding that a FreeSpin Phase Result has been shown
 * by the game client.
 */
interface Gen2FreeSpinPhaseCompleteReply extends Gen2SlotGamePhaseCompleteReply
{
    /**
     * @inheritDoc
     */
    messageType : "WMG_FREESPIN_PHASE_COMPLETE_REPLY";
}

/**
 * A request to inform the server that a Quick Phase Result has been shown by the
 * game client.
 */
interface Gen2QuickPhaseCompleteRequest extends Gen2SlotGamePhaseCompleteReqeust
{
    /**
     * @inheritDoc
     */
    messageType : "WMG_QUICK_PHASE_COMPLETE_REQUEST";

    wmgSpinRef : number | string;
}

/**
 * A reply from the server, acknowleding that a FreeSpin Phase Result has been shown
 * by the game client.
 */
interface Gen2QuickPhaseCompleteReply extends Gen2SlotQuickPhaseCompleteReply
{
    /**
     * @inheritDoc
     */
    messageType : "WMG_QUICK_PHASE_COMPLETE_REPLY";
}


