//--------------------------------------------------------------------------------------------------
// Type script definitions for the Services module
//--------------------------------------------------------------------------------------------------
// This is the important public API available ( even within the business logic layer )
//
// TODO:
// A lot of documentation and tidy up is still required (this is about 10% complete..)
//--------------------------------------------------------------------------------------------------

interface ServicesApi
{
    /**
     * Adds one or more request property injectors to the services instance. This mechanism
     * allows us to inject arbitrary additional properties into outgoing request messages
     * (useful in certain cases where we may want to use a standard Services impementation,
     * but we require to support some additional values being sent, which could be entirely
     * abritrary in nature).
     * @param {RequestPropertyInjector[]} injectors 
     * One or more request property injectors to add.
     */
    addRequestPropertyInjectors(...injectors) : void;

    /**
     * 
     * @param onSuccess
     * Callback executed when the Download Statistics operation has completed.
     * @param onFailure 
     * Comms Failure Handler, invoked in the case that the service fails due
     * to some kind of comms error, or the server returns a response packet that has an
     * error field (ie: the server refused to execute the service). If a failure handler
     * is not supplied, the service will default to throwing a Fatal Error event.
     */
    sendDownloadStatisticsRequest(
        onSuccess : CommsSuccessHandler<DownloadStatisticsData>,
        onFailure : CommsFailureHandler);

    /**
     * Sends a Check For Open Session request to the server. If a session is open,
     * the a result of some kind will be returned : this is the result state to which
     * the game must restore.
     * @public
     * @param onSuccess
     * Callback to execute when the server returns a reply with no errors. If a game
     * session is open and should be restored, then the success handle will be passed a
     * Game Result Packet to restore from. If no Game Result Packet is passed to the
     * success handler, this indicates that their is no session to restore.
     * @param onFailure
     * Comms Failure Handler, invoked in the case that the service fails due
     * to some kind of comms error, or the server returns a response packet that has an
     * error field (ie: the server refused to execute the service). If a failure handler
     * is not supplied, the service will default to throwing a Fatal Error event.
     */
    sendCheckForOpenSessionRequest(
        gameplayMode : GameplayMode,
        onSuccess : (reply? : RestoredGameResultPacket<?>) => void,
        onFailure : CommsFailureHandler) : void;

    /**
     * Fetches balance information from the server.
     * @param gameplayMode
     * The mode of the session.
     * @param onSuccess 
     * Callback executed when balance information has been succesfully
     * retrieved from the Game Engine Server.
     * @param onFailure
     * Comms Failure Handler, invoked in the case that the service fails due
     * to some kind of comms error, or the server returns a response packet that has an
     * error field (ie: the server refused to execute the service). If a failure handler
     * is not supplied, the service will default to throwing a Fatal Error event.
     * @param [customRequestParameters]
     * Optional set of additional fields which are added to the request.
     */
    sendCheckBalanceRequest(
        gameplayMode : GameplayMode,
        onSuccess : CommsSuccessHandler<BalanceData>,
        onFailure : CommsFailureHandler,
        customRequestParameters ? : Object
    );
    
    /**
     * 
     * @param requestedBalance
     * The amount of credit the player wishes to have for in the new game session.
     * @param gameplayMode 
     * The mode the player wants for their new game session.
     * @param onSuccess 
     * @param onFailure
     * Comms Failure Handler, invoked in the case that the service fails due
     * to some kind of comms error, or the server returns a response packet that has an
     * error field (ie: the server refused to execute the service). If a failure handler
     * is not supplied, the service will default to throwing a Fatal Error event.
     */
    sendSessionInitRequest(
        requestedBalance : number,
        gameplayMode : GameplayMode,
        onSuccess : CommsSuccessHandler<Gen2SessionInitReply>,
        onFailure : CommsFailureHandler
    );


    /**
     * Sends a Session Init Request, asking the server to open a new session with max
     * balance.
     * @param gameplayMode 
     * The mode the player wants for their new game session.
     * @param onSuccess 
     * @param onFailure
     * Comms Failure Handler, invoked in the case that the service fails due
     * to some kind of comms error, or the server returns a response packet that has an
     * error field (ie: the server refused to execute the service). If a failure handler
     * is not supplied, the service will default to throwing a Fatal Error event.
     */
    sendSessionInitWithMaxBalanceRequest(
        gameplayMode : GameplayMode,
        onSuccess : CommsSuccessHandler<Gen2SessionInitReply>,
        onFailure : CommsFailureHandler,
        customRequestParameters ? : Object
    );

    // TODO: I think this is not used, and should be deprecated ?
    sendGetOpenSessionRequest(
        gameplayMode : GameplayMode,
        onSuccess : CommsSuccessHandler<Gen2SessionInitReply>,
        onFailure : CommsFailureHandler
    );

    /**
     * Sends an add credit request to the Game Engine Server.
     * @param amountOfCreditToAdd
     * The amount of credit the player wishes to add to add to their game session.
     * @param onSuccess
     * @param onFailure
     * Comms Failure Handler, invoked in the case that the service fails due
     * to some kind of comms error, or the server returns a response packet that has an
     * error field (ie: the server refused to execute the service). If a failure handler
     * is not supplied, the service will default to throwing a Fatal Error event.
     */
    sendAddCreditRequest(
        amountOfCreditToAdd : number,
        onSuccess : CommsSuccessHandler<Gen2AddCreditReply>,
        onFailure : CommsFailureHandler
    );

    /**
     * 
     * @param onSuccess 
     * @param onFailure
     * Comms Failure Handler, invoked in the case that the service fails due
     * to some kind of comms error, or the server returns a response packet that has an
     * error field (ie: the server refused to execute the service). If a failure handler
     * is not supplied, the service will default to throwing a Fatal Error event.
     */
    sendSessionEndRequest(
        onSuccess : CommsSuccessHandler<Gen2SessionEndReply>,
        onFailure : CommsFailureHandler
    );

    /**
     * 
     * @param onSuccess 
     * @param onFailure
     * Comms Failure Handler, invoked in the case that the service fails due
     * to some kind of comms error, or the server returns a response packet that has an
     * error field (ie: the server refused to execute the service). If a failure handler
     * is not supplied, the service will default to throwing a Fatal Error event.
     */
    sendGameCompleteRequest(
        onSuccess : CommsSuccessHandler<Gen2GameCompleteReply>,
        onFailure : CommsFailureHandler
    );


    /**
     * Sends the Session Keep Alive request to the server.
     * @param onSuccess 
     * @param onFailure 
     */
    sendKeepAliveRequest(
        onSuccess : () => void,
        onFailure ? : CommsFailureHandler
    );

    
    /*
    sendFunBonusActivationRequest(
        onSuccess : CommsSuccessHandler,
        onFailure : CommsFailureHandler
    );
    */
}

interface SlotGameServicesApi extends ServicesApi
{

    /**
     * @param betSettings 
     * @param onSuccess
     * @param onFailure
     * Comms Failure Handler, invoked in the case that the service fails due
     * to some kind of comms error, or the server returns a response packet that has an
     * error field (ie: the server refused to execute the service). If a failure handler
     * is not supplied, the service will default to throwing a Fatal Error event.
     */
    sendQuickPhaseResultsRequest(
        betSettings : SlotBetSettings,
        gameStartParams : GameStartParams | null,
        onSuccess : (
            reply : GameResultPacket<QuickSpinPhaseResult>,
            lmsCustomData ? : Object) => void,
        onFailure : CommsFailureHandler
    )

    // TODO : this is subtly different to what is currently being done.
    // That is because we are parsing even the V2 results model a little
    // bit (because we don't return exactly what the client currently wants...)
    // So, I might need to reconsider this part
    /**
     * 
     * @param betSettings 
     * @param onSuccess 
     * @param onFailure
     * Comms Failure Handler, invoked in the case that the service fails due
     * to some kind of comms error, or the server returns a response packet that has an
     * error field (ie: the server refused to execute the service). If a failure handler
     * is not supplied, the service will default to throwing a Fatal Error event.
     */
    sendSingleSpinPhaseResultsRequest(
        betSettings : SlotBetSettings,
        gameStartParams : GameStartParams | null,
        onSuccess : (
            reply : GameResultPacket<SingleSpinPhaseResult>,
            lmsCustomData ? : Object) => void,
        onFailure : CommsFailureHandler
    );

    /**
     * @param onFailure
     * Comms Failure Handler, invoked in the case that the service fails due
     * to some kind of comms error, or the server returns a response packet that has an
     * error field (ie: the server refused to execute the service). If a failure handler
     * is not supplied, the service will default to throwing a Fatal Error event.
     */
    sendSingleSpinPhaseCompleteRequest(
        onSuccess : () => void,
        onFailure : CommsFailureHandler
    );

    // TODO : this is subtly different to what is currently being done.
    // That is because we are parsing even the V2 results model a little
    // bit (because we don't return exactly what the client currently wants...)
    // So, I might need to reconsider this part
    /**
     * 
     * @param betSettings 
     * @param onSuccess 
     * @param onFailure
     * Comms Failure Handler, invoked in the case that the service fails due
     * to some kind of comms error, or the server returns a response packet that has an
     * error field (ie: the server refused to execute the service). If a failure handler
     * is not supplied, the service will default to throwing a Fatal Error event.
     * 
     */
    sendSpin1PhaseResultsRequest(
        betSettings : SlotBetSettings,
        gameStartParams : GameStartParams | null,
        onSuccess : (
            reply : GameResultPacket<Spin1PhaseResult>,
            lmsCustomData ? : Object) => void,
        onFailure : CommsFailureHandler
    );

    /**
     * 
     * @param onSuccess 
     * @param onFailure 
     * Comms Failure Handler, invoked in the case that the service fails due
     * to some kind of comms error, or the server returns a response packet that has an
     * error field (ie: the server refused to execute the service). If a failure handler
     * is not supplied, the service will default to throwing a Fatal Error event.
     */
    sendSpin1PhaseCompleteRequest(
        onSuccess : () => void,
        onFailure : CommsFailureHandler
    );

    // TODO : this is subtly different to what is currently being done.
    // That is because we are parsing even the V2 results model a little
    // bit (because we don't return exactly what the client currently wants...)
    // So, I might need to reconsider this part
    /**
     * 
     * @param holdsPattern
     * The holds pattern selected by the player for Spin 2.
     * @param onSuccess
     * @param onFailure
     * Comms Failure Handler, invoked in the case that the service fails due
     * to some kind of comms error, or the server returns a response packet that has an
     * error field (ie: the server refused to execute the service). If a failure handler
     * is not supplied, the service will default to throwing a Fatal Error event.
     */
    sendSpin2PhaseResultsRequest(
        holdsPattern : HoldsPattern,
        onSuccess : (reply : GameResultPacket<Spin2PhaseResult>) => void,
        onFailure : CommsFailureHandler
    );

    /**
     * 
     * @param onSuccess 
     * @param onFailure
     * Comms Failure Handler, invoked in the case that the service fails due
     * to some kind of comms error, or the server returns a response packet that has an
     * error field (ie: the server refused to execute the service). If a failure handler
     * is not supplied, the service will default to throwing a Fatal Error event.
     */
    sendSpin2PhaseCompleteRequest(
        onSuccess : () => void,
        onFailure : CommsFailureHandler
    );

    // TODO: For now, we do not "adapt" the bonus model
    /**
     * 
     * @param onSuccess 
     * @param onFailure
     * Comms Failure Handler, invoked in the case that the service fails due
     * to some kind of comms error, or the server returns a response packet that has an
     * error field (ie: the server refused to execute the service). If a failure handler
     * is not supplied, the service will default to throwing a Fatal Error event.
     */
    sendBonusPhaseResultsRequest(
        onSuccess : (reply : GameResultPacket<?>) => void,
        onFailure : CommsFailureHandler
    );

    /**
     * Sends a single bonus selection request to the server. Some implementations of the
     * Services Api require this operation to be sent after every selection in the bonus
     * phase (eg: the V1 platform). Other implementations do not, but will implement the
     * method as part of their public API (to allow for simple cross compatibility for
     * Game Clients which must support more than 1 platform). If you know that your game
     * client is only going to be deployed for a platform that does not require this
     * operation, then you can safely ignore it.
     * @param selectionId
     * The id of the selection that has just been made.
     * @param onSuccess 
     * A callback that will be invoked when the server acknowledges the selection request.
     * @param onFailure 
     * A failure handler that will be invoked if the service fails for any reason.
     */
    sendBonusPhaseSelectionRequest(
        selectionId : number,
        onSuccess : () => void,
        onFailure : CommsFailureHandler
    );

    /**
     * Sends a Bonus Selection request with 1 or more selectsion to the server. Some
     * implementations of the Services Api require this operation to be sent after every
     * selection in the bonus phase (eg: the V1 platform). Other implementations do not,
     * but will implement the method as part of their public API (to allow for simple
     * cross compatibility for Game Clients which must support more than 1 platform). If
     * you know that your game client is only going to be deployed for a platform that
     * does not require this operation, then you can safely ignore it.
     * @param selectionIds
     * The ordered list of selections that have been made since the last selection
     * request was sent to the server. This should have at least 1 item present.
     * @param onSuccess 
     * A callback that will be invoked when the server acknowledges the selection request.
     * @param onFailure 
     * A failure handler that will be invoked if the service fails for any reason.
     */
    sendBonusPhaseMultipleSelectionRequest(
        selectionIds : number[],
        onSuccess : () => void,
        onFailure : CommsFailureHandler
    );

    /**
     * 
     * @param onSuccess 
     * @param onFailure
     * Comms Failure Handler, invoked in the case that the service fails due
     * to some kind of comms error, or the server returns a response packet that has an
     * error field (ie: the server refused to execute the service). If a failure handler
     * is not supplied, the service will default to throwing a Fatal Error event.
     */
    sendBonusPhaseCompleteRequest(
        onSuccess : () => void,
        onFailure : CommsFailureHandler
    );

    /**
     * 
     * @param onSuccess 
     * @param onFailure
     * Comms Failure Handler, invoked in the case that the service fails due
     * to some kind of comms error, or the server returns a response packet that has an
     * error field (ie: the server refused to execute the service). If a failure handler
     * is not supplied, the service will default to throwing a Fatal Error event.
     */
    sendFreeSpinPhaseResultsRequest(
        onSuccess : (reply : GameResultPacket<FreeSpinPhaseResult>) => void,
        onFailure : CommsFailureHandler
    );

    /**
     * 
     * @param onSuccess 
     * @param onFailure
     * Comms Failure Handler, invoked in the case that the service fails due
     * to some kind of comms error, or the server returns a response packet that has an
     * error field (ie: the server refused to execute the service). If a failure handler
     * is not supplied, the service will default to throwing a Fatal Error event.
     */
    sendFreeSpinPhaseCompleteRequest(
        onSuccess : () => void,
        onFailure : CommsFailureHandler
    );

    /**
     * 
     * @param onSuccess 
     * @param onFailure
     * Comms Failure Handler, invoked in the case that the service fails due
     * to some kind of comms error, or the server returns a response packet that has an
     * error field (ie: the server refused to execute the service). If a failure handler
     * is not supplied, the service will default to throwing a Fatal Error event.
     */
    sendGamblePhaseResultsRequest(
        onSuccess : (reply:Gen2GamblePhaseResultsReply) => void,
        onFailure: CommsFailureHandler
    );

    /**
     * @param onFailure
     * Comms Failure Handler, invoked in the case that the service fails due
     * to some kind of comms error, or the server returns a response packet that has an
     * error field (ie: the server refused to execute the service). If a failure handler
     * is not supplied, the service will default to throwing a Fatal Error event.
     */
    sendGamblePhaseCompleteRequest(
        onSuccess : () => void,
        onFailure : CommsFailureHandler
    );

    /**
     * @param onFailure
     * Comms Failure Handler, invoked in the case that the service fails due
     * to some kind of comms error, or the server returns a response packet that has an
     * error field (ie: the server refused to execute the service). If a failure handler
     * is not supplied, the service will default to throwing a Fatal Error event.
     */
    sendQuickPhaseCompleteRequest(
        onSuccess : () => void,
        onFailure : CommsFailureHandler
    )
}

/**
 * A Request Property Injector, is a plugin for a services implementation: it allows us to
 * add arbitrary additional properties into a request object. In some contexts, we may whish
 * to use a standard services implementation, but we need to inject 1 or more additional
 * properties into the request object (or specify some custom headers in the outgoing request).
 * A property injector instance is simply a list of injection hooks, allowing us to add this
 * extra data, while still using a standard services implementation.
 */
interface RequestPropertyInjector
{
    /**
     * Returns an object with additional headers to be added to the request. The
     * CustomXmlHttpHeaders object describes which headers may be overriden with
     * this method.
     */
    getRequestHeaderFields ? : () => CustomXmlHttpHeaders;

    /**
     * Inject properties into all request objects.
     */
    injectCommonProperties ? : (request) => void;
}

interface DownloadStatisticsData
{
    playerNickName : string;
}

/**
 * Balance Data, returned by the Check Balance Service.
 */
interface BalanceData
{
    /**
     * The amount of credit available on the player's account.
     */
    accountBalance : number;

    /**
     * The maximum amount the player may transfer.
     */
    maxTransfer : number;

    /**
     * The maximum amount a player may transfer into a session.
     */
    maxTransferInSession : number;
}

/**
 * Additional parameters which may be sent when a new game or sub-bet is started.
 */
interface GameStartParams
{
    /**
     * Indicates the number of promo-games available to the player. When a new promo-games
     * session is started, the player may have had the choice of how many rounds and what
     * bet settings - so the Services module doesn't know how many promogames are available
     * (we must tell it when the new promo-games sub-bet starts).
     */
    numPromoGames ? : number;
}