interface SimpleShakeConfig
{
    /**
     * The duration of the shake, in seconds.
     */
    duration : number;

    /**
     * Delay before the shake should start, in seconds.
     */
    delay : number;

    /**
     * The amount that the shake should move by, on x axis.
     */
    xAmplitude : number;

    /**
     * The amount that the shake should move by, on y axis.
     */
    yAmplitude : number;

    /**
     * "Multiplier for the amplitude on each update"
     * - A value of 1, means that the shake is stable over each update.
     * - A value greater than 1, means that the shake will get larger on each update!!
     * - A value less than 1, means that the shake will get smaller on each update.
     */
    amplitudeMultiplier : number;
}