/**
 * A Shuffler is any implementation of an algorithm which can randomly shuffle the
 * order of elements in an array.
 */
interface Shuffler
{
    /**
     * Randomly shuffles the data in an array. No new array is created: the array passed
     * to param data will have the ordering of its elements altered, using some kind of
     * pseduo-random technique.
     * 
     * All implementations of Shuffler must guarantee that after shuffling, the array will
     * still meet the following conditions:
     * 1) It will have exactly the same number of elements in as before
     * 2) The exact same elements will all be present in the altered array: all that changes
     *    is the ordering.
     * 
     * @param data 
     * The array of data that needs to be shuffled.
     */
    shuffleArray(data : *[]) : void;
}