/**
 * Defines a set of symbols shown on a reel-band.
 * - Outer index represents the hand in use. For single hand games,
 *   we will still have this array (but there will only be one child
 *   item in it).
 * - Middle index represents reel-band on the hand.
 * - Innter index represents each visible position on the reel-band.
 * @todo:
 * This could actually be a list of SymbolConfig objects instead.
 * This means we would already have all of the special data about the
 * symbol in question.
 */
type SymbolsMatrix = SymbolConfig[][][];

type SymbolType = string;

/**
 * Defines information about a Symbol for a game.
 */
interface SymbolConfig
{
    /**
     * The numerical id of the symbol.
     */
    id : number;

    /**
     * String name of the symbol.
     */
    name : string;

    /**
     * Indicates the special type of the symbol.
     */
    type : SymbolType;
}

/**
 * Configuration for a single Winline.
 */
interface WinlineConfig
{
    /**
     * The numerical id of the winline.
     */
    id : number;

    /**
     * The basic position offsets for the whole winline. These should be "indexed": 0
     * refers to the top visible position, 1 refers to the second visible position (etc).
     */
    positions : number[];

    /**
     * Specifies the index of the target reel, at which any numerical Win Value will be
     * shown, when a win on this line is highlighted. If this value is not specified,
     * then the win will be horizontally centered within the reels area. If this value
     * IS specified, then it represents the left to right index of the reel, on which
     * the Win Value will be placed (for example, a value of 2, places the Win Value
     * horizontally half the way through the 3rd reel from the left).
     */
    targetReelIndexForWin ? : number;

    /**
     * Specifies the index of the target symbol position, at which any numberical Win
     * Value will be shown, when a win on this line is highlighted. If this value is
     * not specified, then the win will be vertically centered within the reels area.
     * If this value IS specified, then it represents the top to bottom index of a
     * visible symbol, over which the Win Value will be placed (for example, a value
     * of 1, places the Win Value vertically half the way through the 2nd symbol from
     * the top).
     */
    targetPosIndexForWin ? : number;
}

/**
 * Configuration object, which defines how the slot phase for a game will behave.
 */
interface SlotConfig
{
    /**
     * Game Client Id: we must send this to the server, so that it knows which maths
     * module to connect the game client to.
     */
    gameId : string;

    /**
     * Indicates if this is a 2 spin game.
     */
    isTwoSpin : boolean;

    /**
     * Indicates if this is a quick spin game
     */
    isQuickSpin : boolean;

    
    /**
     * The holds mode that is active in the game client.
     */
    holdsMode : "holdReels" | "holdSymbols";

    /**
     * The maximum number of hands that exist in the game.
     */
    numHands : number;

    /**
     * The number of reels for each hand in the game. At the moment, we assume this to
     * be a fixed value (we will never have hands with different numbers of reels!!)
     */
    numReels : number;

    /**
     * Indicates how many symbols are on each reel-band. This field should always be defined.
     * When "numSymsPerReelInData" is NOT defined, then this field represents 2 things:
     * 1) the number of symbols per reel, which will be shown visually on the reelbands (this
     *    is only really releveant when using the default, configurable library ReelsGroup).
     *    This field is used to configure visual aspects, including automatic layout code.
     * 2) The number of symbols per reel, expected in data returned from the server
     * 
     * When "numSymsPerReelInData" is defined, then this field only represents the visual aspect,
     * eg: our server may return more symbols,
     */
    numSymsPerReel : number[];

    /**
     * When defined, this overrides "numSymsPerReel" in the data parsing layer : this would
     * indicate that the server may return a different number of symbols per reel than is shown
     * to the player (we would generally expect this to be more). We only expect to use this
     * field in special cases.
     */
    numSymsPerReelInData ? : number[];

    /**
     * Associated mapping between symbol id (as a number) and a symbol configuration.
     */
    symbols : { [id : number] : SymbolConfig };

    /**
     * Defines information about all of the available reelbands - used only for V1 build
     * of the game client (as v2 server side )
     * - Outer array is "reelIndex"
     * - Inner array is "posIndex"
     * If this field is present, then it indicates the game is using fixed reel-bands.
     * In this event, the server will be returning a set of reel positions in spin results.
     * The reel-bands information is needed by the client, in order to generate the correct
     * list of symbols to show at the end of a spin. Otherwise, if the field is not define,
     * it indicates that all symbols are returned separately in the spin results.
     */
    reelbands ? : number[][];

    /**
     * The list of data for all winlines.
     */
    winlines : { [id:number] : WinlineConfig };
    
    /**
     * Configuration for how the player may change bet settings.
     */
    betConfig : BetSettingsConfig;

    /**
     * Default set of bet settings for the game.
     */
    defaultBet ? : SlotBetSettings;
}

type SlotReelBacksDisplayMode = "displayHeldOnly" | "displayHeldAndNotHeld";

/**
 * Configuration for the standard "Reel Backs" Slot View component. Allows us to configure
 * some visual settings for the ReelBacks, as well as sounds triggered when the player changes
 * holds on a two spin game.
 */
interface SlotReelBacksConfig
{
    /**
     * Indicates if keyboard input should be allowed for toggling holds pattern. By default,
     * we use the top numerical buttons on the keyboard to toggle reels. This is fine, for cases
     * where there are only a limited number of reels (eg: 5), but potentially might not work in
     * some future use cases.
     */
    allowKeyboardInput : boolean;

    /**
     * Indicates the mechanism to use for displaying holds on the reel-backs (as well as "non-holds").
     * 
     * If set to "displayHeldOnly", then when a reel is not held (at ANY point that it is visible on 
     * screen), no visible state is shown for the reel within the reel-backs layer (meaning any layer
     * that is below the reel will be visible to the player): when the reel changes to held, the held
     * state is shown.
     * 
     * When set to "displayHeldAndNotHeld", then a visual state is shown for both HELD and NOT_HELD
     * states, at all times: the reel-back serves as a visual state for the whole reel.
     * 
     * The default behavious is to show state for both HELD and NOT_HELD, so you will need to set
     * this flag to false to override this.
     */
    displayMode : SlotReelBacksDisplayMode;

    /**
     * Indicates if an animated cross-fade should be applied when a reel visually changes state from
     * not-held to held (and vice-versa). If set to true, a short default time is applied for the
     * cross-fade. If set to false, the reel changes state immediately.
     */
    useCrossFade : boolean;

    /**
     * Configures sounds used when holds patterns change. By default, no sound is configured,
     * as this is a Two Spin game feature, and the sound mechanism is likely to be different
     * for each game.
     */
    toggleHoldsSounds ? :
    {
        /**
         * Id of an optional sound to play when holds input is first enabled.
         */
        holdsEnabledSound ? : string;

        /**
         * Specifies the id of the "reel-held" sound which should be used for each reel.
         * This sound will be triggered whenever an individual reel is pressed, and its state
         * is changed to "held" (or when the corresponding key-board button is pressed, with
         * the same outcome). This is ordered from left to right - index 0 is the left
         * most reel, and so on.
         */
        heldSoundsPerReel : string[];

        /**
         * Specifies the id of the "reel not held" sound which should be used for each reel.
         * This sound will be triggered whenever an individual reel is pressed, and its state
         * is changed to "not-held" (or when the corresponding key-board button is pressed,
         * with the same outcome). This is ordered from left to right - index 0 is the left
         * most reel, and so on.
         */
        unheldSoundsPerReel : string[];
    }
}

/**
 * Configuration settings for general spin mechanics.
 */
interface SlotSpinConfig
{
    /**
     * Duration (in seconds) that a single Symbol Win will be shown for, during the main
     * win presentation.
     */
    symbolWinDuration : number;

    /**
     * Duration (in seconds) that the scatter win presentation should last for. This is a
     * special win presentation, shown before triggering the Bonus Phase.
     */
    scatterWinDuration : number;

    /**
     * Duration (in seconds) that a single Symbol Win presentation will be shown for, during
     * the idle win presentation.
     */
    idleSymbolWinDuration : number;

    /**
     * Default configuration settings. These settings are always applied to Spin 1 phase,
     * and will also be applied to Spin 2 / FreeSpin, unless the separate Spin2 / FreeSpin
     * configuration fields are specified.
     */
    spinTimings : SlotSpinPhaseTimingsConfig;

    /**
     * Optional configuration settings specifically for Spin 2. If not supplied, then the
     * values from the default spinTimings field will be used on Spin2.
     */
    spin2Timings ? : SlotSpinPhaseTimingsConfig;

    /**
     * Optional configuration settings specifically for FreeSpin. If not supplied, then the
     * values from the default spinTimings field will be used on FreeSpins.
     */
    freeSpinTimings ? : SlotSpinPhaseTimingsConfig;

    // TODO: Add the possibility to configure independently for all spins ?
    /**
     * Default slot spin shake config. If configured, this will be applied to all spins.
     */
    shake ? : SlotSpinShakeConfig;
}

/**
 * Intersection type, for configuring Spin Phase Timings. The value of the "type" field
 * indicates which possible configuration is being specified.
 */
type SlotSpinPhaseTimingsConfig = SimpleSlotSpinPhaseTimingsConfig;

/**
 * Simplest form of Slot Spin Phase timings config: we can set a singular set of timings,
 * applied to all respins and rounds.
 */
interface SimpleSlotSpinPhaseTimingsConfig extends SlotSpinPhaseTimings
{
    type : "simple";
}

/**
 * Actual timings configuration for any given Slot Spin Phase.
 */
interface SlotSpinPhaseTimings
{
    /**
     * Delay (in seconds) to use for this spin phase, between each successive reel starting to spin.
     */
    reelStartDelayTime : number;

    /**
     * Delay (in seconds) to use for this spin phase, between each successive reel coming to a halt.
     * For every reel that is not teased, this is the standard timing that is used to stop a single
     * reel. If no reels are teased, then total delay is (NumReels - 1) * reelStopDelayTime.
     */
    reelStopDelayTime : number;

    /**
     * Delay (in seconds) to use for this spin phase, for stopping a reel which is being teased.
     * All subsequent reels that are stopped implicitly inherit this delay, even if subsequent reels
     * being stopped used the standard delay time.
     * 
     * Here is an example use case:
     * - reelStopDelayTime = 0.2 seconds
     * - teaseReelStopDelayTime = 0.5 seconds
     * - there are 5 reels ([0,1,2,3,4])
     * - reels [2,4] are being teased
     * 
     * Here is what will happen:
     * - reel 0 will stop with a delay of 0.0 seconds after "stop commencing"
     * - reel 1 will stop with a delay of 0.2 seconds after reel 0 stops (and 0.2 seconds after "stop commencing")
     * - reel 2 will stop with a delay of 0.5 seconds after reel 1 stops (and 0.7 seconds after "stop commencing")
     * - reel 3 will stop with a delay of 0.2 seconds after reel 2 stops (and 0.9 seconds after "stop commencing")
     * - reel 4 will stop with a delay of 0.5 seconds after reel 3 stops (and 1.4 seconds after "stop commencing")
     */
    teaseReelStopDelayTime : number;
    
    /**
     * The minimum total time (in seconds) that must elapse, before its possible to stop the spin.
     * If this value is shorter than the total time implied by reelStartDelayTime (and the number
     * of reels), then the spin still cannot be stopped until all reels have started spinning. If
     * it is longer, then this is the time that must elapse before the spin can be interrupted.
     * PLEASE NOTE: This value is not used in the event that no reels are actually going to spin
     * (eg: when all reels are held). In that scenario, a very short delay is used instead.
     */
    minSpinTime : number;
}

//--------------------------------------------------------------------------------------------------
/**
 * 
 */
interface SingleReelSpinEventData
{
    /**
     * The absolute index of the reel that something happened on.
     */
    reelIndex : number;

    /**
     * The progressive index of the reel that something happened on: this is relative
     * to the number of reels spinning, and is meaningful for a Two Spin game. EG: if we
     * have 5 reels, but only spin reels [0,1,1,0,1], then reel index 2 - would be reel
     * progressive index 1.
     */
    reelProgressiveIndex : number;

    /**
     * The total number of reels involved in the spin (especially relevant for a two spin
     * game, where it may not always be the full number of reels). This will always refer
     * to the actual number of reels meant to spin - even if not all have started spinning
     * yet (or some have stopped).
     */
    numReelsInvolvedInSpin : number;
}

//--------------------------------------------------------------------------------------------------
// Configuration for the "shake" feature, which can be applied at the end of spins (and potentially
// before)
//--------------------------------------------------------------------------------------------------
/**
 * Union type, to define a "triggering policy" for spin shake: this simply means the point
 * at which the shake is triggered. We might want it triggered when all reels have stopped
 * spinning, we might want it triggered on a specific reel index (if we know that all reels
 * will be spinning), we might want it triggered on a progressive index (if this is a two
 * spin game, on spin 2, we may not know exactly how many reels will be spinning): this
 * can be expanded in the future with new triggering policies.
 */
type SlotSpinShakeTriggeringPolicy =
    TriggerOnReelIndexSlotSpinShakeTriggeringPolicy |
    TriggerOnReelProgressiveSlotSpinShakeTriggeringPolicy |
    TriggerOnFinalReelSlotSpinShakeTriggeringPolicy |
    TriggerOnAllReelsStoppedSlotSpinShakeTriggeringPolicy;

/**
 * For some spin effects (eg: Spin Shake) that are linked to the spins of a specific reel,
 * we may trigger our effect at a specific moment in the spin life-cycle. This union type
 * gives us the 3 main steps
 * - infiniteSpinStarted (when the spin first starts)
 * - infiniteSpinEnd (when we transition from infinite spin to "stopping")
 * - reelStopped (when the reel has come to a rest)
 */
type ReelSpinTriggerEvent = "infiniteSpinStart" | "infiniteSpinEnd" | "reelStopped";

/**
 * Spin Shake Triggering policy, indicating to shake when a specific reel index stops.
 */
interface TriggerOnReelIndexSlotSpinShakeTriggeringPolicy
{
    trigger : "onReel";

    /**
     * The index of the reel which should trigger the shake.
     */
    triggerReelIndex : number;

    /**
     * Indicates what moment for the reel that the shake gets triggered on
     */
    bindToReelEvent : ReelSpinTriggerEvent;
}

/**
 * Trigger on a progressive reel index (good for 2 spin games, at least for spin 2)
 */
interface TriggerOnReelProgressiveSlotSpinShakeTriggeringPolicy
{
    trigger : "onReelProgressive";

    /**
     * The target progressive index on which the shake should be triggered (when that reel
     * stops spinning). If fewer reels than this are spinning, then the shake will be triggered
     * when all reels have stopped spinning.
     */
    triggerProgressiveReelIndex : number;

    /**
     * Indicates what moment for the reel that the shake gets triggered on
     */
    bindToReelEvent : ReelSpinTriggerEvent;
}

/**
 * Trigger on the very final reel (whatever that may be) of the spin phase.
 */
interface TriggerOnFinalReelSlotSpinShakeTriggeringPolicy
{
    trigger : "onFinalReel";

    /**
     * Indicates what moment for the reel that the shake gets triggered on
     */
    bindToReelEvent : ReelSpinTriggerEvent;
}

/**
 * Spin Shake triggering policy, indicating to shake when all reels have stopped.
 */
interface TriggerOnAllReelsStoppedSlotSpinShakeTriggeringPolicy
{
    trigger : "onAllReelsStopped";
}

// TODO: current typescript doesnt seem to like extending from intersection types
// (or doesnt like this when there are multiple type extensions). However, this
// is working right now (13.11.2020): cannot tell yet if this is a bug in recent
// typescript (I would swear the extension method used to work), or is actually
// expected with current typescript standards.
/**
 * Configures the standard "Simple" shake of the reels.
 */
type SimpleSlotSpinShakeConfig =
    { type : "simple" } &
    SlotSpinShakeTriggeringPolicy &
    SimpleShakeConfig;

/**
 * Indicates
 */
interface NoSlotSpinShakeConfig
{
    type : "none";
}

type SlotSpinShakeConfig = SimpleSlotSpinShakeConfig | NoSlotSpinShakeConfig;

//--------------------------------------------------------------------------------------------------
// 
//--------------------------------------------------------------------------------------------------
/**
 * Used to dinstinguish basic Type of a SpinPhase (which a "Spin" visualization belongs
 * to).
 * 1 == Spin1 or SingleSpin
 * 2 == Spin2
 * 3 == FreeSpin
 * 
 * Useful as a flag to pass to methods which trigger Spin Animations - we may want to
 * let our spin animations do different things, according to whether we are currently
 * showing a spin as part of Spin1 (SingleSpin) / Spin2 / FreeSpin
 */
type SpinPhaseType = 1 | 2 | 3;

/**
 * Defines configuration settings for the Spin Indicator, for a single device target (eg: mobile /
 * tablet / desktop)
 */
interface SpinIndicatorConfig
{
    /**
     * The x position of the Spin Indicator (in pixels), within the coordinate space of reels group.
     */
    posX : number;

    /**
     * The y position of the Spin Indicator (in pixels), within the coordinate space of reels group.
     */
    posY : number;

    /**
     * The height of the Spin Indicator (in pixels), within the coordinate space of reels group.
     */
    height : number;
}

interface SkipWinPresentationConfig
{
    /**
     * Indicates if the "Skip Win Presentation" functionality is enabled for this game client.
     */
    enabled : boolean;

    /**
     * Indicates if skipping Big Win is allowed.
     */
    allowSkipBigWin : boolean;
}