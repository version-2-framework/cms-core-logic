//--------------------------------------------------------------------------------------------------
// Definitions for the common results model used throughout our Game Clients.
//--------------------------------------------------------------------------------------------------
// The result models here are common to all games (so we exclude anything Bonus related),
// and represent PARSED results. The slot result model closely matches the Generation 2 maths
// result model, but we do a bit of pre-parsing and organizing of the data returned (and for
// clients using Generation 1 maths, we use additional parsers, to convert the old server results
// into the same result model as is presented here).

import { P } from "../../../cms-core-view/src/input/KeyCode";

//--------------------------------------------------------------------------------------------------
interface HoldReelsPattern
{
    type : "holdReels";

    /**
     * The pattern applied for all hands. Has the same length as num reels, and each
     * boolean indicates if that reel is held or not.
     */
    pattern : boolean[][];
};

interface HoldSymbolsPattern
{
    type : "holdSymbols";

    /**
     * The pattern applied for all hands. Outer array represents reels, inner array
     * represents a symbol for each position available.
     */
    pattern : boolean[][][];
}

/**
 * The different types of Holds Patterns available.
 */
type HoldsPattern = HoldReelsPattern | HoldSymbolsPattern;

interface SlotWin
{
    /**
     * Winnings group this reward is assigned to (credit, superbet etc)
     */
    rewardGroup : number;

    /**
     * Type of reward given on this Slot Win.
     */
    rewardType : number;

    /**
     * Value of Reward given on this Slot Win.
     */
    rewardValue : number;
}

/**
 * Expanded version of Winning Result.
 */
interface WinningResult
{
    /**
     * The total Credit winnings for this Spin (in points).
     */
    totalCreditWon : number;

    /**
     * The total SuperBet winnings for this Spin (in points).
     */
    totalSuperBetWon : number;

    /**
     * The number of FreeGames won for this Spin.
     */
    numFreeGamesWon : number;
}

/**
 * A basic interface, describing winnings data we expect to be
 * present for an overall bonus phase result. This can also be
 * used as a proxy parameter type, when parsing bonus results
 * returned by the server.
 */
interface BonusWinningsData extends WinningResult
{
    numFreeSpinsWon : number;

    multiplier : number;
}

/**
 * A SpecialWin is a SlotWin (but not awarded amongst the Symbols)
 */
interface SpecialWin extends SlotWin
{
    winType : number;
}

/**
 * A SymbolWin is a SlotWin awarded with a set of Symbols.
 */
interface SymbolWin extends SlotWin
{
    /**
     * The id of the hand that this win is on.
     */
    handId : number;

    /**
     * The id of the winline that this single hand win is on.
     */
    lineId : number;

    /**
     * The id of the winnng symbol for this winline.
     * @todo
     * It is possible, one day, that this simple value might not be very informative
     * (if this object is supposed to represent something REALLY complicated as a win)
     */
    winningSymbol : SymbolConfig;
    
    /**
     * A map of all winning positions for the hand.
     * @todo: We could expand this to be a map of objects
     */
    winningPositionsMap : boolean[][];

    /**
     * The list of all winning reels. This array has the same length as "winningPositions".
     * Each item is a reel id ( eg: 1 to 5 for a 5 reel game ). The corresponding index in
     * "winningPositions" contains the position of the symbol that is winning.
     * 
     * So, if we have a single symbol winning on reels 3, 4 & 5 ( and nothing on reels
     * 1 & 2 ), this array will have the value [3,4,5].
     * 
     * If we ever have multiple winning symbols to be highlighted on a single reel (for a
     * single win), then this array would contain multiple instances of the same reel id.
     * EG: if the first 2 symbols on reel 1 are winning ( and we have 1 winning symbol on
     * each of reels 2 & 3 ), then this array would be [1,1,2,3]. The corresponding indices
     * in "winningSymbols" will still contain the position of the symbol that is winning,
     * for each case (in the 2nd example, "winningSymbols" might be [1,2,2,2]).
     */
    winningReels : number[];

    /**
     * The position for each reel ( listed in winning reels ) that is involved in the win.
     * This field has the same length as "winningReels", and each index corresponds to the
     * same index in "winningReels". The values in this array, are "position ids": if there
     * are 3 symbols visible for each reel, then the possible values are 1,2,3.
     * 
     * If multiple positions on a single reel are marked as winning for this win, then the
     * reel id will appear multiple times in "winningReels", and each corresponding index
     * in "winningSymbols" will indicate the position for the win. eg:
     * winningReels:   [ 2, 3, 3, 3, 4, 4, 5, 5, 5 ]
     * winningSymbols: [ 2, 1, 2, 3, 2, 3, 1, 2, 3 ]
     * would indicate that symbol 2 on reel 2 is winning, symbols 1,2,3 on reel 3 are winning,
     * symbols 2,3 on reel 4 are winning, and symbols 1,2,3 on reel 5 are winning ( there is
     * no winning symbol on reel 1! )
     */
    winningPositions : number[];

    /**
     * Indicates the number of unique reelbands with a win on them. For a game with 5 reels,
     * then a value of 5 here would indicate that each reel has a winning symbol in this win.
     * In a game with multiple winning symbols on a single reel, that reel only contributes
     * "1" to this value.
     * 
     * Historically, this was "numWinningSymbols", but as we are introducing the idea that a
     * WinlineWin might have several symbols on 1 reel that contribute to the win, the name
     * needed to change.
     */
    //numWinningReels : number;

    /**
     * Mechanic of the win (eg: winlines, scatter, all ways). The constants will be fixed,
     * but are not strongly defined yet
     */
    winMechanic : number;

    /**
     * Indicates the id of any logical "group of wins" that this symbol win belongs to.
     * The meaning is game specific.
     */
    winGroup : number;
 
    /**
     * The "size" of the win, changes according to win mechanic, but usually has the general
     * meaning of "number of consecutive symbols". EG: if this was a winline win, with 5 symbols
     * in a row left to right, then "winSize" would be 5. WinningPositions cannot indicate this,
     * because the symbol win might represent more than 1 win (eg: in the case of AllWaysWins)
     */
    winSize : number;
 
    /**
     * Number of wins indicated by this SymbolWin. Some winMechanics (eg: AllWaysWin) will indicate
     * more than 1 win - this number indicates the value. For a regular winline win, you would
     * normally expect this to hold a value of 1 (unless the maths is using some special mechanic)
     */
    numWins : number;
     
    /**
     * A list of special symbols to highlight for the win
     */
    specialSymbols : SpecialSymbol[];
}

interface SpecialSymbol
{
    position : number;
    type : number;
    value : number;
}

// TODO: we could possibly drop this altogether, if we wanted to let the View do
// its own decisions about when to show different result simulations.
interface MultiHandSymbolWin extends SlotWin
{
    /**
     * The id of the winline that this win is on. (Will be 0 for cases where the
     * win is not on any winline).
     */
    lineId : number;
    
    /**
     * The wins on each individual hand. For a single hand game, this will be
     * an array with a single element, so you can simply access winPerHand[0]
     * to get the additional required information. For a multi-hand game, this
     * will contain a list describing the wins on each individual hand, but for
     * a single winline (for a multi hand game, we will show all wins for a
     * single winline simultaneously: however, the wins on the same winline on
     * each hand, could be different, and not all hands may have a win on that
     * winline).
     */
    forHand : { [id:number] : SymbolWin }
}


/**
 * Interface for the quick spin phase win amounts
 */
interface quickSpinPhaseWinAmount 
{
    /**
     * The prize value of the selection
     */
    winValue: number;

    /**
     *  The index of the position of the prize. This is normally 0-5
     */
    winPosition: number
}

/**
 * The common results model to be consumed by the Slot View classes.
 * Initially, this is a copy of the old "SpinModel" api - but without
 * any getter methods (just plain old data). If we then need to start
 * cutting away data, we can do that.
 */
interface SpinResult extends WinningResult
{
    /**
     * Returns the symbols to show at the end of the most recent spin.
     */
    finalSymbolIds : SymbolsMatrix;

    /**
     * Map of all symbols, indicating any that are sticky.
     */
    stickySymbolsMap : boolean[][][];

    /**
     * Indicates if there any wins to show this spin (no matter the kind)
     */
    hasAnyWin : boolean;

    /**
     * Total number of FreeSpins won for this Spin Result.
     */
    numFreeSpinsWon : number;

    /**
     * The progressive miltiplier attached to this Spin.
     */
    progressiveMultiplier : number;

    /**
     * Type of Spin that this was, eg: "normal", "sticky respin", "transform respin"
     */
    spinType : number;

    /**
     * All symbol wins to show for this Spin Result. If no Symbol Wins are involved
     * in the win, then this will be an empty list.
     */
    symbolWins : SymbolWin[];

    /**
     * All SpecialWins awarded on this Spin Result.
     */
    specialWins : SpecialWin[];

    /**
     * Indicates if any Bonus Win was present in these results.
     */
    hasBonusWin : boolean;
}

/**
 * A SpinRound groups several SpinResults together. A standard SingleSpin / Spin 1 /
 * Spin 2 will only have a single Round (which may consist of multiple Spin Results).
 * A FreeSpin Phase will usually have multiple Spin Rounds (each of which may still
 * consist of multiple Spin Results).
 */
interface SpinRoundResult extends WinningResult
{
    /**
     * Ordered list of Spin Results for this Spin Round.
     */
    spins : SpinResult[];
}

/**
 * Common base class of all Phase results.
 */
interface GamePhaseResult extends WinningResult
{

}

interface SlotGamePhaseResult extends GamePhaseResult
{

}

/**
 * Common data for a Spin Phase Result.
 */
interface SpinPhaseResult extends SlotGamePhaseResult
{
    /**
     * Ordered list of rounds for this Spin Phase.
     */
    rounds : SpinRoundResult[];

    /**
     * The total number of FreeSpins won from this Spin Phase Result.
     */
    numFreeSpinsWon : number;

    /**
     * Indicates if there should be a Bonus round after this Spin Phase Result.
     */
    hasBonusWin : boolean;

    /**
     * The ids of all hands that have a bonus win.
     */
    handsWithBonusWin : number[];

    /**
     * Indicates, per hand, if a bonus win is present.
     */
    hasBonusWinPerHand : boolean[];
}

/**
 * Single Spin Phase result
 */
interface SingleSpinPhaseResult extends SpinPhaseResult
{
    
}

/**
 * Spin 1 Phase result
 */
interface Spin1PhaseResult extends SpinPhaseResult
{
    /**
     * Holds pattern suggested for spin 2.
     */
    suggestedAutoholds : HoldsPattern;

    /**
     * Indicates if a Spin 2 is required.
     */
    playSpin2 : boolean;
}

/**
 * Spin 2 Phase Result
 */
interface Spin2PhaseResult extends SpinPhaseResult
{
    /**
     * Symbols at the start of this Spin 2 phase.
     */
    startSymbolIds : SymbolsMatrix;

    /**
     * The holds pattern used for Spin 2.
     */
    holdsUsed : HoldsPattern;
}

/**
 * FreeSpin Phase Result
 */
interface FreeSpinPhaseResult extends SlotGamePhaseResult
{
    /**
     * Type of FreeSpin phase that was triggered.
     */
    freeSpinType : number;

    /**
     * Ordered list of rounds for this Spin Phase.
     */
    rounds : SpinRoundResult[];
}

/**
 * Interface for the quick spin phase result
 */
interface QuickPhaseResult extends SlotGamePhaseResult {

    /**
     * List of results for the Quick Spin Phase Result
     */
     winAmounts: winAmount[];

     /**
     * Indicates if there should be a Bonus round after the quick phase
     */
    hasBonusWin : boolean;

    /**
     * The ids of all hands that have a bonus win.
     */
    handsWithBonusWin : number[];
}

/**
 * Interface for the quick phase win amount 
 */
interface winAmount {

    winValue : number;

    winPosition: number;

}



//--------------------------------------------------------------------------------------------------
// Phase Result model interfaces.
//--------------------------------------------------------------------------------------------------
/**
 * Standard interface for a Game Phase Model.
 */
interface GamePhaseModel
{
    /**
     * Indicates if this is a reloaded Phase.
     */
    getIsReloadedPhase() : boolean;

    /**
     * Returns the total Credit won for the whole phase.
     */
    getTotalCreditWon() : number;

    /**
     * Returns the total SuperBet won for the whole phase.
     */
    getTotalSuperBetWon() : number;

    /**
     * Returns the total number of FreeGames won for the whole phase.
     */
    getNumFreeGamesWon() : number;

    /**
     * Returns the total number of FreeSpins won for the whole phase.
     */
    getNumFreeSpinsWon() : number;
}

/**
 * Interface for the Spin Phase Model.
 */
interface SpinPhaseModel extends GamePhaseModel
{
    /**
     * Indicates if the current spin phase is a Single Spin phase.
     */
    getIsSingleSpinPhase() : boolean;

    /**
     * Indicates if the current spin phase is a Spin 1 phase.
     */
    getIsSpin1Phase() : boolean;

    /**
     * Indicates if the current spin phase is a Spin 2 phase.
     */
    getIsSpin2Phase() : boolean;

    /**
     * Indicates if the current spin phase is a FreeSpin phase.
     */
    getIsFreeSpinPhase() : boolean;

    /**
     * Returns the Symbol Ids to show, at the start of the current Spin Phase.
     */
    getStartSymbolIds() : SymbolsMatrix;

    /**
     * Returns the Symbol Ids to show, at the end of the current Spin Phase.
     */
    getFinalSymbolIds() : SymbolsMatrix;

    /**
     * Returns the current value of player holds pattern. When Spin 1 results are
     * returned, this will be initialized to the same value as the auto-holds pattern
     * returned by the server. The player may change the value of holds used: the
     * value of this field will therefore be changed (and the new value will be sent
     * to the server when we request Spin 2 results). The player holds pattern is also
     * returned in Spin 2 results (alongside autoholds pattern), so in the event of
     * game restore, we can show the actual holds pattern applied for second spin.
     * 
     * Because we use the same holds pattern applied across all hands (for a multi
     * hand game), this pattern is only as long as the number of reelbands that have
     * per hand.
     */
    getPlayerHoldsPattern() : HoldsPattern;

    /**
     * Explicitly sets the hold state for a given hand and reel. Internally, this method
     * will handle "multi-hand" games automatically, eg: it changing holds independently
     * for each hand is allowed, this method will take care of it (otherwise, it will change
     * the hold state for all hands simultaneously, no matter the value of handIndex passed
     * in).
     * This method does not currently broadcast any event when the pattern is changed.
     * @param handIndex
     * The index of the hand to set the hold state for: for single hand games, this would
     * always be a value of 0.
     * @param reelIndex
     * The index of the reel to set the hold state for. This is always relative to the hand
     * that hold is being changed for.
     * @param shouldBeHeld
     * Indicates if the taget reel should now be held or not.
     */
    setReelHoldState(handIndex:number, reelIndex:number, shouldBeHeld:boolean) : void;

    /**
     * Toggles the current hold state for a given hand and reel. Internally, this method
     * will handle "multi-hand" games automatically, eg: it changing holds independently
     * for each hand is allowed, this method will take care of it (otherwise, it will change
     * the hold state for all hands simultaneously, no matter the value of handIndex passed
     * in).
     * This method does not currently broadcast any event when the pattern is changed.
     * @param handIndex
     * The index of the hand to set the hold state for: for single hand games, this would
     * always be a value of 0.
     * @param reelIndex
     * The index of the reel to set the hold state for. This is always relative to the hand
     * that hold is being changed for.
     */
    toggleReelHoldState(handIndex:number, reelIndex:number) : void;

    /**
     * Returns a parsed object, representing the results of this Spin Phase.
     */
    getPhaseResults() : SpinPhaseResult;

    /**
     * Indicates if a Bonus Win is present in this Spin Phase.
     */
    hasBonusWin() : boolean;

    /**
     * Indicates if a FreeSpin Win is present in this Spin Phase.
     */
    hasFreeSpinWin() : boolean;

    /**
     * Indicates if Spin 2 is required.
     */
    getIsSpin2Required() : boolean;

    /**
     * Returns the Results Presentation to show for the current Spin Phase Result.
     */
    getResultsPresentation() : SpinPhaseResultPresentation;

    /**
     * Returns a specific Spin Round Result, based on a round index. If results exist, and the given round index
     * is within the range allowed for current spin phase result, then this round will be returned. If the round
     * index is too high, then the final round of the current spin phase will be  returned. As long as the round
     * index you pass in is valid for current results, expect to get the appropriate round result back. If it's
     * not possible to return any round result, then expect to get null or undefined back.
     * @param roundIndex
     * Index of the round to fetch Spin Round Result Presentation for
     */
    getRoundResult(roundIndex:number) : SpinRoundResult;

     /**
      * Returns a specific Spin Result, based on a round index, and a spin index (relative to the specified round
      * index). If the given round index is within the range allowed for current spin phase result, then this round
      * will be read : if the round index is too high, then the final round of the current spin phase will be read.
      * The same formula is repeated for spin result (if spin index is within range for the selected round, the
      * appropriate spin result is returned: if not, the final spin result for the round is returned). As long as
      * the round index and spin index you pass in are valid for the current spin phase results, expect to get the
      * appropriate spin result back. If it's not possible to return any spin result, then expect to get null or
      * undefined back.
      * @param roundIndex
      * @param spinIndex
      */
    getSpinResult(roundIndex:number, spinIndex:number) : SpinResult;

    /**
     * Returns a specific Spin Round Result presentation, based on a round index. If results exist, and the
     * given round index is within the range allowed for current spin phase result, then this round will be
     * returned. If the round index is too high, then the final round of the current spin phase will be 
     * returned. As long as the round index you pass in is valid for current results, expect to get the
     * appropriate round presentation back. If it's not possible to return any round result, then expect to get
     * null or undefined back.
     * @param roundIndex
     * Index of the round to fetch Spin Round Result Presentation for
     */
    getRoundPresentation(roundIndex:number) : SpinRoundResultPresentation;

    /**
     * Returns a specific Spin Result presentation, based on a round index, and a spin index (relative to the
     * specified round index). If the given round index is within the range allowed for current spin phase result,
     * then this round will be read : if the round index is too high, then the final round of the current spin
     * phase will be read. The same formula is repeated for spin result (if spin index is within range for the
     * selected round, the appropriate spin result presentation is returned: if not, the final spin result
     * presentation for the round is returned). As long as the round index and spin index you pass in are valid for
     * the current spin phase results, expect to get the appropriate spin result presentation back. If it's not
     * possible to return an spin result, then expect to get null or undefined back.
     * @param roundIndex
     * @param spinIndex
     */
    getSpinPresentation(roundIndex:number, spinIndex:number) : SpinResultPresentation;
}

interface MutableSpinPhaseModel extends SpinPhaseModel
{
    /**
     * Clears any cached result.
     */
    clearCachedData () : void;

    /**
     * Clears holds pattern, resetting it to a blank state (usefull to call at the
     * end of a game).
     */
    clearHoldsPattern() : void;

    setSingleSpinResult(result : GameResultPacket<SingleSpinPhaseResult>) : void;
    setSpin1Result(result : GameResultPacket<Spin1PhaseResult>) : void;
    setSpin2Result(result : GameResultPacket<Spin2PhaseResult>) : void;
    setFreeSpinResult(resultPacket : GameResultPacket<FreeSpinPhaseResult>) : void;

    /**
     * Used when restoring from a bonus phase result : we explicitly set the last set
     * of symbol ids that the server returned with the restored bonus phase result.
     * @param symbolIds 
     */
    setFinalSymbolIdsAfterBonus(symbolIds : SymbolsMatrix) : void;
}

interface BonusPhaseModel extends GamePhaseModel
{
    /**
     * Returns the "type" of Bonus Phase this is
     * @todo: Confirm if this is the user selectable option.
     */
    getBonusType() : number;

    /**
     * Indicates if the whole Bonus Phase should be treated as a Big Win(ning) Bonus.
     */
    getIsBigWin() : boolean;
}

interface MutableBonusPhaseModel extends BonusPhaseModel
{
    setPhaseResults(result : GameResultPacket<?>) : void;
}

interface GamblePhaseModel extends GamePhaseModel
{

}

/**
 * Provides state, as well as a game result of some kind.
 */
interface GameResultPacket<Res>
{
    regulation ? : SessionInfo;

    betSettings : SlotBetSettings;

    currentWallet : number;

    winningsAmount : number;

    superbetAmount : number;

    numPromoGames : number;

    numFreeSpins : number;

    result : Res;

    isReloaded ? : boolean;
}

/**
 * Data returned for a restored game phase.
 */
interface RestoredGameResultPacket<Res> extends GameResultPacket<Res>
{
    /**
     * @inheritDoc
     */
    isReloaded : true;

    /**
     * Id of the game phase that is being restored.
     */
    restoreGamePhase : string;

    /**
     * An optional set of previous symbols data: usually present in a restored
     * bonus phase result.
     */
    previousSymbols ? : SymbolsMatrix;

    /**
     * An optional set of previous seletions data: may be present for some
     * forms of reloaded Game Result Packet, where the phase result is a Bonus
     * Phase.
     */
    previousSelections ? : number[];
}