/**
 * Defines layout parameters for the ReelGroup, for a single target screen size.
 */
interface SlotLayoutConfig
{
    // TODO: Still have very little idea how this property was meant to be used
    reelMaskExpand : number;

    /**
     * The expected width of a symbol, within "Reel-Group" space.
     */
    symWidth : number;

    /**
     * The expected height of a symbol, within "Reel-Group" space.
     */
    symHeight : number;

    // TODO: Not clear if this is an offset relative to top of reel space, or another value.
    // Semantically, it would make sense for this to be from top of reel-space: in terms of how
    // it's currently used, i think its the opposite. This is a wiener property: the idea behind
    // it is correct, but the name is poor and there was no documentation about how they intended
    // it to be used (and I am also pretty sure we haven't used it yet), so it can be refactored
    // as required.
    /**
     * Additional starting point offset, for individual reels.
     */
    reelYOrigins : number[];

    /**
     * The width of any reels separator that may be shown on reelbacks (or other equivalent default
     * components). 
     */
    reelsSeparatorWidth : number;

    /**
     * Indicates if a Reels Bg graphic should be shown.
     */
    showReelsBg : boolean;

    /**
     * X position (in pixels) of the reels bg graphic. The ReelsBg is positioned as part of
     * the statically laid out reels view component.
     */
    reelsBgPosX : number;

    /**
     * Y position (in pixels) of the reels bg graphic. The ReelsBg is positioned as part of
     * the statically laid out reels view component.
     */
    reelsBgPosY : number;

    /**
     * Width (in pixels) that the reels bg graphic should be set to. The ReelsBg is positioned as
     * part of the statically laid out reels view component.
     */
    reelsBgWidth : number;

    /**
     * Height (in pixels) that the reels bg graphic should be set to. The ReelsBg is positioned as
     * part of the statically laid out reels view component.
     */
    reelsBgHeight : number;
        
    /**
     * The total width (in pixels) that the statically laid out content is expected to be.
     * This value is used for horizontal scaling on-screen: if the actual pixel width of
     * any display objects might be larger than this (for example, some content may be pushed
     * to the left or right of our core area, or may be masked), then this value is critical
     * for measuring the intended width.
     */
    reelsWidth : number;
        
    /**
     * The total height (in pixels) that the statically laid out content is expected to be.
     * This value is used for horizontal scaling on-screen: if the actual pixel height of
     * any display objects might be larger than this (for example, some content may be pushed
     * above or below our core area, or may be masked), then this value is critical for
     * measuring the intended height.
     */
    reelsHeight : number;

    /**
     * The x position (in pixels) within the statically laid out reels content, that should be
     * treated as our anchor position. When we try and position the reelsGroup on screen, and
     * target a specific position in screenspace, this is horizontal position within reelsGroup
     * which will be placed upon that position. For example, if reelsWidth is 1600, and you want
     * the horizontal anchor to fall at 50%, then you would set a value of 800.
     */
    reelsAnchorPosX : number;

    /**
     * The y position (in pixels) within the statically laid out reels content, that should be
     * treated as our anchor position. When we try and position the reelsGroup on screen, and
     * target a specific position in screenspace, this is vertical position within reelsGroup
     * which will be placed upon that position. For example, if reelsHeight is 900, and you want
     * the vertical anchor to fall at 50%, then you would set a value of 450.
     */
    reelsAnchorPosY : number;

    /**
     * X position (in pixels) which the reels start from. This is not a coordinate in screen
     * space: instead, it is a coordinate within the Reels view from which we start drawing
     * the reels. Its primary purpose is to allow us to add some offset, dedicated for any
     * reels frame (or winline numers which we may wish to show to the left of the reels).
     * It is valid for this value to be 0.
     */
    reelsPosX : number;

    /**
     * Y position (in pixels) which the reels start from. This is not a coordinate in screen
     * space: instead, it is a coordinate within the Reels view from which we start drawing
     * the reels. Its primary purpose is to allow us to add some offset, dedicated for any
     * reels frame. It is valid for this value to be 0.
     */
    reelsPosY : number;

    /**
     * Additional padding (in pixels) between the top edge of the reelsGroup, and the top edge
     * of the first (top-most) symbol. It is valid for this padding to be 0.
     */
    reelsMarginTop : number;

    /**
     * Additional padding (in pixels) between the left edge of the reelsGroup, and the left edge
     * of the first (left-most) symbol. It is valid for this padding to be 0.
     */
    reelsMarginLeft : number;

    /**
     * Additional padding (in pixels) between the right edge of the reelsGroup, and the right edge
     * of the last (right-most) symbol. It is valid for this padding to be 0.
     */
    reelsMarginRight : number;

    /**
     * Additional padding (in pixels) between the bottom edge of the reelsGroup, and the bottom edge
     * of the last (bottom-most) symbol. It is valid for this padding to be 0.
     */
    reelsMarginBottom : number;
        
    /**
     * Horizontal padding (in pixels) between 2 successive reels. This value is only added between
     * reels which are not the first and last reels (it is NOT added to the left and right of the
     * first and final reels).
     */
    reelGap : number;

    /**
     * The vertical gap, in pixels, between successive symbols on a reel. This gap does not occur
     * before the first (top-most) symbol, or after the last (bottom-most) symbol: it only occurs
     * between 2 successive symbols. It is valid for the gap to be 0.
     */
    symGap : number;
        
    /**
     * Indicates if a separator between 2 successive reels should be shown (its width will
     * be the value of reelGap).
     */
    showReelSeparator : boolean;

    /**
     * Height (in pixels) of all winline buttons (either left or right hand-side buttons). This height is a
     * value within the coordinate space of the ReelsGroup (winline buttons are placed within ReelsGroup).
     */
    winLineButHeight : number;
        
    /**
     * Horizontal position (in pixels) of all winline buttons placed on the left of the reels. This posisition
     * is within the coordinate space of the ReelsGroup (winline buttons are placed within ReelsGroup).
     */
    winlineButPosXLeft : number;

    /**
     * Horizontal position (in pixels) of all winline buttons placed on the right of the reels. This posisition
     * is within the coordinate space of the ReelsGroup (winline buttons are placed within ReelsGroup).
     */
    winlineButPosXRight : number;

    /**
     * Width that winlines will be rendererd at (in pixels, within internal coordinate space of ReelGroup).
     */
    winlineWidth : number;

    secondaryHandReelWidth : number;
    secondaryHandReelHeight : number;
    secondaryHandPosX : number[];
    secondaryHandPosY : number[];
    secondaryHandSymPosX : number[];
    secondaryHandSymPosY : number[];
    secondaryHandSymWidth : number;
    secondaryHandSymHeight : number;
}