// TODO: This might now need to be deprecated.

/**
 * Something that can parse a result from the server, and return the standard data
 * model that will be consumed by the view layer.
 */
interface SpinResultParser
{
    /**
     * Parses a result from the server, and returns an ordered list of Spin Round Results.
     * @param serverResult
     */
    parseServerResult(serverResult : Object) : SpinPhaseResult;

    /**
     * Takes a list of Symbol Ids, and returns a broken down matrix of Symbol(Config) objects.
     */
    extractSymbols(symbolIds : number[]) : SymbolsMatrix;

    /**
     * Parses a raw holds pattern into something more useful for the game.
     * @todo: Agree (with Wiener) the actual holds pattern structure we will adopt.
     * I propose that its probably a SymbolMatrix (or something similar).
     */
    extractHoldsPattern(rawHoldsPattern : number[]) : HoldsPattern;
}