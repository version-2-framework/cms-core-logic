// Basic experimental fleshing out of what an abstract component would look like:
// eg: let us write new logic for each game about win sounds, instead of trying to

// configure everything from the same config data
interface SlotSpinSoundController
{
    /**
     * Plays a sound, when the game enters into "game idle" state.
     */
    playGameIdleSound() : void;

    /**
     * Plays a sound, when the game enters into "game idle with wins to show" state.
     */
    playGameIdleWinSequenceSound() : void;

    /**
     * Plays a Spin Anticipation sound when a new spin starts. Different implemnentations of
     * this interface, are free to use specialized internal logic to pick the most appropriate
     * sound effect to play.
     * @param spinPhaseType
     * The SpinPhaseType that the spin belongs to (Spin1, Spin2, FreeSpin)
     * @param spinRoundIndex
     * The index of the Spin Round being played, within the spin phase. For Spin1 and Spin2,
     * this must always be a value of 0. For FreeSpin phases, this needs to be the index of
     * the FreeSpin round (first FreeSpin == spinRoundIndex of 0, etc).
     * @param spinIndex 
     * The index of the spin being played, within the spin round in progress. The first spin
     * of a spin round will always have index 0, the second index 1, etc. For Spin1 and Spin2,
     * there will only be 1 spin round. For FreeSpins (in cases where there are multiple spins
     * happening for each round), this must be reset to 0 every new round that starts.
     */
    playSpinAnticipationSound(spinPhaseType:SpinPhaseType, spinRoundIndex:number, spinIndex:number):void;

    /**
     * Plays the most appropriate sound for when a specific reel starts spinning.
     * @param reelIndex
     * The index of the reel that is starting to spin.
     * @param numReelsAlreadySpin 
     * The number of reels that have started spinning, before this reel started.
     */
    playReelStartSound(reelIndex:number, numReelsAlreadySpin:number) : void;

    /**
     * Returns the conptinuous "rolling" reel sound that is used by some spinner implementations.
     * If the method returns null, then no rolling sound should be played.
     * @param reelIndex
     * The index of the reel to get the rolling sound for.
     * @return
     * A data object indicating the sound to play (ands its max volume), or null if there is no sound
     * to play for reel-roll.
     */
    getReelRollSound(reelIndex:number) : ReelRollSound | null;

    /**
     * Plays the most appropriate reel stop sound, for a set of symbols.
     * @param stopSymbols
     * @param reelIndex
     * @param numReelsAlreadyStopped
     * The number of reels which have already been stopped, before this reel is stopped.
     * @param presentation
     * The presentation data for the Spin Result Presentation being shown.
     */
    playReelStopSound(
        stopSymbols:SymbolConfig[], reelIndex:number, numReelsAlreadyStopped:number, presentation:SpinResultPresentation) : void;
    
    /**
     * Plays the most appropriate sound for a Symbol Win Presentation.
     * @param winPresentation
     * The Symbol Win Presentation data which describes the win currently being shown.
     * @param isQuickWin 
     * If true, indicates that this is a "quick win" (when win values are being shown).
     * If false, indicates that this is an "idle win" (when we loop through any available
     * Symbol Wins to show).
     * @param progressiveIndex
     * The overall progressive index of the Symbol Win sound being played.
     */
    playSymbolWinSound(winPresentation:SymbolWinPresentation, isQuickWin:boolean, progressiveIndex:number) : void;

    /**
     * Plays the most appropriate sound for Spin 2 idle.
     * @param symbols
     * The full matrix of reel symbols shown on all hands.
     */
    playSpin2IdleSound(symbols:SymbolConfifg[][][]) : void;
}

/**
 * A method which plays a Game Idle sound.
 */
type PlayGameIdleSound = () => void;

// TODO: This almost certainly wants to accept more parameters
/**
 * A method which plays a single Game Idle Win Sequence sound.
 */
type PlayGameIdleWinSequenceSound = () => void;

/**
 * Plays a reel start sound.
 * @param reelIndex
 * The left-to-right index of the reel (within the primary hand) to play a reel start
 * sound for.
 */
type PlayReelStartSound = (reelIndex:number, numReelsAlreadySpun:number) => void;

/**
 * Returns the reel roll sound to use for a specific reel.
 * @param reelIndex
 * The left-to-right index of the reel (within the primary hand) to get a roll sound for.
 * @return
 * A data description of a sound instance to play for reel roll, or null, if not sound
 * instance is supposed to be played for the given reelIndex.
 */
type GetReelRollSound = (reelIndex:number) => ReelRollSound | null;

/**
 * A method which will play reel stop sounds.
 * @param symbols
 * The ordered list of symbols, shown on this reel, when it stops.
 * @param reelIndex
 * The left-to-right index of the reel (within the primary hand) to play a reel stop
 * sound for.
 * @param numReelsStopped
 * The number of reels already stopped, before this reel is stopped. A value of 0, means that
 * no reels have stopped before this one (therefore, this is the first progressive reel to be
 * stopped).
 * @param presentation
 * The full presentation for this SpinResult. Some-times this data is usefull: for example, some
 * stop-sound mechanisms may require us to perform checks relative to all of this data, before 
 * deciding whether to play a given sound.
 */
type PlayReelStopSound =
    (symbols:SymbolConfig[], reelIndex:number, numReelsStopped:number, presentation?:SpinResultPresentation) => void;

/**
 * A method which will play a Symbol Win sound.
 * @param winPresentation
 * The presentation data for the Symbol Win that is being shown
 * @param isQuickWin
 * Indicates if this is a quickWin sequence or not. (If not, by definition it is an idle win sequence)
 * @param progressiveIndex
 * The "progressive" index of the Symbol Win that is being played, within the context of either the Quick
 * or Idle win sequences. Suppose this is the quick win sequence: a progressiveIndex of 0, indicates that
 * this is the first SymbolWin shown during the quick win sequence.
 */
type PlaySymbolWinSound = (winPresentation:SymbolWinPresentation, isQuickWin:boolean, progressiveIndex:number) => void;

/**
 * Plays a Spin Anticipation sound when a new spin starts. Different implemnentations of
 * this interface, are free to use specialized internal logic to pick the most appropriate
 * sound effect to play.
 * @param spinPhaseType
 * The SpinPhaseType that the spin belongs to (Spin1, Spin2, FreeSpin)
 * @param spinRoundIndex
 * The index of the Spin Round being played, within the spin phase. For Spin1 and Spin2,
 * this must always be a value of 0. For FreeSpin phases, this needs to be the index of
 * the FreeSpin round (first FreeSpin == spinRoundIndex of 0, etc).
 * @param spinIndex 
 * The index of the spin being played, within the spin round in progress. The first spin
 * of a spin round will always have index 0, the second index 1, etc. For Spin1 and Spin2,
 * there will only be 1 spin round. For FreeSpins (in cases where there are multiple spins
 * happening for each round), this must be reset to 0 every new round that starts.
 */
type PlaySpinAnticipationSound = (spinPhaseType:SpinPhaseType, spinRoundIndex:number, spinIndex:number) => void;

/**
 * Plays a Spin 2 Idle sound, when we enter Spin 2 idle state.
 * @param symbols
 * The full matrix of symbols, shown on all hands.
 */
type PlaySpin2IdleSound = (symbols:SymbolConfig[][][]) => void;

/**
 * Overall configuration for the Configurable Spin Sound component.
 */
interface ConfigurableSpinSoundConfig
{
    /**
     * Configuration for a single sound, played on entering Game Idle
     */
    gameIdleSound ? : GameIdleSoundConfig;

    /**
     * Configuration for a single sound, played on enter Game Idle Win Sequence.
     */
    gameIdleWinSequenceSound ? : GameIdleWinSequenceSoundConfig;

    /**
     * Configures an optional Spin Anticipation sound. A Spin Anticipation sound, is
     * a single sound, played exactly once, when the whole spin begins. Example use
     * cases:
     * - a spin sound that you want played once (you don't want a spin sound played
     *   on every reel starting)
     */
    spinAnticipationSound ? : SpinAnticipationSoundConfig;

    /**
     * Configures the optional Reel spin start sound.
     */
    reelStartSoundConfig ? : ReelStartSoundConfig;

    /**
     * Configures the optional reel roll sound.
     */
    reelRollSoundConfig ? : ReelRollSoundConfig;

    /**
     * Configures the Reel Stop sound.
     */
    reelStopSoundConfig ? : PrimaryReelStopSoundConfig;

    /**
     * Configures the sounds to play when Symbol Wins are shown to the player.
     */
    symbolWinSoundConfig ? : SymbolWinSoundConfig;

    /**
     * Configures any special sounds which may be played during Spin2 idle (eg: we might
     * want to play sounds when certain symbols are in view for spin 2, to tease the player).
     */
    spin2IdleSounds ? : Spin2IdleSoundConfig;
}

interface ReelRollSound
{
    /**
     * String id of the reel-roll sound to play.
     */
    soundId : string;

    /**
     * Max volume that the reel-roll sound should reach.
     */
    maxVolume : number;
}

//--------------------------------------------------------------------------------------------------
// Game idle Sounds
//--------------------------------------------------------------------------------------------------
/**
 * Configuration for default Game Idle sounds.
 */
type GameIdleSoundConfig =
    NoGameIdleSoundConfig |
    RandomGameIdleSoundConfig;

/**
 * Indicates that no sound should be played for game idle.
 */
interface NoGameIdleSoundConfig
{
    type : "none";
}

/**
 * Configures a random sound that will be played for game idle.
 */
interface RandomGameIdleSoundConfig
{
    type : "random";

    /**
     * A list of Sound Ids, from which the game idle sound will be randomly selected.
     */
    soundIds : string[];

    /**
     * The volume (from 0 to 1) at which game idle sounds will be played.
     */
    soundVolume : number;
}

//--------------------------------------------------------------------------------------------------
// Game Idle Win Sequence Sounds
//--------------------------------------------------------------------------------------------------
type GameIdleWinSequenceSoundConfig =
    NoGameIdleWinSequenceSoundConfig |
    RandomGameIdleWinSequenceSoundConfig;

/**
 * Indicates that no sound should be played for game idle win sequence.
 */
interface NoGameIdleWinSequenceSoundConfig
{
    type : "none";
}

/**
 * Configures a random sound that will be played for game idle win sequence.
 */
interface RandomGameIdleWinSequenceSoundConfig
{
    type : "random";

    /**
     * A list of Sound Ids, from which the game idle sound will be randomly selected.
     */
    soundIds : string[];

    /**
     * The volume (from 0 to 1) at which game idle sounds will be played.
     */
    soundVolume : number;
}


//--------------------------------------------------------------------------------------------------
// Spin Anticipation Sounds
//--------------------------------------------------------------------------------------------------
/**
 * Configuration for Spin Anticipation sounds.
 */
type SpinAnticipationSoundConfig =
    NoSpinAnticipationSoundConfig |
    SimpleSpinAnticipationSoundConfig |
    LoopingSpinAnticipationSoundConfig |
    RandomSpinAnticipationSoundConfig;

/**
 * Indicates that there should be no spin anticipation sound played.
 */
interface NoSpinAnticipationSoundConfig
{
    type : "none";
}

/**
 * Plays a single sound for spin anticipation, no matter what spin is being used.
 */
interface SimpleSpinAnticipationSoundConfig
{
    type : "simple";

    /**
     * The id of the sound to play.
     */
    soundId : string;

    /**
     * The volume to play the sound at.
     */
    soundVolume : number;
};

/**
 * Simple loop through a sequence of possible sounds on each new "spin result" starting. Does not
 * take into account spin phase, spin round, etc.
 */
interface LoopingSpinAnticipationSoundConfig
{
    type : "looping";

    /**
     * A series of sounds to loop through.
     */
    sounds : string[];

    /**
     * The volume at which all sounds are played.
     */
    soundVolume : number;
}

/**
 * Randomly select one of a list of available sounds to play for the spin anticipation sound.
 * Does not take into account spin phase, spin round, etc.
 */
interface RandomSpinAnticipationSoundConfig
{
    type : "random";

    /**
     * A selection of sound ids: each time a Spin Anticipation sound is required, one of these
     * sounds will be selected and played at random.
     */
    sounds : string[];

    /**
     * The volume at which the random spin anticipation sounds will be played.
     */
    soundVolume : number;
}

//--------------------------------------------------------------------------------------------------
// Start spin sounds
//--------------------------------------------------------------------------------------------------
type ReelStartSoundConfig =
    NoReelStartSoundConfig |
    SimpleReelStartSoundConfig |
    PerReelStartSoundConfig |
    ProgressiveReelStartSoundConfig;

/**
 * Indicates that no sound should be played when the reel starts spinning.
 */
interface NoReelStartSoundConfig
{
    type : "none";
}

/**
 * Simple configuration for reel start sounds: all reels use exactly the same sound.
 */
interface SimpleReelStartSoundConfig
{
    type : "simple";

    /**
     * The string id of the sound to play for each reel that starts spinning.
     */
    soundId : string;

    /**
     * The volume that the sound will be played at.
     */
    soundVolume : number;
}

/**
 * PerReel configuration for reel start sounds: a sound is used for each reel.
 */
interface PerReelStartSoundConfig
{
    type : "perReel";

    /**
     * The prefix for all start reel sounds.
     */
    soundPrefix : string;

    padLength : number;

    soundVolume : number;
}

/**
 * Progressive configuration for reel start sounds: a different sound is used for
 * each progressive reel to start spinning.
 */
interface ProgressiveReelStartSoundConfig
{
    type : "progressive";

    soundPrefix : string;

    padLength : number;

    soundVolume : number;
}

//--------------------------------------------------------------------------------------------------
// Reel roll sounds
//--------------------------------------------------------------------------------------------------
type ReelRollSoundConfig =
    NoReelRollSoundConfig |
    SimpleReelRollSoundConfig |
    PerReelRollSoundConfig;

/**
 * Indicates that no reel roll sound should be used.
 */
interface NoReelRollSoundConfig
{
    type : "none";
}

/**
 * A basic configuration for Reel Roll sound, where all reels will use the same sound.
 */
interface SimpleReelRollSoundConfig
{
    type : "simple";

    /**
     * The id of the sound to use for reel-roll on all reels.
     */
    soundId : string;

    /**
     * The maximum volume that the reel-roll sound will reach.
     */
    maxVolume : number;
}

interface PerReelRollSoundConfig
{
    type : "perReel";

    soundPrefix : string;

    padLength : number;

    /**
     * The maximum volume that the reel-roll sound will reach.
     */
    maxVolume : number;
}

//--------------------------------------------------------------------------------------------------
// Stop Spin Sounds
//--------------------------------------------------------------------------------------------------
/**
 * Defines the configuration for a single reel stopping sound.
 */
type ReelStopSoundConfig =
    NoReelStopSoundConfig |
    SimpleReelStopSoundConfig |
    PerReelStopSoundConfig |
    ProgressiveReelStopSoundConfig;

/**
 * Configuration for a ReelStop Sound, with a custom filter applied (the filter let's us add
 * detailed game specific control about whether to use the sound in certain circumstances).
*/
interface ReelStopSoundConfigWithFilter
{
    /**
     * An optional custom filter, which let's use determine if we should really play a sound
     * for the reel-stop, in certain circumstances.
     */
    filter ? : ReelStopSoundFilter;
}

type SpecialSymbolReelStopSoundConfig = ReelStopSoundConfig & ReelStopSoundConfigWithFilter;

/**
 * Defines a filter to use on a "special symbol stop sound"
 */
type ReelStopSoundFilter = (symbols:SymbolConfig[], reelIndex:number, progressiveIndex:number, presentation:SpinResultPresentation) => boolean;

// TODO: This interface doesn't have a good name!
// TODO: Should probably support custom volumes for each sound.
/**
 * The overall configuration for stop sounds.
 */
interface PrimaryReelStopSoundConfig
{
    /**
     * Configures the stop sound to play when a reel stops with no special symbols on it.
     */
    standardReelStopSound : ReelStopSoundConfig;

    /**
     * Ordered list of special symbol ids. If one of these special symbols is present on a
     * reel when it stops spinning, then the appropriate symbol sound will be played. The
     * sound will be picked in the same priority order that this array specifies: index 0
     * is the highest priority sound, followed by index 1, etc.
     */
    specialSymbolIds ? : number[];

    /**
     * Configures the stop sound to play with each Special Symbol Id.
     */
    specialSymbolStopSounds ? : SpecialSymbolReelStopSoundConfig[];
};

/**
 * Indicates that no reel-stop sounds should be played when the reels stop spinning.
 */
interface NoReelStopSoundConfig
{
    type : "none";
}

/**
 * Defines reel stop sounds, where each reel has the same sound played when it stops spinning.
 */
interface SimpleReelStopSoundConfig
{
    type : "simple";

    /**
     * The id of the stop sound to use when any reel stops spinning.
     */
    soundId : string;

    /**
     * The volume at which the stop sound should be played (from 0 to 1).
     */
    soundVolume : number;
}

/**
 * Defines reel stop sounds, where each reel stops with a different sound, and the sound
 * is tied explicitly to a reel. EG: whenever reel 3 stops spinning, the reel 3 stop sound
 * is played. This rule is upheld on Two Spin games, where Spin2 may not have all of the
 * reels spinning.
 */
type PerReelStopSoundConfig =
    PerReelStopWithPrefixSoundConfig |
    PerReelStopWithIdsSoundConfig;


interface BasePerReelStopSoundConfig
{
    type : "perReel";

    /**
     * The volume that the sound on each reel should be played at.
     */
    soundVolume;
}

interface PerReelStopWithPrefixSoundConfig extends BasePerReelStopSoundConfig
{
    /**
     * Defines the standard stop sounds played for a specific reel. Regardless of how
     * many other reels are spinning, the specific sound will always be used exactly
     * for the reel that has stopped. EG: for a Spin2, where reels with indices [2,3]
     * are held, and reels with indices [0,1,4] are being spun, then when
     * - reel 0 stops, sound_1 is played
     * - reel 1 stops, sound_2 is played
     * - reel 4 stops, sound_4 is played
     * The stop sound is always strongly linked to a specific reel.
     */
    soundPrefix : string;

    padLength : number;
}

interface PerReelStopWithIdsSoundConfig extends BasePerReelStopSoundConfig
{
    soundIds : string[];
}

/**
 * Defines reel stop sounds, where each reel stops with a different sound, and the sound is
 * "progressive": this means that a unique sound is played according to the number of reels
 * that have so far stopped spinning. For a Two Spin slot game, on Spin2, we may have some
 * reels held: the 3rd reel to stop spinning will have the "3rd reel sound" played, but this
 * reel may NOT be the 3rd reel from the left.
 */
type ProgressiveReelStopSoundConfig =
    ProgressiveReelStopWithPrefixSoundConfig |
    ProgressiveReelStopWithIdsSoundConfig;

interface BaseProgressiveReelStopSoundConfig
{
    type : "progressive";

    /**
     * The volume that each sound should be played at (each index corresponds to an index in
     * sounds).
     */
    soundVolume : number;
}

interface ProgressiveReelStopWithPrefixSoundConfig extends BaseProgressiveReelStopSoundConfig
{
    /**
     * Defines the standard stop sounds to play for successive reel stops. This array needs
     * to be the same as the full length of all reels. In this mode, sounds are played in
     * accordance with the progressive index of the stop sound being played. EG: for a Spin2,
     * where reels with indices [2,3] are held, and reels with indices [0,1,4] are being spun,
     * then when
     * - reel 0 stops, sound_1 is played
     * - reel 1 stops, sound_2 is played
     * - reel 4 stops, sound_3 is played
     * This can let us configured a progressive rising sound on reel stops, without gaps for
     * a 2 spin game, when reels are skipped (owing to being held). This could also be useful
     * in certain cases of respin games with symbols being held.
     */
    soundPrefix : string;

    /**
     * Padding applied to the numerical part of the sound id (added after sound prefix)
     */
    padLength : number;
}

interface ProgressiveReelStopWithIdsSoundConfig extends BaseProgressiveReelStopSoundConfig
{
    /**
     * List of actual sound ids to use on each progressive reel stop.
     */
    soundIds : string[];
}

//--------------------------------------------------------------------------------------------------
// Symbol Win Sounds
//--------------------------------------------------------------------------------------------------
type SymbolWinSoundConfig =
    SimpleSymbolWinSoundConfig |
    SymbolIdSymbolWinSoundConfig |
    WinlineSymbolWinSoundConfig |
    ProgressiveSymbolWinSoundConfig |
    RandomSymbolWinSoundConfig;

/**
 * Specifies a single sound id that will be played for all symbol wins when they are shown.
 * This sound is always played during the quick win sequence. Optionally, it can be played
 * during the idle win sequence as well.
 */
interface SimpleSymbolWinSoundConfig
{
    type : "simple";

    /**
     * The id of the sound to play.
     */
    defaultSoundId : string;

    /**
     * The volume to play the sound at.
     */
    soundVolume : number;

    /**
     * A map of Special Symbol ids, which can have their own custom sounds. If a symbol has a
     * special sound id, then it can override the defaultSoundId in the event that the symbol
     * falls on a non-winline win, and it can even override the "winline specific" sound if the
     * win is on a winline.
     */
    specialSymbolSounds ? : { [symbolId:number]:string }

    /**
     * Indicates if this sound should also be played during "idle win".
     */
    useInIdleWin : boolean;
}

/**
 * Configures Symbol Win sounds, where each Symbol Id gets a unique sound played for it.
 * The string id of the symbol is expected to be in the form "SOUNDNAME_PADDEDSYMBOLID".
 * You can specify a sound id prefix, a pad length, and a sound volume (applied to all
 * of the related sounds).
 */
interface SymbolIdSymbolWinSoundConfig
{
    type : "symbolId";

    /**
     * The prefix id of the sound. It is assumed that all symbol id sounds will use the same
     * basic sound id.
     */
    soundPrefix : string;

    /**
     * Number of numerical padding to add to the symbol id portion.
     */
    padLength : number;

    /**
     * The volume which will be applied to all symbol sounds.
     */
    soundVolume : number;

    /**
     * Indicates if the sound should also be used in idle win.
     */
    useInIdleWin : boolean;
}

/**
 * Configures a Symbol Win sound, tied to specific winlines.
 */
interface WinlineSymbolWinSoundConfig
{
    type : "winlineWin";

    /**
     * The prefix id of the sound to use for winline-wins. It is assumed that all winline id sounds
     * will use the same basic sound id.
     */
    winlineSoundPrefix : string;

    /**
     * Number of numerical padding to add to the winlineId portion (also used for SymbolId portion
     * for "special symbol ids").
     */
    padLength : number;

    /**
     * The volume which will be applied to all winline sounds.
     */
    soundVolume : number;

    /**
     * Full id of a sound to use as a fallback, in the event that a non-winline win needs to be
     * shown.
     */
    defaultSoundId ? : string;

    /**
     * A map of Special Symbol ids, which can have their own custom sounds. If a symbol has a
     * special sound id, then it can override the defaultSoundId in the event that the symbol
     * falls on a non-winline win, and it can even override the "winline specific" sound if the
     * win is on a winline.
     */
    specialSymbolSounds ? : { [symbolId:number]:string }

    /**
     * Indicates if the sound should also be used in idle win.
     */
    useInIdleWin : boolean;
}

/**
 * Configures Symbol Win sounds, where each progressive win gets a new sound from a sequence
 * played on it.
 */
type ProgressiveSymbolWinSoundConfig =
    ProgressiveSymbolWinWithPrefixSoundConfig |
    ProgressiveSymbolWinWithSoundIdsSoundConfig;

interface BaseProgressiveSymbolWinSoundConfig
{
    type : "progressive";

    soundVolume : number;

    /**
     * Indicates if the sound should also be used in idle win.
     */
    useInIdleWin : boolean;

    /**
     * A map of Special Symbol ids, which can have their own custom sounds. If a symbol has a
     * special sound id, then it can override the defaultSoundId in the event that the symbol
     * falls on a non-winline win, and it can even override the "winline specific" sound if the
     * win is on a winline.
     */
    specialSymbolSounds ? : { [symbolId:number]:string }
}

interface ProgressiveSymbolWinWithPrefixSoundConfig extends BaseProgressiveSymbolWinSoundConfig
{
    soundPrefix : string;

    padLength : number;
}

interface ProgressiveSymbolWinWithSoundIdsSoundConfig extends BaseProgressiveSymbolWinSoundConfig
{
    soundIds : string[];
}

/**
 * For each Symbol Win shown, a sound will be chosen at random from the list of sound ids provided
 */
interface RandomSymbolWinSoundConfig
{
    type : "random";

    /**
     * List of sound ids, from which a Symbol Win sound will be chosen at random.
     */
    soundIds : string[];

    soundVolume : number;

    useInIdleWin : boolean;
}


//--------------------------------------------------------------------------------------------------
// Spin 2 Tease Sounds
//--------------------------------------------------------------------------------------------------
/**
 * Configuration for any "tease sounds"
 */
interface Spin2IdleSoundConfig
{
    /**
     * An optional default sound, played as a fallback when there is no symbol tease sound to play.
     */
    defaultSound ? : Spin2IdleDefaultSoundConfig

    /**
     * An optional, priority ordered list of "tease sounds" to play on Spin 2 Idle. Each tease sound
     * is generally configured to target a specific symbol id. Because we only want to play one
     * sound, we need the priority ordered list (to indicate the order of importance of picking a
     * sound, when we have more than 1 tease configured). Think of each tease sound config, as being
     * a set of trigger rules, specifying the conditions in which they will be played.
     */
    teaseSounds ? : Spin2IdleTeaseSoundConfig[];
}

type Spin2IdleDefaultSoundConfig =
    NoSpin2IdleDefaultSoundConfig |
    SimpleSpin2IdleDefaultSoundConfig;

/**
 * Indicates that no sound should be played.
 */
interface NoSpin2IdleDefaultSoundConfig
{
    type : "none";
}

/**
 * A single sound will be played when spin 2 idle state is entered.
 */
interface SimpleSpin2IdleDefaultSoundConfig
{
    type : "simple";

    soundId : string;

    soundVolume : number;
}

type Spin2IdleTeaseSoundConfig =
    NoSpin2IdleTeaseSoundConfig |
    Spin2IdleTeaseScatterSoundConfig;

/**
 * Indicates that no sound should be played for Spin 2 idle tease.
 */
interface NoSpin2IdleTeaseSoundConfig
{
    type : "none";
}

/**
 * Plays a special sound when any number of symbols are in view, with the symbols confirming to "scatter"
 * rules: this means that it does not matter where on the reels they appear, we only count the total number
 * of symbols present. This configuration need not be used to target only scatter symbols (it can be set
 * for any arbitrary symbol id).
 * 
 * With this configuration, number of symbols present is taken into account, but only a single outcome
 * sound is played.
 */
interface Spin2IdleTeaseScatterSoundConfig
{
    type : "scatter";

    /**
     * The id of the symbol that this "scatter tease" will be played for.
     */
    symbolId : number;

    /**
     * The minimum number of the target symbolId that must be present on any single hand on the reels, to
     * trigger the sound. For a multi-hand game, any hand which has the target number of symbols, will
     * trigger this sound to be played.
     */
    minNumSym : number;

    /**
     * The id of the sound to play.
     */
    soundId : string;

    /**
     * The volume at which to play the sound.
     */
    soundVolume : number;
}