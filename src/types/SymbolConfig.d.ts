//--------------------------------------------------------------------------------------------------
// Types for configuring symbol animations
// Currently, this is in prototype stage
//--------------------------------------------------------------------------------------------------

// TODOS:
// 1) Clearly there need to be defaults for the WinFrame and WinBackground (although the
//    possibility to define them)
// 2) The WinBackground and WinFrame values currently relate only to "normal sized" wins,
//    and maybe... there should be some ability to change this ? If not, then the docs
//    need to clearly indicate that they relate only to normal symbol state.

/**
 * This is the master config for symbol animations
 */
interface SymbolsConfiguration
{
    /**
     * Specifies the effect that should be used for symbol fading.
     */
    symbolFade : SymbolFadeConfig;

    /**
     * Default scale to use for all symbols, when they are in a standard (non-winning, and
     * non-tall) visual state. A value of 100, means "normal scale"
     */
    defaultScale : number;

    /**
     * Default scale to use for the symbol idle win animation. A value of 100 means "normal
     * scale".
     */
    defaultAnimScale : number;

    /**
     * Indicates if the symbol blur effect should be shown (will only be seen, if the
     * Spinner implementation being used supports blur). The blur effect will be applied
     * to ALL symbols (by definition, it cannot be configured independently for different
     * symbol ids).
     */
    useBlur : boolean;

    /**
     * Indicates how tall the "blur symbol" layers are meant to be shown, relative to standard
     * symbol assets. A default value of 1, indicates that the blur symbol assets are the to be
     * shown as the same height as the standard symbol assets: a value of 2 would indicate that
     * the blur symbol assets are to be shown as twice the height of the standard symbol assets.
     */
    blurVerticalScale : number;

    /**
     * Default background used for the symbol when in idle state. A default background MUST be
     * configured, in order to use any background on symbols - consider the config for a default
     * background, as the flag which indicates to SymbolView "backgrounds are enabled". If you
     * only want backgrounds on certain symbols (and not on all), you can simply specify the
     * default background as having type "invisible" (and then configure any custom backgrounds
     * required for specific symbols).
     */
    defaultBackground ? : SymbolComponentConfig;

    /**
     * Default background animation to use when the symbol is in held state. A default HELD
     * background MUST be configured, in order to use any HELD background on symbols - consider
     * the config for a default held background, as the flag which indicates to SymbolView "held
     * backgrounds are enabled". If you only want held backgrounds on certain symbols (and not on
     * all), you can simply specify the default background as having type "invisible" (and then
     * configure any custom backgrounds required for specific symbols).
     */
    defaultHeldBackground ? : SymbolHeldComponentConfig;

    /**
     * Default win foreground animation to use when the symbol is in a winning visual state of some
     * kind. A default win foreground animation MUST be configured, in order to use any foreground
     * animation on symbols - consider the config for default win foreground, as the flag which
     * indiates to Symbol View "win foregrounds are enabled". If you only want win foregrounds on
     * certain symbols (and not on all), you can simply specify the default win foreground as having
     * type "invisible" (and then configure any custom win foregrounds required on specific symbols).
     */
    defaultWinForeground ? : SymbolWinComponentConfig;

    /**
     * Default win background animation to use when the symbol is in a winning visual state of some
     * kind. A default win background animation MUST be configured, in order to use any background
     * animation on symbols - consider the config for default win background, as the flag which
     * indiates to Symbol View "win backgrounds are enabled". If you only want win backgrounds on
     * certain symbols (and not on all), you can simply specify the default win background as having
     * type "invisible" (and then configure any custom win backgrounds required on specific symbols).
     */
    defaultWinBackground ? : SymbolWinComponentConfig;

    /**
     * Map of symbol ids to animated configurations. This allows us to generate highly
     * specific configurations for individual symbols, include animations, background
     * and foreground effects, tall symbols, etc.
     */
    perSymbol : { [symbolId:number]:SymbolViewConfig };

    /**
     * The maximum number of symbols to be used in a tall symbol. The game's tall symbol assets
     * MUST include an animation of this height for every symbol that can be a tall symbol. This
     * value refers to the largest size of tall symbol assets, and can basically be thought of as
     * the "default": we would normally expect this value to be the same as the number of symbols
     * to show on the tallest single reel in the game. If this value is less than the number of
     * symbols on the tallest reel in the game, then the game client will be forced to divide up
     * tall symbols vertically as a fallback: this could be fine, but is a caveat the must be
     * understood when designing tall symbol assets for a game.
     * 
     * This value is integral to the correct functioning of tall symbols for a number of reasons 
     * 1) If we know the maximum height of a tall symbol, then we can automatically choose between
     *    showing a custom height asset (if one is available), or falling back to a default height
     *    asset (we will use the largest available asset size, specified by this field, and then
     *    we will crop it vertically)
     * 2) If we have to use the cropping, this value tells us how to calibrate the size and position
     *    of the cropped tall symbol.
     */
    tallSymMaxNumSym : number;

    // TODO: For now this is a SymbolViewConfig, it might need to change to a dedicated "Tall Symbol"
    // specific config.
    /**
     * Map of all tall symbol configurations. If not specified, it will be assumed that tall symbols
     * are not enabled for the current game client.
     */
    tallSymPerSymbol ? : { [symbolId:number]:{ [numSymInTallSymbol:number]:SymbolViewConfig } }
}

/**
 * Configures the animations for a single symbol, for the default symbol view.
 */
interface SymbolViewConfig
{
    anchor ? : { x:number, y:number }

    // Scale and AnimatedScale here are more like the defaults, for 
    /**
     * Scale of the symbol when in its normal, non-tall state. This is a percent value,
     * in the range of 0 to 100 (although a value of 0 will be ignored). If not specified,
     * then the symbol will be rendered at normal size.
     */
    scale ? : number;

    // TODO: I think the type of this should be slightly different to the foreground
    // and background types (or rather, the type intersection should be different, not
    // the raw type objects that are available)
    /**
     * Optional win animation, to be played during the "idle winnings" sequence.
     */
    winAnimation ? : SymbolAnimationConfig;

    /**
     * Optional win animation, to be played during the big win and quick win sequence. This seperate to the idle winnings sequence
     */
    winlineAnimation ? : AnimatedSymbolComponentConfig;

    /**
     * Optional background, which will be shown behind this symbol, whenever the symbol
     * is in idle state.  In order for this to work, you must have configured a default
     * background (even if the default background is configured as "invisible" - eg:
     * nothing is shown). The custom background will be used only for this symbol.
     */
    background ? : SymbolComponentConfig;

    /**
     * Optional background, which will be shown behind this symbol, whenever the symbol
     * is shown in held visual state. In order for this to work, you must have configured
     * a default held background (even if the default held background is configured as
     * "invisible" - eg: nothing is shown). The custom held background will be used only
     * for this symbol.
     */
    heldBackground ? : SymbolHeldComponentConfig;

    /**
     * Optional background, which will be shown behind this symbol, whenever a win is
     * being shown. In order for this to work, you must have configured a default win
     * background (even if the default win background is configured as "invisible" -
     * eg: nothing is shown). The custom win background will be used only for this symbol.
     */
    winBackground ? : SymbolWinComponentConfig;

    /**
     * Optional "frame" which will be shown as an overlay for this symbol, whenever a win
     * is being shown.  In order for this to work, you must have configured a default win
     * foreground (even if the default win foreground is configured as "invisible" -
     * eg: nothing is shown). The custom win foreground will be used only for this symbol.
     */
    winForeground ? : SymbolWinComponentConfig;

    /**
     * Indicates if the tall symbol should be cropped to its width without including the reel gap. This is set to true by default in the symbol class.
     */
    tallSymbolCropBySymbolWidth ? : boolean;

    /**
     * Offsets the position of a partial tall symbol i.e a symbol where only sections of the tall symbol are shown.
     */
    partialTallSymbolOffsetPosition ? : {x:number, y: number}

    /**
     * Indicates if the partial tall symbol should animation should scale when played
     */
    partialTallSymbolScaleAnim ? : boolean;
}

//--------------------------------------------------------------------------------------------------
// Configuration options for main symbol animations
//--------------------------------------------------------------------------------------------------
type SymbolAnimationConfig =
    ShineSymbolAnimationConfig |
    AnimatedSymbolAnimationConfig |
    SkeletalSymbolAnimationConfig;

interface ShineSymbolAnimationConfig
{
    type : "shine";

    /**
     * Optional scale for this animation. The scale is relative to the normal size of the symbo,
     * and is a "percentage scaled" value, eg: a vaue of 100 will see the symbol rendered at a
     * standard size, 50 will render it at 50% of normal size, and 200 will render it at 200%
     * of normal size. A value of 0 will actually be ignored (symbol will be renderer at normal
     * size), and if the field is omitted, the symbol animation will also be rendered at normal
     * size.
     */
    scale ? : number;

    /**
     * An optional additional scale factor (applied on top of "scale") for a "bounce" animation.
     * If this field is set, then a bouncing scale animation will also be applied: the symbol
     * grows from its default size (or its size after "scale" is applied) to its full "bounceScale"
     * size, and then reverts back. The growring / shrinking effect takes the same time as 1 loop
     * of the shine animation (if "loop" is true, then the bouncing effect will also loop).
     */
    bounceScale ? : number;

    /**
     * Indicates if the shine effect should loop.
     */
    loop : boolean;
}

interface FadeSymbolAnimationConfig
{
    type : "fade";

    /**
     * Indicates if the fade effect should loop.
     */
    loop : boolean;
}

interface AnimatedSymbolAnimationConfig extends AnimationConfig
{
    type : "animated";

    
    /**
     * Offset position for the full tall symbol animation when there are 3 symbols shown 
     */
    fullTallAnimOffsetPosition ? : {x: number, y:number};
}

interface SkeletalSymbolAnimationConfig
{
    type : "skeletal";

    /**
     * Scale of the skeletal symbol animation (100 == normal size)
     */
    scale ? : number;

    /**
     * The string name of the dragonbones armature to show for this Symbol Animation.
     */
    armatureName : string;

    /**
     * The string name of the dragonbones data object in which the armature exists. This is
     * the name that a dragonbones texture atlas / armature collection is cached under (not
     * necessarily the name of the asset on disk, as in "atlases.json", we may configure a
     * bespoke name to represent the data asset - though generally we should be using a name
     * similar to the assets on disk).
     */
    dragonBonesDataName : string;
}

//--------------------------------------------------------------------------------------------------
// Configuration options for additional symbol components
//--------------------------------------------------------------------------------------------------

interface BaseSymbolComponentConfig
{
    /**
     * Time (in seconds) that it will take to fade in the static component. Setting a
     * value of 0, or omitting the field, means that the component will be displayed
     * instantaneously when required.
     */
    fadeInTime ? : number;

    /**
     * Time (in seconds) that it will take to fade out the static component, when it
     * needs to be hidden. Setting a value of 0, or omitting the field, means that the
     * component will be instantaneously hidden when required.
     */
    fadeOutTime ? : number;
}

/**
 * Additional standard properties for a Symbol Win component config (fields which define
 * exactly which win sequences that the component should be visible in)
 */
interface BaseSymbolWinComponentConfig
{
    /**
     * Indicates if this compomnent will be shown in the Quick win sequence.
     */
    useInIdleWin : boolean;

    /**
     * Indicates if this component will be shown in the Idle win sequence.
     */
    useInQuickWin : boolean;

    /**
     * Indicates if this component will be shown in the Big Win sequence.
     */
    useInBigWin : boolean;
}

interface InvisibleSymbolComponentConfig
{
    type : "invisible";
}

/**
 * Configures a static symbol component: basically, a single image.
 */
interface StaticSymbolComponentConfig extends BaseSymbolComponentConfig
{
    type : "static";

    /**
     * The name of the static art asset that will be loaded for this symbol component.
     */
    assetName : string;

    /**
     * Optional scale for this static symbol win component. The scale is relative to the normal
     * size of the symbol, and is a "percentage scaled" value, eg: a vaue of 100 will see the
     * symbol rendered at a standard size, 50 will render it at 50% of normal size, and 200 will
     * render it at 200% of normal size. A value of 0 will actually be ignored (symbol will be
     * renderered at normal size), and if the field is omitted, the symbol win component will also
     * be rendered at normal size.
     */
    scale ? : number;

    /**
     * Optional anchor for the static symbol win component.
     */
    anchor ? : { x:number, y:number };
}

// TODO: I think that this should inherit from the standard "animated symbol config"
/**
 * Configures an animated symbol component: an animated sprite.
 */
interface AnimatedSymbolComponentConfig
    extends AnimationConfig, BaseSymbolComponentConfig
{
    type : "animated";

    /**
     * The string prefix which identifies the animated asset frames.
     */
    prefix : string;
}

/**
 * Basic configuration for a Symbol component - defines everything visual about HOW it should
 * look when shown, but does not define any use case specific rules about WHEN it should be used.
 * It is possible to configure a symbol component to be "invisible", meaning it is not shown.
 * The purpose of this, is to configure edge cases: for example, to use a symbol component, it
 * must be created for the symbol (but we may want to override certain cases, and have it not
 * shown).
 */
type SymbolComponentConfig =
    InvisibleSymbolComponentConfig |
    StaticSymbolComponentConfig |
    AnimatedSymbolComponentConfig;

/**
 * Defines a single win component for the symbol. The component will be shown whenever the
 * symbol's functionality requires that this specific component be shown.
 */
type SymbolWinComponentConfig = SymbolComponentConfig & BaseSymbolWinComponentConfig;

/**
 * Defines a single "Held" component for a Symbol. The comopnent will be shown whenever the
 * symbol is in some kind of held visual state.
 */
type SymbolHeldComponentConfig = SymbolComponentConfig;

// Name temporarily changed from "SymbolAnimConfig" so TS doesnt think its the same thing
/**
 * Data configuration for a sprite based animation.
 */
interface AnimationConfig
{
    /**
     * Optional scale for this animation. The scale is relative to the normal size of the symbo,
     * and is a "percentage scaled" value, eg: a vaue of 100 will see the symbol rendered at a
     * standard size, 50 will render it at 50% of normal size, and 200 will render it at 200%
     * of normal size. A value of 0 will actually be ignored (symbol animation will be renderered
     * at normal size), and if the field is omitted, the symbol animation will also be rendered
     * at normal size.
     */
    scale ? : number;

    /**
     * The numerical id of the start frame of the animation.
     */
    startFrame : number;

    /**
     * The numerical id of the final frame of the animation.
     */
    finalFrame : number;

    /**
     * The speed that the animation should be played at. If not supplied, a default speed
     * value will be applied.
     */
    speed ? : number;

    /**
     * Should the animation loop.
     */
    loop : boolean;

    /**
     * Padding length of frame names for the animation. For example, if you have 60 frames (1 to 60),
     * each labelled as "0001" to "0060", then you need to set a padLen of 4.
     */
    padLen : number;

    /**
     * Optional anchor for the animation.
     */
    anchor ? : { x:number, y:number };
}

/**
 * Configures how the "symbol fade" effect should look. "Symbol Fade" is applied to non-winning
 * symbols during a symbol win presentation. It is possible to specify "NONE" as type (in which
 * case, no special effect will be applied), or various visual effects may be applied (to make
 * the symbols "fade" into the background).
 */
type SymbolFadeConfig = NoSymbolFade | AlphaSymbolFade | TintSymbolFade;

/**
 * Specifies that no symbol fade effect will be applied.
 */
interface NoSymbolFade
{
    type : "NONE";
}

/**
 * Common properties for any symbol fade type which is not NONE
 */
interface BaseSymbolFade
{
    /**
     * Optional setting (in seconds) for how long the symbol fade effect will take to initially
     * fade in. A default value (likely to be larger than 0) will be used if no value is supplied,
     * so in order to have the effect applied immediately, you need to explicitly specify a
     * duration of 0.
     */
    duration ? : number;

    /**
     * Indicates if the fade effect will also be applied to any background layers (if they exist).
     * If false (or if the field is not specified), the fade effect will be applied only to the
     * primary foreground part of the symbol. If true, then the effect will be applied to both the
     * the foreground and background elements of the symbol. During a symbol fade, we expect ONLY
     * the main symbol graphic (and an otpional background) to be visible (if this changes in the
     * future, then this field will still only offer a binary choice between fading the foreground
     * only, and fading every part of the symbol that is visible).
     */
    applyToBackground ? : boolean;
}

/**
 * Specifies an alpha fade effect.
 */
interface AlphaSymbolFade extends BaseSymbolFade
{
    type : "ALPHA";

    /**
     * Specifies the target alpha level to fade to.
     */
    alpha : number;
}

/**
 * Specifies a tinting fade effect.
 */
interface TintSymbolFade extends BaseSymbolFade
{
    type : "TINT";

    /**
     * Specifies the target colour to tint to.
     */
    tintColour : number;
}