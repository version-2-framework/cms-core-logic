/**
 * Defines the configuration of a single test-server.
 */
interface TestServerConfig
{
    /**
     * The name to be associated with this test-server. This is for debug purposes only,
     * and may be shown on-screen in the Url Selection View.
     */
    name : string;

    /**
     * The url that will be used as an edpoint.
     */
    url : string;
}