//--------------------------------------------------------------------------------------------------
// Typescript definitions supporting the ToolTip visual component for the GUI
//--------------------------------------------------------------------------------------------------

/**
 * Indicates whether a tooltup is horizontal or vertical
 * - if horizontal, its arrow will point to the left or the right
 * - if vertical, its arrow will point above or below
 */
type TooltipOrientation = "horizontal" | "vertical";

/**
 * Indicates what position a tooltip should occupt, relative to a targeted button.
 */
type TooltipPosition = "above" | "below" | "left" | "right";

/**
 * Defines visual style settings for a tooltip.
 */
interface TooltipStyle
{
    /**
     * Colour of the tooltip background.
     */
    bgColour : number | string;

    /**
     * Colour of the text on the tooltip.
     */
    textColour : number | string;

    /**
     * Colour of any stroke to show around the tooltip.
     */
    lineColour : number | string;
}