//--------------------------------------------------------------------------------------------------
// Configurations for the default Total Winnins animations.
//--------------------------------------------------------------------------------------------------

// TODO: Config currently has some other fields for TotalWin - we can continue to support them
// as optional fields, but in my opinion, we should prefer this config (if it is supplied) -
// this might be a way of progressively updating the config.

/**
 * Configuration for the Default Total Win animation. If you are using a custom Total Win
 * Animation, then you should NOT be using this: just write the animation the way you want
 * it to work. This config is solely for the case where we fall back on the (highly
 * flexibile) standard Total Win animation.
 */
interface TotalWinAnimationConfig
{
    /**
     * Id of an optional sound to play during the total win sequence (it will be played
     * from the start of the animation). This field is legacy - it is maintained, because
     * it existed from early on, but now there are specific sound configuration fields,
     * that will specify several configurable sounds.
     */
    soundId ? : string;

    /**
     * Volume that the optional Total Win sound should be played at.
     */
    soundVolume ? : number;

    /**
     * Optionally specify the prefix for number char assets. If not specified, the total
     * winnings view will fall back to using 'wpanim_' as the prefix. This only works,
     * when "useWinValue" is false.
     */
    numberCharPrefix ? : string;

    /**
     * If set to true, the the "WinValue" created by GameFactory will be used to show
     * winning amounts: otherwise, characters will be automatically generated for a custom
     * animation/
     */
    useWinValue : boolean;

    /**
     * Percentage of vertical target area that should be occupied by the title text.
     * If omitted, a default value is used.
     */
    titleRelativeHeight ? : number;

    /**
     * Maximum percentage of horizontal target area that should be occupied by the title text.
     * If omitted, a default value is used.
     */
    titleRelativeWidth ? : number;

    /**
     * Percentage of vertical target area, at which the vertical center of the target text should
     * be placed. If omitted, a default value is used.
     */
    titleRelativePosY ? : number;

    /**
     * Percentage of vertical target area, at which the vertical center of the win value text should
     * be placed. If omitted, a default value is used.
     */
    winValueRelativePosY ? : number;

    /**
     * Maximum width (as a percentage of allowed area) that the Win Value text can be.
     */
    winValueRelativeWidth ? : number;

    /**
     * Maximum height (as a percentage of allowed area) that the Win Value text can be.
     */
    winValueRelativeHeight ? : number;

    /**
     * Configures an optional, one off sound that will played at the start of the Total Win
     * animation.
     */
    totalWinStartSound ? : SimpleSoundConfig;

    /**
     * Configures an optional, one off sound that will be played at the end of the Total Win sequence,
     * just before the Total Win animation gets faded out.
     */
    totalWinEndSound ? : SimpleSoundConfig;
}

/**
 * Configuration explicitly for the default Slot Total Win Animation.
 */
interface SlotTotalWinAnimationConfig extends TotalWinAnimationConfig
{
    /**
     * Duration - in seconds - that the animation will last for. When not specified, a default
     * value (generally about 2 seconds) will be applied.
     */
    duration ? : number;

    /**
     * Indicates whether to center the Total Win animation (for the slot game) within the area
     * of the reels. If set to false, the Total Win Animation will be centered and scaled
     * relative to the screen: if set to true, it will be centered and scaled relative to
     * the reels (no matter where on-screen they are).
     */
    centerInReels : boolean;
}

interface BonusTotalWinAnimationConfig extends TotalWinAnimationConfig
{
    /**
     * Number of seconds that a single Multiplier "count-up" is shown for on-screen (excluding
     * the very final one)
     */
    multiplierDuration ? : number;

    /**
     * Number of seconds that the final Multiplier "count-up" is shown for on-screen.
     */
    finalMultiplierDuration ? : number;

    /**
     * Position at which the win value must be shown, during the "multiplier win" sequence.
     */
    winValueMultiplierRelativePosY ? : number;

    /**
     * Position at which the title must be shown, during the "multiplier win" sequence.
     */
    titleMultiplierRelativePosY ? : number;

    /**
     * Maximum allowed width (as a percentage of screen width) of the "Multiplier" text, for when
     * there is a Multiplier win sequence to show.
     */
    multiplierRelWidth ? : number;

    /**
     * Maximum allowed height (as a percentage of screen height) of the "Multiplier" text, for when
     * there is a Multiplier win sequence to show.
     */
    multiplierRelHeight ? : number;
    
    /**
     * Position (as a percentage of screen height) at which the vertical center of the "Multiplier"
     * text will be placed.
     */
    multiplierRelPosY ? : number;
    
    /**
     * Configures an optional, one off sound that will be played if a multiplier is in use. This
     * sound will be played
     * - every time the win value shown increases due to multiplication, at the very start of the
     *   multiplied value sequence
     * - NOT when the initial prize value is shown (the win value before any multiplication has
     *   been applied)
     * Therefore, this sound is never played when there is no multiplier applied.
     */
    multiplierUpSound ? : SimpleSoundConfig;
}

/**
 * Configures a simple sound: has a sound id, and volume, doesn't allow looping.
 */
interface SimpleSoundConfig
{
    /**
     * Id of the sound to play.
     */
    soundId : string;

    /**
     * Volume to play the sound at.
     */
    soundVolume : number;
}

type FontWeight = "normal" | "bold" | "bolder" | "lighter";

interface SlotWinningsTextStyleConfig
{
    /**
     * Colour (or colours) applied to the text. This can be a single colour, or a series of
     * cololurs (in which case, the text is coloured with a gradient)
     */
    fill : string[] | number[];

    /**
     * The FontWeight applied to the text. This actually depends on the font used: not all
     * fonts will support all standard html fontweights.
     */
    fontWeight : FontWeight;

    /**
     * Indicates if certain size config values are treated as proportionate values relative
     * to the size of the text (which is not defined in this config), or are relative to screen
     * height. The fields affected are the fields such as ["strokeSize", "dropShadowDistance",
     * "dropShadowBlur", "padding"]. For example:
     * - when set to true, "strokeSize" sets the proportional height of the stroke relative to
     *   however big text is. A value of 10 for stroke size would ensure that the stroke is
     *   always 10% of the height of the text.
     * - when set to false, "strokeSize" sets the height of the stroke relative to screen height,
     *   so a value of 10 would set stroke to 10% of screen height.
     */
    useSizesRelativeToText : boolean;

    /**
     * Applies padding to the sides of the font: sometimes (due to the underlying nature of the
     * font itself), your stroke and dropshadows can come out cropped. Adding padding can help
     * alleviate this (you have to play around with it). As with other distance settings, this
     * field works in 2 modes
     * - when "useSizesRelativeToText" is true, its a value that is a percentage of text height,
     *   eg: a value of 1 means "add padding equivalent to 1% of text height"
     * - when "useSizesRelativeToText" is false, its a value relative to screen height, eg: a
     *   value of 1 means "add padding equivalent to 1% of screen height"
     */
    padding : number;

    /**
     * Indicates if a stroke effect should be applied to the text. The rendering of this stroke
     * depends on the various stroke related settings.
     */
    useStroke : boolean;

    /**
     * Sets the size of the stroke. When useSizesRelativeToText is true, this is a value that is
     * proportional to the size of the text: for example, a value of 0.1 sets the stroke to be
     * 10% of the height of the text (this yields consistent behaviour across different scenarios).
     * When useSizesRelativeToText is false, this is a value proportional to screen-height (this
     * is a legacy behaviour)
     */
    strokeSize : number;

    /**
     * Colour applied to the stroke.
     */
    strokeColor: number | string;

    /**
     * Specifies the line-join mode for the stroke.
     */
    lineJoin : "miter" | "round" | "bevel";

    /**
     * If stroke's lineJoin mode is miter, this specifies the miterLimit.
     */
    miterLimit : number;

    /**
     * Indicates if drop-shadow should be applied to the text. The various drop-shadow related
     * settings only work, if this flag is set to true.
     */
    useDropShadow : boolean;

    /**
     * The colour applied to any drop-shadow.
     */
    dropShadowColor : number | string;

    /**
     * The alpha value of the drop-shadow (in the range of 0 to 1)
     */
    dropShadowAlpha : number;

    /**
     * Distance that the drop-shadow is away from the text.
     */
    dropShadowDistance : number;

    /**
     * Blur level for the drop-shadow.
     */
    dropShadowBlur : number;
}