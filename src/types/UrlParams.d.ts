interface UrlParams
{
    
}

/**
 * Standard Url Parameters for the Gen1 game client integration.
 */
interface UrlParamsGen1 extends UrlParams
{
    /**
     * The value of Wmg Web Session Id.
     */
    JSESSIONID : string;
}

/**
 * Standard url Parameters expected to be delivered to the V2 Platform Game Client. These
 * parameters are managed manually by the V2 WMG Game Engine Server, ensuring the game
 * client is given a minimum amount of dynamic information.
 */
interface UrlParamsGen2 extends UrlParams
{
    /**
     * Indicates if this is a qa, staging or production deployment.
     */
    config : ClientConfigValue;

    /**
     * Value of Wmg Web Session Id.
     */
    wmgWebSessionId : string;

    /**
     * Integer value of game client id.
     */
    gameId : number;

    /**
     * Licensee Id for the game client. This is a number (even if it comes in string form - this part is
     * not clear). It represents the numerical enum of the licensee on which the game client is being
     * played. We use this to load an appropriate set of business configs (config options for the game
     * client, which configure the GUI, certain features that are enabled)
     */
    licenseeId : number | string;

    /**
     * Sub-licensee id for the game client. This is similar to licenseeId, however:
     * - the value is always relative to licenseeId
     * - the value is always a string.
     * 
     * Consider these 2 cases
     * - where licenseeId is 1, and subLicenseeId is "A12", 
     * - where licenseeId is 14, and subLicenseeId is "A12"
     * In both cases, the meaning of subLicenseeId is different (as it relates only to licenseeId)
     */
    subLicenseeId ? : string;

    /**
     * The country in which the game client is configured to run.
     */
    country : Country;

    /**
     * The currency in use for the game client.
     */
    currency : Currency;

    /**
     * The total amount of money which may be transferred into a single game session.
     */
    regMoneyLimit : number;

    /**
     * Gameplay mode, selected on the licensee portal.
     */
    gameplayMode : GameplayMode;

    /**
     * For sg digital builds, we may be supplied with an optional operator id (equivalent
     * to a sub-licensee id, but this is not supplied by our own server: it comes from
     * sg digital's launch parameters, and we add it as a custom url parameter).
     */
    sgdOperatorId ? : number | string;

    /**
     * Variable set by the licensee so that they have control of how the application behaves in the environment 
     * that it is launched in. If this variable is not set then it will default to a value of 0 indicating that this variable is not applicable. 
     */
    clientEnv: number | string;
}

/**
 * All possible values of the "config" url param for V2 game clients.
 */
type ClientConfigValue = 1 | 2 | 3;