/**
 * Represents the outcome of any Validation method.
 */
interface Validation
{
    /**
     * The result of the validation.
     */
    isValid : boolean;

    /**
     * For the case where isValid is false, an optional error string can be supplied.
     */
    error ? : string;
}