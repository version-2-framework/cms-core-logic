//--------------------------------------------------------------------------------------------------
// Utility methods relating to arrays.
//--------------------------------------------------------------------------------------------------
/**
 * Returns a new array, with a specific size, filled with an initial value.
 * @param {number} size 
 * @param {T} initialValue 
 * @return {T[]}
 * @template T
 */
export function createArray(size, initialValue)
{
    let array = [];

    for (let i = 0; i < size; i ++) {
        array.push(initialValue);
    }

    return array;
};

/**
 * Copies values from one array to another. If target array is not large enough,
 * its size will be set to the same value as the size of the "valuesToCopy" array
 * @template T
 * @param {T[]} target
 * The target array to copy into
 * @param {T[]} valuesToCopy
 * The values to copy
 */
export function copyArray(target, valuesToCopy)
{
    let numValuesToCopy = valuesToCopy.length;

    if (numValuesToCopy > target.length) {
        target.length = numValuesToCopy;
    }

    for (let i = 0; i < numValuesToCopy; i ++) {
        target[i] = valuesToCopy[i];
    }
}