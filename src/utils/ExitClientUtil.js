//--------------------------------------------------------------------------------------------------
// Functionality for Exiting the game client, when an "Exit Client" button is invoked.
//--------------------------------------------------------------------------------------------------
// We have a config object, known as an "ExitClientAction", which specifies various ways that we
// can close / exit a game client. We can then check the type of Exit Client Action that has been
// specified, and invoke the appropriate functionality. This allows us to add new mechanics for
// exiting the game client.
//--------------------------------------------------------------------------------------------------

import * as LogManager from "../logging/LogManager";

const log = LogManager.getDefaultLogger();

export class ExitClientUtil
{
    /**
     * Constructs a new Exit Client util.
     * @param {Object} urlParams
     * The set of url parameters in use by the game client.
     */
    constructor(urlParams)
    {
        this._urlParams = urlParams;
    };


    /**
     * Checks if an Exit Client Config is valid, and if the action it represents can be performed
     * (to the best of our knowledge). If the config is not valid or cannot be performed, its possible
     * for us to disable any button present in the GUI that would attempt to call "exitClient", and to
     * report the error in the logs. We should aim to fail gracefully: for example, if we are supposed
     * to redirect to a url from a urlParameter, but the urlParameter has not been supplied, we should
     * not show such a button, and log the issue (game client is still functional).
     * @public
     * @param {ExitClientAction} action 
     * @return {Validation}
     */
    isExitClientActionValid(action)
    {
        if (action)
        {
            if (action.type === "postMessage") {
                return isPostMessageActionValid(action);
            }
            else
            if (action.type === "redirect") {
                return isRedirectActionValid(action);
            }
            else
            if (action.type === "redirectToUrlInParam") {
                return isRedirectToUrlInParamActionValid(action, this._urlParams);
            }
            else
            if (action.type === "closeCurrentTab") {
                // TODO: Check if it is possible to see if window.close is allowed. For now, we will
                // simply indicate that the action is valid.
                return { isValid:true };
            }
            // Uncognized type
            else
            {
                return { isValid:false, error:`ExitClientAction.type ${action.type} is not recognized`};
            }
        }
        else return { isValid:false, error:`ExitClientAction was not specified` };
    }


    /**
     * Attempts to exit the game client.
     * @param {ExitClientAction} exitClientAction 
     * The configuration for exiting the game client.
     */
    exitClient(exitClientAction)
    {
        // Double check if the action is valid, and bail
        // out early if it is not.
        let validation = this.isExitClientActionValid(exitClientAction);
        if (validation.isValid === false) {
            log.error(`Attempted to invoked exit client action, but could not: ${validation.error}`);
            return;
        }

        if (exitClientAction.type === "redirect") {
            redirectToUrl(exitClientAction);
        }
        else
        if (exitClientAction.type === "redirectToUrlInParam") {
            redirectToUrlInParam(exitClientAction, this._urlParams);
        }
        else
        if (exitClientAction.type === "postMessage") {
            postMessage(exitClientAction);
        }
        else
        if (exitClientAction.type === "closeCurrentTab") {
            try {
                window.close();
            }
            catch (e) {
                log.error(`ExitClientUtil: window.close has failed (${e.toString()})`);
            }
        }
    };
}


/**
 * Checks if a postMessage exit client config is valid.
 * @param {PostMessageExitClientAction} action
 * @return {Validation}
 */
function isPostMessageActionValid(action)
{
    let isValid = false;
    let error;

    if (action)
    {
        if (action.postMessageCommand && action.postMessageTargets)
        {
            let target = getPostMessageTarget(action);
            isValid = target !== null;
            error = isValid? null : `window.${action.postMessageTargets} does not point to a valid object`;
        }
        else
        {
            isValid = false;
            error = `both command and target are required `;
        }
    }
    else
    {
        isValid = false;
        error = "PostMessageAction config is null";
    }

    return { isValid, error };
};


/**
 * Assembles the target object for a postMessage action. If the target cannot be assembled
 * (eg: the action config specifies an invalid target), then null will be returned.
 * @param {PostMessageExitClientAction} action
 * @return {Object | null}
 */
function getPostMessageTarget(action)
{
    let currTarget = window;
    let foundTarget = true;
    let targetTokens = action.postMessageTargets.split('.');

    for (let targetIndex = 0; targetIndex < targetTokens.length; targetIndex ++) {
        let target = targetTokens[targetIndex];

        if (target in currTarget) {
            currTarget = currTarget[target];
        }
        else {
            foundTarget = false;
        }
    }

    if (foundTarget) {
        return currTarget;
    }
    else return null;
};


/**
* Close command that you want to send to the browser hosting the game
* @param {PostMessageExitClientAction} action
* @return {void}
*/
function postMessage(action) {
    let target = getPostMessageTarget(action);

    if (!target) {
        log.error(`ExitClient.postMessage: could not assemble a valid postMessage target from ${action.postMessageTargets}`);
    }
    // We have the target, so we can try and post a message
    else {
        try {
            log.info(`ExitClient.postMessage: calling window.${action.postMessageTargets}.postMessage(${action.postMessageCommand})`);
            target.postMessage(action.postMessageCommand, "*");
        }
        catch (e) {
            log.error(`ExitClient.postMessage: failed (${e.toString()})`);
        }
    }
};


/**
 * Validates a Redirect to Url from UrlParam action. If the action config is null, is missing
 * the required fields, or the url parameter specified was not passed to the game client, then
 * the action will be treated as invalid.
 * @param {RedirectToUrlParamExitClientAction} action 
 * @param {Object} urlParams
 * A simple dictionary of url parameters.
 * @return {Validation}
 */
function isRedirectToUrlInParamActionValid(action, urlParams)
{
    let isValid = false;
    let error;

    if (action)
    {
        if (action.urlParam)
        {
            let urlParamExists = action.urlParam in urlParams;
            if (urlParamExists) {
                isValid = true;
            }
            else error = `urlParam with key ${action.urlParam} has not been passed to the GameClient`;
        }
        else error = 'No target urlParam key has been provided';
    }
    else
    {
        error = "No action provided";
    }

    return { isValid, error };
};


/**
 * Redirects to a url stored in a Url Parameter
 * @param {RedirectToUrlParamExitClientAction} action 
 * @param {Object} urlParams
 * A simple dictionary of url parameters.
 */
function redirectToUrlInParam(action, urlParams)
{
    let urlParamId = action.urlParam;
	let url = urlParams[urlParamId];
    
    log.info(`ExitClient.redirectToUrlInParam(urlParam:${urlParamId},url:${url})`);
	
	if (url) {
		window.open(url, "_self");
	}
	else {
		log.warn(`ExitClientUtil.redirectToUrlInParam: no target url available for ${action}`);
	}
};


/**
 * Validates a Redirect ExitClientAction.
 * @param {RedirectExitClientAction} action 
 * @return {Validation}
 */
function isRedirectActionValid(action)
{
    // TODO: We could check the format of the url, to see if anything is obviously amiss?
    // This may turn out to be a future requirement.

    return { isValid:true };
};


/**
 * Redirects to a specific url, within the same page.
 * @param {RedirectExitClientAction} action
 */
function redirectToUrl(action)
{
    log.info(`ExitClient.redirectoToUrl(${action.url})`);
    
	window.open(action.url, "_self");
};