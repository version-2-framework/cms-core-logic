import { CmsRng } from "../maths/CmsRng";

/**
 * Implementation of the Shuffler interface, using the Fisher Yates shuffle algorithm.
 * @implements {Shuffler}
 */
export class FisherYatesShuffler
{
    /**
     * @param {Rng} rng 
     */
    constructor(rng)
    {
        /**
         * @private
         */
        this._rng = rng || new CmsRng();
    };


    /**
     * @public
     * @inheritdoc
     * @param {*[]} data 
     */
    shuffleArray(data)
    {
        fisherYatesShuffle(data, this._rng);
    };
}


/**
 * Performs a Fisher Yates shuffle, with a given rng.
 * @param {*[]} data
 * The data to be shuffled.
 * @param {Rng} rng
 * The RNG instance to use to drive the shuffle.
 */
export function fisherYatesShuffle(data, rng)
{
    for (let srcIndex = 0; srcIndex < data.length; srcIndex ++) {
        let tgtIndex = rng.getRandom(srcIndex + 1);
        if (tgtIndex != srcIndex) {
            let srcVal = data[srcIndex];
            let tgtVal = data[tgtIndex];
            data[srcIndex] = tgtVal;
            data[tgtIndex] = srcVal;
        }
    }
};