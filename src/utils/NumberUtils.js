//--------------------------------------------------------------------------------------------------
// A collection of basic utility methods that relate to numbers.
//--------------------------------------------------------------------------------------------------
/**
 * Checks if a numerical value is a whole integer.
 * @param {number} value
 * @return {boolean}
 */
export function isWholeInteger(value) {
    return Math.round(value) === value;
};

/**
 * Checks if a numerical value is a positive, whole integer.
 * @param {number} value 
 * @return {boolean}
 */
export function isPositiveWholeInteger(value) {
    return Math.round(value) === value && value > 0;
};