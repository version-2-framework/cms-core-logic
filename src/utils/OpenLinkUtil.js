//--------------------------------------------------------------------------------------------------
// Open Link utility functionality
//--------------------------------------------------------------------------------------------------

import * as LogManager from "../logging/LogManager";

const log = LogManager.getDefaultLogger();
const popupSpecs = "fullscreen=no,toolbar=no,menubar=no";

/**
 * Utility class for opening a link in a popup, as specified by an Open Link Action.
 * Each Open Link Action specified, may provide different mechanism for requesting
 * the link to be opened.
 */
export class OpenLinkUtil
{
    /**
     * 
     * @param {Dependencies} dependencies 
     */
    constructor(dependencies)
    {
        /**
         * @private
         */
        this._urlParams = dependencies.urlParams;
    };


    /**
     * Opens an external link
     * @public
     * @param {OpenLinkAction} action 
     * @param {Object} [urlParameters]
     */
    openLink(action, urlParameters) {
        let url = this.fetchUrlFor(action);
        if (url) {
            openLink(url, urlParameters);
        }
    };


    /**
     * Determines the url that should be used for an open link action. If the Open
     * Link Action specified is null or invalid, this method may return a null
     * value, otherwise it should return a url to be opened.
     * @private
     * @param {OpenLinkAction} action 
     */
    fetchUrlFor(action) {
        let url = null;
        if (action) {
            if (action.type === "url") {
                url = action.url;
            }
            else
            if (action.type === "urlParam") {
                url = this._urlParams[action.urlParam];
            }
        }
        return url;
    };
};

/**
 * Opens a link in a popup (on desktop) or a separate tab (on mobile)
 * @private
 * @param {string} url
 * @param {Object} [urlParameters]
 */
export function openLink(url, urlParameters)
{
    log.info(`OpenLink.openPopup(url:${url}, urlParameters:${JSON.stringify(urlParameters)})`);

    let urlToOpen;
    if (urlParameters) {
        urlToOpen = `${url}?${encodeQueryStringParameters(urlParameters)}`;
    }
    else urlToOpen = url;

    window.open(urlToOpen, null, popupSpecs, false);
};

/**
 * Encodes an object into a string of query parameters. This doesn't append any prefix:
 * if you pass in { param1:value, param2:value }, you will get back the following string:
 * 'param1=value&param2=value'
 * @param {Object} parameters
 * @return {string}
 */
export function encodeQueryStringParameters(parameters)
{
    let encoded = "";

    let keys = Object.keys(parameters);

    keys.forEach((key, keyIndex) => {
        encoded += `${encodeURIComponent(key)}=${encodeURIComponent(parameters[key])}`;

        if (keyIndex + 1 < keys.length) {
            encoded += '&';
        }
    });

    return encoded;
}