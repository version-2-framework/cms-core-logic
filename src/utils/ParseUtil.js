import * as C_MessageField from "../const/C_MessageField";
import C_ServerErrorCode from "../const/C_ServerErrorCode";
import C_ClientErrorCode from "../const/C_ClientErrorCode";

//--------------------------------------------------------------------------------------------------
// Parse Util
//--------------------------------------------------------------------------------------------------
// A collection of methods for the task of extracting data out of a JSON response object.
//
// There are methods for extrating values of specific kinds: all of these return some kind of
// default value if the field is not present on the object.
// There are methods for parsing certain data fields in a specific way (eg: holds patterns).
//
// It is not necessary to use this parsing functionality when reading a JSON response object.
// We started using methods like this originally in the Flash games, because earlier versions
// used XML as the messaging protocol (and because occasionally, we were not sure if a field's
// value would be rendered as a string or not). The abstration of using parsing methods provided
// some safety against changes that occurred behind the scenes.
// 
// Generally, we only use these methods on game clients that need to support the V1 Game Engine
// Server, as the V1 server has some REALLY unhelpfull behaviours in the json data it returns
// - "stringly typed" - an array of numbers or strings will not be encoded in standard, semantically
//   correct json, it will almost always be a string (not certain if this is true 100% of the time,
//   the V1 Game Engine Server json protocol is often inconsistent)
//--------------------------------------------------------------------------------------------------
/**
 * Extracts a boolean value from a data object. If no property
 * matching the id exists on the data object, or if the value
 * cannot accurately be parsed as a boolean, the value returned
 * will be false: if its a valid boolean, its value will be
 * returned.
 * @param {string} id
 * The id of the field to read from the target object.
 * @param {Object} data
 * The data object that we should read the value from.
 * @return {boolean}
 */
export function extractBoolean(id, data)
{
	if (id in data)
	{
		let val = data[id];
		return parseBoolean(val);
	}
	else return false;
};


/**
 * Extracts a string field. If no value exists with the given
 * field id, then an empty string is returned, otherwise the
 * field's value is cast to string and returned.
 * @param {string} id
 * @param {Object} data
 * @return {string}
 */
export function extractString(id, data)
{
	return (id in data)? data[id].toString() : "";
};


/**
 * Extracts a field, and parses it as an integer. If not field
 * with the given id exists, then the value returned will be 0.
 * @param {string} id
 * @param {Object} data
 * @return {number}
 */
export function extractInt(id, data)
{
	return (id in data)? parseInt(data[id], 10) : 0;
};

/**
 * 
 * @param {*} data 
 */
export function asInt(data)
{
    return parseInt(data);
};


/**
 * Extracts a field, and parses it as a floating point value.
 * If no field with the given id exists, then the value returned
 * will be 0.
 * @param {string} id
 * @param {Object} data
 * @return {number}
 */
export function extractNumber(id, data)
{
    return (id in data)? parseFloat(data[id]) : 0;
};


/**
 * Extracts a field, and parses it as an array of string values.
 * If no field with the given id exists, then the value returned
 * will be an empty array.
 * @param {string} id
 * @param {Object} data
 * @return {string[]}
 */
export function extractStringArray(id, data)
{
	return (data[id])? String(data[id]).split(',') : [];
};


/**
 * Parses an objact to a string array
 * @param {Object} data 
 * @returns {string[]}
 */
export function asStringArray(data)
{
    return data ? String(data).split(',') : [];
}


/**
 * Extracts a field, and parses it as an array of number values.
 * If no field with the given id exists, then the value returned
 * will be an empty array.
 * @param {string} id
 * @param {Object} data
 * @return {number[]}
 */
export function extractNumberArray(id, data)
{
    if (data[id])
    {
        let val = data[id];
        if (Array.isArray(val)) {
            return val;
        }
        else return String(val).split(',').map(Number);
    }
    else return [];
};


/**
 * Parses a piece of data as an integer array.
 * @param {*} data 
 * @return {number[]}
 */
export function asIntegerArray(data)
{
    if (Array.isArray(data)) {
        return data;
    }
    else
    {
        return data === ""? [] : String(data).split(',').map(Number);
    }
};


/**
 * Extracts a field, and parses it as an array of boolean values.
 * If no field with the given id exists, the the value returned will
 * be an empty array.
 * @param {string} id
 * @param {Object} data
 * @return {boolean[]}
 */
export function extractBooleanArray(id, data)
{
    return (data[id])? String(data[id]).split(',').map(parseBoolean) : [];
};


/**
 * Parses a value as a boolean array.
 * @param {*} data 
 * @return {boolean[]}
 */
export function asBooleanArray(data)
{
    return String(data).split(',').map(parseBoolean);
};


/**
 * Extracts a field, and parses it as a holds array. This is a special case,
 * where holds are normally returned as string constants. This function will
 * parse the string constants into boolean values, returning a boolean array.
 * @param {string} id
 * @param {Object} data
 * @return{boolean[]}
 */
export function extractHoldsArray(id, data)
{
	return asHoldsArray(data[id]);
};


/**
 * Parses an arbitrary piece of data (assumed to be encoded in some way as V1 holds, and
 * returns a boolean array. The input can be an array or a "string encoded array" (without
 * brackeds) - the second case is really what is expected here (as if the input is an array,
 * it gets turned into a set of strings, which is inefficeint). The output array will only
 * have the same "depth" as the original input data (eg: it will not produce nested array
 * outputs) - if you want to decode holds, it may be better to use dedicated methods, eg:
 * "asHoldReelsPattern"
 * @param {*} data
 * @return {boolean[]}
 */
export function asHoldsArray(data)
{
    let holdsStrings = asStringArray(data)
	let holds = [];
	for (let i = 0; i < holdsStrings.length; i ++)
	{
		holds.push(["H","1","true"].indexOf(holdsStrings[i]) > -1);
	}
	return holds;
};


/**
 * Parses a piece of data as a "Hold Reels" Holds Pattern. (This is the only
 * holds pattern type that gets used on V1 platform)
 * @param {*} data
 * @param {number} numHands
 * @param {number} numReelsPerHand
 * @return {HoldsPattern}
 */
export function asHoldReelsPattern(data, numHands, numReelsPerHand)
{
    let holdsStringTokens = asStringArray(data);
    let pattern = [];
    for (let i = 0; i < numHands; i ++) {
        // We will create an identical pattern for each hand, because
        // on V1, this is the correct behaviour. So we are only really
        // interested in the first N tokens from raw server-returned
        // holds pattern, where N is "numReelsPerHand"
        let patternForHand = [];
        for (let j = 0; j < numReelsPerHand; j ++) {
            let token = holdsStringTokens[j];
            patternForHand.push(["H","1","true"].indexOf(token) > -1);
        }

        pattern.push(patternForHand);
    }

    return { type : "holdReels", pattern };
};


/**
 * Accepts an array to be treated as a holds pattern, and writes it to
 * a holds string array (ready to be sent to the server).
 * @param {number[] | boolean[] | string[]} holds
 * @return {string[]}
 */
export function createHoldsStringArray(holds)
{
    /*
    let stringHolds = [];
    for (let i = 0; i < holds.length; i ++) {
        let val = holds[i];
        let isHeld = false;
        if (val === "H" || val === true || val > 0) {
            isHeld = true;
        }
        stringHolds.push(isHeld ? "H" : "NH");
    }
    return stringHolds;
    */

    return holds.map(holdValue => ["H","1","true"].indexOf(holdValue.toString()) > -1? "H" : "NH");
};


/**
 * Accepts an input value, and attempts to parse it as a boolean. Any
 * invalid value will be returned as false.
 * @param {any} value
 * @return {boolean}
 */
export function parseBoolean(value)
{
    return (value === "true" || value === true || value === 1 || value === "1")? true : false;
};


/**
 * Checks if a response message has a server error indicated.
 * @param {Object} request 
 */
export function hasServerError(request) {
	let hasError = false;
	if (C_MessageField.ERROR in request) {
		let errorCode = request[C_MessageField.ERROR];
		if (errorCode !== C_ServerErrorCode.NO_ERROR) {
			hasError = true;
		}
	}
	return hasError;
};


/**
 * Fetches any error code from a request, and parses it to a client error
 * code. This error code can then be dispatched through the client, to
 * identify what the actual issue was. If the ERROR field is missing, or
 * indicates no error returned by the server, then this will return false.
 * Otherwise, it will pick the most appropriate client error code.
 * @param {Gen1ServiceReply} reply
 * @return {ServiceExecutionError | null}
 */
export function getGen1CommsErrorFor(reply)
{
	if (C_MessageField.ERROR in reply && reply[C_MessageField.ERROR] != "0000") {
        let serverErrorCode = reply[C_MessageField.ERROR];
        let errorCode = reply.ERROR_KEY_CODE? `SR${reply.ERROR_KEY_CODE}` : "SR00";
        let errorDescription = "There was an error!";
		
		if (serverErrorCode === C_ServerErrorCode.SERVER_CLOSED_SESSION) {
            errorDescription = "The Game Engine Server has automatically closed the session";
		}
		else
		if (serverErrorCode === C_ServerErrorCode.REQUEST_ALREADY_PROCESSED) {
            errorDescription = "The GameEngineServer has already processed this request";
		}
		else
		if (serverErrorCode === C_ServerErrorCode.CREDIT_LIMIT_REACHED) {
            errorDescription = "The player has already reached their credit limit for the session";
		}
		else
		if (serverErrorCode !== C_ServerErrorCode.NO_ERROR) {
			// Default to returning generic "server
			// gave us an error" Client error code.
            errorDescription = `Unknown server side error reported: (${serverErrorCode})`;
        }
        
        return {
            code : errorCode,
            description : errorDescription,
            isFatal : true
        };
	}
    else return null;
};