//const STANDARD_VALUES = [5,10,20,30,40,50,]

import { isPositiveWholeInteger } from './NumberUtils';

/**
 * Partitions a number into exactly N values. Because the strict requirements of this method
 * may not be possible, 0 values will be inserted, if required, in order to make up the number
 * of values requested.
 * @param {number} value 
 * @param {number} numPartitions
 * @return {number[]}
 */
/*
export function partitionValueIntoExactlyNValues(value, numPartitions)
{
    let partitionedValues = [];

    return partitionedValues;
};
*/

export class Partitioner
{
    /**
     * Constructs a new Bell Partitioner instance.
     * @param {Rng} rng 
     */
    constructor(rng) {
        this._rng = rng;
    };

    // TODO: My attempt to document and explain this method, and to name the local variables,
    // is rather poor. I apologize in advance.
    // Russell W, 03.03.2020

    /**
     * Partitions a value, using only values from a set of numbers. So, passing a value of 5,
     * and a partitionValue of [1,2,3,4,5], the method might return any combination of
     * [1,1,1,1,1],[1,1,1,3],[1,2,2],[2,3],[1,4],[5].
     * 
     * This value is generally appropriate for dividing up smaller winning amounts, eg: a
     * multiplier, into a set of known possible outcomes.
     * 
     * An optional parameter maxNumPartitions is provided: this specifies the maximum number of
     * units that the value should be partitioned into. By default, no limit is imposed: the
     * method will attempt to divide up the value using only values from the set of partition
     * values.
     * 
     * When not enforcing a maximum number of partitions, then the method will attempt to return
     * a random number of partitions. If the maximum value of totalValue is known, then the max
     * number of partitions can be implicitly understood (based on the set of partitionValues
     * provided). In any case where the partition requested is impossible (eg: the value cannot
     * be fully partitioned using the set of partition values), the method will return an array
     * with a  single value (the whole value to be partitioned).
     * 
     * When enforcing a maximum number of partitions, the method guarantees not to exceed the
     * number requested. It will attempt to use only values from the value set, but if this proves
     * impossible (eg: 9 cannot be divided into 3 parititions using only values of [1,5]), it may
     * return an array where 1 value includes a leftover amount (but the other values will be
     * values from the value set).
     * 
     * If the partition value itself does not conform to the rules (eg: its negative, zero, or
     * not a whole integer), then the method will return an empty array.
     * 
     * @param {number} totalValue 
     * The value to be partitioned. This should be a whole integer. If it has a value of less
     * then 1, then the method will return an empty array.
     * @param {number[]} partitionValues
     * The set of
     * @param {number} [maxNumPartitions]
     * @return {number[]}
     * The series of divided values. This is guaranteed to contain at least 1 value, with the
     * exception of the cases where totalValue to be divided was not a positive, whole integer.
     */
    partitionUsing(totalValue, partitionValues, maxNumPartitions=Number.POSITIVE_INFINITY) {
        let partitions = [];
        let valueLeftToBeAwarded = totalValue;
        let sortedPartitionValues = partitionValues.slice(0, partitionValues.length).sort();
        
        /**
         * Finds the largest value index (from sortedPartitionValues) that is still less than
         * or equal to a value to be partitioned. Will return -1 if no such value exists.
         * @param {number} valueToAward
         * The amount that is still to be partitioned.
         * @return {number}
         * The index of the highest value that can be used, or -1, if no value is available
         */
        let findMaxValueIndex = valueToAward => {
            let maxIndex = -1;
            sortedPartitionValues.forEach((value, valueIndex) => {
                if (value <= valueToAward) {
                    maxIndex = valueIndex;
                }
            });
            return maxIndex;
        }

        if (isPositiveWholeInteger(totalValue)) {
            if (maxNumPartitions !== Number.POSITIVE_INFINITY) {
                while (valueLeftToBeAwarded > 0) {
                    let maxPartitionIndex = findMaxValueIndex(valueLeftToBeAwarded);
                    if (maxPartitionIndex > -1) {
                        let randomPartitionValueIndex = this._rng.getRandom(maxPartitionIndex);
                        let randomPartitionValue = sortedPartitionValues[randomPartitionValueIndex];
                        partitions.push(randomPartitionValue);
                        valueLeftToBeAwarded -= randomPartitionValue;
                    }
                    // We cannot perform any more partitions, simply stash the remainder
                    // into the list of partitioned values as a new value, and be done.
                    else
                    {
                        partitions.push(valueLeftToBeAwarded);
                        break;
                    }
                }
            }
            else
            {
                // TODO !!!
            }
        }

        return partitions;
    };

    /**
     * Partitions a value, into a sequence of whole integer values. This method is ideal for
     * using against a total win value (where we want to split it up, into a certain number
     * of whole values).
     * 
     * The general rules of this method are as follows: it will endeavour to distribute your
     * value into exactly the number of partitions requested, unless this is impossible, in
     * which case the actual number returned will be fewer. The number of prize values returned
     * is guaranteed to be at least 1, unless the totalValue passed in is less than 1 (in which
     * case, the function will return an empty array). The size of the array returned will
     * never be more than the number of partitions requested.
     * @public
     * @param {number} totalValue 
     * The value that will be partitioned. This must be a whole integer: considering that this
     * function attempts to return whole integers, any floating point value passed for totalValue
     * means that the behaviour of the function is unspecified.
     * @param {number} maxNumPartitions
     * The maximum number of partitions that the prize should be divided into. This must be a
     * positive, whole integer value. The method will attempt to respect this value, but if the
     * number of partitions is too large (larger than the prize value), then of course its not
     * possible (so the method will return as many prize partitions as it can).
     * @param {number} [targetPrizeUnit]
     * A target prize unit to divide the value into. If no value is passed (or the value passed
     * is less than 1), then the default value of 5 is used. The method will attempt to divide
     * up the prize into multiplies of 5 (if the prize does not exactly divide by 5, then we
     * can expect 1 prize partition that will contain any remainder). The "bell" distribution
     * component of this method uses this targetPrizeUnit to work out the distribution of prize
     * values.
     * @return {number[]}
     * The partitioned sequence of prize values. No orderering is applied: the values are in an
     * arbitrary sequence, and should be reordered as required by the user of the method.
     */
    partitionValue(totalValue, maxNumPartitions, targetPrizeUnit=5)
    {
        let partitionedValues = [];

        if (totalValue < 1) {
            return partitionedValues;
        }
        else
        if (maxNumPartitions <= 1) {
            partitionedValues = [totalValue];
        }
        else
        {
            let prizeUnit = targetPrizeUnit < 1? 5 : targetPrizeUnit;

            // First, we split the totalValue into 2 components: a part that is divisible by
            // the prizeUnit, and a remainder (of course, the remainder might be 0). We will
            // use the part divisible by prizeUint when calculating our bell distribution. The
            // remainder is simply an amount that we have to add on somewhere.
            let totalValueDisibleByPrizeUnit = totalValue - (totalValue % prizeUnit);
            let totalValueRemainder = totalValue - totalValueDisibleByPrizeUnit;

            // We will split the part that is divisible by the prizeUnit, using a simple bell
            // distribution. First, we divide that value by prizeUnit: this gives us "numver of
            // prize units to distribute". The mean of this is the center of our distribution
            // (we round it down to a whole number, and push anything we have taken on to out
            // tally of a remainder).
            let numPrizeUnitsToDistribute = totalValueDisibleByPrizeUnit /  prizeUnit;
            
            let targetNumPartitions = maxNumPartitions;
            if (numPrizeUnitsToDistribute <= maxNumPartitions) {
                targetNumPartitions = numPrizeUnitsToDistribute;
            }
            
            let meanNumPrizeUnits = Math.floor(numPrizeUnitsToDistribute / targetNumPartitions);
            totalValueRemainder += prizeUnit * (numPrizeUnitsToDistribute - (meanNumPrizeUnits * targetNumPartitions));

            // We will endeavour for this not to happen, but it can.
            if (meanNumPrizeUnits === 1)
            {
                for (let i = 0; i < targetNumPartitions; i ++) {
                    partitionedValues.push(prizeUnit);
                }
                partitionedValues[0] += totalValueRemainder;
            }
            else
            {
                // Now, how many mean values will we award ? To perform a bell distribution, we need to add
                // units on either side of the mean in pairs. Therefore, the number of means awarded MUST
                // be at least 1, but leave a value divisible by 2 leftover.
                let numMeanPartitionsAwarded = 1 + this._rng.getRandom(targetNumPartitions);

                // This tells us how many more partitions we are supposed to add
                let numPartitionsLeftToAward = targetNumPartitions - numMeanPartitionsAwarded;
                if (numPartitionsLeftToAward % 2 !== 0) {
                    numMeanPartitionsAwarded += 1;
                    numPartitionsLeftToAward -= 1;
                }

                // Now, let's start creating a list of "multiplies of prizeUnit". First we add the mean
                // awards (of which there will be at least 1).
                let multipliesOfPrizeUnitToAward = [];
                for (let i = 0; i < numMeanPartitionsAwarded; i ++) {
                    multipliesOfPrizeUnitToAward.push(meanNumPrizeUnits);
                }

                // Add a bell like distribution of everything else: while we still have extra "fiveUnits" to
                // award, we need to add awards either side of the mean. We generate a random offset from the
                // mean (this random offset cannot be more than mean - 1: this keeps the lesser value with a
                // minimum of 1 five unit).
                for (let i = 0; i < numPartitionsLeftToAward / 2; i ++)
                {
                    let randomOffset = 1 + this._rng.getRandom(meanNumPrizeUnits - 1);
                    let increasedValue = meanNumPrizeUnits + randomOffset;
                    let decreasedValue = meanNumPrizeUnits - randomOffset;

                    multipliesOfPrizeUnitToAward.push(increasedValue);
                    multipliesOfPrizeUnitToAward.push(decreasedValue);
                }

                // Finally, we can assemble our output.
                multipliesOfPrizeUnitToAward.forEach(multipleOfPrizeUnit => {
                    partitionedValues.push(multipleOfPrizeUnit * prizeUnit);
                });

                // Add our remainder somewhere randomly.
                let randomPartitionIndex = this._rng.getRandom(partitionedValues.length);
                partitionedValues[randomPartitionIndex] += totalValueRemainder;
            }
        }


        return partitionedValues;
    };
}