import platform from 'platform';

/**
 * Implementation of the platform api, using Platform js.
 * @implements {PlatformApi}
 */
export class PlatformJsPlatformApi
{
    constructor()
    {
        
    }

    /**
     * @public
     * @inheritdoc
     * @return {boolean}
     */
    isDesktop() {
        return !this.isMobileOrTablet();
    };

    /**
     * @public
     * @inheritdoc
     * @return {boolean}
     */
    isMobile() {
        if (platform) {
            let family = platform.os.family;
            let mobileFamilyKeys = ['Windows Phone', 'Android', 'iOS'];
            return mobileFamilyKeys.indexOf(family) > -1;
        }
        else return false;
    };

    /**
     * @public
     * @inheritdoc
     * @return {boolean}
     */
    isTablet() {
        if (platform) {
            return false;
        }
        else return false;
    };

    /**
     * @public
     * @inheritdoc
     * @return {boolean}
     */
    isMobileOrTablet() {
        return this.isMobile() || this.isTablet();
    };
}