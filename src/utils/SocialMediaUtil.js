//--------------------------------------------------------------------------------------------------
// Social Media Utiliesi
//--------------------------------------------------------------------------------------------------
// SocialMediaUtil is a static utility for sharing simple messages on social media.
// Platforms currently supported, are:
// - Twitter
// - Facebook 
//--------------------------------------------------------------------------------------------------

import { getAppLogger } from "../logging/LogManager";

const log = getAppLogger();

/**
 * Specifications for any popup that is opened.
 */
const popupSpecs = "fullscreen=no,toolbar=no,menubar=no";

/**
 * Share a message string on to Facebook.
 * @param {string} url
 * The url to share, as part of the facebook post.
 * @param {string} msg
 * The message string to put in the facebook post.
 */
export function shareOnFacebook(url, msg)
{
	log.info(`[SocialMedia].shareOnFacebook(msg:${msg},url:${url})`);
	
	let msgToShare = processMsg(msg);
    let urlString = `https://facebook.com/sharer/sharer.php?t=${msgToShare}&u=${escape(url)}`;
	
	openPage(urlString);
};


/**
 * Share a message string on to Twitter.
 * @param {string} url
 * The url to link within the twitter post.
 * @param {string} msg
 * The message string to put in the twitter post.
 */
export function shareOnTwitter(url, msg)
{
	log.info(`[SocialMedia].shareOnTwitter(msg:${msg},url:${url})`);
	
	let msgToShare = processMsg(msg);
    let urlString = `https://twitter.com/intent/tweet?source=webclient&text=${msgToShare}%3A${escape(url)}`;
	
	openPage(urlString);
};


/**
 * Sanitizes a text string into a PHP friendly form.
 * @param {string} msg
 * The message to process.
 * @return {string}
 * A PHP friendly formatted version of msg.
 */
function processMsg(msg)
{
	// todo: check if any more validation must be done.
	return msg.replace(/ /g, "+");
};


/**
 * Opens a specific URL in a seperate browser window.
 * @param {string} urlString
 * The url to open.
 */
function openPage(urlString)
{
	log.debug(`[SocialMedia].openPage(url:${urlString})`);
	
	// TODO: Improve !
	window.open(urlString, null, popupSpecs, false);
};