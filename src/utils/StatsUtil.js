//--------------------------------------------------------------------------------------------------
// Stats Utils
//--------------------------------------------------------------------------------------------------
// A collection of simple methods for calculating game statistics.
//--------------------------------------------------------------------------------------------------
// @ts-check

/**
 * Calculates and returns relative RTP, as a number.
 * @param {number} totalWon
 * The total amount won (in points). If a value of 0 is passed in,
 * then RTP returned will be 0.
 * @param {number} totalSpent
 * The total amount spent (in points). If a value of 0 is passed in,
 * then RTP returned will be 0.
 * @return {number}
 * The rtp, normalized to a value "out of 100".
 */
export function getRtp(totalWon, totalSpent)
{
    if (totalWon > 0 && totalSpent > 0) {
        return totalWon / totalSpent * 100;
    }
    else return 0;
};

/**
 * Calculates and returns relative RTP, as a ready formatted string.
 * @param {number} totalWon 
 * @param {number} totalSpent 
 * @return {string}
 */
export function getRtpString(totalWon, totalSpent)
{
    let rtp = getRtp(totalWon, totalSpent);
    let rtpString = `${rtp.toFixed(2)}%`;
    return rtpString;
};

/**
 * Returns the hit-rate of some event, within a number of games, as a number.
 * @param {number} numEvents
 * @param {number} numGames
 * @return {number}
 */
export function getHitRate(numEvents, numGames)
{
    if (numEvents > 0 && numGames > 0) {
        return numGames / numEvents;
    }
    else return 0;
};

/**
 * Returns the hit-rate of some event, within a number of games, as a ready
 * formatted string.
 * @param {number} numEvents
 * @param {number} numGames
 * @return {string}
 */
export function getHitRateString(numEvents, numGames)
{
    let hitrate = getHitRate(numEvents, numGames);
    let hitrateString = hitrate === 0? '1 in 0' : `1 in ${hitrate.toFixed(2)}`;
    return hitrateString;
};

/**
 * Calculates and returns a percentage relationship between 2 numbers, as a number.
 * @param {number} totalAmount 
 * @param {number} totalOutOf
 * @return {number}
 */
export function getPercentage(totalAmount, totalOutOf)
{
    if (totalAmount > 0 && totalOutOf > 0) {
        return totalAmount / totalOutOf * 100;
    }
    else return 0;
};

/**
 * Calculates and returns a percentage relationship between 2 numbers, as a ready
 * formatted string.
 * @param {number} amount 
 * @param {number} outOf
 * @return {string}
 */
export function getPercentageString(amount, outOf)
{
    let percentage = getPercentage(amount, outOf);
    let percentageString = `${percentage.toFixed(2)}%`;
    return percentageString;
};