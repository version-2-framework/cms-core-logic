//--------------------------------------------------------------------------------------------------
// Utility methods for fomatting / padding strings.
//--------------------------------------------------------------------------------------------------

/**
 * String constant filled with 60 chars of zeros.
 */
const ZEROS  = "000000000000000000000000000000000000000000000000000000000000";

/**
 * String constant filled with 60 chars of white space.
 */
const SPACES = "                                                            ";

/**
 * Pads a string with "0"s. If the parameter "str" has a length shorter than parameter
 * "paddedLength", then the length of the string returned will be equal to parameter
 * paddedLength, and the remaining space will be made up with "0"s. If "str" has a length
 * longer than "paddedLength", then "str" is returned unmodified.
 * @param {string} str The input string to pad.
 * @param {number} paddedLength The desired new length of the string.
 * @return {string} The padded string, guaranteed to be the requested length (or more).
 */
export function zeroPadString(str, paddedLength)
{
    str = str || "";
    paddedLength = paddedLength || 0;
    while (str.length < paddedLength)
    {
        str = ZEROS.substr(0, paddedLength - str.length) + str;
    }
    return str;
};

/**
 * Pads a string with white space. If the parameter "str" has a length shorter than
 * parameter "paddedLength", then the length of the string returned will be equal to
 * parameter paddedLength, and the remaining space will be made up with white space.
 * If "str" has a length longer than "paddedLength", then "str" is returned unmodified.
 * @param {string} str The input string to pad.
 * @param {number} paddedLength The desired new length of the string.
 * @return {string} The padded string, guaranteed to be the requested length (or more).
 */
export function spacePadString(str, paddedLength)
{
    str = str || "";
    paddedLength = paddedLength || 0;
    while (str.length < paddedLength)
    {
        str = SPACES.substr(0, paddedLength - str.length) + str;
    }
    return str;
};

/**
 * Accepts an array, and returns a formatted string. If the array is nested,
 * this returned string will have some meaningful formatting, regarding the
 * array's nested contents.
 * @param {any[]} arr
 */
export function arrayToString(arr)
{
    let ret = "[";
    for (let i = 0; i < arr.length; i ++)
    {
        if (Array.isArray(arr[i]))
        {
            ret += arrayToString(arr[i]);
        }
        else ret += JSON.stringify(arr[i]);
        if (i < arr.length - 1) ret += ",";
    }
    ret += "]";
    return ret;
};

/**
 * Accepts a Symbols matrix, and renders it as a formatted string, organized
 * in the order that we expect to see all of the symbols data (ie: similar to
 * on screen). The only exception, is that hands will be ordered vertically.
 * @param {SymbolsMatrix} matrix
 * The symbols matrix to parse into a string
 * @param {boolean} [longMode]
 * Indicates if this should be rendered in long form (using symbol ids). If
 * false (the default), then output will be rendered using only symbol ids.
 */
export function symbolsMatrixToString(matrix, longMode=false)
{
    let numHands = matrix.length;
    let numReels = matrix[0].length;
    let numChars = 3; // in short-mode, assume we need 2 chars to render a sym id, and 1 for space
    let output = "";

    // 1) Preparse the input, to find some constants, that will let us
    //    make a better guess about rendering the output.
    let maxNumChars = 0;
    let maxNumSym = 0;
    for (let handIndex = 0; handIndex < matrix.length; handIndex ++) {
        let hand = matrix[handIndex];
        for (let reelIndex = 0; reelIndex < hand.length; reelIndex ++) {
            let reel = hand[reelIndex];
            if (reel.length > maxNumSym) {
                maxNumSym = reel.length;
            }
            for (let posIndex = 0; posIndex < reel.length; posIndex ++) {
                let symName = reel[posIndex].name;
                if (symName.length > maxNumChars) {
                    maxNumChars = symName.length;
                }
            }
        }
    }

    if (longMode) {
        numChars = maxNumChars + 1;
    }

    // 2) Now, lets render the output
    for (let handIndex = 0; handIndex < matrix.length; handIndex ++) {
        let hand = matrix[handIndex];
        for (let posIndex = 0; posIndex < maxNumSym; posIndex ++) {
            let rowOutput = "";
            for (let reelIndex = 0; reelIndex < numReels; reelIndex ++) {
                let sym, reel = hand[reelIndex];
                if (posIndex < reel.length) {
                    sym = reel[posIndex];
                }

                // let's add the symbol rendering.
                if (sym) {
                    rowOutput += spacePadString(longMode?sym.name:sym.id.toString(), numChars);
                }
                else {
                    rowOutput += spacePadString("", numChars);
                }
            }
            output += rowOutput + (posIndex<maxNumSym-1?"\n":"");
        }
    }

    return output;
}