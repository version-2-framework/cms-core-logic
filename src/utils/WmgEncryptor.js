import CryptoJS from "crypto-js";

const KEY = CryptoJS.enc.Hex.parse('546869734973415365637265744b6579');
const IV  = CryptoJS.enc.Hex.parse('00000000000000000000000000000000');

/**
 * Default implementation of the Encryptor interface. Uses AES encoding (which is the "default"
 * used by client / server comms for the WMG platform).
 * @implements {Encryptor}
 */
export class WmgEncryptor
{
    constructor()
    {
        
    }

    /**
     * Encrypts a string (ready to be sent).
     * @public
     * @param {string} message
     * The message to encrypt.
     * @return {string}
     * Encrypted message.
     */
    encrypt(message)
    {	
        let encoded = CryptoJS.enc.Utf8.parse(message);
        
    	let cipher = CryptoJS.AES.encrypt(encoded, KEY, {
    		mode: CryptoJS.mode.CBC, 
    		padding: CryptoJS.pad.Pkcs7,
    		iv: IV
    	});
    
    	return cipher.toString();
    };
    
    /**
     * Decrypts a string (ready to be read from).
     * @public
     * @param {string} cipher
     * The message to decrypt.
     * @return {string}
     * The decrypted message.
     */
    decrypt(cipher)
    {
    	let convertedCipher = CryptoJS.lib.CipherParams.create({ciphertext: CryptoJS.enc.Base64.parse(cipher)});

    	let res = CryptoJS.AES.decrypt(convertedCipher, KEY, {
    		mode: CryptoJS.mode.CBC, 
    		padding: CryptoJS.pad.Pkcs7,
    		iv: IV
        });

    	return res.toString(CryptoJS.enc.Utf8);
    }
};