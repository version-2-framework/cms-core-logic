//--------------------------------------------------------------------------------------------------
// Utilities for formatting percentage values, when shown as text strings on-screen.
// This is generally used when we need to show an RTP value.
// Currently, this supports the idea of "country specific" formatting, but this may evolve in the
// future.
//--------------------------------------------------------------------------------------------------

import * as C_Country from "../const/C_Country";

/**
 * @param {number} value
 * @return {string}
 */
function formatPercentageItaly(value) {
    return `${value.toString().replace(".",",")} %`;
};

/**
 * @type {{[Country] : (number) => string}}
 */
let map = {
    [C_Country.ITALY] : formatPercentageItaly
};

/**
 * Formats a numerical value, into a percentage string. This is useful for when we need to render
 * Return to Player (RTP) information on-screen, and we want to pick a locale specific way of doing
 * it.
 * @param {string} country 
 * The string id of the country the game is being played in. If the value is not recognized, then
 * we return a default string rendering of the value.
 * @param {Country} value
 * The actual percentage value. This should be in a real percentage range, eg : "90.5%" will be passed
 * in as a value of 90.5
 * @return {string}
 * The percentage value, formatted to locale specific requirements.
 */
export default function formatPercentage(country, value) {
    if (country in map) {
        return map[country](value);
    }
    else return `${value}%`;
};