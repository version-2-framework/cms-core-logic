/**
 * Determines what Slot Layout object should be used to position reels, symbols, etc,
 * for the current device (desktop, mobile, tablet)
 * @param {*} app 
 * @return {SlotLayoutConfig}
 */
export default function getActiveSlotLayout(app)
{
    /** @type {PlatformApi} */
    let platform = app.platform;

    let config = app.config;

    if (platform.isDesktop() && config.slotLayoutDesktop) {
        return config.slotLayoutDesktop;
    }
    else
    if (platform.isMobile() && config.slotLayoutMobile) {
        return config.slotLayoutMobile;
    }
    else
    if (platform.isTablet() && config.slotLayoutTablet) {
        return config.slotLayoutTablet;
    }
    else
    if (platform.isMobileOrTablet() && config.slotLayoutMobileTablet) {
        return config.slotLayoutMobileTablet;
    }
    else return config;
}